﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class FileData : Entity
    {
        [DataMember(Name="id")]
        public string ID { get; set; }

        [DataMember(Name = "fileName")]
        public string FileName { get; set; }

        [DataMember(Name = "fileExt")]
        public string FileExt { get; set; }

        [DataMember(Name = "fileContent")]
        public string FileContent { get; set; }

        [DataMember(Name = "fileStream")]
        public System.IO.Stream FileStream { get; set; }
    }
}