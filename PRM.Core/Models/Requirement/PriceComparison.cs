﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class PriceComparison : Entity
    {
        [DataMember(Name = "priceCompareObject")]
        public List<PriceCompareObject> PriceCompareObject { get; set; }

        [DataMember(Name = "requirement")]
        public Requirement requirement { get; set; }

        [DataMember(Name = "minQuotationPrice")]
        public double MinQuotationPrice { get; set; }
    }
}