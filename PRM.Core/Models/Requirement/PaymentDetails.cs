﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class PaymentDetails : Entity
    {
        [DataMember(Name = "requirementID")]
        public int ReqID { get; set; }

        [DataMember(Name = "paymentDate")]
        public string PaymentDate { get; set; }

        [DataMember(Name = "paymentAmount")]
        public double PaymentAmount { get; set; }

        [DataMember(Name = "paymentMode")]
        public string PaymentMode { get; set; }

        [DataMember(Name = "bankDetails")]
        public string BankDetails { get; set; }

        [DataMember(Name = "comments")]
        public string Comments { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }
    }
}