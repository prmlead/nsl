﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class QuotationObject : Entity
    {
        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        [DataMember(Name = "quotation")]
        public byte[] Quotation { get; set; }

        [DataMember(Name = "fileType")]
        public string FileType { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID{get;set;}

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "itemPrice")]
        public double ItemPrice { get; set; }

        [DataMember(Name = "tax")]
        public double Tax { get; set; }

        [DataMember(Name = "isRevised")]
        public bool IsRevised { get; set; }
    }
}