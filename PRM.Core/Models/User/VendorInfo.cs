﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class VendorInfo : Entity
    {
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "contactNum")]
        public string ContactNum { get; set; }

        [DataMember(Name = "rating")]
        public int Rating { get; set; }

        [DataMember(Name = "category")]
        public string[] Category { get; set; }

        [DataMember(Name = "panNum")]
        public string PanNum { get; set; }

        [DataMember(Name = "serviceTaxNum")]
        public string ServiceTaxNum { get; set; }

        [DataMember(Name = "vatNum")]
        public string VatNum { get; set; }

        [DataMember(Name = "referringUserID")]
        public int ReferringUserID { get; set; }

        [DataMember(Name = "knownSince")]
        public string KnownSince { get; set; }

        [DataMember(Name = "username")]
        public string UserName { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "institution")]
        public string Institution { get; set; }
    }
}