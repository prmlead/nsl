﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class StoreFinance : ResponseAudit
    {
        [DataMember(Name = "storeItemInfo")]
        public StoreItem StoreItemInfo { get; set; }

        [DataMember(Name = "totalItems")]
        public decimal TotalItems { get; set; }

        [DataMember(Name = "inStockAmount")]
        public decimal InStockAmount { get; set; }

        [DataMember(Name = "totalStockAmount")]
        public decimal TotalStockAmount { get; set; }
    }
}