﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class StoreAudit : ResponseAudit
    {
        [DataMember(Name = "storeAuditID")]
        public int StoreAuditID { get; set; }

        [DataMember(Name = "storeItemInfo")]
        public StoreItem StoreItemInfo { get; set; }

        [DataMember(Name = "auditType")]
        public string AuditType { get; set; }

        [DataMember(Name = "quantity")]
        public int Quantity { get; set; }

        [DataMember(Name = "itemPrice")]
        public decimal ItemPrice { get; set; }
    }
}