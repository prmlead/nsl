﻿using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class LiveBidding : ResponseAudit
    {
        [DataMember(Name = "vendor")]
        public User Vendor { get; set; }

        [DataMember(Name = "vendorCompany")]
        public Company VendorCompany { get; set; }

        [DataMember(Name = "arrayKeyValue")]
        public KeyValuePair[] ArrayKeyValue { get; set; }
    }
}