﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class Answer : ResponseAudit
    {
        [DataMember(Name = "answerID")]
        public int AnswerID { get; set; }

        [DataMember(Name = "questionDetails")]
        public Question QuestionDetails { get; set; }

        [DataMember(Name = "vendor")]
        public UserDetails Vendor { get; set; }

        string answerText = string.Empty;
        [DataMember(Name = "answerText")]
        public string AnswerText
        {
            get
            {
                if (!string.IsNullOrEmpty(answerText))
                {
                    return answerText;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    answerText = value;
                }
            }
        }
        
        [DataMember(Name = "userMarks")]
        public decimal UserMarks { get; set; }

        [DataMember(Name = "attachmentID")]
        public int AttachmentID { get; set; }

        [DataMember(Name = "attachment")]
        public byte[] Attachment { get; set; }

        [DataMember(Name = "attachmentName")]
        public string AttachmentName { get; set; }

        string resComments = string.Empty;
        [DataMember(Name = "resComments")]
        public string ResComments
        {
            get
            {
                if (!string.IsNullOrEmpty(resComments))
                {
                    return resComments;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    resComments = value;
                }
            }
        }

    }
}