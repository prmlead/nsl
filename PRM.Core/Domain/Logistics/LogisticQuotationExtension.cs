﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Logistics
{
    public static  class LogisticQuotationExtension
    {

        public static decimal CalculateVal(decimal val, int valType, double qty)
        {

            decimal result;


            if (valType > 0 && val > 0 && qty > 0)
            {
                result = (val * Convert.ToDecimal(qty));
            }
            else if (val > 0 && qty > 0)
            {
                result = val;
            }
            else
            {
                result = 0;
            }

            return result;

        }

        public static decimal CalulateTotal(this LogisticQuotationReport entity)
        {
            decimal total  = 0;

            total += CalculateVal(entity.Ams, entity.AmsType, entity.ProductQuantity);
            total += CalculateVal(entity.Xray, entity.XrayType, entity.ProductQuantity);
            total += CalculateVal(entity.ForwordDgFee, entity.ForwordDgFeeType, entity.ProductQuantity);
            total += CalculateVal(entity.DgFee, entity.DgFeeType, entity.ProductQuantity);
            total += CalculateVal(entity.Fsc, entity.FscType, entity.ProductQuantity);
            total += CalculateVal(entity.Ssc, entity.SscType, entity.ProductQuantity);
            //total += CalculateVal(entity.ServiceCharges, entity.ServiceChargesType, entity.ProductQuantity);
            total += CalculateVal(entity.OtherCharges, entity.OtherChargesType, entity.ProductQuantity);
            total += CalculateVal(entity.OfferedRate, entity.OfferedRateType, entity.ProductQuantity);
            //total += CalculateVal(entity.CustomClearance, entity.CustomClearanceType, entity.ProductQuantity);
            //total += CalculateVal(entity.TerminalHandling, entity.TerminalHandlingType, entity.ProductQuantity);
            total += CalculateVal(entity.AwbFee, entity.AwbFeeType, entity.ProductQuantity);
            total += CalculateVal(entity.Misc, entity.MiscType, entity.ProductQuantity);
            return total;

        }

        public static decimal CalulateRevTotal(this LogisticQuotationReport entity)
        {
            decimal total = 0;

            total += CalculateVal(entity.Ams, entity.AmsType, entity.ProductQuantity);
            total += CalculateVal(entity.Xray, entity.XrayType, entity.ProductQuantity);
            total += CalculateVal(entity.ForwordDgFee, entity.ForwordDgFeeType, entity.ProductQuantity);
            total += CalculateVal(entity.DgFee, entity.DgFeeType, entity.ProductQuantity);
            total += CalculateVal(entity.Fsc, entity.FscType, entity.ProductQuantity);
            total += CalculateVal(entity.Ssc, entity.SscType, entity.ProductQuantity);
            //total += CalculateVal(entity.ServiceCharges, entity.ServiceChargesType, entity.ProductQuantity);
            total += CalculateVal(entity.RevisedOtherCharges, entity.RevisedOtherChargesType, entity.ProductQuantity);
            total += CalculateVal(entity.RevisedOfferedRate, entity.RevisedOfferedRateType, entity.ProductQuantity);
            //total += CalculateVal(entity.CustomClearance, entity.CustomClearanceType, entity.ProductQuantity);
            //total += CalculateVal(entity.TerminalHandling, entity.TerminalHandlingType, entity.ProductQuantity);
            total += CalculateVal(entity.AwbFee, entity.AwbFeeType, entity.ProductQuantity);
            total += CalculateVal(entity.Misc, entity.MiscType, entity.ProductQuantity);
            return total;

        }
    }
}
