﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Requirments
{

    [Table("requirement_template")]
  public  class RequirementTemplate : BaseEntity
    {
        public int Company_Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreatedOnUtc { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime? UpdatedOnUtc { get; set; }

        public bool LimitedToDepartment { get; set; }
        [Dapper.NotMapped]
        public int[] AllowedDepartments { get; set; }


    }
}
