﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Masters.GeneralTerms
{
    [Table("master_paymentterm")]
    public class PaymentTerm : BaseEntity
    {

        public int Company_Id { get; set; }

        public string Title { get; set; }

        public string Terms { get; set; }
        public bool Active { get; set; }

        public bool AllowEdit { get; set; }

        public bool LimitedToProject { get; set; }

        public bool LimitedToUser { get; set; }

        public bool LimitedToDepartment { get; set; }

        public bool Deleted { get; set; }

        public int CreatedBy { get; set; }

        public DateTime? CreatedOnUtc { get; set; }

        public int UpdatedBy { get; set; }
        public DateTime? UpdatedOnUtc { get; set; }

        [Dapper.NotMapped]
        public int[] AllowedUsers { get; set; }
        [Dapper.NotMapped]
        public int[] AllowedDepartments { get; set; }

        [Dapper.NotMapped]
        public int[] AllowedProjects { get; set; }
    }
}
