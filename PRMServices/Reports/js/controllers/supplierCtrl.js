﻿prmApp

    .controller('supplierCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {
            
            $scope.show = false;
            $scope.show1 = false;
            $scope.show2 = false;
            $scope.show3 = false;


            $scope.renderChart = function () {
                //$scope.Name = 'Supplier';
                $scope.Name = 'Defect Rate >= 2.5 ';
                $scope.data = [0,1,1.5,2,2.5,3,3.5];
                $scope.startTime = '08-01-2020 10:59';
                $scope.endTime = '08-07-2020 10:59';
                $scope.chartOptions = {
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                      
                        title: {
                           // text: 'Time ( Start Time: ' + '08-01-2020 10:59' + ' - End Time: ' + '08-07-2020 10:59' + ')'
                            text: ''
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        }
                    },
                    series: [{
                        name: $scope.Name,
                       // name1: $scope.Name1,
                        data: $scope.data
                    }]
                };
                $scope.show = true;
            };
            $scope.renderChart();

            $scope.renderSupplierAvailability = function () {
                //$scope.Name = 'Supplier';
                $scope.Name = 'Supplier Availability < 90% ';
                $scope.data = [0, 1, 1.5, 2, 2.5, 3];
                $scope.startTime = '08-01-2020 10:59';
                $scope.endTime = '08-07-2020 10:59';
                $scope.supplierAvailability = {
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {

                        title: {
                            // text: 'Time ( Start Time: ' + '08-01-2020 10:59' + ' - End Time: ' + '08-07-2020 10:59' + ')'
                            text: ''
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        }
                    },
                    series: [{
                        name: $scope.Name,
                        // name1: $scope.Name1,
                        data: $scope.data
                    }]
                };
                $scope.show1 = true;
            };
            $scope.renderSupplierAvailability();

            $scope.renderLeadTime = function () {
                //$scope.Name = 'Supplier';
                $scope.Name = 'Lead Time > 5.2 ';
                $scope.data = [0, 1, 1.5, 2, 2.5, 3];
                $scope.startTime = '08-01-2020 10:59';
                $scope.endTime = '08-07-2020 10:59';
                $scope.leadTime = {
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {

                        title: {
                            // text: 'Time ( Start Time: ' + '08-01-2020 10:59' + ' - End Time: ' + '08-07-2020 10:59' + ')'
                            text: ''
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        }
                    },
                    series: [{
                        name: $scope.Name,
                        // name1: $scope.Name1,
                        data: $scope.data
                    }]
                };
                $scope.show2 = true;
            };
            $scope.renderLeadTime();


            $scope.defectType = {};

            //$scope.supplierDefectType = function () {
            //    var charts = [],
            //        containers = document.querySelectorAll('#trellis td'),
            //        datasets = [{
            //            name: 'No Impact',
            //            data: [80, 66, 67, 60, 60]
            //        },
            //        {
            //            name: 'Impact',
            //            data: [5, 19, 11, 8, 21]
            //        },
            //        {
            //            name: 'Rejected',
            //            data: [15, 25, 22, 32,19]
            //        }
            //        ];

                

            //    datasets.forEach(function (dataset, i) {
            //        charts.push(Highcharts.chart(containers[i], {

            //            chart: {
            //                type: 'bar',
            //                marginLeft: i === 0 ? 100 : 10
            //            },

            //            title: {
            //                text: dataset.name,
            //                align: 'left',
            //                x: i === 0 ? 90 : 0
            //            },

            //            credits: {
            //                enabled: false
            //            },

            //            xAxis: {
            //                categories: ['Supplier 1', 'Supplier 2', 'Supplier 3', 'Supplier 4', 'Supplier 5'],
            //                labels: {
            //                    enabled: i === 0
            //                }
            //            },

            //            yAxis: {
            //                allowDecimals: false,
            //                title: {
            //                    text: null
            //                },
            //                min: 0,
            //                max: 100
            //            },


            //            legend: {
            //                enabled: false
            //            },

            //            series: [dataset]

            //        }));
            //        $scope.show3 = true;
            //    });
            //}
            //$scope.supplierDefectType();


            $scope.supplierDefectType = function () {
                Highcharts.chart('container', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Supplier Defect Rate & Defect Type'
                    },
                    xAxis: {
                        categories: ['Supplier 1', 'Supplier 2', 'Supplier 3', 'Supplier 4', 'Supplier 5']
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Defect Type'
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'gray'
                            }
                        }
                    },
                    legend: {
                        align: 'right',
                        x: -30,
                        verticalAlign: 'top',
                        y: 25,
                        floating: true,
                        backgroundColor:
                            Highcharts.defaultOptions.legend.backgroundColor || 'white',
                        borderColor: '#CCC',
                        borderWidth: 1,
                        shadow: false
                    },
                    tooltip: {
                        headerFormat: '<b>{point.x}</b><br/>',
                        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [{
                        name: 'No Impact',
                        data: [80, 66, 67, 60, 60]
                    }, {
                        name: 'Impact',
                            data: [5, 19, 11, 8, 21]
                    }, {
                        name: 'Rejected',
                            data: [15, 25, 22, 32, 19]
                    }]
                });
            }
            $scope.supplierDefectType();

            $scope.deliveryTime = function () {
                Highcharts.chart('deliveryTime', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Delivery Time'
                    },
                    xAxis: {
                        categories: ['Supplier 1', 'Supplier 2', 'Supplier 3', 'Supplier 4', 'Supplier 5']
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        reversed: true
                    },

                    plotOptions: {
                        series: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [{
                        name: 'No Impact',
                        data: [80, 66, 67, 60, 60]
                    }, {
                        name: 'Impact',
                        data: [5, 19, 11, 8, 21]
                    }, {
                        name: 'Rejected',
                        data: [15, 25, 22, 32, 19]
                    }]
                });
            }
            $scope.deliveryTime();
        }
    ]);