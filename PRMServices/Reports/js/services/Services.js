﻿prmApp.constant('PRMAnalysisServicesDomain', 'Reports/svc/PRMAnalysisService.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMAnalysisServices', ["PRMAnalysisServicesDomain", "userService", "httpServices",
    function (PRMAnalysisServicesDomain, userService, httpServices) {


        var PRMAnalysisServices = this;

        PRMAnalysisServices.GetPurchaserAnalysis = function (params) {
            let url = PRMAnalysisServicesDomain + 'GetPurchaserAnalysis?userID=' + params.userID + '&fromdate=' + params.fromDate + '&todate=' + params.toDate + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };
       
        PRMAnalysisServices.GetPRMAnalyticsTotals = function (params) {
            let url = PRMAnalysisServicesDomain + 'getprmanalyticstotals?fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMAnalysisServices.GetAnylaticsReports = function (params) {
            let url = PRMAnalysisServicesDomain + 'GetAnylaticsReports?userID=' + params.userID + '&from=' + params.from + ' &to=' + params.to + '&type=' + params.type + '&plantCode=' + params.plantCode + '&compID=' + params.compID + '&reqIDs=' + params.reqIDs + '&qcsID=' + params.qcsID + '&sessionid=' + userService.getUserToken() + '&search=' + params.search + '&page=' + params.page + '&pagesize=' + params.pagesize;
            return httpServices.get(url);
        };


        PRMAnalysisServices.GetOpenPRStatsbyMonth = function (params) {
            let url = PRMAnalysisServicesDomain + 'getopenprstatsbymonth?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMAnalysisServices.GetOpenPRData = function (params) {
            let url = PRMAnalysisServicesDomain + 'getopenprdata?compid=' + params.compid + '&yearmonth=' + params.yearmonth + '&page=' + params.page + '&pagesize=' + params.pagesize + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMAnalysisServices.GetOpenPOStatsbyMonth = function (params) {
            let url = PRMAnalysisServicesDomain + 'getopenpostatsbymonth?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMAnalysisServices.GetOpenPOData = function (params) {
            let url = PRMAnalysisServicesDomain + 'getopenpodata?compid=' + params.compid + '&yearmonth=' + params.yearmonth + '&page=' + params.page + '&pagesize=' + params.pagesize + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMAnalysisServices.GetPRMGRNAnalyticsTotals = function (params) {
            let url = PRMAnalysisServicesDomain + 'getprmgrnanalyticstotals?compid=' + params.compid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMAnalysisServices.getAnylaticsReportsData = function (params) {
            let url = PRMAnalysisServicesDomain + 'getanylaticsreportsdata?compid=' + params.compid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate + '&datasettype= ' + params.dataSetType + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        
        return PRMAnalysisServices;

    }]);