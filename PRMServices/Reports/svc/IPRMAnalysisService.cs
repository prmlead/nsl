﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMAnalysisService
    {



        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetPurchaserAnalysis?userID={userID}&fromDate={fromDate}&toDate={toDate}&sessionid={sessionid}")]
        PurchaserAnalysis GetPurchaserAnalysis(int userID, DateTime fromDate, DateTime toDate, string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getprmanalyticstotals?compid={compid}&fromdate={fromdate}&todate={todate}&sessionid={sessionid}")]
        PRMAnalyticsTotals GetPRMAnalyticsTotals(int compid, DateTime fromdate, DateTime todate, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAnylaticsReports?userID={userID}&from={from}&to={to}&type={type}&plantCode={plantCode}&compID={compID}&reqIDs={reqIDs}&qcsID={qcsID}&sessionID={sessionID}&search={search}&page={page}&pagesize={pagesize}")]
        OpsReports GetAnylaticsReports(int userID, string from, string to, string type, string plantCode, int compID, string reqIDs, int qcsID, string sessionID, string search, int page, int pagesize);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVendorNotParticipatedList?vendorID={vendorID}")]
        List<Requirement> GetVendorNotParticipatedList(int vendorID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getopenprstatsbymonth?compid={compid}&sessionid={sessionid}")]
        List<KeyValuePair> GetOpenPRStatsbyMonth(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getopenprdata?compid={compid}&yearmonth={yearmonth}&page={page}&pagesize={pagesize}&sessionid={sessionid}")]
        List<PRItems> GetOpenPRData(int compid, string yearmonth, int page, int pagesize, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getopenpostatsbymonth?compid={compid}&sessionid={sessionid}")]
        List<KeyValuePair> GetOpenPOStatsbyMonth(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getopenpodata?compid={compid}&yearmonth={yearmonth}&page={page}&pagesize={pagesize}&sessionid={sessionid}")]
        List<POReport> GetOpenPOData(int compid, string yearmonth, int page, int pagesize, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getprmgrnanalyticstotals?compid={compid}&fromDate={fromDate}&toDate={toDate}&sessionid={sessionid}")]
        PRMAnalyticsTotals GetPRMGRNAnalyticsTotals(int compid, DateTime fromdate, DateTime todate, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getanylaticsreportsdata?compid={compid}&fromDate={fromDate}&toDate={toDate}&sessionid={sessionid}")]
        List<GRNDetails> GetAnylaticsReportsData(int compid, DateTime fromdate, DateTime todate, string sessionid);
    }
}
