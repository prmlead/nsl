﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class PRMAnalyticsSpend : Entity
    {
        [DataMember]
        public List<KeyValuePair> DepartmentSpend { get; set; }

        [DataMember]
        public List<KeyValuePair> CategorySpend { get; set; }

        [DataMember]
        public List<KeyValuePair> DepartmentCategorySpend { get; set; }

        [DataMember]
        public List<KeyValuePair> DepartmentUserSpend { get; set; }
        [DataMember]
        public List<KeyValuePair> CategoryUserSpend { get; set; }
    }
}

