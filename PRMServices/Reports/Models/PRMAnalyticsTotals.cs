﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class PRMAnalyticsTotals : Entity
    {
        [DataMember]
        [DataNames("PARAM_TOTAL_PLANTS")]
        public int PARAM_TOTAL_PLANTS { get; set; }

        [DataMember]
        [DataNames("PARAM_TOTAL_USERS")]
        public int PARAM_TOTAL_USERS { get; set; }

        [DataMember]
        [DataNames("PARAM_TOTAL_DEPARTMENTS")]
        public int PARAM_TOTAL_DEPARTMENTS { get; set; }

        [DataMember]
        [DataNames("PARAM_TOTAL_CATEGORIES")]
        public int PARAM_TOTAL_CATEGORIES { get; set; }

        [DataMember]
        [DataNames("PARAM_TOTAL_SAVINGS")]
        public decimal PARAM_TOTAL_SAVINGS { get; set; }

        [DataMember]
        [DataNames("PARAM_TOTAL_SPEND")]
        public decimal PARAM_TOTAL_SPEND { get; set; }

        [DataMember]
        [DataNames("TOTAL_RFQS")]
        public int TOTAL_RFQS { get; set; }

        [DataMember]
        [DataNames("TOTAL_RFQ_PRS")]
        public int TOTAL_RFQ_PRS { get; set; }

        [DataMember]
        [DataNames("TOTAL_QCS")]
        public int TOTAL_QCS { get; set; }

        [DataMember]
        [DataNames("TOTAL_VENDORS")]
        public int TOTAL_VENDORS { get; set; }

        [DataMember]
        [DataNames("TOTAL_PR_PO_COUNT")]
        public int TOTAL_PR_PO_COUNT { get; set; }

        [DataMember]
        [DataNames("PLANTS")]
        public List<PlantValues> PLANTS { get; set; }

        [DataMember]
        [DataNames("TOTAL_GRN_COUNT")]
        public decimal TOTAL_GRN_COUNT { get; set; }

        [DataMember]
        [DataNames("TOTAL_GRN_VALUE")]
        public decimal TOTAL_GRN_VALUE { get; set; }

        [DataMember]
        [DataNames("TOTAL_VENDOR_COUNT")]
        public int TOTAL_VENDOR_COUNT { get; set; }

        [DataMember]
        [DataNames("TOTAL_PO_COUNT")]
        public int TOTAL_PO_COUNT { get; set; }

        [DataMember]
        [DataNames("TOTAL_MATERIAL_COUNT")]
        public int TOTAL_MATERIAL_COUNT { get; set; }

        [DataMember]
        [DataNames("TOTAL_CATEGORY_COUNT")]
        public int TOTAL_CATEGORY_COUNT { get; set; }

        [DataMember]
        [DataNames("TOTAL_COUNTRY_COUNT")]
        public int TOTAL_COUNTRY_COUNT { get; set; }

        [DataMember]
        [DataNames("TOTAL_PAYMENT_TERM_CODE_COUNT")]
        public int TOTAL_PAYMENT_TERM_CODE_COUNT { get; set; }        

        [DataMember]
        [DataNames("TOTAL_GRN_QTY")]
        public decimal TOTAL_GRN_QTY { get; set; }

        [DataMember]
        [DataNames("TOTAL_GRN_REJECTED_QTY")]
        public decimal TOTAL_GRN_REJECTED_QTY { get; set; }

        [DataMember]
        [DataNames("ASN_PERC")]
        public decimal ASN_PERC { get; set; }

        [DataMember]
        [DataNames("INVOICE_PERC")]
        public decimal INVOICE_PERC { get; set; }

    }

    public class PlantValues : Entity
    {
        [DataMember]
        [DataNames("PLANT_CODE")]
        public int PLANT_CODE { get; set; }

        [DataMember]
        [DataNames("PLANT_NAME")]
        public string PLANT_NAME { get; set; }
    }

}

