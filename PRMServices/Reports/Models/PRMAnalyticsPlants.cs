﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class PRMAnalyticsPlants : Entity
    {
        [DataMember]
        public List<KeyValuePair> DepartmentPlants { get; set; }

        [DataMember]
        public List<KeyValuePair> CategoryPlants { get; set; }

        [DataMember]
        public List<KeyValuePair> DepartmentCategoryPlants { get; set; }

        [DataMember]
        public List<KeyValuePair> DepartmentUserPlants { get; set; }
        [DataMember]
        public List<KeyValuePair> CategoryUserPlants { get; set; }
    }
}

