﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class PRMAnalyticsSavings : Entity
    {
        [DataMember]
        public List<KeyValuePair> DepartmentSavings { get; set; }

        [DataMember]
        public List<KeyValuePair> CategorySavings { get; set; }

        [DataMember]
        public List<KeyValuePair> DepartmentCategorySavings { get; set; }

        [DataMember]
        public List<KeyValuePair> DepartmentUserSavings { get; set; }

        [DataMember]
        public List<KeyValuePair> CategoryUserSavings { get; set; }
    }
}

