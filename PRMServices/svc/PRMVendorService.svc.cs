﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using PRM.Core.Models;
using PRM.Core;
using PRM.Services.Vendors;
using PRM.Services.Configurations;
using PRMServices.Models.Vendor;
using PRM.Core.Domain.Vendors;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PRMVendorService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PRMVendorService.svc or PRMVendorService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMVendorService : IPRMVendorService
    {
        public PRMVendorService(IWorkContext workingContext, IVendorService vendorServcie, IRegistrationFormService registrationFormService)
        {
            _workingContext = workingContext;
            _vendorServcie = vendorServcie;
            _registrationFormService = registrationFormService;
        }

        private readonly IWorkContext _workingContext;
        private readonly IVendorService _vendorServcie;
        private readonly IRegistrationFormService _registrationFormService;

        public Response CalculateVendorScore(string userId)
        {
            var score = _vendorServcie.CalculateScore(_workingContext.CurrentCompany, Convert.ToInt32(userId));

            return new Response() { ObjectID = score };
        }

        public List<VendorScoreModel>  GetVendorScore(string vendorId)
        {
            var list = _vendorServcie.GetVendorScore(_workingContext.CurrentCompany,Convert.ToInt32(vendorId)).ToList();

            var ModelList = new List<VendorScoreModel>();
            if(list != null && list.Any())
            {
                list.ForEach(e =>
                {
                    ModelList.Add(new VendorScoreModel() {
                        DefaultValue = Convert.ToInt32(e["DefaultValue"]),
                         Field = Convert.ToString(e["Field"]),
                          Value = Convert.ToInt32(e["Value"])
                    });

                });
            }


            return ModelList;
        }

        public Response SaveVendorScore(VendorScoreUpdateModel model)
        {
            var list = new List<VendorScore>();
           foreach(var score in model.Scores)
            {
                list.Add(new VendorScore() { Field = score.Field, Value = score.Value });
            }
            _vendorServcie.SaveVendorScore(model.User_Id, list);

            return new Response() { Message = "Updated successfully" };

        }


        public int[] GetVendorSubCategories(string userId)
        {
            return _vendorServcie.GetVendorSubCategort(Convert.ToInt32(userId));
        }

        public Response SaveVendorStatus(VendorStatusUpdateModel model)
        {
            var venorr = _vendorServcie.GetVendorList(new { U_ID = model.User_Id }).FirstOrDefault();
            if(venorr != null)
            {
                venorr.Status = model.Status;
                venorr.Reason = model.Reason;
                venorr.Comment = model.Comment;
                _vendorServcie.UpdateVendor(venorr);
                _vendorServcie.UpdateVedorCategory(model.User_Id, model.Category);
                _vendorServcie.UpdateVedorSubCategory(model.User_Id, _workingContext.CurrentCompany, model.SubCategories);
            }

            return new Response() { Message = "Vendor status updated" };

        }
    }
}
