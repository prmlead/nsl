﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMPOService
    {
        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpoinformation?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        //List<POInformation> GetPOInformation(int reqID, int userID, string sessionID);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //RequestFormat = WebMessageFormat.Json,
        //ResponseFormat = WebMessageFormat.Json,
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //UriTemplate = "savepoinfo")]List<PaymentInfo> GetPendingPayments(int compid, string sessionid)
        //Response SavePOInfo(POInformation[] poList, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdespoinfo?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        POVendor GetDesPoInfo(int reqID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendors?reqid={reqID}&sessionid={sessionID}")]
        List<UserDetails> GetVendors(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdescdispatch?poid={poID}&dtid={dtID}&sessionid={sessionID}")]
        DispatchTrack GetDescDispatch(int poID, int dtID, string sessionID);
        
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorpolist?reqid={reqID}&userid={userID}&poid={poID}&sessionid={sessionID}")]
        VendorPO GetVendorPoList(int reqID, int userID, int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorpoinfo?reqid={reqID}&userid={userID}&poid={poID}&sessionid={sessionID}")]
        VendorPO GetVendorPoInfo(int reqID, int userID, int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdispatchtracklist?poorderid={poorderid}&sessionid={sessionID}")]
        List<DispatchTrack> GetDispatchTrackList(string poorderid, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdispatchtrack?poorderid={poorderid}&dcode={dcode}&sessionid={sessionID}")]
        List<DispatchTrack> GetDispatchTrack(string poorderid, string dcode, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpaymenttrack?vendorid={vendorID}&poid={poID}&sessionid={sessionID}")]
        PaymentTrack GetPaymentTrack(int vendorID, int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposchedule?ponumber={ponumber}&sessionid={sessionID}")]
        List<POSchedule> GetPOScheduler(string ponumber, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAssignedPO?userid={userId}&sessionid={sessionID}")]
        List<PendingPO> GetAssignedPO(int userId, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getCompanyPendingPOS?compid={compId}&sessionid={sessionID}&isVendor={IsVendor}&userId={UserId}")]
        List<PendingPO> GetCompanyPendingPOS(int compId, string sessionID, bool IsVendor, int UserId);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetFiltersOnLoadData?compid={compId}&sessionid={sessionID}")]
        List<Filter> GetFiltersOnLoadData(int compId, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getDeliverySchedules?poNumbers={poNumbers}&sessionid={sessionID}")]
        List<POSchedule> GetDeliverySchedules(string poNumbers,string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpendingpayments?compid={compid}&sessionid={sessionid}")]
        List<PaymentInfo> GetPendingPayments(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposchedulefilters?compid={compid}&sessionid={sessionid}")]
        List<CArrayKeyValue> GetPOScheduleFilters(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposchedulelist?compid={compid}&uid={uid}&search={search}" +
            "&categoryid={categoryid}&productid={productid}&supplier={supplier}&postatus={postatus}&deliverystatus={deliverystatus}&plant={plant}&fromdate={fromdate}&todate={todate}&page={page}&pagesize={pagesize}" +
            "&sessionid={sessionid}")]
        List<POScheduleDetails> GetPOScheduleList(int compid,string uid,string search, string categoryid, string productid, string supplier, string postatus, string deliverystatus, string plant,
            string fromdate, string todate, int page, int pagesize, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposcheduleitems?ponumber={ponumber}&moredetails={moredetails}&forasn={forasn}&sessionid={sessionid}")]
        List<POScheduleDetailsItems> GetPOScheduleItems(string ponumber, int moredetails, bool forasn, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpoinvoicedetails?ponumber={ponumber}&sessionid={sessionid}")]
        POInvoice[] GetPOInvoiceDetails(string ponumber, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getgrndetailslist?compid={compid}&uid={uid}&search={search}" +
            "&supplier={supplier}&fromdate={fromdate}&todate={todate}&page={page}&pagesize={pagesize}" +
            "&sessionid={sessionid}")]
        List<GRNDetails> GetGRNDetailsList(int compid, string uid, string search, string supplier, string fromdate, string todate, int page, int pagesize, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getinvoicelist?compid={COMP_ID}&userid={U_ID}&sessionid={sessionid}")]
        List<POInvoice> GetInvoiceList(int COMP_ID, int U_ID, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "savepoinvoice")]
        Response SavePOInvoice(POInvoice details);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //RequestFormat = WebMessageFormat.Json,
        //ResponseFormat = WebMessageFormat.Json,
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //UriTemplate = "savepoinvoice")]
        //Response SavePOInvoice(POInvoice[] poInvDet);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deletepoinvoice")]
        Response DeletePOInvoice(string ponumber, string invoicenumber, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveposchedule")]
        Response SavePOSchedule(POSchedule details);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //RequestFormat = WebMessageFormat.Json,
        //ResponseFormat = WebMessageFormat.Json,
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //UriTemplate = "SaveVendorAck")]
        //Response SaveVendorAck(POSchedule details);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savepovendorquantityack")]
        Response SavePOVendorQuantityAck(string ponumber, string poitemline, string status, string comments, int isVendPoAck, decimal quantity, int user, string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getasndetails?compid={compid}&asnid={asnid}&ponumber={ponumber}&grncode={grncode}&asncode={asncode}&vendorid={vendorid}&sessionid={sessionid}")]
        List<ASNDetails> GetASNDetails(int compid, int asnid, string asncode, string ponumber, string grncode, int vendorid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getasndetailslist?ponumber={ponumber}&vendorid={vendorid}&sessionid={sessionid}")]
        List<ASNDetails> GetASNDetailsList(string ponumber, int vendorid, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveasndetails")]
        Response SaveASNDetails(ASNDetails[] detailsarray);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "checkuniqueifexists")]
        bool CheckUniqueIfExists(string param, string idtype, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedescpoinfo")]
        Response SaveDescPoInfo(POVendor povendor);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatepostatus")]
        Response UpdatePOStatus(int poID, string status, string sessionID);
        
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedesdispatchtrack")]
        Response SaveDesDispatchTrack(DispatchTrack dispatchtrack, POVendor povendor);        

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedispatchtrack")]
        Response SaveDispatchTrack(DispatchTrack dispatchtrack, string requestType);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savevendorpoinfo")]
        Response SaveVendorPOInfo(VendorPO vendorpo);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savepaymentinfo")]
        Response SavePaymentInfo(PaymentInfo paymentinfo);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpogeneratedetails?compid={compid}&template={template}&vendorid={vendorid}&status={status}&creator={creator}" +
            "&plant={plant}&purchasecode={purchasecode}&search={search}&sessionid={sessionid}&page={page}&pagesize={pagesize}")]
        SAPOEntity[] GetPOGenerateDetails(int compid, string template, int vendorid, string status, string creator,
            string plant, string purchasecode, string search, string sessionid, int page = 0, int pagesize = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpoitems?ponumber={ponumber}&quotno={quotno}&sessionid={sessionid}")]
        SAPOEntity[] GetPOItems(string ponumber, string quotno, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getFilterValues?compid={compID}&sessionid={sessionID}")]
        List<POFieldMapping> GetFilterValues(int compID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementpo?compid={compid}&reqid={reqid}&sessionid={sessionid}")]
        SAPOEntity[] GetRequirementPO(int compid, int reqid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "lastupdatedate?table={table}&sessionid={sessionid}")]
        Response GetLastUpdatedDate(string table, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openporeport?compid={compid}&sessionid={sessionid}")]
        List<OpenPO> GetOpenPOReport(int compid, string sessionid);
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openpo?compid={compid}&pono={pono}&plant={plant}&purchase={purchase}&sessionid={sessionid}&exclude={exclude}")]
        List<OpenPO> GetOpenPO(int compid, string pono, int plant, int purchase, string sessionid, int exclude);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "sapaccess?userid={userid}&sessionid={sessionid}")]
        List<Response> GetSapAccess(int userid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getSplitPODetails?compid={compid}&reqid={reqid}&qcsId={qcsId}&sessionid={sessionid}")]
        FTPPOSchedule[] GetSplitPODetails(int compid, int reqid, int qcsId, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getPaymentInvoiceDetails?sessionid={sessionid}&compId={compId}")]
        List<PaymentInfo> GetPaymentInvoiceDetails(string sessionid, int compId);
    }
}
