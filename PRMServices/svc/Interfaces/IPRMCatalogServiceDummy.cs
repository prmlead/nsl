﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
//using PRM.Core.Models;
//using PRMServices.Models;
using PRMServices.Models.Catalog;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPRMRealTimePriceService" in both code and config file together.
    [ServiceContract]
    public interface IPRMCatalogServiceDummy
    {

        #region Attribute calls
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAttribute?CompanyId={CompanyId}&sessionid={sessionId}")]
        List<AttributeModel> GetAttribute(int CompanyId, string sessionId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveAttribute")]
        CatalogResponse SaveAttribute(AttributeModel property, string sessionId);

        #endregion Attribute calls






    }
}
 