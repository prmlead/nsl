﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMStoreService : IPRMStoreService
    {
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        public List<Store> GetStores(int storeID, string storeCode, int compID, int includeBranch, string sessionID)
        {
            if (string.IsNullOrEmpty(storeCode))
            {
                storeCode = string.Empty;
            }

            List<Store> details = new List<Store>();
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_STORE_ID", storeID);
                sd.Add("P_STORE_CODE", storeCode);
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_INCLUDE_BRANCH", includeBranch);
                DataSet ds = sqlHelper.SelectList("st_GetStoreDetails", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Store detail = new Store();
                        detail = StoreUtility.GetStoreObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                Store errorInfo = new Store();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<Store> GetCompanyStores(int compID, int parentStoreID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<Store> details = new List<Store>();
            try
            {
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_PARENT_STORE_ID", parentStoreID);
                DataSet ds = sqlHelper.SelectList("st_GetCompanyStores", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Store detail = new Store();
                        detail = StoreUtility.GetStoreObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                Store errorInfo = new Store();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<StoreItem> GetStoreItems(int storeID, int itemID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<StoreItem> details = new List<StoreItem>();
            try
            {
                sd.Add("P_STORE_ID", storeID);
                sd.Add("P_ITEM_ID", itemID);
                DataSet ds = sqlHelper.SelectList("st_GetStoreInventory", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        StoreItem detail = StoreUtility.GetStoreItemObject(row);

                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                StoreItem errorInfo = new StoreItem();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<StoreAudit> GetStoreItemAudit(int storeID, int itemID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<StoreAudit> details = new List<StoreAudit>();
            try
            {
                sd.Add("P_STORE_ID", storeID);
                sd.Add("P_ITEM_ID", itemID);
                DataSet ds = sqlHelper.SelectList("st_GetStoreItemAudit", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        StoreAudit detail = new StoreAudit();

                        detail.StoreAuditID = row["STORE_Audit_ID"] != DBNull.Value ? Convert.ToInt32(row["STORE_Audit_ID"]) : -1;
                        detail.StoreItemInfo = StoreUtility.GetStoreItemObject(row);
                        detail.AuditType = row["AUDIT_TYPE"] != DBNull.Value ? Convert.ToString(row["AUDIT_TYPE"]) : string.Empty;
                        detail.Quantity = row["QUANTITY"] != DBNull.Value ? Convert.ToInt32(row["QUANTITY"]) : 0;
                        detail.ItemPrice = row["ITEM_PRICE"] != DBNull.Value ? Convert.ToDecimal(row["ITEM_PRICE"]) : 0;

                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                StoreAudit errorInfo = new StoreAudit();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<StoreItem> GetStoreItemAlert(int storeID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<StoreItem> details = new List<StoreItem>();
            try
            {
                sd.Add("P_STORE_ID", storeID);
                DataSet ds = sqlHelper.SelectList("st_GetStoreInventoryThresholdAlert", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        StoreItem detail = StoreUtility.GetStoreItemObject(row);

                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                StoreItem errorInfo = new StoreItem();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<StoreFinance> GetStoreFinance(int storeID, int consolidate, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<StoreFinance> details = new List<StoreFinance>();
            try
            {
                sd.Add("P_STORE_ID", storeID);
                sd.Add("P_CONSOLIDATE", consolidate);
                DataSet ds = sqlHelper.SelectList("st_StoreFinances", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        StoreFinance detail = new StoreFinance();
                        detail.StoreItemInfo = StoreUtility.GetStoreItemObject(row);
                        detail.InStockAmount = Convert.ToDecimal(row.GetColumnValue("IN_STOCK_TOTAL_AMOUNT", detail.InStockAmount.GetType()));
                        detail.TotalStockAmount = Convert.ToDecimal(row.GetColumnValue("TOTAL_STOCK_TOTAL_AMOUNT", detail.TotalStockAmount.GetType()));
                        detail.TotalItems = Convert.ToDecimal(row.GetColumnValue("TOTAL_ITEMS", detail.TotalItems.GetType()));
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                StoreFinance errorInfo = new StoreFinance();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<StoreItem> GetStoresItemStatus(int compID, string itemCode, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<StoreItem> details = new List<StoreItem>();
            try
            {
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_ITEM_CODE", itemCode);
                DataSet ds = sqlHelper.SelectList("st_GetStoreItemStatusAcrossStores", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        StoreItem detail = StoreUtility.GetStoreItemObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                StoreItem errorInfo = new StoreItem();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<CustomProperty> GetCompanyProps(int compID, string module, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<CustomProperty> details = new List<CustomProperty>();
            try
            {
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_CUST_MODULE", module);
                DataSet ds = sqlHelper.SelectList("cp_GetCompanyCustProp", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        CustomProperty detail = PRMUtility.GetCustPropObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                CustomProperty errorInfo = new CustomProperty();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<CustomProperty> GetCompanyPropValues(int entityID, string module, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<CustomProperty> details = new List<CustomProperty>();
            try
            {
                sd.Add("P_ENTITY_ID", entityID);
                sd.Add("P_CUST_MODULE", module);
                DataSet ds = sqlHelper.SelectList("cp_GetCompanyCustPropValues", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        CustomProperty detail = PRMUtility.GetCustPropObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                CustomProperty errorInfo = new CustomProperty();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<StoreItemDetails> GetStoreItemDetails(int itemID, int notDispatched, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<StoreItemDetails> details = new List<StoreItemDetails>();
            try
            {
                sd.Add("P_ITEM_ID", itemID);
                sd.Add("P_NOT_DISPATCHED", notDispatched);
                DataSet ds = sqlHelper.SelectList("st_GetStoreItemDetails", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        StoreItemDetails detail = new StoreItemDetails();
                        detail = StoreUtility.GetStoreItemDetailsObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                StoreItemDetails errorInfo = new StoreItemDetails();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<StoreItemDetails> GetStoreIndentItemDetails(int storeID, string ginCode, string grnCode, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<StoreItemDetails> details = new List<StoreItemDetails>();
            try
            {
                sd.Add("P_STORE_ID", storeID);
                sd.Add("P_GIN_CODE", ginCode.Trim());
                sd.Add("P_GRN_CODE", grnCode.Trim());
                DataSet ds = sqlHelper.SelectList("st_GetIndentItemDetails", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        StoreItemDetails detail = new StoreItemDetails();
                        detail = StoreUtility.GetStoreItemDetailsObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                StoreItemDetails errorInfo = new StoreItemDetails();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<StoreItemReceive> GetStoreItemReceiveDetails(string grnCode, int storeID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<StoreItemReceive> details = new List<StoreItemReceive>();
            try
            {
                sd.Add("P_GRN_CODE", grnCode);
                sd.Add("P_STORE_ID", storeID);
                DataSet ds = sqlHelper.SelectList("st_GetStoreItemReceiveDetails", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(grnCode))
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            StoreItemReceive detail = StoreUtility.GetStoreItemReceiveDetailsObject(row);
                            details.Add(detail);
                        }
                    }
                    else
                    {
                        StoreItemReceive detail = StoreUtility.GetStoreItemReceiveDetailsObject(ds.Tables[0].Rows[0]);
                        List<StoreItem> storeItems = new List<StoreItem>();
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            StoreItem item = StoreUtility.GetStoreItemObject(row);
                            storeItems.Add(item);
                        }

                        detail.StoreItems = storeItems.ToArray();
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                StoreItemReceive errorInfo = new StoreItemReceive();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<StoreItemIssue> GetStoreItemIssueDetails(string ginCode, int storeID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<StoreItemIssue> details = new List<StoreItemIssue>();
            ginCode = string.IsNullOrEmpty(ginCode) ? string.Empty : ginCode;
            try
            {
                sd.Add("P_GIN_CODE", ginCode);
                sd.Add("P_STORE_ID", storeID);
                DataSet ds = sqlHelper.SelectList("st_GetStoreItemIssueDetails", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(ginCode))
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            StoreItemIssue detail = StoreUtility.GetStoreItemIssueDetailsObject(row);
                            details.Add(detail);
                        }
                    }
                    else
                    {
                        StoreItemIssue detail = StoreUtility.GetStoreItemIssueDetailsObject(ds.Tables[0].Rows[0]);
                        List<StoreItem> storeItems = new List<StoreItem>();
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            StoreItem item = StoreUtility.GetStoreItemObject(row);
                            storeItems.Add(item);
                        }

                        detail.StoreItemDetails = storeItems.ToArray();
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                StoreItemIssue errorInfo = new StoreItemIssue();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public List<StoreItem> GetUniqueItems(int compID, string itemName, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<StoreItem> details = new List<StoreItem>();
            try
            {
                if (itemName == null || itemName.Trim() == string.Empty)
                {
                    itemName = string.Empty;
                }

                sd.Add("P_COMP_ID", compID);
                sd.Add("P_ITEM_NAME", itemName);
                DataSet ds = sqlHelper.SelectList("st_GetUniqueItems", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        StoreItem detail = StoreUtility.GetStoreItemObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                StoreItem errorInfo = new StoreItem();
                errorInfo.ErrorMessage = ex.Message;
                details.Add(errorInfo);
            }

            return details;
        }

        public Response SaveStore(Store store)
        {
            Response response = new Response(); Utilities.ValidateSession(store.SessionID);
            try
            {
                DataSet ds = StoreUtility.SaveStoreEntity(store);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveStoreItem(StoreItem item)
        {
            Response response = new Response();
            Utilities.ValidateSession(item.SessionID);
            try
            {
                DataSet ds = StoreUtility.SaveStoreItemEntity(item);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveStoreAudit(StoreAudit audit)
        {
            Utilities.ValidateSession(audit.SessionID);
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_STORE_Audit_ID", audit.StoreAuditID);
                sd.Add("P_STORE_ID", audit.StoreItemInfo.StoreDetails.StoreID);
                sd.Add("P_ITEM_ID", audit.StoreItemInfo.ItemID);
                sd.Add("P_AUDIT_TYPE", audit.AuditType);
                sd.Add("P_QUANTITY", audit.Quantity);
                sd.Add("P_ITEM_PRICE", audit.ItemPrice);
                sd.Add("P_USER", audit.ModifiedBY);
                DataSet ds = sqlHelper.SelectList("st_SaveStoreItemAudit", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveCompanyProp(CustomProperty companyProp)
        {
            Response response = new Response();
            Utilities.ValidateSession(companyProp.SessionID);
            try
            {
                DataSet ds = PRMUtility.SaveCompanyCustProp(companyProp);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveCompanyPropValue(CustomProperty companyProp)
        {
            Utilities.ValidateSession(companyProp.SessionID);
            Response response = new Response();
            try
            {
                DataSet ds = PRMUtility.SaveCompanyPropValue(companyProp);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveStoreItemDetails(StoreItemDetails item)
        {
            Utilities.ValidateSession(item.SessionID);
            Response response = new Response();
            try
            {
                DataSet ds = StoreUtility.SaveStoreItemDetailsEntity(item);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveStoreItemReceive(StoreItemReceive item)
        {
            Utilities.ValidateSession(item.SessionID);
            Response response = new Response();
            try
            {
                DataSet ds = StoreUtility.SaveStoreItemReceiveEntity(item);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveStoreItemIssue(StoreItemIssue item)
        {
            Utilities.ValidateSession(item.SessionID);
            Response response = new Response();
            try
            {
                DataSet ds = StoreUtility.SaveStoreItemIssueEntity(item);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response TransferStock(int sourceStoreId, int destStoreId, string itemCode, int quantity, decimal itemPrice, int user, string SessionID)
        {
            Utilities.ValidateSession(SessionID);
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_SOURCE_STORE_ID", sourceStoreId);
                sd.Add("P_DEST_STORE_ID", destStoreId);
                sd.Add("P_ITEM_CODE", itemCode);
                sd.Add("P_QUANTITY", quantity);
                sd.Add("P_ITEM_PRICE", itemPrice);
                sd.Add("P_USER", user);
                DataSet ds = sqlHelper.SelectList("st_StoreTransferItemStock", sd);
                response.ObjectID = 1;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response DeleteStore(int storeId, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            Response response = new Response();
            try
            {
                string query = string.Format("UPDATE Store SET IS_VALID = 0 WHERE STORE_ID = {0};", storeId);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                response.ObjectID = storeId;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response DeleteStoreItem(int storeItemId, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            Response response = new Response();
            try
            {
                string query = string.Format("UPDATE StoreItems SET IS_VALID = 0 WHERE ITEM_ID = {0};", storeItemId);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                response.ObjectID = storeItemId;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response DeleteStoreItemDetails(int storeItemDetailsId, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            Response response = new Response();
            try
            {
                string query = string.Format("DELETE FROM StoreItemDetails WHERE ITEM_DETAIL_ID = {0};", storeItemDetailsId);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                response.ObjectID = storeItemDetailsId;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response ImportEntity(ImportEntity entity)
        {
            Utilities.ValidateSession(entity.SessionID);
            string moreInfo = string.Empty;
            DataTable currentData = new DataTable();
            Response response = new Response();
            string sheetName = string.Empty;

            try
            {

                if (entity.Attachment != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(entity.Attachment, 0, entity.Attachment.Length);
                        using (ExcelPackage package = new ExcelPackage(ms))
                        {
                            currentData = StoreUtility.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                            sheetName = package.Workbook.Worksheets[1].Name;
                        }
                    }
                }

                #region Stores Entity

                if (string.Compare(entity.EntityName, "Stores", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
                {
                    if (!sheetName.Equals("Stores", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return response;
                    }

                    int count = 0;
                    foreach (DataRow row in currentData.Rows)
                    {
                        Store store = StoreUtility.GetStoreObject(row);                        
                        DataSet ds = StoreUtility.SaveStoreEntity(store);                        
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            int resp = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            if (resp > 0)
                            {
                                count++;
                            }
                        }
                    }

                    response.ObjectID = count;
                }
                #endregion

                #region StoreItems Entity

                if (string.Compare(entity.EntityName, "StoreItems", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
                {
                    if (!sheetName.Equals("StoreItems", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return response;
                    }

                    string message = string.Empty;
                    int count = 0;
                    foreach (DataRow row in currentData.Rows)
                    {
                        try
                        {
                            StoreItem storeItem = StoreUtility.GetStoreItemObject(row);
                            DataSet ds = StoreUtility.SaveStoreItemEntity(storeItem);
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                int resp = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                                if (resp > 0)
                                {
                                    count++;
                                }
                            }
                        }
                        catch
                        {
                            message = message + row["ITEM_CODE"].ToString() + ",";
                            continue;
                        }                        
                    }

                    if (!string.IsNullOrEmpty(message))
                    {
                        response.Message = "Failed to save items:" + message;
                    }

                    response.ObjectID = count;
                }
                #endregion

                #region StoreItemsProp Entity

                if (string.Compare(entity.EntityName, "CompanyProp", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
                {
                    if (!sheetName.Equals("CompanyProp", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return response;
                    }

                    string message = string.Empty;
                    int count = 0;
                    foreach (DataRow row in currentData.Rows)
                    {
                        try
                        {
                            CustomProperty companyProp = PRMUtility.GetCustPropObject(row);
                            DataSet ds = PRMUtility.SaveCompanyCustProp(companyProp);
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                int resp = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                                if (resp > 0)
                                {
                                    count++;
                                }
                            }
                        }
                        catch
                        {
                            message = message + row["CUST_PROP_CODE"].ToString() + ",";
                            continue;
                        }
                    }

                    if (!string.IsNullOrEmpty(message))
                    {
                        response.Message = "Failed to save:" + message;
                    }

                    response.ObjectID = count;
                }
                #endregion

                #region StoreItemsPropValues Entity

                if (string.Compare(entity.EntityName, "CompanyPropValues", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
                {
                    if (!sheetName.Equals("CompanyPropValues", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return response;
                    }

                    DataSet ds = new DataSet();
                    string message = string.Empty;
                    int count = 0;
                    foreach (DataRow row in currentData.Rows)
                    {
                        try
                        {
                            CustomProperty companyProp = PRMUtility.GetCustPropObject(row);
                            if(companyProp.CustPropID <=0)
                            {
                                ds = PRMUtility.SaveCompanyCustProp(companyProp);
                                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                                {
                                    int resp = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                                    companyProp.CustPropID = resp;
                                }
                            }

                            ds = PRMUtility.SaveCompanyPropValue(companyProp);
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                int resp = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                                if (resp > 0)
                                {
                                    count++;
                                }
                            }
                        }
                        catch
                        {
                            message = message + row["CUST_PROP_CODE"].ToString() + ",";
                            continue;
                        }
                    }

                    if (!string.IsNullOrEmpty(message))
                    {
                        response.Message = "Failed to save:" + message;
                    }

                    response.ObjectID = count;
                }
                #endregion

                #region StoreItemsPropValues Entity

                if (string.Compare(entity.EntityName, "StoreItemDetails", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
                {
                    if (!sheetName.Equals("StoreItemDetails", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return response;
                    }

                    DataSet ds = new DataSet();                    
                    string message = string.Empty;
                    int count = 0;
                    foreach (DataRow row in currentData.Rows)
                    {
                        try
                        {
                            StoreItemDetails storeItemDetails = StoreUtility.GetStoreItemDetailsObject(row);
                            ds = StoreUtility.SaveStoreItemDetailsEntity(storeItemDetails);
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                int resp = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                                if (resp > 0)
                                {
                                    count++;
                                }
                            }
                        }
                        catch
                        {
                            message = message + row["SERIAL_NO"].ToString() + ",";
                            continue;
                        }
                    }

                    if (!string.IsNullOrEmpty(message))
                    {
                        response.Message = "Failed to save:" + message;
                    }

                    response.ObjectID = count;
                }
                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message + moreInfo;
            }

            return response;
        }
    }
}