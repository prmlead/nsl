﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMTechEvalService : IPRMTechEvalService
    {
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();

        #region Get
        public Questionnaire GetQuestionnaire(int evalID, int GetQuestions, string sessionID)
        {
            Questionnaire questionnaire = new Questionnaire();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionID);
                sd.Add("P_EVAL_ID", evalID);
                sd.Add("P_GET_QUESTIONS", GetQuestions);
                DataSet ds = sqlHelper.SelectList("te_GetQuestionnaire", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    questionnaire = TechEvalUtility.GetQuestionnaireObject(row);
                }
                List<Question> questions = new List<Question>();
                if (GetQuestions == 1 && ds.Tables.Count > 1)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        Question question = new Question();
                        question = TechEvalUtility.GetQuestion(row);
                        questions.Add(question);
                    }
                }

                questionnaire.Questions = questions.ToArray();
                questionnaire.Vendors = GetVendorsForTechEval(evalID, sessionID);
            }
            catch (Exception ex)
            {
                questionnaire.ErrorMessage = ex.Message;
            }

            return questionnaire;
        }

        public List<Answer> GetResponses(int userID, int evalID, int reqID, string sessionID)
        {
            List<Answer> answers = new List<Answer>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionID);
                sd.Add("P_EVAL_ID", evalID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("te_GetResponses", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Answer answer = new Answer();
                        answer = TechEvalUtility.GetAnswer(row);
                        answer.QuestionDetails = TechEvalUtility.GetQuestion(row);
                        answers.Add(answer);
                    }
                }
            }
            catch (Exception ex)
            {
                Answer answer = new Answer();
                answer.ErrorMessage = ex.Message;
                answers.Add(answer);
            }

            return answers;
        }

        public TechEvalReport GetEvalReport(int userID, int evalID, string sessionID)
        {
            TechEvalReport report = new TechEvalReport();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionID);
                sd.Add("P_EVAL_ID", evalID);
                DataSet ds = sqlHelper.SelectList("te_GetEvalReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    Questionnaire question = TechEvalUtility.GetQuestionnaireObject(row);
                    report.Description = question.Description;
                    report.EndDate = question.EndDate;
                    report.EvalID = question.EvalID;
                    report.ReqID = question.ReqID;
                    report.StartDate = question.StartDate;
                    report.Title = question.Title;
                    report.CreatedBy = question.CreatedBy;
                }

                List<Answer> vendors = new List<Answer>();
                if (ds.Tables.Count > 1)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        Answer vendor = new Answer();
                        vendor = TechEvalUtility.GetVendorReport(row);
                        vendors.Add(vendor);
                    }
                }

                report.Vendors = vendors.ToArray();
            }
            catch (Exception ex)
            {
                report.ErrorMessage = ex.Message;
            }

            return report;
        }

        public List<Questionnaire> GetQuestionnaireList(int userID, int evalID, string sessionID)
        {
            List<Questionnaire> questionnaireList = new List<Questionnaire>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_EVAL_ID", evalID);
                DataSet ds = sqlHelper.SelectList("te_GetQuestionnaireList", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Questionnaire questionnaire = new Questionnaire();
                        questionnaire = TechEvalUtility.GetQuestionnaireObject(row);
                        questionnaireList.Add(questionnaire);
                    }
                }

            }
            catch (Exception ex)
            {
                Questionnaire questionnaire = new Questionnaire();
                questionnaire.ErrorMessage = ex.Message;
                questionnaireList.Add(questionnaire);
            }

            return questionnaireList;
        }

        public List<TechEval> GetTechEvalution(int evalID, int reqID, int userID, string sessionID)
        {
            List<TechEval> details = new List<TechEval>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_EVAL_ID", evalID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("te_GetTechEvaluation", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        TechEval detail = new TechEval();
                        detail = TechEvalUtility.GetTechEvalReport(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                TechEval error = new TechEval();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details;
        }

        public List<VendorDetails> GetVendorsForTechEval(int evalID, string sessionID)
        {
            List<VendorDetails> users = new List<VendorDetails>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_EVAL_ID", evalID);
                DataSet ds = sqlHelper.SelectList("te_GetVendorsForTechEval", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        VendorDetails user = new VendorDetails();
                        user = TechEvalUtility.GetUserDetails(row);
                        users.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
                VendorDetails user = new VendorDetails();
                user.ErrorMessage = ex.Message;
                users.Add(user);
            }

            return users;
        }

        public Questionnaire GetTechEvalForRequirement(int reqID, int GetQuestions, string sessionID)
        {
            Questionnaire questionnaire = new Questionnaire();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_GET_QUESTIONS", GetQuestions);
                DataSet ds = sqlHelper.SelectList("te_GetTechEvalForRequirement", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    questionnaire = TechEvalUtility.GetQuestionnaireObject(row);
                }

                List<Question> questions = new List<Question>();
                if (GetQuestions == 1 && ds.Tables.Count > 1)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        Question question = new Question();
                        question = TechEvalUtility.GetQuestion(row);
                        questions.Add(question);
                    }
                }

                questionnaire.Questions = questions.ToArray();
                questionnaire.Vendors = GetVendorsForTechEval(questionnaire.EvalID, sessionID);
            }
            catch (Exception ex)
            {
                questionnaire.ErrorMessage = ex.Message;
            }

            return questionnaire;
        }

        #endregion

        #region Post
        public Response SaveResponse(List<Answer> answers)
        {
            Response response = new Response();
            try
            {
                //Utilities.ValidateSession(answers[0].SessionID, null);                
                foreach (Answer answer in answers)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    string fileName = string.Empty;
                    if (answer.Attachment != null)
                    {

                        long tick = DateTime.Now.Ticks;
                        fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "_answer_" + "_" + tick + "_" + answer.AttachmentName);
                        SaveFile(fileName, answer.Attachment);
                        fileName = "_answer_" + "_" + tick + "_" + answer.AttachmentName;
                        Response res = SaveAttachment(fileName);
                        if (res.ErrorMessage != string.Empty)
                        {
                            response.ErrorMessage = res.ErrorMessage;
                        }

                        fileName = res.ObjectID.ToString();
                        answer.AttachmentID = Convert.ToInt32(fileName);
                    }

                    DataSet ds = TechEvalUtility.SaveAnswerEntity(answer);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveQuestion(Question question)
        {
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (question.Attachment != null)
                {
                    long tick = DateTime.Now.Ticks;
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "_question_" + "_" + tick + "_" + question.AttachmentName);
                    SaveFile(fileName, question.Attachment);
                    fileName = "_question_" + "_" + tick + "_" + question.AttachmentName;
                    Response res = SaveAttachment(fileName);
                    if (res.ErrorMessage != string.Empty)
                    {
                        response.ErrorMessage = res.ErrorMessage;
                    }

                    fileName = res.ObjectID.ToString();
                    question.AttachmentID = Convert.ToInt32(fileName);
                }

                DataSet ds = TechEvalUtility.SaveQuestionEntity(question);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveQuestionnaire(Questionnaire questionnaire)
        {
            Response response = new Response();
            PRMNotifications notifications = new PRMNotifications();
            int evalId = 0;
            try
            {
                Utilities.ValidateSession(questionnaire.SessionID);
                if (questionnaire.EvalID > 0)
                {
                    Questionnaire old = GetQuestionnaire(questionnaire.EvalID, 1, questionnaire.SessionID);
                    int[] vendorsToRemove = GetVendorsToRemove(questionnaire.Vendors, old.Vendors);
                    foreach (int id in vendorsToRemove)
                    {
                        Response res = RemoveVendorFromAuction(id, questionnaire.EvalID, questionnaire.SessionID, "VendoremailForQuestionnaireRemoval");
                    }
                }

                DataSet ds = TechEvalUtility.SaveQuestionnaireEntity(questionnaire);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    evalId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }

                foreach (Question question in questionnaire.Questions)
                {
                    question.EvalID = evalId;
                    TechEvalUtility.SaveQuestionEntity(question);
                }

                SaveVendorForQuestionnaire(evalId, questionnaire.Vendors, questionnaire.SessionID);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response AssignQuestionnaire(Questionnaire questionnaire)
        {
            Response response = new Response();
            int evalId = 0;
            try
            {
                Utilities.ValidateSession(questionnaire.SessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                DataSet ds = TechEvalUtility.AssignQuestionnaireEntity(questionnaire);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    evalId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ObjectID = evalId;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        public Response SendQuestionnaireToVendors(int evalID, string sessionID)
        {
            PRMNotifications notifications = new PRMNotifications();
            Response response = new Response();
            Questionnaire questionnaire = GetQuestionnaire(evalID, 1, sessionID);
            try
            {
                Utilities.ValidateSession(sessionID);
                questionnaire.IsSentToVendor = 1;
                TechEvalUtility.SaveQuestionnaireEntity(questionnaire);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private int[] GetVendorsToRemove(List<VendorDetails> newList, List<VendorDetails> oldList)
        {
            List<int> oldVendorList = oldList.Select(x => x.VendorID).ToList();
            List<int> newVendorList = newList.Select(x => x.VendorID).ToList();
            int[] removedVendors = oldVendorList.Except(newVendorList).ToList().ToArray();
            return removedVendors;
        }

        private Response RemoveVendorFromAuction(int id, int evalid, string sessionID, string templateName)
        {
            PRMNotifications notifications = new PRMNotifications();
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", id);
                sd.Add("P_EVAL_ID", evalid);
                DataSet ds = sqlHelper.SelectList("te_RemoveVendorFromQuestionnaire", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveVendorForQuestionnaire(int evalID, List<VendorDetails> vendors, string sessionID)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(sessionID);
                DataSet ds = new DataSet();
                Questionnaire questionnaire = GetQuestionnaire(evalID, 1, sessionID);
                foreach (VendorDetails vendor in vendors)
                {
                    ds = TechEvalUtility.SaveVendorForQuestionnaireEntity(questionnaire, vendor);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveTechEvaluation(TechEval techeval)
        {
            Response response = new Response();
            try
            {
                DataSet ds = TechEvalUtility.SaveTechEvalEntity(techeval);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response DeleteQuestion(Question question)
        {
            Response response = new Response();
            try
            {                
                string query = string.Format("DELETE FROM techeval_requirementquestions WHERE Q_ID = {0};", question.QuestionID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response ImportEntity(ImportEntity entity)
        {
            string moreInfo = string.Empty;
            DataTable currentData = new DataTable();
            Response response = new Response();
            string sheetName = string.Empty;
            try
            {
                if (entity.Attachment != null)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(entity.Attachment, 0, entity.Attachment.Length);
                        using (ExcelPackage package = new ExcelPackage(ms))
                        {
                            currentData = StoreUtility.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                            sheetName = package.Workbook.Worksheets[1].Name;
                        }
                    }
                }

                #region Questions Entity

                if (string.Compare(entity.EntityName, "Questions", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
                {
                    if (!sheetName.Equals("Questions", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return response;
                    }

                    Utilities.ValidateSession(entity.SessionID);

                    int count = 0;
                    foreach (DataRow row in currentData.Rows)
                    {
                        Question question = TechEvalUtility.GetQuestion(row);
                        DataSet ds = TechEvalUtility.SaveQuestionEntity(question);

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            int resp = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            if (resp > 0)
                            {
                                count++;
                            }
                        }
                    }

                    response.ObjectID = count;
                }
                #endregion

                #region Answer Entity

                if (string.Compare(entity.EntityName, "Answers", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
                {
                    if (!sheetName.Equals("Answers", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return response;
                    }

                    Utilities.ValidateSession(entity.SessionID);
                    string message = string.Empty;
                    int count = 0;
                    foreach (DataRow row in currentData.Rows)
                    {
                        try
                        {
                            Answer answer = TechEvalUtility.GetAnswer(row);
                            DataSet ds = TechEvalUtility.SaveAnswerEntity(answer);
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                int resp = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                                if (resp > 0)
                                {
                                    count++;
                                }
                            }
                        }
                        catch
                        {
                            message = message + row["ITEM_CODE"].ToString() + ",";
                            continue;
                        }
                    }

                    if (!string.IsNullOrEmpty(message))
                    {
                        response.Message = "Failed to save items:" + message;
                    }

                    response.ObjectID = count;
                }
                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message + moreInfo;
            }

            return response;
        }

        public Response DeleteQuestionnaire(int evalID, string sessionID)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(sessionID);
                string query = string.Format("UPDATE techeval_requirementquestionnaire SET IS_VALID = 0 WHERE EVAL_ID = {0};", evalID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        #endregion

        #region Private
        

        private void SaveFile(string fileName, byte[] fileContent)
        {

            var allowedExtns = ConfigurationManager.AppSettings["SUPPORTED.FILE.EXT"].ToString().Split(',').ToList();
            var isValid = allowedExtns.Any(e => fileName.ToLower().Contains(e));

            if (isValid)
            {
                Utilities.SaveFile(fileName, fileContent);
            }
            else
            {
                //logger.Warn("Unsupported file uploaded: " + fileName);
            }
        }

        private Response SaveAttachment(string path)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion
    }
}