﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;
using System.Text.RegularExpressions;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMPOService : IPRMPOService
    {
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #region DESC

        public POVendor GetDesPoInfo(int reqID, int userID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            POVendor PoObject = new POVendor();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("po_GetDesPoInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    PoObject = POUtility.GetDescPoObject(row);
                }
            }
            catch (Exception ex)
            {
                PoObject.ErrorMessage = ex.Message;
            }

            return PoObject;
        }

        public List<UserDetails> GetVendors(int reqID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<UserDetails> listUser = new List<UserDetails>();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("po_getVendors", sd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        UserDetails user = new UserDetails();
                        user.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : 0;
                        user.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        user.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        user.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        user.Price = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
                        listUser.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
                UserDetails user = new UserDetails();
                user.ErrorMessage = ex.Message;
                listUser.Add(user);
            }

            return listUser;
        }

        public DispatchTrack GetDescDispatch(int poID, int dtID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            DispatchTrack dispatchtrack = new DispatchTrack();
            try
            {
                sd.Add("P_PO_ID", poID);
                sd.Add("P_DT_ID", dtID);
                DataSet ds = sqlHelper.SelectList("po_GetDesDispatchInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    dispatchtrack = POUtility.GetDescDispatchObject(row);
                }
            }
            catch (Exception ex)
            {
                dispatchtrack.ErrorMessage = ex.Message;
            }

            return dispatchtrack;
        }

        public Response SaveDescPoInfo(POVendor povendor)
        {
            Utilities.ValidateSession(povendor.SessionID);
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (povendor.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + povendor.POFile.FileName);
                    SaveFile(fileName, povendor.POFile.FileStream);
                    fileName = "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + povendor.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    povendor.POLink = fileName;
                }
                else
                {
                    PRMServices prm = new PRMServices();
                    Requirement req = prm.GetRequirementData(povendor.ReqID, povendor.CreatedBy, povendor.SessionID);
                    Requirement reqVendor = prm.GetRequirementData(povendor.ReqID, povendor.VendorID, povendor.SessionID);
                    UserDetails Vendor = prm.GetUserDetails(povendor.VendorID, povendor.SessionID);
                    UserDetails Customer = prm.GetUserDetails(povendor.CreatedBy, povendor.SessionID);
                    int margin = 12;
                    long tick = DateTime.Now.Ticks;
                    PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateDesPO(povendor, req, reqVendor, Vendor, Customer), PdfSharp.PageSize.A4, margin);
                    pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf"));
                    fileName = "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    povendor.POLink = fileName;
                }

                DataSet ds = POUtility.SaveDescPoInfoEntity(povendor);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdatePOStatus(int poID, string status, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            Response response = new Response();
            try
            {
                string query = string.Format("UPDATE poinformation SET PO_STATUS = '{0}' WHERE PO_ID = {1};", status, poID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                response.ObjectID = 0;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        
        public Response SaveDesDispatchTrack(DispatchTrack dispatchtrack, POVendor povendor)
        {
            Utilities.ValidateSession(dispatchtrack.SessionID);
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (dispatchtrack.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + dispatchtrack.POFile.FileName);
                    SaveFile(fileName, dispatchtrack.POFile.FileStream);
                    fileName = "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + dispatchtrack.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    dispatchtrack.DispatchLink = fileName;
                }
                else if (1 == 0)
                {
                    PRMServices prm = new PRMServices();
                    Requirement req = prm.GetRequirementData(povendor.ReqID, povendor.CreatedBy, povendor.SessionID);
                    Requirement reqVendor = prm.GetRequirementData(povendor.ReqID, povendor.VendorID, povendor.SessionID);
                    UserDetails Vendor = prm.GetUserDetails(povendor.VendorID, povendor.SessionID);
                    UserDetails Customer = prm.GetUserDetails(povendor.CreatedBy, povendor.SessionID);
                    int margin = 12;
                    long tick = DateTime.Now.Ticks;
                    PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateDesPO(povendor, req, reqVendor, Vendor, Customer), PdfSharp.PageSize.A4, margin);
                    pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf"));
                    fileName = "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    povendor.POLink = fileName;
                }

                DataSet ds = POUtility.SaveDesDispatchTrackObject(dispatchtrack, povendor);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<PaymentInfo> GetPaymentInvoiceDetails(string sessionid, int compId)
        {
            Utilities.ValidateSession(sessionid);
            List<PaymentInfo> details = new List<PaymentInfo>();
            try
            {
                string query = $"select PAYMENT_CODE,PAYMENT_DATE,INVOICE_NUMBER,PAYMENT_AMOUNT,PAYMENT_TRANSACT_ID from paymentdetails where getcompanyid(CREATED_BY) = {compId} ORDER BY PD_ID DESC";
                DataSet ds = sqlHelper.ExecuteQuery(query);
                CORE.DataNamesMapper<PaymentInfo> mapper = new CORE.DataNamesMapper<PaymentInfo>();
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        PaymentInfo pi = new PaymentInfo();
                        pi.PaymentCode = row["PAYMENT_CODE"] != DBNull.Value ? Convert.ToString(row["PAYMENT_CODE"]) : string.Empty;
                        pi.PaymentDate = row["PAYMENT_DATE"] != DBNull.Value ? Convert.ToDateTime(row["PAYMENT_DATE"]) : DateTime.MaxValue;
                        pi.InvoiceNumber = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                        pi.PaymentAmount = row["PAYMENT_AMOUNT"] != DBNull.Value ? Convert.ToDecimal(row["PAYMENT_AMOUNT"]) : 0;
                        pi.TransactionID = row["PAYMENT_TRANSACT_ID"] != DBNull.Value ? Convert.ToString(row["PAYMENT_TRANSACT_ID"]) : string.Empty;
                        details.Add(pi);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Issue in GetPaymentInvoiceDetails" + ex.Message);
            }
            return details;
        }

        #endregion DESC

        #region ITEM

        public VendorPO GetVendorPoList(int reqID, int userID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            VendorPO vendorpo = new VendorPO();
            vendorpo.Vendor = new UserDetails();
            vendorpo.Req = new Requirement();
            vendorpo.ListPOItems = new List<POItems>();

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetPoList", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    vendorpo.Vendor = POUtility.GetVendorPoObject(row);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    vendorpo.Req = POUtility.GetVendorPoReqObject(row1);
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        POItems poitems = new POItems();
                        poitems = POUtility.GetVendorPoItemsObject(row2);
                        vendorpo.ListPOItems.Add(poitems);
                    }
                }
            }
            catch (Exception ex)
            {
                vendorpo.ErrorMessage = ex.Message;
            }

            return vendorpo;
        }

        public VendorPO GetVendorPoInfo(int reqID, int userID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            VendorPO vendorpo = new VendorPO();
            vendorpo.Vendor = new UserDetails();
            vendorpo.Req = new Requirement();
            vendorpo.ListPOItems = new List<POItems>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetVendorPoInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    vendorpo.Vendor = POUtility.GetVendorPoObject(row);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    vendorpo.Req = POUtility.GetVendorPoReqObject(row1);
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        POItems poitems = new POItems();
                        poitems = POUtility.GetVendorPoItemsObject(row2);
                        vendorpo.ListPOItems.Add(poitems);
                    }

                    if (vendorpo.ListPOItems != null && vendorpo.ListPOItems.Count > 0)
                    {
                        vendorpo.PurchaseOrderID = string.Empty;
                        if (vendorpo.ListPOItems.Any(p => !string.IsNullOrEmpty(p.PurchaseID)))
                        {
                            vendorpo.PurchaseOrderID = vendorpo.ListPOItems.First(p => !string.IsNullOrEmpty(p.PurchaseID)).PurchaseID;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                vendorpo.ErrorMessage = ex.Message;
            }

            return vendorpo;
        }

        public List<POSchedule> GetPOScheduler(string ponumber, string sessionID)
        {
            List<POSchedule> details = new List<POSchedule>();
            string query = $"SELECT * FROM PODeliverySchedule WHERE PURCHASE_ORDER_ID = '{ponumber}'";
            DataSet ds = sqlHelper.ExecuteQuery(query);
            CORE.DataNamesMapper<POSchedule> mapper = new CORE.DataNamesMapper<POSchedule>();
            details = mapper.Map(ds.Tables[0]).ToList();

            return details;
        }

        public List<PendingPO> GetAssignedPO(int userId,string sessionID)
        {
            List<PendingPO> details = new List<PendingPO>();
            string query = $@"SELECT PURCHASE_ORDER_ID, 
                (CASE WHEN PO_ID IS NULL OR PO_STATUS = 'PENDING' THEN 'PENDING'  ELSE 'COMPLETE'  END) AS PO_STATUS, 
                (select count(po_id) from poinformation where vendor_id = '{userId}') as TOTAL_COUNT FROM poinformation where vendor_id = '{userId}' 
                GROUP BY PO_ID, PURCHASE_ORDER_ID, PO_STATUS";
            DataSet ds = sqlHelper.ExecuteQuery(query);
            CORE.DataNamesMapper<PendingPO> mapper = new CORE.DataNamesMapper<PendingPO>();
            details = mapper.Map(ds.Tables[0]).ToList();

            return details;
        }

        public List<PendingPO> GetCompanyPendingPOS(int compID, string sessionID, bool IsVendor, int UserId)
        {
            List<PendingPO> details = new List<PendingPO>();
            string query = "";
            if (!IsVendor) {
                //query = $"select PO_ID,REQ_ID,VENDOR_ID,PURCHASE_ORDER_ID,EXPECTED_DELIVERY_DATE,PO_STATUS from poinformation where REQ_ID in (select REQ_ID from requirementdetails where getcompanyid(U_ID) = getcompanyid('{UserId}')) and PO_STATUS = 'PENDING' GROUP BY PURCHASE_ORDER_ID ORDER BY CREATED DESC";
                query = $"select po.PO_ID,po.REQ_ID,VENDOR_ID,po.PURCHASE_ORDER_ID,EXPECTED_DELIVERY_DATE,PO_STATUS,PO_TOTAL_PRICE,PO_IS_SEND_TO_VENDOR, " +
                    $" po.CREATED,VENDOR_DELIVERY_DATE,rd.REQ_CURRENCY,concat(v.U_FNAME, ' ', v.U_LNAME) as SUPPLIER_NAME, " +
                    $" count(distinct po.ITEM_ID) as NO_OF_ITEMS , " +
                    $" 'DELIVERED' as DELIVERY_STATUS, " +
                    $" (select count(DT_ID) - isnull(count(RECEIVED_DATE), 0) from dispatchtrack where PURCHASE_ORDER_ID = po.PURCHASE_ORDER_ID) as AWAITING_RECEIPT, " +
                    $" (select distinct value +',' FROM string_split(STRING_AGG(CAST (ri.CATALOGUE_ITEM_ID AS VARCHAR(MAX)),','),',') FOR XML PATH('') ) as PRODUCT_ID, " +
                    $" (select distinct value +',' FROM string_split(STRING_AGG(CAST (ri.PROD_ID AS VARCHAR(MAX)),','),',') FOR XML PATH('') ) as ITEMS, " +
                    $" (select distinct value +',' FROM string_split(STRING_AGG(CAST (cat.categoryName AS VARCHAR(MAX)),','),',') FOR XML PATH('') ) as CATEGORIES, " +
                    $" (select distinct value +',' FROM string_split(STRING_AGG(CAST (cd.DEPT_CODE AS VARCHAR(MAX)),','),',') FOR XML PATH('') ) as DEPARTMENTS, " +
                    $" (select distinct value +',' FROM string_split(STRING_AGG(CAST (ps.DELIVERY_DATE AS VARCHAR(MAX)),','),',') FOR XML PATH('') ) as DELIVERY_DATE " +
                    $" from poinformation po inner join requirementdetails rd on po.REQ_ID = rd.REQ_ID inner join vendors v on po.VENDOR_ID = v.V_ID" +
                    $" left join dispatchtrack dt on dt.PO_ID = po.PO_ID left join requirementitems ri on ri.REQ_ID = po.REQ_ID " +
                    $" left join cm_productcategory pc on pc.ProductId = ri.CATALOGUE_ITEM_ID left join cm_category cat on cat.CategoryId = pc.CategoryId " +
                    $" left join requirementdepartments rdept on rdept.REQ_ID = PO.REQ_ID left join companydepartments cd on cd.DEPT_ID = rdept.DEPT_ID " +
                    $" LEFT join podeliveryschedule ps on ps.PURCHASE_ORDER_ID = PO.PURCHASE_ORDER_ID where po.REQ_ID  in (select REQ_ID from requirementdetails where getcompanyid(U_ID) = getcompanyid('{UserId}')) " +
                    $" GROUP BY po.PURCHASE_ORDER_ID,po.PO_ID,po.REQ_ID,VENDOR_ID,po.PURCHASE_ORDER_ID,EXPECTED_DELIVERY_DATE,PO_STATUS,PO_TOTAL_PRICE,PO_IS_SEND_TO_VENDOR," +
                    $" po.CREATED,VENDOR_DELIVERY_DATE,rd.REQ_CURRENCY,v.U_FNAME,v.U_LNAME ORDER BY CREATED DESC; ";
            } else {
               // query = $"select PO_ID,REQ_ID,VENDOR_ID,PURCHASE_ORDER_ID,EXPECTED_DELIVERY_DATE,PO_STATUS from poinformation where VENDOR_ID = '{UserId}' and PO_STATUS = 'PENDING' GROUP BY PURCHASE_ORDER_ID ORDER BY CREATED DESC";
                query = $"select max(PO_ID) as PO_ID,max(REQ_ID) AS REQ_ID,max(VENDOR_ID) AS VENDOR_ID,max(PURCHASE_ORDER_ID) AS PURCHASE_ORDER_ID," +
                    $" max(EXPECTED_DELIVERY_DATE) AS EXPECTED_DELIVERY_DATE, max(PO_STATUS) AS PO_STATUS, max(PO_TOTAL_PRICE) AS PO_TOTAL_PRICE,(select concat(U_FNAME, ' ', U_LNAME) " +
                    $"from[user] where u_id = vendor_id)as SUPPLIER_NAME,CREATED,(select count(1) from requirementitems where req_id = po.REQ_ID  and (dbo.fn_Is_CoreCategoryProduct(CATALOGUE_ITEM_ID,'') = 1)) as NO_OF_ITEMS,ROW_NUMBER() OVER(ORDER BY CREATED desc) " +
                    $"from poinformation po where VENDOR_ID = '{UserId}' AND PO_STATUS = 'PENDING' group by PURCHASE_ORDER_ID,VENDOR_ID,CREATED,req_id";

            }
            DataSet ds = sqlHelper.ExecuteQuery(query);
            CORE.DataNamesMapper<PendingPO> mapper = new CORE.DataNamesMapper<PendingPO>();
            details = mapper.Map(ds.Tables[0]).ToList();

            return details;
        }



        public List<POSchedule> GetDeliverySchedules(string poNumbers, string sessionID)
        {
            List<POSchedule> details = new List<POSchedule>();
            string query = $"SELECT PURCHASE_ORDER_ID,DELIVERY_DATE,QUANITTY, " +
                $" (SELECT count(ITEM_ID) FROM PODeliverySchedule WHERE PURCHASE_ORDER_ID IN(SELECT value FROM STRING_SPLIT('{poNumbers}', ',')) and DELIVERY_DATE > GETUTCDATE()) as TOTAL_COUNT " +
                $" FROM PODeliverySchedule WHERE PURCHASE_ORDER_ID IN(SELECT value FROM STRING_SPLIT('{poNumbers}', ',')) and DELIVERY_DATE > GETUTCDATE(); ";
            DataSet ds = sqlHelper.ExecuteQuery(query);
            CORE.DataNamesMapper<POSchedule> mapper = new CORE.DataNamesMapper<POSchedule>();
            details = mapper.Map(ds.Tables[0]).ToList();

            return details;
        }

        public List<PaymentInfo> GetPendingPayments(int compid, string sessionid)
        {
            List<PaymentInfo> details = new List<PaymentInfo>();
            string query = $"select PD.PAYMENT_STATUS,PD.PAYMENT_DATE,PD.MODIFIED,PD.PAYMENT_CODE,PD.DISPATCH_CODE,PD.PURCHASE_ORDER_ID,PD.PAYMENT_AMOUNT,dt.INVOICE_AMOUNT,"+
                $"ri.PROD_ID,cat.categoryName,dt.INVOICE_NUMBER,dt.RECEIVED_DATE,(select count(*) From paymentdetails) as TotalPayments,group_concat(distinct(ri.PROD_ID)) as ITEMS,group_concat(distinct(cat.categoryName)) as CATEGORIES, " +
                $"group_concat(distinct(cd.DEPT_CODE)) as DEPARTMENTS,pr.pr_number from paymentdetails PD" +
                $" left join dispatchtrack dt on dt.PO_ID = PD.PO_ID left join poinformation po on po.PO_ID = PD.PO_ID" +
                $" left join requirementitems ri on ri.REQ_ID = po.REQ_ID left join pr_items prit on prit.req_id = ri.REQ_ID" +
                $" left join pr_details pr on pr.pr_id = prit.pr_id left join cm_productcategory pc on pc.ProductId = ri.CATALOGUE_ITEM_ID" +
                $" left join cm_category cat on cat.CategoryId = pc.CategoryId" +
                $" left join requirementdepartments rd on rd.REQ_ID = PO.REQ_ID left join companydepartments cd on cd.DEPT_ID = rd.DEPT_ID" +
                $" where pd.po_id > 0 and cd.comp_id = {compid} group by PD.PAYMENT_CODE" +
                $" ORDER BY 1 DESC";
            DataSet ds = sqlHelper.ExecuteQuery(query);

            try
            {
                if (ds!=null & ds.Tables.Count > 0)
                {
                    foreach(var row in ds.Tables[0].AsEnumerable())
                    {
                        PaymentInfo detail = new PaymentInfo();
                        detail.PaymentDate = row["PAYMENT_DATE"] != DBNull.Value ? Convert.ToDateTime(row["PAYMENT_DATE"]) : DateTime.MaxValue;
                        detail.InvoiceDate = row["MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["MODIFIED"]) : DateTime.MaxValue;
                        detail.PaymentCode = row["PAYMENT_CODE"] != DBNull.Value ? Convert.ToString(row["PAYMENT_CODE"]) : string.Empty;

                        detail.DispatchCode = row["DISPATCH_CODE"] != DBNull.Value ? Convert.ToString(row["DISPATCH_CODE"]) : string.Empty;
                        detail.PrNumber = row["pr_number"] != DBNull.Value ? Convert.ToString(row["pr_number"]) : string.Empty;
                        detail.POOrderId = row["PURCHASE_ORDER_ID"] != DBNull.Value ? Convert.ToString(row["PURCHASE_ORDER_ID"]) : string.Empty;
                        detail.PaymentAmount = row["PAYMENT_AMOUNT"] != DBNull.Value ? Convert.ToDecimal(row["PAYMENT_AMOUNT"]) : 0;
                        detail.InvoiceAmount = row["INVOICE_AMOUNT"] != DBNull.Value ? Convert.ToString(row["INVOICE_AMOUNT"]) : string.Empty;
                        detail.TotalPayments = row["TotalPayments"] != DBNull.Value ? Convert.ToInt32(row["TotalPayments"]) : 0;

                        detail.PaymentDateTemp = row["PAYMENT_DATE"] != DBNull.Value ? Convert.ToDateTime(row["PAYMENT_DATE"]) : DateTime.MaxValue;
                        detail.InvoiceDateTemp = row["MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["MODIFIED"]) : DateTime.MaxValue;
                        detail.ItemName = row["ITEMS"] != DBNull.Value ? Convert.ToString(row["ITEMS"]) : string.Empty;
                        detail.Department = row["DEPARTMENTS"] != DBNull.Value ? Convert.ToString(row["DEPARTMENTS"]) :string.Empty;
                        detail.Category = row["CATEGORIES"] != DBNull.Value ? Convert.ToString(row["CATEGORIES"]) : string.Empty;
                        detail.InvoiceNumber = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                        detail.ReceivedDate = row["RECEIVED_DATE"] != DBNull.Value ? Convert.ToDateTime(row["RECEIVED_DATE"]) : DateTime.MaxValue;
                        detail.PaymentStatus = row["PAYMENT_STATUS"] != DBNull.Value ? Convert.ToString(row["PAYMENT_STATUS"]) : string.Empty;
                        //var days = detail.PaymentDateTemp.Day - detail.InvoiceDateTemp.Day;



                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                
            }


            return details;
        }


        public List<Filter> GetFiltersOnLoadData(int compid, string sessionid)
        {
            List<Filter> details = new List<Filter>();
            string query = $"select categoryid as ID,categoryname as `NAME`,'CATEGORY' AS `TYPE` from cm_category where companyid = {compid} " +
                $"and isvalid = 1 union select dept_id AS ID,dept_code AS `NAME`,'DEPARTMENT' AS `TYPE` from companydepartments " +
                $"where comp_id = {compid} and is_valid = 1; ";
            DataSet ds = sqlHelper.ExecuteQuery(query);

            try
            {
                if (ds != null & ds.Tables.Count > 0)
                {
                    foreach (var row in ds.Tables[0].AsEnumerable())
                    {
                        Filter detail = new Filter();
                        detail.ID = row["ID"] != DBNull.Value ? Convert.ToInt32(row["ID"]) : 0;
                        detail.NAME = row["NAME"] != DBNull.Value ? Convert.ToString(row["NAME"]) : string.Empty;
                        detail.TYPE = row["TYPE"] != DBNull.Value ? Convert.ToString(row["TYPE"]) : string.Empty;

                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {

            }


            return details;
        }

        public POInvoice[] GetPOInvoiceDetails(string ponumber, string sessionid)
        {
            List<POInvoice> details = new List<POInvoice>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $"SELECT * FROM POInvoiceDetails WHERE PO_NUMBER = '{ponumber}'";
                DataSet ds = sqlHelper.ExecuteQuery(query);
                CORE.DataNamesMapper<POInvoice> mapper = new CORE.DataNamesMapper<POInvoice>();
                details = mapper.Map(ds.Tables[0]).ToList();

                if (details != null && details.Count > 0)
                {
                    foreach (var detail in details)
                    {
                        if (!string.IsNullOrEmpty(detail.ATTACHMENTS))
                        {
                            ds = sqlHelper.ExecuteQuery($"SELECT * FROM attachmentdetails WHERE ATT_ID IN ({detail.ATTACHMENTS})");
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                detail.AttachmentsArray = new List<FileUpload>();
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    detail.AttachmentsArray.Add(new FileUpload()
                                    {
                                        FileID = Convert.ToInt32(row["ATT_ID"]),
                                        FileName = Convert.ToString(row["ATT_PATH"]),
                                    }); ;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return details.ToArray();
        }

        public List<POInvoice> GetInvoiceList(int COMP_ID, int U_ID, string sessionid)
        {
            List<POInvoice> details = new List<POInvoice>();
            Utilities.ValidateSession(sessionid);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_U_ID", U_ID);
            sd.Add("P_COMP_ID", COMP_ID);
            CORE.DataNamesMapper<POInvoice> mapper = new CORE.DataNamesMapper<POInvoice>();
            var dataset = sqlHelper.SelectList("po_GetInvoiceList", sd);
            details = mapper.Map(dataset.Tables[0]).ToList();

            return details;
        }

        public Response SavePOInvoice(POInvoice details)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(details.SessionID);
                if (details != null && !string.IsNullOrEmpty(details.PO_NUMBER))
                {
                    if (details.AttachmentsArray != null && details.AttachmentsArray.Count > 0)
                    {
                        string fileName = string.Empty;
                        foreach (FileUpload fd in details.AttachmentsArray)
                        {
                            if (fd.FileStream != null && !string.IsNullOrWhiteSpace(fd.FileName))
                            {
                                fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
                                if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                                {
                                    var attachName = string.Empty;
                                    long tick = DateTime.UtcNow.Ticks;
                                    attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "POINVOICE_" + tick + "_VENDOR_" + details.VENDOR_ID + "_" + fd.FileName);
                                    SaveFile(attachName, fd.FileStream);
                                    attachName = "POINVOICE_" + tick + "_VENDOR_" + details.VENDOR_ID + "_" + fd.FileName;

                                    Response res = SaveAttachment(attachName);
                                    if (res.ErrorMessage != "")
                                    {
                                        response.ErrorMessage = res.ErrorMessage;
                                    }

                                    fd.FileID = res.ObjectID;
                                }
                            }

                            fileName += Convert.ToString(fd.FileID) + ",";
                        }

                        fileName = fileName.Substring(0, fileName.Length - 1);
                        details.ATTACHMENTS = fileName;
                    }

                    string query = $@"INSERT INTO [dbo].[POInvoiceDetails] ([V_COMP_ID],[C_COMP_ID],[PO_NUMBER], [VENDOR_CODE], [VENDOR_ID], [INVOICE_NUMBER], [INVOICE_AMOUNT], [ATTACHMENTS], [COMMENTS], [STATUS], 
                                [DATE_CREATED], [DATE_MODIFIED], [CREATED_BY], [MODIFIED_BY])      
                                VALUES ('{details.V_COMP_ID}','{details.C_COMP_ID}','{details.PO_NUMBER}', '{details.VENDOR_CODE}', {details.VENDOR_ID}, '{details.INVOICE_NUMBER}', {details.INVOICE_AMOUNT}, 
                                '{details.ATTACHMENTS}', '{details.COMMENTS}', '{details.STATUS}', GETUTCDATE(), GETUTCDATE(), {details.VENDOR_ID}, {details.VENDOR_ID})";
                    sqlHelper.ExecuteNonQuery_IUD(query);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.ObjectID = -1;
            }

            return response;
        }

        public POInvoice GetInvoiceDetails(int invoiceId, string sessionid)
        {
            POInvoice details = new POInvoice();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $"SELECT *,dbo.GetUserName(VENDOR_ID) AS VENDOR_COMP_NAME,dbo.GetCompanyName(VENDOR_ID) as VENDOR_NAME FROM POInvoiceDetails inv  WHERE INVOICE_ID = {invoiceId};";
                DataSet ds = sqlHelper.ExecuteQuery(query);
                CORE.DataNamesMapper<POInvoice> mapper = new CORE.DataNamesMapper<POInvoice>();
                details = mapper.Map(ds.Tables[0]).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error("exception in GetInvoiceDetails >>> " + ex.Message);
            }

            return details;
        }

        //public Response SavePOInvoice(POInvoice[] poInvDet)
        //{
        //    Response response = new Response();
        //    try
        //    {
        //        Utilities.ValidateSession(poInvDet[0].SessionID);
        //        if (poInvDet != null && poInvDet.Length > 0)
        //        {
        //            foreach (POInvoice details in poInvDet)
        //            {
        //                if (details != null && !string.IsNullOrEmpty(details.PO_NUMBER))
        //                {
        //                    if (details.AttachmentsArray != null && details.AttachmentsArray.Count > 0)
        //                    {
        //                        string fileName = string.Empty;
        //                        foreach (FileUpload fd in details.AttachmentsArray)
        //                        {
        //                            if (fd.FileStream != null && !string.IsNullOrWhiteSpace(fd.FileName))
        //                            {
        //                                fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
        //                                if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
        //                                {
        //                                    var attachName = string.Empty;
        //                                    long tick = DateTime.UtcNow.Ticks;
        //                                    attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "POINVOICE_" + tick + "_VENDOR_" + details.VENDOR_ID + "_" + fd.FileName);
        //                                    SaveFile(attachName, fd.FileStream);
        //                                    attachName = "POINVOICE_" + tick + "_VENDOR_" + details.VENDOR_ID + "_" + fd.FileName;

        //                                    Response res = SaveAttachment(attachName);
        //                                    if (res.ErrorMessage != "")
        //                                    {
        //                                        response.ErrorMessage = res.ErrorMessage;
        //                                    }

        //                                    fd.FileID = res.ObjectID;
        //                                }
        //                            }

        //                            fileName += Convert.ToString(fd.FileID) + ",";
        //                        }

        //                        fileName = fileName.Substring(0, fileName.Length - 1);
        //                        details.ATTACHMENTS = fileName;
        //                    }
        //                    int workFlowId = 0, workflowCreatedBy = 0;
        //                    if (!string.IsNullOrEmpty(details.PO_NUMBER))
        //                    {
        //                        var DataSetQueryResult = getLocationWorkflowID();
        //                        if (DataSetQueryResult != null && DataSetQueryResult.Tables.Count > 0 && DataSetQueryResult.Tables[0].Rows.Count > 0)
        //                        {
        //                            workFlowId = DataSetQueryResult.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(DataSetQueryResult.Tables[0].Rows[0][0]) : 0;
        //                            workflowCreatedBy = DataSetQueryResult.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToInt32(DataSetQueryResult.Tables[0].Rows[0][1]) : 0;
        //                        }
        //                    }
        //                    int invoiceId = 0;
        //                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //                    sd.Add("P_V_COMP_ID", details.V_COMP_ID);
        //                    sd.Add("P_C_COMP_ID", details.C_COMP_ID);
        //                    sd.Add("P_PO_NUMBER", details.PO_NUMBER);
        //                    sd.Add("P_PO_LINE_ITEM", details.PO_LINE_ITEM);
        //                    sd.Add("P_INVOICE_TYPE", details.INVOICE_TYPE);
        //                    sd.Add("P_INVOICE_ID", details.INVOICE_ID);
        //                    sd.Add("P_INVOICE_NUMBER", details.INVOICE_NUMBER);
        //                    sd.Add("P_INVOICE_DATE", details.INVOICE_DATE);
        //                    sd.Add("P_ATTACHMENTS", details.ATTACHMENTS);
        //                    sd.Add("P_ASN_NUMBER", details.ASN_NUMBER);
        //                    sd.Add("P_DUE_DATE", details.DUE_DATE);
        //                    sd.Add("P_TO_ADDRESS", details.TO_ADDRESS);
        //                    sd.Add("P_INVOICE_QTY", details.INVOICE_QTY);
        //                    sd.Add("P_INVOICE_AMOUNT", details.INVOICE_AMOUNT);
        //                    sd.Add("P_CREATED_BY", details.VENDOR_ID);
        //                    sd.Add("P_MODIFIED_BY", details.VENDOR_ID);
        //                    sd.Add("P_LOCATION", details.LOCATION);
        //                    sd.Add("P_WF_ID", workFlowId);
        //                    //sd.Add("P_COMP_NAME", ConfigurationManager.AppSettings["COMPANY_NAME"].ToString());
        //                    //sd.Add("P_INV_TYPE_ID", details.INV_TYPE_ID);
        //                    //sd.Add("P_CREDIT_NOTE_ATTACHMENTS", details.ATTACHMENTS_1);
        //                    CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
        //                    var DataSetInvoiceId = sqlHelper.SelectList("po_SaveInvoiceDetails", sd);
        //                    if (DataSetInvoiceId != null && DataSetInvoiceId.Tables.Count > 0 && DataSetInvoiceId.Tables[0].Rows.Count > 0 && DataSetInvoiceId.Tables[0].Rows[0][0] != null)
        //                    {
        //                        invoiceId = DataSetInvoiceId.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(DataSetInvoiceId.Tables[0].Rows[0][0]) : 0;
        //                    }

        //                    if (workFlowId > 0 && invoiceId > 0)
        //                    {
        //                        if (invoiceId > 0 && workFlowId > 0)
        //                        {
        //                            PRMWFService pRMWF = new PRMWFService();
        //                            Response res2 = pRMWF.AssignWorkflow(workFlowId, invoiceId, workflowCreatedBy, details.SessionID);
        //                        }
        //                    }

        //                }
        //            }


        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        response.ErrorMessage = ex.Message;
        //        response.ObjectID = -1;
        //    }

        //    return response;
        //}

        private DataSet getLocationWorkflowID()
        {
            DataSet ds = new DataSet();
            string worfklowQuery = $@"select top 1 wf_id,CREATED_BY from workflows where WF_MODULE = 'VENDOR_INVOICE' ORDER BY MODIFIED DESC";
            ds = sqlHelper.ExecuteQuery(worfklowQuery);
            return ds;
        }


        public Response DeletePOInvoice(string ponumber, string invoicenumber, string sessionid)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                if (!string.IsNullOrEmpty(ponumber) && !string.IsNullOrEmpty(invoicenumber))
                {
                    string query = $@"DELETE FROM [dbo].[POInvoiceDetails] WHERE PO_NUMBER = '{ponumber}' AND INVOICE_NUMBER = '{invoicenumber}'";
                    sqlHelper.ExecuteNonQuery_IUD(query);
                    response.ObjectID = 1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.ObjectID = -1;
            }

            return response;
        }

        public Response SavePOSchedule(POSchedule details)
        {
            Response response = new Response();
            Utilities.ValidateSession(details.SessionID);
            var deliveryDate = details.DELIVERY_DATE.HasValue ? details.DELIVERY_DATE.Value : DateTime.UtcNow.AddMonths(3);
            string query = $@"INSERT INTO podeliveryschedule (PURCHASE_ORDER_ID, ITEM_ID, QUANITTY, COMMENTS, DELIVERY_DATE, DATE_CREATED, DATE_MODIFIED, CREATED_BY, MODIFIED_BY)
            VALUES ('{details.PURCHASE_ORDER_ID}', {details.ITEM_ID},  {details.QUANITTY}, '{details.COMMENTS}', '{deliveryDate.ToString("yyyy-MM-dd")}', UTC_TIMESTAMP(), UTC_TIMESTAMP(), 0, 0); ";

            sqlHelper.ExecuteNonQuery_IUD(query);

            response.ObjectID = 1;

            return response;
        }

        public Response SaveVendorPOInfo(VendorPO vendorpo)
        {
            Utilities.ValidateSession(vendorpo.SessionID);
            Response response = new Response();
            Requirement req = new Requirement();
            Requirement vendorreq = new Requirement();
            UserDetails customer = new UserDetails();
            UserDetails vendor = new UserDetails();
            PRMPRService prmpr = new PRMPRService();
            try
            {
                PRMServices prm = new PRMServices();
                string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);
                req = prm.GetRequirementData(vendorpo.Req.RequirementID, vendorpo.Req.CustomerID, vendorpo.SessionID);
                vendorreq = prm.GetRequirementData(vendorpo.Req.RequirementID, vendorpo.Vendor.UserID, vendorpo.SessionID);
                vendor = prm.GetUserDetails(vendorpo.Vendor.UserID, vendorpo.SessionID);
                customer = prm.GetUserDetails(vendorpo.Req.CustomerID, vendorpo.SessionID);
                string fileName = string.Empty;


                int poSeries = 0;
                if (vendorpo.ListPOItems[0].POID == 0)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_MODIFIED_BY", vendorpo.Req.CustomerID);
                    DataSet ds1 = sqlHelper.SelectList("po_genSeries", sd);
                    if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0 && ds1.Tables[0].Rows[0][0] != null)
                    {
                        poSeries = ds1.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString()) : -1;
                        // response.Message = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }

                    //vendorpo.PurchaseOrderID = prmpr.generatePRNumber("", "PO", vendorpo.SessionID, vendorpo.ListPOItems[0].COMP_ID, vendorpo.ListPOItems[0].DEPT_ID, vendorpo.ListPOItems[0].PurchaseID);
                    vendorpo.PurchaseOrderID = "PO-" + DateTime.Now.Ticks.ToString();
                }
                else
                {
                    vendorpo.PurchaseOrderID =  vendorpo.ListPOItems[0].PurchaseID;
                }


                if (vendorpo.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + req.RequirementID + "_user" + vendor.UserID + "_" + vendorpo.POFile.FileName);
                    SaveFile(fileName, vendorpo.POFile.FileStream);
                    fileName = "req" + req.RequirementID + "_user" + vendor.UserID + "_" + vendorpo.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    vendorpo.POLink = fileName;
                }
                else
                {
                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateItemizedPO(vendorpo, req, vendorreq, customer, vendor), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf"));
                    //fileName = "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTablePo(vendorpo, req, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTablePoToPdf(poTbl, @folderPath + "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, vendorpo);
                    fileName = "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    vendorpo.POLink = fileName;
                }


                foreach (POItems POItem in vendorpo.ListPOItems)
                {
                    POItem.PurchaseID = vendorpo.PurchaseOrderID;
                    if (!string.IsNullOrEmpty(vendorpo.PurchaseOrderID) && !POItem.IsSelected)
                    {
                        string query = $"DELETE FROM poinformation WHERE REQ_ID = {vendorpo.Req.RequirementID} AND VENDOR_ID = {vendorpo.Vendor.UserID} AND ITEM_ID = {POItem.ItemID}";
                        sqlHelper.ExecuteNonQuery_IUD(query);
                    }
                    else
                    {
                        if (POItem.IsSelected)
                        {
                            DataSet ds = POUtility.SavePOItemEntity(POItem, vendorpo, req, vendor);
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            }

                            if (response.ObjectID > 0 && vendorpo.Req.IsContract && POItem.IsCoreProductCategory > 0)
                            {
                                var contactStartTime = POItem.ContractStartTime.HasValue ? POItem.ContractStartTime.Value : DateTime.UtcNow;
                                var contactEndTime = POItem.ContractEndTime.HasValue ? POItem.ContractEndTime.Value : DateTime.UtcNow.AddMonths(3);
                                string contractQuery = $@"INSERT INTO productcontractdetails (ProductId, U_ID, Number, Value, Quantity, AvailedQuantity, document, StartTime, EndTime, IsValid, CompanyName) VALUES 
                                ({POItem.CatalogProductId}, {vendorpo.Vendor.UserID}, {POItem.ItemID}, {POItem.VendorTotalPrice}, {POItem.VendorPOQuantity}, 0, '', '{contactStartTime.ToString("yyyy-MM-dd")}', '{contactEndTime.ToString("yyyy-MM-dd")}', 1, '{vendorpo.Vendor.FirstName + ' ' + vendorpo.Vendor.LastName}'); ";
                                sqlHelper.ExecuteNonQuery_IUD(contractQuery);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }




        //public Response SaveMACVendorPOInfo(MACRequirement vendorpo)
        //{
        //    Utilities.ValidateSession(vendorpo.SessionID, null);
        //    Response response = new Response();
        //    //Requirement req = new Requirement();
        //    //Requirement vendorreq = new Requirement();
        //    //UserDetails customer = new UserDetails();
        //    //UserDetails vendor = new UserDetails();
        //    try
        //    {
        //        foreach (MACRequirementItems macItem in vendorpo.ReqItems)
        //        {
        //            //DataSet ds = POUtility.SaveMACPOItemEntity(macItem);
        //            //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
        //            //{
        //            //    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
        //            //}
        //            //public static DataSet SaveMACPOItemEntity(MACRequirementItems poitems, int reqID)
        //            //{
        //            //    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //            //    sd.Add("P_PO_ID", 0);
        //            //    sd.Add("P_REQ_ID", reqID);
        //            //    sd.Add("P_VENDOR_ID", poitems.VendorID);
        //            //    sd.Add("P_ITEM_ID", poitems.ItemID);
        //            //    sd.Add("P_VENDOR_PO_QUANTITY", 0);
        //            //    sd.Add("P_PO_QUANTITY", poitems.ProductQuantity);
        //            //    sd.Add("P_EXPECTED_DELIVERY_DATE", poitems.ExpectedDeliveryDate);
        //            //    sd.Add("P_PO_PRICE", poitems.RevVendorUnitPrice);
        //            //    sd.Add("P_PO_COMMENTS", "");
        //            //    sd.Add("P_PO_STATUS", "");
        //            //    sd.Add("P_MODIFIED_BY", 0);
        //            //    sd.Add("P_CREATED_BY", 0);
        //            //    sd.Add("P_PURCHASE_ORDER_ID", poitems.PurchaseID);
        //            //    sd.Add("P_PO_LINK", vendorpo.POLink);
        //            //    sd.Add("P_DELIVERY_ADDR", poitems.DeliveryAddress);
        //            //    sd.Add("P_INDENT_ID", '');
        //            //    sd.Add("P_PO_TOTAL_PRICE", poitems.PoTotalPrice);
        //            //    DataSet ds = sqlHelper.SelectList("po_SavePoInfo", sd);
        //            //    return ds;
        //            //}
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.ErrorMessage = ex.Message;
        //    }

        //    return response;
        //}


        public List<DispatchTrack> GetDispatchTrackList(string poorderid, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Utilities.ValidateSession(sessionID);
            List<DispatchTrack> listDispatchTrack = new List<DispatchTrack>();
            Requirement req = new Requirement();
            UserDetails vendor = new UserDetails();
            try
            {
                sd.Add("P_PURCHASE_ORDER_ID", poorderid);
                DataSet ds = sqlHelper.SelectList("po_GetDispatchTrackList", sd);
                List<POItems> listDispatcPOItems = new List<POItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    req = POUtility.GetRequirementDetails(row1);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    DataRow row2 = ds.Tables[2].Rows[0];
                    vendor = POUtility.GetVendorDetails(row2);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DispatchTrack dispatchtrack = new DispatchTrack();
                        dispatchtrack = POUtility.GetDispatchTrackObject(row);
                        dispatchtrack.VendorPOObject = new VendorPO();
                        dispatchtrack.VendorPOObject.Req = new Requirement();
                        dispatchtrack.VendorPOObject.Req = req;
                        dispatchtrack.VendorPOObject.Vendor = vendor;
                        listDispatchTrack.Add(dispatchtrack);
                    }
                }
            }
            catch (Exception ex)
            {
                DispatchTrack dispatchtrack = new DispatchTrack();
                dispatchtrack.ErrorMessage = ex.Message;
                listDispatchTrack.Add(dispatchtrack);
            }

            return listDispatchTrack;
        }

        public List<DispatchTrack> GetDispatchTrack(string poorderid, string dcode, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DispatchTrack> listDispatchTrack = new List<DispatchTrack>();
            try
            {
                sd.Add("P_PURCHASE_ORDER_ID", poorderid);
                sd.Add("P_DISPATCH_CODE", dcode);
                DataSet ds = sqlHelper.SelectList("po_GetDispatchTrack", sd);
                List<POItems> listDispatcPOItems = new List<POItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        POItems dispatchPoItem = new POItems();
                        dispatchPoItem = POUtility.GetDispatchPoItem(row);
                        listDispatcPOItems.Add(dispatchPoItem);
                    }
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DispatchTrack dispatchtrack = new DispatchTrack();
                        dispatchtrack = POUtility.GetDispatchTrackObject(row);
                        dispatchtrack.POItemsEntity = listDispatcPOItems.Where(p => p.PurchaseID == dispatchtrack.PurchaseID).ToList();
                        listDispatchTrack.Add(dispatchtrack);
                    }
                }
            }
            catch (Exception ex)
            {
                DispatchTrack dispatchtrack = new DispatchTrack();
                dispatchtrack.ErrorMessage = ex.Message;
                listDispatchTrack.Add(dispatchtrack);
            }

            return listDispatchTrack;
        }

        public Response SaveDispatchTrack(DispatchTrack dispatchtrack, string requestType)
        {
            Utilities.ValidateSession(dispatchtrack.SessionID);
            PRMNotifications notifications = new PRMNotifications();
            Response response = new Response();
            Requirement newreq = new Requirement();
            Requirement vendorreq = new Requirement();
            UserDetails customer = new UserDetails();
            UserDetails vendor = new UserDetails();
            try
            {
                PRMServices prm = new PRMServices();
                string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);
                string fileName = string.Empty;
                newreq = prm.GetRequirementData(dispatchtrack.POItemsEntity[0].ReqID, 0, dispatchtrack.SessionID);
                vendorreq = prm.GetRequirementData(dispatchtrack.POItemsEntity[0].ReqID, dispatchtrack.POItemsEntity[0].VendorID, dispatchtrack.SessionID);
                customer = prm.GetUserDetails(newreq.CustomerID, dispatchtrack.SessionID);
                vendor = prm.GetUserDetails(dispatchtrack.POItemsEntity[0].VendorID, dispatchtrack.SessionID);
                if (requestType == "DISPATCH")
                {
                    string mrrTable = string.Empty;
                    foreach (POItems poItem in dispatchtrack.POItemsEntity)
                    {
                        string xml = string.Empty;
                        xml = notifications.GenerateEmailBody("MrrDispatchXML");
                        xml = String.Format(xml, poItem.ProductIDorName, poItem.VendorPOQuantity, poItem.SumDispatchQuantity, poItem.SumRecivedQuantity, poItem.DispatchQuantity);
                        mrrTable += xml;

                    }

                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateMRRPO(dispatchtrack, newreq, mrrTable, customer), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PurchaseID_" + dispatchtrack.PurchaseID + "_DispatchCode_" + dispatchtrack.DispatchCode + ".pdf"));


                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTableDispatch(dispatchtrack, newreq, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTableDispatchToPdf(poTbl, @folderPath + "MaterialDispatch" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, dispatchtrack);
                    fileName = "MaterialDispatch" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    dispatchtrack.DispatchLink = fileName;
                }

                if (requestType == "RECEIVE")
                {
                    string mrrTable = string.Empty;
                    foreach (POItems poItem in dispatchtrack.POItemsEntity)
                    {
                        string xml = string.Empty;
                        xml = notifications.GenerateEmailBody("MrrReceiveXML");
                        xml = String.Format(xml, poItem.ProductIDorName, poItem.VendorPOQuantity, poItem.SumDispatchQuantity, poItem.SumRecivedQuantity, poItem.SumReturnQuantity, poItem.DispatchQuantity, poItem.RecivedQuantity, poItem.ReturnQuantity);
                        mrrTable += xml;

                    }

                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTableMRR(dispatchtrack, newreq, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTableMRRToPdf(poTbl, @folderPath + "ASN" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, dispatchtrack);
                    fileName = "ASN" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateMRRPOReportpdf(dispatchtrack, newreq, mrrTable, customer), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PurchaseID_" + dispatchtrack.PurchaseID + "_ReceivedCode_" + dispatchtrack.RecivedCode + ".pdf"));
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    dispatchtrack.RecivedLink = Convert.ToInt32(fileName);
                }

                Response res = SaveAttachment(fileName);
                fileName = res.ObjectID.ToString();
                foreach (POItems poItem in dispatchtrack.POItemsEntity)
                {
                    if(poItem.IsCoreProductCategory > 0)
                    {
                        DataSet ds = POUtility.SaveDispatchTrackObject(dispatchtrack, poItem, requestType);
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public PaymentTrack GetPaymentTrack(int vendorID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID);
            PaymentTrack paymenttrack = new PaymentTrack();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                //sd.Add("P_VENDOR_ID", vendorID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetPaymentTrack", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    UserDetails Vendor = new UserDetails();
                    DataRow row = ds.Tables[0].Rows[0];
                    Vendor = POUtility.GetVendorPoObject(row);
                    paymenttrack.Vendor = Vendor;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    Requirement Req = new Requirement();
                    DataRow row1 = ds.Tables[1].Rows[0];
                    Req = POUtility.GetVendorPoReqObject(row1);
                    paymenttrack.Req = Req;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    POItems PO = new POItems();
                    DataRow row2 = ds.Tables[2].Rows[0];
                    PO = POUtility.GetPoObject(row2);
                    paymenttrack.PO = PO;
                }

                List<DispatchTrack> ListDispatchObject = new List<DispatchTrack>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0 && ds.Tables[3].Rows[0][0] != null)
                {
                    foreach (DataRow row3 in ds.Tables[3].Rows)
                    {
                        DispatchTrack DispatchObject = new DispatchTrack();
                        DispatchObject = POUtility.GetPaymentTrackObject(row3);
                        ListDispatchObject.Add(DispatchObject);
                    }

                    paymenttrack.DispatchObject = ListDispatchObject;
                }

                List<PaymentInfo> ListPaymentInfo = new List<PaymentInfo>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0 && ds.Tables[4].Rows[0][0] != null)
                {
                    foreach (DataRow row4 in ds.Tables[4].Rows)
                    {
                        PaymentInfo paymentinfo = new PaymentInfo();
                        paymentinfo = POUtility.GetPaymentInfoObject(row4);
                        ListPaymentInfo.Add(paymentinfo);
                    }

                    paymenttrack.PaymentInfoObject = ListPaymentInfo;
                }
            }
            catch (Exception ex)
            {
                paymenttrack.ErrorMessage = ex.Message;
            }

            return paymenttrack;
        }

        public Response SavePaymentInfo(PaymentInfo paymentinfo)
        {
            Utilities.ValidateSession(paymentinfo.SessionID);
            Response response = new Response();
            try
            {
                DataSet ds = POUtility.SavePaymentInfoObject(paymentinfo);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public bool CheckUniqueIfExists(string param, string idtype, string sessionID)
        {
            bool response = false;
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PARAM", param);
                sd.Add("P_ID_TYPE", idtype);
                DataSet ds = sqlHelper.SelectList("cp_CheckUniqueIfExists", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    int result = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (result > 0)
                    {
                        response = true;
                    }
                    else
                    {
                        response = false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return response;
        }
        public SAPOEntity[] GetRequirementPO(int compid, int reqid, string sessionid)
        {
            List<SAPOEntity> details = new List<SAPOEntity>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $"SELECT * FROM POGenerateDetails WHERE COMP_ID = {compid} AND REQ_ID = {reqid}";
                CORE.DataNamesMapper<SAPOEntity> mapper = new CORE.DataNamesMapper<SAPOEntity>();
                var dataset = sqlHelper.ExecuteQuery(query);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }

            return details.ToArray();
        }
        public SAPOEntity[] GetPOGenerateDetails(int compid, string template, int vendorid, string status, string creator,
           string plant, string purchasecode, string search, string sessionid, int page = 0, int pagesize = 0)
        {
            List<SAPOEntity> details = new List<SAPOEntity>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_PO_TEMPLATE", template);
                sd.Add("P_VENDOR", vendorid);
                sd.Add("P_STATUS", status);
                sd.Add("P_PLANT", plant);
                sd.Add("P_PURCHASE_GROUP", purchasecode);
                sd.Add("P_CREATOR", creator);
                sd.Add("P_SEARCH", search);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", pagesize);

                CORE.DataNamesMapper<SAPOEntity> mapper = new CORE.DataNamesMapper<SAPOEntity>();
                var dataset = sqlHelper.SelectList("PO_GetPOGenerateList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }

            return details.ToArray();
        }

        public SAPOEntity[] GetPOItems(string ponumber, string quotno, string sessionid)
        {
            List<SAPOEntity> details = new List<SAPOEntity>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_NUMBER", ponumber);
                sd.Add("P_QUOT_NO", quotno);

                CORE.DataNamesMapper<SAPOEntity> mapper = new CORE.DataNamesMapper<SAPOEntity>();
                var dataset = sqlHelper.SelectList("PO_GetPOItems", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }

            return details.ToArray();
        }

        public List<POFieldMapping> GetFilterValues(int compID, string sessionID)
        {
            List<POFieldMapping> details = new List<POFieldMapping>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compID);
                CORE.DataNamesMapper<POFieldMapping> mapper = new CORE.DataNamesMapper<POFieldMapping>();
                var dataset = sqlHelper.SelectList("po_GetPOFilterValues", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }

            return details;
        }

        #endregion ITEM

        #region OPENPO
        public Response GetLastUpdatedDate(string table, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                string query = string.Format("SELECT DATE_MODIFIED FROM {0} ORDER BY DATE_MODIFIED DESC LIMIT 1", table);
                var ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    details.Message = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]).ToString("dd MMM, yyyy") : string.Empty;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving last table updated date.");
            }

            return details;
        }

        public List<Response> GetSapAccess(int userid, string sessionid)
        {
            List<Response> details = new List<Response>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                string query = string.Format("SELECT * FROM SAP_Access WHERE U_ID = {0}", userid);
                var ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string accessString = row["TEAMS"] != DBNull.Value ? Convert.ToString(row["TEAMS"]) : string.Empty;
                        var accessList = accessString.Split(';');
                        foreach (var access in accessList)
                        {
                            Response response = new Response();
                            response.ErrorMessage = row["ACCESS_TYPE"] != DBNull.Value ? Convert.ToString(row["ACCESS_TYPE"]) : string.Empty;
                            if (!string.IsNullOrEmpty(access))
                            {
                                var val = access.Split(':');
                                response.Message = val[0];
                                response.ObjectID = Convert.ToInt16(val[1]);
                                details.Add(response);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving SAP Access");
            }

            return details;
        }

        public Response SavePOComments(string pono, string itemno, double quantity, string comments, DateTime newdeliverdate, string type,
            int userid, string materialdescription, string apiname, string sitename, int compid, int plant, int purchase,
            string sessionid, OpenPO openpo, string category, string status)
        {
            string dateFormat = "dd/MM/yyyy";
            Response response = new Response();
            return response;
        }

        public Response UpdatePoStatus(string pono, string itemno, string sessionid, string status, int userid)
        {
            //     string dateFormat = "dd/MM/yyyy";
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {

                int isValidSession = Utilities.ValidateSession(sessionid);

                sd.Add("P_PO_NO", pono);
                sd.Add("P_ITEM_NO", itemno);
                sd.Add("P_STATUS", status);
                sd.Add("P_USER", userid);
                DataSet ds = sqlHelper.SelectList("sap_UpdatePOStatus", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        // response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        response.PR_NUMBER = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][0].ToString()) : string.Empty;
                        response.PR_ITM_NO = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                        response.ErrorMessage = ds.Tables[0].Rows[0][2] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][2].ToString()) : string.Empty;

                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<OpenPO> GetOpenPO(int compid, string pono, int plant, int purchase, string sessionid, int exclude)
        {
            List<OpenPO> details = new List<OpenPO>();
            List<OpenPO> deliveredData = new List<OpenPO>();
            int count = 0;
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_PO_NO", pono);
                sd.Add("P_EXCLUDE", exclude);
                CORE.DataNamesMapper<OpenPO> mapper = new CORE.DataNamesMapper<OpenPO>();
                var dataset = sqlHelper.SelectList("sap_GetOpenPOData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
                deliveredData = mapper.Map(dataset.Tables[1]).ToList();

                foreach (var detail in details)
                {
                    count++;

                    if (detail.NEW_DELIVERY_DATE_COMM != null)
                    {
                        detail.NEW_DELIVERY_DATE_COMM = getStringDateFromString(detail.NEW_DELIVERY_DATE_COMM, "MM/dd/yyyy");
                    }

                    if (detail.IDEAL_DELIVERY_DATE != null && detail.IDEAL_DELIVERY_DATE.HasValue)
                    {
                        detail.IDEAL_DELIVERY_MONTH = detail.IDEAL_DELIVERY_DATE.Value.ToString("MMMM-yyyy");                        
                    }
                }

                foreach (var detail1 in deliveredData)
                {
                    if (detail1.IDEAL_DELIVERY_DATE != null && detail1.IDEAL_DELIVERY_DATE.HasValue)
                    {
                        detail1.IDEAL_DELIVERY_MONTH = detail1.IDEAL_DELIVERY_DATE.Value.ToString("MMMM-yyyy");
                    }
                    detail1.STATUS = "Delivered";
                }

                if (plant > 0)
                {
                    details = details.FindAll(d => d.PLANT == plant.ToString());
                    deliveredData = deliveredData.FindAll(d => d.PLANT == plant.ToString());
                }

                if (purchase > 0)
                {
                    details = details.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                    deliveredData = deliveredData.FindAll(d => d.PURCHASING_GROUP == purchase.ToString());
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            details = details.OrderBy(d => d.IDEAL_DELIVERY_DATE).ToList();
            deliveredData = deliveredData.OrderBy(d => d.IDEAL_DELIVERY_DATE).ToList();
            details.AddRange(deliveredData);
            return details;
        }

        public List<OpenPO> GetOpenPOReport(int compid, string sessionid)
        {
            List<OpenPO> details = new List<OpenPO>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<OpenPO> mapper = new CORE.DataNamesMapper<OpenPO>();
                string query = @"SELECT C.QUANTITY AS C_QUANTITY,NEW_DELIVERY_DATE, C.COMMENTS, C.CREATED AS COMMENT_CREATED, PO.PLANT, P.PLANT_NAME, PO.PURCHASING_GROUP, PL.PURCHASER, PO.PURCHASING_DOCUMENT, PO.ITEM, 
                concat(PO.PURCHASING_DOCUMENT, PO.ITEM) AS 'CONCAT', DOCUMENT_DATE AS PO_DATE, CONCAT(MONTHNAME(NOW()), '-',YEAR(NOW())) AS PO_MONTH,
                PO.NAME_OF_SUPPLIER, PO.MATERIAL, PO.SHORT_TEXT, API.API_NAME, PO.ORDER_UNIT, PO.ORDER_QUANTITY, PO.STILL_TO_BE_DELIVERED_QTY,
                PR.USER_ID, PR.MULTIPLE_PR_TEXT, PR.PR_NUMBER, PR.PR_ITM_NO, PR.PR_CREATE_DATE, PR.PO_DELV_DATE, 
                IF(IFNULL(PR.VENDORS_COUNTRY, 'India') = 'India', 'India', 'Import') AS IMPORT_LOCAL, MAT.LEAD_TIME, 
                GREATEST(DATE_ADD(IFNULL(PR.PR_CREATE_DATE, DATE('1900-01-01')), INTERVAL SAPLeadTime(PO.MATERIAL) DAY), PR.PR_DELV_DATE) AS IDEAL_DELIVERY_DATE,
                CONCAT(MONTHNAME(GREATEST(DATE_ADD(IFNULL(PR.PR_CREATE_DATE, DATE('1900-01-01')), INTERVAL SAPLeadTime(PO.MATERIAL) DAY), PR.PO_DELV_DATE)), '-', YEAR(GREATEST(DATE_ADD(IFNULL(PR.PR_CREATE_DATE, DATE('1900-01-01')), INTERVAL SAPLeadTime(PO.MATERIAL) DAY), PR.PO_DELV_DATE))) AS DELIVERY_MONTH,
                PR.PR_DELV_DATE
                FROM 
                (SELECT * FROM (SELECT * FROM sap_openpo_comments WHERE TYPE = 'SHORTAGE' 
                ORDER BY PURCHASING_DOCUMENT DESC, ITEM DESC, CREATED DESC) AS C1 
                GROUP BY PURCHASING_DOCUMENT, ITEM) AS C
                INNER JOIN sap_openpo PO ON PO.PURCHASING_DOCUMENT = C.PURCHASING_DOCUMENT AND PO.ITEM = C.ITEM
                LEFT JOIN sapplantlookup p ON PO.PLANT = P.PLANT_CODE
                LEFT JOIN sappurchaserlookup PL ON PO.PURCHASING_GROUP = PL.PURCHASER_GRP
                LEFT JOIN sapapiname API ON API.HANA_CODE = PO.MATERIAL
                LEFT JOIN sapmaterialmaster MAT ON MAT.CODE = PO.MATERIAL
                LEFT JOIN(SELECT* FROM sap_prgrn GROUP BY PO_NO, LN_ITM) PR ON PR.PO_NO = PO.PURCHASING_DOCUMENT AND PR.LN_ITM = PO.ITEM
                WHERE TYPE = 'SHORTAGE'";
                var dataset = sqlHelper.ExecuteQuery(query);
                details = mapper.Map(dataset.Tables[0]).ToList();

                foreach (var detail in details)
                {
                    if (string.IsNullOrEmpty(detail.COMMENTS))
                    {
                        detail.COMMENTS = "";
                    }

                    if (string.IsNullOrEmpty(detail.API_NAME))
                    {
                        detail.API_NAME = "";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        public List<POScheduleDetails> GetPOScheduleList(int compid, string uid, string search, string categoryid, string productid, string supplier, string postatus, string deliverystatus, string plant,
            string fromdate, string todate, int page, int pagesize, string sessionid)
        {
            List<POScheduleDetails> details = new List<POScheduleDetails>();
            try
            {
                search = "%" + search + "%";
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_U_ID", uid);
                sd.Add("P_SEARCH", search);
                sd.Add("P_CATEGORY_ID", categoryid);
                sd.Add("P_PRODUCT_ID", productid);
                sd.Add("P_SUPPLIER", supplier);
                sd.Add("P_PO_STATUS", postatus);
                sd.Add("P_DELIVERY_STATUS", deliverystatus);
                sd.Add("P_PLANT", plant);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", pagesize);
                CORE.DataNamesMapper<POScheduleDetails> mapper = new CORE.DataNamesMapper<POScheduleDetails>();
                var dataset = sqlHelper.SelectList("po_GetPOScheduleList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                if (details != null && details.Count > 0 && dataset.Tables[1].Rows.Count > 0)
                {
                    var row = dataset.Tables[1].Rows[0];
                    details[0].STATS_TOTAL_COUNT = row["TOTAL_COUNT"] != DBNull.Value ? Convert.ToInt32(row["TOTAL_COUNT"]) : 0;
                    details[0].STATS_PO_AWAITING_RECEIPT = row["PO_AWAITING_RECEIPT"] != DBNull.Value ? Convert.ToInt32(row["PO_AWAITING_RECEIPT"]) : 0;
                    details[0].STATS_PO_NOT_INITIATED = row["PO_NOT_INITIATED"] != DBNull.Value ? Convert.ToInt32(row["PO_NOT_INITIATED"]) : 0;
                    details[0].STATS_PO_PARTIAL_DELIVERY = row["PO_PARTIAL_DELIVERY"] != DBNull.Value ? Convert.ToInt32(row["PO_PARTIAL_DELIVERY"]) : 0;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        //public List<POScheduleDetailsItems> GetPOScheduleItems(string ponumber, int moredetails, string sessionid)
        //{
        //    List<POScheduleDetailsItems> details = new List<POScheduleDetailsItems>();
        //    try
        //    {
        //        Utilities.ValidateSession(sessionid);
        //        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //        sd.Add("P_PO_NUMBER", ponumber);
        //        sd.Add("P_DETAILS", moredetails);
        //        CORE.DataNamesMapper<POScheduleDetailsItems> mapper = new CORE.DataNamesMapper<POScheduleDetailsItems>();
        //        var dataset = sqlHelper.SelectList("po_GetPOScheduleItems", sd);
        //        details = mapper.Map(dataset.Tables[0]).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex, "Error in retrieving Data");
        //    }

        //    return details;
        //}

        public List<POScheduleDetailsItems> GetPOScheduleItems(string ponumber, int moredetails, bool forasn, string sessionid)
        {
            List<POScheduleDetailsItems> details = new List<POScheduleDetailsItems>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_NUMBER", ponumber);
                sd.Add("P_DETAILS", moredetails);
                CORE.DataNamesMapper<POScheduleDetailsItems> mapper = new CORE.DataNamesMapper<POScheduleDetailsItems>();
                var dataset = sqlHelper.SelectList("po_GetPOScheduleItems", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                if (dataset.Tables.Count > 1 && dataset.Tables[1].Rows.Count > 0)
                {
                    CORE.DataNamesMapper<GRNItem> grnMapper = new CORE.DataNamesMapper<GRNItem>();
                    List<GRNItem> grnList = grnMapper.Map(dataset.Tables[1]).ToList();
                    if (grnList != null && grnList.Count > 0)
                    {
                        foreach (var po in details)
                        {
                            po.GRNItems = new List<GRNItem>();
                            po.GRNItems = grnList.Where(g => (g.PO_NUMBER == po.PO_NUMBER && g.PO_LINE_ITEM == po.PO_LINE_ITEM)).ToList();
                        }
                    }
                }

                CORE.DataNamesMapper<POInvoice> InvoiceInfoMapper = new CORE.DataNamesMapper<POInvoice>();
                List<POInvoice> invoiceInfo = new List<POInvoice>();
                if (dataset.Tables.Count > 2 && dataset.Tables[2].Rows.Count > 0)
                {
                    invoiceInfo = InvoiceInfoMapper.Map(dataset.Tables[2]).ToList();
                }

                if (invoiceInfo != null && invoiceInfo.Count > 0)
                {
                    foreach (var po in details)
                    {
                        po.PO_INV = new List<POInvoice>();
                        po.PO_INV = invoiceInfo.Where(invoice => (invoice.PO_NUMBER == po.PO_NUMBER && invoice.PO_LINE_ITEM == po.PO_LINE_ITEM)).ToList();
                    }
                }

                if (forasn)
                {
                    details = details.Where(d => !string.IsNullOrEmpty(d.VENDOR_ACK_STATUS) && d.REMAINING_NET_QTY > 0 && (d.VENDOR_ACK_STATUS == "ACKNOWLEDGE" || d.VENDOR_ACK_STATUS == "EDIT")).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<CArrayKeyValue> GetPOScheduleFilters(int compid, string sessionid)
        {
            List<CArrayKeyValue> details = new List<CArrayKeyValue>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                CORE.DataNamesMapper<POScheduleDetailsItems> mapper = new CORE.DataNamesMapper<POScheduleDetailsItems>();
                var dataset = sqlHelper.SelectList("po_GetPOScheduleFilters", sd);

                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "CATEGORY";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach(var row in dataset.Tables[0].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key = row["CATEGORY_ID"] != DBNull.Value ? Convert.ToInt32(row["CATEGORY_ID"]) : 0;
                        keyValuePair.Value = row["CategoryCode"] != DBNull.Value ? Convert.ToString(row["CategoryCode"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 1 && dataset.Tables[1].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PRODUCT";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[1].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key = row["PRODUCT_ID"] != DBNull.Value ? Convert.ToInt32(row["PRODUCT_ID"]) : 0;
                        keyValuePair.Key1 = row["ProductCode"] != DBNull.Value ? Convert.ToString(row["ProductCode"]) : "";
                        keyValuePair.Value = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 2 && dataset.Tables[2].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "VENDORS";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[2].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["VENDOR_COMPANY"] != DBNull.Value ? Convert.ToString(row["VENDOR_COMPANY"]) : "";
                        keyValuePair.Value = row["VENDOR_COMPANY"] != DBNull.Value ? Convert.ToString(row["VENDOR_COMPANY"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 3 && dataset.Tables[3].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PLANT";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[3].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["PLANT"] != DBNull.Value ? Convert.ToString(row["PLANT"]) : "";
                        keyValuePair.Value = row["PLANT_NAME"] != DBNull.Value ? Convert.ToString(row["PLANT_NAME"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 4 && dataset.Tables[4].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PO_STATUS";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[4].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["PO_STATUS"] != DBNull.Value ? Convert.ToString(row["PO_STATUS"]) : "";
                        keyValuePair.Value = row["PO_STATUS"] != DBNull.Value ? Convert.ToString(row["PO_STATUS"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }


            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<GRNDetails> GetGRNDetailsList(int compid, string uid, string search, string supplier, string fromdate, string todate, int page, int pagesize, string sessionid)
        {
            List<GRNDetails> details = new List<GRNDetails>();
            try
            {
                search = "%" + search + "%";
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_U_ID", uid);
                sd.Add("P_SEARCH", search);
                sd.Add("P_SUPPLIER", supplier);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", pagesize);
                CORE.DataNamesMapper<GRNDetails> mapper = new CORE.DataNamesMapper<GRNDetails>();
                var dataset = sqlHelper.SelectList("po_GetGRNDetailsList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response SavePOVendorQuantityAck(string ponumber, string poitemline, string status, string comments, int isVendPoAck, decimal quantity, int user, string sessionid)
        {
            Response detail = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = string.Empty;
                query = $@"select * from POScheduleDetails WHERE PO_NUMBER = '{ponumber}'";
                if (!string.IsNullOrEmpty(poitemline))
                {
                    query += $@" AND PO_LINE_ITEM = '{poitemline}'";
                }

                query = string.Empty;

                if (status == "ACKNOWLEDGE" || status == "EDIT")
                {
                    query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}',  ACK_QTY = ORDER_QTY, ACK_DATE = utc_timestamp,
                            DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user} WHERE PO_NUMBER = '{ponumber}'";
                }
                else
                {
                    query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}',  ACK_QTY = 0, ACK_DATE = utc_timestamp,
                            DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user} WHERE PO_NUMBER = '{ponumber}'";
                }

                if (!string.IsNullOrEmpty(poitemline))
                {
                    if (status == "ACKNOWLEDGE" || status == "EDIT")
                    {
                        query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}',  ACK_QTY = {quantity}, ACK_DATE = utc_timestamp,
                            DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user} WHERE PO_NUMBER = '{ponumber}' AND PO_LINE_ITEM = '{poitemline}'";
                    }
                    else
                    {
                        query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}',  ACK_QTY = 0, ACK_DATE = utc_timestamp,
                            DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user} WHERE PO_NUMBER = '{ponumber}' AND PO_LINE_ITEM = '{poitemline}'";
                    }
                }

                logger.Debug("query >>>>>" + query);
                sqlHelper.ExecuteNonQuery_IUD(query);
                detail.ObjectID = 1;
            }
            catch (Exception ex)
            {
                detail.ErrorMessage = ex.Message;
                logger.Error(ex, ex.Message);
            }

            return detail;
        }


        #region ASN

        public List<ASNDetails> GetASNDetails(int compid, int asnid, string asncode, string ponumber, string grncode, int vendorid, string sessionid)
        {
            List<ASNDetails> details = new List<ASNDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                asncode = asncode == "0" ? string.Empty : asncode;
                ponumber = ponumber == "0" ? string.Empty : ponumber;
                grncode = grncode == "0" ? string.Empty : grncode;
                string query = $"SELECT * FROM ASNDetails WHERE ";
                string query1 = @"SELECT PO_NUMBER, PO_LINE_ITEM,  ORDER_QTY, SUM(NET_WT) AS NET_WT, ((ORDER_QTY) - SUM(NET_WT)) REMAINING_NET_QTY 
                                FROM ASNDetails WHERE ";

                bool conditions = false;
                if (vendorid > 0)
                {
                    query += $" {(conditions ? " AND " : "")} VENDOR_ID = {vendorid}";
                    query1 += $" VENDOR_ID = {vendorid}";
                    conditions = true;
                }
                if (asnid > 0)
                {
                    query += $" {(conditions ? " AND " : "")} ASN_ID = {asnid}";
                    conditions = true;
                }

                if (!string.IsNullOrWhiteSpace(asncode))
                {
                    query += $" {(conditions ? " AND " : "")} ASN_CODE = '{asncode}'";
                    conditions = true;
                }

                if (!string.IsNullOrWhiteSpace(ponumber))
                {
                    query += $" {(conditions ? " AND " : "")} PO_NUMBER = '{ponumber}'";
                    query1 += $" AND PO_NUMBER = '{ponumber}'";
                    conditions = true;
                }

                if (!string.IsNullOrWhiteSpace(grncode))
                {
                    query += $" {(conditions ? " AND " : "")} GRN_CODE = '{grncode}'";
                    conditions = true;
                }

                if (compid > 0)
                {
                    query += $" {(conditions ? " AND " : "")} PO_NUMBER IN (SELECT PO_NUMBER FROM POScheduleDetails WHERE COMP_ID = {compid})";
                    conditions = true;
                }

                query += $" ORDER BY DATE_MODIFIED DESC";
                query1 += $" GROUP BY PO_NUMBER, PO_LINE_ITEM, ORDER_QTY";

                CORE.DataNamesMapper<ASNDetails> mapper = new CORE.DataNamesMapper<ASNDetails>();
                var dataTable = sqlHelper.SelectQuery(query);
                var dataTable1 = sqlHelper.SelectQuery(query1);
                details = mapper.Map(dataTable).ToList();

                if (!string.IsNullOrWhiteSpace(asncode) && details != null && details.Count > 0)
                {
                    foreach (var detail in details)
                    {
                        if (!string.IsNullOrEmpty(detail.ATTACHMENTS))
                        {
                            DataSet ds = sqlHelper.ExecuteQuery($"SELECT * FROM attachmentdetails WHERE ATT_ID IN ({detail.ATTACHMENTS})");
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                detail.AttachmentsArray = new List<FileUpload>();
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    detail.AttachmentsArray.Add(new FileUpload()
                                    {
                                        FileID = Convert.ToInt32(row["ATT_ID"]),
                                        FileName = Convert.ToString(row["ATT_PATH"]),
                                    }); ;
                                }
                            }
                        }
                    }
                }

                if (dataTable1 != null && dataTable1.Rows.Count > 0 && details != null && details.Count > 0)
                {
                    foreach (var row in dataTable1.AsEnumerable())
                    {
                        decimal remainingQty = row["REMAINING_NET_QTY"] != null && row["REMAINING_NET_QTY"] != DBNull.Value ? Convert.ToDecimal(row["REMAINING_NET_QTY"]) : 0;
                        string poNumber = row["PO_NUMBER"] != null && row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                        string poLineItem = row["PO_LINE_ITEM"] != null && row["PO_LINE_ITEM"] != DBNull.Value ? Convert.ToString(row["PO_LINE_ITEM"]) : string.Empty;
                        //string asnCode = row["ASN_CODE"] != null && row["ASN_CODE"] != DBNull.Value ? Convert.ToString(row["ASN_CODE"]) : string.Empty;

                        var item = details.Where(d => d.PO_NUMBER == poNumber && d.PO_LINE_ITEM == poLineItem).ToList();
                        if (item != null && item.Count > 0)
                        {
                            item[0].REMAINING_NET_QTY = remainingQty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<ASNDetails> GetASNDetailsList(string ponumber, int vendorid, string sessionid)
        {
            List<ASNDetails> details = new List<ASNDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                ponumber = ponumber == "0" ? string.Empty : ponumber;
                string query = $"SELECT ASN_CODE, PO_NUMBER FROM ASNDetails WHERE";

                bool conditions = false;
                if (vendorid > 0)
                {
                    query += $" {(conditions ? " AND " : "")} VENDOR_ID = {vendorid}";
                    conditions = true;
                }
                if (!string.IsNullOrWhiteSpace(ponumber))
                {
                    query += $" {(conditions ? " AND " : "")} PO_NUMBER = '{ponumber}'";
                    conditions = true;
                }

                query += $" GROUP BY ASN_CODE, PO_NUMBER";

                CORE.DataNamesMapper<ASNDetails> mapper = new CORE.DataNamesMapper<ASNDetails>();
                var dataTable = sqlHelper.SelectQuery(query);
                details = mapper.Map(dataTable).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public Response SaveASNDetails(ASNDetails[] detailsarray)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(detailsarray[0].SessionID);

                if (detailsarray != null && detailsarray.Length > 0)
                {
                    string attachments = string.IsNullOrEmpty(detailsarray[0].ATTACHMENTS) ? string.Empty : detailsarray[0].ATTACHMENTS + ",";
                    if (detailsarray[0].AttachmentsArray != null && detailsarray[0].AttachmentsArray.Count > 0)
                    {
                        foreach (FileUpload fd in detailsarray[0].AttachmentsArray)
                        {
                            if (fd.FileStream != null && !string.IsNullOrWhiteSpace(fd.FileName))
                            {
                                fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
                                if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                                {
                                    long tick = DateTime.UtcNow.Ticks;
                                    string fileName = "POASN" + tick + "_VENDOR_" + detailsarray[0].VENDOR_ID + "_" + fd.FileName;
                                    string attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + fileName);
                                    SaveFile(attachName, fd.FileStream);
                                    attachName = fileName;

                                    Response res = SaveAttachment(attachName);
                                    if (res.ErrorMessage != "")
                                    {
                                        response.ErrorMessage = res.ErrorMessage;
                                    }

                                    fd.FileID = res.ObjectID;
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(attachments))
                            {
                                attachments += Convert.ToString(fd.FileID) + ",";
                            }
                            else
                            {
                                attachments += Convert.ToString(fd.FileID) + ",";
                            }
                        }

                        if (attachments.EndsWith(","))
                        {
                            attachments = attachments.Substring(0, attachments.Length - 1);
                        }

                        detailsarray[0].ATTACHMENTS = attachments;
                    }


                    string asnCode = DateTime.Now.Ticks.ToString();
                    foreach (var details in detailsarray)
                    {
                        string query = string.Empty;
                        if (details.ASN_ID > 0)
                        {
                            query = $@"UPDATE [dbo].[ASNDetails] SET ASN_TYPE = '{details.ASN_TYPE}', 
                                  DELIVERY_DATE = '{details.DELIVERY_DATE?.ToString("yyyy-MM-dd")}', SHIPMENT_DATE = '{details.SHIPMENT_DATE?.ToString("yyyy-MM-dd")}', 
                                  DOCUMENT_DATE = null, CUSTOMER_BATCH = '{details.CUSTOMER_BATCH}', 
                                  SHIP_FROM_LOCATION = '{details.SHIP_FROM_LOCATION}', CUSTOMER_LOCATION = '{details.CUSTOMER_LOCATION}', 
                                  SHIP_TO_LOCATION = '{details.SHIP_TO_LOCATION}', UNLOADING_POINT = '{details.UNLOADING_POINT}', SHIPPED_THROUGH = '{details.SHIPPED_THROUGH}', 
                                   MANUFACTURED_DATE = '{details.MANUFACTURED_DATE?.ToString("yyyy-MM-dd")}', BEST_BEFORE_DATE = '{details.BEST_BEFORE_DATE?.ToString("yyyy-MM-dd")}', 
                                  TOTAL_WEIGHT_DETAILS = '{details.TOTAL_WEIGHT_DETAILS}',TOTAL_VOLUME_DETAILS = '{details.TOTAL_VOLUME_DETAILS}', FREIGHT_INVOICE_NO = '{details.FREIGHT_INVOICE_NO}', 
                                  FREIGHT_TOTAL_INVOICE_AMOUNT = '{details.FREIGHT_TOTAL_INVOICE_AMOUNT}', FREIGHT_TAX = '{details.FREIGHT_TAX}', 
                                  INVOICE_NUMBER = '{details.INVOICE_NUMBER}', INVOICE_AMOUNT = '{details.INVOICE_AMOUNT}', SERVICE_CODE = '{details.SERVICE_CODE}', 
                                  SERVICE_COMPLETION_DATE = '{details.SERVICE_COMPLETION_DATE?.ToString("yyyy-MM-dd")}', SERVICE_COMPLETED_DATE = '{details.SERVICE_COMPLETED_DATE?.ToString("yyyy-MM-dd")}', 
                                  SERVICE_DOCUMENT_DATE = '{details.SERVICE_DOCUMENT_DATE?.ToString("yyyy-MM-dd")}', 
                                  SERVICE_CUSTOMER_LOCATION = '{details.SERVICE_CUSTOMER_LOCATION}', SERVICE_LOCATION = '{details.SERVICE_LOCATION}', SERVICE_BY = '{details.SERVICE_BY}', COMMENTS = '{details.COMMENTS}', 
                                  RECEIVED_CODE = '{details.RECEIVED_CODE}', 
                                  RECEIVED_BY = '{details.RECEIVED_BY}', RECEIVED_COMMENTS = '{details.RECEIVED_COMMENTS}', RECEIVED_DATE = '{details.RECEIVED_DATE?.ToString("yyyy-MM-dd")}',
                                  REQUESTED_DELIVERY_DATE = '{details.REQUESTED_DELIVERY_DATE?.ToString("yyyy-MM-dd")}', SHIP_NOTICE_TYPE = '{details.SHIP_NOTICE_TYPE}',
                                  TRANSPOTER_NAME = '{details.TRANSPOTER_NAME}', VECHICLE_NO = '{details.VECHICLE_NO}', TRACKING_DATE = '{details.TRACKING_DATE?.ToString("yyyy-MM-dd")}',
                                  INCOTERM = '{details.INCOTERM}', PAYMENT_METHOD = '{details.PAYMENT_METHOD}', PRODUCT_CODE = '{details.PRODUCT_CODE}',
                                  PRODUCT_NAME = '{details.PRODUCT_NAME}', UOM = '{details.UOM}', ORDER_QTY = '{details.ORDER_QTY}',
                                  GROSS_WT = '{details.GROSS_WT}', NET_WT = '{details.NET_WT}', NO_OF_PACKAGES = '{details.NO_OF_PACKAGES}', ATTACHMENTS = '{attachments}', PO_LINE_ITEM = '{details.PO_LINE_ITEM}', 
                                  DATE_MODIFIED = '{DateTime.UtcNow.ToString("yyyy-MM-dd")}', MODIFIED_BY = {details.MODIFIED_BY} WHERE ASN_ID = {details.ASN_ID}";

                            sqlHelper.ExecuteNonQuery_IUD(query);
                        }
                        else
                        {
                            query = @"INSERT INTO [dbo].[ASNDetails] ";
                            query += $@"SELECT {details.COMP_ID}, '{details.PO_NUMBER}', '{details.GRN_CODE}', '{details.VENDOR_CODE}', {details.VENDOR_ID}, 'ASN{asnCode}', '{details.ASN_TYPE}', 
                                  '{details.DELIVERY_DATE?.ToString("yyyy-MM-dd")}', '{details.SHIPMENT_DATE?.ToString("yyyy-MM-dd")}', 
                                   null, '{details.CUSTOMER_BATCH}', '{details.SHIP_FROM_LOCATION}', '{details.CUSTOMER_LOCATION}', 
                                  '{details.SHIP_TO_LOCATION}', '{details.UNLOADING_POINT}', '{details.SHIPPED_THROUGH}', 
                                   '{details.MANUFACTURED_DATE?.ToString("yyyy-MM-dd")}', '{details.BEST_BEFORE_DATE?.ToString("yyyy-MM-dd")}', 
                                  '{details.TOTAL_WEIGHT_DETAILS}', '{details.TOTAL_VOLUME_DETAILS}', '{details.FREIGHT_INVOICE_NO}', '{details.FREIGHT_TOTAL_INVOICE_AMOUNT}', '{details.FREIGHT_TAX}', 
                                   '{details.INVOICE_NUMBER}', '{details.INVOICE_AMOUNT}', '{details.SERVICE_CODE}', 
                                  '{details.SERVICE_COMPLETION_DATE?.ToString("yyyy-MM-dd")}', '{details.SERVICE_COMPLETED_DATE?.ToString("yyyy-MM-dd")}', '{details.SERVICE_DOCUMENT_DATE?.ToString("yyyy-MM-dd")}', 
                                   '{details.SERVICE_CUSTOMER_LOCATION}', '{details.SERVICE_LOCATION}', '{details.SERVICE_BY}', '{details.COMMENTS}', '{details.RECEIVED_CODE}', 
                                  '{details.RECEIVED_BY}', '{details.RECEIVED_COMMENTS}', '{details.RECEIVED_DATE?.ToString("yyyy-MM-dd")}', '{DateTime.UtcNow.ToString("yyyy-MM-dd")}', 
                                   '{DateTime.UtcNow.ToString("yyyy-MM-dd")}', {details.CREATED_BY}, {details.MODIFIED_BY},
                                    '{details.REQUESTED_DELIVERY_DATE?.ToString("yyyy-MM-dd")}', '{details.SHIP_NOTICE_TYPE}', '{details.TRANSPOTER_NAME}', '{details.VECHICLE_NO}', '{details.TRACKING_DATE?.ToString("yyyy-MM-dd")}', 
                                    '{details.INCOTERM}', '{details.PAYMENT_METHOD}', '{attachments}', '{details.PRODUCT_CODE}', '{details.PRODUCT_NAME}', '{details.UOM}', '{details.ORDER_QTY}', '{details.GROSS_WT}', '{details.NET_WT}', '{details.NO_OF_PACKAGES}', '{details.PO_LINE_ITEM}', '{0}'";

                            sqlHelper.ExecuteNonQuery_IUD(query);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.ObjectID = -1;
            }

            return response;
        }
        #endregion


        public FTPPOSchedule[] GetSplitPODetails(int compid, int reqid, int qcsId, string sessionid)
        {
            List<FTPPOSchedule> details = new List<FTPPOSchedule>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $"SELECT PO.*,dbo.GetCompanyName(VENDOR_ID) as VENDOR_COMP_NAME,(select top 1 value from string_split(p.ProductCode,'|'))  AS MATERIAL1 FROM POScheduleDetails  PO " +
                    $" inner join cm_product p on p.ProductId = PO.PRODUCT_ID WHERE COMP_ID = {compid} AND REQ_ID = {reqid} and QCS_ID = {qcsId}";
                CORE.DataNamesMapper<FTPPOSchedule> mapper = new CORE.DataNamesMapper<FTPPOSchedule>();
                var dataset = sqlHelper.ExecuteQuery(query);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error("exception in GetSplitPODetails>>>" + ex.Message);
            }

            return details.ToArray();
        }
        #endregion

        #region Private

        private string getStringDateFromString(string datetime, string format)
        {

            string response = string.Empty;

            if (datetime != null && datetime != "" && !string.IsNullOrEmpty(datetime))
            {
                response = Convert.ToDateTime(datetime).ToString(format);
            }

            return response;

        }
        private void SaveFile(string fileName, byte[] fileContent)
        {
            var allowedExtns = ConfigurationManager.AppSettings["SUPPORTED.FILE.EXT"].ToString().Split(',').ToList();
            var isValid = allowedExtns.Any(e => fileName.ToLower().Contains(e));
            if (isValid)
            {
                Utilities.SaveFile(fileName, fileContent);
            }
            else
            {
                //logger.Warn("Unsupported file uploaded: " + fileName);
            }
        }

        private Response SaveAttachment(string path)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private string GenerateItemizedPO(Requirement req, List<POVendor> poVendors, VendorDetails vendor, string sessionID)
        {
            PRMServices prm = new PRMServices();
            UserDetails customer = prm.GetUserDetails(req.CustomerID, sessionID);
            UserDetails vendorObj = prm.GetUserDetails(vendor.VendorID, sessionID);
            string itemRows = string.Empty;
            double tax = 0;
            double totalPriceRev = 0;
            int[] itemsArray = poVendors.Select(p => p.ItemID).ToArray();
            List<RequirementItems> items = req.ListRequirementItems.Where(i => itemsArray.Contains(i.ItemID)).ToList();
            string POID = string.Empty;
            string Comments = string.Empty;
            foreach (POVendor item in poVendors)
            {
                Requirement reqForVendor = prm.GetRequirementData(req.RequirementID, vendor.VendorID, sessionID);
                RequirementItems currentItem = reqForVendor.ListRequirementItems.Where(it => it.ItemID == item.ItemID).FirstOrDefault();
                tax = vendor.Taxes;
                RequirementItems selectedItem = items.Where(i => i.ItemID == item.ItemID).FirstOrDefault();
                string tableRows = "<tr>";
                tableRows += "<td>" + item.ProductIDorName + "</td>";
                tableRows += "<td>" + selectedItem.ProductNo + "</td>";
                tableRows += "<td>" + selectedItem.ProductDescription + "</td>";
                tableRows += "<td>" + selectedItem.ProductBrand + "</td>";
                tableRows += "<td>" + (item.Price * item.VendorPOQuantity).ToString() + "</td>";
                tableRows += "</tr>";
                totalPriceRev += Convert.ToDouble(item.Price * item.VendorPOQuantity);
                itemRows += tableRows;
                POID += item.POID;
                Comments += item.Comments + "<br/>";
            }

            Requirement reqVendor = prm.GetRequirementData(req.RequirementID, vendorObj.UserID, sessionID);
            string taxRows = string.Empty;
            double totalPrice = totalPriceRev;
            foreach (RequirementTaxes taxItem in reqVendor.ListRequirementTaxes)
            {
                string tableRows = "<tr>";
                tableRows += "<td colspan=4>" + taxItem.TaxName + "</td>";
                tableRows += "<td>" + taxItem.TaxPercentage + "%</td>";
                tableRows += "</tr>";
                taxRows += tableRows;
                totalPrice += (totalPriceRev * taxItem.TaxPercentage) / 100;
            }
            totalPrice += 0;//vendor.RevVendorFreight;

            Credentials tinCred = prm.GetUserCredentials(req.CustomerID, sessionID).FirstOrDefault(v => v.FileType == "TIN");

            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");

            //DateTime dateTimeObj = new DateTime();
            //if (req.DeliveryTime != null)
            //{
            //    dateTimeObj = (DateTime)req.DeliveryTime;
            //}

            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "ItemizedPOText.html")),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString(),
                customer.Address.ToString(),
                customer.PhoneNum.ToString(),
                customer.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                POID.ToString(),
                vendorObj.CompanyName.ToString(), // 7
                vendorObj.Address.ToString(),
                vendorObj.PhoneNum.ToString(),
                vendorObj.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                req.Title.ToString(),
                itemRows,
                totalPriceRev.ToString(),
                totalPriceRev.ToString(),
                tax,
                totalPrice,
                !string.IsNullOrEmpty(vendorObj.Address) ? customer.FirstName + " " + customer.LastName + " - " + vendorObj.Address.ToString() : customer.FirstName + " " + customer.LastName + " - " + customer.Address.ToString(),
                poVendors[0].DeliveryAddress.ToString(),
                //dateTimeObj != null ? dateTimeObj.ToShortDateString() : "No Delivery Date Specified by the Customer",
                req.DeliveryTime != "" ? req.DeliveryTime.ToString() : "No Delivery Date Specified by the Customer",              
                "",
                tinCred.CredentialID,
                customer.FirstName.ToString() + " " + customer.LastName.ToString(),
                vendorObj.FirstName.ToString() + " " + vendorObj.LastName.ToString(), // 24
                string.IsNullOrEmpty(Comments) ? "" : Comments,
                taxRows,0
                );
            return html1;
        }

        private string GenerateDesPO(POVendor povendor, Requirement req, Requirement reqVendor, UserDetails vendor, UserDetails customer)
        {
            string taxRows = string.Empty;
            double totalPrice = Convert.ToDouble(povendor.Price);
            foreach (RequirementTaxes tax in reqVendor.ListRequirementTaxes)
            {
                string tableRows = "<tr>";
                tableRows += "<td colspan=1>" + tax.TaxName + "</td>";
                tableRows += "<td>" + tax.TaxPercentage + "%</td>";
                tableRows += "</tr>";
                taxRows += tableRows;                
            }

            string html = string.Empty;
            html = "DescPO.html";

            string filestring = File.ReadAllText(HttpContext.Current.Server.MapPath(Utilities.FILE_URL + html));
            string htmlRows = string.Empty;
            try
            {
                htmlRows = String.Format(filestring,
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString(),
                customer.Address.ToString(),
                customer.PhoneNum.ToString(),
                customer.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                povendor.PurchaseID.ToString(),
                vendor.CompanyName.ToString(), // 7
                vendor.Address.ToString(),
                vendor.PhoneNum.ToString(),
                vendor.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                req.Title.ToString(),
                req.Description.ToString(), // 13
                reqVendor.AuctionVendors[0].RevPrice.ToString(),
                taxRows,
                0,
                totalPrice.ToString(),
                povendor.ExpectedDeliveryDate.ToString(),
                povendor.DeliveryAddress.ToString(),
                povendor.Comments.ToString(),
                customer.FirstName.ToString() + " " + customer.LastName.ToString()
               );
            }
            catch
            {

            }

            return htmlRows;
        }

        private string GenerateItemizedPO(VendorPO vendorpo, Requirement req, Requirement vendorreq, UserDetails customer, UserDetails vendor)
        {

            PRMServices prm = new PRMServices();
            string itemRows = string.Empty;
            double tax = 0;
            double totalPriceRev = 0;

            List<POItems> poItems = vendorpo.ListPOItems.Where(i => i.VendorPOQuantity > 0).ToList();                
            int[] itemsArray = poItems.Select(p => p.ItemID).ToArray();

            List<RequirementItems> items = vendorreq.ListRequirementItems.Where(i => itemsArray.Contains(i.ItemID)).ToList();

            DateTime? commonExpectedDeliveryDate = DateTime.Now;
            string commmonDeliveryAddress = "";

            foreach (RequirementItems ri in items)
            {
                 List<POItems> poitems = poItems.Where(i => i.ItemID == ri.ItemID).ToList();

                 if(poitems.Count > 0){

                     POItems poitem = poitems[0];

                     var GST = Convert.ToDouble(poitem.CGst) + Convert.ToDouble(poitem.SGst) + Convert.ToDouble(poitem.IGst);
                     var priceQuantity = Convert.ToDouble(poitem.POPrice) * Convert.ToDouble(poitem.VendorPOQuantity);

                     string tableRows = "<tr>";
                     tableRows += "<td>" + ri.ProductIDorName + "</td>";
                     tableRows += "<td>" + ri.ProductNo + "</td>";
                     tableRows += "<td>" + ri.ProductDescription + "</td>";
                     tableRows += "<td>" + ri.ProductBrand + "</td>";
                     tableRows += "<td>" + poitem.POPrice + "</td>";
                     tableRows += "<td>" + poitem.VendorPOQuantity + "</td>";
                     tableRows += "<td>" + GST + "</td>";
                     tableRows += "<td>" + Convert.ToDouble(priceQuantity + ((priceQuantity / 100) * (GST))) + "</td>";

                     if (!vendorpo.Common)
                     {
                         tableRows += "<td>" + poitem.ExpectedDeliveryDate + "</td>";
                         tableRows += "<td>" + poitem.DeliveryAddress + "</td>";
                     }
                     else
                     {
                         commonExpectedDeliveryDate = poitem.ExpectedDeliveryDate;
                         commmonDeliveryAddress = poitem.DeliveryAddress;
                     }

                     
                     tableRows += "</tr>";



                     totalPriceRev += Convert.ToDouble(priceQuantity + ((priceQuantity / 100) * (GST)));
                     itemRows += tableRows;
                 }               

            }


            string POID = string.Empty;
            string Comments = string.Empty;            

            //Requirement reqVendor = prm.GetRequirementData(req.RequirementID, vendorObj.UserID, sessionID);
            string taxRows = string.Empty;
            double totalPrice = totalPriceRev;
            foreach (RequirementTaxes taxItem in vendorreq.ListRequirementTaxes)
            {
                string tableRows = "<tr>";
                tableRows += "<td colspan=4>" + taxItem.TaxName + "</td>";
                tableRows += "<td>" + taxItem.TaxPercentage + "%</td>";
                tableRows += "</tr>";
                taxRows += tableRows;
                totalPrice += (totalPriceRev * taxItem.TaxPercentage) / 100;
            }
            totalPrice += 0;//vendorreq.AuctionVendors[0].RevVendorFreight;

            Credentials tinCred = prm.GetUserCredentials(req.CustomerID, vendorpo.SessionID).FirstOrDefault(v => v.FileType == "TIN");

            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");

            //DateTime dateTimeObj = new DateTime();
            //if (req.DeliveryTime != null)
            //{
            //    dateTimeObj = (DateTime)req.DeliveryTime;
            //}

            string pageName = "ItemizedPOText.html";

            if (!vendorpo.Common)
            {
                pageName = "ItemizedPOText.html";
            }
            else
            {
                pageName = "ItemizedPOTextCommon.html";
            }
            


            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + pageName)),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString(),
                customer.Address.ToString(),
                customer.PhoneNum.ToString(),
                customer.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                vendorpo.ListPOItems[0].PurchaseID.ToString(),
                vendor.CompanyName.ToString(), // 7
                vendor.Address.ToString(),
                vendor.PhoneNum.ToString(),
                vendor.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                req.Title.ToString(),


                itemRows,
                totalPriceRev.ToString(),
                totalPriceRev.ToString(),
                tax,
                totalPrice,
                !string.IsNullOrEmpty(vendor.Address) ? customer.FirstName + " " + customer.LastName + " - " + vendor.Address.ToString() : customer.FirstName + " " + customer.LastName + " - " + customer.Address.ToString(),
                vendorpo.ListPOItems[0].DeliveryAddress.ToString(),
                //dateTimeObj != null ? dateTimeObj.ToShortDateString() : "No Delivery Date Specified by the Customer",
                req.DeliveryTime != "" ? req.DeliveryTime.ToString() : "No Delivery Date Specified by the Customer",
                "",
                tinCred.CredentialID,
                customer.FirstName.ToString() + " " + customer.LastName.ToString(),
                vendor.FirstName.ToString() + " " + vendor.LastName.ToString(), // 24
                string.IsNullOrEmpty(Comments) ? "" : Comments,
                taxRows,
                0,
                vendorpo.ListPOItems[0].IndentID.ToString(), //28
                commonExpectedDeliveryDate.ToString(),
                commmonDeliveryAddress.ToString()
                );
            return html1;
        }

        private string GenerateMRRPO(DispatchTrack dispatchdetails , Requirement newreq , string mrrTable , UserDetails customer)
        {
            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");


            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "MRRPDF.html")),
                dispatchdetails.PurchaseID,
                dispatchdetails.IndentID,
                dispatchdetails.DispatchType,
                dispatchdetails.DeliveryTrackID,
                dispatchdetails.DispatchCode,
                dispatchdetails.DispatchMode,
                dispatchdetails.DispatchDate,
                dispatchdetails.DispatchComments,
                newreq.Title,
                mrrTable.ToString(),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString()

                );
            return html1;
        }

        private string GenerateMRRPOReportpdf(DispatchTrack dispatchdetails, Requirement newreq, string mrrTable, UserDetails customer)
        {

            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");


            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "MaterialReceivedReportpdf.html")),
                dispatchdetails.PurchaseID,
                dispatchdetails.IndentID,
                dispatchdetails.DispatchType,
                dispatchdetails.DeliveryTrackID,
                dispatchdetails.DispatchCode,
                dispatchdetails.DispatchMode,
                dispatchdetails.DispatchDate,
                dispatchdetails.DispatchComments,
                newreq.Title,
                mrrTable.ToString(),
                dispatchdetails.RecivedCode,
                dispatchdetails.RecivedBy,
                dispatchdetails.RecivedComments,
                dispatchdetails.RecivedDate,
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString()
                );
            return html1;
        }

        #endregion
    }
    
}
 