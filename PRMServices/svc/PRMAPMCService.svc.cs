﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNet.SignalR;
using PRMServices.SignalR;
using PRMServices.SQLHelper;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMAPMCService : IPRMAPMCService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        #region Get

        public List<APMC> GetAPMCList(int userID, string sessionID)
        {
            List<APMC> APMClist = new List<APMC>();

            try
            {
                ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_GetAPMCList", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        APMC apmc = new APMC();

                        apmc.APMCID = row["APMC_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_ID"]) : 0;
                        apmc.APMCName = row["APMC_NAME"] != DBNull.Value ? Convert.ToString(row["APMC_NAME"]) : string.Empty;
                        apmc.APMCDescription = row["APMC_DESC"] != DBNull.Value ? Convert.ToString(row["APMC_DESC"]) : string.Empty;
                        apmc.Company = new Company
                        {
                            CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0,
                            CompanyName = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : string.Empty
                        };
                        apmc.Customer = new User
                        {
                            UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0,
                            FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty,
                            LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty
                        };

                        APMClist.Add(apmc);
                    }
                }
            }
            catch (Exception ex)
            {
                APMC err = new APMC();
                err.ErrorMessage = ex.Message;
                APMClist.Add(err);
            }

            return APMClist;
        }

        public APMC GetAPMC(int apmcID, int userID, string sessionID)
        {
            APMC apmc = new APMC();
            try
            {
                ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_APMC_ID", apmcID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_GetAPMC", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        apmc.APMCID = row["APMC_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_ID"]) : 0;
                        apmc.APMCName = row["APMC_NAME"] != DBNull.Value ? Convert.ToString(row["APMC_NAME"]) : string.Empty;
                        apmc.APMCDescription = row["APMC_DESC"] != DBNull.Value ? Convert.ToString(row["APMC_DESC"]) : string.Empty;
                        apmc.Category = row["CATEGORY"] != DBNull.Value ? Convert.ToString(row["CATEGORY"]) : string.Empty;
                        apmc.Company = new Company
                        {
                            CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0,
                            CompanyName = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : string.Empty
                        };
                        apmc.Customer = new User
                        {
                            UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0,
                            FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty,
                            LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty
                        };
                    }
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                    {
                        DataRow row = ds.Tables[1].Rows[0];
                        apmc.apmcSettings = new APMCSettings();

                        apmc.apmcSettings.APMCID = row["APMC_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_ID"]) : 0;
                        apmc.apmcSettings.APMCSettingsID = row["APMC_SET_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_SET_ID"]) : 0;
                        apmc.apmcSettings.RelatedCess = row["RELATED_CESS"] != DBNull.Value ? Convert.ToDouble(row["RELATED_CESS"]) : 0;
                        apmc.apmcSettings.Hamali = row["HAMALI"] != DBNull.Value ? Convert.ToDouble(row["HAMALI"]) : 0;
                        apmc.apmcSettings.Freight = row["FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT"]) : 0;
                        apmc.apmcSettings.Sutli = row["SUTLI"] != DBNull.Value ? Convert.ToDouble(row["SUTLI"]) : 0;
                        apmc.apmcSettings.SC = row["SC"] != DBNull.Value ? Convert.ToDouble(row["SC"]) : 0;
                        apmc.apmcSettings.Brokerage = row["BROKERAGE"] != DBNull.Value ? Convert.ToDouble(row["BROKERAGE"]) : 0;
                        apmc.apmcSettings.Adat = row["ADAT"] != DBNull.Value ? Convert.ToDouble(row["ADAT"]) : 0;
                        apmc.apmcSettings.Packing = row["PACKING"] != DBNull.Value ? Convert.ToDouble(row["PACKING"]) : 0;
                    }


                    List<APMCVendor> vendors = new List<APMCVendor>();
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        APMCVendor apmcVendor = new APMCVendor();

                        apmcVendor.APMCID = row["APMC_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_ID"]) : 0;
                        apmcVendor.APMCVendorID = row["APMC_V_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_V_ID"]) : 0;
                        apmcVendor.Company = new Company
                        {
                            CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0,
                            CompanyName = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : string.Empty
                        };
                        apmcVendor.Vendor = new User
                        {
                            UserID = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_ID"]) : 0,
                            FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty,
                            LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty
                        };
                        apmcVendor.CompanyID = apmcVendor.Company.CompanyID;
                        apmcVendor.CompanyName = apmcVendor.Company.CompanyName;
                        apmcVendor.VendorID = apmcVendor.Vendor.UserID;
                        apmcVendor.VendorName = apmcVendor.Vendor.FirstName + " " + apmcVendor.Vendor.LastName;

                        vendors.Add(apmcVendor);
                    }
                    apmc.apmcVendors = vendors;
                }
            }
            catch (Exception ex)
            {
                apmc.ErrorMessage = ex.Message;
            }

            return apmc;
        }

        public APMCDashboard GetAPMCNegotiationList(int userID, string sessionID, DateTime fromdate, DateTime todate)
        {
            APMCDashboard apmcdashboard = new APMCDashboard();
            apmcdashboard.APMCNegotiationList = new List<APMCNegotiation>();
            try
            {
                ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                DataSet ds = sqlHelper.SelectList("cp_GetAPMCNegotiationList", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        APMCNegotiation apmcnegotiation = new APMCNegotiation();

                        apmcnegotiation.APMCNegotiationID = row["APMC_NEG_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_NEG_ID"]) : 0;
                        apmcnegotiation.CustPriceInclInit = row["EXP_PRICE_INCL"] != DBNull.Value ? Convert.ToDecimal(row["EXP_PRICE_INCL"]) : 0;
                        apmcnegotiation.CustPriceExclInit = row["CUST_PRICE_EXCL_INIT"] != DBNull.Value ? Convert.ToDecimal(row["CUST_PRICE_EXCL_INIT"]) : 0;
                        apmcnegotiation.CreatedDate = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;

                        apmcdashboard.APMCNegotiationList.Add(apmcnegotiation);
                    }
                }
            }
            catch (Exception ex)
            {
                apmcdashboard.ErrorMessage = ex.Message;
            }

            return apmcdashboard;
        }

        public APMCDashboard GetAPMCNegotiation(int apmcnegid, int userID, string sessionID)
        {
            APMCDashboard dashboard = new APMCDashboard();
            dashboard.APMCNegotiationList = new List<APMCNegotiation>();

            try
            {
                ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_APMC_NEG_ID", apmcnegid);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_GetAPMCNegotiation", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        DataRow row = ds.Tables[0].Rows[0];
                        dashboard.APMCInput = new APMCInput
                        {
                            APMCNegotiationID = row["APMC_NEG_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_NEG_ID"]) : 0,
                            InputPrice = row["EXP_PRICE_INCL"] != DBNull.Value ? Convert.ToDecimal(row["EXP_PRICE_INCL"]) : 0,
                            IsComplete = row["IS_COMPLETE"] != DBNull.Value ? Convert.ToInt32(row["IS_COMPLETE"]) : 0
                        };
                    }

                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        APMCNegotiation negotiation = new APMCNegotiation();
                        negotiation.APMCNegotiationID = row["APMC_NEG_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_NEG_ID"]) : 0;
                        negotiation.APMCID = row["APMC_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_ID"]) : 0;
                        negotiation.APMCPriceID = row["APMC_VP_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_VP_ID"]) : 0;
                        negotiation.APMCName = row["APMC_NAME"] != DBNull.Value ? Convert.ToString(row["APMC_NAME"]) : string.Empty;
                        negotiation.IsFrozen = row["IS_FROZEN"] != DBNull.Value ? Convert.ToInt32(row["IS_FROZEN"]) : 0;
                        negotiation.QuantityUpdated = row["QUANTITY_UPDATED"] != DBNull.Value ? Convert.ToInt32(row["QUANTITY_UPDATED"]) : 0;
                        negotiation.ModifiedBy = row["MODIFIED_BY"] != DBNull.Value ? Convert.ToInt32(row["MODIFIED_BY"]) : 0;

                        negotiation.CustPriceExclInit = row["CUST_PRICE_EXCL_INIT"] != DBNull.Value ? Convert.ToDecimal(row["CUST_PRICE_EXCL_INIT"]) : 0;
                        negotiation.CustPriceInclInit = row["CUST_PRICE_INCL_INIT"] != DBNull.Value ? Convert.ToDecimal(row["CUST_PRICE_INCL_INIT"]) : 0;
                        negotiation.VendorPriceExclInit = row["VEND_PRICE_EXCL_INIT"] != DBNull.Value ? Convert.ToDecimal(row["VEND_PRICE_EXCL_INIT"]) : 0;
                        negotiation.VendorPriceInclInit = row["VEND_PRICE_INCL_INIT"] != DBNull.Value ? Convert.ToDecimal(row["VEND_PRICE_INCL_INIT"]) : 0;

                        negotiation.CustPriceInclFinal = row["CUST_PRICE_INCL_FINAL"] != DBNull.Value ? Convert.ToDecimal(row["CUST_PRICE_INCL_FINAL"]) : 0;
                        negotiation.CustPriceExclFinal = row["CUST_PRICE_EXCL_FINAL"] != DBNull.Value ? Convert.ToDecimal(row["CUST_PRICE_EXCL_FINAL"]) : 0;
                        negotiation.VendorPriceExclFinal = row["VEND_PRICE_EXCL_FINAL"] != DBNull.Value ? Convert.ToDecimal(row["VEND_PRICE_EXCL_FINAL"]) : 0;
                        negotiation.VendorPriceInclFinal = row["VEND_PRICE_INCL_FINAL"] != DBNull.Value ? Convert.ToDecimal(row["VEND_PRICE_INCL_FINAL"]) : 0;

                        negotiation.DifferenceExcl = row["DIFF_EXCL"] != DBNull.Value ? Convert.ToDecimal(row["DIFF_EXCL"]) : 0;
                        negotiation.DifferenceIncl = row["DIFF_INCL"] != DBNull.Value ? Convert.ToDecimal(row["DIFF_INCL"]) : 0;
                        negotiation.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDecimal(row["SAVINGS"]) : 0;

                        if (negotiation.Savings <= 0)
                        {
                            negotiation.Savings = 0;
                        }

                        negotiation.IsNewNegotiationStarted = row["P_IS_NEW_NEG_STARTED"] != DBNull.Value ? Convert.ToInt32(row["P_IS_NEW_NEG_STARTED"]) : 0;
                        negotiation.PrevNegotiationID = row["P_APMC_NEG_PREV_ID"] != DBNull.Value ? Convert.ToInt32(row["P_APMC_NEG_PREV_ID"]) : 0;

                        negotiation.AttachmentID = row["ATTACHMENT"] != DBNull.Value ? Convert.ToInt32(row["ATTACHMENT"]) : 0;

                        negotiation.Vendor = new User
                        {
                            UserID = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_ID"]) : 0,
                            FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty,
                            LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty
                        };
                        negotiation.VendorCompany = new Company
                        {
                            CompanyName = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : string.Empty,
                            CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0
                        };
                        dashboard.APMCNegotiationList.Add(negotiation);
                    }
                }
            }
            catch (Exception ex)
            {
                dashboard.ErrorMessage = ex.Message;
            }

            return dashboard;
        }

        public List<APMCVendor> GetAPMCVendorList(int userID, string sessionID)
        {
            List<APMCVendor> APMClist = new List<APMCVendor>();
            try
            {
                ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_GetAPMCVendorList", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        APMCVendor apmc = new APMCVendor();

                        apmc.APMCID = row["APMC_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_ID"]) : 0;
                        apmc.APMCVendorID = row["APMC_V_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_V_ID"]) : 0;
                        apmc.Company = new Company
                        {
                            CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0,
                            CompanyName = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : string.Empty
                        };
                        apmc.Vendor = new User
                        {
                            UserID = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_ID"]) : 0,
                            FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty,
                            LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty
                        };
                        apmc.VendorName = apmc.Vendor.FirstName + ' ' + apmc.Vendor.LastName;
                        apmc.APMCName = row["APMC_NAME"] != DBNull.Value ? Convert.ToString(row["APMC_NAME"]) : string.Empty;
                        apmc.UserID = userID;
                        APMClist.Add(apmc);
                    }
                }
            }
            catch (Exception ex)
            {
                APMCVendor err = new APMCVendor();
                err.ErrorMessage = ex.Message;
                APMClist.Add(err);
            }

            return APMClist;
        }

        //I thing SP is wrong here
        public List<APMCSettings> GetAPMCSettingsList(int APMCID, int userID, string sessionID)
        {
            List<APMCSettings> APMClist = new List<APMCSettings>();
            try
            {
                ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_APMC_ID", APMCID);
                DataSet ds = sqlHelper.SelectList("cp_GetAPMCVendorList", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        APMCSettings apmc = new APMCSettings();

                        apmc.APMCID = row["APMC_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_ID"]) : 0;
                        apmc.APMCSettingsID = row["APMC_SET_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_SET_ID"]) : 0;
                        apmc.RelatedCess = row["RELATED_CESS"] != DBNull.Value ? Convert.ToDouble(row["RELATED_CESS"]) : 0;
                        apmc.Hamali = row["HAMALI"] != DBNull.Value ? Convert.ToDouble(row["HAMALI"]) : 0;
                        apmc.Freight = row["FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT"]) : 0;
                        apmc.Sutli = row["SUTLI"] != DBNull.Value ? Convert.ToDouble(row["SUTLI"]) : 0;
                        apmc.SC = row["SC"] != DBNull.Value ? Convert.ToDouble(row["SC"]) : 0;
                        apmc.Brokerage = row["BROKERAGE"] != DBNull.Value ? Convert.ToDouble(row["BROKERAGE"]) : 0;
                        apmc.Adat = row["ADAT"] != DBNull.Value ? Convert.ToDouble(row["ADAT"]) : 0;
                        apmc.Packing = row["PACKING"] != DBNull.Value ? Convert.ToDouble(row["PACKING"]) : 0;

                        APMClist.Add(apmc);
                    }
                }
            }
            catch (Exception ex)
            {
                APMCSettings err = new APMCSettings();
                err.ErrorMessage = ex.Message;
                APMClist.Add(err);
            }

            return APMClist;
        }

        public List<APMCVendorQuantity> GetAPMCVendorQuantityList(int apmcnegid, int userID, int vendorID, string sessionID)
        {
            List<APMCVendorQuantity> APMClist = new List<APMCVendorQuantity>();
            try
            {
                ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_VENDOR_ID", vendorID);
                sd.Add("P_APMC_NEG_ID", apmcnegid);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_GetAPMCVendorQuantityList", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        APMCVendorQuantity apmc = new APMCVendorQuantity();

                        apmc.APMCNegotiationID = row["APMC_NEG_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_NEG_ID"]) : 0;
                        apmc.APMCVendorQuantityID = row["APMC_VQ_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_VQ_ID"]) : 0;
                        apmc.VendorID = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_ID"]) : 0;
                        apmc.Quantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        apmc.Price = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
                        apmc.Units = row["UNITS"] != DBNull.Value ? Convert.ToString(row["UNITS"]) : string.Empty;
                        apmc.DateCreated = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MaxValue;

                        APMClist.Add(apmc);
                    }
                }
            }
            catch (Exception ex)
            {
                APMCVendorQuantity err = new APMCVendorQuantity();
                err.ErrorMessage = ex.Message;
                APMClist.Add(err);
            }

            return APMClist;
        }

        public List<APMCAudit> GetAPMCAudit(int apmcnegID, int userID, int vendorID, string sessionID)
        {
            List<APMCAudit> audit = new List<APMCAudit>();
            try
            {
                ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_APMC_NEG_ID", apmcnegID);
                sd.Add("P_VENDOR_ID", vendorID);
                DataSet ds = sqlHelper.SelectList("cp_GetAPMCAudit", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        APMCAudit apmc = new APMCAudit();
                        apmc.APMCNegotiationID = row["APMC_NEG_ID"] != DBNull.Value ? Convert.ToInt32(row["APMC_NEG_ID"]) : 0;
                        apmc.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        apmc.VendorID = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_ID"]) : 0;
                        apmc.VendorPrice = row["VENDOR_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VENDOR_PRICE"]) : 0;
                        apmc.CustomerPrice = row["CUSTOMER_PRICE"] != DBNull.Value ? Convert.ToDouble(row["CUSTOMER_PRICE"]) : 0;
                        apmc.Created = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MaxValue;
                        apmc.IsFrozen = row["IS_FROZEN"] != DBNull.Value ? Convert.ToInt32(row["IS_FROZEN"]) : 0;
                        audit.Add(apmc);
                    }
                }
            }
            catch (Exception ex)
            {
                APMCAudit err = new APMCAudit();
                err.ErrorMessage = ex.Message;
                audit.Add(err);
            }

            return audit;
        }

        public bool CheckUniqueIfExists(string param, string idtype, string sessionID)
        {
            bool response = false;
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PARAM", param);
                sd.Add("P_ID_TYPE", idtype);
                DataSet ds = sqlHelper.SelectList("cp_CheckUniqueIfExists", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    int result = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (result > 0)
                    {
                        response = true;
                    }
                    else
                    {
                        response = false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return response;
        }

        #endregion

        #region Post

        public Response SaveAPMC(APMC apmc)
        {
            Response response = new Response();
            try
            {
                DataSet ds = APMCUtility.SaveAPMCObject(apmc);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    apmc.APMCID = response.ObjectID;
                    if (apmc.APMCID == 0)
                    {
                        ds = APMCUtility.SaveAPMCSettings(new APMCSettings());
                    }
                    else
                    {
                        apmc.apmcSettings.APMCID = apmc.APMCID;
                        ds = APMCUtility.SaveAPMCSettings(apmc.apmcSettings);
                    }

                    foreach (APMCVendor vendor in apmc.apmcVendors)
                    {
                        vendor.APMCID = apmc.APMCID;
                        vendor.UserID = apmc.Customer.UserID;
                        ds = APMCUtility.SaveAPMCVendor(vendor);
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveAPMCSettings(APMCSettings apmcSettings)
        {
            Response response = new Response();
            try
            {
                DataSet ds = APMCUtility.SaveAPMCSettings(apmcSettings);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveAPMCVendor(APMCVendor apmcVendor)
        {
            Response response = new Response();
            try
            {
                DataSet ds = APMCUtility.SaveAPMCVendor(apmcVendor);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveAPMCInput(APMCInput apmcInput)
        {
            Response response = new Response();
            try
            {
                DataSet ds = APMCUtility.SaveAPMCInput(apmcInput);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveAPMCNegotiation(APMCNegotiation apmcNegotiation)
        {
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (apmcNegotiation.FileStream != null)
                {

                    long tick = DateTime.Now.Ticks;
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "_apmcQuotation_" + "_" + tick + "_" + apmcNegotiation.FileName);
                    SaveFile(fileName, apmcNegotiation.FileStream);

                    fileName = "_apmcQuotation_" + "_" + tick + "_" + apmcNegotiation.FileName;
                    Response res = SaveAttachment(fileName);
                    if (res.ErrorMessage != "")
                    {
                        response.ErrorMessage = res.ErrorMessage;
                    }

                    fileName = res.ObjectID.ToString();
                    apmcNegotiation.AttachmentID = Convert.ToInt32(fileName);
                }

                DataSet ds = APMCUtility.SaveAPMCNegotiation(apmcNegotiation, apmcNegotiation.APMCNegotiationID, apmcNegotiation.UserID);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        var context = GlobalHost.ConnectionManager.GetHubContext<ApmcHub>();
                        context.Clients.Group(Utilities.APMCGroupName + apmcNegotiation.APMCNegotiationID).refreshDashboard(apmcNegotiation);
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveAPMCVendorQuantityList(List<APMCVendorQuantity> list)
        {
            Response response = new Response();
            try
            {                
                DataSet ds = new DataSet();
                foreach (APMCVendorQuantity vendorQ in list)
                {
                    ds = APMCUtility.SaveAPMCVendorQuantity(vendorQ);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveAPMCNegotiationList(List<APMCNegotiation> apmcNegotiationList, int apmcNegotiationID, int userID)
        {
            Response response = new Response();
            try
            {
                DataSet ds = new DataSet();
                foreach (APMCNegotiation apmcNegotiation in apmcNegotiationList)
                {
                    ds = APMCUtility.SaveAPMCNegotiation(apmcNegotiation, apmcNegotiationID, userID);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion

        #region Private

        private int ValidateSession(string sessionId)
        {
            return Utilities.ValidateSession(sessionId);
        }

        private void SaveFile(string fileName, byte[] fileContent)
        {
            var allowedExtns = ConfigurationManager.AppSettings["SUPPORTED.FILE.EXT"].ToString().Split(',').ToList();
            var isValid = allowedExtns.Any(e => fileName.ToLower().Contains(e));

            if (isValid)
            {
                Utilities.SaveFile(fileName, fileContent);
            }
            else
            {
                logger.Warn("Unsupported file uploaded: " + fileName);
            }
        }

        private Response SaveAttachment(string path)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public string GenerateEmailBody(string TemplateName)
        {
            string body = string.Empty;
            //body = Engine.Razor.RunCompile(File.ReadAllText(templateFolderPath + "/EmailTemplates/" + TemplateName), "Email", null, model);
            XmlDocument doc = new XmlDocument();
            doc.Load(templateFolderPath + "/EmailTemplates/EmailFormats.xml");
            XmlNode node = doc.DocumentElement.SelectSingleNode(TemplateName);
            body = node.InnerText;
            string footerName = "";
            if (TemplateName.ToLower().Contains("email"))
            {
                footerName = "EmailFooter";
            }
            else if (TemplateName.ToLower().Contains("sms"))
            {
                footerName = "SMSFooter";
            }
            else
            {
                footerName = "FooterXML";
            }


            if (footerName == "FooterXML")
            {

            }
            else
            {
                XmlNode footernode = doc.DocumentElement.SelectSingleNode(footerName);
                body += footernode.InnerText;
            }


            return body;
        }

        public async Task SendEmail(string To, string Subject, string Body, string sessionID = null, Attachment attachment = null, List<Attachment> ListAttachment = null)
        {

            try
            {
                PRMServices prm = new PRMServices();
                Communication communication = new Communication();
                communication = Utilities.GetCommunicationData("EMAIL_FROM", sessionID);
                Body = Body.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                Body = Body.Replace("COMPANY_NAME", !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                Body = Body.Replace("USER_NAME", !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
                Body = Body.Replace("COMPANY_ADDRESS", !string.IsNullOrEmpty(communication.CompanyAddress) ? communication.CompanyAddress : "");


                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString());
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
                smtpClient.Credentials = credentials;

                smtpClient.EnableSsl = false;

                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(prm.GetFromAddress(communication.User.Email), prm.GetFromDisplayName(communication.User.Email));
                mail.BodyEncoding = System.Text.Encoding.ASCII;
                if (attachment != null)
                {
                    mail.Attachments.Add(attachment);
                }

                if (ListAttachment != null)
                {
                    foreach (Attachment singleAttachment in ListAttachment)
                    {
                        if (singleAttachment != null)
                        {
                            mail.Attachments.Add(singleAttachment);
                        }
                    }
                }

                List<string> ToAddresses = To.Split(',').ToList<string>();
                foreach (string address in ToAddresses)
                {
                    if (!string.IsNullOrEmpty(address))
                    {
                        mail.To.Add(new MailAddress(address));
                    }

                }

                //if (!string.IsNullOrEmpty(communication.User.Email))
                //{
                //    mail.CC.Add(communication.User.Email);
                //}

                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = Body;

                //smtpClient.Send(mail);
                await smtpClient.SendMailAsync(mail);

                Response response = new Response();
                response.ObjectID = 1;
                //return response;
            }
            catch (Exception ex)
            {
                Response response = new Response();
                response.ErrorMessage = ex.Message;
                //return response;
            }
        }

        private string templateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");

        #endregion

    }
}