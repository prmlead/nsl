﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Models;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMCatalogReportsService : IPRMCatalogReportsService
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();

        #region Services

        public List<cr_UserDepartments> GetUserDepartments(int U_ID, int IS_SUPER_USER, string sessionid)
        {
            List<cr_UserDepartments> details = new List<cr_UserDepartments>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_IS_SUPER_USER", IS_SUPER_USER);
                CORE.DataNamesMapper<cr_UserDepartments> mapper = new CORE.DataNamesMapper<cr_UserDepartments>();
                var dataset = sqlHelper.SelectList("cr_GetUserDepartments", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_DepartmentCategories> GetDepartmentCategories(int U_ID, string DEPT_IDS, string sessionid)
        {
            List<cr_DepartmentCategories> details = new List<cr_DepartmentCategories>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_DEPT_IDS", DEPT_IDS);
                CORE.DataNamesMapper<cr_DepartmentCategories> mapper = new CORE.DataNamesMapper<cr_DepartmentCategories>();
                var dataset = sqlHelper.SelectList("cr_GetDepartmentCategories", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_DepartmentCategories> GetCategorySubcategories(int U_ID, string CAT_IDS, string sessionid)
        {
            List<cr_DepartmentCategories> details = new List<cr_DepartmentCategories>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_CAT_IDS", CAT_IDS);
                CORE.DataNamesMapper<cr_DepartmentCategories> mapper = new CORE.DataNamesMapper<cr_DepartmentCategories>();
                var dataset = sqlHelper.SelectList("cr_GetCategorySubcategories", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_CategoryProducts> GetCategoryProducts(int U_ID, string CAT_IDS, string sessionid)
        {
            List<cr_CategoryProducts> details = new List<cr_CategoryProducts>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_CAT_IDS", CAT_IDS);
                CORE.DataNamesMapper<cr_CategoryProducts> mapper = new CORE.DataNamesMapper<cr_CategoryProducts>();
                var dataset = sqlHelper.SelectList("cr_GetCategoryProducts", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public cr_CategoryProducts GetProductAnalysis(int U_ID, int PROD_ID, string sessionid, int call_code, DateTime fromDate, DateTime toDate, int callID)
        {
            cr_CategoryProducts details = new cr_CategoryProducts();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_PROD_ID", PROD_ID);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                CORE.DataNamesMapper<cr_CategoryProducts> mapper = new CORE.DataNamesMapper<cr_CategoryProducts>();
                var dataset = new DataSet();
                if (call_code == 1)
                {
                    dataset = sqlHelper.SelectList("cr_GetProductAnalysis", sd);
                }
                else if (call_code == 2)
                {
                    dataset = sqlHelper.SelectList("cr_GetProductAnalysisCall2", sd);
                }
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_VendorsAndOrders> GetVendorsAndOrders(int U_ID, int PROD_ID, string sessionid)
        {
            List<cr_VendorsAndOrders> details = new List<cr_VendorsAndOrders>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_PROD_ID", PROD_ID);
                CORE.DataNamesMapper<cr_VendorsAndOrders> mapper = new CORE.DataNamesMapper<cr_VendorsAndOrders>();
                var dataset = new DataSet();
                dataset = sqlHelper.SelectList("cr_GetVendorsAndOrders", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_VendorsAndOrders> GetMostOrdersAndVendors(int U_ID, int PROD_ID, string sessionid)
        {
            List<cr_VendorsAndOrders> details = new List<cr_VendorsAndOrders>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_PROD_ID", PROD_ID);
                CORE.DataNamesMapper<cr_VendorsAndOrders> mapper = new CORE.DataNamesMapper<cr_VendorsAndOrders>();
                var dataset = new DataSet();
                dataset = sqlHelper.SelectList("cr_GetMostOrdersAndVendors", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_VendorsAndOrders> GetProductRequirements(int U_ID, int PROD_ID, DateTime fromDate, DateTime toDate, int callID, string sessionid)
        {
            List<cr_VendorsAndOrders> details = new List<cr_VendorsAndOrders>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_PROD_ID", PROD_ID);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_CALL_ID", callID);
                CORE.DataNamesMapper<cr_VendorsAndOrders> mapper = new CORE.DataNamesMapper<cr_VendorsAndOrders>();
                var dataset = new DataSet();
                dataset = sqlHelper.SelectList("cr_GetProductRequirements", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<cr_VendorsAndOrders> GetProductPriceHistory(int U_ID, int PROD_ID, DateTime fromDate, DateTime toDate, int callID, string sessionid)
        {
            List<cr_VendorsAndOrders> details = new List<cr_VendorsAndOrders>();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_PROD_ID", PROD_ID);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_CALL_ID", callID);
                CORE.DataNamesMapper<cr_VendorsAndOrders> mapper = new CORE.DataNamesMapper<cr_VendorsAndOrders>();
                var dataset = new DataSet();
                dataset = sqlHelper.SelectList("cr_GetProductPriceHistory", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public cr_ProductAnalysis GetProductAnalysisByProduct(int U_ID, int PROD_ID, DateTime fromDate, DateTime toDate, int callID, string sessionid)
        {
            cr_ProductAnalysis details = new cr_ProductAnalysis();
            try
            {
                //Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PRODUCT_ID", PROD_ID);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_CALL_ID", callID);

                var dataset = sqlHelper.SelectList("rp_GetProductAnalysisByProduct", sd);
                if (dataset != null && dataset.Tables.Count > 0)
                {
                    if (dataset.Tables[2].Rows.Count > 0)
                    {
                        DataRow row = dataset.Tables[2].Rows[0];
                        details.LEAST_ITEM_L1_PRICE = row["PARAM_LEAST_ITEM_SUM"] != DBNull.Value ? Convert.ToDouble(row["PARAM_LEAST_ITEM_SUM"]) : 0;
                        //details.VENDOR_SHARE_PRICE = row["PARAM_VENDOR_SHARE"] != DBNull.Value ? Convert.ToDouble(row["PARAM_VENDOR_SHARE"]) : 0;
                        details.RFQ_VENDORS = row["PARAM_ITEM_SUPPLIERS"] != DBNull.Value ? Convert.ToInt32(row["PARAM_ITEM_SUPPLIERS"]) : 0;

                    }

                    if (dataset.Tables[3].Rows.Count > 0)
                    {
                        details.VendorShare = new List<VendorShare>();
                        foreach (DataRow row3 in dataset.Tables[3].Rows)
                        {
                            VendorShare vend = new VendorShare();
                            vend.U_ID = row3["U_ID"] != DBNull.Value ? Convert.ToInt32(row3["U_ID"]) : 0;
                            vend.VENDOR_NAME = row3["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row3["VENDOR_NAME"]) : string.Empty;
                            vend.VENDOR_SHARE_PRICE = row3["PARAM_VENDOR_SHARE"] != DBNull.Value ? Convert.ToDouble(row3["PARAM_VENDOR_SHARE"]) : 0;
                            details.VendorShare.Add(vend);
                        }
                    }
                    if (dataset.Tables[0].Rows.Count > 0)
                    {
                        details.MaterialSourced = new List<MaterialSourced>();
                        foreach (DataRow row in dataset.Tables[0].Rows)
                        {
                            MaterialSourced ms = new MaterialSourced();
                            ms.ITEM_BASED_UNITS = row["PARAM_SUM_ITEM_QTY_BASED_ON_UNITS"] != DBNull.Value ? Convert.ToInt32(row["PARAM_SUM_ITEM_QTY_BASED_ON_UNITS"]) : 0;
                            ms.ITEM_UOM = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
                            details.MaterialSourced.Add(ms);
                        }
                    }

                    if (dataset.Tables[1].Rows.Count > 0)
                    {
                        details.FrequencySourcing = new List<FrequencySourcing>();
                        foreach (DataRow row in dataset.Tables[1].Rows)
                        {
                            FrequencySourcing fs = new FrequencySourcing();
                            fs.MONTH_REQ = row["REQ_ID_COUNT"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID_COUNT"]) : 0;
                            fs.MONTH_NAME = row["MONTH_NAME"] != DBNull.Value ? Convert.ToString(row["MONTH_NAME"]) : string.Empty;
                            details.FrequencySourcing.Add(fs);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        #endregion Services

    }
}