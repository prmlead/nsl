angular = require('angular');

angular.module('loginModule')
    .controller('forgotpasswordCtrl',["$scope", "loginService", function LoginCtrl($scope, loginService) {
        $scope.forgotpasswordfunction = function () {
            loginService.forgotpassword($scope.forgotpassword)
                .then(function (response) {
                    if (response.data.errorMessage == "") {
                        $scope.forgotpassword = {};
                        swal("Done!", "Password reset link sent to your registerd Email and Mobile Number.", "success");
                        
                    } else {
                        swal("Warning", "Please check the Email/Phone you have entered.", "warning");
                    }
                });
        };
    }]);