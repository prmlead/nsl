angular.module('profileModule')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/profile-info');

    $stateProvider
        .state('profile-home', {
            url: '/profile-home',
            templateUrl: '/partials/profile-home.html',
            controller: 'profileHomeCtrl'
        }).state('profile-info', {
            url: '/profile-info',
            templateUrl: '/partials/profile-info.html',
            controller: 'profileInfoCtrl'
        }).state('profile-subusers', {
            url: '/profile-subusers',
            templateUrl: '/partials/profile-subusers.html',
            controller: 'profileSubUsersCtrl'
        }).state('profile-myvendors', {
            url: '/profile-myvendors',
            templateUrl: '/partials/profile-myvendors.html',
            controller: 'profileMyVendorsCtrl'
        }).state('profile-password', {
            url: '/profile-password',
            templateUrl: '/partials/profile-password.html',
            controller: 'profilePasswordCtrl'
        }).state('profile-settings', {
            url: '/profile-settings',
            templateUrl: '/partials/profile-settings.html',
            controller: 'profileSettingsCtrl'
        }).state('profile-myrequirements', {
            url: '/profile-myrequirements',
            templateUrl: '/partials/profile-myrequirements.html',
            controller: 'profileMyrequirementCtrl'
        }).state('profile-myleads', {
            url: '/profile-myleads',
            templateUrl: '/partials/profile-myleads.html',
            controller: 'profileMyLeadsCtrl'
        }).state('profile', {
            url: '/profile',
            templateUrl: '/partials/profile.html',
            controller: 'profileCtrl'
        })
}]);