angular.module('storeModule')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/stores');

    $stateProvider
        .state('stores', {
            url: '/stores',
            templateUrl: '/partials/stores.html',
            controller: 'profileHomeCtrl'
        }).state('add-new-store', {
            url: '/add-new-store',
            templateUrl: '/partials/addnewstore.html',
            controller: 'addnewstoreCtrl'
        }).state('store-items', {
            url: '/store-items',
            templateUrl: '/partials/storeitems.html',
            controller: 'storeitemsCtrl'
        }).state('add-new-store-item', {
            url: '/add-new-store-item',
            templateUrl: '/partials/addnewstoreitem.html',
            controller: 'addnewstoreitemCtrl'
        }).state('store-item-properties', {
            url: '/store-item-properties',
            templateUrl: '/storeitemproperties.html',
            controller: 'storeitempropertiesCtrl'
        })
}]);