﻿using System;
using System.Collections.Generic;
using System.Data;
using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices.Common
{
    public class WFUtility
    {
        public static Workflow GetWorkflowObject(DataRow row)
        {
            Workflow detail = new Workflow();
            detail.WorkflowID = Convert.ToInt32(row.GetColumnValue("WF_ID", detail.WorkflowID.GetType()));
            detail.CompanyID = Convert.ToInt32(row.GetColumnValue("COMP_ID", detail.CompanyID.GetType()));
            detail.WorkflowTitle = Convert.ToString(row.GetColumnValue("WF_TITLE", detail.WorkflowTitle.GetType()));
            detail.WorkflowDetails = Convert.ToString(row.GetColumnValue("WF_DETAILS", detail.WorkflowDetails.GetType()));
            detail.WorkflowModule = Convert.ToString(row.GetColumnValue("WF_MODULE", detail.WorkflowModule.GetType()));
            detail.DeptID = Convert.ToInt32(row.GetColumnValue("DEPT_ID", detail.DeptID.GetType()));
            detail.TrackWfId = Convert.ToInt32(row.GetColumnValue("TRACK_WF_ID", detail.TrackWfId.GetType()));
            detail.IsActive = Convert.ToInt32(row.GetColumnValue("IS_ACTIVE", detail.IsActive.GetType()));
            return detail;
        }

        public static WorkflowStages GetWorkflowStagesObject(DataRow row)
        {
            WorkflowStages detail = new WorkflowStages();
            detail.StageID = Convert.ToInt32(row.GetColumnValue("WF_STAGE_ID", detail.WorkflowID.GetType()));
            detail.WorkflowID = Convert.ToInt32(row.GetColumnValue("WF_ID", detail.WorkflowID.GetType()));
            detail.Approver = CompanyDesignationObj(row); // Convert.ToInt32(row.GetColumnValue("WF_APPROVER", detail.Approver.GetType()));
            detail.Department = CompanyDepartmentsObj(row);
            detail.Order = Convert.ToInt32(row.GetColumnValue("WF_ORDER", detail.Order.GetType()));
            detail.Time = Convert.ToInt32(row.GetColumnValue("WF_TIME", detail.Time.GetType()));
            detail.ObjectID = Convert.ToInt32(row.GetColumnValue("U_DEPT_ID", detail.ObjectID.GetType()));
            detail.Approver.ObjectID = Convert.ToInt32(row.GetColumnValue("WF_APPROVER", detail.Approver.ObjectID.GetType()));
            detail.ApproverRange = Convert.ToInt32(row.GetColumnValue("WF_APPR_RANGE", detail.ApproverRange.GetType()));
            return detail;
        }

        //public static WorkflowTrack GetWorkflowTrackObject(DataRow row)
        //{
        //    WorkflowTrack detail = new WorkflowTrack();
        //    detail.TrackID = Convert.ToInt32(row.GetColumnValue("WF_TRACK_ID", detail.TrackID.GetType()));
        //    detail.Status = Convert.ToString(row.GetColumnValue("WF_STATUS", detail.Status.GetType()));
        //    detail.Order = Convert.ToInt32(row.GetColumnValue("WF_ORDER", detail.Order.GetType()));
        //    detail.ModuleID = Convert.ToInt32(row.GetColumnValue("MODULE_ID", detail.ModuleID.GetType()));
        //    detail.Comments = Convert.ToString(row.GetColumnValue("WF_COMMENTS", detail.Comments.GetType()));

        //    return detail;
        //}

        public static WorkflowTrack GetWorkflowTrackObject(DataRow row)
        {
            WorkflowTrack detail = new WorkflowTrack();
            detail.TrackID = Convert.ToInt32(row.GetColumnValue("WF_TRACK_ID", detail.TrackID.GetType()));
            detail.ApproverID = Convert.ToInt32(row.GetColumnValue("WF_APPROVER", detail.ApproverID.GetType()));
            detail.Status = Convert.ToString(row.GetColumnValue("WF_STATUS", detail.Status.GetType()));
            detail.Order = Convert.ToInt32(row.GetColumnValue("WF_ORDER", detail.Order.GetType()));
            detail.ModuleID = Convert.ToInt32(row.GetColumnValue("MODULE_ID", detail.ModuleID.GetType()));
            detail.ModuleID = Convert.ToInt32(row.GetColumnValue("MODULE_ID", detail.ModuleID.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("WF_COMMENTS", detail.Comments.GetType()));
            detail.CreatedBy = Convert.ToInt32(row.GetColumnValue("CREATED_BY", detail.CreatedBy.GetType()));
            detail.ModifiedBY = Convert.ToInt32(row.GetColumnValue("MODIFIED_BY", detail.CreatedBy.GetType()));
            detail.DateCreated = Convert.ToDateTime(row.GetColumnValue("CREATED", detail.DateCreated.GetType()));
            detail.DateModified = Convert.ToDateTime(row.GetColumnValue("MODIFIED", detail.DateModified.GetType()));
            detail.REJECTED_ORDER = Convert.ToInt32(row.GetColumnValue("REJECTED_ORDER", detail.REJECTED_ORDER.GetType()));

            detail.ApproverDetails = new User();

            detail.ApproverDetails.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.ApproverDetails.UserID.GetType()));
            detail.ApproverDetails.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.ApproverDetails.FirstName.GetType()));
            detail.ApproverDetails.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.ApproverDetails.LastName.GetType()));
            detail.ApproverDetails.PhoneNum = Convert.ToString(row.GetColumnValue("U_PHONE", detail.ApproverDetails.PhoneNum.GetType()));
            // detail.ApproverDetails.Email = Convert.ToString(row.GetColumnValue("U_EMAIL", detail.ApproverDetails.Email.GetType()));
            detail.ApproverDetails.ApprovedBy = Convert.ToString(row.GetColumnValue("APPROVED_BY", detail.ApproverDetails.ApprovedBy.GetType()));


            detail.Approver = CompanyDesignationObj(row);
            detail.Department = CompanyDepartmentsObj(row);

            detail.WorkflowDetails = GetWorkflowObject(row);

            return detail;
        }

        public static WorkflowAudit GetWorkflowAuditObject(DataRow row)
        {
            WorkflowAudit detail = new WorkflowAudit();
            detail.AuditID = Convert.ToInt32(row.GetColumnValue("WF_AUDIT_ID", detail.AuditID.GetType()));
            detail.TrackDetails.TrackID = Convert.ToInt32(row.GetColumnValue("WF_TRACK_ID", detail.TrackDetails.TrackID.GetType()));
            detail.Status = Convert.ToString(row.GetColumnValue("WF_STATUS", detail.Status.GetType()));
            detail.Level = Convert.ToInt32(row.GetColumnValue("WF_LEVEL", detail.Level.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("WF_COMMENTS", detail.Comments.GetType()));
            detail.CreatedBy = Convert.ToInt32(row.GetColumnValue("CREATED_BY", detail.CreatedBy.GetType()));
            detail.DateCreated = Convert.ToDateTime(row.GetColumnValue("CREATED", detail.DateCreated.GetType()));

            detail.TrackDetails = GetWorkflowTrackObject(row);
            return detail;
        }



        public static DataSet SaveWorkflowObject(Workflow workflow)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_WF_ID", workflow.WorkflowID);
            sd.Add("P_COMP_ID", workflow.CompanyID);
            sd.Add("P_WF_TITLE", workflow.WorkflowTitle);
            sd.Add("P_WF_DETAILS", workflow.WorkflowDetails);
            sd.Add("P_WF_MODULE", workflow.WorkflowModule);
            sd.Add("P_USER", workflow.ModifiedBY);
            sd.Add("P_DEPT_ID", workflow.DeptID);
            return DatabaseProvider.GetDatabaseProvider().SelectList("wf_SaveWorkflow", sd);
        }

        public static DataSet SaveWorkflowStageObject(WorkflowStages stage)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_WF_STAGE_ID", stage.StageID);
            sd.Add("P_WF_ID", stage.WorkflowID);
            sd.Add("P_WF_APPROVER", stage.Approver.ObjectID);
            sd.Add("P_WF_DEPT_ID", stage.Department.DeptID);
            sd.Add("P_WF_DESIG_ID", stage.Approver.DesigID);
            sd.Add("P_WF_ORDER", stage.Order);
            sd.Add("P_WF_TIME", stage.Time);
            sd.Add("P_USER", stage.ModifiedBY);
            sd.Add("P_APPROVER_RANGE", stage.ApproverRange);
            return DatabaseProvider.GetDatabaseProvider().SelectList("wf_SaveWorkflowStage", sd);
        }

        public static DataSet SaveWorkflowAuditObject(WorkflowAudit audit)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_WF_AUDIT_ID", audit.AuditID);
            sd.Add("P_WF_TRACK_ID", audit.TrackDetails.TrackID);
            sd.Add("P_WF_STATUS", audit.Status);
            sd.Add("P_WF_LEVEL", audit.Level);
            sd.Add("P_WF_COMMENTS", audit.Comments);
            sd.Add("P_USER", audit.ModifiedBY);
            return DatabaseProvider.GetDatabaseProvider().SelectList("wf_SaveWorkflowAudit", sd);
        }

        public static DataSet SaveWorkflowTrackObject(WorkflowTrack track)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_WF_TRACK_ID", track.TrackID);
            sd.Add("P_WF_ID", track.WorkflowDetails.WorkflowID);
            sd.Add("P_MODULE_ID", track.ModuleID);
            sd.Add("P_WF_APPROVER", track.ApproverID);
            sd.Add("P_WF_ORDER", track.Order);
            sd.Add("P_WF_STATUS", track.Status);
            sd.Add("P_WF_COMMENTS", track.Comments);
            sd.Add("P_USER", track.ModifiedBY);
            sd.Add("P_MODULE_NAME", track.ModuleName);
            return DatabaseProvider.GetDatabaseProvider().SelectList("wf_SaveWorkflowTrack", sd);
        }



        public static DataSet SaveWorkflowTrackEmailObject(WorkflowTrack track)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_USER", track.ModifiedBY);
            sd.Add("P_WF_TRACK_ID", track.TrackID);
            sd.Add("P_WF_STATUS", track.Status);
            sd.Add("P_MODULE_ID", track.ModuleID);
            return DatabaseProvider.GetDatabaseProvider().SelectList("wf_SaveWorkflowTrackEmail", sd);
        }

        public static DataSet AssignWorkflow(int wID, int moduleID, int user)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_WF_ID", wID);
            sd.Add("P_MODULE_ID", moduleID);
            sd.Add("P_USER", user);
            return DatabaseProvider.GetDatabaseProvider().SelectList("wf_AssignWorkflow", sd);
        }

        public static CompanyDepartments CompanyDepartmentsObj(DataRow row)
        {
            CompanyDepartments detail = new CompanyDepartments();

            detail.DeptID = Convert.ToInt32(row.GetColumnValue("DEPT_ID", detail.DeptID.GetType()));
            detail.CompID = Convert.ToInt32(row.GetColumnValue("COMP_ID", detail.CompID.GetType()));
            detail.DeptCode = Convert.ToString(row.GetColumnValue("DEPT_CODE", detail.DeptCode.GetType()));
            detail.DeptDesc = Convert.ToString(row.GetColumnValue("DEPT_DESC", detail.DeptDesc.GetType()));
            detail.DeptAdmin = Convert.ToInt32(row.GetColumnValue("DEPT_ADMIN", detail.DeptAdmin.GetType()));

            return detail;
        }


        public static CompanyDesignations CompanyDesignationObj(DataRow row)
        {
            CompanyDesignations detail = new CompanyDesignations();

            detail.DesigID = Convert.ToInt32(row.GetColumnValue("DESIG_ID", detail.DesigID.GetType()));
            detail.DesigCode = Convert.ToString(row.GetColumnValue("DESIG_CODE", detail.DesigCode.GetType()));
            detail.DesigDesc = Convert.ToString(row.GetColumnValue("DESIG_DESC", detail.DesigDesc.GetType()));

            return detail;
        }

        public static UserInfo GetUsersForWFEmailsObj(DataRow row)
        {
            UserInfo user = new UserInfo();

            user.UserID = row["U_ID"] != DBNull.Value ? row["U_ID"].ToString() : string.Empty;
            user.FirstName = row["U_FNAME"] != DBNull.Value ? row["U_FNAME"].ToString() : string.Empty;
            //user.FirstName = row["U_FNAME"] != DBNull.Value ? row["U_FNAME"].ToString() : string.Empty;
            user.LastName = row["U_LNAME"] != DBNull.Value ? row["U_LNAME"].ToString() : string.Empty;
            user.Email = row["U_EMAIL"] != DBNull.Value ? row["U_EMAIL"].ToString() : string.Empty;
            user.Username = row["LG_LOGIN"] != DBNull.Value ? row["LG_LOGIN"].ToString() : string.Empty;
            user.Username = row["LG_LOGIN"] != DBNull.Value ? row["LG_LOGIN"].ToString() : string.Empty;
            user.PhoneNum = row["U_PHONE"] != DBNull.Value ? row["U_PHONE"].ToString() : string.Empty;
            user.UserType = row["U_TYPE"] != DBNull.Value ? row["U_TYPE"].ToString() : string.Empty;

            return user;
        }
    }
}