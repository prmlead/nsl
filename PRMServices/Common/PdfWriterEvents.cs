﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Common
{
    class PdfWriterEvents : IPdfPageEvent
    {
        string watermarkText = string.Empty;
        float _xPosition = 300;
        float _yPosition = 400;

        public PdfWriterEvents(string watermark, float xPosition = 300, float yPosition = 400)
        {
            watermarkText = watermark;
            _xPosition = xPosition;
            _yPosition = yPosition;
        }

        public void OnOpenDocument(PdfWriter writer, Document document) {
            float fontSize = 80;
            float xPosition = _xPosition;
            float yPosition = _yPosition;
            float angle = 45;
            try
            {
                PdfContentByte under = writer.DirectContentUnder;
                PdfGState gs1 = new PdfGState();
                gs1.FillOpacity = 0.2f;
                under.SaveState();
                under.SetGState(gs1);
                BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
                under.BeginText();
                under.SetColorFill(new BaseColor(128, 128, 128));
                under.SetFontAndSize(baseFont, fontSize);
                under.ShowTextAligned(PdfContentByte.ALIGN_CENTER, watermarkText, xPosition, yPosition, angle);
                under.EndText();
                under.RestoreState();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        public void OnCloseDocument(PdfWriter writer, Document document) {
            float fontSize = 80;
            float xPosition = _xPosition;
            float yPosition = _yPosition;
            float angle = 45;
            try
            {
                PdfContentByte under = writer.DirectContentUnder;
                PdfGState gs1 = new PdfGState();
                gs1.FillOpacity = 0.2f;
                under.SaveState();
                under.SetGState(gs1);
                BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
                under.BeginText();
                under.SetColorFill(new BaseColor(128, 128, 128));
                under.SetFontAndSize(baseFont, fontSize);
                under.ShowTextAligned(PdfContentByte.ALIGN_CENTER, watermarkText, xPosition, yPosition, angle);
                under.EndText();
                under.RestoreState();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        public void OnStartPage(PdfWriter writer, Document document)
        {
            float fontSize = 80;
            float xPosition = _xPosition;
            float yPosition = _yPosition;
            float angle = 45;
            try
            {
                PdfContentByte under = writer.DirectContentUnder;
                PdfGState gs1 = new PdfGState();
                gs1.FillOpacity = 0.2f;
                under.SaveState();
                under.SetGState(gs1);
                BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
                under.BeginText();
                under.SetColorFill(new BaseColor(128, 128, 128));
                under.SetFontAndSize(baseFont, fontSize);
                under.ShowTextAligned(PdfContentByte.ALIGN_CENTER, watermarkText, xPosition, yPosition, angle);
                under.EndText();
                under.RestoreState();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        public void OnEndPage(PdfWriter writer, Document document) {
            //float fontSize = 80;
            //float xPosition = 300;
            //float yPosition = 400;
            //float angle = 45;
            //try
            //{
            //    PdfContentByte under = writer.DirectContent;
            //    BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
            //    under.BeginText();
            //    //under.SetColorFill(BaseColor.LIGHT_GRAY);
            //    under.SetFontAndSize(baseFont, fontSize);
            //    under.ShowTextAligned(PdfContentByte.ALIGN_CENTER, watermarkText, xPosition, yPosition, angle);
            //    under.EndText();
            //}
            //catch (Exception ex)
            //{
            //    Console.Error.WriteLine(ex.Message);
            //}
        }
        public void OnParagraph(PdfWriter writer, Document document, float paragraphPosition) { }
        public void OnParagraphEnd(PdfWriter writer, Document document, float paragraphPosition) { }
        public void OnChapter(PdfWriter writer, Document document, float paragraphPosition, Paragraph title) { }
        public void OnChapterEnd(PdfWriter writer, Document document, float paragraphPosition) { }
        public void OnSection(PdfWriter writer, Document document, float paragraphPosition, int depth, Paragraph title) { }
        public void OnSectionEnd(PdfWriter writer, Document document, float paragraphPosition) { }
        public void OnGenericTag(PdfWriter writer, Document document, Rectangle rect, String text) { }

        
    }
}