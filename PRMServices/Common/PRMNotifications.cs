﻿using System;
using System.Collections.Generic;
using PRMServices.Models;
using System.Net.Mail;
using System.ComponentModel;
using System.Xml;
using System.IO;

namespace PRMServices.Common
{
    public class PRMNotifications
    {
        public void AuctionStartNotification(int reqID, int userID, string sessionID, List<Attachment> vendorAttachments, string watchers)
        {
            PRMServices prm = new PRMServices();
            string ScreenName = "NEGOTIATION_START_TIME";
            
            Requirement req = prm.GetRequirementDataOffline(reqID, userID, sessionID);
            req.Module = ScreenName;
            req.StartTime = toLocal(req.StartTime);
            foreach (VendorDetails vendor in req.AuctionVendors)
            {
                string body = GenerateEmailBody("VendoremailForAuctionStartUpdate"); 
                UserInfo user = prm.GetUserNew(vendor.VendorID, sessionID);
                User altUser = prm.GetAlternateCommunications(reqID, vendor.VendorID, sessionID);
                body = String.Format(body, user.FirstName, user.LastName, req.RequirementNumber, req.StartTime, req.Title);
                prm.SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "Negotiation Start time for Req Number: " + req.RequirementNumber + " is " + req.StartTime, body, reqID, vendor.VendorID, req.Module, sessionID, null, vendorAttachments, req.StartTime, 15, "NEGOTATION_START", string.Empty).ConfigureAwait(false);
                string message = GenerateEmailBody("VendorsmsForAuctionStartUpdate");
                message = String.Format(message, user.FirstName, user.LastName, reqID, req.StartTime, req.Title);
                message = message.Replace("<br/>", "");
              //  prm.SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(message.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, vendor.VendorID,req.Module, sessionID).ConfigureAwait(false);
                PushNotifications push1 = new PushNotifications();
                push1.Message = message.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0];
                push1.MessageType = ScreenName;
                push1.RequirementID = req.RequirementID;
                prm.SendPushNotificationAsync(push1, user).ConfigureAwait(false);
            }

            if (req.Status != GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString()))
            {
                string body1 = GenerateEmailBody("CustomeremailForAuctionStartUpdate");
                UserInfo user1 = prm.GetUserNew(userID, sessionID);
                body1 = String.Format(body1, user1.FirstName, user1.LastName, req.RequirementNumber, req.StartTime, req.Title);
                prm.SendEmail(user1.Email + "," + user1.AltEmail, "Negotiation Start time has been updated for Req Number: " + req.RequirementNumber + " Title: " + req.Title, body1, reqID,Convert.ToInt32(user1.UserID), req.Module, sessionID, null, null, req.StartTime, 15, "NEGOTATION_START", watchers).ConfigureAwait(false);

                string message1 = GenerateEmailBody("CustomersmsForAuctionStartUpdate");
                message1 = String.Format(message1, user1.FirstName, user1.LastName, req.RequirementNumber, req.StartTime, req.Title);
                message1 = message1.Replace("<br/>", "");
               // prm.SendSMS(string.Empty, user1.PhoneNum + "," + user1.AltPhoneNum, System.Web.HttpUtility.UrlEncode(message1), reqID, Convert.ToInt32(user1.UserID), req.Module).ConfigureAwait(false);


                UserInfo superUser = prm.GetSuperUser(userID, sessionID);

                string bodySuper = GenerateEmailBody("CustomeremailForAuctionStartUpdate");
                bodySuper = String.Format(bodySuper, superUser.FirstName, superUser.LastName, req.RequirementNumber, req.StartTime, req.Title);

                string body2Super = GenerateEmailBody("CustomersmsForAuctionStartUpdate");
                body2Super = String.Format(body2Super, superUser.FirstName, superUser.LastName, req.RequirementNumber, req.StartTime, req.Title);
                body2Super = body2Super.Replace("<br/>", "");
                string subUserID = req.CustomerID.ToString();               
                string bodyTelegram = GenerateEmailBody("UpdateStartTimeTelegramsms");
                bodyTelegram = String.Format(bodyTelegram, req.CustomerCompanyName, req.CustFirstName, req.CustLastName, req.Title, req.Description, req.StartTime);
                bodyTelegram = bodyTelegram.Replace("<br/>", "");

                TelegramMsg tgMsg = new TelegramMsg();
                tgMsg.Message = "Negotiation Start time has been updated FOR THE REQ ID:" + req.RequirementNumber + (bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);
                tgMsg = prm.SendTelegramMsg(tgMsg);
            }
        }

        public void RequirementSaveNotifications(Requirement requirement)
        {

        }

        private string GetEnumDesc<T>(string value) where T : struct, IConvertible
        {
            var type = typeof(T);
            var memInfo = type.GetMember(value);
            var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            var description = ((DescriptionAttribute)attributes[0]).Description;
            return description;
        }

        public string GenerateEmailBody(string TemplateName)
        {
            string templateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
            string body = string.Empty;
            //body = Engine.Razor.RunCompile(File.ReadAllText(templateFolderPath + "/EmailTemplates/" + TemplateName), "Email", null, model);
            XmlDocument doc = new XmlDocument();
            doc.Load(templateFolderPath + "/EmailTemplates/EmailFormats.xml");
            XmlNode node = doc.DocumentElement.SelectSingleNode(TemplateName);
            body = node.InnerText;
            string footerName = "";
            if (TemplateName.ToLower().Contains("email"))
            {
                footerName = "EmailFooter";
            }
            else if (TemplateName.ToLower().Contains("sms"))
            {
                footerName = "SMSFooter";
            }
            else
            {
                footerName = "FooterXML";
            }


            if (footerName == "FooterXML")
            {

            }
            else
            {
                XmlNode footernode = doc.DocumentElement.SelectSingleNode(footerName);
                body += footernode.InnerText;
            }


            return body;
        }

        public void ForwardAuctionStartNotification(int reqID, int userID, string sessionID, List<Attachment> vendorAttachments)
        {
            PRMFwdReqService prm = new PRMFwdReqService();
            PRMServices prm1 = new PRMServices();
            string ScreenName = "UPDATE_STARTTIME";
            FwdRequirement req = prm.GetRequirementDataOfflinePrivate(reqID, userID, sessionID);
            foreach (VendorDetails vendor in req.AuctionVendors)
            {
                string body = GenerateForwardEmailBody("VendoremailForAuctionStartUpdate");
                UserInfo user = prm1.GetUserNew(vendor.VendorID, sessionID);
                User altUser = prm.GetAlternateCommunications(reqID, vendor.VendorID, sessionID);
                body = String.Format(body, user.FirstName, user.LastName, reqID, toLocal(req.StartTime), req.Title);
                prm1.SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "Negotiation Start time has been updated for RQID: " + reqID + " Title: " + req.Title, body, reqID, vendor.VendorID, req.Module, sessionID, null, vendorAttachments).ConfigureAwait(false);
                string message = GenerateForwardEmailBody("VendorsmsForAuctionStartUpdate");
                message = String.Format(message, user.FirstName, user.LastName, reqID, toLocal(req.StartTime), req.Title);
                message = message.Replace("<br/>", "");
               // prm1.SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(message.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, vendor.VendorID, req.Module, sessionID).ConfigureAwait(false);
                PushNotifications push1 = new PushNotifications();
                push1.Message = message.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0];
                push1.MessageType = ScreenName;
                push1.RequirementID = req.RequirementID;
                prm1.SendPushNotificationAsync(push1, user).ConfigureAwait(false);
            }

            if (req.Status != GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString()))
            {
                string body1 = GenerateForwardEmailBody("CustomeremailForAuctionStartUpdate");
                UserInfo user1 = prm1.GetUserNew(userID, sessionID);
                body1 = String.Format(body1, user1.FirstName, user1.LastName, reqID, toLocal(req.StartTime), req.Title);
                prm1.SendEmail(user1.Email + "," + user1.AltEmail, "Negotiation Start time has been updated for RQID: " + reqID + " Title: " + req.Title, body1, reqID, Convert.ToInt32(user1.UserID), req.Module, sessionID).ConfigureAwait(false);

                string message1 = GenerateForwardEmailBody("CustomersmsForAuctionStartUpdate");
                message1 = String.Format(message1, user1.FirstName, user1.LastName, reqID, toLocal(req.StartTime), req.Title);
                message1 = message1.Replace("<br/>", "");
               // prm1.SendSMS(string.Empty, user1.PhoneNum + "," + user1.AltPhoneNum, System.Web.HttpUtility.UrlEncode(message1), reqID, Convert.ToInt32(user1.UserID), req.Module).ConfigureAwait(false);


                UserInfo superUser = prm1.GetSuperUser(userID, sessionID);

                string bodySuper = GenerateForwardEmailBody("CustomeremailForAuctionStartUpdate");
                bodySuper = String.Format(bodySuper, superUser.FirstName, superUser.LastName, reqID, toLocal(req.StartTime), req.Title);

                string body2Super = GenerateForwardEmailBody("CustomersmsForAuctionStartUpdate");
                body2Super = String.Format(body2Super, superUser.FirstName, superUser.LastName, reqID, toLocal(req.StartTime), req.Title);
                body2Super = body2Super.Replace("<br/>", "");

                string subUserID = req.CustomerID.ToString();

                //if (superUser.UserID != subUserID)
                //{

                //    prm1.SendEmail(superUser.Email + "," + superUser.AltEmail, "Negotiation Start time has been updated for RQID: " + reqID + " Title: " + req.Title, bodySuper, reqID, req.CustomerID, req.Module, sessionID).ConfigureAwait(false);
                //    prm1.SendSMS(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2Super.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, req.CustomerID, sessionID).ConfigureAwait(false);
                //    PushNotifications push1 = new PushNotifications();
                //    push1.Message = body2Super.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0];
                //    push1.MessageType = ScreenName;
                //    push1.RequirementID = req.RequirementID;
                //    prm1.SendPushNotificationAsync(push1, superUser).ConfigureAwait(false);
                //}

                //Requirement req1 = GetRequirementDataOffline(reqID, userID, sessionID);

                string bodyTelegram = GenerateForwardEmailBody("UpdateStartTimeTelegramsms");
                bodyTelegram = String.Format(bodyTelegram, req.CustomerCompanyName, req.CustFirstName, req.CustLastName, req.Title, req.Description, toLocal(req.StartTime));
                bodyTelegram = bodyTelegram.Replace("<br/>", "");

                TelegramMsg tgMsg = new TelegramMsg();
                tgMsg.Message = "Negotiation Start time has been updated FOR THE REQ ID:" + reqID + (bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);
                tgMsg = prm1.SendTelegramMsg(tgMsg);
            }
        }

        public string GenerateForwardEmailBody(string TemplateName)
        {
            string templateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
            string body = string.Empty;
            //body = Engine.Razor.RunCompile(File.ReadAllText(templateFolderPath + "/EmailTemplates/" + TemplateName), "Email", null, model);
            XmlDocument doc = new XmlDocument();
            doc.Load(templateFolderPath + "/EmailTemplates/EmailForwardFormats.xml");
            XmlNode node = doc.DocumentElement.SelectSingleNode(TemplateName);
            body = node.InnerText;
            string footerName = "";
            if (TemplateName.ToLower().Contains("email"))
            {
                footerName = "EmailFooter";
            }
            else if (TemplateName.ToLower().Contains("sms"))
            {
                footerName = "SMSFooter";
            }
            else
            {
                footerName = "FooterXML";
            }


            if (footerName == "FooterXML")
            {

            }
            else
            {
                XmlNode footernode = doc.DocumentElement.SelectSingleNode(footerName);
                body += footernode.InnerText;
            }


            return body;
        }

        public static DateTime toLocal(DateTime? date)
        {
            DateTime convertedDate = DateTime.SpecifyKind(DateTime.Parse(date.ToString()), DateTimeKind.Utc);

            var kind = convertedDate.Kind;

            DateTime dt = convertedDate.ToLocalTime();

            return dt;
        }
    }
}