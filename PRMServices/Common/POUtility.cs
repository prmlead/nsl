﻿using System;
using System.Collections.Generic;
using System.Data;
using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices.Common
{
    public class POUtility
    {
        public static POVendor GetPOVendorObject(DataRow row)
        {
            POVendor detail = new POVendor();
            detail.ItemID = Convert.ToInt32(row.GetColumnValue("ITEM_ID", detail.ItemID.GetType()));
            detail.ProductIDorName = Convert.ToString(row.GetColumnValue("PROD_ID", detail.ProductIDorName.GetType()));
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.Vendor = new UserDetails();
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.Vendor.UserID.GetType()));
            detail.Vendor.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.Vendor.FirstName.GetType()));
            detail.Vendor.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.Vendor.LastName.GetType()));
            //detail.ExpectedDeliveryDate = Convert.ToDateTime(row.GetColumnValue("EXPECTED_DELIVERY_DATE", typeof(DateTime)));
            detail.ReqQuantity = Convert.ToDecimal(row.GetColumnValue("REQ_QUANTITY", detail.ReqQuantity.GetType()));
            detail.POQuantity = Convert.ToDecimal(row.GetColumnValue("PO_QUANTITY", detail.POQuantity.GetType()));
            detail.VendorPOQuantity = Convert.ToDecimal(row.GetColumnValue("VENDOR_PO_QUANTITY", detail.VendorPOQuantity.GetType()));
            detail.Price = Convert.ToDecimal(row.GetColumnValue("PO_PRICE", detail.Price.GetType()));
            detail.Price = Convert.ToDecimal(row.GetColumnValue("PO_PRICE", detail.Price.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("PO_COMMENTS", detail.Comments.GetType()));
            detail.Status = Convert.ToString(row.GetColumnValue("PO_STATUS", detail.Status.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.POLink = Convert.ToString(row.GetColumnValue("PO_LINK", detail.POLink.GetType()));
            detail.DeliveryAddress = Convert.ToString(row.GetColumnValue("DELIVERY_ADDR", detail.DeliveryAddress.GetType()));
            
            return detail;
        }
        
        public static POVendor GetDescPoObject(DataRow row)
        {
            POVendor detail = new POVendor();
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.Vendor = new UserDetails();
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("VENDOR_ID", detail.Vendor.UserID.GetType()));
            detail.VendorID = Convert.ToInt32(row.GetColumnValue("VENDOR_ID", detail.VendorID.GetType()));
            detail.ExpectedDeliveryDate = Convert.ToDateTime(row.GetColumnValue("EXPECTED_DELIVERY_DATE", detail.ExpectedDeliveryDate.GetType()));           
            detail.Price = Convert.ToDecimal(row.GetColumnValue("PO_PRICE", detail.Price.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("PO_COMMENTS", detail.Comments.GetType()));
            detail.Status = Convert.ToString(row.GetColumnValue("PO_STATUS", detail.Status.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.POLink = Convert.ToString(row.GetColumnValue("PO_LINK", detail.POLink.GetType()));
            detail.DeliveryAddress = Convert.ToString(row.GetColumnValue("DELIVERY_ADDR", detail.DeliveryAddress.GetType()));
            detail.Title = Convert.ToString(row.GetColumnValue("REQ_TITLE", detail.Title.GetType()));
            detail.IndentID = Convert.ToString(row.GetColumnValue("INDENT_ID", detail.Title.GetType()));

            return detail;
        }

        public static DataSet SaveDescPoInfoEntity(POVendor povendor)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_PO_ID", povendor.POID);
            sd.Add("P_REQ_ID", povendor.ReqID);
            sd.Add("P_VENDOR_ID", povendor.VendorID);
            sd.Add("P_EXPECTED_DELIVERY_DATE", povendor.ExpectedDeliveryDate);
            sd.Add("P_PO_PRICE", povendor.Price);
            sd.Add("P_PO_COMMENTS", povendor.Comments);
            sd.Add("P_PO_STATUS", povendor.Status);
            sd.Add("P_CREATED_BY", povendor.CreatedBy);
            sd.Add("P_MODIFIED_BY", povendor.ModifiedBY);
            sd.Add("P_PURCHASE_ORDER_ID", povendor.PurchaseID);
            sd.Add("P_PO_LINK", povendor.POLink);
            sd.Add("P_DELIVERY_ADDR", povendor.DeliveryAddress);
            sd.Add("P_INDENT_ID", povendor.IndentID);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("po_SaveDesPoInfo", sd);
            return ds;
        }
        
        public static DispatchTrack GetDescDispatchObject(DataRow row)
        {

            DispatchTrack detail = new DispatchTrack();

            detail.DTID = Convert.ToInt32(row.GetColumnValue("DT_ID", detail.DTID.GetType()));

            //detail.PoVendors = new POVendor();
            //detail.PoVendors.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.PoVendors.POID.GetType()));

            detail.DispatchDate = Convert.ToDateTime(row.GetColumnValue("DISPATCH_DATE", detail.DispatchDate.GetType()));
            detail.DispatchType = Convert.ToString(row.GetColumnValue("DISPATCH_TYPE", detail.DispatchType.GetType()));
            detail.DispatchComments = Convert.ToString(row.GetColumnValue("DISPATCH_COMMENTS", detail.DispatchComments.GetType()));

            detail.DispatchCode = Convert.ToString(row.GetColumnValue("DISPATCH_CODE", detail.DispatchCode.GetType()));

            detail.DispatchMode = Convert.ToString(row.GetColumnValue("DISPATCH_MODE", detail.DispatchMode.GetType()));
            detail.DeliveryTrackID = Convert.ToString(row.GetColumnValue("DELIVERY_TRACK_ID", detail.DeliveryTrackID.GetType()));
            detail.DispatchLink = Convert.ToString(row.GetColumnValue("DISPATCH_LINK", detail.DispatchLink.GetType()));

            detail.RecivedQuantity = Convert.ToDecimal(row.GetColumnValue("RECEIVED_QUANTITY", detail.RecivedQuantity.GetType()));
            detail.RecivedDate = Convert.ToDateTime(row.GetColumnValue("RECEIVED_DATE", detail.RecivedDate.GetType()));
            detail.RecivedComments = Convert.ToString(row.GetColumnValue("RECEIVED_COMMENTS", detail.RecivedComments.GetType()));
            detail.RecivedCode = Convert.ToString(row.GetColumnValue("RECEIVED_CODE", detail.RecivedCode.GetType()));
            detail.RecivedLink = Convert.ToInt32(row.GetColumnValue("RECEIVED_LINK", detail.RecivedLink.GetType()));
            detail.RecivedBy = Convert.ToString(row.GetColumnValue("RECEIVED_BY", detail.RecivedBy.GetType()));
            detail.Status = Convert.ToString(row.GetColumnValue("STATUS", detail.Status.GetType()));

            return detail;
        }

        public static DataSet SaveDesDispatchTrackObject(DispatchTrack dispatchtrack, POVendor povendor)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };           
            sd.Add("P_DT_ID", dispatchtrack.DTID);
            sd.Add("P_PO_ID", povendor.POID);
            sd.Add("P_DISPATCH_QUANTITY", dispatchtrack.DispatchQuantity);
            sd.Add("P_DISPATCH_DATE", null);
            sd.Add("P_DISPATCH_TYPE", dispatchtrack.DispatchType);
            sd.Add("P_DISPATCH_COMMENTS", dispatchtrack.DispatchComments);
            sd.Add("P_DISPATCH_CODE", dispatchtrack.DispatchCode);
            sd.Add("P_DISPATCH_MODE", dispatchtrack.DispatchMode);
            sd.Add("P_DELIVERY_TRACK_ID", dispatchtrack.DeliveryTrackID);
            sd.Add("P_DISPATCH_LINK", dispatchtrack.DispatchLink);
            sd.Add("P_RECEIVED_QUANTITY", dispatchtrack.RecivedQuantity);
            sd.Add("P_RECEIVED_DATE", null);
            sd.Add("P_RECEIVED_COMMENTS", dispatchtrack.RecivedComments);
            sd.Add("P_RECEIVED_CODE", dispatchtrack.RecivedCode);
            sd.Add("P_RECEIVED_LINK", dispatchtrack.RecivedLink);
            sd.Add("P_RECEIVED_BY", dispatchtrack.RecivedBy);
            sd.Add("P_STATUS", dispatchtrack.Status);
            sd.Add("P_CREATED_BY", dispatchtrack.CreatedBy);
            sd.Add("P_MODIFIED_BY", dispatchtrack.ModifiedBY);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("po_SaveDispatchTrack", sd);
            return ds;
        }
        
        public static DataSet SavePOInfoEntity(POVendor poVendor)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_PO_ID", poVendor.POID);
            sd.Add("P_REQ_ID", poVendor.ReqID);
            sd.Add("P_VENDOR_ID", poVendor.Vendor.UserID);
            sd.Add("P_ITEM_ID", poVendor.ItemID);
            sd.Add("P_VENDOR_PO_QUANTITY", poVendor.VendorPOQuantity);
            sd.Add("P_PO_QUANTITY", poVendor.POQuantity);
            poVendor.ExpectedDeliveryDate = DateTime.Now;
            sd.Add("P_EXPECTED_DELIVERY_DATE", poVendor.ExpectedDeliveryDate);
            sd.Add("P_PO_PRICE", poVendor.Price);
            sd.Add("P_PO_COMMENTS", poVendor.Comments);
            sd.Add("P_PO_STATUS", poVendor.Status);
            sd.Add("P_MODIFIED_BY", poVendor.ModifiedBY);
            sd.Add("P_CREATED_BY", poVendor.CreatedBy);
            sd.Add("P_PURCHASE_ORDER_ID", poVendor.PurchaseID);
            sd.Add("P_PO_LINK", poVendor.POLink);
            sd.Add("P_DELIVERY_ADDR", poVendor.DeliveryAddress);
            sd.Add("P_INDENT_ID", poVendor.IndentID);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("po_SavePoInfo", sd);

            return ds;
        }
        
        public static VendorDetails GetUserDetails(DataRow row)
        {
            VendorDetails detail = new VendorDetails();
            detail.VendorID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.VendorID.GetType()));
            detail.VendorName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.VendorName.GetType()));
            detail.VendorName += " " + Convert.ToString(row.GetColumnValue("U_LNAME", detail.VendorName.GetType()));
            detail.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", detail.CompanyName.GetType()));

            return detail;
        }
        
        public static UserDetails GetVendorPoObject(DataRow row)
        {
            UserDetails detail = new UserDetails();

            detail.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.UserID.GetType()));
            detail.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.FirstName.GetType()));
            detail.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.LastName.GetType()));            

            return detail;
        }

        public static Requirement GetVendorPoReqObject(DataRow row)
        {
            Requirement detail = new Requirement();

            detail.RequirementID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.RequirementID.GetType()));
            detail.CustomerID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.CustomerID.GetType()));
            detail.Title = Convert.ToString(row.GetColumnValue("REQ_TITLE", detail.Title.GetType()));
            detail.IsLockReq = Convert.ToInt32(row.GetColumnValue("LOCK_REQ", detail.IsLockReq.GetType()));
            detail.DeliveryLocation = Convert.ToString(row.GetColumnValue("REQ_DELIVERY_LOC", detail.DeliveryLocation.GetType()));
            var isContract = Convert.ToInt32(row.GetColumnValue("IS_CONTRACT", detail.IsContract.GetType()));
            detail.IsContract = isContract == 1 ? true : false;
            return detail;
        }

        public static POItems GetPoObject(DataRow row)
        {
            POItems detail = new POItems();

            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.POLink = Convert.ToString(row.GetColumnValue("PO_LINK", detail.POLink.GetType()));

            return detail;
        }

        public static DispatchTrack GetPaymentTrackObject(DataRow row)
        {
            DispatchTrack detail = new DispatchTrack();

            detail.DTID = Convert.ToInt32(row.GetColumnValue("DT_ID", detail.DTID.GetType()));
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.DispatchDate = Convert.ToDateTime(row.GetColumnValue("DISPATCH_DATE", detail.DispatchDate.GetType()));
            detail.DispatchCode = Convert.ToString(row.GetColumnValue("DISPATCH_CODE", detail.DispatchCode.GetType()));
            detail.DeliveryTrackID = Convert.ToString(row.GetColumnValue("DELIVERY_TRACK_ID", detail.DeliveryTrackID.GetType()));
            detail.RecivedDate = Convert.ToDateTime(row.GetColumnValue("RECEIVED_DATE", detail.RecivedDate.GetType()));
            detail.ReceivedQtyPrice = Convert.ToInt32(row.GetColumnValue("RECEIVED_QTY_PRICE", detail.ReceivedQtyPrice.GetType()));

            return detail;
        }

        public static PaymentInfo GetPaymentInfoObject(DataRow row)
        {
            PaymentInfo detail = new PaymentInfo();

            detail.PDID = Convert.ToInt32(row.GetColumnValue("PD_ID", detail.PDID.GetType()));
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.PaymentAmount = Convert.ToDecimal(row.GetColumnValue("PAYMENT_AMOUNT", detail.PaymentAmount.GetType()));
            detail.PaymentDate = Convert.ToDateTime(row.GetColumnValue("PAYMENT_DATE", detail.PaymentDate.GetType()));
            detail.PaymentComments = Convert.ToString(row.GetColumnValue("PAYMENT_COMMENTS", detail.PaymentComments.GetType()));
            detail.PaymentCode = Convert.ToString(row.GetColumnValue("PAYMENT_CODE", detail.PaymentCode.GetType()));
            detail.TransactionID = Convert.ToString(row.GetColumnValue("PAYMENT_TRANSACT_ID", detail.TransactionID.GetType()));
            detail.PaymentMode = Convert.ToString(row.GetColumnValue("PAYMENT_MODE", detail.PaymentMode.GetType()));
            detail.AckComments = Convert.ToString(row.GetColumnValue("RECEIVED_COMMENTS", detail.AckComments.GetType()));
           

            return detail;
        }

        public static POItems GetVendorPoItemsObject(DataRow row)
        {
            POItems detail = new POItems();            

            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.VendorID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.VendorID.GetType()));
            detail.ItemID = Convert.ToInt32(row.GetColumnValue("ITEM_ID", detail.ItemID.GetType()));

            detail.VendorPOQuantity = Convert.ToInt32(row.GetColumnValue("VENDOR_PO_QUANTITY", detail.VendorPOQuantity.GetType()));
            detail.ExpectedDeliveryDate = Convert.ToDateTime(row.GetColumnValue("EXPECTED_DELIVERY_DATE", detail.ExpectedDeliveryDate.GetType()));
            detail.POPrice = Convert.ToDecimal(row.GetColumnValue("PO_PRICE", detail.POPrice.GetType()));
            detail.VendorPrice = Convert.ToDecimal(row.GetColumnValue("VENDOR_PRICE", detail.VendorPrice.GetType()));

            detail.Comments = Convert.ToString(row.GetColumnValue("PO_COMMENTS", detail.Comments.GetType()));
            detail.Status = Convert.ToString(row.GetColumnValue("PO_STATUS", detail.Status.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.IndentID = Convert.ToString(row.GetColumnValue("INDENT_ID", detail.IndentID.GetType()));
            detail.POLink = Convert.ToString(row.GetColumnValue("PO_LINK", detail.POLink.GetType()));
            detail.DeliveryAddress = Convert.ToString(row.GetColumnValue("DELIVERY_ADDR", detail.DeliveryAddress.GetType()));
            detail.ReqQuantity = Convert.ToDecimal(row.GetColumnValue("REQ_QUANTITY", detail.ReqQuantity.GetType()));
            detail.POQuantity = Convert.ToDecimal(row.GetColumnValue("PO_QUANTITY", detail.POQuantity.GetType()));
            detail.ProductIDorName = Convert.ToString(row.GetColumnValue("PROD_ID", detail.IndentID.GetType()));
            detail.VendorName = Convert.ToString(row.GetColumnValue("VENDOR_NAME", detail.VendorName.GetType()));
            detail.VendorPhone = Convert.ToString(row.GetColumnValue("U_PHONE", detail.VendorPhone.GetType()));

            detail.CGst = Convert.ToDecimal(row.GetColumnValue("C_GST", detail.CGst.GetType()));
            detail.SGst = Convert.ToDecimal(row.GetColumnValue("S_GST", detail.SGst.GetType()));
            detail.IGst = Convert.ToDecimal(row.GetColumnValue("I_GST", detail.IGst.GetType()));
            detail.VendorTotalPrice = Convert.ToDecimal(row.GetColumnValue("REVICED_PRICE", detail.VendorTotalPrice.GetType()));
            detail.IsPOToVendor = Convert.ToInt32(row.GetColumnValue("PO_IS_SEND_TO_VENDOR", detail.IsPOToVendor.GetType()));
            detail.IsCoreProductCategory = Convert.ToInt32(row.GetColumnValue("IS_CORE_CATEGORY", detail.IsCoreProductCategory.GetType()));
            detail.CatalogProductId = Convert.ToInt32(row.GetColumnValue("PRODUCT_ID", detail.CatalogProductId.GetType()));
            detail.RemainingQuantity = Convert.ToDecimal(row.GetColumnValue("REMAINING_QTY", detail.RemainingQuantity.GetType()));
            detail.VendorComments = Convert.ToString(row.GetColumnValue("VENDOR_COMMENTS", detail.VendorComments.GetType()));
            detail.DELIVERY_DATE = Convert.ToDateTime(row.GetColumnValue("VENDOR_DELIVERY_DATE", detail.DELIVERY_DATE.GetType()));
            detail.IsSelected = false;
            if (detail.IsCoreProductCategory > 0)
            {
                if (detail.POID > 0)
                {
                    detail.IsSelected = true;
                }
                else
                {
                    detail.VendorPOQuantity = detail.RemainingQuantity;
                }
            }
            else
            {
                detail.VendorPOQuantity = 1;
                detail.IsSelected = true;
            }
            
            return detail;
        }

        public static DataSet SavePOItemEntity(POItems poitems, VendorPO vendorpo, Requirement req, UserDetails vendor)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_PO_ID", poitems.POID);
            sd.Add("P_REQ_ID", req.RequirementID);
            sd.Add("P_VENDOR_ID", vendor.UserID);
            sd.Add("P_ITEM_ID", poitems.ItemID);
            sd.Add("P_VENDOR_PO_QUANTITY", poitems.VendorPOQuantity);
            sd.Add("P_PO_QUANTITY", poitems.POQuantity);
            sd.Add("P_EXPECTED_DELIVERY_DATE", poitems.ExpectedDeliveryDate);
            sd.Add("P_PO_PRICE", poitems.POPrice);
            sd.Add("P_PO_COMMENTS", poitems.Comments);
            sd.Add("P_PO_STATUS", poitems.IsCoreProductCategory > 0 ? poitems.Status : "COMPLETE");
            sd.Add("P_MODIFIED_BY", poitems.ModifiedBY);
            sd.Add("P_CREATED_BY", poitems.CreatedBy);
            sd.Add("P_PURCHASE_ORDER_ID", poitems.PurchaseID);
            sd.Add("P_PO_LINK", vendorpo.POLink);
            sd.Add("P_DELIVERY_ADDR", poitems.DeliveryAddress);
            sd.Add("P_INDENT_ID", poitems.IndentID);
            sd.Add("P_PO_TOTAL_PRICE", poitems.PoTotalPrice);
            sd.Add("P_PO_IS_SEND_TO_VENDOR", poitems.IsPOToVendor);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("po_SavePoInfo", sd);
            return ds;
        }

        public static DispatchTrack GetDispatchTrackObject(DataRow row)
        {           
            DispatchTrack detail = new DispatchTrack();

            detail.DTID = Convert.ToInt32(row.GetColumnValue("DT_ID", detail.DTID.GetType()));
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.IndentID = Convert.ToString(row.GetColumnValue("INDENT_ID", detail.IndentID.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.DispatchQuantity = Convert.ToDecimal(row.GetColumnValue("DISPATCH_QUANTITY", detail.DispatchQuantity.GetType()));
            detail.DispatchDate = Convert.ToDateTime(row.GetColumnValue("DISPATCH_DATE", detail.DispatchDate.GetType()));
            detail.DispatchType = Convert.ToString(row.GetColumnValue("DISPATCH_TYPE", detail.DispatchType.GetType()));
            detail.DispatchComments = Convert.ToString(row.GetColumnValue("DISPATCH_COMMENTS", detail.DispatchComments.GetType()));
            detail.DispatchCode = Convert.ToString(row.GetColumnValue("DISPATCH_CODE", detail.DispatchCode.GetType()));
            detail.DispatchMode = Convert.ToString(row.GetColumnValue("DISPATCH_MODE", detail.DispatchMode.GetType()));
            detail.DeliveryTrackID = Convert.ToString(row.GetColumnValue("DELIVERY_TRACK_ID", detail.DeliveryTrackID.GetType()));
            detail.DispatchLink = Convert.ToString(row.GetColumnValue("DISPATCH_LINK", detail.DispatchLink.GetType()));

            detail.RecivedQuantity = Convert.ToDecimal(row.GetColumnValue("RECEIVED_QUANTITY", detail.RecivedQuantity.GetType()));
            detail.RecivedDate = Convert.ToDateTime(row.GetColumnValue("RECEIVED_DATE", detail.RecivedDate.GetType()));
            detail.RecivedComments = Convert.ToString(row.GetColumnValue("RECEIVED_COMMENTS", detail.RecivedComments.GetType()));
            detail.RecivedCode = Convert.ToString(row.GetColumnValue("RECEIVED_CODE", detail.RecivedCode.GetType()));
            detail.RecivedLink = Convert.ToInt32(row.GetColumnValue("RECEIVED_LINK", detail.RecivedLink.GetType()));
            detail.RecivedBy = Convert.ToString(row.GetColumnValue("RECEIVED_BY", detail.RecivedBy.GetType()));

            detail.ReturnQuantity = Convert.ToDecimal(row.GetColumnValue("RETURN_QUANTITY", detail.ReturnQuantity.GetType()));

            detail.ShipmentDate = Convert.ToDateTime(row.GetColumnValue("SHIPMENT_DATE", detail.ShipmentDate.GetType()));
            detail.DocumentDate = Convert.ToDateTime(row.GetColumnValue("DOCUMENT_DATE", detail.DocumentDate.GetType()));
            detail.CustomerBatch = Convert.ToString(row.GetColumnValue("CUSTOMER_BATCH", detail.CustomerBatch.GetType()));
            detail.CustomerLocation = Convert.ToString(row.GetColumnValue("CUSTOMER_LOCATION", detail.CustomerLocation.GetType()));
            detail.ShipmentFromLocation = Convert.ToString(row.GetColumnValue("SHIP_FROM_LOCATION", detail.ShipmentFromLocation.GetType()));
            detail.ShipToLocation = Convert.ToString(row.GetColumnValue("SHIP_TO_LOCATION", detail.ShipToLocation.GetType()));
            detail.UnloadingPoint = Convert.ToString(row.GetColumnValue("UNLOADING_POINT", detail.UnloadingPoint.GetType()));
            detail.UnloadingPoint = Convert.ToString(row.GetColumnValue("UNLOADING_POINT", detail.UnloadingPoint.GetType()));

            detail.ManufacDate = Convert.ToDateTime(row.GetColumnValue("MANUFAC_DATE", detail.ManufacDate.GetType()));
            detail.BestBeforeDate = Convert.ToDateTime(row.GetColumnValue("BEST_BEFORE_DATE", detail.BestBeforeDate.GetType()));
            detail.TotalWeight = Convert.ToString(row.GetColumnValue("TOTAL_WEIGHT", detail.TotalWeight.GetType()));
            detail.TotalVolume = Convert.ToString(row.GetColumnValue("TOTAL_VOLUME", detail.TotalVolume.GetType()));
            detail.FreightInvoice = Convert.ToString(row.GetColumnValue("FREIGHT_INVOICE", detail.FreightInvoice.GetType()));
            detail.FreightTotalInvoice = Convert.ToString(row.GetColumnValue("FREIGHT_TOTAL_INVOICE", detail.FreightTotalInvoice.GetType()));
            detail.FreightTax = Convert.ToString(row.GetColumnValue("FREIGHT_TAX", detail.FreightTax.GetType()));
            detail.InvoiceNumber = Convert.ToString(row.GetColumnValue("INVOICE_NUMBER", detail.InvoiceNumber.GetType()));
            detail.InvoiceAmount = Convert.ToString(row.GetColumnValue("INVOICE_AMOUNT", detail.InvoiceAmount.GetType()));
            detail.ShippedThrough = Convert.ToString(row.GetColumnValue("SHIPPED_THROUGH", detail.ShippedThrough.GetType()));

            detail.Status = Convert.ToString(row.GetColumnValue("STATUS", detail.Status.GetType()));

            return detail;
        }

        public static POItems GetDispatchPoItem(DataRow row)
        {
            POItems detail = new POItems();
            detail.DTID = Convert.ToInt32(row.GetColumnValue("DT_ID", detail.DTID.GetType()));
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.VendorID = Convert.ToInt32(row.GetColumnValue("VENDOR_ID", detail.VendorID.GetType()));
            detail.ItemID = Convert.ToInt32(row.GetColumnValue("ITEM_ID", detail.ItemID.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.VendorPOQuantity = Convert.ToDecimal(row.GetColumnValue("VENDOR_PO_QUANTITY", detail.VendorPOQuantity.GetType()));
            detail.IndentID = Convert.ToString(row.GetColumnValue("INDENT_ID", detail.IndentID.GetType()));
            detail.ProductIDorName = Convert.ToString(row.GetColumnValue("PROD_ID", detail.ProductIDorName.GetType()));
            detail.DispatchQuantity = Convert.ToDecimal(row.GetColumnValue("DISPATCH_QUANTITY", detail.DispatchQuantity.GetType()));
            detail.POPrice = Convert.ToDecimal(row.GetColumnValue("PO_PRICE", detail.POPrice.GetType()));
            detail.ExpectedDeliveryDate = Convert.ToDateTime(row.GetColumnValue("EXPECTED_DELIVERY_DATE", detail.ExpectedDeliveryDate.GetType()));
            detail.DeliveryAddress = Convert.ToString(row.GetColumnValue("DELIVERY_ADDR", detail.DeliveryAddress.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("PO_COMMENTS", detail.Comments.GetType()));
            detail.RecivedQuantity = Convert.ToDecimal(row.GetColumnValue("RECEIVED_QUANTITY", detail.RecivedQuantity.GetType()));
            detail.ReturnQuantity = Convert.ToDecimal(row.GetColumnValue("RETURN_QUANTITY", detail.ReturnQuantity.GetType()));
            detail.SumDispatchQuantity = Convert.ToDecimal(row.GetColumnValue("SUM_DISPATCH_QUANTITY", detail.SumDispatchQuantity.GetType()));
            detail.SumRecivedQuantity = Convert.ToDecimal(row.GetColumnValue("SUM_RECEIVED_QUANTITY", detail.SumRecivedQuantity.GetType()));
            detail.SumReturnQuantity = Convert.ToDecimal(row.GetColumnValue("SUM_RETURN_QUANTITY", detail.SumReturnQuantity.GetType()));
            detail.IsCoreProductCategory = Convert.ToInt32(row.GetColumnValue("IS_CORE_CATEGORY", detail.IsCoreProductCategory.GetType()));
            return detail;
        }

        public static DataSet SaveDispatchTrackObject(DispatchTrack dis, POItems dispatchtrack, string type)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_DT_ID", dispatchtrack.DTID);
            sd.Add("P_PO_ID", dispatchtrack.POID);
            sd.Add("P_PURCHASE_ORDER_ID", dis.POOrderId);
            sd.Add("P_DISPATCH_QUANTITY", dispatchtrack.DispatchQuantity);
            if (dis.DispatchDate.ToString().Contains("0001") || dis.DispatchDate.ToString().Contains("10000"))
            { dis.DispatchDate = DateTime.MaxValue; }
            sd.Add("P_DISPATCH_DATE", dis.DispatchDate);
            sd.Add("P_DISPATCH_TYPE", dis.DispatchType);
            sd.Add("P_DISPATCH_COMMENTS", dis.DispatchComments);
            sd.Add("P_DISPATCH_CODE", dis.DispatchCode);
            sd.Add("P_DISPATCH_MODE", dis.DispatchMode);
            sd.Add("P_DELIVERY_TRACK_ID", dis.DeliveryTrackID);
            sd.Add("P_DISPATCH_LINK", dis.DispatchLink);

            if (dis.RecivedDate.ToString().Contains("0001") || dis.RecivedDate.ToString().Contains("10000"))
            { dis.RecivedDate = DateTime.MaxValue; }

            sd.Add("P_RECEIVED_QUANTITY", dispatchtrack.RecivedQuantity);
            sd.Add("P_RECEIVED_DATE", dis.RecivedDate);
            sd.Add("P_RECEIVED_COMMENTS", dis.RecivedComments);
            sd.Add("P_RECEIVED_CODE", dis.RecivedCode);
            sd.Add("P_RECEIVED_LINK", dis.RecivedLink);
            sd.Add("P_RECEIVED_BY", dis.RecivedBy);
            sd.Add("P_STATUS", dis.Status);
            sd.Add("P_CREATED_BY", dis.CreatedBy);
            sd.Add("P_MODIFIED_BY", dis.ModifiedBY);
            sd.Add("P_RETURN_QUANTITY", dispatchtrack.ReturnQuantity);
            sd.Add("P_REQUEST_TYPE", type);
            if (dis.ShipmentDate.ToString().Contains("0001") || dis.ShipmentDate.ToString().Contains("10000"))
            { dis.ShipmentDate = DateTime.MaxValue; }
            if (dis.DocumentDate.ToString().Contains("0001") || dis.DocumentDate.ToString().Contains("10000"))
            { dis.DocumentDate = DateTime.MaxValue; }
        
            sd.Add("P_SHIPMENT_DATE", dis.ShipmentDate);
            sd.Add("P_DOCUMENT_DATE", dis.DocumentDate);
            sd.Add("P_CUSTOMER_BATCH", dis.CustomerBatch);
            sd.Add("P_SHIP_FROM_LOCATION", dis.ShipmentFromLocation);
            sd.Add("P_SHIP_TO_LOCATION", dis.ShipToLocation);
            sd.Add("P_CUSTOMER_LOCATION", dis.CustomerLocation);
            sd.Add("P_UNLOADING_POINT", dis.UnloadingPoint);
            sd.Add("P_SHIPPED_THROUGH", dis.ShippedThrough);

            if (dis.ManufacDate.ToString().Contains("0001") || dis.ManufacDate.ToString().Contains("10000"))
            { dis.ManufacDate = DateTime.MaxValue; }
            if (dis.BestBeforeDate.ToString().Contains("0001") || dis.BestBeforeDate.ToString().Contains("10000"))
            { dis.BestBeforeDate = DateTime.MaxValue; }
            sd.Add("P_MANUFAC_DATE", dis.ManufacDate);
            sd.Add("P_BEST_BEFORE_DATE", dis.BestBeforeDate);
            sd.Add("P_TOTAL_WEIGHT", dis.TotalWeight);
            sd.Add("P_TOTAL_VOLUME", dis.TotalVolume);
            sd.Add("P_FREIGHT_INVOICE", dis.FreightInvoice);
            sd.Add("P_FREIGHT_TOTAL_INVOICE", dis.FreightTotalInvoice);
            sd.Add("P_FREIGHT_TAX", dis.FreightTax);
            sd.Add("P_INVOICE_NUMBER", dis.InvoiceNumber);
            sd.Add("P_INVOICE_AMOUNT", dis.InvoiceAmount);

            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("po_SaveDispatchTrack", sd);
            return ds;
        }

        public static DataSet SavePaymentInfoObject(PaymentInfo paymentinfo)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_PD_ID", paymentinfo.PDID);
            sd.Add("P_PO_ID", paymentinfo.POID);
            sd.Add("P_PAYMENT_CODE", paymentinfo.PaymentCode);
            sd.Add("P_PAYMENT_AMOUNT", paymentinfo.PaymentAmount);
            sd.Add("P_PAYMENT_DATE", paymentinfo.PaymentDate);
            sd.Add("P_PAYMENT_COMMENTS", paymentinfo.PaymentComments);
            sd.Add("P_PAYMENT_MODE", paymentinfo.PaymentMode);
            sd.Add("P_PAYMENT_TRANSACT_ID", paymentinfo.TransactionID);
            sd.Add("P_RECEIVED_COMMENTS", paymentinfo.AckComments);
            sd.Add("P_PAYMENT_STATUS", paymentinfo.IsACK == true ? "ACCEPTED" : "PENDING");
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("po_SavePaymentInfo", sd);
            return ds;
        }

        public static UserDetails GetVendorDetails(DataRow row)
        {
            UserDetails detail = new UserDetails();
            detail.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.UserID.GetType()));
            detail.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.FirstName.GetType()));
            detail.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.LastName.GetType()));
            return detail;
        }

        public static Requirement GetRequirementDetails(DataRow row)
        {
            Requirement detail = new Requirement();

            detail.RequirementID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.RequirementID.GetType()));
            detail.CustomerID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.CustomerID.GetType()));
            detail.Title = Convert.ToString(row.GetColumnValue("REQ_TITLE", detail.Title.GetType()));

            detail.PostedUser = new User();
            detail.PostedUser.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.PostedUser.FirstName.GetType()));
            detail.PostedUser.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.PostedUser.LastName.GetType()));

            return detail;
        }
        
    }
}