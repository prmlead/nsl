﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.ServiceModel.Web;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using System.Net;

namespace PRMServices.Common
{
    public static class JwtAuthentication
    {
        public static string Realm { get; set; }
        public static bool AllowMultiple => false;

        public static bool Authenticate()
        {
            IncomingWebRequestContext request = WebOperationContext.Current.IncomingRequest;
            bool isValid = false;
            WebHeaderCollection headers = request.Headers;
            if (headers != null && headers.Count > 0 && headers.AllKeys.Contains("Authorization") && !string.IsNullOrWhiteSpace(headers["Authorization"]))
            {
                string tokenVal = headers["Authorization"];
                tokenVal = tokenVal.Replace("Bearer ", string.Empty);
                var principal = AuthenticateJwtToken(tokenVal);

                if (principal != null && principal.Result != null && principal.Result.Identity != null && principal.Result.Identity.IsAuthenticated)
                    isValid = true;
            }
            else
            {
                throw new Exception("Authentication, headers are in-valid.");
            }

            return isValid;
        }

        private static bool ValidateToken(string token, out string username)
        {
            username = null;

            var simplePrinciple = AuthenticateJwt.GetPrincipal(token);
            var identity = simplePrinciple?.Identity as ClaimsIdentity;

            if (identity == null)
                return false;

            if (!identity.IsAuthenticated)
                return false;

            var usernameClaim = identity.FindFirst(ClaimTypes.Name);
            username = usernameClaim?.Value;

            if (string.IsNullOrEmpty(username))
                return false;

            // More validate to check whether username exists in system

            return true;
        }

        public static Task<IPrincipal> AuthenticateJwtToken(string token)
        {
            string username;

            if (ValidateToken(token, out username))
            {
                // based on username to get more information from database in order to build local identity
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, username)
                    // Add more claims if needed: Roles, ...
                };

                var identity = new ClaimsIdentity(claims, "Jwt");
                IPrincipal user = new ClaimsPrincipal(identity);

                return Task.FromResult(user);
            }

            return Task.FromResult<IPrincipal>(null);
        }

        public static Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            Challenge(context);
            return Task.FromResult(0);
        }

        private static void Challenge(HttpAuthenticationChallengeContext context)
        {
            string parameter = null;

            if (!string.IsNullOrEmpty(Realm))
                parameter = "realm=\"" + Realm + "\"";

            context.ChallengeWith("Bearer", parameter);
        }
    }
}