﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('list-pendingPO', {
                    url: '/list-pendingPO',
                    templateUrl: 'PendingPO/views/list-pendingPO.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('list-pendingPOOverall', {
                    url: '/list-pendingPOOverall/:poID',
                    templateUrl: 'PendingPO/views/list-pendingPOOverall.html',
                    params: {
                        detailsObj: null
                    }
                })


                .state('vendorInvoices', {
                    url: '/vendorInvoices',
                    templateUrl: 'PendingPO/views/vendorInvoicesList.html'
                })

                .state('createInvoice', {
                    url: '/createInvoice/:poNumber/:asnCode/:invoiceID',
                    templateUrl: 'PendingPO/views/createInvoice.html'
                })

                .state('paymentTracking', {
                    url: '/paymentTracking',
                    templateUrl: 'PendingPO/views/paymenttracking.html'
                });

               
        }]);