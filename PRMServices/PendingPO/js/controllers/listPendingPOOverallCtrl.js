﻿prmApp
    .controller('listPendingPOOverallCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices", "poService",
        "PRMCustomFieldService","PRMPOService",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, poService, PRMCustomFieldService, PRMPOService) {
            $scope.poNumber = $stateParams.poID;
            $scope.fromDate = $stateParams.fromDate;
            $scope.toDate = $stateParams.toDate;
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();

            $scope.poOrderId = $stateParams.poOrderId;
            $scope.dCode = $stateParams.dCode;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            $scope.compID = userService.getUserCompanyId();
            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5; //Number of pager buttons to show

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/
            $scope.billedQty = 0;
            $scope.receivedQty = 0;
            $scope.rejectedQty = 0;
            $scope.remainingQty = 0;
            $scope.removeQty = 0;

            $scope.getPendingPOOverall = function () {
                $scope.billedQty = 0;
                $scope.receivedQty = 0;
                $scope.rejectedQty = 0;
                $scope.remainingQty = 0;
                $scope.removeQty = 0;

                var params = {
                    "compid": $scope.isCustomer ? $scope.compID : 0,
                    "uid": $scope.isCustomer ? 0 : $scope.userID,
                    "search": $scope.poNumber ,
                    "categoryid": '',
                    "productid": '',
                    "supplier": '',
                    "postatus": '',
                    "deliverystatus": '',
                    "plant": '',
                    "fromdate": '1970-01-01',
                    "todate": '2100-01-01',
                    "page": 0,
                    "pagesize": 10,
                    "sessionid": userService.getUserToken()

                };

                $scope.pageSizeTemp = (params.page + 1);

                PRMPOService.getPOScheduleList(params)
                    .then(function (response) {
                        $scope.pendingPOList = [];
                        $scope.filteredPendingPOsList = [];
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_CLOSED_DATE = item.PO_CLOSED_DATE ? moment(item.PO_CLOSED_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RELEASE_DATE = item.PO_RELEASE_DATE ? moment(item.PO_RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RECEIPT_DATE = item.PO_RECEIPT_DATE ? moment(item.PO_RECEIPT_DATE).format("DD-MM-YYYY") : '-';
                                $scope.pendingPOList.push(item);

                                //item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE).split(' ')[0];
                            });
                        }
                        $scope.filteredPendingPOsList = $scope.pendingPOList[0];
                    });


                var params1 = {
                    "ponumber": $scope.poNumber,
                    "moredetails": 1,
                    "forasn": false
                };
                PRMPOService.getPOScheduleItems(params1)
                    .then(function (response) {
                        $scope.pendingPOItems = response;
                        $scope.pendingPOItems.forEach(function (item, index) {
                            item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            item.PR_RELEASE_DATE = item.PR_RELEASE_DATE ? moment(item.PR_RELEASE_DATE).format("DD-MM-YYYY") : '-';
                            item.PR_DELIVERY_DATE = item.PR_DELIVERY_DATE ? moment(item.PR_DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            item.VALID_FROM = item.VALID_FROM ? moment(item.VALID_FROM).format("DD-MM-YYYY") : '-';
                            item.VALID_TO = item.VALID_TO ? moment(item.VALID_TO).format("DD-MM-YYYY") : '-';

                            $scope.billedQty += item.ORDER_QTY ;
                            $scope.receivedQty += item.RECEIVED_QTY ;
                            $scope.rejectedQty += item.REJECTED_QTY ;
                            $scope.remainingQty += item.REMAINING_QTY;
                            //$scope.removeQty += item.REMOVE_QTY;
                        });

                        //pendingPODetails.pendingPOItems = $scope.pendingPOItems;


                    });
                
            };

            $scope.getPendingPOOverall();


            $scope.getPOInvoiceDetails = function () {
                $scope.params = {
                    "ponumber": $scope.poNumber,
                    "sessionID": userService.getUserToken()
                };

                PRMPOService.getPOInvoiceDetails($scope.params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.multipleAttachments = [];
                            });
                        }
                        $scope.invoiceList = response;
                        //$scope.invoiceList.forEach(function (item) {
                        //    var attchArray = item.ATTACHMENTS.split(',');
                        //    attchArray.forEach(function (att, index) {

                        //        var fileUpload = {
                        //            fileStream: [],
                        //            fileName: '',
                        //            fileID: parseInt(att)
                        //        };

                        //        item.multipleAttachments.push(fileUpload);
                        //    });

                        //})


                    });
            };

            $scope.getPOInvoiceDetails()


            $scope.viewPO = function (grnNo) {
                var url = $state.href('list-ViewGRN', { "grnNo": grnNo, "isGRN": false });

                $window.open(url, '_blank');
            };

     
        }]);