﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class GRNDetails : ResponseAudit
    {
        [DataMember] [DataNames("GRN_NUMBER")] public string GRN_NUMBER { get; set; }
        [DataMember] [DataNames("GRN_LINE_ITEM")] public string GRN_LINE_ITEM { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("ITEM_RECEIVED_DATE")] public string ITEM_RECEIVED_DATE { get; set; }
        [DataMember] [DataNames("RECEIVED_QUANTITY")] public decimal RECEIVED_QUANTITY { get; set; }
        [DataMember] [DataNames("REJECTED_QUANTITY")] public decimal REJECTED_QUANTITY { get; set; }
        [DataMember] [DataNames("SUPPLIER_NAME")] public string SUPPLIER_NAME { get; set; }
        [DataMember] [DataNames("TOTAL_COUNT")] public int TOTAL_COUNT { get; set; }
    }
}