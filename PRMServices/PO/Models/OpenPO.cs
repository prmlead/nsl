﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class OpenPO : ResponseAudit
    {

        [DataMember] [DataNames("PLANT")] public string PLANT { get; set; }
        [DataMember] [DataNames("PLANT_NAME")] public string PLANT_NAME { get; set; }
        [DataMember] [DataNames("PURCHASING_GROUP")] public string PURCHASING_GROUP { get; set; }
        [DataMember] [DataNames("PURCHASER")] public string PURCHASER { get; set; }
        [DataMember] [DataNames("PURCHASING_DOCUMENT")] public string PURCHASING_DOCUMENT { get; set; }
        [DataMember] [DataNames("ITEM")] public string ITEM { get; set; }
        [DataMember] [DataNames("CONCAT")] public string CONCAT { get; set; }
        [DataMember] [DataNames("PO_DATE")] public DateTime? PO_DATE { get; set; }
        [DataMember] [DataNames("PO_MONTH")] public string PO_MONTH { get; set; }
        [DataMember] [DataNames("NAME_OF_SUPPLIER")] public string NAME_OF_SUPPLIER { get; set; }
        [DataMember] [DataNames("MATERIAL")] public string MATERIAL { get; set; }
        [DataMember] [DataNames("SHORT_TEXT")] public string SHORT_TEXT { get; set; }
        [DataMember] [DataNames("API_NAME")] public string API_NAME { get; set; }
        [DataMember] [DataNames("ORDER_UNIT")] public string ORDER_UNIT { get; set; }
        [DataMember] [DataNames("ORDER_QUANTITY")] public string ORDER_QUANTITY { get; set; }
        [DataMember] [DataNames("STILL_TO_BE_DELIVERED_QTY")] public string STILL_TO_BE_DELIVERED_QTY { get; set; }
        [DataMember] [DataNames("USER_ID")] public string USER_ID { get; set; }
        [DataMember] [DataNames("MULTIPLE_PR_TEXT")] public string MULTIPLE_PR_TEXT { get; set; }
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("PR_ITM_NO")] public string PR_ITM_NO { get; set; }
        [DataMember] [DataNames("PR_CREATE_DATE")] public DateTime? PR_CREATE_DATE { get; set; }
        [DataMember] [DataNames("PO_DELV_DATE")] public DateTime? PO_DELV_DATE { get; set; }
        [DataMember] [DataNames("IMPORT_LOCAL")] public string IMPORT_LOCAL { get; set; }
        [DataMember] [DataNames("LEAD_TIME")] public string LEAD_TIME { get; set; }
        [DataMember] [DataNames("IDEAL_DELIVERY_DATE")] public DateTime? IDEAL_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("DELIVERY_MONTH")] public string DELIVERY_MONTH { get; set; }
        [DataMember] [DataNames("PR_DELV_DATE")] public DateTime? PR_DELV_DATE { get; set; }

        //[DataMember] public string STATUS { get; set; }
        [DataMember] [DataNames("STATUS")] public string STATUS { get; set; }
        [DataMember] public string DAYS_TO_DELIVERY { get; set; }
        [DataMember] public string DELIVERY_RANGE { get; set; }
        [DataMember] public string IDEAL_DELIVERY_MONTH { get; set; }

        //Comments
        [DataMember]
        [DataNames("C_QUANTITY")]
        public decimal C_QUANTITY { get; set; }

        [DataMember]
        [DataNames("NEW_DELIVERY_DATE")]
        public DateTime? NEW_DELIVERY_DATE { get; set; }

        [DataMember]
        [DataNames("COMMENTS")]
        public string COMMENTS { get; set; }

        [DataMember]
        [DataNames("COMMENT_CREATED")]
        public DateTime? COMMENT_CREATED { get; set; }

        [DataMember]
        [DataNames("CATEGORY")]
        public string CATEGORY { get; set; }

        [DataMember]
        [DataNames("NWSTATUS")]
        public string NWSTATUS { get; set; }

        [DataMember]
        [DataNames("PR_QUAN")]
        public decimal PR_QUAN { get; set; }

        [DataMember]
        [DataNames("NEW_DELIVERY_DATE_COMM")]
        public string NEW_DELIVERY_DATE_COMM { get; set; }

        [DataMember]
        [DataNames("DELIVERED_LIST")]
        public List<OpenPO> DELIVERED_LIST { get; set; }

        [DataMember]
        [DataNames("POattachmentName")]
        public string POattachmentName { get; set; }

        [DataMember]
        [DataNames("POAttachment")]
        public byte[] POAttachment { get; set; }

        [DataMember]
        [DataNames("POAttachmentID")]
        public string POAttachmentID { get; set; }
    }
}