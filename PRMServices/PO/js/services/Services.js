prmApp.constant('PRMPOServicesDomain', 'po/svc/PRMPOModuleService.svc/REST/');
prmApp.service('PRMPOServices', ["PRMPOServicesDomain", "SAPIntegrationServicesDomain", "userService", "httpServices", "poDomain",
    function (PRMPOServicesDomain, SAPIntegrationServicesDomain, userService, httpServices, poDomain) {

        var PRMPOServices = this;
        //PRMPOServices.GetSeries = function (series, seriestype, deptid) {
        //    var url = PRMPOServicesDomain + 'getseries?series=' + series + '&seriestype=' + seriestype + '&compid=' + userService.getUserCompanyId() + '&deptid=' + deptid + '&sessionid=' + userService.getUserToken();
        //    return httpServices.get(url);
        //};


        PRMPOServices.SavePODetailsToSAP = function (params) {
            let url = SAPIntegrationServicesDomain + 'postpodata';
            return httpServices.post(url, params);
        };

        PRMPOServices.SavePODetailsToFTP = function (params) {
            let url = SAPIntegrationServicesDomain + 'sendpostpodata';
            return httpServices.post(url, params);
        };

        PRMPOServices.getPOGenerateDetails = function (params) {
            let url = poDomain + 'getpogeneratedetails?compid=' + params.compid + '&template=' + params.template
                + '&vendorid=' + params.vendorid + '&status=' + params.status + '&creator=' + params.creator
                + '&plant=' + params.plant
                + '&purchasecode=' + params.purchasecode + '&search=' + params.search
                + '&sessionid=' + params.sessionid + '&page=' + params.page
                + '&pagesize=' + params.pageSize;
            return httpServices.get(url);
        };

        PRMPOServices.getPOItems = function (params) {
            let url = poDomain + 'getpoitems?ponumber=' + params.ponumber + '&quotno=' + params.quotno + '&sessionid=' + params.sessionid;

            return httpServices.get(url);
        };

        PRMPOServices.getFilterValues = function (params) {
            let url = poDomain + 'getFilterValues?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOServices.getRequirementPO = function (params) {
            let url = poDomain + 'getrequirementpo?compid=' + params.compid + '&reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMPOServices.GetLastUpdatedDate = function (table) {
            let url = poDomain + 'lastupdatedate?table=' + table + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOServices.GetOpenPOReport = function (compid) {
            let url = poDomain + 'openporeport?compid=' + compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOServices.GetOpenPOShortageReport = function (compid, pono, plant, purchase, exclude) {
            let url = poDomain + 'getopenposhortagereport?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&pono=' + pono + '&sessionid=' + userService.getUserToken() + '&exclude=' + exclude;
            return httpServices.get(url);
        };

        PRMPOServices.GetOpenPRShortageReport = function (compid, plant, purchase, exclusion) {
            let url = poDomain + 'getopenprshortagereport?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken() + '&exclusion=' + exclusion;
            return httpServices.get(url);
        };

        PRMPOServices.GetPoComments = function (pono, item, type) {
            let url = poDomain + 'openpocomments?pono=' + pono + '&itemno=' + item + '&type=' + type + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOServices.UpdatePoStatus = function (params) {
            var url = poDomain + 'updatepostatus';
            return httpServices.post(url, params);
        };

        PRMPOServices.SavePoComments = function (params) {
            var url = poDomain + 'savepocomments';
            return httpServices.post(url, params);
        };

        PRMPOServices.GetSapAccess = function (userid) {
            let url = poDomain + 'sapaccess?userid=' + userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOServices.GetOpenPO = function (compid, pono, plant, purchase, exclude) {
            let url = poDomain + 'openpo?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&pono=' + pono + '&sessionid=' + userService.getUserToken() + '&exclude=' + exclude;
            return httpServices.get(url);
        };

        PRMPOServices.GetOpenPOPivot = function (compid, plant, purchase) {
            let url = poDomain + 'openpopivot?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOServices.getSplitPODetails = function (params) {
            let url = poDomain + 'getSplitPODetails?compid=' + params.compid + '&reqid=' + params.reqid + '&qcsId=' + params.qcsId + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        return PRMPOServices;

}]);