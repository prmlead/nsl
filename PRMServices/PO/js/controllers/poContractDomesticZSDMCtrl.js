﻿prmApp
    .controller('poContractDomesticZSDMCtrl', ["$state", "$stateParams", "$scope", "$rootScope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "catalogService", "PRMPRServices", "PRMCustomFieldService", "$modal", "PRMPOServices", 
        function ($state, $stateParams, $scope, $rootScope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, catalogService, PRMPRServices, PRMCustomFieldService, $modal, PRMPOServices) {

            $scope.currentUserId = +userService.getUserId();
            $scope.isCustomer = $scope.customerType === "CUSTOMER" ? true : false;
            $scope.currentUserSessionId = userService.getUserToken();
            $scope.currentUsercompanyId = userService.getUserCompanyId();
            $scope.currentUserName = userService.getFirstname() + ' ' + userService.getLastname();
            $scope.currentUserSapId = userService.getSAPUserId();
            $scope.contractDetails = $stateParams.contractDetails;
            $scope.vendorAssignments = $stateParams.detailsObj;
            $scope.productDetails = $stateParams.productDetails || {};
            $scope.poRAWJSON = $stateParams.poRawJSON;
            $scope.prDetails = $stateParams.prDetails;
            $scope.prDetailsItems = [];
            console.log($scope.contractDetails);
            console.log($scope.prDetails);
            let poRoundDigits = 2;
            $scope.POServiceStatusText = '';
            $scope.documentType = $stateParams.templateName;
            $scope.selectedVendors = [];
            $scope.currentUIVendor = {};
            $scope.currentUIPlant = {};
            $scope.formDetails = {};
            $scope.formDetails.items = [];
            

            $scope.formDetails.MEMORY_TYPE = 'X';
            $scope.formDetails.PR_NUMBER = '';
            $scope.formDetails.PO_CREATOR = $scope.currentUserSapId;
            $scope.formDetails.INCO_TERMS1 = '';
            $scope.formDetails.INCO_TERMS2 = '';
            $scope.formDetails.DOC_TYPE = $scope.documentType;
            $scope.poPlants = [];

            if ($scope.contractDetails && $scope.contractDetails.plantsArray && $scope.contractDetails.plantsArray.length > 0) {
                $scope.contractDetails.plantsArray.forEach(function (item, index) {
                    $scope.poPlants.push({
                        PLANT_CODE: item.plantCode,
                        PLANT_NAME: item.plantName
                    });
                });
            }

            $scope.currentUIPlant = $scope.poPlants[0];

            $scope.quantityError = false;
            var SelectedVendorDetails;
            $scope.companyConfigList = [];
            $scope.COMPANY_INCO_TERMS = [];
            auctionsService.GetCompanyConfiguration($scope.currentUsercompanyId, 'INCO', userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyConfigList = unitResponse;
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey === 'INCO') {
                            $scope.COMPANY_INCO_TERMS.push(item);
                        }
                    });
                });

            //if ($scope.contractDetails && $scope.contractDetails.contractTable && $scope.contractDetails.contractTable.length > 0) {
            //    $scope.contractDetails.contractTable.forEach(function (item, index) {
            //        item.contactsArray.forEach(function (item1, index) {
            //            if (item1.isSelected) {
            //                $scope.selectedVendors.push({
            //                    'vendorId': item1.vendorId, 'vendorCompany': item1.companyName,
            //                    vendorObj: { 'vendorId': item1.vendorId, 'vendorCompany': item1.companyName, 'vendorCode': item1.selectedVendorCode }
            //                });
            //            }
            //        });
            //    });

            //    $scope.currentUIVendor = $scope.selectedVendors[0];
            //}

            if ($scope.vendorAssignments && $scope.vendorAssignments.length > 0) {
                $scope.vendorAssignments.forEach(function (assignment, index) {
                    let temp = _.filter($scope.selectedVendors, function (vendorObj) {
                        return vendorObj.vendorId === assignment.vendorID;
                    });

                    if (!temp || temp.length <= 0) {
                        $scope.selectedVendors.push({ 'vendorId': assignment.vendorID, 'vendorCompany': assignment.vendorName, vendorObj: assignment });
                    }
                });
            }

            if ($scope.prDetails && $scope.prDetails.length > 0) {
                $scope.formDetails.PURCHASE_GROUP_CODE = $scope.prDetails[0].PURCHASE_GROUP_CODE ? $scope.prDetails[0].PURCHASE_GROUP_CODE : '';
                $scope.prDetails.forEach(function (prObj, index) {
                    prObj.PRItems.forEach(function (pritem, index) {
                        pritem.PLANT_CODE = prObj.PLANT;
                        pritem.PR_NUMBER = prObj.PR_NUMBER;
                        $scope.prDetailsItems.push(pritem);
                        if (!$scope.formDetails.PURCHASE_GROUP_CODE && pritem.ItemPRS && pritem.ItemPRS.length > 0) {
                            $scope.formDetails.PURCHASE_GROUP_CODE = pritem.ItemPRS[0].PURCHASE_GROUP_CODE ? pritem.ItemPRS[0].PURCHASE_GROUP_CODE : pritem.ItemPRS[0].PURCHASE_GROUP_NAME;
                        }
                    });
                });


                $scope.formDetails.GMP_TYPE = $scope.prDetails[0].isGMP ? true : false;
                $scope.formDetails.GMP_TYPE_HIDE = $scope.prDetails[0].isGMP ? 'GMP' : 'Non GMP';
            }

            $scope.precisionRound = function (number) {
                var factor = Math.pow(10, poRoundDigits);
                return Math.round(number * factor) / factor;
            };

            $scope.deleteItem = function (item) {
                item.isDeleted = true;
            };

            $scope.cancel = function () {
                $window.history.back();
            };

            $scope.filterSelectionChange = function () {
                if ($scope.currentUIVendor && $scope.currentUIVendor.vendorId && $scope.currentUIPlant && $scope.currentUIPlant.PLANT_CODE) {
                    $scope.formDetails.items = [];
                    $scope.formDetails.VENDOR_COMPANY = $scope.currentUIVendor.vendorCompany;
                    $scope.formDetails.VENDOR_ID = $scope.currentUIVendor.vendorId;
                    $scope.formDetails.VENDOR_QUOTATION = $stateParams.quoteLink ? $stateParams.quoteLink : $scope.currentUIVendor.vendorId;
                    $scope.formDetails.VENDOR_QUOTATION1 = 'CON-' + $scope.currentUIVendor.vendorId;
                    $scope.formDetails.INCO_TERMS1 = '';
                    $scope.formDetails.INCO_TERMS2 = '';

                    let totalVendorItems = 1;
                    let packageTax = 0;
                    let packageValWithTax = 0;
                    let packageValWithOutTax = 0;

                    let freightTax = 0;
                    let freightValWithTax = 0;
                    let freightValWithOutTax = 0;

                    let miscTax = 0;
                    let miscValWithTax = 0;
                    let miscValWithOutTax = 0;


                    //Packing & Forwarding charges
                    var packaging = null;
                    if (packaging && packaging.length > 0) {
                        packageTax = packaging[0].iGst ? packaging[0].iGst : (packaging[0].sGst + packaging[0].cGst);
                        packageValWithTax = packaging[0].itemPrice;
                        packageValWithOutTax = packaging[0].unitPrice;
                    }

                    //Freight charges
                    var freightCharges = null;
                    if (freightCharges && freightCharges.length > 0) {
                        freightTax = freightCharges[0].iGst ? freightCharges[0].iGst : (freightCharges[0].sGst + freightCharges[0].cGst);
                        freightValWithTax = freightCharges[0].itemPrice;
                        freightValWithOutTax = freightCharges[0].unitPrice;
                    }

                    //Miscellenous
                    var miscellenous = null;
                    if (miscellenous && miscellenous.length > 0) {
                        miscTax = miscellenous[0].iGst ? miscellenous[0].iGst : (miscellenous[0].sGst + miscellenous[0].cGst);
                        miscValWithTax = miscellenous[0].itemPrice;
                        miscValWithOutTax = miscellenous[0].unitPrice;
                    }

                    $scope.formDetails.PO_CURRENCY = '';
                    $scope.formDetails.VENDOR_NAME = $scope.currentUIVendor.vendorObj.vendorName;
                    $scope.formDetails.VENDOR_CODE = $scope.currentUIVendor.vendorObj.vendorCode;

                    //Items Populate:
                    let vendorItems = _.filter($scope.vendorAssignments, function (assignment) {
                        return assignment.vendorID === $scope.currentUIVendor.vendorId;
                    });

                    if (vendorItems && vendorItems.length > 0) {
                        totalVendorItems = vendorItems.length;
                        vendorItems.forEach(function (vendorItem, index) {
                            let poItemObj = {
                                MPN_CODE: '',
                                Deliver_SCHEDULE: ''
                            };

                            poItemObj.PRODUCT_ID = vendorItem.itemID;
                            poItemObj.CONTRACT_NUMBER = vendorItem.contractNumber;
                            let prItemObj = _.filter($scope.prDetailsItems, function (prItem) {
                                return prItem.PRODUCT_ID === vendorItem.itemID && ((prItem.PLANT_CODE || '') === $scope.currentUIPlant.PLANT_CODE || prItem.PlantCodeArr.includes($scope.currentUIPlant.PLANT_CODE));
                            });

                            poItemObj.PRODUCT_ID = prItemObj[0].PRODUCT_ID;
                            poItemObj.DISCOUNT_VALUE = 0;
                            poItemObj.PO_NETPRICE = parseFloat(+vendorItem.totalPrice);
                            poItemObj.PO_PRICE = parseFloat(+vendorItem.totalPrice);
                            poItemObj.NET_PRICE_PUR_DOC = $scope.precisionRound(parseFloat(+vendorItem.totalPrice));

                            poItemObj.QUANTITY = parseFloat(+vendorItem.assignedQty);
                            
                            poItemObj.PRICE_UNIT = parseFloat(+vendorItem.assignedPrice);

                            poItemObj.PLANT = $scope.currentUIPlant.PLANT_CODE;
                            if (prItemObj && prItemObj.length > 0) {
                                poItemObj.ORDER_PRICE_UNIT = prItemObj[0].UNITS;
                                poItemObj.PR_NUMBER = _.map(prItemObj, 'PR_NUMBER').join();
                                if (!poItemObj.PR_NUMBER) {
                                    poItemObj.PR_NUMBER = prItemObj[0].PRNumbers;
                                }
                                poItemObj.MATERIAL_CODE = prItemObj[0].ITEM_CODE.replace(/^0+/, '');
                                poItemObj.ITEM_OF_REQUESITION = _.map(prItemObj, 'ITEM_NUM').join();
                                poItemObj.MATERIAL_TYPE = prItemObj[0].MATERIAL_TYPE;
                                poItemObj.SHORT_TEXT = prItemObj[0].SHORT_TEXT;
                                //poItemObj.PLANT = $scope.contractDetails.plantCode;
                                poItemObj.PO_UOM = prItemObj[0].UNITS;
                            }
                            
                            if (!poItemObj.QUANTITY) {
                                poItemObj.quantityError = true;
                            }

                            //Packing & Forwarding charges
                            packaging = null;
                            if (packaging && packaging.length > 0) {
                                poItemObj.PACK_FORWARD_PER = packageTax; //parseFloat((packageTax / totalVendorItems).toFixed(2));
                                poItemObj.PACK_VALUE = parseFloat((packageValWithTax / totalVendorItems).toFixed(2));
                            }

                            //Freight charges
                            freightCharges = null;
                            if (freightCharges && freightCharges.length > 0) {
                                poItemObj.FREIGHT_TAXABLE_PER = freightTax;//parseFloat((freightTax / totalVendorItems).toFixed(2));
                                poItemObj.FREIGHT_VALUE = parseFloat((freightValWithTax / totalVendorItems).toFixed(2));
                                poItemObj.FREIGHT_NON_TAX_PER = parseFloat((freightValWithOutTax / totalVendorItems).toFixed(2));
                            }

                            //Miscellenous
                            miscellenous = null;
                            if (miscellenous && miscellenous.length > 0) {
                                poItemObj.MISC_TAXABLE_PER = miscTax; //parseFloat(( miscTax / totalVendorItems).toFixed(2));
                                poItemObj.MISC_TAXABLE_VAL = parseFloat((miscValWithTax / totalVendorItems).toFixed(2));
                                poItemObj.MISC_VALUE_NONTAX = parseFloat((miscValWithOutTax / totalVendorItems).toFixed(2));
                            }

                            userService.getProfileDetails({ "userid": $scope.formDetails.VENDOR_ID, "sessionid": $scope.currentUserSessionId })
                                .then(function (response) {
                                    if (response) {
                                        SelectedVendorDetails = response;
                                        if (SelectedVendorDetails) {
                                            $scope.formDetails.PO_CURRENCY = SelectedVendorDetails.currency;
                                            $scope.formDetails.PAYMENT_TERMS = SelectedVendorDetails.paymentTermDesc;
                                            $scope.formDetails.PAYMENT_TERMS_HIDE = SelectedVendorDetails.paymentTermCode;
                                            $scope.formDetails.INCO_TERMS1 = SelectedVendorDetails.incoTerm;
                                            $scope.COMPANY_INCO_TERMS.forEach(function (term, idx) {
                                                if (term.configValue == $scope.formDetails.INCO_TERMS1) {
                                                    $scope.formDetails.INCO_TERMS2 = term.configText;
                                                }
                                            });
                                        }
                                    }
                                });

                            if (poItemObj.PR_NUMBER) {
                                $scope.formDetails.items.push(poItemObj);
                            }
                        });
                    }
                }
            };
        
            $scope.submitPOToSAP = function (isDraft) {
                let sapDataList = [];
                $scope.POServiceStatusText = '';
                if ($scope.formDetails && $scope.formDetails.items && $scope.formDetails.items.length > 0) {
                    $scope.formDetails.items.forEach(function (poItem, index) {
                        if (!poItem.isDeleted) {
                            let PRNumberList = [];
                            let PRItemListList = [];
                            let counter = 0;
                            if (poItem.PR_NUMBER.includes(',')) {
                                PRNumberList = poItem.PR_NUMBER.split(',');
                                PRItemListList = poItem.ITEM_OF_REQUESITION.split(',');
                            }
                            else {
                                PRNumberList.push(poItem.PR_NUMBER);
                                PRItemListList.push(poItem.ITEM_OF_REQUESITION);
                            }
                            
                            PRNumberList.forEach(function (prNumber, index) {
                                if (!poItem.QUANTITY) {
                                    poItem.quantityError = true;
                                    return;
                                } else {
                                    poItem.quantityError = false;
                                }

                                sapDataList.push({
                                    COMP_ID: $scope.currentUsercompanyId,
                                    REQ_ID: $scope.reqId,
                                    CONTRACT_NUMBER: poItem.CONTRACT_NUMBER,
                                    QCS_ID: $scope.qcsId,
                                    PO_TEMPLATE: $scope.documentType,
                                    LIFNR: $scope.formDetails.VENDOR_CODE,
                                    BSART: $scope.formDetails.DOC_TYPE,
                                    BUKRS: '',
                                    EKORG: '',
                                    ZTERM: $scope.formDetails.PAYMENT_TERMS_HIDE,
                                    ERNAM: $scope.formDetails.PO_CREATOR,
                                    INCO1: $scope.formDetails.INCO_TERMS1,
                                    INCO2: $scope.formDetails.INCO_TERMS2,
                                    EKGRP: $scope.formDetails.PURCHASE_GROUP_CODE,
                                    WAERS: $scope.formDetails.PO_CURRENCY,
                                    MEMORYTYPE: $scope.formDetails.MEMORY_TYPE,
                                    DELIVERY_MODE: $scope.formDetails.Delivery_MODE, //PENDING
                                    ITM_SP_INST: $scope.formDetails.ITEM_SPECIAL_INST,
                                    ZZQANO: $scope.detectLinks($stateParams.quoteLink),
                                    ZZBONGING: $scope.formDetails.ZZBONGING,
                                    ZZMAP_NON_GMP: $scope.formDetails.GMP_TYPE_HIDE,
                                    VENDOR_ID: $scope.formDetails.VENDOR_ID,
                                    INSURANCE_NUMBER: $scope.formDetails.INSURANCE_NUMBER,
                                    PENALITY_PER_WEEK: $scope.formDetails.PENALITY_PER_WEEK,
                                    NUMBER_WEEKS: $scope.formDetails.NUMBER_WEEKS,
                                    MAX_PENALITY: $scope.formDetails.MAX_PENALITY,
                                    MAX_WEEKS: $scope.formDetails.MAX_WEEKS,
                                    EINDT: poItem.Deliver_SCHEDULE,
                                    //ITM_SP_INST: poItem.ITEM_SPECIAL_INST,
                                    WERKS: poItem.PLANT,
                                    MTART: poItem.MATERIAL_TYPE,
                                    MATNR: poItem.MATERIAL_CODE,
                                    EMATN: poItem.MPN_CODE,
                                    BANFN: prNumber, //poItem.PR_NUMBER,
                                    BNFPO: PRItemListList[counter], //poItem.ITEM_OF_REQUESITION,
                                    MEINS: poItem.PO_UOM,
                                    BPRME: poItem.ORDER_PRICE_UNIT, //PENDING/DOUBT
                                    PEINH: poItem.PRICE_UNIT,
                                    PEINHFieldSpecified: (poItem.PACK_SIZE ? parseFloat(poItem.PACK_SIZE) : 0) ? 1 : 0,
                                    IDNLF: poItem.Vendor_CATALOG, //PENDING
                                    KONNR: '', //PENDING
                                    KTPNR: '', //PENDING
                                    MWSKZ: poItem.TAX_CODE,
                                    PBXX: poItem.PO_PRICE ? $scope.precisionRound(parseFloat(poItem.PO_PRICE)) : 0,
                                    PBXXFieldSpecified: (poItem.PO_PRICE ? parseFloat(poItem.PO_PRICE) : 0) ? 1 : 0,
                                    RB00: poItem.DISCOUNT_VALUE ? $scope.precisionRound(poItem.DISCOUNT_VALUE) : 0,
                                    RB00FieldSpecified: (poItem.DISCOUNT_VALUE ? parseFloat(poItem.DISCOUNT_VALUE) : 0) ? 1 : 0,
                                    P_26F1: poItem.PACK_FORWARD_PER ? parseFloat(poItem.PACK_FORWARD_PER) : 0,
                                    P_26F1FieldSpecified: (poItem.PACK_FORWARD_PER ? parseFloat(poItem.PACK_FORWARD_PER) : 0) ? 1 : 0,
                                    P_26F2: poItem.PACK_VALUE ? parseFloat(poItem.PACK_VALUE) : 0,
                                    P_26F2FieldSpecified: (poItem.PACK_VALUE ? parseFloat(poItem.PACK_VALUE) : 0) ? 1 : 0,
                                    FRA2: poItem.FRIEGHT_NON_TAX_PER ? parseFloat(poItem.FRIEGHT_NON_TAX_PER) : 0,
                                    FRA2FieldSpecified: (poItem.FRIEGHT_NON_TAX_PER ? parseFloat(poItem.FRIEGHT_NON_TAX_PER) : 0) ? 1 : 0,
                                    FRA1: poItem.FREIGHT_TAXABLE_PER ? parseFloat(poItem.FREIGHT_TAXABLE_PER) : 0,
                                    FRA1FieldSpecified: (poItem.FREIGHT_TAXABLE_PER ? parseFloat(poItem.FREIGHT_TAXABLE_PER) : 0) ? 1 : 0,
                                    FRB1: poItem.FREIGHT_VALUE ? parseFloat(poItem.FREIGHT_VALUE) : 0,
                                    FRB1FieldSpecified: (poItem.FREIGHT_VALUE ? parseFloat(poItem.FREIGHT_VALUE) : 0) ? 1 : 0,
                                    FRB2: poItem.FREIGHT_NON_TAX_PER ? parseFloat(poItem.FREIGHT_NON_TAX_PER) : 0,
                                    FRB2FieldSpecified: (poItem.FREIGHT_NON_TAX_PER ? parseFloat(poItem.FREIGHT_NON_TAX_PER) : 0) ? 1 : 0,
                                    ZMS1: poItem.MISC_TAXABLE_PER ? parseFloat(poItem.MISC_TAXABLE_PER) : 0,
                                    ZMS1FieldSpecified: (poItem.MISC_TAXABLE_PER ? parseFloat(poItem.MISC_TAXABLE_PER) : 0) ? 1 : 0,
                                    ZMS2: poItem.MISC_TAXABLE_VAL ? parseFloat(poItem.MISC_TAXABLE_VAL) : 0,
                                    ZMS2FieldSpecified: (poItem.MISC_TAXABLE_VAL ? parseFloat(poItem.MISC_TAXABLE_VAL) : 0) ? 1 : 0,
                                    ZMS3: poItem.MISC_NONTAX_PER ? parseFloat(poItem.MISC_NONTAX_PER) : 0,
                                    ZMS3FieldSpecified: (poItem.MISC_NONTAX_PER ? parseFloat(poItem.MISC_NONTAX_PER) : 0) ? 1 : 0,
                                    ZMS4: poItem.MISC_VALUE_NONTAX ? parseFloat(poItem.MISC_VALUE_NONTAX) : 0,
                                    ZMS4FieldSpecified: (poItem.MISC_VALUE_NONTAX ? parseFloat(poItem.MISC_VALUE_NONTAX) : 0) ? 1 : 0,
                                    MENGE: poItem.QUANTITY ? $scope.precisionRound(parseFloat(poItem.QUANTITY)) : 0,
                                    MENGEFieldSpecified: (poItem.QUANTITY ? parseFloat(poItem.QUANTITY) : 0) ? 1 : 0,
                                    NETPR: $scope.precisionRound(poItem.PO_NETPRICE),
                                    QUOT_NO: $scope.poQuotId ? $scope.poQuotId : '',
                                    PRODUCT_ID: poItem.PRODUCT_ID ? poItem.PRODUCT_ID : 0,
                                    USER: $scope.currentUserId
                                });

                                counter++;
                            });
                        }
                    });

                    var params = {
                        "data": sapDataList,
                        "rawJSON": JSON.stringify($scope.formDetails),
                        "qcsVendorAssignmentJSON": JSON.stringify($scope.vendorAssignments),
                        "qcsRequirementJSON": JSON.stringify($scope.requirementDetails),
                        "isDraft": isDraft ? true : false
                    };

                    PRMPOServices.SavePODetailsToSAP(params)
                        .then(function (response) {
                            //$scope.GetRequirementPO();
                            if (!response.errorMessage) {
                                swal("Thanks !", response.message, "success");
                                $scope.POServiceStatusText = response.message;
                            } else {
                                swal("Error!", response.errorMessage, "error");
                                $scope.POServiceStatusText = response.errorMessage;
                            }
                        });
                }
            };

            $scope.detectLinks = function urlify(text) {
                if (text) {
                    var urlRegex = /(https?:\/\/[^\s]+)/g;
                    return text.replace(urlRegex, function (url) {
                        return url;//'<a target="_blank" href="' + url + '">' + url + '</a>';
                    });
                }
            };

        }]);