﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;
using PRMServices.Models.Catalog;
using MAT = PRMServices.Syngene.Material.Service;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMSAPIntegrationService
    {

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "erp_pr_stage")]
        Response SaveERPPRStageData(ERPPRStage[] data);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "postpodata")]
        Response PostPOData(SAPOEntity[] data, string rawJSON, string qcsVendorAssignmentJSON, string qcsRequirementJSON, bool isDraft);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "sendpostpodata")]
        Response SendPostPoData(SAPOEntity[] data, string rawJSON, string qcsVendorAssignmentJSON, string qcsRequirementJSON, bool isDraft);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmaterialfromsap?materialcode={materialcode}&sessionid={sessionid}")]
        Product GetMaterialFromSAP(string materialcode, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmaterialfromsap1?materialcode={materialcode}&sessionid={sessionid}")]
        MAT.ZSMATFINAL[] GetMaterialFromSAPTemp(string materialcode, string sessionid);

    }
}
