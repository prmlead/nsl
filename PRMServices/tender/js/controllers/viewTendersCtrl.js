﻿
prmApp
    .controller('viewTendersCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService", "$stateParams",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, $stateParams) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 8;
            $scope.itemsPerPage2 = 8;
            $scope.maxSize = 8;
            $scope.myAuctions = [];
            $scope.categories = [];
            $scope.reqStatus = 'ALL';
            $scope.biddingType = 'ALL';
            $scope.category = 'ALL CATEGORIES';

            $scope.isSuperUser = userService.getUserObj().isSuperUser;

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            // Clickable dashboard //
            $scope.NavigationFilters = $stateParams.filters;
            // Clickable dashboard //

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

            $scope.pageChanged = function () {
                //$scope.getAuctions();
            };

            $scope.getAuctions = function (isfilter) {
                $log.info($scope.formRequest.isForwardBidding);
                if (!$scope.formRequest.isForwardBidding) {


                    if (isfilter) {
                        $scope.totalItems = 0;
                        $scope.currentPage = 1;
                        $scope.currentPage2 = 1;
                        $scope.itemsPerPage = 8;
                        $scope.itemsPerPage2 = 8;
                        $scope.maxSize = 8;
                        $scope.myAuctions = [];
                    }

                    auctionsService.getmyAuctions({ "userid": userService.getUserId(), "reqstatus": $scope.reqStatus, "sessionid": userService.getUserToken() })
                        .then(function (response) {
                            $scope.myAuctions1 = [];
                            //$scope.myAuctions1 = $scope.myAuctions.concat(response);
                            $scope.myAuctions1 = response;
                            $scope.myAuctions1 = $scope.myAuctions1.filter(function (auction) {
                                return (auction.biddingType === 'TENDER');
                            });

                            $scope.myAuctions1.forEach(function (item, index) {
                                item.postedOn = $scope.GetDateconverted(item.postedOn);
                                item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                                item.expStartTime = $scope.GetDateconverted(item.expStartTime);
                                item.startTime = $scope.GetDateconverted(item.startTime);

                                if (String(item.startTime).includes('9999')) {
                                    item.startTime = '';
                                }


                                if (item.status == 'UNCONFIRMED') {
                                    item.statusColor = 'text-warning';
                                }
                                else if (item.status == 'NOT STARTED') {
                                    item.statusColor = 'text-warning';
                                }
                                else if (item.status == 'STARTED') {
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'Negotiation Ended') {
                                    item.statusColor = 'text-success';
                                }
                                else if (item.status == 'Negotiation Ended') {
                                    item.statusColor = 'text-success';
                                }
                                else {
                                    item.statusColor = '';
                                }


                                if (item.isRFP) {
                                    item.rfpStatusColor = 'orangered';
                                } else {
                                    item.rfpStatusColor = 'darkgreen';
                                }



                            })

                            $scope.myAuctions = $scope.myAuctions1;

                            //$scope.participatedVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                            //    return (vendor.runningPrice > 0 && vendor.companyName != 'PRICE_CAP');
                            //});

                            //if ($scope.myAuctions1 && $scope.myAuctions1.length > 0)
                            //{
                            //    $scope.totalItems = $scope.myAuctions1[0].totalCount;
                            //    for (var i = 0; i <= $scope.myAuctions1.length; i++) {
                            //        if ($scope.myAuctions1[i] && $scope.myAuctions1[i].requirementID > 0)
                            //        {
                            //            $scope.myAuctions[(($scope.currentPage - 1) * $scope.itemsPerPage) + i] = $scope.myAuctions1[i];
                            //        }

                            //    }
                            //}




                            if ($scope.myAuctions.length > 0) {
                                $scope.myAuctionsLoaded = true;
                                $scope.totalItems = $scope.myAuctions.length;
                            } else {
                                $scope.myAuctionsLoaded = false;
                                $scope.totalItems = 0;
                                $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                            }



                            $scope.tempStatusFilter = angular.copy($scope.myAuctions);
                            $scope.tempCategoryFilter = angular.copy($scope.myAuctions);


                            // Clickable dashboard //

                            if ($scope.NavigationFilters && $scope.NavigationFilters.status) {
                                $scope.getStatusFilter($scope.NavigationFilters.status);
                                $scope.reqStatus = $scope.NavigationFilters.status;
                            }

                            // Clickable dashboard //

                        });
                }
                else {
                    fwdauctionsService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                        .then(function (response) {
                            $scope.myAuctions = response;
                            if ($scope.myAuctions.length > 0) {
                                $scope.myAuctionsLoaded = true;
                                $scope.totalItems = $scope.myAuctions.length;
                            } else {
                                $scope.myAuctionsLoaded = false;
                                $scope.totalItems = 0;
                                $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                            }
                        });
                }
            };

            $scope.getAuctions();

            $scope.GetRequirementsReport = function () {
                auctionsService.getrequirementsreport({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.reqReport = response;
                        alasql.fn.handleDate = function (date) {
                            return new moment(date).format("MM/DD/YYYY");
                        }
                        alasql('SELECT requirementID as RequirementID, title as RequirementTitle, handleDate(postedDate) as [PostedDate], status as Status, vendorID as VendorID, companyName as CompanyName, userPhone as Phone,userEmail as Email, itemID as [ItemID], productBrand as Brand, productQuantity as Quantity, productQuantityIn as [Units], costPrice as [InitialCostPrice], revCostPrice as [FinalCostPrice], netPrice as [InitialNetPrice], marginAmount as [InitialMarginAmount], unitDiscount as [MarginPercentage],revUnitDiscount as [RevisedMarginPercentage],unitMRP as [MRP],gst as [GST],savings as [Savings], handleDate(startTime) as [StartTime], status as [Status], othersBrands as [ManufacturerName] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xlsx", $scope.reqReport]);
                    });

                //alasql('SELECT requirementID as [RequirementID],title as [Title],vendorID as [VendorID],companyName as [Company],userPhone as [Phone],userEmail as [Email],itemID as [ItemID],productBrand as [Brand],productQuantity as [Quantity], productQuantityIn as [Units],costPrice as [InitialCostPrice],revCostPrice as [FinalCostPrice],netPrice as [NetPrice],marginAmount as [InitialMarginAmount],unitDiscount as [InitialMargin],revUnitDiscount as [FinalMargin],unitMRP as [MRP],gst as [GST],maxInitMargin as [MaximumInitialMargin],maxFinalMargin as [MaximumFinalMargin],savings as [Savings] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xslx", $scope.reqReport]);

            };
            
            $scope.goToReqReport = function (reqID) {
                    //$state.go("reports", { "reqID": reqID });

                    var url = $state.href('reports', { "reqID": reqID });
                    $window.open(url, '_blank');

            };

            $scope.cloneRequirement = function (requirement) {
                let tempObj = {};
                let stateView = 'save-requirementAdv';
                if (requirement.biddingType && requirement.biddingType === 'TENDER') {
                    stateView = 'save-tender';
                }
                
                tempObj.cloneId = requirement.requirementID;
                $state.go(stateView, { 'Id': requirement.requirementID, reqObj: tempObj });
            };

            $scope.searchTable = function (str) {

                //$scope.category = 'ALL CATEGORIES';
                //$scope.reqStatus = 'ALL';
                //$scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                //    return (String(req.requirementID).includes(str) == true || String(req.title).includes(str) == true || req.category[0].includes(str) == true );
                //});

                // RFQ Date Based Filter // 

                str = str.toLowerCase();
                $scope.category = 'ALL CATEGORIES';
                $scope.reqStatus = 'ALL';
                $scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                    return (String(req.requirementID).includes(str) == true
                        || String(req.title.toLowerCase()).includes(str) == true
                        || String(req.userName.toLowerCase()).includes(str) == true
                        || String(req.postedOn).includes(str) == true
                        || req.category[0].toLowerCase().includes(str) == true);
                });

                // RFQ Date Based Filter //

                $scope.totalItems = $scope.myAuctions.length;
            };

            
            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));

                            $scope.categories.push('ALL CATEGORIES');

                            //categories.splice(categories.indexOf("ALL"), 1);
                            //categories.unshift('ALL');

                            $scope.categories = $scope.categories.filter(item => item !== "ALL CATEGORIES" && item !== "");
                            $scope.categories.unshift("ALL CATEGORIES");

                            //console.log($scope.categories);

                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    }
                }, function (result) {
                });

            };


            $scope.getCategories();

            $scope.getStatusFilter = function (filterVal) {
                $scope.filterArray = [];
                if (filterVal == 'ALL') {
                    $scope.myAuctions = $scope.myAuctions1;

                } else {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        return $scope.prmStatus($scope.isCustomer, item.status) === $scope.prmStatus($scope.isCustomer, filterVal);
                    });
                    $scope.myAuctions = $scope.filterArray;
                }

                $scope.totalItems = $scope.myAuctions.length;

            };

            $scope.getCategoryFilter = function (filterCategoryVal) {
                $scope.filterCatArray = [];
                if (filterCategoryVal == 'ALL CATEGORIES') {
                    $scope.myAuctions = $scope.myAuctions1;
                } else {
                    $scope.filterCatArray = $scope.tempCategoryFilter.filter(function (item) {
                        return item.category[0] == filterCategoryVal;
                    });
                    $scope.myAuctions = $scope.filterCatArray;

                }

                $scope.totalItems = $scope.myAuctions.length;
            };
           
        }]);