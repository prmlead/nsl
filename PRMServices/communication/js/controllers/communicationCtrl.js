﻿prmApp.controller('communicationCtrl', function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, PRMCommunicationService,
    fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, SettingService, catalogService, $stateParams) {
    $scope.ExcelProducts = [];

    var self = this;
    $scope.uId = userService.getUserId();
    $scope.compId = userService.getUserCompanyId();
    $scope.sessionid = userService.getUserToken();
    $scope.vendorsList = [];
    $scope.vendorsList1 = [];
    $scope.userObj = {};
    userService.getUserDataNoCache()
        .then(function (response) {
            $scope.userObj = response;
        });

    $scope.communicationOptions = {
        allVendors: false,
        selectedVenders: false,
        checkBoxSms: false,
        checkBoxEmail: false,
        subject: "",
        body: ""
    };

    $scope.selectedSubCategoriesList = [];
    $scope.subcategories = [];
    $scope.sub = {
        selectedSubcategories: [],
    };

    $scope.selectedSubcategories = [];
    $scope.totalSubcats = [];
    $scope.totalVendors = 0;
    $scope.totalInactiveVendors = 0;
    $scope.currentPage = 1;
    $scope.currentPage2 = 1;
    $scope.itemsPerPage = 5;
    $scope.itemsPerPage2 = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function () { };
    /*pagination code*/

    $scope.userObj = userService.getUserObj();
    var loginUserData = userService.getUserObj();
    this.userId = userService.getUserId();
    $rootScope.$on("CallProfileMethod", function () {
        $scope.updateUserDataFromService();
    });
    $scope.selectedProducts = [];
    var date = new Date();

    $scope.validateData = function () {
        var isValid = false;

        if (($scope.communicationOptions.checkBoxSms || $scope.communicationOptions.checkBoxEmail)
            && ($scope.communicationOptions.allVendors || $scope.communicationOptions.selectedVenders)
            && $scope.communicationOptions.subject && $scope.communicationOptions.body) {
            isValid = true;
        }

        return isValid;
    }

    $scope.GetCompanyVendors = function () {
        $scope.params = { "userID": userService.getUserId(), PageSize: 0, NumberOfRecords: 5000, searchString: '', "sessionID": userService.getUserToken() };
        userService.GetCompanyVendors($scope.params)
            .then(function (response) {
                console.log(response);
                if (response && response.length > 0) {
                    response = _.filter(response, function (o) {
                        return o.isValid == true;
                    });
                    $scope.vendorsList = response;
                    $scope.vendorsList1 = response;
                    $scope.vendorDisplayCollecton = [].concat($scope.vendorsList);
                }
            });
    };


    $scope.myCategories = false;

    $scope.selectAllVendors = function () {
        $scope.myCategories = false;
        $scope.vendorDisplayCollecton = [];
        $scope.vendorsList = [];
        $scope.vendorsList1 = [];
        $scope.communicationOptions.selectedVenders = false;
        if ($scope.communicationOptions.allVendors) {
            $scope.GetCompanyVendors();
        }

        $scope.validateData();
    };

    $scope.retriveCategoryVendors = function () {
        console.log($scope.selectedCategories);
        if ($scope.selectedCategories && $scope.selectedCategories.length > 0) {
            PRMCommunicationService.getCatalogueVendors($scope.selectedCategories.join(","), $scope.uId).then(function (response) {
                console.log(response);
                if (response && response.length > 0) {
                    response = _.filter(response, function (o) {
                        return o.isValid == true;
                    });
                    $scope.vendorsList = response;
                   
                    $scope.vendorDisplayCollecton = [].concat($scope.vendorsList);
                }
            });
        }
    }

    $scope.searchTable = function (value) {
        console.log($scope.searchKeyword)
        if (value) {

            $scope.vendorsList = _.filter($scope.vendorsList1, function (item) {
                return (item.email.toUpperCase().indexOf(value.toUpperCase()) > -1 ||
                    item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1 ||
                    item.phoneNum.indexOf(value) > -1

                );
            });
        } else {
            $scope.vendorsList = $scope.vendorsList1;
        }

        $scope.totalItems = $scope.vendorsList.length;
    }
    $scope.selectedVendors = function () {
        $scope.myCategories = $scope.myCategories ? false : true;
        console.log($scope.communicationOptions);
        $scope.vendorDisplayCollecton = [];
        $scope.vendorsList = [];
        $scope.communicationOptions.allVendors = false;
    };

    $scope.selectVendor = function (vendorObj) {
        vendorObj.selected = !vendorObj.selected;
        $scope.validateData();
    };

    $scope.selectAllVendor = function (vendorObj) {
        //_.forEach($scope.vendorDisplayCollecton, function (vendor, i) {
        //    vendor.selected = !vendor.selected;
        //});

        _.forEach($scope.vendorsList, function (vendor, i) {
            vendor.selected = !vendor.selected;
        });
    };

    $scope.sendCommunication = function () {
        var selectedVendors = _.filter($scope.vendorsList, function (o) {
            return o.selected == true;
        });


        if (selectedVendors && selectedVendors.length > 0 && ($scope.communicationOptions.checkBoxSms || $scope.communicationOptions.checkBoxEmail)
            && $scope.communicationOptions.subject && $scope.communicationOptions.body) {

            $scope.communicationOptions.bodyTemp = $scope.communicationOptions.body.replace(/\n/g, "<br />");

            PRMCommunicationService.SendCommunications({
                "vendors": selectedVendors,
                "subject": $scope.communicationOptions.subject,
                "body": $scope.communicationOptions.bodyTemp,
                "sendSMS": $scope.communicationOptions.checkBoxSms,
                "sendEmail": $scope.communicationOptions.checkBoxEmail,
                "sessionID": userService.getUserToken()
            }).then(function (response) {
                if (response.errorMessage != "") {
                    swal({
                        title: "Error!",
                        text: "Error sending communication, please contact support.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {

                        });
                }
                else if (response.errorMessage == "" && response.objectID > 0) {
                    swal({
                        title: "Done!",
                        text: "Communication Sent Successfully",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            location.reload();
                        });
                }
            });
        } else {
            var errorMsg = "Please enter valid details to send communication.";
            if (!selectedVendors || selectedVendors.length <= 0) {
                errorMsg = "Please select vendors to send communication.";
            } else if (!$scope.communicationOptions.checkBoxSms && !$scope.communicationOptions.checkBoxEmail) {
                errorMsg = "Please select communication mode to send communication.";
            } else if ($scope.communicationOptions.subject == "" || $scope.communicationOptions.body == "") {
                errorMsg = "Please enter Subject & Body to send communication.";
            }

            swal({
                title: "Error!",
                text: errorMsg,
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            },
                function () {

                });
        }
    };

    $scope.getCategories = function () {
        auctionsService.getCategories(userService.getUserId())
            .then(function (response) {
                $scope.categories = response;
                $scope.addVendorCats = _.uniq(_.map(response, 'category'));
                $scope.categoriesdata = response;
                $scope.showCategoryDropdown = true;
            })
    };

    $scope.getCategories();

    $scope.loadSubCategories = function () {
        $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.newVendor.category });
    };

    $scope.changeCategory = function () {
        $scope.selectedSubCategoriesList = [];
        $scope.loadSubCategories();
    };

    $scope.selectSubcat = function () {
        $scope.selectedSubCategoriesList = [];
        $scope.vendorsLoaded = false;
        var category = [];
        var count = 0;
        var succategory = "";
        $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
        selectedcount = $scope.sub.selectedSubcategories.length;
        if (selectedcount > 0) {
            succategory = _.map($scope.sub.selectedSubcategories, 'id');
            category.push(succategory);
            $scope.selectedSubCategoriesList = succategory;
        }
        else {
        }
    };

    $scope.selectSubcat();

    $scope.goTocategory = function () {
        var url = $state.href('category', {});
        $window.open(url, '_blank');
    };

    //// catalog management
    $scope.parentNodes = null;
    $scope.getcatalogcategories = function (vendorId, type) {
        if (type == 'undefined' || type == "" || type == null) {
            type = '';
        }

        $scope.currentVendorId = vendorId;
        var catNodes = [];
        // checking for catalogue category checked issue
        catalogService.getVendorCategories(vendorId, 0, $scope.compId, type,0,0,"")
        //catalogService.getVendorCategories(vendorId, 0, $scope.compId, type, $scope.vendCatalogfetchRecordsFrom, $scope.vendCatalogPageSize, searchString ? searchString : "")
            .then(function (response) {
                $scope.parentNodes = response;
                $scope.collapseAll();
            });
    }
    $scope.getsub = function (parentData) {
        $scope.productDetails = null;
        var nodeData = parentData;

        var subNodes = [];

        if (nodeData && nodeData.id && (nodeData.nodes == null || nodeData.nodes.length <= 0)) {
            catalogService.getSubCatagories(nodeData.id, $scope.compId)
                .then(function (response) {
                    var subCatArray = response;
                    subCatArray.forEach(function (item, index) {
                        var subCat = {
                            "comId": item.compId,
                            "parentID": item.catParentId,
                            "id": item.catId,
                            "title": item.catName,
                            "catdesc": item.catDesc,
                            "subCatCount": item.subCatCount,
                            "childCollapsed": true,
                            "nodeChecked": parentData.nodeChecked,
                            "nodes": []
                        }
                        subNodes.push(subCat);
                    })
                });

            nodeData.nodes = subNodes;
        }
    }
    $scope.selectedCategories = [];
    $scope.selectChildNodes = function (childNode, ischecked) {
        childNode.nodeChecked = ischecked;
        childNode.nodes.forEach(function (item, index) {
            $scope.selectChildNodes(item, ischecked);
        });
    }
    $scope.getSelectedNodes = function () {
        $scope.selectedCategories = [];

        $scope.parentNodes.forEach(function (item, index) {
            $scope.getSelectedNodesIteration(item);
        })
    }
    $scope.getSelectedNodesIteration = function (selectedNode) {
        if (selectedNode.nodeChecked) {
            $scope.selectedCategories.push(selectedNode.catId);
        }
        if (selectedNode.nodes != null && selectedNode.nodes.length > 0) {
            selectedNode.nodes.forEach(function (item, index) {
                if (item.nodeChecked) {
                    $scope.getSelectedNodesIteration(item);
                }

            })
        }
    }
    $scope.selectParentNode = function (ischecked, selectedNodeScope) {
        if (selectedNodeScope) {
            var parentScope = selectedNodeScope.$parent;
            if (parentScope && parentScope.node && parentScope.node.catId > 0) {
                if (ischecked) {
                    parentScope.node.nodeChecked = ischecked;
                }
                else {
                    if (parentScope.node.nodes && parentScope.node.nodes.length > 0) {
                        if ($filter('filter')(parentScope.node.nodes, { 'nodeChecked': true }).length <= 0) {
                            parentScope.node.nodeChecked = ischecked;
                        }
                    }
                    else {
                        parentScope.node.nodeChecked = ischecked;
                    }
                }

                $scope.selectParentNode(ischecked, parentScope);
            }
        }
    }
    $scope.checkChanged = function (selectedNode, selectedNodeScope) {
        $scope.selectChildNodes(selectedNode, selectedNode.nodeChecked);
        //$scope.selectParentNode(selectedNode.nodeChecked, selectedNodeScope);
        $scope.getSelectedNodes();
    }
    $scope.toggle = function (scope) {
        scope.toggle()
    };
    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    };
    $scope.expandAll = function () {
        $scope.$broadcast('angular-ui-tree:expand-all');
    };


    $scope.limit1 = 100;
    $scope.productsLimit = 100;


    $scope.newToggle = function (val) {
        $scope.limit1 = $scope.limit1 + 10;
    };

    $scope.getcatalogcategories(0);
});