﻿using System;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class ModuleBasedError : Entity
    {
        //PO_NUMBER,PO_LINE_ITEM,PRODUCT_CODE,CATEGORY_CODE,PLANT_CODE,REASON,DATE_CREATED
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
        [DataMember] [DataNames("CATEGORY_CODE")] public string CATEGORY_CODE { get; set; }
        [DataMember] [DataNames("PLANT_CODE")] public string PLANT_CODE { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("PO_STATUS")] public string PO_STATUS { get; set; }
        [DataMember] [DataNames("PO_DATE")] public DateTime? PO_DATE { get; set; }
        [DataMember] [DataNames("INVOICE_NUMBER")] public string INVOICE_NUMBER { get; set; }
        [DataMember] [DataNames("PAYMENT_TRANSACT_ID")] public string PAYMENT_TRANSACT_ID { get; set; }
        [DataMember] [DataNames("PAYMENT_CODE")] public string PAYMENT_CODE { get; set; }
        [DataMember] [DataNames("PAYMENT_AMOUNT")] public decimal PAYMENT_AMOUNT { get; set; }
        [DataMember] [DataNames("PAYMENT_DATE")] public DateTime? PAYMENT_DATE { get; set; }
        [DataMember] [DataNames("MODULE_NAME")] public string MODULE_NAME { get; set; }
        [DataMember] [DataNames("JOB_ID")] public string JOB_ID { get; set; }
        [DataMember] [DataNames("REASON")] public string REASON { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
    }
}