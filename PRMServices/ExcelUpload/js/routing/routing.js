﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('moduleErrorList', {
                    url: '/moduleErrorList/:moduleName',
                    templateUrl: 'ExcelUpload/views/moduleErrorList.html'
                });

        }]);