prmApp

    .service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function(file, uploadUrl){
            var fd = new FormData();
            fd.append('file', file);
        
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
        
            .success(function(){
            })
        
            .error(function(){
            });
        }
    }])
    // =========================================================================
    // Auction Details Services
    // =========================================================================
    .service('auctionsService', function($http,store,$state,$rootScope,domain, version) {
        //var domain = 'http://182.18.169.32/services/';
        var auctions = this;
		auctions.getdate = function(){
            var date = new Date();
            var time = date.getTime();
            return $http({
                method: 'GET',
                url: domain+'getdate?time=' + time,
                encodeURI: true,
                headers: {'Content-Type': 'application/json'}
                //data: params
            }).then(function(response) {
                var list=[];
                if(response && response.data ){
                     if(response.data.length>0){
                        list=response.data;   
                     }
                     return list;
                }else{
                    //console.log(response.data[0].errorMessage);
                }
            }, function(result) {
                //console.log("date error");
            });
		}

		auctions.isnegotiationended = function (reqid, sessionid) {
		    return $http({
		        method: 'GET',
		        url: domain + 'isnegotiationended?reqid=' + reqid + '&sessionid=' + sessionid,
		        encodeURI: true,
		        headers: { 'Content-Type': 'application/json' }
		        //data: params
		    }).then(function (response) {
		        var list = {};
		        if (response && response.data) {
		            return response.data;
		        } else {

		        }
		    }, function (result) {
		        //console.log("date error");
		    });
		}


        auctions.selectVendor = function(params){
            return $http({
                method: 'POST',
                url: domain+'selectvendor',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     return response.data;
                }else{
                    
                }
            }, function(result) {
                //console.log(result);
            });
        }

        auctions.getauctions=function (params) {
            return $http({
                method: 'GET',
                url: domain+'getrunningauctions?section='+params.section+'&userid='+params.userid+'&sessionid='+params.sessionid+'&limit='+params.limit,
                encodeURI: true,
                headers: {'Content-Type': 'application/json'}
                //data: params
            }).then(function(response) {
                var list=[];
                if(response && response.data ){
                     if(response.data.length>0){
                        list=response.data;   
                     }
                     return list;
                }else{
                    //console.log(response.data[0].errorMessage);
                }
            }, function(result) {
                //console.log("there is no current Negotiations");
            });
        };

        auctions.rateVendor = function(params){
            return $http({
                method: 'POST',
                url: domain+'userratings',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     return response.data;
                }else{
                    
                }
            }, function(result) {
                //console.log(result);
            });
        }



        auctions.updateStatus = function(params){
            return $http({
                method: 'POST',
                url: domain+'updatestatus',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     return response.data;
                }else{
                    
                }
            }, function(result) {
                //console.log(result);
            });
        }

        auctions.uploadquotationsfromexcel = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'uploadquotationsfromexcel',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.itemwiseselectvendor = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'itemwiseselectvendor',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.updatedeliverdate = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'updateexpdelandpaydate',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.updatepaymentdate = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'updateexpdelandpaydate',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.StartNegotiation = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'startNegotiation',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
            });
        }        


        auctions.RestartNegotiation = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'restartnegotiation',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
            });
        }

        auctions.DeleteVendorFromAuction = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'deletevendorfromauction',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {

            });
        }

        auctions.generatepo = function(params){
            var data = {
                reqPO: params
            };
            return $http({
                method: 'POST',
                url: domain+'generatepoforuser',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: data
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    
                }
            }, function(result) {
                //console.log(result);
            });
        }
        
        auctions.vendorreminders = function(params){
            var data = params;
            return $http({
                method: 'POST',
                url: domain+'vendorreminders',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: data
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                    list=response.data;
                    return list;
                }else{
                    
                }
            }, function(result) {
                //console.log(result);
            });
        }


        auctions.SaveCompanyDepartment = function (params) {
            var data = params;
            return $http({
                method: 'POST',
                url: domain + 'savecompanydepartment',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.DeleteDepartment = function (params) {
            var data = params;
            return $http({
                method: 'POST',
                url: domain + 'deletedepartment',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.GetCompanyDepartments = function (userid, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getcompanydepartments?userid=' + userid + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }        

        auctions.GetUserDepartments = function (userid, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getuserdepartments?userid=' + userid + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        auctions.SaveUserDepartments = function (params) {
            var data = params;
            return $http({
                method: 'POST',
                url: domain + 'saveuserdepartments',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.GetDepartmentUsers = function (deptid, userid, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getdepartmentusers?deptid=' + deptid + '&userid=' + userid + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        
        auctions.GetReqDepartments = function (userid, reqid, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getreqdepartments?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        auctions.SaveReqDepartments = function (params) {
            var data = params;
            return $http({
                method: 'POST',
                url: domain + 'savereqdepartments',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.GetCompanyConfiguration = function (compid, configkey, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getcompanyconfiguration?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        auctions.GetIsAuthorized = function (userid, reqid, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getisauthorized?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        auctions.materialdispatch = function(params){
            var data = {
                reqMat: params
            };
            return $http({
                method: 'POST',
                url: domain+'materialdispatch',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: data
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    
                }
            }, function(result) {
                //console.log(result);
            });
        }

        auctions.paymentdetails = function(params){
            var data = {
                paymentDets: params
            };
            return $http({
                method: 'POST',
                url: domain+'paymentdetails',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: data
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    
                }
            }, function(result) {
                //console.log(result);
            });
        }


        auctions.generatePOinServer = function(params){
            return $http({
                method: 'POST',
                url: domain+'generatepo',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    
                }
            }, function(result) {
                //console.log(result);
            });
        }

        auctions.getDashboardStats = function(params){
            return $http({
                method:'GET',
                url:domain + 'getdashboardstats?userid='+params.userid+'&sessionid='+params.sessionid,
                encodeURI:true,
                headers:{'Content-Type': 'application/json'}
            }).then(function(response){
                if(response && response.data){
                    if( response.data.errorMessage == ''){
                        return response.data;
                    } else {
                        return {};
                    }                    
                }
            });
        };

        auctions.getCategories=function (params) {
            return $http({
                method: 'GET',
                url: domain+'getcategories?userid=' + (params ? params : -1),
                encodeURI: true,
                headers: {'Content-Type': 'application/json'}
                //data: params
            }).then(function(response) {
                var list=[];
                if(response && response.data ){
                     if(response.data.length>0){
                        list=response.data;   
                     }
                     return list;
                }else{
                    //console.log(response.data[0].errorMessage);
                }
            }, function(result) {
                //console.log("there is no categories");
            });
        };

        auctions.getKeyValuePairs = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getkeyvaluepairs?parameter=' + params,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
                //data: params
            }).then(function (response) {
                var list = [];
                if (response && response.data) {
                    if (response.data.length > 0) {
                        list = response.data;
                    }
                    return list;
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("there is no categories");
            });
        };

        auctions.getmyAuctions= function(params){
            return $http({
                method: 'GET',
                url: domain+'getmyauctions?userid='+params.userid+"&sessionid="+params.sessionid,
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    //console.log(response.data.errorMessage);
                }
            }, function(result) {
                //console.log(result);
                //console.log("there is no your Negotiations");
            });
        };






        auctions.getactiveleads= function(params){
            return $http({
                method: 'GET',
                url: domain+'getactiveleads?userid='+params.userid+"&sessionid="+params.sessionid,
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    //console.log(response.data.errorMessage);
                }
            }, function(result) {
                //console.log(result);
                //console.log("there is no your Negotiations");
            });
        };




        auctions.getrequirementdata = function(params){
            return $http({
                method: 'GET',
                url: domain+'getrequirementdata?reqid='+params.reqid+"&sessionid="+params.sessionid+"&userid="+params.userid,
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    //console.log(response.data.errorMessage);
                }
            }, function(result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };



        auctions.GetReportsRequirement = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getreportsrequirement?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no reports");
            });
        };


        auctions.GetVendorReminders = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getvendorreminders?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no Reminders");
            });
        };

        auctions.GetBidHistory = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };

        auctions.getpodetails = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getpodetails?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };
        
        auctions.getcomments = function(params){
            return $http({
                method: 'GET',
                url: domain+'getcomments?reqid='+params.reqid + "&userID=" + params.userid +  "&sessionid="+params.sessionid,
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    //console.log(response.data.errorMessage);
                }
            }, function(result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };
        
        auctions.getpricecomparison = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getpricecomparison?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };

        auctions.GetPriceComparisonPreNegotiation = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getpricecomparisonprenegotiation?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };
        

        auctions.savecomment = function (params) {
            var comment = {
                "com": {
                    "requirementID": params.reqID,
                    "firstName": "",
                    "lastName": "",
                    "userID": params.userID,
                    "commentText": params.commentText,
                    "replyCommentID": -1,
                    "commentID": -1,
                    "errorMessage": "",
                    "sessionID": params.sessionID
                }
            }
            return $http({
                method: 'POST',
                url: domain+'savecomment',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: comment
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    //console.log(response.data.errorMessage);
                }
            }, function(result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };


        auctions.deleteAttachment = function(Attaachmentparams){
           // params.action="updatebidtime";
            return $http({
                method: 'POST',
                url: domain+'deleteattachment',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: Attaachmentparams
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    
                }
            }, function(result) {
                //console.log("service failed, please try later...!");
            });
        };
        

        auctions.postrequirementdata = function(params){
            //params.action="requirementsave";
            var myDate = new Date(); // Your timezone!
            var myEpoch = parseInt(myDate.getTime()/1000);
            //console.log(myEpoch);
            params.minBidAmount = 0;
            params.postedOn = "/Date(" + myEpoch + "000+0000)/";
            params.timeLeft = -1;
            params.price = -1;
            var requirement = {
            "requirement": {
                "title": params.title,
                "description": params.description,
                "category": params.category,
                "subcategories": params.subcategories,
                "urgency": params.urgency,
                "budget": params.budget,
                "attachmentName": params.attachmentName,
                "deliveryLocation": params.deliveryLocation,
                "taxes": params.taxes,
                "paymentTerms": params.paymentTerms,
                "requirementID": params.requirementID,
                "customerID": params.customerID,
                "isClosed": params.isClosed,
                "endTime": null,
                "sessionID": params.sessionID,
                "errorMessage": "",
                "timeLeft": -1,
                "price": -1,
                "auctionVendors": params.auctionVendors,
                "startTime": null,
                "status": "",
                "postedOn": "/Date("+myEpoch+"000+0000)/",
                "custLastName": params.customerLastname,
                "custFirstName":params.customerFirstname,
                "deliveryTime": params.deliveryTime, 
                "includeFreight": params.includeFreight, 
                "inclusiveTax": params.inclusiveTax,
                "minBidAmount": 0,
                "checkBoxEmail": params.checkBoxEmail,
                "checkBoxSms": params.checkBoxSms,
                "quotationFreezTime": params.quotationFreezTime,
                "currency": params.currency,
                "timeZoneID": params.timeZoneID,
                "listRequirementItems": params.listRequirementItems,
                "isTabular": params.isTabular,
                "reqComments": params.reqComments,
                "itemsAttachment": params.itemsAttachment,
                "isSubmit": params.isSubmit,
                "isQuotationPriceLimit": params.isQuotationPriceLimit,
                "quotationPriceLimit": params.quotationPriceLimit,
                "noOfQuotationReminders": params.noOfQuotationReminders,
                "remindersTimeInterval": params.remindersTimeInterval
            },"attachment":params.attachment
        };
            return $http({
                method: 'POST',
                url: domain+'requirementsave',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: requirement
            }).then(function(response) {
                var list = {};
                if(response && response.data ){
                     list=response.data;
                     return list;
                }else{
                    //console.log(response.data.errorMessage);
                }
            }, function(result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };
        
        auctions.updatebidtime = function(params){
           // params.action="updatebidtime";
            return $http({
                method: 'POST',
                url: domain+'updatebidtime',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                if(response && response.data ){
                     return "";
                }else{
                    return response.data.errorMessage;
                }
            }, function(result) {
                //console.log("service failed, please try later...!");
            });
        };
        
        auctions.makeabid = function(params){
            //params.action="makeabid";
            var list=[];
            return $http({
                method: 'POST',
                url: domain+'makeabid',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                if(response && response.data ){
                     //return "";
                     list=response.data;
                     return list;
                }else{
                    return response.data.errorMessage;
                }
            }, function(result) {
                //console.log("service failed, please try later...!");
            });
        };



        auctions.uploadQuotation = function (params) {
            //params.action="makeabid";
            var list = [];
            return $http({
                method: 'POST',
                url: domain + 'uploadquotation',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data) {
                    //return "";
                    list = response.data;
                    return list;
                } else {
                    return response.data.errorMessage;
                }
            }, function (result) {
                //console.log("service failed, please try later...!");
            });
        };



        auctions.revquotationupload = function (params) {
            //params.action="makeabid";
            var list = [];
            return $http({
                method: 'POST',
                url: domain + 'revquotationupload',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data) {
                    //return "";
                    list = response.data;
                    return list;
                } else {
                    return response.data.errorMessage;
                }
            }, function (result) {
                //console.log("service failed, please try later...!");
            });
        };


        auctions.QuatationAprovel = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'quatationaprovel',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.savepricecap = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'savepricecap',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.GetCompanyLeads = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getcompanyleads?userid=' + params.userid + '&searchstring=' + params.searchstring + '&searchtype=' + params.searchtype + '&sessionid=' + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }
        
        auctions.updateauctionstart = function(params){
            //params.action="makeabid";
            return $http({
                method: 'POST',
                url: domain+'updateauctionstart',
                encodeURI: true,
                headers: {'Content-Type': 'application/json'},
                data: params
            }).then(function(response) {
                if(response && response.data ){
                     return "";
                }else{
                    return response.data.errorMessage;
                }
            }, function(result) {
                //console.log("service failed, please try later...!");
            });
        };

        return auctions;        
    })





   



    // =========================================================================
    // Header Messages and Notifications list Data
    // =========================================================================

    .service('messageService', ['$resource', function($resource){
       /* this.getMessage = function(img, user, text) {
            var gmList = $resource("data/messages-notifications.json");
            
            return gmList.get({
                img: img,
                user: user,
                text: text
            });
        }*/
    }])
    

     

    // =========================================================================
    // Malihu Scroll - Custom Scroll bars
    // =========================================================================
    .service('scrollService', function() {
        var ss = {};
        ss.malihuScroll = function scrollBar(selector, theme, mousewheelaxis) {
            $(selector).mCustomScrollbar({
                theme: theme,
                scrollInertia: 100,
                axis:'yx',
                mouseWheel: {
                    enable: true,
                    axis: mousewheelaxis,
                    preventDefault: true
                }
            });
        }
        
        return ss;
    })

    //==============================================
    // BOOTSTRAP GROWL
    //==============================================

    .service('growlService', function(){
        var gs = {};
        gs.growl = function(message, type) {
            $.growl({
                message: message
            },{
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 2500,
                animate: {
                        enter: 'animated bounceIn',
                        exit: 'animated bounceOut'
                },
                offset: {
                    x: 20,
                    y: 85
                }
            });
        }
        
        return gs;
    })

.service('authService', function($http,store,$state,$rootScope,domain, version) {
    var authService = this;
    var responseObj = {};
    responseObj.objectID = 0;
    authService.isValidSession = function (sessionid) {
        return $http({
            method: 'GET',
            url: domain + 'issessionvalid?sessionid=' + sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            if (response && response.data) {
                responseObj = response.data;
                return responseObj;
            } else {
                return responseObj;
            }
        }, function (result) {
            //console.log("date error");
        });
    }

    return authService;
})







prmApp


    // =========================================================================
    // Auction Details Services
    // =========================================================================
    .service('storeService', function ($http, store, $state, $rootScope, storeDomain, version, userService) {
        //var domain = 'http://182.18.169.32/services/';
        var storeService = this;

        storeService.savestore = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestore',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.deletestore = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'deletestore',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.deleteStoreItemDetails = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'deletestoreitemdetails',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.getcompanystores = function (compID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'companystores?compid=' + compID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }


        storeService.getstores = function (storeID, compID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'stores?storeid=' + storeID + '&compid' + compID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }





        storeService.savestoreitem = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitem',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        storeService.deletestoreitem = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'deletestoreitem',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.getuniqueitems = function (compID, itemName) {
            return $http({
                method: 'GET',
                url: storeDomain + 'uniqueitems?compid=' + compID + '&itemname=' + itemName + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                console.log("date error");
            });
        }

        storeService.getcompanystoreitems = function (storeID, itemID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storeitems?storeid=' + storeID + '&itemid=' + itemID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }



        storeService.importentity = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'importentity',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }



        storeService.storesitempropvalues = function (itemID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storesitempropvalues?itemid=' + itemID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }


        storeService.savestoreitemprop = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitemprop',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.savestoreitempropvalue = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitempropvalue',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        storeService.storesitempropvalues = function (itemID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storesitempropvalues?itemid=' + itemID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }



        storeService.getStoreItemDetails = function (itemID, notDispatched) {
            return $http({
                method: 'GET',
                url: storeDomain + 'getstoreitemdetails?itemid=' + itemID + '&notdispatched=' + notDispatched + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        storeService.saveStoreItemDetails = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitemdetails',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.saveStoreItemGIN = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitemissue',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.saveStoreItemGRN = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitemreceive',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.getStoreItemGIN = function (ginCode, storeID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storeitemissuedetails?gincode=' + ginCode + '&storeid=' + storeID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        storeService.getStoreItemGRN = function (grnCode, storeID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storeitemreceivedetails?grncode=' + grnCode + '&storeid=' + storeID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        storeService.getIndentItemDetails = function (storeID, ginCode, grnCode) {
            return $http({
                method: 'GET',
                url: storeDomain + 'indentitemdetails?storeid=' + storeID + '&gincode=' + ginCode + '&grncode=' + grnCode + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }


        return storeService;
    });






prmApp
    .service('techevalService', function ($http, store, $state, $rootScope, techevalDomain, version, userService) {
        //var domain = 'http://182.18.169.32/services/';
        var techevalService = this;

        techevalService.getquestionnairelist = function () {
            return $http({
                method: 'GET',
                url: techevalDomain + 'getquestionnairelist?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        };

        techevalService.savequestionnaire = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'savequestionnaire',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };       

        techevalService.importentity = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'importentity',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };

        techevalService.getquestionnaire = function (evalid, loadquestions) {
            return $http({
                method: 'GET',
                url: techevalDomain + 'getquestionnaire?evalid=' + evalid + '&loadquestions=' + loadquestions + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        };

        techevalService.saveresponse = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'saveresponse',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };

        techevalService.saveTechApproval = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'saveresponse',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };


        techevalService.getresponses = function (reqid, evalid, userid) {
            return $http({
                method: 'GET',
                url: techevalDomain + 'getresponses?evalid=' + evalid + '&userid=' + userid + '&reqid=' + reqid + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        };
        

        techevalService.GetTechEvalution = function (evalid, reqid) {
            return $http({
                method: 'GET',
                url: techevalDomain + 'gettechevalution?evalid=' + evalid + '&reqid=' + reqid + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        };
        

        techevalService.saveTechApproval = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'savetechevaluation',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };

        return techevalService;
    })






angular.module("prm.user").service('userService', userService);


function userService($http, store, $state, $rootScope, version, domain, growlService) {
    //var domain = 'http://182.18.169.32/services/';
    var self = this;
    var successMessage = '';

    // currentUser must be stored in an object since it is nullable
    // If we two-way bind to currentUser directly, the bind would break
    // when currentUser is set to null.

    self.userData = { currentUser: null };

    self.getUserObj = function () {
        if (!self.userData.currentUser || !self.userData.currentUser.id) {
            self.userData.currentUser = store.get('currentUser');
        }
        return self.userData.currentUser || {};
    };
    self.getUserToken = function () {
        return store.get('sessionid');
    };

    self.getLocalEntitlement = function () {
        return store.get('localEntitlement');
    };

    self.removeLocalEntitlement = function () {
        return store.remove('localEntitlement');
    };

    self.getOtpVerified = function () {
        return self.getUserObj().isOTPVerified;
    };

    self.getEmailOtpVerified = function () {
        return self.getUserObj().isEmailOTPVerified;
    };

    self.reloadProfileObj = function(){
        
    }

    self.getDocsVerified = function () {
        return self.getUserObj().credentialsVerified;
    };
    self.removeUserToken = function () {
        store.remove('sessionid');
    };

    self.getRememberMeToken = function () {
        return store.get('rememberMeToken');
    };
    self.removeRememberMeToken = function () {
        store.remove('rememberMeToken');
    };

    self.removeUser = function () {
        store.remove('currentUser');
    };

    self.getUsername = function () {
        return self.getUserObj().username;
    };

    self.getFirstname = function () {
        return self.getUserObj().firstName;
    };

    self.getLastname = function () {
        return self.getUserObj().lastName;
    };

    self.getUserId = function () {
        return self.getUserObj().userID;
    };

    self.getUserCompanyId = function () {
        return self.getUserObj().companyId;
    };

    self.setMessage = function (msg) {
        successMessage = msg;
    };

    self.getMessage = function () {
        return successMessage;
    };

    self.getUserType = function () {
        return self.getUserObj().userType;
    };


    self.setCurrentUser = function () {
        self.userData.sessionid = self.getUserToken();
        self.userData.currentUser = self.getUserObj();
    };

    self.setCurrentUser();

    self.checkUniqueValue21 = function (type, currentvalue) {
        var data = {
            "type": type,
            "currentvalue": currentvalue
        };
        //console.log(data);
        return false;
        // return $http.post("", data).then( function(res) {
        //   return res.data.isUnique;
        // });
    };

    self.checkUniqueValue = function (type, currentvalue) {
        var data = {
            "phone": currentvalue,
            "idtype": type
        };
        //console.log(data);
        //return false;
        return $http({
            method: 'POST',
            url: domain + 'checkuserifexists',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            //console.log(response);
            return response.data.CheckUserIfExistsResult;
        });
    };

    self.forgotpassword = function (forgot) {
        var data = {
            "email": forgot.email,
            "sessionid":''
        };
        //data=forgot.email;
        return $http({
            method: 'POST',
            url: domain + 'forgotpassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            return response;
        });
        //  $http.post(domain+"forgotassword", data).then( function(res) {
        //   return res;
        // });
    };

    self.resetpassword = function (resetpass) {
        var data = {
            "email": resetpass.email,
            "sessionid":resetpass.sessionid,
            "NewPass": resetpass.NewPass,
            "ConfNewPass": resetpass.ConfNewPass
        };
        //data=forgot.email;
        return $http({
            method: 'POST',
            url: domain + 'resetpassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            $state.go('login');
            return response;
        });
        //  $http.post(domain+"forgotassword", data).then( function(res) {
        //   return res;
        // });
    };

    self.resendotp = function (userid) {
        var data = {
            "userID": userid
        };
        return $http({
            method: 'POST',
            url: domain + 'sendotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            swal("Done!", "OTP send to your registered Mobile No.", "success");
        });
    };


    self.isnegotiationrunning = function () {
        var data = {
            "userID": self.getUserId(),
            "sessionID": self.getUserToken()
        };
        return $http({
            method: 'POST',
            url: domain + 'isnegotiationrunning',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            //console.log('isnegotiationrunning');
            return response;
        });
    };


    self.resendemailotp = function (userid) {
        var data = {
            "userID": userid
        };
        return $http({
            method: 'POST',
            url: domain + 'sendotpforemail',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            swal("Done!", "OTP send to your registered Email.", "success");
        });
    };


    self.getUserDataNoCache = function(){
        var params = {
            userid: self.getUserId(),
            sessionid: self.getUserToken()
        }
        return $http({
            method: 'GET',
            url: domain + 'getuserinfo?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {

            store.set('currentUser', response.data);
            self.setCurrentUser();
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }

    self.verifyOTP = function (otpobj) {
        var data = {
            "OTP": otpobj,
            "userID": self.getUserId(),
            "phone": otpobj.phone ? otpobj.phone : ""
        };
        return $http({
            method: 'POST',
            url: domain + 'verifyotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            if (response.data.sessionID != "" || response.data.userInfo.sessionID != "") {
                store.set('sessionid', response.data.sessionID != "" ? response.data.sessionID : response.data.userInfo.sessionID);
                if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
                    store.set('verified', 1);
                } else {
                    store.set('verified', 0);
                }
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                } else {
                    store.set('emailverified', 0);
                }
                if(response.data.userInfo.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                $state.go('pages.profile.profile-about');
            } else {
                swal('Warning', "The OTP is not valid. Please enter the valid OTP sent to your mobile number", "warning");
            }
            /*if(response.data.userInfo.isOTPVerified==1 && response.data.userInfo.credentialsVerified==1){
              store.set('verified',response.data.userInfo.credentialsVerified);
              $state.go('home');
              growlService.growl('Welcome to PRM360', 'inverse');                                                
            }*/
            return response.data;
        });
    };





    self.verifyEmailOTP = function (otpobj) {
        var data = {
            "OTP": otpobj,
            "userID": self.getUserId(),
            "email": otpobj.email ? otpobj.email : ""
        };
        return $http({
            method: 'POST',
            url: domain + 'verifyemailotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            if (response.data.sessionID != "" || response.data.userInfo.sessionID != "") {
                store.set('sessionid', response.data.sessionID != "" ? response.data.sessionID : response.data.userInfo.sessionID);
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                } else {
                    store.set('emailverified', 0);
                }
                if (response.data.userInfo.isOTPVerified == 1) {
                    store.set('verified', 1);
                } else {
                    store.set('verified', 0);
                }
                if(response.data.userInfo.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                $state.go('pages.profile.profile-about');
            } else {
                swal('Warning', "The OTP is not valid. Please enter the valid OTP sent to your Email", "warning");
            }
            /*if(response.data.userInfo.isOTPVerified==1 && response.data.userInfo.credentialsVerified==1){
              store.set('verified',response.data.userInfo.credentialsVerified);
              $state.go('home');
              growlService.growl('Welcome to PRM360', 'inverse');                                                
            }*/
            return response.data;
        });
    };









  		self.isLoggedIn = function () {
        return (self.getUserToken() && self.getOtpVerified() && self.getDocsVerified()) ? true : false;
  		};

    self.updateUser = function (params) {


        var params1 = {
            "user": {
                "userID": params.userID,
                "firstName": params.firstName,
                "lastName": params.lastName,
                "email": params.email,
                "phoneNum": params.phoneNum,
                "sessionID": params.sessionID,
                "isOTPVerified": params.isOTPVerified,
                "credentialsVerified": params.credentialsVerified,
                "errorMessage": params.errorMessage,
                "logoFile": params.logoFile ? params.logoFile : null,
                "logoURL": params.logoURL ? params.logoURL : "",
                "aboutUs": params.aboutUs ? params.aboutUs : "",
                "achievements": params.achievements ? params.achievements : "",
                "assocWithOEM": params.assocWithOEM ? params.assocWithOEM : false ,
                "assocWithOEMFile": params.assocWithOEMFile ? params.assocWithOEMFile : null ,                
                "assocWithOEMFileName": params.assocWithOEMFileName ? params.assocWithOEMFileName : "" ,                
                "clients": params.clients ? params.clients : "",
                "establishedDate": ("establishedDate" in params) ? params.establishedDate :'/Date(634334872000+0000)/',
                "products": params.products ? params.products : "",
                "strengths": params.strengths ? params.strengths : "",
                "responseTime": params.responseTime ? params.responseTime : "",
                "oemCompanyName": params.oemCompanyName ? params.oemCompanyName : "",
                "oemKnownSince": params.oemKnownSince? params.oemKnownSince : "",
                "files": [],
                "profileFile": params.profileFile ? params.profileFile : null ,                
                "profileFileName": params.profileFileName ? params.profileFileName : "",
                "workingHours": params.workingHours ? params.workingHours : "",
                "directors": params.directors ? params.directors : "",
                "address": params.address ? params.address : "",
                "subcategories": params.subcategories
            }
        }
        // if (params.hasOwnProperty("establishedDate")) {
        //     var establishedDate = params.establishedDate.replace(/(\d+)\/(\d+)\/(\d+)/, "$3/$2/$1");
        //     var date = new Date(establishedDate);
        //     console.log(date);
        //     var milliseconds = parseInt(date.getTime() / 1000.0);
        //     console.log(milliseconds);
        //     params1.user.establishedDate = "/Date(" + milliseconds + "000+0530)/";
        // }
        //console.log(params1);
        return $http({
            method: 'POST',
            url: domain + 'updateuserinfo',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params1
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
                    store.set('verified', 1);
                } if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                }
                if(response.data.userInfo.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                if (response.data.objectID > 0) {

                    growlService.growl('User data has updated Successfully!', 'inverse');
                    //self.getProfileDetails();
                    //$state.go('pages.profile.profile-about');
                    if(params.ischangePhoneNumber == 1){
                        //self.logout();
                        $state.go('login');
                        return responce.data;
                    }else{
                        $state.reload();
                    }
                    return "true";
                } else {
                    return "false";
                }
            } else if (response && response.data && response.data.errorMessage != "") {
                swal('Error', response.data.errorMessage, 'error');
                //store.set('emailverified', 1);
                return response.data.errorMessage;
                
            } else {
                return "Update failed";
            }
        }, function (result) {
            return "Update failed";
        });
    }

    self.getProfileDetails = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getuserdetails?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
        $state.go('pages.profile.profile-about');
    }

    self.updatePassword = function(params){
        return $http({
            method: 'POST',
            url: domain + 'changepassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }


    self.AddVendorsExcel = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'importentity',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }

    self.getuseraccess = function (userid, sessionid) {
        return $http({
            method: 'GET',
            url: domain + 'getuseraccess?userid=' + userid + '&sessionid=' + sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        }, function (result) {
            console.log("date error");
        });
    }





    self.saveUserAccess = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'saveuseraccess',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }




    self.getSubUsersData = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getsubuserdata?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
            }).then(function (response) {
                return response.data;
        });
    }



	   self.updateVerified = function (verified) {
        store.set('credverified', verified);
    }
    self.checkUserUniqueResult = function (uniquevalue, idtype) {
        var data = {
            "phone": uniquevalue,
            "idtype": idtype
        };
        return $http({
            method: 'POST',
            url: domain + 'checkuserifexists',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            return response.data.CheckUserIfExistsResult;
        });
    }
    self.login = function (user) {
        return $http({
            method: 'POST',
            url: domain + 'loginuser',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: user
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                if (response.data.userInfo.isOTPVerified == 1) {
                    store.set('verified', 1);
                }
                else if (response.data.userInfo.isOTPVerified == 0) {
                    self.resendotp(response.data.objectID);                    
                }
                if(response.data.userInfo.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                }
                else if (response.data.userInfo.isEmailOTPVerified == 0) {
                    self.resendemailotp(response.data.objectID);                    
                }

                $rootScope.entitlements = [];
                self.getuseraccess(response.data.objectID, response.data.sessionID)
               .then(function (responseObj) {
                   if (responseObj && responseObj.length > 0) {
                       $rootScope.entitlements = responseObj;
                       store.set('localEntitlement', $rootScope.entitlements);
                   }

                   $state.go('home');
               })
                
                return response.data;
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Login failed";
            }
        }, function (result) {
            return "Login failed";
        });
    };

    self.logout = function () {
        return $http({
            method: 'POST',
            data: { "userID": self.getUserId(), "sessionID": self.userData.sessionid },
            url: domain + 'logoutuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            self.removeUserToken();
            self.removeLocalEntitlement();
            self.removeUser();
            delete self.userData.sessionid;
            delete self.userData.currentUser;
            self.setMessage("Successfully Logged Out");
            $state.go('login');
        });
    };

    self.deleteUser = function(params){
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'deleteuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.activateUser = function(params){
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'activateuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.activatecompanyvendor = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'activatecompanyvendor',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }
    

    self.userregistration = function (register) {
        var params = {
            "userInfo": {
                "userID": "0",
                "firstName": register.firstName,
                "lastName": register.lastName,
                "email": register.email,
                "phoneNum": register.phoneNum,
                "username": register.username,
                "password": register.password,
                "birthday": '/Date(634334872000+0000)/',
                "userType": "CUSTOMER",
                "isOTPVerified": 0,
                "category": "",
                "institution": ("institution" in register) ? register.institution : "",
                "addressLine1": "",
                "addressLine2": "",
                "addressLine3": "",
                "city": "",
                "state": "",
                "country": "",
                "zipCode": "",
                "addressPhoneNum": "",
                "extension1": "",
                "extension2": "",
                "userData1": "",
                "registrationScore" : 0,
                "errorMessage": "",
                "sessionID": ""
            }
        };

        return $http({
            method: 'POST',
            url: domain + 'registeruser',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('verified', 0);
                store.set('emailverified', 0);
                if(response.data.userInfo.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                return response.data;
                //$state.go('home');
                //growlService.growl('Welcome to PRM360, To add new requirement Please use "+" at the Bottom.', 'inverse');
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Registeration failed";
            }
        }, function (result) {
            return "Registeration failed";
        });
    };

    self.vendorregistration = function (vendorregisterobj) {
        var vendorcat = [];
        vendorcat.push(vendorregisterobj.category);

        var params = {
            "vendorInfo": {
                "firstName": vendorregisterobj.firstName,
                "lastName": vendorregisterobj.lastName,
                "email": vendorregisterobj.email,
                "contactNum": vendorregisterobj.phoneNum,
                "username": vendorregisterobj.username,
                "password": vendorregisterobj.password,
                "institution": vendorregisterobj.institution ? vendorregisterobj.institution : "",
                "rating": 1,
                "isOTPVerified": 0,
                "category": vendorcat,
                "panNum": ("panno" in vendorregisterobj) ? vendorregisterobj.panno : "",
                "serviceTaxNum": ("taxno" in vendorregisterobj) ? vendorregisterobj.taxno : "",
                "vatNum": ("vatno" in vendorregisterobj) ? vendorregisterobj.vatno : "",
                "referringUserID": 0,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": ""
            }
        };
        return $http({
            method: 'POST',
            url: domain + 'addnewvendor',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "") {
                store.set('sessionid', response.data.sessionID);
                store.set('currentUser', response.data);
                store.set('verified', 0);
                store.set('emailverified', 0);
                if(response.data.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                self.setCurrentUser();
                return response.data;
                //growlService.growl('Welcome to PRM360, Please go thourgh with your Auctions', 'inverse');
                //return "Vendor Registration Successfully Completed.";
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Vendor Registration Failed.";
            }
        });
    };


    self.vendorregistration1 = function (vendorregisterobj) {
        var vendorcat = [];
        vendorcat.push(vendorregisterobj.category);
        //console.log(vendorregisterobj.role);
        var params = {
            "register": {
                "firstName": vendorregisterobj.firstName,
                "lastName": vendorregisterobj.lastName,
                "email": vendorregisterobj.email,
                "phoneNum": vendorregisterobj.phoneNum,
                "username": vendorregisterobj.username,
                "password": vendorregisterobj.password,
                "companyName": vendorregisterobj.institution ? vendorregisterobj.institution : "",
                "isOTPVerified": 0,
                "category": vendorregisterobj.category ? vendorregisterobj.category : "",
                "userType": vendorregisterobj.role,
                "panNumber": ("panno" in vendorregisterobj) ? vendorregisterobj.panno : "",
                "stnNumber": ("taxno" in vendorregisterobj) ? vendorregisterobj.taxno : "",
                "vatNumber": ("vatno" in vendorregisterobj) ? vendorregisterobj.vatno : "",
                "referringUserID": 0,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": "",
                "userID": 0,
                "department": "",
                "currency": vendorregisterobj.currency,
                "timeZone": vendorregisterobj.timeZone,
                "subcategories": vendorregisterobj.subcategories ? vendorregisterobj.subcategories : []
                
            }
        };
        return $http({
            method: 'POST',
            url: domain + 'register',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "") {
                store.set('sessionid', response.data.sessionID);
                store.set('currentUser', response.data.userInfo);
                store.set('verified', 0);
                store.set('emailverified', 0);
                if(response.data.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                self.setCurrentUser();
                return response.data;
                //growlService.growl('Welcome to PRM360, Please go thourgh with your Auctions', 'inverse');
                //return "Vendor Registration Successfully Completed.";
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Vendor Registration Failed.";
            }
        });
    };




    self.addnewuser = function (addnewuserobj) {
        var referringUserID = 0;
        referringUserID = self.getUserId();
        var params = {
            "register": {
                "firstName": addnewuserobj.firstName,
                "lastName": addnewuserobj.lastName,
                "email": addnewuserobj.email,
                "phoneNum": addnewuserobj.phoneNum,
                "username": addnewuserobj.email,
                "password": addnewuserobj.phoneNum,
                "companyName": addnewuserobj.companyName ? addnewuserobj.companyName : "",
                "isOTPVerified": 0,
                "category": addnewuserobj.category ? addnewuserobj.category : "",
                "userType": addnewuserobj.userType,
                "panNumber": "",
                "stnNumber": "",
                "vatNumber": "",
                "referringUserID": referringUserID,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": "",
                "userID": 0,
                "department": "",
                "currency": addnewuserobj.currency
            }
        };
        return $http({
            method: 'POST',
            url: domain + 'register',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            return response.data;
        });
    };


    self.GetCompanyVendors = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getcompanyvendors?userid=' + params.userID + '&sessionid=' + params.sessionID,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }


};


