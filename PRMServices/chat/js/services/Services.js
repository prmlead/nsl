prmApp.constant('PRMChatServiceDomain', 'chat/svc/PRMChatService.svc/REST/');
prmApp.constant('signalRChatHubName', 'PRMChatHub');
prmApp.service('PRMChatServices', ["PRMChatServiceDomain", "userService", "httpServices",
    function (PRMChatServiceDomain, userService, httpServices) {


        var PRMChatServices = this;


        PRMChatServices.getChatHistory = function (params) {
            var url = PRMChatServiceDomain + 'getchathistory?fromid=' + params.currentUserId + '&toid=' + params.chatUserId + '&moduleid=' + params.moduleId + '&module=' + params.module + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMChatServices.getModuleChatHistory = function (params) {
            var url = PRMChatServiceDomain + 'getmodulechathistory?moduleid=' + params.moduleId + '&module=' + params.module + '&user=' + params.user + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMChatServices.saveChatDetails = function (params) {
            let url = PRMChatServiceDomain + 'savechatdetails';
            return httpServices.post(url, params);
        };

        return PRMChatServices;

}]);