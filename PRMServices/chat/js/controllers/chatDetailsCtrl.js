﻿prmApp
    .controller('chatDetailsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService",
        "growlService", "PRMChatServices", "SignalRFactory", "signalRHubName",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMChatServices,
            SignalRFactory, signalRHubName) {

            $scope.currentUserId = userService.getUserId();
            $scope.sessionId = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.reqId = $stateParams.reqId;
            $scope.vendorId = $stateParams.vendorId;
            $scope.currentRequirementVendors = [];
            $scope.currentUserChatHistory = [];
            $scope.chatMessage = '';
            $scope.selectedUserToChat = {};
            $scope.requirementDetails = {};
            $scope.chatMessageObj = {
                ChatId: 0,
                Message: '',
                FromId: $scope.currentUserId,
                ToId: 0,
                ModuleId: $scope.reqId,
                Module: 'REQUIREMENT',
                sessionID: userService.getUserToken()
            };
            $scope.currentUserDisplayName = userService.getUserObj().firstName + ' ' + userService.getUserObj().lastName;

            var prmChatHub = SignalRFactory('', signalRHubName);
            $scope.reconnectHub = function () {
                if (prmChatHub) {
                    if (prmChatHub.getStatus() === 0) {
                        prmChatHub.reconnect();
                        return true;
                    }
                } else {
                    prmChatHub = SignalRFactory('', signalRHubName);
                }

            };

            $scope.$on("$destroy", function () {
                $log.info('disconecting signalR');
                prmChatHub.stop();
                $log.info('disconected signalR');
            });

            $scope.invokeSignalR = function (methodName, params, callback) {
                if ($scope.checkConnection() === 1) {
                    prmChatHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                } else {
                    $scope.reconnectHub();
                    prmChatHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                }
            };
            $scope.checkConnection = function () {
                if (prmChatHub) {
                    return prmChatHub.getStatus();
                } else {
                    return 0;
                }
            };

            prmChatHub.on('chatUpdate', function (payLoad) {
                //console.log(payLoad);
                if (payLoad && payLoad.FromId !== +$scope.currentUserId) {
                    if ($scope.isCustomer && payLoad.FromId === +$scope.selectedUserToChat.vendorID) {
                        payLoad.DateCreated = 'second ago';
                        $scope.currentUserChatHistory.push(payLoad);
                    }

                    if (!$scope.isCustomer && payLoad.ToId === +$scope.currentUserId) {
                        payLoad.DateCreated = 'second ago';
                        $scope.currentUserChatHistory.push(payLoad);
                    }
                }
            });

            $scope.invokeSignalR('joinGroup', 'chatGroup' + $scope.reqId);
            $log.info('connected to service');

            auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": $scope.sessionId, "userid": $scope.currentUserId })
                .then(function (response) {
                    $scope.requirementDetails = response;
                    $scope.currentRequirementVendors = response.auctionVendors;
                    if ($scope.currentRequirementVendors && $scope.currentRequirementVendors.length > 0) {
                        if ($scope.isCustomer && $scope.vendorId && $scope.vendorId > 0) {
                            $scope.currentRequirementVendors = $scope.currentRequirementVendors.filter(function (vendor) {
                                return vendor.vendorID === +$scope.vendorId;
                            });
                        }

                        if (!$scope.isCustomer) {
                            $scope.currentRequirementVendors = $scope.currentRequirementVendors.filter(function (vendor) {
                                return vendor.vendorID === +$scope.currentUserId;
                            });
                        }                       

                        if ($scope.currentRequirementVendors && $scope.currentRequirementVendors.length > 0) {
                            $scope.currentRequirementVendors[0].activeChat = true;
                            $scope.selectedUserToChat = $scope.currentRequirementVendors[0];
                            $scope.getChatHistory($scope.selectedUserToChat);
                        }

                        
                    }
                });

            $scope.getChatHistory = function (vendor) {
                $scope.currentUserChatHistory = [];
                $scope.currentRequirementVendors.forEach(function (vendorTemp, index) {
                    vendorTemp.activeChat = false;
                });
                vendor.activeChat = true;
                $scope.selectedUserToChat = vendor;
                var params = {
                    user: vendor.vendorID,
                    moduleId: $scope.reqId,
                    module: 'REQUIREMENT'
                };

                PRMChatServices.getModuleChatHistory(params)
                    .then(function (response) {
                        if (response) {
                            $scope.currentUserChatHistory = response;
                            $scope.currentUserChatHistory.forEach(function (chat, index) {
                                if (chat.DateCreated) {
                                    chat.DateCreated = userService.toLocalDate(chat.DateCreated);
                                }                               
                            });
                        }
                    });

            };

            $scope.saveUserChat = function () {
                if ($scope.chatMessage) {
                    $scope.resetChatMessage();
                    $scope.chatMessageObj.ToId = $scope.isCustomer ? $scope.selectedUserToChat.vendorID : $scope.requirementDetails.customerID;
                    $scope.chatMessageObj.FromName = userService.getFirstname() + ' ' + userService.getLastname();
                    $scope.chatMessageObj.Message = $scope.chatMessage;
                    $scope.chatMessageObj.IsCustomer = $scope.isCustomer;
                    var params = {
                        details: $scope.chatMessageObj
                    };

                    PRMChatServices.saveChatDetails(params)
                        .then(function (response) {
                            $scope.invokeSignalR('SendMessage', $scope.chatMessageObj);
                            $scope.chatMessageObj.DateCreated = 'second ago';
                            $scope.currentUserChatHistory.push($scope.chatMessageObj);
                            $scope.chatMessage = '';
                            $scope.resetChatMessage();
                        });
                }
            };

            $scope.resetChatMessage = function () {
                $scope.chatMessageObj = {
                    ChatId: 0,
                    Message: '',
                    FromId: $scope.currentUserId,
                    ToId: 0,
                    ModuleId: $scope.reqId,
                    Module: 'REQUIREMENT',
                    sessionID: userService.getUserToken()
                };
            };

            $scope.isIncomingChat = function (chat) {
                return +chat.FromId !== +$scope.currentUserId;
            };

        }]);