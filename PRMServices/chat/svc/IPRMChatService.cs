﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMChatService
    {

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getchathistory?fromid={fromid}&toid={toid}&moduleid={moduleid}&module={module}&sessionid={sessionid}")]
        List<ChatHistory> GetChatHistory(int fromid, int toid, int moduleid, string module, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmodulechathistory?moduleid={moduleid}&module={module}&user={user}&sessionid={sessionid}")]
        List<ChatHistory> GetModuleChatHistory(int moduleid, string module, int user, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "savechatdetails")]
        Response SaveChatDetails(ChatHistory details);
    }
}
