﻿using PRMServices.Common;
using PRMServices.Models;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel.Activation;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMSlotService : IPRMSlotService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();


        //This is To Save Slots By Customers
        public Response SaveSlotDetails(SlotDetails sltDets, string sessionID,int customerUserID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response slotDetails = new Response();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_SLOT_ID", sltDets.Slot_id);
                sd.Add("P_SLOT_DESCRIPTION", sltDets.Slot_description);
                sd.Add("P_SLOT_ENTITY_ID", sltDets.Slot_entity_id);
                sd.Add("P_SLOT_VENDORS", sltDets.Slot_no_of_vendors);
                sd.Add("P_SLOT_START_TIME", sltDets.Slot_Start_Time);
                sd.Add("P_SLOT_END_TIME", sltDets.Slot_End_Time);
                sd.Add("P_SLOT_IS_VALID", sltDets.Slot_is_valid);
                sd.Add("P_SLOT_ENTITY_TYPE", sltDets.Slot_entity_type);
                sd.Add("P_SLOT_USER", customerUserID);
                sd.Add("P_SLOT_FLAG", sltDets.Slot_flag);
                
                DataSet ds = sqlHelper.SelectList("slt_SaveSlotDetails", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    slotDetails.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    slotDetails.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

            }
            catch (Exception ex)
            {
                sltDets.ErrorMessage = ex.Message;
                logger.Error(ex.Message);
            }

            return slotDetails;
        }

        // This is For Vendors To show Which Slots Are Available
        public List<SlotDetails> GetSlotDetails(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<SlotDetails> sltdetails = new List<SlotDetails>();
           // List<SlotBooking> bookedslots = new List<SlotBooking>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("slt_GetSlotDetails", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        SlotDetails sltdet = new SlotDetails();
                        sltdet.Slot_id = row["slot_id"] != DBNull.Value ? Convert.ToInt32(row["slot_id"]) : 0;
                        sltdet.Slot_description = row["slot_description"] != DBNull.Value ? Convert.ToString(row["slot_description"]) : string.Empty;
                        sltdet.Slot_entity_id = row["slot_entity_id"] != DBNull.Value ? Convert.ToInt32(row["slot_entity_id"]) : 0;
                        sltdet.Slot_Start_Time = row["slot_start_time"] != DBNull.Value ? Convert.ToDateTime(row["slot_start_time"]) : DateTime.UtcNow;
                        sltdet.Slot_End_Time = row["slot_end_time"] != DBNull.Value ? Convert.ToDateTime(row["slot_end_time"]) : DateTime.UtcNow;
                        sltdet.Slot_is_valid = row["slot_is_valid"] != DBNull.Value ? Convert.ToInt32(row["slot_is_valid"]) : 0;
                        sltdet.Slot_no_of_vendors = row["slot_vendors"] != DBNull.Value ? Convert.ToInt32(row["slot_vendors"]) : 0;
                        sltdet.Slot_entity_type = row["slot_entity_type"] != DBNull.Value ? Convert.ToString(row["slot_entity_type"]) : string.Empty;
                        sltdet.Slot_booked_count = row["slot_booked_count"] != DBNull.Value ? Convert.ToInt32(row["slot_booked_count"]) : 0;
                        
                        sltdet.VENDORS_SLOT_BOOKED = row["VENDORS_SLOT_BOOKED"] != DBNull.Value ? Convert.ToString(row["VENDORS_SLOT_BOOKED"]) : string.Empty;
                        sltdet.VENDORS_SLOT_CANCELED = row["VENDORS_SLOT_CANCELED"] != DBNull.Value ? Convert.ToString(row["VENDORS_SLOT_CANCELED"]) : string.Empty;
                        sltdet.VENDORS_SLOT_REJECTED = row["VENDORS_SLOT_REJECTED"] != DBNull.Value ? Convert.ToString(row["VENDORS_SLOT_REJECTED"]) : string.Empty;
                        sltdet.VENDORS_SLOT_APPROVED = row["VENDORS_SLOT_APPROVED"] != DBNull.Value ? Convert.ToString(row["VENDORS_SLOT_APPROVED"]) : string.Empty;

                        sltdetails.Add(sltdet);
                    }
                    //foreach (DataRow row in ds.Tables[1].Rows)
                    //{
                    //    SlotBooking bookSlot = new SlotBooking();
                    //    bookSlot.Slot_id = row["slot_id"] != DBNull.Value ? Convert.ToInt32(row["slot_id"]) : 0;
                    //    bookSlot.Slot_vendor_id = row["slot_vendor_id"] != DBNull.Value ? Convert.ToInt32(row["slot_vendor_id"]) : 0;
                    //    bookSlot.Slot_entity_id = row["slot_entity_id"] != DBNull.Value ? Convert.ToInt32(row["slot_entity_id"]) : 0;
                    //    bookSlot.Slot_is_valid = row["slot_is_valid"] != DBNull.Value ? Convert.ToInt32(row["slot_is_valid"]) : 0;
                    //    bookedslots.Add(bookSlot);
                    //}
                }
            }
            catch (Exception ex)
            {
                SlotDetails slots = new SlotDetails();
                slots.ErrorMessage = ex.Message;
                sltdetails.Add(slots);
                logger.Error(ex.Message);
            }

            return sltdetails;
        }
        
        // This is to show which Vendors have booked Slots
        public List<SlotBooking> GetBookedSlotDetails(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<SlotBooking> bookedslots = new List<SlotBooking>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("slt_GetSlotDetails", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        SlotBooking bookSlot = new SlotBooking();
                        bookSlot.Slot_id = row["slot_id"] != DBNull.Value ? Convert.ToInt32(row["slot_id"]) : 0;
                        bookSlot.Slot_vendor_id = row["slot_vendor_id"] != DBNull.Value ? Convert.ToInt32(row["slot_vendor_id"]) : 0;
                        bookSlot.Slot_entity_id = row["slot_entity_id"] != DBNull.Value ? Convert.ToInt32(row["slot_entity_id"]) : 0;
                        bookSlot.Slot_is_valid = row["slot_is_valid"] != DBNull.Value ? Convert.ToInt32(row["slot_is_valid"]) : 0;
                        bookedslots.Add(bookSlot);



                    }
                }
            }
            catch (Exception ex)
            {
                SlotBooking bookSlot = new SlotBooking();
                bookSlot.ErrorMessage = ex.Message;
                bookedslots.Add(bookSlot);
                logger.Error(ex.Message);
            }

            return bookedslots;
        }

        //This is To Book A Slot By Vendors
        public Response BookSlots(SlotBooking sltBook,string sessionID) {

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response slotBook = new Response();

            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_SLOT_ID", sltBook.Slot_id);
                sd.Add("P_SLOT_VENDOR_ID", sltBook.Slot_vendor_id);
                sd.Add("P_SLOT_IS_VALID", sltBook.Slot_is_valid);
                sd.Add("P_SLOT_ENTITY_ID", sltBook.Slot_entity_id);

                DataSet ds = sqlHelper.SelectList("slt_BookSlots", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    slotBook.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    slotBook.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

                SlotBooking book = new SlotBooking();
                book = GetSlotDetailsByID(sltBook.Slot_id, sltBook.Slot_entity_id, sltBook.Slot_vendor_id,sessionID,sltBook.Slot_vendor_id);
                SlotUtility.BookSlotsFromVendorAlertsToCustomer(sltBook.Slot_entity_id, sltBook.Slot_vendor_id, sessionID, book);
                //a.BookSlotsFromVendorAlerts();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }


            return slotBook;
        }
        
        //This is To Approve Slots By Customers
        public Response ApproveRejectSlot(int flag, int vendorId, int reqId, string sessionID, int customerID, int slotID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response slotDetails = new Response();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_IS_APPROVED", flag);
                sd.Add("P_VENDOR_ID", vendorId);
                sd.Add("P_REQ_ID", reqId);

                DataSet ds = sqlHelper.SelectList("slt_ApproveSlots", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    slotDetails.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    slotDetails.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

                SlotBooking book = new SlotBooking();
                book = GetSlotDetailsByID(slotID, reqId, vendorId, sessionID, customerID);
                SlotUtility.BookSlotsFromVendorAlertsToCustomer(reqId, vendorId, sessionID, book);

            }
            catch (Exception ex)
            {
                slotDetails.ErrorMessage = ex.Message;
                logger.Error(ex.Message);
            }

            return slotDetails;
        }
        
        public SlotBooking GetSlotDetailsByID(int slotID,int entityID,int VendorID,string sessionID, int userID)
        {

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            SlotBooking slotBook = new SlotBooking();

            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_SLOT_ID", slotID);
                sd.Add("P_SLOT_ENTITY_ID", entityID);
                sd.Add("P_SLOT_VENDOR_ID", VendorID);
                sd.Add("P_USER_ID", userID);

                DataSet ds = sqlHelper.SelectList("slt_GetSlotDetailsBySlotID", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    slotBook.Slot_id = row["slot_id"] != DBNull.Value ? Convert.ToInt32(row["slot_id"]) : 0;
                    slotBook.Slot_description = row["slot_description"] != DBNull.Value ? Convert.ToString(row["slot_description"]) : string.Empty;     
                    slotBook.Slot_Start_Time = row["slot_start_time"] != DBNull.Value ? Convert.ToDateTime(row["slot_start_time"]) : DateTime.UtcNow;
                    slotBook.Slot_End_Time = row["slot_end_time"] != DBNull.Value ? Convert.ToDateTime(row["slot_end_time"]) : DateTime.UtcNow;
                    slotBook.Slot_is_valid = row["slot_is_valid"] != DBNull.Value ? Convert.ToInt32(row["slot_is_valid"]) : 0;
                    slotBook.Is_slot_approved = row["IS_SLOT_APPROVED"] != DBNull.Value ? Convert.ToInt32(row["IS_SLOT_APPROVED"]) : 0;
                    slotBook.IS_CUSTOMER = row["IS_CUSTOMER"] != DBNull.Value ? (Convert.ToString(row["IS_CUSTOMER"]) == "CUSTOMER" ? true : false) : false;
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }


            return slotBook;
        }




        public Response DeleteSlot(int slotID, int entityID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response deleteSlotDetails = new Response();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_SLOT_ID", slotID);
                sd.Add("P_ENTITY_ID", entityID);

                DataSet dataset = sqlHelper.SelectList("slt_DeleteSlot", sd);
                if (dataset != null && dataset.Tables.Count > 0)
                {
                    deleteSlotDetails.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    deleteSlotDetails.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                deleteSlotDetails.ErrorMessage = ex.Message;
                logger.Error(ex.Message);
            }

            return deleteSlotDetails;
        }

    }
}