﻿
prmApp.constant('opsDomain', 'svc/PRMOpsService.svc/REST/');

prmApp

    .service('opsServices', ["opsDomain", "userService", "httpServices", "$window", function (opsDomain, userService, httpServices, $window) {

        var opsServices = this;



        opsServices.GetClients = function (from, to) {
            let url = opsDomain + 'GetClients?from=' + from + '&to=' + to + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetClientDetails = function (domain) {
            let url = opsDomain + 'GetClientDetails?domain=' + domain + '&sessionID=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetClientsRequirements = function (clientid, domain, from, to, auctionType) {
            let url = opsDomain + 'GetClientsRequirements?clientid=' + clientid + '&domain=' + domain + '&from=' + from + ' &to=' + to + '&auctionType=' + auctionType + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetClientsVendors = function (reqid, domain, clientid, auctionType) {
            let url = opsDomain + 'GetClientsVendors?reqid=' + reqid + '&domain=' + domain + '&clientid=' + clientid + '&auctionType=' + auctionType + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetInfo = function (reqid, type, domain, from, to) {
            let url = opsDomain + 'GetInfo?reqid=' + reqid + '&type=' + type + '&domain=' + domain + '&from=' + from + ' &to=' + to;
            return httpServices.get(url);
        };

        opsServices.GetVendorsOnline = function (reqid, userid, domain, auctionType) {
            let url = opsDomain + 'GetVendorsOnline?reqid=' + reqid + '&userid=' + userid + '&domain=' + domain + '&auctionType=' + auctionType + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetPendingQuotations = function (reqid, clientid, domain, auctionType) {
            let url = opsDomain + 'GetPendingQuotations?reqid=' + reqid + '&clientid=' + clientid + '&domain=' + domain + '&auctionType=' + auctionType + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetAllQuotationsVendors = function (reqid, clientid, domain, auctionType) {
            let url = opsDomain + 'GetAllQuotationsVendors?reqid=' + reqid + '&clientid=' + clientid + '&domain=' + domain + '&auctionType=' + auctionType + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };



        opsServices.GetReminders = function (reqid, clientid, domain) {
            let url = opsDomain + 'GetReminders?reqid=' + reqid + '&clientid=' + clientid + '&domain=' + domain + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);

        };

        opsServices.GetVendorDetails = function (vendorID, domain) {
            let url = opsDomain + 'GetVendorDetails?vendorID=' + vendorID + '&domain=' + domain + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);

        };

        //opsServices.GetReportsTemplates = function (name, domain, from, to) {
        //    let url = opsDomain + 'GetReportsTemplates?name=' + name + '&domain=' + domain + '&from=' + from + ' &to=' + to + '&sessionID=' + userService.getUserToken();
        //    return httpServices.get(url);

        //};

        opsServices.GetReportsTemplates = function (name, domain, from, to) {
            //let url = reportingDomain + 'getconsolidatedtemplates?template=' + template + '&from=' + from + '&to=' + to + '&userid=' + userid + '&userType=' + userType + '&sessionid=' + userService.getUserToken();
            let url = opsDomain + 'GetReportsTemplates?name=' + name + '&domain=' + domain + '&from=' + from + ' &to=' + to + '&sessionID=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", name + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        opsServices.GetTemplates = function (name, domain, from, to) {
            //let url = reportingDomain + 'getconsolidatedtemplates?template=' + template + '&from=' + from + '&to=' + to + '&userid=' + userid + '&userType=' + userType + '&sessionid=' + userService.getUserToken();
            let url = opsDomain + 'GetTemplates?name=' + name + '&domain=' + domain + '&from=' + from + ' &to=' + to + '&sessionID=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", name + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }


        opsServices.GetDomain = function (params) {
            let url = opsDomain + 'getDomain?domain=' + params.domain;
            return httpServices.get(url);

        };

        opsServices.SendOTP = function (params) {
            let url = opsDomain + 'SendOTP';
            return httpServices.post(url, params);

        };

        opsServices.updatePassword = function (params) {
            let url = opsDomain + 'changepassword';
            return httpServices.post(url, params);

        };

        opsServices.SendPassword = function (params) {
            let url = opsDomain + 'SendPassword';
            return httpServices.post(url, params);

        };

        opsServices.GetVendorNeedTraining = function (clientid, domain, from, to, auctionType, isLgst, compName) {
            let url = opsDomain + 'GetVendorNeedTraining?clientid=' + clientid + '&domain=' + domain + '&from=' + from + '&to=' + to + '&auctionType=' + auctionType + '&isLgst=' + isLgst + '&compName=' + compName + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetReports = function (domain, from, to) {
            let url = opsDomain + 'GetReports?domain=' + domain + '&from=' + from + '&to=' + to + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetAssignetTickets = function () {
            let url = opsDomain + 'GetAssignetTickets?sessionID=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.SaveVendorTraining = function (params) {
            let url = opsDomain + 'SaveVendorTraining';
            return httpServices.post(url, params);

        };

        opsServices.GetVendorTrainingHistory = function (vtID, reqID, vendorID, domain) {
            let url = opsDomain + 'GetVendorTrainingHistory?vtID=' + vtID + '&reqID=' + reqID + ' &vendorID=' + vendorID + '&domain=' + domain;
            return httpServices.get(url);
        };


        opsServices.GetALLVendorTrainingDetails = function (domain, from, to) {
            let url = opsDomain + 'GetALLVendorTrainingDetails?sessionID=' + userService.getUserToken() + '&domain=' + domain + '&from=' + from + '&to=' + to;
            return httpServices.get(url);
        };

        opsServices.GetAllClientsVendorTrainingDetails = function (from, to) {
            let url = opsDomain + 'GetAllClientsVendorTrainingDetails?sessionID=' + userService.getUserToken() + '&from=' + from + ' &to=' + to;
            return httpServices.get(url);
        };

        opsServices.GetRegularRfqReport = function (domain, from, to) {
            let url = opsDomain + 'GetRegularRfqReport?domain=' + domain + '&from=' + from + '&to=' + to + '&sessionID=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetLogisticsRfqReport = function (domain, from, to) {
            let url = opsDomain + 'GetLogisticsRfqReport?domain=' + domain + '&from=' + from + '&to=' + to + '&sessionID=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetSubUserDataReport = function (domain, from, to) {
            let url = opsDomain + 'GetSubUserDataReport?domain=' + domain + '&from=' + from + '&to=' + to + '&sessionID=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetVendorRegisteredReport = function (domain, from, to) {
            let url = opsDomain + 'GetVendorRegisteredReport?domain=' + domain + '&from=' + from + '&to=' + to + '&sessionID=' + userService.getUserToken();
            return httpServices.get(url);
        };

       

        opsServices.SaveAssignTickets = function (params) {
            let url = opsDomain + 'SaveAssignTickets';
            return httpServices.post(url, params);

        };

        opsServices.GetVendorRfqSearch = function (domain, from, to, email, phone) {
            let url = opsDomain + 'GetVendorRfqSearch?domain=' + domain + '&from=' + from + '&to=' + to + '&email=' + email + '&phone=' + phone + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        return opsServices;
    }]);