﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
               
                .state('opsReports', {
                    url: '/opsReports',
                    templateUrl: 'Ops/views/opsReports.html'

                })

                .state('opsVendorReqSearch', {
                    url: '/opsVendorReqSearch',
                    templateUrl: 'Ops/views/opsVendorReqSearch.html'

                })

               
                .state('opsAsigneeTickets', {
                    url: '/opsAsigneeTickets',
                    templateUrl: 'Ops/views/opsAsigneeTickets.html'
                })

        }]);