﻿prmApp
    .controller('opsAsigneeTicketsCtrl', ['$scope', '$state', '$stateParams', 'opsServices', 'userService', 'growlService', '$http', function ($scope, $state, $stateParams, opsServices, userService, growlService, $http) {

        $scope.sessionID = userService.getUserToken();

        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 8;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
        };

        $scope.trainer = 'All';
        $scope.source = 'All';

        $scope.filterList = {
            tagsList: [],
            issueTypeList: [],
            clientNameList: []
        }
        $scope.responderTicket = [];
        $scope.responderTicketTemp = [];
        $scope.tableView = true;
        $scope.violationView = false;
        $scope.selectedStatus = [];
        $scope.selectedTags = [];
        $scope.selectedtype = [];
        $scope.selectedClient = [];
        $scope.statusList = ['Open', 'Pending', 'Resolved', 'Closed', 'Awaiting response', 'Trainings Scheduled', 'Pending Training Confirmation'];
        $scope.closedToDate = '';
        $scope.closedFromDate = '';
        $scope.closedDateFilter = 'today';

        $scope.transformData = function (page) {
            $scope.filterList.tagsList = [];
            $scope.filterList.issueTypeList = [];
            $scope.filterList.clientNameList = [];
            $scope.trainerCount = {};
            $scope.sourceCount = {};

            $scope.responderTicket.forEach(function (item) {
                item.responderName = '';
                item.sourceText = '';
                if (item.created_at.includes("T") || item.created_at.includes("Z")) {
                    item.created_at = new moment(item.created_at).format("DD-MM-YYYY HH:mm");
                } else {
                    item.created_at = item.created_at;
                }

                if (item.updated_at.includes("T") || item.updated_at.includes("Z")) {
                    item.updated_at = new moment(item.updated_at).format("DD-MM-YYYY HH:mm");
                } else {
                    item.updated_at = item.updated_at;
                }


                if (item.status == 2) {
                    item.status = 'Open'
                } else if (item.status == 3) {
                    item.status = 'Pending'
                } else if (item.status == 4) {
                    item.status = 'Resolved'
                } else if (item.status == 5) {
                    item.status = 'Closed'
                } else if (item.status == 8) {
                    item.status = 'Awaiting response'
                } else if (item.status == 7) {
                    item.status = 'Trainings Scheduled'
                } else if (item.status == 6) {
                    item.status = 'Pending Training Confirmation'
                }


                if (item.priority == 1) {
                    item.priority = 'Low'
                } else if (item.priority == 2) {
                    item.priority = 'Medium'
                } else if (item.priority == 3) {
                    item.priority = 'High'
                } else if (item.priority == 4) {
                    item.priority = 'Urgent'
                } else {
                    item.priority = ''
                }

                if ($scope.sourceCount[item.source]) {
                    $scope.sourceCount[item.source]++;
                } else {
                    $scope.sourceCount[item.source] = 1;
                }


                if (item.source == 1) {
                    item.sourceText = 'Email'
                } else if (item.source == 2) {
                    item.sourceText = 'Portal'
                } else if (item.source == 3) {
                    item.sourceText = 'Phone'
                } else if (item.source == 7) {
                    item.sourceText = 'Chat'
                } else if (item.source == 9) {
                    item.sourceText = 'Feedback Widget'
                } else if (item.source == 10) {
                    item.sourceText = 'Outbound Email'
                } else {
                    item.sourceText = ''
                }



                if ($scope.trainerCount[item.responder_id]) {
                    $scope.trainerCount[item.responder_id]++;
                } else {
                    $scope.trainerCount[item.responder_id] = 1;
                }

                if (item.responder_id == 81024420267) {
                    item.responderName = 'Karan'
                } else if (item.responder_id == 81024420713) {
                    item.responderName = 'Srilekha'
                } else if (item.responder_id == 81025035308) {
                    item.responderName = 'Sanjana'
                } else if (item.responder_id == 81024420107) {
                    item.responderName = 'Jasjeet'
                } else if (item.responder_id == 81024420183) {
                    item.responderName = 'Kabita'
                } else if (item.responder_id == 81024420620) {
                    item.responderName = 'Pavan'
                } else if (item.responder_id == 81024420515) {
                    item.responderName = 'Manoj'
                } else if (item.responder_id == 81024420378) {
                    item.responderName = 'Kshithija'
                }

                if (item.tags) {
                    item.tags.split(',').forEach(function (tag) {
                        if ($scope.filterList.tagsList.indexOf(tag) < 0) {
                            $scope.filterList.tagsList.push(tag);
                        }
                    });
                }

                if (item.type) {
                    item.type.split(',').forEach(function (issueType) {
                        if ($scope.filterList.issueTypeList.indexOf(issueType) < 0) {
                            $scope.filterList.issueTypeList.push(issueType);
                        }
                    });
                }

                if (item.sourceName) {

                    if ($scope.filterList.clientNameList.indexOf(item.sourceName) < 0) {
                        $scope.filterList.clientNameList.push(item.sourceName);
                    }
                }

            })

            // $scope.getTicket(page + 1);

            $scope.responderTicketTemp = $scope.responderTicket;
            $scope.totalItems = $scope.responderTicket.length;

            $scope.summaryView();
            $scope.violationView();
        }
        $scope.responderTicketAllData = [];
        //let RefreshTime = "";
        $scope.RefreshTime = '';
        $scope.GetAssignetTickets = function (page) {
            opsServices.GetAssignetTickets().then(function (response) {

                if (response) {
                    $scope.responderTicketAllData = response;
                    $scope.RefreshTime = moment(response[0].refreshTime).utc().format("YYYY-MM-DDTHH:mm:ss[Z]");
                    $scope.RefreshTimeDisplay = moment(response[0].refreshTime).format("YYYY-MM-DD HH:mm:ss");
                    $scope.responderTicketAllData.forEach(function (item) {
                        item.tickectID = parseInt(item.tickectID)
                        $scope.responderTicket.push(item);

                    })
                    $scope.responderTicket = _.orderBy($scope.responderTicket, ['tickectID'], ['desc']);

                    $scope.transformData(page);

                }
            })
        }
        $scope.GetAssignetTickets();

        $scope.count = {
            insertCount: 0,
            updateCount: 0
        };
        $scope.ticket = {
            source: 0,
            status: "",
            created_at: "",
            updated_at: "",
            type: "",
            to_emails: "",
            tags: "",
            subject: "",
            sourceName: "",
            tickectID: "",
            comments: "",
            trainingMode: "",
            responderName: "",
            responder_id: 0,
            requester_id: 0,
            group_id: 0,
            priority: ""
        };
        $scope.AllTickets = [];
        $scope.UpdateTickets = [];
        $scope.refreshTickets = [];

        $scope.processRefreshTicket = function () {
            var time = moment().subtract(1, 'days').utc().format("YYYY-MM-DDTHH:mm:ss[Z]");
            var newRefreshTime = moment().format("YYYY-MM-DD HH:mm:ss");
            $scope.AllTickets = [];
            $scope.UpdateTickets = [];
            // let UnMatchedData = _.differenceWith(response.data, $scope.responderTicketAllData, _.isEqual);

            var UnMatchedData = $scope.refreshTickets.filter(function (obj) {
                return !$scope.responderTicketAllData.some(function (obj2) {
                    obj2.tickectID = parseInt(obj2.tickectID);
                    return obj.id == obj2.tickectID;
                });
            });

            var UpdateData = $scope.refreshTickets.filter(function (obj) {
                obj.updated_at = new moment(obj.updated_at).format("DD-MM-YYYY HH:mm");
                obj.created_at = new moment(obj.created_at).format("DD-MM-YYYY HH:mm");
                return !$scope.responderTicketAllData.some(function (obj2) {
                    return obj.updated_at == obj2.updated_at;
                });
            });

            var UpdateDataTemp = UpdateData.filter(function (obj) {
                return !UnMatchedData.some(function (obj2) {
                    return obj.id == obj2.id;
                });
            })

            UnMatchedData.forEach(function (item) {
                // $scope.responderTicket.push(item);
                $scope.ticket = {
                    source: item.source, status: item.status,
                    created_at: item.created_at, updated_at: item.updated_at,
                    type: item.type, to_emails: item.to_emails ? item.to_emails.toString() : "",
                    tags: item.tags ? item.tags.toString() : "", subject: item.subject,
                    sourceName: item.custom_fields.cf_client, tickectID: item.id,
                    comments: "", trainingMode: "",
                    responderName: item.responderName, responder_id: item.responder_id ? item.responder_id : 0,
                    requester_id: item.requester_id, group_id: item.group_id,
                    priority: item.priority,
                    refreshTime: newRefreshTime
                };
                $scope.AllTickets.push($scope.ticket);
            });

            UpdateDataTemp.forEach(function (item) {

                $scope.ticket = {
                    source: item.source, status: item.status,
                    created_at: item.created_at, updated_at: item.updated_at,
                    type: item.type, to_emails: item.to_emails ? item.to_emails.toString() : "",
                    tags: item.tags ? item.tags.toString() : "", subject: item.subject,
                    sourceName: item.custom_fields.cf_client, tickectID: item.id,
                    comments: "", trainingMode: "",
                    responderName: "", responder_id: item.responder_id ? item.responder_id : 0,
                    requester_id: item.requester_id, group_id: item.group_id,
                    priority: item.priority,
                    refreshTime: newRefreshTime
                };
                $scope.UpdateTickets.push($scope.ticket);
            });
            var params = {
                AssignedTickets: $scope.AllTickets,
                UpdateData: $scope.UpdateTickets
            }


            opsServices.SaveAssignTickets(params).then(function (response) {
                if (response.errorMessage != "") {
                    growlService.growl(response.errorMessage, "inverse");

                }
                else if (response.errorMessage == "") {
                    $scope.count.insertCount = response.insertCount;
                    $scope.count.updateCount = response.updateCount;
                    growlService.growl("Saved Successfully");
                    //$scope.getTicket(page + 1);
                    $scope.GetAssignetTickets(page + 1);
                }

            })


            location.reload();
        }

        $scope.getTicket = function (page) {

            $http({
                method: 'GET',
                //url: "https://prm360.freshdesk.com/api/v2/tickets?per_page=100&page="+page,
                url: "https://prm360.freshdesk.com/api/v2/tickets?updated_since=" + $scope.RefreshTime + "&per_page=100&page=" + page,

                headers: {
                    'Content-Type': 'application/json',
                    "Authorization": "Basic " + window.btoa('oLRuHDsQvII8LrfthZAj' + ":x")
                },
                dataType: "json"
            }).then(function (response) {
                if (response.data && response.data.length > 0) {
                    response.data.forEach(function (item) {
                        $scope.refreshTickets.push(item);
                    });
                    if (response.data.length < 100) {
                        $scope.processRefreshTicket()
                        return
                    } else {
                        $scope.getTicket(page + 1);
                    }
                }

                //location.reload();
            });
        };

        // $scope.getTicket(1);
        $scope.getTicket1 = function () {
            var time = moment().utc().format("YYYY-MM-DDTHH:mm:ss[Z]");
        }

        function remove_duplicates(a, b) {
            for (var i = 0, len = a.length; i < len; i++) {
                for (var j = 0, len = b.length; j < len; j++) {
                    if (a[i].id == b[j].tickectID) {
                        b.splice(j, 1);
                    }
                }
            }

        }


        $scope.summaryView = function () {
            $scope.summaryViewArray = [];

            $scope.filterList.tagsList.forEach(function (cilent) {
                var tickets = $scope.responderTicket.filter(function (item) {
                    return item.tags.split(',').indexOf(cilent) > -1;
                });
                var obj = {
                    clientName: cilent,
                    total: tickets.length,
                    trainers: []
                }
                tickets.forEach(function (ticket) {
                    var temp = _.find(obj.trainers, function (trainer) { return trainer.name === ticket.responderName });
                    if (temp) {
                        temp.total++;
                        if (temp[ticket.status]) {
                            temp[ticket.status]++;
                        } else {
                            temp[ticket.status] = 1;
                        }
                    } else {
                        obj.trainers.push({
                            name: ticket.responderName,
                            total: 1,
                            [ticket.status]: 1
                        });
                    }
                });

                $scope.summaryViewArray.push(obj);
                $scope.summaryViewArray = _.orderBy($scope.summaryViewArray, ['total'], ['desc']);

            });


            $scope.summaryViewTrainerArray = [];
            $scope.responderTicket.forEach(function (ticket) {
                var temp1 = _.find($scope.summaryViewTrainerArray, function (trainer) { return trainer.name === ticket.responderName });
                if (temp1) {
                    temp1.total++;
                    if (temp1[ticket.status]) {
                        temp1[ticket.status]++;
                    } else {
                        temp1[ticket.status] = 1;
                    }
                } else {
                    $scope.summaryViewTrainerArray.push({
                        name: ticket.responderName,
                        total: 1,
                        [ticket.status]: 1
                    });
                }

            })
            $scope.summaryViewTrainerArray = _.orderBy($scope.summaryViewTrainerArray, ['total'], ['desc']);
        }

        $scope.violationView = function () {
            $scope.violationViewTagsArray = [];
            $scope.violationViewGroupArray = [];
            $scope.violationViewTypeArray = [];
            $scope.ticketsClosedArray = [];
            $scope.responderTicket.forEach(function (ticket) {
                if (ticket.responderName == '') {
                    ticket.responderName = 'Undefined';
                }
                if (ticket.tags == '') {
                    var temp2 = _.find($scope.violationViewTagsArray, function (trainer) { return trainer.name === ticket.responderName });
                    if (temp2) {
                        temp2.total++;

                    } else {
                        $scope.violationViewTagsArray.push({
                            name: ticket.responderName,
                            total: 1
                        });
                    }
                }
                if (ticket.group_id == '') {
                    var temp3 = _.find($scope.violationViewGroupArray, function (trainer) { return trainer.name === ticket.responderName });
                    if (temp3) {
                        temp3.total++;

                    } else {
                        $scope.violationViewGroupArray.push({
                            name: ticket.responderName,
                            total: 1
                        });
                    }
                }
                if (ticket.type == '') {
                    var temp4 = _.find($scope.violationViewTypeArray, function (trainer) { return trainer.name === ticket.responderName });
                    if (temp4) {
                        temp4.total++;

                    } else {
                        $scope.violationViewTypeArray.push({
                            name: ticket.responderName,
                            total: 1
                        });
                    }
                }

            })

            $scope.violationViewTagsArray = _.orderBy($scope.violationViewTagsArray, ['total'], ['desc']);
            $scope.violationViewGroupArray = _.orderBy($scope.violationViewGroupArray, ['total'], ['desc']);
            $scope.violationViewTypeArray = _.orderBy($scope.violationViewTypeArray, ['total'], ['desc']);



        }

        $scope.closedView = function () {
            var ticketsClosed = [];
            $scope.ticketsClosedArray = [];

            if ($scope.closedDateFilter) {
                $scope.closedFromDate = '';
                $scope.closedToDate = '';
            }
            if ($scope.closedFromDate && $scope.closedToDate) {

                ticketsClosed = $scope.responderTicketTemp.filter(function (item) {
                    return (moment(item.updated_at, 'DD-MM-YYYY') >= moment($scope.closedFromDate, 'DD-MM-YYYY') && moment(item.updated_at, 'DD-MM-YYYY') <= moment($scope.closedToDate, 'DD-MM-YYYY')) && item.status == 'Closed';

                });
            }

            if ($scope.closedDateFilter == 'today') {
                ticketsClosed = $scope.responderTicketTemp.filter(function (item) {
                    return moment(item.updated_at, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().format('DD-MM-YYYY') && item.status == 'Closed';
                });

            } else if ($scope.closedDateFilter == 'yesterday') {
                ticketsClosed = $scope.responderTicketTemp.filter(function (item) {
                    return moment(item.updated_at, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().add('days', -1).format('DD-MM-YYYY') && item.status == 'Closed';
                });

            } else if ($scope.closedDateFilter == 'lastWeek') {
                ticketsClosed = $scope.responderTicketTemp.filter(function (item) {
                    return moment(item.updated_at, 'DD-MM-YYYY') >= moment().add('days', -7) && item.status == 'Closed';
                });
            } else if ($scope.closedDateFilter == 'lastMonth') {
                ticketsClosed = $scope.responderTicketTemp.filter(function (item) {
                    return moment(item.updated_at, 'DD-MM-YYYY') >= moment().add('days', -30) && item.status == 'Closed';
                });
            }

            $scope.ticketsClosedCount = ticketsClosed.length;
            console.log(ticketsClosed)

            ticketsClosed.forEach(function (ticket) {
                var temp5 = _.find($scope.ticketsClosedArray, function (trainer) { return trainer.name === ticket.responderName });
                if (temp5) {
                    temp5.total++;

                } else {
                    $scope.ticketsClosedArray.push({
                        name: ticket.responderName,
                        total: 1
                    });
                }
            })
            console.log($scope.ticketsClosedArray)
            $scope.ticketsClosedArray = _.orderBy($scope.ticketsClosedArray, ['total'], ['desc']);

        }

        $scope.eachClient = function (item) {
            $scope.summaryViewArrayEach = [];
            $scope.summaryViewArrayEach = $scope.summaryViewArray.filter(function (client) {
                return client.clientName == item
            })
        }



        $scope.clientFromDate = '';
        $scope.clientToDate = '';
        $scope.dateFilter = '';
        $scope.selectedStatus = [];
        $scope.filterList = [];

        $scope.setFilters = function () {

            $scope.filterArray = [];
            $scope.trainerCount = {};
            $scope.sourceCount = {};
            $scope.responderTicket = $scope.responderTicketTemp;



            if ($scope.clientFromDate && $scope.clientToDate) {

                $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                    return (moment(item.updated_at, 'DD-MM-YYYY') >= moment($scope.clientFromDate, 'DD-MM-YYYY') && moment(item.updated_at, 'DD-MM-YYYY') <= moment($scope.clientToDate, 'DD-MM-YYYY'))
                });
            }

            if ($scope.selectedStatus.length > 0) {
                $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                    return $scope.selectedStatus.indexOf(item.status) > -1;
                });

            }

            if ($scope.selectedClient.length > 0) {
                $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                    return $scope.selectedClient.indexOf(item.sourceName) > -1;
                });

            }

            if ($scope.selectedTags.length > 0) {
                $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                    return _.intersection($scope.selectedTags, item.tags.split(',')).length > 0;
                });

            }

            if ($scope.selectedtype.length > 0) {
                $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                    return _.intersection($scope.selectedtype, item.type.split(',')).length > 0;
                });

            }

            if ($scope.dateFilter == 'today') {
                $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                    return moment(item.updated_at, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().format('DD-MM-YYYY');
                });

            } else if ($scope.dateFilter == 'yesterday') {
                $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                    return moment(item.updated_at, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().add('days', -1).format('DD-MM-YYYY');
                });

            } else if ($scope.dateFilter == 'lastWeek') {
                $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                    return moment(item.updated_at, 'DD-MM-YYYY') >= moment().add('days', -7)
                });
            } else if ($scope.dateFilter == 'lastMonth') {
                $scope.responderTicket = $scope.responderTicket.filter(function (item) {
                    return moment(item.updated_at, 'DD-MM-YYYY') >= moment().add('days', -30)
                });
            }



            if ($scope.responderTicket.length > 0) {

                $scope.responderTicket.forEach(function (item, index) {

                    if ($scope.trainerCount[item.responder_id]) {
                        $scope.trainerCount[item.responder_id]++;
                    } else {
                        $scope.trainerCount[item.responder_id] = 1;
                    }

                    if ($scope.sourceCount[item.source]) {
                        $scope.sourceCount[item.source]++;
                    } else {
                        $scope.sourceCount[item.source] = 1;
                    }
                });

            }
            if ($scope.trainer !== 'All' && $scope.tableView) {
                $scope.responderTicket = $scope.responderTicket.filter(function (item) {

                    return item.responderName == $scope.trainer;

                });
            }
            if ($scope.source !== 'All' && $scope.tableView) {
                $scope.responderTicket = $scope.responderTicket.filter(function (item) {

                    return item.sourceText == $scope.source;

                });
            }

            if ($scope.tableView) {
                $scope.filterList = [$scope.clientFromDate, $scope.clientToDate, $scope.dateFilter, $scope.trainer, $scope.source]
                $scope.selectedStatus.forEach(function (item) {
                    $scope.filterList.push(item)
                })
                $scope.selectedClient.forEach(function (item) {
                    $scope.filterList.push(item)
                })
                $scope.selectedTags.forEach(function (item) {
                    $scope.filterList.push(item)
                })
                $scope.selectedtype.forEach(function (item) {
                    $scope.filterList.push(item)
                })
                $scope.selectedtype.forEach(function (item) {
                    $scope.filterList.push(item)
                })
            }


            $scope.totalItems = $scope.responderTicket.length;

        }



        //$scope.dateFilterValue = function (page) {
        //    var url = '';
        //    if ($scope.dateFilter == 'today') {
        //        var today = moment().format('YYYY-MM-DD');
        //        url = 'https://prm360.freshdesk.com/api/v2/search/tickets?query="created_at:%27' + today + '%27"&page=' + page

        //    } else if ($scope.dateFilter == 'yesterday') {
        //        var yesterday = moment().add('days', -1).format('YYYY-MM-DD');
        //        url = 'https://prm360.freshdesk.com/api/v2/search/tickets?query="created_at:%27' + yesterday + '%27"&page=' + page

        //    } else if ($scope.dateFilter == 'lastWeek') {
        //        var lastWeekFrom = moment().add('days', -7).format('YYYY-MM-DD');
        //        var lastWeekTo = moment().format('YYYY-MM-DD');
        //        url = 'https://prm360.freshdesk.com/api/v2/search/tickets?query="created_at:>%27' + lastWeekFrom + '%27%20AND%20created_at:<%27' + lastWeekTo + '%27"&page=' + page
        //    } else if ($scope.dateFilter == 'lastMonth') {
        //        var lastMonthFrom = moment().add('days', -30).format('YYYY-MM-DD');
        //        var lastMonthTo = moment().format('YYYY-MM-DD');
        //        url = 'https://prm360.freshdesk.com/api/v2/search/tickets?query="created_at:>%27' + lastMonthFrom + '%27%20AND%20created_at:<%27' + lastMonthTo + '%27"&page=' + page
        //    }
        //    $http({
        //        method: 'GET',
        //        url: url,

        //        encodeURI: true,
        //        headers: {
        //            'Content-Type': 'application/json',
        //            "Authorization": "Basic " + window.btoa('oLRuHDsQvII8LrfthZAj' + ":x")
        //        },
        //        dataType: "json"
        //    }).then(function (response) {
        //        console.log(response)
        //        if (response.data.results && response.data.results.length > 0) {
        //            response.data.results.forEach(function (item) {
        //                $scope.responderTicket.push(item);
        //            });
        //            if (response.data.results.length < 30) {
        //                $scope.transformData();
        //                return
        //            } else {
        //                $scope.dateFilterValue(page + 1);
        //            }
        //        }



        //    });
        //}


    }]);