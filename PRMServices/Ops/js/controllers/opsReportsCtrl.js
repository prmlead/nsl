﻿prmApp
    .controller('opsReportsCtrl', ['$scope', '$state', '$stateParams', 'opsServices', 'userService', 'growlService',
        function ($scope, $state, $stateParams, opsServices, userService, growlService) {

            //$scope.clientid = $stateParams.id;
            $scope.domain = $stateParams.domain;
            // $scope.opsObj = $stateParams.opsObj


            $scope.showGraphs = false;

            $scope.tableView = true;
            $scope.sessionID = userService.getUserToken();

            var today = moment();
            $scope.axis = [];
            $scope.clientsFromDate = today.add('days', -30).format('YYYY-MM-DD');
            today = moment().format('YYYY-MM-DD');
            $scope.clientsToDate = today;
            $scope.Dates = {
                clientsFromDate: '',
                clientsToDate: ''
            }

            $scope.selectedClientsArray = [];
            $scope.selectedClientsString = [];
            $scope.selectedClientNamesArray = [];

            $scope.Dates.clientsFromDate = $scope.clientsFromDate;
            // $scope.Dates.clientsFromDate = $stateParams.startdate;
            $scope.Dates.clientsToDate = $scope.clientsToDate;
            // $scope.Dates.clientsToDate = $stateParams.enddate;
            $scope.myAuctions = [];
            $scope.myAuctions1 = [];
            $scope.reqStatus = 'ALL';

            $scope.totalItems1 = 0;
            $scope.totalItems2 = 0;
            $scope.totalItems3 = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.currentPage3 = 1;
            $scope.itemsPerPage = 10;
            $scope.itemsPerPage2 = 10;
            $scope.itemsPerPage3 = 10;
            $scope.maxSize = 8;

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            $scope.auctionType = 0;

            //  $scope.userid = $scope.clientid;

            $scope.clientDetails = [];

            $scope.vendorInvitedArray = [];
            $scope.vendorsPartipatedArray = [];
            $scope.transactionsArray = [];
            $scope.vendorsSavingsArray = [];
            $scope.reqPostedArray = [];
            $scope.reqClosedArray = [];




            $scope.GetClientDetails = function () {

                opsServices.GetClientDetails($scope.domain)
                    .then(function (response) {
                        $scope.clientDetails = response;

                    });

            }
            $scope.GetClientDetails();

            $scope.companyClients = [];
            $scope.companyClientsTemp = [];



            $scope.GetClients = function () {
                $scope.companyClientsTemp = [];
                opsServices.GetClients($scope.Dates.clientsFromDate, $scope.Dates.clientsToDate)
                    .then(function (response) {
                        //var clients = _.map(response, function (o) {
                        //    if (o.clientSource == "INTERNAL_SOURCE") return o;
                        //});
                        // var clients = _.filter(response, { clientSource: 'INTERNAL_SOURCE' });
                        response = _.orderBy(response, ['cl_compName', 'asc']);
                        var clients = response;
                        clients.unshift({ cl_compName: 'ALL_CLIENTS' })
                        $scope.companyClients = clients;

                        $scope.clientsTemp = $scope.companyClients;

                    });
            }
            $scope.GetClients();

            $scope.getReport = function (name) {
                opsServices.GetTemplates(name, $scope.selectedClientsString, $scope.Dates.clientsFromDate, $scope.Dates.clientsToDate);
            }

            $scope.reqTransactions = [];
            $scope.subUsersRegistered = [];
            $scope.vendorRegistered = [];

            $scope.reqTransactionsTemp = [];
            $scope.subUsersRegisteredTemp = [];
            $scope.vendorRegisteredTemp = [];

            $scope.reports = function () {

                if ($scope.dateFilter == 'today') {

                    $scope.Dates.clientsFromDate = moment().format('YYYY-MM-DD');
                    $scope.Dates.clientsToDate = moment().format('YYYY-MM-DD');


                } else if ($scope.dateFilter == 'yesterday') {

                    $scope.Dates.clientsFromDate = moment().add('days', -1).format('YYYY-MM-DD');
                    $scope.Dates.clientsToDate = moment().add('days', -1).format('YYYY-MM-DD');

                } else if ($scope.dateFilter == 'lastWeek') {
                    $scope.Dates.clientsFromDate = moment().add('days', -7).format('YYYY-MM-DD');
                    $scope.Dates.clientsToDate = moment().format('YYYY-MM-DD');

                } else if ($scope.dateFilter == 'lastMonth') {
                    $scope.Dates.clientsFromDate = moment().add('days', -30).format('YYYY-MM-DD');
                    $scope.Dates.clientsToDate = moment().format('YYYY-MM-DD');

                }

                opsServices.GetReports($scope.selectedClientsString, $scope.Dates.clientsFromDate, $scope.Dates.clientsToDate)

                    .then(function (response) {

                        $scope.reportsList = response;
                        $scope.vendorsInvited = 0;
                        $scope.vendorsParticipated = 0;
                        $scope.minIbitialQuotationPrice = 0;
                        $scope.savings = 0;
                        $scope.percentage = 0;
                        $scope.createdByArray = [];

                        $scope.reportsList.reqTransactions.forEach(function (item) {
                            item.req_POSTED_ON = new moment(item.req_POSTED_ON).format("DD-MM-YYYY HH:mm");
                            $scope.vendorsInvited = item.vendorsInvited + $scope.vendorsInvited;
                            $scope.vendorsParticipated = item.vendorsParticipated + $scope.vendorsParticipated;
                            $scope.minIbitialQuotationPrice = Math.round(item.minIbitialQuotationPrice) + $scope.minIbitialQuotationPrice;
                            $scope.savings = Math.round(item.savings) + $scope.savings;
                            $scope.percentage = ($scope.vendorsParticipated / $scope.vendorsInvited) * 100;

                            if ($scope.createdByArray.indexOf(item.userName) < 0) {
                                $scope.createdByArray.push(item.userName)
                            }
                        });


                        $scope.reqTransactions = $scope.reportsList.reqTransactions;
                        $scope.totalItems1 = $scope.reqTransactions.length;

                        $scope.reportsList.subUsersRegistered.forEach(function (item) {
                            item.dateCreated = new moment(item.dateCreated).format("DD-MM-YYYY HH:mm");
                        });
                        $scope.subUsersRegistered = $scope.reportsList.subUsersRegistered;
                        $scope.totalItems2 = $scope.subUsersRegistered.length;


                        $scope.reportsList.vendorsRegistered.forEach(function (item) {
                            item.dateCreated = new moment(item.dateCreated).format("DD-MM-YYYY HH:mm");
                        });
                        $scope.vendorRegistered = $scope.reportsList.vendorsRegistered;
                        $scope.totalItems3 = $scope.vendorRegistered.length;



                        $scope.reqTransactionsTemp = $scope.reqTransactions;
                        $scope.subUsersRegisteredTemp = $scope.subUsersRegistered;
                        $scope.vendorRegisteredTemp = $scope.vendorRegistered;

                        $scope.showGraphs = true;
                        $scope.summaryView();
                    })

            }
            //$scope.reports();



            $scope.searchClients = function (str) {
                str = str.toLowerCase();
                $scope.companyClients = $scope.clientsTemp.filter(function (req) {
                    return (String(req.cl_compName.toLowerCase()).includes(str) == true);
                });

            };

            $scope.fillValue = function (clients, companyName, domain, isChecked) {
                $scope.clientsArray = [];
                if (isChecked) {


                    if (companyName == 'ALL_CLIENTS') {
                        $scope.selectedClientsArray = [];
                        clients.forEach(function (obj, objIdx) {
                            obj.isChecked = true;
                            if (obj.clientDB) {
                                $scope.selectedClientsArray.push(obj.clientDB);
                            }

                        })
                    } else {
                        clients.forEach(function (obj, objIdx) {
                            if (obj.isChecked == true && obj.clientDB) {
                                $scope.selectedClientsArray.push(obj.clientDB);
                            }
                        })
                    }
                    var arr = new Set($scope.selectedClientsArray)
                    $scope.selectedClientsArray = Array.from(arr);
                    $scope.selectedClientsString = $scope.selectedClientsArray.join(',');

                } else {

                    if (companyName == 'ALL_CLIENTS') {
                        $scope.selectedClientsArray = [];
                        clients.forEach(function (obj, objIdx) {
                            obj.isChecked = false;

                        })
                        $scope.clientsArray = [];

                        $scope.selectedClientsString = $scope.clientsArray.join(',');

                    } else {
                        $scope.clientsArray = $scope.selectedClientsArray;
                        var tempindex = $scope.selectedClientsArray.indexOf(domain);
                        if (tempindex > -1) {
                            $scope.selectedClientsArray.splice(tempindex, 1);
                            $scope.clientsArray = $scope.selectedClientsArray;


                            $scope.selectedClientsString = $scope.clientsArray.join(',');
                        }
                    }
                }


            }

            $scope.dateFilter = '';
            $scope.reqStatus = 'ALL';
            $scope.createdBy = '';
            $scope.setFilters = function (table) {

                $scope.vendorsInvited = 0;
                $scope.vendorsParticipated = 0;
                $scope.minIbitialQuotationPrice = 0;
                $scope.savings = 0;

                if (table == 'requirementTable') {
                    $scope.reqTransactions = $scope.reqTransactionsTemp;


                    if ($scope.reqClientName) {
                        $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                            return item.companyName == $scope.reqClientName;
                        })

                    }

                    if ($scope.reqStatus !== 'ALL') {
                        $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                            return item.closed == $scope.reqStatus;
                        });
                    }

                    if ($scope.createdBy) {
                        $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                            return item.userName == $scope.createdBy;
                        });
                    }

                    $scope.createdByArray = [];

                    $scope.reqTransactions.forEach(function (item) {
                        if ($scope.createdByArray.indexOf(item.userName) < 0) {
                            $scope.createdByArray.push(item.userName)
                        }
                    });


                    //if ($scope.dateFilter == 'today') {
                    //    $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                    //        return moment(item.req_POSTED_ON, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().format('DD-MM-YYYY');
                    //    });

                    //} else if ($scope.dateFilter == 'yesterday') {
                    //    $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                    //        return moment(item.req_POSTED_ON, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().add('days', -1).format('DD-MM-YYYY');
                    //    });

                    //} else if ($scope.dateFilter == 'lastWeek') {
                    //    $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                    //        return moment(item.req_POSTED_ON, 'DD-MM-YYYY') >= moment().add('days', -7)
                    //    });
                    //} else if ($scope.dateFilter == 'lastMonth') {
                    //    $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                    //        return moment(item.req_POSTED_ON, 'DD-MM-YYYY') >= moment().add('days', -30)
                    //    });
                    //} 

                    $scope.reqTransactions.forEach(function (item) {
                        $scope.vendorsInvited = item.vendorsInvited + $scope.vendorsInvited;
                        $scope.vendorsParticipated = item.vendorsParticipated + $scope.vendorsParticipated;
                        $scope.minIbitialQuotationPrice = Math.round(item.minIbitialQuotationPrice) + $scope.minIbitialQuotationPrice;
                        $scope.savings = Math.round(item.savings) + $scope.savings;
                        $scope.percentage = ($scope.vendorsParticipated / $scope.vendorsInvited) * 100



                    });
                    $scope.totalItems1 = $scope.reqTransactions.length;
                    $scope.summaryView();

                } else if (table == 'usersTable') {
                    $scope.subUsersRegistered = $scope.subUsersRegisteredTemp;

                    if ($scope.userClientName) {
                        $scope.subUsersRegistered = $scope.subUsersRegistered.filter(function (item) {
                            return item.companyName == $scope.userClientName
                        })
                    }

                    $scope.totalItems2 = $scope.subUsersRegistered.length;

                } else if (table == 'vendorTable') {
                    $scope.vendorRegistered = $scope.vendorRegisteredTemp;

                    if ($scope.vendorClientName) {
                        $scope.vendorRegistered = $scope.vendorRegistered.filter(function (item) {
                            return item.companyName == $scope.vendorClientName
                        })
                    }

                    $scope.totalItems3 = $scope.vendorRegistered.length;

                }

                $scope.summaryView();
            }
            $scope.clientFilter = '';


            $scope.summaryView = function () {
                $scope.subUserRegisteredArray = [];
                $scope.vendorRegisteredArray = [];
                $scope.reqPostedArray = [];
                $scope.reqClosedArray = [];
                $scope.transactionsArray = [];
                $scope.vendorsSavingsArray = [];
                $scope.vendorInvitedArray = [];
                $scope.vendorsPartipatedArray = [];
                $scope.clientsArray = [];
                $scope.selectedClientReqCount = [];
                $scope.selectedClientUserCount = [];
                $scope.selectedClientVendorCount = [];
                $scope.reqCountArray = [];
                $scope.activeUsersCountArray = [];
                $scope.vendorsParticipatedCountArray = [];
                $scope.createdByArrayGraph = [];
                $scope.datesArray = [];
                $scope.userArrayCount = [];
                var diff = (moment($scope.Dates.clientsToDate, 'YYYY-MM-DD').diff(moment($scope.Dates.clientsFromDate, 'YYYY-MM-DD'), 'days'))

                if (diff <= 30) {
                    for (i = 0; i <= diff; i++) {
                        $scope.datesArray.push(moment($scope.Dates.clientsFromDate, 'YYYY-MM-DD').add('days', i).format('YYYY-MM-DD'))
                    }


                } else {
                    //var temp = Math.round(diff / 30)
                    //for (i = 0; i <= 31; i++) {
                    //    $scope.datesArray.push(moment($scope.Dates.clientsFromDate, 'YYYY-MM-DD').add('days', i * temp).format('YYYY-MM-DD'))
                    //}
                    //var temp = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
                    //var day = 30;
                    //for (i = 0; i <=diff ; i++) {
                    //    $scope.datesArray.push(moment($scope.Dates.clientsFromDate, 'YYYY-MM-DD').add('days', i * day).format('YYYY-MM-DD'))
                    //}

                    //To get year and month data between selected dates
                    var dateEnd = moment($scope.Dates.clientsToDate, 'YYYY-MM-DD');
                    var dateStart = moment($scope.Dates.clientsFromDate, 'YYYY-MM-DD');
                    while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
                        $scope.datesArray.push(dateStart.format('YYYY-MM'));
                        dateStart.add(1, 'month');
                    }


                }


                if (diff <= 30) {

                    $scope.datesArray.forEach(function (date, index) {
                        var reqCountArray = $scope.reqTransactions.filter(function (item) {

                            return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD') === date;
                        });

                        var usersArray = [];
                        reqCountArray.forEach(function (item) {
                            if (usersArray.indexOf(item.userName) < 0) {
                                usersArray.push(item.userName)
                            }

                        })
                        var activeUsersCountArray = $scope.subUsersRegistered.filter(function (item) {
                            return moment(item.dateCreated, 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD') === date;
                        });

                        var vendorParticipatedCountArray = $scope.reqTransactions.filter(function (item) {
                            return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD') === date && item.closed === 'Negotiation Ended';

                        })
                        var percentageCount = (_.sumBy(vendorParticipatedCountArray, 'vendorsParticipated') / _.sumBy(vendorParticipatedCountArray, 'vendorsInvited')) * 100;

                        $scope.vendorsParticipatedCountArray.push(Math.round(percentageCount) || 0);
                        $scope.reqCountArray.push(reqCountArray.length)
                        $scope.activeUsersCountArray.push(activeUsersCountArray.length)
                        $scope.userArrayCount.push(usersArray.length)

                    })
                } else {
                    $scope.datesArray.forEach(function (date, index) {

                        var reqCountArray = $scope.reqTransactions.filter(function (item) {

                            return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm').format('YYYY-MM') === date;
                        })
                        var usersArray = [];
                        reqCountArray.forEach(function (item) {
                            if (usersArray.indexOf(item.userName) < 0) {
                                usersArray.push(item.userName)
                            }

                        })
                        var activeUsersCountArray = $scope.subUsersRegistered.filter(function (item) {

                            return moment(item.dateCreated, 'DD-MM-YYYY HH:mm').format('YYYY-MM') === date;
                        })
                        var vendorParticipatedCountArray = $scope.reqTransactions.filter(function (item) {
                            return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm').format('YYYY-MM') === date && item.closed === 'Negotiation Ended';

                        })
                        var percentageCount = (_.sumBy(vendorParticipatedCountArray, 'vendorsParticipated') / _.sumBy(vendorParticipatedCountArray, 'vendorsInvited')) * 100;

                        $scope.vendorsParticipatedCountArray.push(Math.round(percentageCount) || 0);

                        $scope.reqCountArray.push(reqCountArray.length)
                        $scope.activeUsersCountArray.push(activeUsersCountArray.length)
                        $scope.userArrayCount.push(usersArray.length)

                    })

                }




                $scope.selectedClientsArray.forEach(function (client) {

                    //if ($scope.clientFilter == 'transactional') {
                    //    $scope.clientsArray = ['Glenmark Pharmaceuticals', 'Piramal Pharma Solutions', 'Herbalife Nutrition', 'JK Files and Tools Corporation'];
                    //    //var transational = ['Glenmark Pharmaceuticals', 'Piramal Pharma Solutions', 'Herbalife Nutrition', 'JK Files and Tools Corporation'];

                    //} else if ($scope.clientFilter == 'enterprise') {
                    //    var clientObj1 = _.find($scope.companyClients, function (item) { return item.clientDB === client });
                    //    $scope.clientsArray.push(clientObj1.cl_compName);

                    //} else {
                    //    var clientObj = _.find($scope.companyClients, function (item) { return item.clientDB === client });
                    //    $scope.clientsArray.push(clientObj.cl_compName);

                    //}

                    var clientObj = _.find($scope.companyClients, function (item) { return item.clientDB === client });
                    var transationalClients = ['Glenmark Pharmaceuticals', 'Piramal Pharma Solutions', 'Herbalife Nutrition', 'JK Files and Tools Corporation'];

                    if ($scope.clientFilter == 'transactional') {
                        if (transationalClients.indexOf(clientObj.cl_compName) === -1) {
                            return;
                        }

                    }
                    else if ($scope.clientFilter == 'enterprise') {

                        var enterpriseClients = $scope.companyClients.filter(function (item) {
                            return transationalClients.indexOf(item.cl_compName) === -1;
                        })
                        if (!_.find(enterpriseClients, function (item) { return item.cl_compName === clientObj.cl_compName })) {
                            return;
                        }

                    }
                    $scope.clientsArray.push(clientObj.cl_compName);


                    var transactions = $scope.reqTransactions.filter(function (item) {
                        return item.companyName === clientObj.cl_compName;
                    });



                    var vendorsInvited = 0;
                    var vendorsParticipated = 0;
                    var minIbitialQuotationPrice = 0;
                    var savings = 0;

                    transactions.forEach(function (ticket) {

                        vendorsInvited = ticket.vendorsInvited + vendorsInvited;
                        vendorsParticipated = ticket.vendorsParticipated + vendorsParticipated;
                        minIbitialQuotationPrice = Math.round(ticket.minIbitialQuotationPrice) + minIbitialQuotationPrice;
                        savings = Math.round(ticket.savings) + savings;

                    });

                    $scope.vendorInvitedArray.push(vendorsInvited);
                    $scope.vendorsPartipatedArray.push(vendorsParticipated);
                    $scope.transactionsArray.push(minIbitialQuotationPrice);
                    $scope.vendorsSavingsArray.push(savings);


                    var activeUsers = $scope.subUsersRegistered.filter(function (item) {
                        return item.companyName === clientObj.cl_compName;
                    })


                    var obj = {
                        clientName: clientObj.cl_compName,
                        total: $scope.subUsersRegistered.filter(function (item) {
                            return item.companyName === clientObj.cl_compName;
                        }).length

                    }


                    $scope.subUserRegisteredArray.push(obj);
                    $scope.subUserRegisteredArray = _.orderBy($scope.subUserRegisteredArray, ['total'], ['desc']);



                    var obj1 = {
                        clientName: clientObj.cl_compName,
                        total: $scope.vendorRegistered.filter(function (item) {
                            return item.companyName === clientObj.cl_compName;
                        }).length
                    }

                    $scope.vendorRegisteredArray.push(obj1);
                    $scope.vendorRegisteredArray = _.orderBy($scope.vendorRegisteredArray, ['total'], ['desc']);


                    var dataTransactionalArray = [];
                    var dataUsersArray = [];
                    var dataVendorsArray = [];

                    if (diff > 30) {
                        $scope.datesArray.forEach(function (date, index) {
                            var transactionsFilter = transactions.filter(function (item) {
                                //if (index == 0) {
                                //    return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD') === date && item.closed !== 'Negotiation Ended';
                                //} else {
                                //    return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm') > moment($scope.datesArray[index - 1], 'YYYY-MM-DD') &&
                                //        moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm') <= moment(date, 'YYYY-MM-DD') && item.closed !== 'Negotiation Ended';
                                //}
                                return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm').format('YYYY-MM') === date;

                            });
                            dataTransactionalArray.push(transactionsFilter.length);



                            var activeUsersFilter = activeUsers.filter(function (item) {
                                return moment(item.dateCreated, 'DD-MM-YYYY HH:mm').format('YYYY-MM') === date;

                            })
                            dataUsersArray.push(activeUsersFilter.length);

                            var vendorsParticiaptedPerFilter = transactions.filter(function (item) {
                                return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm').format('YYYY-MM') === date && item.closed === 'Negotiation Ended';

                            })
                            var percentage = (_.sumBy(vendorsParticiaptedPerFilter, 'vendorsParticipated') / _.sumBy(vendorsParticiaptedPerFilter, 'vendorsInvited')) * 100;
                            dataVendorsArray.push(Math.round(percentage) || 0);

                        })
                    } else {
                        $scope.datesArray.forEach(function (date) {
                            var transactionsFilter = transactions.filter(function (item) {
                                return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD') === date;
                            });
                            dataTransactionalArray.push(transactionsFilter.length);

                            var activeUsersFilter = activeUsers.filter(function (item) {
                                return moment(item.dateCreated, 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD') === date;

                            })
                            dataUsersArray.push(activeUsersFilter.length);
                            var vendorsParticiaptedPerFilter = transactions.filter(function (item) {
                                return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD') === date && item.closed === 'Negotiation Ended';

                            })

                            var percentage = (_.sumBy(vendorsParticiaptedPerFilter, 'vendorsParticipated') / _.sumBy(vendorsParticiaptedPerFilter, 'vendorsInvited')) * 100;

                            dataVendorsArray.push(Math.round(percentage) || 0);

                        })

                    }

                    //$scope.reqCountArray.push(_.sum(dataTransactionalArray))
                    //$scope.activeUsersCountArray.push(_.sum(dataUsersArray))
                    //$scope.vendorsParticipatedCountArray.push(_.sum(dataVendorsArray))

                    //To display only clients with data
                    if (_.sum(dataTransactionalArray) > 0) {
                        $scope.selectedClientReqCount.push({
                            name: clientObj.cl_compName,
                            data: dataTransactionalArray
                        });

                    }
                    if (_.sum(dataUsersArray) > 0) {
                        $scope.selectedClientUserCount.push({
                            name: clientObj.cl_compName,
                            data: dataUsersArray
                        });
                    }
                    if (_.sum(dataVendorsArray) > 0) {
                        $scope.selectedClientVendorCount.push({
                            name: clientObj.cl_compName,
                            data: dataVendorsArray
                        });

                    }
                });
                console.log($scope.createdByArray)
                $('#button').click();
            }

            $scope.activeUsersView = function () {
                $scope.createdByArrayGraph = [];
                var diff = (moment($scope.Dates.clientsToDate, 'YYYY-MM-DD').diff(moment($scope.Dates.clientsFromDate, 'YYYY-MM-DD'), 'days'))

                $scope.createdByArray.forEach(function (createdBy) {
                    var createdByArrayGraph = [];
                    var userTransations = $scope.reqTransactions.filter(function (item) {
                        return item.userName == createdBy;
                    });
                    if (diff <= 30) {
                        $scope.datesArray.forEach(function (date, index) {
                            var reqCountArray = userTransations.filter(function (item) {
                                return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD') === date;
                            });
                            createdByArrayGraph.push(reqCountArray.length)
                        })
                    }

                    else {
                        $scope.datesArray.forEach(function (date, index) {
                            var reqCountArray = userTransations.filter(function (item) {
                                return moment(item.req_POSTED_ON, 'DD-MM-YYYY HH:mm').format('YYYY-MM') === date;
                            });

                            createdByArrayGraph.push(reqCountArray.length)
                        })
                    }
                    $scope.createdByArrayGraph.push({ name: createdBy, data: createdByArrayGraph })

                })

                $('#button1').click();
            }




            $scope.reqValue = {
                credits: {
                    enabled: false
                },
                chart: {

                    width: 1250,

                    height: 500,
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];
                            //chart.update({ series: series }, true, true)
                            $('#button').click(function () {
                                //series = [{ name: 'Requirement Closed', data: $scope.reqPostedArray }, { name: 'Requirement Closed', data: $scope.reqClosedArray }];
                                series = $scope.selectedClientReqCount
                                chart.update({
                                    series: series,
                                    xAxis: {

                                        categories: $scope.datesArray
                                    }
                                }, true, true);
                            });
                            $('#updateCountButton').click(function () {
                                series = [{ name: 'Requirement Count', data: $scope.reqCountArray }];
                                chart.update({
                                    series: series,
                                    xAxis: {
                                        categories: $scope.datesArray
                                    }
                                }, true, true);
                            });

                        }
                    }
                },
                title: {
                    text: 'Requirement Trend'
                },
                subtitle: {
                    text: 'Requirement Trend'
                },
                yAxis: {
                    title: {
                        text: 'Count'
                    }
                },


                xAxis: {
                    categories: []
                },

                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 0
                    }
                },
                series: [],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            };

            $scope.newUsersValue = {
                credits: {
                    enabled: false
                },
                chart: {

                    width: 1250,

                    height: 500,
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];
                            $('#button').click(function () {
                                series = $scope.selectedClientUserCount
                                chart.update({
                                    series: series,
                                    xAxis: {

                                        categories: $scope.datesArray
                                    }
                                }, true, true);
                            });
                            $('#updateCountButton').click(function () {
                                series = [{ name: 'Active Users Count', data: $scope.activeUsersCountArray }];
                                chart.update({
                                    series: series,
                                    xAxis: {
                                        categories: $scope.datesArray
                                    }
                                }, true, true);
                            });
                        }
                    }
                },
                title: {
                    text: 'New Users'
                },
                subtitle: {
                    text: 'New Users'
                },
                yAxis: {
                    title: {
                        text: 'Count'
                    }
                },
                series: [],


                xAxis: {
                    categories: []
                },

                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 0
                    }
                },

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            };
            $scope.activeUsersValue = {
                credits: {
                    enabled: false
                },
                chart: {

                    width: 1250,

                    height: 500,
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];
                            $('#button1').click(function () {
                                series = $scope.createdByArrayGraph
                                chart.update({
                                    series: series,
                                    xAxis: {

                                        categories: $scope.datesArray
                                    }
                                }, true, true);
                            });
                            $('#updateCountButton1').click(function () {
                                console.log($scope.userArrayCount)
                                series = [{ name: 'Active Users Count', data: $scope.userArrayCount }];
                                chart.update({
                                    series: series,
                                    xAxis: {
                                        categories: $scope.datesArray
                                    }
                                }, true, true);
                            });

                        }
                    }
                },
                title: {
                    text: 'Active Users'
                },
                subtitle: {
                    text: 'Active Users'
                },
                yAxis: {
                    title: {
                        text: 'Count'
                    }
                },
                series: [],


                xAxis: {
                    categories: []
                },

                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 0
                    }
                },

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            };


            $scope.vendorParticipatedValue = {
                credits: {
                    enabled: false
                },
                chart: {

                    width: 1250,

                    height: 500,
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];
                            $('#button').click(function () {
                                series = $scope.selectedClientVendorCount
                                chart.update({
                                    series: series,
                                    xAxis: {

                                        categories: $scope.datesArray
                                    }
                                }, true, true);
                            });
                            $('#updateCountButton').click(function () {
                                series = [{ name: 'Vendors Participation Percentage', data: $scope.vendorsParticipatedCountArray }];
                                chart.update({
                                    series: series,
                                    xAxis: {
                                        categories: $scope.datesArray
                                    }
                                }, true, true);
                            });
                        }
                    }
                },
                title: {
                    text: 'Vendors Participated'
                },
                subtitle: {
                    text: 'Vendors Participated'
                },
                yAxis: {
                    title: {
                        text: 'Percentage'
                    },
                    labels: {
                        format: '{value}%'
                    }
                },
                series: [],


                xAxis: {
                    categories: []
                },

                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 0
                    }
                },

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            };

            $scope.transactionValue = {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'bar',
                    width: 1250,

                    height: 500,
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];
                            //chart.update({ series: series }, true, true)
                            $('#button').click(function () {
                                series = [{ name: 'Transactional Value', data: $scope.transactionsArray, pointPadding: 0 }, { name: 'Transactional Savings', data: $scope.vendorsSavingsArray, pointPadding: 0.3 }];

                                chart.update({
                                    series: series,
                                    xAxis: {
                                        categories: $scope.clientsArray
                                    }
                                }, true, true);
                            });
                        }
                    }
                },
                title: {
                    text: 'Transactional Value'
                },
                subtitle: {
                    text: 'Transactional Value'
                },
                yAxis: {
                    title: {
                        text: 'Count'
                    }
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                    shared: true
                },
                xAxis: {
                    categories: []
                },

                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        grouping: false
                    },
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Transactional Value',
                    data: []
                }, {
                    name: 'Transactional Savings',
                    data: []
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            };

            $scope.vendorValue = {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'bar',
                    width: 1250,

                    height: 500,
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];
                            //chart.update({ series: series }, true, true)
                            $('#button').click(function () {
                                series = [{ name: 'Vendors Invited', data: $scope.vendorInvitedArray, pointPadding: 0 }, { name: 'Vendors Participated', data: $scope.vendorsPartipatedArray, pointPadding: 0.3 }];

                                chart.update({
                                    series: series,
                                    xAxis: {
                                        categories: $scope.clientsArray
                                    }
                                }, true, true);
                            });
                        }
                    }
                },
                title: {
                    text: 'Vendors'
                },
                subtitle: {
                    text: 'Vendors'
                },
                yAxis: {
                    title: {
                        text: 'Count'
                    }
                },

                xAxis: {
                    categories: []
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                    shared: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        grouping: false
                    },
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Vendors Invited',
                    data: []
                }, {
                    name: 'Vendors Participated',
                    data: []
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            };




        }]);