﻿prmApp
    .controller('opsVendorReqSearchCtrl', ['$scope', '$state', '$stateParams', 'opsServices', 'userService', 'growlService', '$http',
        function ($scope, $state, $stateParams, opsServices, userService, growlService, $http) {

            //$scope.clientid = $stateParams.id;
            $scope.domain = $stateParams.domain;
            // $scope.opsObj = $stateParams.opsObj


            $scope.showGraphs = false;

            $scope.tableView = true;
            $scope.sessionID = userService.getUserToken();

            var today = moment();
            $scope.axis = [];
            $scope.clientsFromDate = today.add('days', -30).format('YYYY-MM-DD');
            today = moment().format('YYYY-MM-DD');
            $scope.clientsToDate = today;
            $scope.Dates = {
                clientsFromDate: '',
                clientsToDate: '',
                searchEmail: '',
                searchPhone: ''
            }

            $scope.assignToUsers = [
                { name: 'Karan', id: 81024420267 },
                { name: 'Srilekha', id: 81024420713 },
                { name: 'Sanjana', id: 81025035308 },
                { name: 'Jasjeet', id: 81024420107 },
                { name: 'Kabita', id: 81024420183 },
                { name: 'Pavan', id: 81024420620 },
                { name: 'Manoj', id: 81024420515 },
                { name: 'Kshithija', id: 81024420378 },
            ]

            $scope.trainAssignee = {
                assignedTo: {},
                trainedTo: '',
                contactNumber: '',
                email: '',
                status: 'Open',
                comments: '',
                freshDeskId: '',
                companyName: '',
                vendorID: '',
                postedOn: '',
                reqID: '',
                vtID: 0,
                isEdit: 0
            }
            $scope.DBName = "";
            $scope.Company_Name = "";
            $scope.selectedClientsArray = [];
            $scope.selectedClientsString = [];
            $scope.selectedClientNamesArray = [];

            $scope.Dates.clientsFromDate = $scope.clientsFromDate;
            // $scope.Dates.clientsFromDate = $stateParams.startdate;
            $scope.Dates.clientsToDate = $scope.clientsToDate;
            // $scope.Dates.clientsToDate = $stateParams.enddate;
            $scope.myAuctions = [];
            $scope.myAuctions1 = [];
            $scope.reqStatus = 'ALL';

            $scope.totalItems = 0;
            $scope.totalItems2 = 0;
            $scope.totalItems3 = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.currentPage3 = 1;
            $scope.itemsPerPage = 10;
            $scope.itemsPerPage2 = 10;
            $scope.itemsPerPage3 = 10;
            $scope.maxSize = 8;

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            $scope.auctionType = 0;

            //  $scope.userid = $scope.clientid;

            $scope.clientDetails = [];

            $scope.vendorInvitedArray = [];
            $scope.vendorsPartipatedArray = [];
            $scope.transactionsArray = [];
            $scope.vendorsSavingsArray = [];
            $scope.reqPostedArray = [];
            $scope.reqClosedArray = [];




            $scope.GetClientDetails = function () {

                opsServices.GetClientDetails($scope.domain)
                    .then(function (response) {
                        $scope.clientDetails = response;

                    });

            }
            $scope.GetClientDetails();

            $scope.companyClients = [];
            $scope.companyClientsTemp = [];

            $scope.errorDisplay = '';

            $scope.GetClients = function () {
                $scope.companyClientsTemp = [];
                opsServices.GetClients($scope.Dates.clientsFromDate, $scope.Dates.clientsToDate)
                    .then(function (response) {
                        //var clients = _.map(response, function (o) {
                        //    if (o.clientSource == "INTERNAL_SOURCE") return o;
                        //});
                        // var clients = _.filter(response, { clientSource: 'INTERNAL_SOURCE' });
                        response = _.orderBy(response, ['cl_compName', 'asc']);
                        var clients = response;
                        //clients.unshift({ cl_compName: 'ALL_CLIENTS' })
                       
                        $scope.companyClients = clients;
                        $scope.companyClients.forEach(function (item) {
                            $scope.selectedClientsString = item.clientDB;
                        })
                        $scope.clientsTemp = $scope.companyClients;
                    });
            }
            $scope.GetClients();

            $scope.getReport = function (name) {
                opsServices.GetTemplates(name, $scope.selectedClientsString, $scope.Dates.clientsFromDate, $scope.Dates.clientsToDate);
            }

            $scope.reqTransactions = [];
            $scope.subUsersRegistered = [];
            $scope.vendorRegistered = [];

            $scope.reqTransactionsTemp = [];
            $scope.subUsersRegisteredTemp = [];
            $scope.vendorRegisteredTemp = [];

            $scope.reports = function () {

                if ($scope.Dates.searchPhone != "" || $scope.Dates.searchEmail != "") {

                } else {
                    swal("Error!", "Please enter Phone or Email", 'error');
                    return;
                }
                opsServices.GetVendorRfqSearch($scope.selectedClientsString, $scope.Dates.clientsFromDate, $scope.Dates.clientsToDate, $scope.Dates.searchEmail, $scope.Dates.searchPhone)

                    .then(function (response) {
                        response = _.orderBy(response, ['requirementID'], ['desc']);
                        $scope.reportsList = response;
                        $scope.vendorsInvited = 0;
                        $scope.vendorsParticipated = 0;
                        $scope.minIbitialQuotationPrice = 0;
                        $scope.savings = 0;
                        $scope.percentage = 0;
                        $scope.createdByArray = [];

                        $scope.reportsList.forEach(function (item) {

                            if (item.isDiscountQuotation == 0 && item.IS_CB_ENABLED == false && item.LOT_ID == 0) {
                                item.closedType = "PRICE";
                            } else if (item.isDiscountQuotation == 0 && item.IS_CB_ENABLED == true) {
                                item.closedType = "CB - PRICE";
                            } else if (item.isDiscountQuotation == 0 && item.LOT_ID > 0) {
                                item.closedType = "LOT - PRICE";
                            }

                            item.req_POSTED_ON = new moment(item.req_POSTED_ON).format("DD-MM-YYYY HH:mm");
                            item.quotation_FREEZ_TIME = new moment(item.quotation_FREEZ_TIME).format("DD-MM-YYYY HH:mm");
                            item.exp_START_TIME = new moment(item.exp_START_TIME).format("DD-MM-YYYY HH:mm");
                            $scope.savings = Math.round(item.savings) + $scope.savings;

                            if ($scope.createdByArray.indexOf(item.userName) < 0) {
                                $scope.createdByArray.push(item.userName)
                            }
                        });


                        $scope.reqTransactions = $scope.reportsList;
                        $scope.totalItems = $scope.reqTransactions.length;

                        $scope.reqTransactionsTemp = $scope.reqTransactions;

                    })

            }
            //$scope.reports();



            $scope.searchClients = function (str) {
                str = str.toLowerCase();
                $scope.companyClients = $scope.clientsTemp.filter(function (req) {
                    return (String(req.cl_compName.toLowerCase()).includes(str) == true);
                });

            };

            $scope.fillValue = function (clients, companyName, domain, isChecked) {
                $scope.clientsArray = [];
                if (isChecked) {


                    if (companyName == 'ALL_CLIENTS') {
                        $scope.selectedClientsArray = [];
                        clients.forEach(function (obj, objIdx) {
                            obj.isChecked = true;
                            if (obj.clientDB) {
                                $scope.selectedClientsArray.push(obj.clientDB);
                            }

                        })
                    } else {
                        clients.forEach(function (obj, objIdx) {
                            if (obj.isChecked == true && obj.clientDB) {
                                $scope.selectedClientsArray.push(obj.clientDB);
                            }
                        })
                    }
                    var arr = new Set($scope.selectedClientsArray)
                    $scope.selectedClientsArray = Array.from(arr);
                    $scope.selectedClientsString = $scope.selectedClientsArray.join(',');

                } else {

                    if (companyName == 'ALL_CLIENTS') {
                        $scope.selectedClientsArray = [];
                        clients.forEach(function (obj, objIdx) {
                            obj.isChecked = false;

                        })
                        $scope.clientsArray = [];

                        $scope.selectedClientsString = $scope.clientsArray.join(',');

                    } else {
                        $scope.clientsArray = $scope.selectedClientsArray;
                        var tempindex = $scope.selectedClientsArray.indexOf(domain);
                        if (tempindex > -1) {
                            $scope.selectedClientsArray.splice(tempindex, 1);
                            $scope.clientsArray = $scope.selectedClientsArray;


                            $scope.selectedClientsString = $scope.clientsArray.join(',');
                        }
                    }
                }


            }

            $scope.dateFilter = '';
            $scope.reqStatus = 'ALL';
            $scope.createdBy = '';

            $scope.setFilters = function (table) {
                // search
                $scope.vendorsInvited = 0;
                $scope.vendorsParticipated = 0;
                $scope.minIbitialQuotationPrice = 0;
                $scope.savings = 0;

                if (table == 'requirementTable') {
                    $scope.reqTransactions = $scope.reqTransactionsTemp;


                    if ($scope.reqClientName) {
                        $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                            return item.companyName == $scope.reqClientName;
                        })
                        if ($scope.reqTransactions.length == 0) {
                            $scope.errorDisplay = 'No Records found for this selected client';
                        } else {
                            $scope.errorDisplay = '';
                        }
                    }
                    if ($scope.reqClientName == 'ALL_CLIENTS' || $scope.reqClientName == null) {
                        $scope.errorDisplay = '';
                    }
                    if ($scope.reqStatus !== 'ALL') {
                        $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                            return item.closed == $scope.reqStatus;
                        });
                    }

                    if ($scope.createdBy) {
                        $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                            return item.userName == $scope.createdBy;
                        });
                    }

                    $scope.createdByArray = [];

                    $scope.reqTransactions.forEach(function (item) {
                        if ($scope.createdByArray.indexOf(item.userName) < 0) {
                            $scope.createdByArray.push(item.userName)
                        }
                    });


                    //if ($scope.dateFilter == 'today') {
                    //    $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                    //        return moment(item.req_POSTED_ON, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().format('DD-MM-YYYY');
                    //    });

                    //} else if ($scope.dateFilter == 'yesterday') {
                    //    $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                    //        return moment(item.req_POSTED_ON, 'DD-MM-YYYY').format('DD-MM-YYYY') == moment().add('days', -1).format('DD-MM-YYYY');
                    //    });

                    //} else if ($scope.dateFilter == 'lastWeek') {
                    //    $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                    //        return moment(item.req_POSTED_ON, 'DD-MM-YYYY') >= moment().add('days', -7)
                    //    });
                    //} else if ($scope.dateFilter == 'lastMonth') {
                    //    $scope.reqTransactions = $scope.reqTransactions.filter(function (item) {
                    //        return moment(item.req_POSTED_ON, 'DD-MM-YYYY') >= moment().add('days', -30)
                    //    });
                    //} 

                    $scope.reqTransactions.forEach(function (item) {
                        $scope.vendorsInvited = item.vendorsInvited + $scope.vendorsInvited;
                        $scope.vendorsParticipated = item.vendorsParticipated + $scope.vendorsParticipated;
                        $scope.minIbitialQuotationPrice = Math.round(item.minIbitialQuotationPrice) + $scope.minIbitialQuotationPrice;
                        $scope.savings = Math.round(item.savings) + $scope.savings;
                        $scope.percentage = ($scope.vendorsParticipated / $scope.vendorsInvited) * 100



                    });
                    $scope.totalItems1 = $scope.reqTransactions.length;
                    $scope.summaryView();

                } else if (table == 'usersTable') {
                    $scope.subUsersRegistered = $scope.subUsersRegisteredTemp;

                    if ($scope.userClientName) {
                        $scope.subUsersRegistered = $scope.subUsersRegistered.filter(function (item) {
                            return item.companyName == $scope.userClientName
                        })
                    }

                    $scope.totalItems2 = $scope.subUsersRegistered.length;

                } else if (table == 'vendorTable') {
                    $scope.vendorRegistered = $scope.vendorRegisteredTemp;

                    if ($scope.vendorClientName) {
                        $scope.vendorRegistered = $scope.vendorRegistered.filter(function (item) {
                            return item.companyName == $scope.vendorClientName
                        })
                    }

                    $scope.totalItems3 = $scope.vendorRegistered.length;

                }

                if (table == 'SEARCH' && $scope.searchKeyword1) {
                    $scope.reqTransactions = $scope.reqTransactionsTemp;
                    $scope.reqTransactions = $scope.reqTransactionsTemp.filter(function (item) {

                        return (String(item.requirementID).includes($scope.searchKeyword1)
                            || String(item.title.toLowerCase()).includes($scope.searchKeyword1.toLowerCase())
                        );
                    });
                };

                $scope.summaryView();
            }


            $scope.summaryView = function () {
                $scope.subUserRegisteredArray = [];
                $scope.vendorRegisteredArray = [];
                $scope.reqPostedArray = [];
                $scope.reqClosedArray = [];
                $scope.transactionsArray = [];
                $scope.vendorsSavingsArray = [];
                $scope.vendorInvitedArray = [];
                $scope.vendorsPartipatedArray = [];
                $scope.clientsArray = [];
                $scope.selectedClientReqCount = [];
                $scope.selectedClientUserCount = [];
                $scope.selectedClientVendorCount = [];
                $scope.reqCountArray = [];

                $scope.datesArray = [];

                var diff = (moment($scope.Dates.clientsToDate, 'YYYY-MM-DD').diff(moment($scope.Dates.clientsFromDate, 'YYYY-MM-DD'), 'days'))

                if (diff <= 30) {
                    for (i = 0; i <= diff; i++) {
                        $scope.datesArray.push(moment($scope.Dates.clientsFromDate, 'YYYY-MM-DD').add('days', i).format('YYYY-MM-DD'))
                    }
                } else {
                    //var temp = Math.round(diff / 30)
                    //for (i = 0; i <= 31; i++) {
                    //    $scope.datesArray.push(moment($scope.Dates.clientsFromDate, 'YYYY-MM-DD').add('days', i * temp).format('YYYY-MM-DD'))
                    //}
                    //var temp = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
                    //var day = 30;
                    //for (i = 0; i <=diff ; i++) {
                    //    $scope.datesArray.push(moment($scope.Dates.clientsFromDate, 'YYYY-MM-DD').add('days', i * day).format('YYYY-MM-DD'))
                    //}

                    //To get year and month data between selected dates
                    var dateEnd = moment($scope.Dates.clientsToDate, 'YYYY-MM-DD');
                    var dateStart = moment($scope.Dates.clientsFromDate, 'YYYY-MM-DD');
                    while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
                        $scope.datesArray.push(dateStart.format('YYYY-MM'));
                        dateStart.add(1, 'month');
                    }
                }




                $scope.selectedClientsArray.forEach(function (client) {
                    var clientObj = _.find($scope.companyClients, function (item) { return item.clientDB === client });

                    $scope.clientsArray.push(clientObj.cl_compName);
                    var transactions = $scope.reqTransactions.filter(function (item) {
                        return item.companyName === clientObj.cl_compName;
                    });

                    var dataTransactionalArray = [];
                    //To display only clients with data
                    if (_.sum(dataTransactionalArray) > 0) {
                        $scope.selectedClientReqCount.push({
                            name: clientObj.cl_compName,
                            data: dataTransactionalArray
                        });

                    }

                    $scope.reqCountArray.push(_.sum(dataTransactionalArray))
                });

                $('#button').click();

            }


            $scope.ManualVerifySmsOTP = function (vendor, compName) {
                $scope.clientsTemp.forEach(function (comp) {
                    if (comp.cl_compName == compName) {
                        $scope.DBName = comp.clientDB;
                    }
                })
                swal({
                    title: "Are you sure?",
                    text: "OTP will be sent",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, I am sure",
                    closeOnConfirm: true
                }, function () {
                    var params = {
                        userID: vendor.userID,
                        domain: $scope.DBName,
                        issmsverified: vendor.isOtpVerified,
                        isemailverified: vendor.isEmailVerified,
                        phone: vendor.phoneNum,
                        email: vendor.email
                    }

                    opsServices.SendOTP(params)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");

                            }
                            else if (response.errorMessage == "") {
                                swal("Thanks !", "OTP send Successfully", "success");
                                angular.element('#vendorVerificationDetails').modal('hide');
                            }
                        })

                })
            }

            $scope.SendPassword = function (vendor, compName) {

                $scope.clientsTemp.forEach(function (comp) {
                    if (comp.cl_compName == compName) {
                        $scope.DBName = comp.clientDB;
                    }
                })

                swal({
                    title: "Are you sure?",
                    text: "Password will be sent",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, I am sure",
                    closeOnConfirm: true
                }, function () {
                    var params = {
                        userID: vendor.userID,
                        domain: $scope.DBName,
                        phone: vendor.phoneNum,
                        email: vendor.email
                    }
                    opsServices.SendPassword(params)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");

                            }
                            else if (response.errorMessage == "") {
                                swal("Thanks !", "Password send Successfully", "success");
                                angular.element('#vendorVerificationDetails').modal('hide');
                            }
                        })
                })
            }

            $scope.vendorTrainingHistory = [];
            $scope.GetVendorTrainingHistory = function (compName) {
                $scope.clientsTemp.forEach(function (comp) {
                    if (comp.cl_compName == compName) {
                        $scope.DBName = comp.clientDB;
                        $scope.Company_Name = compName;
                    }
                })
                opsServices.GetVendorTrainingHistory(0, 0, $scope.trainAssignee.vendorID, $scope.DBName).then(function (response) {
                    if (response) {
                        $scope.vendorTrainingHistory = response;
                        $scope.vendorTrainingHistory.forEach(function (item) {
                            //item.reqPostedDate = new moment(item.reqPostedDate).format("DD-MM-YYYY HH:mm");

                        })
                    }
                })
            }

            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes();
            var dateTime = date + ' ' + time;

            $scope.vendorName = '';

            $scope.openAssigneeModal = function (vendor, comp) {

                $scope.trainAssignee.trainedTo = vendor.firstName;
                $scope.vendorName = vendor.firstName;
                $scope.trainAssignee.contactNumber = vendor.phoneNum;
                $scope.trainAssignee.email = vendor.email;
                $scope.trainAssignee.companyName = vendor.company;
                $scope.trainAssignee.vendorID = vendor.userID;
                $scope.trainAssignee.reqID = comp.requirementID;
                $scope.trainAssignee.postedOn = comp.req_POSTED_ON;
                $scope.trainAssignee.vtID = vendor.VT_ID ? vendor.VT_ID : 0;
                $scope.clientName = comp.companyName;
            }

            $scope.changeCommentText = function (date) {
                if ($scope.trainAssignee.status === 'trainingCompleted' && $scope.trainAssignee.assignedTo && $scope.trainAssignee.assignedTo.name) {
                    $scope.trainAssignee.comments = 'Dear <b>' + $scope.vendorName + '</b>, <br><br>' +
                        'Thank you for your time.<br><br>' +
                        'This email is to confirm that training is completed by our Support coordinator(<b>' + $scope.trainAssignee.assignedTo.name + '</b>) as of <b>' + dateTime + '</b>' + ($scope.trainAssignee.trainedTo.toLowerCase() !== $scope.vendorName.toLowerCase() ? ' to ' + $scope.trainAssignee.trainedTo : ' ') + '.<br>' +
                        'We appreciate your support.<br><br>' +
                        'Reach out to us on <a href="tel:0919949575360">+91-9949575360</a> or <a href="mailto: Support@prm360.com">Support@prm360.com</a> for any further assistance.<br>' +
                        'Regards,<br><b>' +
                        $scope.trainAssignee.assignedTo.name + '</b>.'
                } else if ($scope.trainAssignee.status === 'Open' && $scope.trainAssignee.assignedTo && $scope.trainAssignee.assignedTo.name) {
                    $scope.trainAssignee.comments = 'Dear <b>' + $scope.vendorName + '</b>, <br><br>' +
                        'I am <b>' + $scope.trainAssignee.assignedTo.name + '</b> from PRM360 Vendor Support team.I have been assigned to help you with training on the application. I will be reaching out to you shortly for 15 minutes to help you in submitting the quotation on the System.<br><br>' +
                        'Thank you <br><b>' +
                        $scope.trainAssignee.assignedTo.name + '</b>.'
                } else if ($scope.trainAssignee.status === 'trainingScheduled' && $scope.trainAssignee.assignedTo && $scope.trainAssignee.assignedTo.name && date) {
                    $scope.trainAssignee.comments = 'Dear <b>' + $scope.vendorName + '</b>, <br><br>' +
                        'It was nice to hear from you. As per the communication, your training has been scheduled for ' + date.format('DD-MM-YYYY hh:mm') + '. I will be reaching out to you during the stipulated time.<br><br>' +
                        'Thank you <br><b>' +
                        $scope.trainAssignee.assignedTo.name + '</b>.'
                } else {
                    $scope.trainAssignee.comments = '';
                }

            }

            $scope.createTicket = function () {

                if (!$scope.trainAssignee.comments) {
                    return false;
                }

                $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
                let userObj = userService.getUserObj();
                //$scope.status = $scope.trainAssignee.status === 'trainingCompleted' ? 4 : 2;
                if ($scope.trainAssignee.status === 'trainingCompleted') {
                    $scope.status = 4;
                } else if ($scope.trainAssignee.status === 'trainingScheduled') {
                    $scope.status = 7;
                } else if ($scope.trainAssignee.status === 'Open') {
                    $scope.status = 2;
                }


                var tickectParams = {
                    name: $scope.trainAssignee.trainedTo,
                    email: $scope.trainAssignee.email,
                    phone: $scope.trainAssignee.contactNumber,
                    subject: ' New vendor training of ' + $scope.trainAssignee.companyName + ' for ' + $scope.clientName,
                    status: $scope.status,
                    priority: 3,
                    description: $scope.trainAssignee.comments,
                    email_config_id: 81000024659,
                    group_id: 81000274957,
                    source: 3,
                    type: 'New Training',
                    requester_id: null,
                    responder_id: $scope.trainAssignee.assignedTo.id,
                    tags: ['OPS Screen', $scope.clientName]
                };



                $http({
                    method: 'POST',
                    url: "https://prm360.freshdesk.com/api/v2/tickets",
                    encodeURI: true,
                    headers: {
                        'Content-Type': 'application/json',
                        "Authorization": "Basic " + window.btoa('oLRuHDsQvII8LrfthZAj' + ":x")
                    },
                    data: tickectParams,
                    dataType: "json"
                }).then(function (response) {
                    if (response.data) {
                        $scope.trainAssignee.freshDeskId = response.data.id;
                    }
                    //$scope.trainAssignee.comment = '';
                    return response.data;
                });


            };

            $scope.updateTicket = function () {

                $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
                let userObj = userService.getUserObj();
                //$scope.status = $scope.trainAssignee.status === 'trainingCompleted' ? 4 : 2;
                if ($scope.trainAssignee.status === 'trainingCompleted') {
                    $scope.status = 4;
                } else if ($scope.trainAssignee.status === 'trainingScheduled') {
                    $scope.status = 7;
                } else if ($scope.trainAssignee.status === 'Open') {
                    $scope.status = 2;
                }


                var tickectParams = {
                    name: $scope.trainAssignee.trainedTo,
                    email: $scope.trainAssignee.email,
                    phone: $scope.trainAssignee.contactNumber,
                    //subject: ' New vendor training of ' + $scope.trainAssignee.companyName + ' for ' + $scope.clientName,
                    status: $scope.status,
                    priority: 3,
                    description: $scope.trainAssignee.comments,
                    email_config_id: 81000024659,
                    group_id: 81000274957,
                    source: 3,
                    type: 'New Training',
                    requester_id: null,
                    responder_id: $scope.trainAssignee.assignedTo.id,
                    tags: ['OPS Screen', $scope.clientName]
                };



                $http({
                    method: 'PUT',
                    url: "https://prm360.freshdesk.com/api/v2/tickets/" + $scope.trainAssignee.freshDeskId,
                    encodeURI: true,
                    headers: {
                        'Content-Type': 'application/json',
                        "Authorization": "Basic " + window.btoa('oLRuHDsQvII8LrfthZAj' + ":x")
                    },
                    data: tickectParams,
                    dataType: "json"
                }).then(function (response) {
                    //if (response.data) {
                    //    $scope.trainAssignee.freshDeskId = response.data.id;
                    //}
                    //$scope.trainAssignee.comment = '';
                    //return response.data;
                });


            };

            $scope.editVendor = function (vendor) {

                $scope.trainAssignee.assignedTo = _.find($scope.assignToUsers, function (item) {
                    return item.name === vendor.trainerName;
                });
                $scope.trainAssignee.trainedTo = vendor.trainedContactName;
                $scope.trainAssignee.contactNumber = vendor.vendorPhone;
                $scope.trainAssignee.email = vendor.vendorEmail;
                $scope.trainAssignee.status = vendor.status;
                $scope.trainAssignee.vendorID = vendor.vendorID;
                $scope.trainAssignee.reqID = vendor.reqID;
                $scope.trainAssignee.postedOn = vendor.req_POSTED_ON;
                $scope.trainAssignee.vtID = vendor.VT_ID ? vendor.VT_ID : 0;
                $scope.trainAssignee.comments = vendor.comments;
                $scope.trainAssignee.freshDeskId = vendor.tickectNo;

            }


            $scope.saveAssignee = function () {

                if (!$scope.trainAssignee.assignedTo.name) {
                    return;
                }

                var params = {
                    vendTrain: {
                        vendorID: $scope.trainAssignee.vendorID,
                        reqID: $scope.trainAssignee.reqID,
                        trainerName: $scope.trainAssignee.assignedTo.name,
                        trainedContactName: $scope.trainAssignee.trainedTo + '(' + $scope.vendorName + ')',
                        vendorPhone: $scope.trainAssignee.contactNumber,
                        vendorEmail: $scope.trainAssignee.email,
                        status: $scope.trainAssignee.status,
                        comments: $scope.trainAssignee.comments,
                        tickectNo: $scope.trainAssignee.freshDeskId,
                        reqPostedDate: $scope.trainAssignee.postedOn,
                        VT_ID: $scope.trainAssignee.vtID
                    },
                    domain: $scope.DBName,
                    clientID: $scope.clientid ? $scope.clientid : 0,
                    isUpdate: $scope.trainAssignee.vtID ? 1 : 0
                }

                if ($scope.trainAssignee.vtID !== 0 && $scope.trainAssignee.freshDeskId) {
                    $scope.updateTicket();

                }

                opsServices.SaveVendorTraining(params).then(function (response) {


                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");

                    }
                    else if (response.errorMessage == "") {
                        growlService.growl("Saved Successfully");
                        $scope.GetVendorTrainingHistory($scope.Company_Name);
                        $scope.trainAssignee = {
                            assignedTo: {},
                            //trainedTo: '',
                            //contactNumber: '',
                            //email: '',
                            status: 'Open',
                            comments: '',
                            freshDeskId: '',
                            //companyName: '',
                            //vendorID: '',
                            postedOn: '',
                            //reqID: '',
                            vtID: 0,
                            isEdit: 0
                        }

                    }

                })

            }

        }]);