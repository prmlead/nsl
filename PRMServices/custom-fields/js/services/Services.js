prmApp.constant('PRMCustomFieldServiceDomain', 'custom-fields/svc/PRMCustomFieldService.svc/REST/');
prmApp.service('PRMCustomFieldService', ["PRMCustomFieldServiceDomain", "userService", "httpServices", "$q",
    function (PRMCustomFieldServiceDomain, userService, httpServices, $q) {
        var PRMCustomFieldService = this;
        PRMCustomFieldService.companyFieldTemplates = [];
        PRMCustomFieldService.getCustomFieldList = function (params) {
            params.customfieldid = params.customfieldid ? params.customfieldid : 0;
            params.fieldname = params.fieldname ? params.fieldname : '';
            params.fieldmodule = params.fieldmodule ? params.fieldmodule : '';
            let url = PRMCustomFieldServiceDomain + 'getcustomfields?compid=' + params.compid + '&customfieldid=' + params.customfieldid +
                '&fieldname=' + params.fieldname + '&fieldmodule=' + params.fieldmodule + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMCustomFieldService.GetCustomFieldsByModuleId = function (params) {
            let url = PRMCustomFieldServiceDomain + 'getcustomfieldsbymoduleid?compid=' + params.compid + '&moduleid=' + params.moduleid +
                '&fieldmodule=' + params.fieldmodule + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url, params);
        };

        //PRMCustomFieldService.GetPRMFieldMappingDetails = function (params) {
        //    params.templatename = params.templatename ? params.templatename : '';
        //    let url = PRMCustomFieldServiceDomain + 'getprmfieldmappingdetails?compid=' + params.compid + '&templatename=' + params.templatename + '&sessionid=' + userService.getUserToken();
        //    return httpServices.get(url, params);
        //};

        PRMCustomFieldService.GetPRMFieldMappingTemplates = function () {
            let url = PRMCustomFieldServiceDomain + 'getfieldmappingtemplates?compid=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMCustomFieldService.GetTemplates = function () {
            let url = PRMCustomFieldServiceDomain + 'templates?compid=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMCustomFieldService.GetTemplateFields = function (params) {
            let url = PRMCustomFieldServiceDomain + 'templatefields?templateid=' + params.templateid + '&templatename=' + params.templatename + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMCustomFieldService.GetPRMFieldMappingDetails = function () {
            var def = $q.defer();
            if (PRMCustomFieldService.companyFieldTemplates && PRMCustomFieldService.companyFieldTemplates.length > 0) {
                def.resolve(PRMCustomFieldService.companyFieldTemplates);
            } else {

                var params = {
                    compid: userService.getUserCatalogCompanyId(),
                    templatename: '',
                    sessionId: userService.getUserToken()
                };

                let url = PRMCustomFieldServiceDomain + 'getprmfieldmappingdetails?compid=' + params.compid + '&templatename=' + params.templatename + '&sessionid=' + userService.getUserToken();
                httpServices.get(url)
                    .then(function (response) {
                        def.resolve(response);
                        if (response && response.length > 0) {
                            PRMCustomFieldService.companyFieldTemplates = response;
                        }
                    });
            }

            return def.promise;
        };

        PRMCustomFieldService.getFieldAlaisName = function (fieldName, template, defaultName) {
            var aliasName = defaultName;
            if (!template && template === '') {
                template = 'DEFAULT';
            }
            //if ((userService.companyFieldTemplates && userService.companyFieldTemplates.length > 0) &&
            //    (PRMCustomFieldService.companyFieldTemplates && PRMCustomFieldService.companyFieldTemplates.length > 0)) {

            if (userService.companyFieldTemplates && userService.companyFieldTemplates.length > 0) {
                var objList = _.filter(userService.companyFieldTemplates, function (o) {
                    return o.TEMPLATE_NAME === template && o.FIELD_NAME === fieldName;
                });
                if (objList && objList.length > 0) {
                    aliasName = objList[0].FIELD_ALIAS_NAME;
                }
            }

            return aliasName;
        };

        PRMCustomFieldService.saveCustomField = function (params) {
            let url = PRMCustomFieldServiceDomain + 'savecustomfield';
            return httpServices.post(url, params);
        };

        PRMCustomFieldService.saveCustomFieldValue = function (params) {
            let url = PRMCustomFieldServiceDomain + 'savecustomfieldvalue';
            return httpServices.post(url, params);
        };

        PRMCustomFieldService.SaveTemplate = function (params) {
            let url = PRMCustomFieldServiceDomain + 'savetemplate';
            return httpServices.post(url, params);
        };

        PRMCustomFieldService.SaveTemplateFields = function (params) {
            let url = PRMCustomFieldServiceDomain + 'savetemplatefields';
            return httpServices.post(url, params);
        };

        PRMCustomFieldService.saveTemplateSubItem = function (params) {
            let url = PRMCustomFieldServiceDomain + 'savetemplatesubitem';
            return httpServices.post(url, params);
        };

        PRMCustomFieldService.getTemplateSubItem = function (params) {
            let url = PRMCustomFieldServiceDomain + 'gettemplatesubitem?templateid=' + params.templateid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        return PRMCustomFieldService;

    }]);