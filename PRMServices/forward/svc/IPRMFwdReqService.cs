﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMFwdReqService
    {

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementdata?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        FwdRequirement GetRequirementData(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getbidhistory?reqid={reqID}&userID={userID}&sessionid={sessionID}")]
        List<Comment> GetBidHistory(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "requirementsave")]
        Response RequirementSave(FwdRequirement requirement, byte[] attachment);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "endnegotiation")]
        Response EndNegotiation(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "restartnegotiation")]
        Response RestartNegotiation(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "stopbids")]
        Response StopBids(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleterequirement")]
        Response DeleteRequirement(int reqID, int userID, string sessionID, string reason);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "quatationapproval")]
        Response QuatationApproval(int reqID, int customerID, int vendorID, bool value, string reason, string sessionID, string action);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "savepricecap")]
        Response SavePriceCap(int reqID, string sessionID, int userID, string reqType, double priceCapValue, int isUnitPriceBidding, bool IS_CB_ENABLED);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverunningitemprice")]
        Response SaveRunningItemPrice(List<RequirementItems> itemsList, int userID, int reqID, double price, double vendorBidPrice, double freightcharges,
            double revfreightCharges, double revpackingCharges, double revinstallationCharges);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "makeabid")]
        Response MakeABid(int reqID, int userID, double price, FileUpload[] quotation, string quotationName, string sessionID,
            double tax, double freightcharges, string warranty, string payment, string duration, string validity,
            List<FwdRequirementItems> quotationObject, int revised, double priceWithoutTax, string type, double discountAmount,
            List<RequirementTaxes> listRequirementTaxes, string otherProperties);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "uploadquotation")]
        Response UploadQuotation(List<FwdRequirementItems> quotationObject, int userID, int reqID, string sessionID, double price,
            double tax, double freightcharges, double vendorBidPrice, string warranty, string payment, string duration,
            string validity, int revised, string DesfileName, double discountAmount, List<RequirementTaxes> listRequirementTaxes,
            string otherProperties, string gstNumber, double packingCharges, double packingChargesTaxPercentage, double packingChargesWithTax,
            double installationCharges, double installationChargesTaxPercentage, double installationChargesWithTax, double revpackingCharges, double revinstallationCharges,
            double revpackingChargesWithTax, double revinstallationChargesWithTax, string selectedVendorCurrency,
            string uploadType, List<FileUpload> multipleAttachments,

            double freightCharges,
            double freightChargesTaxPercentage,
            double freightChargesWithTax,
            double revfreightCharges,
            double revfreightChargesWithTax,
            string INCO_TERMS);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "getrevisedquotations")]
        Response GetRevisedQuotations(int reqID, int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getmyauctions?userid={userID}&sessionid={sessionID}")]
        List<FwdRequirement> GetMyAuctions(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getactiveleads?userid={userID}&sessionid={sessionID}")]
        List<FwdRequirement> GetActiveLeads(int userID, string sessionID);
        
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanyleads?userid={userID}&searchstring={searchString}&searchtype={searchType}&sessionid={sessionID}")]
        List<FwdRequirement> GetCompanyLeads(int userID, string searchString, string searchType, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getslotsbyslotid?slotid={slotid}&reqid={reqid}&sessionid={sessionid}")]
        List<SlotBooking> GetSlotsBySlotId(int slotid, int reqid, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "updatepricecap")]
        Response UpdatePriceCap(int uID, int reqID, double price, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "bookSlots")]
        Response BookSlots(SlotBooking sltBook, string sessionID);


        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "approveRejectSlot")]
        Response ApproveRejectSlot(int flagValue, int vendorID, int reqID, string sessionID, int customerID, int slotID);



        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "deleteSlot")]
        Response DeleteSlot(int slotID, int entityID, string sessionID);



    }
}
