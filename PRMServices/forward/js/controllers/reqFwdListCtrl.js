﻿
prmApp
    .controller('reqFwdListCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService",
        "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService", "PRMFwdReqServiceService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService,
            fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, PRMFwdReqServiceService) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 8;
            $scope.itemsPerPage2 = 8;
            $scope.maxSize = 8;
            $scope.myAuctions = [];
            $scope.categories = [];
            $scope.reqStatus = 'ALL';
            $scope.category = 'ALL CATEGORIES';


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

            $scope.pageChanged = function () {
                //$scope.getAuctions();
            };

            $scope.getAuctions = function (isfilter) {
                    if (isfilter) {
                        $scope.totalItems = 0;
                        $scope.currentPage = 1;
                        $scope.currentPage2 = 1;
                        $scope.itemsPerPage = 8;
                        $scope.itemsPerPage2 = 8;
                        $scope.maxSize = 8;
                        $scope.myAuctions = [];
                    }

                    PRMFwdReqServiceService.getmyAuctions({ "userid": userService.getUserId(), "reqstatus": $scope.reqStatus, "sessionid": userService.getUserToken() })
                        .then(function (response) {
                            $scope.myAuctions1 = [];
                            $scope.myAuctions1 = response;

                            $scope.myAuctions1.forEach(function (item, index) {
                                item.postedOn = $scope.GetDateconverted(item.postedOn);
                                item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                                item.expStartTime = $scope.GetDateconverted(item.expStartTime);
                                item.startTime = $scope.GetDateconverted(item.startTime);

                                if (String(item.startTime).includes('9999')) {
                                    item.startTime = '';
                                }


                            })

                            $scope.myAuctions = $scope.myAuctions1;
                            if ($scope.myAuctions.length > 0) {
                                $scope.myAuctionsLoaded = true;
                                $scope.totalItems = $scope.myAuctions.length;
                            } else {
                                $scope.myAuctionsLoaded = false;
                                $scope.totalItems = 0;
                                $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                            }
                            $scope.tempStatusFilter = angular.copy($scope.myAuctions);
                            $scope.tempCategoryFilter = angular.copy($scope.myAuctions);
                        });
            }

            $scope.getAuctions();

            $scope.GetRequirementsReport = function () {
                PRMFwdReqServiceService.getrequirementsreport({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.reqReport = response;
                        alasql.fn.handleDate = function (date) {
                            return new moment(date).format("MM/DD/YYYY");
                        }
                        alasql('SELECT requirementID as RequirementID, title as RequirementTitle, handleDate(postedDate) as [PostedDate], status as Status, vendorID as VendorID, companyName as CompanyName, userPhone as Phone,userEmail as Email, itemID as [ItemID], productBrand as Brand, productQuantity as Quantity, productQuantityIn as [Units], costPrice as [InitialCostPrice], revCostPrice as [FinalCostPrice], netPrice as [InitialNetPrice], marginAmount as [InitialMarginAmount], unitDiscount as [MarginPercentage],revUnitDiscount as [RevisedMarginPercentage],unitMRP as [MRP],gst as [GST],savings as [Savings], handleDate(startTime) as [StartTime], status as [Status], othersBrands as [ManufacturerName] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xlsx", $scope.reqReport]);
                    });

                //alasql('SELECT requirementID as [RequirementID],title as [Title],vendorID as [VendorID],companyName as [Company],userPhone as [Phone],userEmail as [Email],itemID as [ItemID],productBrand as [Brand],productQuantity as [Quantity], productQuantityIn as [Units],costPrice as [InitialCostPrice],revCostPrice as [FinalCostPrice],netPrice as [NetPrice],marginAmount as [InitialMarginAmount],unitDiscount as [InitialMargin],revUnitDiscount as [FinalMargin],unitMRP as [MRP],gst as [GST],maxInitMargin as [MaximumInitialMargin],maxFinalMargin as [MaximumFinalMargin],savings as [Savings] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xslx", $scope.reqReport]);

            }

            $scope.goToReqReport = function (reqID) {
                //$state.go("reports", { "reqID": reqID });

                var url = $state.href('reports', { "reqID": reqID });
                $window.open(url, '_blank');

            };

            $scope.cloneRequirement = function (reqID) {
                let tempObj = {};
                tempObj.cloneId = reqID;
                //$state.go('form.addnewrequirement', { 'Id': reqID, reqObj: tempObj });

                $state.go('save-fwd-req', { 'Id': reqID, reqObj: tempObj });
            };

            $scope.searchTable = function (str) {

                $scope.category = 'ALL CATEGORIES';
                $scope.reqStatus = 'ALL';
                $scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                    return (String(req.requirementID).includes(str) == true || String(req.title).includes(str) == true || req.category[0].includes(str) == true);
                });

                $scope.totalItems = $scope.myAuctions.length;
            }


            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));

                            $scope.categories.push('ALL CATEGORIES');

                            //categories.splice(categories.indexOf("ALL"), 1);
                            //categories.unshift('ALL');

                            $scope.categories = $scope.categories.filter(item => item !== "ALL CATEGORIES" && item !== "");
                            $scope.categories.unshift("ALL CATEGORIES");

                            //console.log($scope.categories);

                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    } else {
                    }
                }, function (result) {
                });

            };


            $scope.getCategories();

            $scope.getStatusFilter = function (filterVal) {
                $scope.filterArray = [];
                if (filterVal == 'ALL') {
                    $scope.myAuctions = $scope.myAuctions1;

                } else {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        return item.status == filterVal;
                    });
                    $scope.myAuctions = $scope.filterArray;
                }

                $scope.totalItems = $scope.myAuctions.length;

            }

            $scope.getCategoryFilter = function (filterCategoryVal) {
                $scope.filterCatArray = [];
                if (filterCategoryVal == 'ALL CATEGORIES') {
                    $scope.myAuctions = $scope.myAuctions1;
                } else {
                    $scope.filterCatArray = $scope.tempCategoryFilter.filter(function (item) {
                        return item.category[0] == filterCategoryVal;
                    });
                    $scope.myAuctions = $scope.filterCatArray;

                }

                $scope.totalItems = $scope.myAuctions.length;
            }

        }]);