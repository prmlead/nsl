prmApp.constant('PRMPRServicesDomain', 'pr/svc/PRMPRService.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMPRServices', ["PRMPRServicesDomain", "userService", "httpServices",
    function (PRMPRServicesDomain, userService, httpServices) {


        var PRMPRServices = this;


        PRMPRServices.GetSeries = function (series, seriestype, deptid) {
            var url = PRMPRServicesDomain + 'getseries?series=' + series + '&seriestype=' + seriestype + '&compid=' + userService.getUserCompanyId() + '&deptid=' + deptid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.getprdetails = function (params) {
            let url = PRMPRServicesDomain + 'getprdetails?prid=' + params.prid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.GetItemDetails = function (params) {
            let url = PRMPRServicesDomain + 'GetItemDetails?prIDS=' + params.prIds + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.GetPRItemsByReqId = function (params) {
            let url = PRMPRServicesDomain + 'getpritemsbyreq?reqid=' + params.reqid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.DeLinkInValidPRItems = function (params) {
            let url = PRMPRServicesDomain + 'delinkinvalidpritems?reqid=' + params.reqid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.getFilterValues = function (params) {
            let url = PRMPRServicesDomain + 'getFilterValues?compid=' + params.compid + '&verticalType=' + params.verticalType + '&sessionid=' + userService.getUserToken() + '&fromdate=' + params.fromDate + '&todate=' + params.toDate;
            return httpServices.get(url);
        };
        
        PRMPRServices.getprlist = function (params) {
            let url = PRMPRServicesDomain + 'getprlist?userid=' + params.userid + '&sessionid=' + params.sessionid + '&deptid=' + params.deptid
                + '&desigid=' + params.desigid + '&depttypeid=' + params.depttypeid + '&onlyopen=' + 0
                + '&plant=' + params.plant + '&projectType=' + params.projectType + '&sectionHead=' + params.sectionHead + '&wbsCode=' + params.wbsCode + '&profitCentre=' + params.profitCentre
                + '&purchaseCode=' + params.purchaseCode + '&creatorName=' + params.creatorName
                + '&clientName=' + params.clientName + '&prStatus=' + params.prStatus + '&search=' + params.searchString
                + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords + '&fromdate=' + params.fromDate + '&todate=' + params.toDate + '&verticalType=' + params.verticalType + '&isNonReleaseDate=' + params.isNonReleaseDate;
            return httpServices.get(url);
        };

        PRMPRServices.GetFullPRDetails = function (params) {
            let url = PRMPRServicesDomain + 'getFullPRDetails?prid=' + params.userid + '&sessionid=' + params.sessionid + '&deptid=' + params.deptid + '&desigid=' + params.desigid + '&depttypeid=' + params.depttypeid;
            return httpServices.get(url);
        };

        PRMPRServices.savePrDetails = function (params) {
            let url = PRMPRServicesDomain + 'saveprdetails';
            return httpServices.post(url, params);
        };

        PRMPRServices.savePRActions = function (params) {
            let url = PRMPRServicesDomain + 'savepractions';
            return httpServices.post(url, params);
        };

        PRMPRServices.getreqprlist = function (params) {
            //let url = PRMPRServicesDomain + 'getreqprlist?userid=' + params.userid + '&sessionid=' + userService.getUserToken();
            //return httpServices.get(url);
            if (params.onlyopen) {
                params.onlyopen = 1;
            } else {
                params.onlyopen = 0;
            }

            let url = PRMPRServicesDomain + 'getprlist?userid=' + params.userid + '&sessionid=' + params.sessionid + '&deptid=' + params.deptid + '&desigid=' + params.desigid + '&depttypeid=' + params.depttypeid + '&onlyopen=' + params.onlyopen;
            return httpServices.get(url);
        };

        PRMPRServices.GetCompanyRFQCreators = function (params) {
            let url = PRMPRServicesDomain + 'getcompanyrfqcreators?u_id=' + userService.getUserId() + '&pr_id=' + params.pr_id + '&sessionid=' + userService.getUserToken();// + '&dept_id=' + params.dept_id
            return httpServices.get(url);
        };

        PRMPRServices.SaveCompanyRFQCreators = function (params) {
            let url = PRMPRServicesDomain + 'savecompanyrfqcreators';
            return httpServices.post(url, params);
        };

        PRMPRServices.getpritemslist = function (params) {
            let url = PRMPRServicesDomain + 'getpritemslist?prid=' + params.prid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.getrequirementprstatus = function (params) {
            let url = PRMPRServicesDomain + 'getrequirementprstatus?reqid=' + params.reqid + '&prid=' + params.prid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.getprfieldmapping = function (params) {
            let url = PRMPRServicesDomain + 'getprfieldmapping?type=' + params.type + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.linkRFPToPR = function (params) {
            let url = PRMPRServicesDomain + 'linktorfp?reqid=' + params.reqid + '&prid=' + params.prid + '&user=' + params.userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        /********  CONSOLIDATE PR ********/

        PRMPRServices.GetIndividualItems = function (params) {
            let url = PRMPRServicesDomain + 'GetIndividualItems?userId=' + userService.getUserId() + '&compId=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken()
                + '&plant=' + params.plant + '&projectType=' + params.projectType + '&sectionHead=' + params.sectionHead + '&wbsCode=' + params.wbsCode + '&profitCentre=' + params.profitCentre + '&purchaseCode=' + params.purchaseCode + '&creatorName=' + params.creatorName + '&clientName=' + params.clientName + '&search=' + params.search + '&isCommonItem=' + params.isCommonItem + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords + '&verticalType=' + params.verticalType;
            return httpServices.get(url);
        };

        PRMPRServices.GetPRSbyItem = function (params) {
            let url = PRMPRServicesDomain + 'GetPRSbyItem?productId=' + params.PRODUCT_ID + '&userId=' + userService.getUserId() +
                '&compId=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken()
                + '&plant=' + params.plant + '&projectType=' + params.projectType + '&sectionHead=' + params.sectionHead + '&wbsCode=' + params.wbsCode + '&profitCentre=' + params.profitCentre + '&search=' + params.search;
            return httpServices.get(url);
        };

        /********  CONSOLIDATE PR ********/

        return PRMPRServices;

}]);