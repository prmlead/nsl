﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class PRFieldMapping : Entity
    {
        [DataMember] [DataNames("FIELD_VALUE")] public string FIELD_VALUE { get; set; }
        [DataMember] [DataNames("FIELD_NAME")] public string FIELD_NAME { get; set; }
        [DataMember] [DataNames("FIELD_TYPE")] public string FIELD_TYPE { get; set; }
        [DataMember] [DataNames("FIELD_PK_ID")] public int FIELD_PK_ID { get; set; }
        [DataMember] [DataNames("TYPE_OF_VERTICAL")] public string TYPE_OF_VERTICAL { get; set; }
        [DataMember] [DataNames("ID")] public string ID { get; set; }
        [DataMember] [DataNames("NAME")] public string NAME { get; set; }
        [DataMember] [DataNames("TYPE")] public string TYPE { get; set; }
    }
}