﻿prmApp
    .controller('prActionsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "PRMPRServices", "poService", "$rootScope", "catalogService",
        "fileReader",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService, PRMPRServices, poService, $rootScope, catalogService,
            fileReader) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.prID = $stateParams.Id;
            $scope.sendcommunication = true;

            $scope.prStatus = [
                //{
                //    display: 'PENDING',
                //    value: 'PENDING'
                //},
                
                {
                    display: 'NOT REQUIRED FOR RFQ',
                    value: 'NOT_REQUIRED_FOR_RFQ'
                }
                //,
                //{
                //    display: 'COMPLETED',
                //    value: 'COMPLETED'
                //},
                //{
                //    display: 'CANCELLED',
                //    value: 'CANCELLED'
                //},
                //{
                //    display: 'REJECTED',
                //    value: 'REJECTED'
                //}
            ];
          
            $scope.savePr = function () {

                var params = {
                    "prdetails": $scope.PRDetails,
                    "sendcommunication": $scope.sendcommunication,
                    "sessionid": userService.getUserToken()
                };

                var ts = userService.toUTCTicks($scope.PRDetails.REQUEST_DATE);
                var m = moment(ts);
                var reqdate = new Date(m);
                var milliseconds = parseInt(reqdate.getTime() / 1000.0);
                params.prdetails.REQUEST_DATE = "/Date(" + milliseconds + "000++530)/";

                ts = userService.toUTCTicks($scope.PRDetails.REQUIRED_DATE);
                m = moment(ts);
                var reqrddate = new Date(m);
                milliseconds = parseInt(reqrddate.getTime() / 1000.0);
                params.prdetails.REQUIRED_DATE = "/Date(" + milliseconds + "000++530)/";
                
                PRMPRServices.savePRActions(params)
                    .then(function (response) {

                        if (response.errorMessage !== '') {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Saved Successfully.", "success");
                        }
                    });
            };


            $scope.getprdetails = function () {
                var params = {
                    "prid": $stateParams.Id,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {

                        $scope.PRDetails = response;                        
                        $scope.PRDetails.REQUEST_DATE = userService.toLocalDate($scope.PRDetails.REQUEST_DATE);
                        $scope.PRDetails.REQUIRED_DATE = userService.toLocalDate($scope.PRDetails.REQUIRED_DATE);                       
                    });
            };


            if ($stateParams.Id > 0) {
                $scope.getprdetails();
            }

            $scope.goToPrList = function (id) {
                var url = $state.href("list-pr");
                window.open(url, '_self');
            };
        }]);
