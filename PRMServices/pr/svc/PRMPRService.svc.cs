﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Models;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMPRService : IPRMPRService
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        public PRMServices prm = new PRMServices();

        #region Services

        public PRDetails GetPRDetails(int prid, string sessionid)
        {
            PRDetails details = new PRDetails();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PR_ID", prid);
                CORE.DataNamesMapper<PRDetails> mapper = new CORE.DataNamesMapper<PRDetails>();
                var dataset = sqlHelper.SelectList("pr_GetPRDetails", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();

                details.PRItemsList = new List<PRItems>();
                details.PRItemsList = GetPRItemsList(prid, sessionid);

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<PRItems> GetPRItemsList(int prid, string sessionid)
        {
            List<PRItems> details = new List<PRItems>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PR_ID", prid);
                CORE.DataNamesMapper<PRItems> mapper = new CORE.DataNamesMapper<PRItems>();
                var dataset = sqlHelper.SelectList("pr_GetPRItems", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }

            return details;
        }

        public List<PRItems> GetPRItemsByReq(int reqid, string sessionid)
        {
            List<PRItems> details = new List<PRItems>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqid);
                CORE.DataNamesMapper<PRItems> mapper = new CORE.DataNamesMapper<PRItems>();
                var dataset = sqlHelper.SelectList("pr_GetPRItemsByReq", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }

            return details;
        }

        public Response DeLinkInValidPRItems(int reqid, string sessionid)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<PRFieldMapping> mapper = new CORE.DataNamesMapper<PRFieldMapping>();
                string query = string.Format($"Update PRItems SET REQ_ID = 0 WHERE REQ_ID = {reqid} AND IS_VALID = 0");
                sqlHelper.ExecuteNonQuery_IUD(query);

                response.ObjectID = 1;
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
                response.ErrorMessage = ex.Message;
                response.ObjectID = -1;
            }

            return response;
        }

        public RequirementPRStatus GetRequirementPRStatus(int reqid, int prid, string sessionid)
        {
            RequirementPRStatus details = new RequirementPRStatus();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqid);
                sd.Add("P_PR_ID", prid);
                CORE.DataNamesMapper<PRItems> mapper = new CORE.DataNamesMapper<PRItems>();
                var dataset = sqlHelper.SelectList("pr_GetRequirementPRStatus", sd);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    List<PRItems> prItems = new List<PRItems>();
                    foreach (DataRow row in dataset.Tables[0].Rows)
                    {
                        var prNumber = row["PR_NUMBER"] != DBNull.Value ? Convert.ToString(row["PR_NUMBER"]) : string.Empty;
                        if (!prItems.Any(p=>p.PR_NUMBER == prNumber))
                        {
                            PRItems prItem = new PRItems();
                            prItem.PR_ID = row["PR_ID"] != DBNull.Value ? Convert.ToInt32(row["PR_ID"]) : 0;
                            prItem.ITEM_CODE = row["ITEM_CODE"] != DBNull.Value ? Convert.ToString(row["ITEM_CODE"]) : string.Empty;
                            prItem.REQUIRED_QUANTITY = row["REQUIRED_QUANTITY"] != DBNull.Value ? Convert.ToDecimal(row["REQUIRED_QUANTITY"]) : 0;
                            prItem.UNITS = row["UNITS"] != DBNull.Value ? Convert.ToString(row["UNITS"]) : string.Empty;
                            prItem.PR_NUMBER = prNumber;
                            prItems.Add(prItem);
                        }
                    }

                    if (prItems.Count > 0)
                    {
                        details.PRItemsList = prItems.ToArray();
                        details.MESSAGE_TYPE = "WARNING";
                        details.ACTION_REQUIRED = 1;
                        details.MESSAGE += $"<br/><strong>Warning:</strong> We found {prItems.Count} more PR(s) which contain similar items of current PR selected. You may want to merge the PRs.<br/>";
                        foreach(var pr in prItems)
                        {
                            details.MESSAGE += $"PR Number: <strong>{pr.PR_NUMBER} - {pr.REQUIRED_QUANTITY} (QTY) - {pr.ITEM_CODE} (Product Code) </strong><br/>";
                        }

                        details.MESSAGE += $"Note: Please adjust quantity accordingly  when you choose link the PR(s).<br/>";
                    }
                }

                if (dataset != null && dataset.Tables.Count > 1 && dataset.Tables[1].Rows.Count > 0)
                {
                    DataRow row = dataset.Tables[1].Rows[0];
                    details.RequirementDetails = new Requirement();
                    details.RequirementDetails.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    details.RequirementDetails.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    details.RequirementDetails.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;

                    var status = details.RequirementDetails.Status.Equals("NOTSTARTED") ? "OPEN" : details.RequirementDetails.Status;

                    string url = "prm360.html#/save-requirementAdv/" + details.RequirementDetails.RequirementID;
                    details.MESSAGE_TYPE = "WARNING";
                    details.MESSAGE += $"<br/><br/><strong>Warning:</strong> We found a requirement: <strong>{details.RequirementDetails.Title} ({details.RequirementDetails.RequirementID})</strong> with same PR attached. You may want to check that requirement.<br/>";
                    details.MESSAGE += $"<a href=\"{url}\">Click here </a> to view the requirement";
                }

                if (dataset != null && dataset.Tables.Count > 2 && dataset.Tables[2].Rows.Count > 0)
                {
                    details.MESSAGE_TYPE = "ERROR";
                    details.MESSAGE += $"<br/><br/><strong>Error:</strong> We found item(s) that marked deleted on PR(s), below are details:";
                    foreach (DataRow row in dataset.Tables[2].Rows)
                    {
                        var pr = row["PR_NUMBER"] != DBNull.Value ? Convert.ToString(row["PR_NUMBER"]) : string.Empty;
                        var item = row["ITEM_CODE"] != DBNull.Value ? Convert.ToString(row["ITEM_CODE"]) : string.Empty;
                        details.MESSAGE += $"<br/><strong>PR:{pr}</strong> - Item Code: {item}";
                    }

                    details.MESSAGE += $"<br/>Please take action accordingly.";
                }

                if (dataset != null && dataset.Tables.Count > 3 && dataset.Tables[3].Rows.Count > 0)
                {
                    details.MESSAGE_TYPE = "WARNING";
                    details.MESSAGE += $"<br/><br/><strong>Warning:</strong> We found there is a mis-match of quantity between Requirement and PR(s) linked.";
                    foreach (DataRow row in dataset.Tables[3].Rows)
                    {
                        var item = row["PROD_NO"] != DBNull.Value ? Convert.ToString(row["PROD_NO"]) : string.Empty;
                        var reqQty = row["REQ_QTY"] != DBNull.Value ? Convert.ToString(row["REQ_QTY"]) : string.Empty;
                        var prQty = row["PR_QTY"] != DBNull.Value ? Convert.ToString(row["PR_QTY"]) : string.Empty;

                        details.MESSAGE += $"<br/><strong>Item:{item}</strong> - Requirement Quantity: {reqQty}, PR Quantity: {prQty}";
                    }

                    details.MESSAGE += $"<br/>Please take action accordingly.";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<PRDetails> GetReqPRList(int userid, string sessionid)
        {
            List<PRDetails> details = new List<PRDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userid);
                CORE.DataNamesMapper<PRDetails> mapper = new CORE.DataNamesMapper<PRDetails>();
                var dataset = sqlHelper.SelectList("pr_GetReqPRList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<PRFieldMapping> GetPRFieldMapping(string type, string sessionid)
        {
            List<PRFieldMapping> details = new List<PRFieldMapping>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<PRFieldMapping> mapper = new CORE.DataNamesMapper<PRFieldMapping>();
                string query = string.Format("SELECT * FROM ERP_FIELD_MAPPING WHERE FIELD_TYPE IN ({0})", type.ToUpper());
                var dataTable = sqlHelper.SelectQuery(query);
                details = mapper.Map(dataTable).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<PRFieldMapping> GetFilterValues(int compID,string verticalType, string sessionID, string fromdate, string todate)
        {
            List<PRFieldMapping> details = new List<PRFieldMapping>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_VERTICAL", verticalType);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                CORE.DataNamesMapper<PRFieldMapping> mapper = new CORE.DataNamesMapper<PRFieldMapping>();
                var dataset = sqlHelper.SelectList("pr_GetPRFilterValues", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }

            return details;
        }
        
        public List<PRItems> GetItemDetails(string prIDS, string sessionID)
        {
            List<PRItems> items = new List<PRItems>();
            try
            {
                Utilities.ValidateSession(sessionID);
                CORE.DataNamesMapper<PRItems> mapper = new CORE.DataNamesMapper<PRItems>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PR_IDS", prIDS);
                var dataset = sqlHelper.SelectList("pr_GetPRItemDetails", sd);
                items = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }

            return items;
        }

        public Response GetSeries(string series, string seriesType, string sessionID, int compID, int deptID)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_SERIES", series);
                sd.Add("P_SERIES_TYPE", seriesType);
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_DEPT_ID", deptID);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var ds = sqlHelper.SelectList("wf_GetSeries", sd);
                //details = mapper.Map(dataset.Tables[0]).tofirst

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    details.Message = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    details.Message = details.Message.ToUpper();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response SavePRActions(PRDetails prdetails, bool sendcommunication, string sessionid)
        {

            Response details = new Response();
           
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                sd.Add("P_PR_ID", prdetails.PR_ID);
                sd.Add("P_U_ID", prdetails.U_ID);
                sd.Add("P_PR_STATUS", prdetails.PR_STATUS);
                sd.Add("P_COMMENTS", prdetails.COMMENTS);


                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("pr_SavePRActions", sd);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

                if (sendcommunication)
                {
                    //Utilities.SendEmail(vendor.AltEmail, subject, body, 0, vendor.UserID, "COMMUNICATIONS", sessionID).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response SavePRDetails(PRDetails prdetails, string sessionid)
        {
            Response details = new Response();
            return details;
        }

        public static Response SendPRTelegramAlerts(string bodyTelegram)
        {
            Response response = new Response();
            PRMServices prmservices = new PRMServices();

            bodyTelegram = bodyTelegram.Replace("<br/>", "");
            bodyTelegram = bodyTelegram.Replace("<br />", "");

            TelegramMsg tgMsg = new TelegramMsg();
            tgMsg.Message = bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0];
            prmservices.SendTelegramMsg(tgMsg);

            return response;
        }

        public List<PRDetails> GetPRList(int userid, string sessionid, int deptID, int desigID, int deptTypeID, int onlyopen, 
            string plant, string projectType, string sectionHead, string wbsCode, string profitCentre, string purchaseCode, string creatorName, string clientName, string prStatus, string search,
            string fromdate, string todate,string verticalType,int isNonReleaseDate, int PageSize = 0, int NumberOfRecords = 0)
        {
            List<PRDetails> details = new List<PRDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userid);
                sd.Add("P_DEPT_ID", deptID);
                sd.Add("P_DESIG_ID", desigID);
                sd.Add("P_DEPT_TYPE_ID", deptTypeID);
                sd.Add("P_PLANT", plant);
                sd.Add("P_PROJECT_TYPE", projectType);
                sd.Add("P_SECTION_HEAD", sectionHead);
                sd.Add("P_WBS_CODE", wbsCode);
                sd.Add("P_PROFIT_CENTRE", profitCentre);
                sd.Add("P_PURCHASE_GROUP", purchaseCode);
                sd.Add("P_CREATOR_NAME", creatorName);
                sd.Add("P_CLIENT_NAME", clientName);
                sd.Add("P_PR_STATUS", prStatus);
                sd.Add("P_SEARCH", search);
                sd.Add("P_PAGE", PageSize);
                sd.Add("P_PAGE_SIZE", NumberOfRecords);
                sd.Add("P_FROM_DATE", isNonReleaseDate == 1 ? null : fromdate);
                sd.Add("P_TO_DATE", isNonReleaseDate == 1 ? null : todate);
                sd.Add("P_VERTICAL_TYPE", verticalType);
                sd.Add("P_NON_RELEASE_DATE", isNonReleaseDate);
                CORE.DataNamesMapper<PRDetails> mapper = new CORE.DataNamesMapper<PRDetails>();
                var dataset = sqlHelper.SelectList("pr_GetPRList_paging_1", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }

            return details;
        }

        public string generatePRNumber(string series, string type, string sessionID, int compID, int deptID, string prNum)
        {
            string genPrNumber = "";

            Response a = GetSeries(series, type, sessionID, compID, deptID);

            int b = a.ObjectID;

            if (b <= 0)
            {
                b = 1;
            }
            if (b < 10)
            {
                genPrNumber = prNum + "/" + "000000000" + b;
            }
            if (b > 9 && b < 100)
            {
                genPrNumber = prNum + "/" + "00000000" + b;
            }
            if (b > 99 && b < 1000)
            {
                genPrNumber = prNum + "/" + "0000000" + b;
            }
            if (b > 999 && b < 10000)
            {
                genPrNumber = prNum + "/" + "000000" + b;
            }
            if (b > 9999 && b < 100000)
            {
                genPrNumber = prNum + "/" + "00000" + b;
            }
            if (b > 99999 && b < 1000000)
            {
                genPrNumber = prNum + "/" + "0000" + b;
            }
            if (b > 999999 && b < 10000000)
            {
                genPrNumber = prNum + "/" + "000" + b;
            }
            if (b > 9999999 && b < 100000000)
            {
                genPrNumber = prNum + "/" + "00" + b;
            }
            if (b > 99999999 && b < 100000000)
            {
                genPrNumber = prNum + "/" + "0" + b;
            }

            return genPrNumber;
        }

        public List<PRRFQCreator> GetCompanyRFQCreators(int U_ID, int PR_ID, string sessionid)//, int dept_id
        {
            List<PRRFQCreator> details = new List<PRRFQCreator>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_MODULE_ID", PR_ID);
                //  sd.Add("P_DEPT_ID", dept_id);
                CORE.DataNamesMapper<PRRFQCreator> mapper = new CORE.DataNamesMapper<PRRFQCreator>();
                var ds = sqlHelper.SelectList("pr_GetCompanyRFQCreators", sd);
                details = mapper.Map(ds.Tables[0]).ToList();

                //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                //{
                //    details.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                //    details.Message = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                //    details.Message = details.Message.ToUpper();
                //}
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public List<PRItems> GetIndividualItems(int userId, int compId, string sessionid, string plant, string projectType, string sectionHead, string wbsCode, string profitCentre, string purchaseCode, string creatorName, string clientName, string search,int isCommonItem, string verticalType, int PageSize = 0, int NumberOfRecords = 0)
        {
            List<PRItems> details = new List<PRItems>();
            List<PRItems> totals = new List<PRItems>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userId);
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_PLANT", plant);
                sd.Add("P_PROJECT_TYPE", projectType);
                sd.Add("P_SECTION_HEAD", sectionHead);
                sd.Add("P_WBS_CODE", wbsCode);
                sd.Add("P_PROFIT_CENTRE", profitCentre);
                sd.Add("P_PURCHASE_GROUP", purchaseCode);
                sd.Add("P_CREATOR_NAME", creatorName);
                sd.Add("P_CLIENT_NAME", clientName);
                sd.Add("P_SEARCH", search);
                sd.Add("P_IS_COMMON_ITEM", isCommonItem);
                sd.Add("P_PAGE", PageSize);
                sd.Add("P_PAGE_SIZE", NumberOfRecords);
                sd.Add("P_VERTICAL_TYPE", verticalType);
                CORE.DataNamesMapper<PRItems> mapper = new CORE.DataNamesMapper<PRItems>();
                var dataset = sqlHelper.SelectList("pr_GetIndividualItems", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
                totals = mapper.Map(dataset.Tables[1]).ToList();

                foreach (PRItems item in details)
                {
                    if (totals != null && totals.Count > 0)
                    {
                        item.TOTAL_PR_COUNT = totals.Where(a => a.PRODUCT_ID == item.PRODUCT_ID).FirstOrDefault().TOTAL_PR_COUNT;
                        item.OVERALL_ITEM_QUANTITY = totals.Where(a => a.PRODUCT_ID == item.PRODUCT_ID).FirstOrDefault().OVERALL_ITEM_QUANTITY;
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }

            return details;
        }

        public List<PRDetails> GetPRSbyItem(int productId, int userId, int compId, string sessionid, string plant, string projectType, string sectionHead, string wbsCode, string profitCentre, string search)
        {
            List<PRDetails> details = new List<PRDetails>();
            List<PRDetails> counts = new List<PRDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PRODUCT_ID", productId);
                sd.Add("P_U_ID", userId);
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_PLANT", plant);
                sd.Add("P_PROJECT_TYPE", projectType);
                sd.Add("P_SECTION_HEAD", sectionHead);
                sd.Add("P_WBS_CODE", wbsCode);
                sd.Add("P_PROFIT_CENTRE", profitCentre);
                sd.Add("P_SEARCH", search);
                CORE.DataNamesMapper<PRDetails> mapper = new CORE.DataNamesMapper<PRDetails>();
                var dataset = sqlHelper.SelectList("pr_GetPRSbyItem", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
                counts = mapper.Map(dataset.Tables[1]).ToList();

                foreach (PRDetails detail in details)
                {
                    detail.TOTAL_ITEMS = counts.Where(a => a.PR_ID == detail.PR_ID).FirstOrDefault().TOTAL_ITEMS;
                    detail.REFERRED_PR_ITEM_IDS = counts.Where(a => a.PR_ID == detail.PR_ID).FirstOrDefault().REFERRED_PR_ITEM_IDS;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }
        
        public Response LinkToRFP(int reqid, int prid, int user, string sessionid)
        {
            Response response = new Response();
            try
            {
                if ( reqid <=0 || prid <= 0)
                {
                    throw new Exception("Invalid RFP Id, PR Id.");
                }
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqid);
                sd.Add("P_PR_ID", prid);
                CORE.DataNamesMapper<PRRFQCreator> mapper = new CORE.DataNamesMapper<PRRFQCreator>();
                var dataset = sqlHelper.SelectList("pr_LinkToRFP", sd);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = dataset.Tables[0].Rows[0][2] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][2].ToString()) : string.Empty;
                }


            }
            catch(Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveCompanyRFQCreators(List<PRRFQCreator> listPRRFQCreator, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);

                foreach (PRRFQCreator User in listPRRFQCreator)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                    sd.Add("P_U_ID", User.U_ID);
                    sd.Add("P_PR_ID", User.PR_ID);
                    sd.Add("P_IS_ASSIGNED", User.IS_ASSIGNED);

                    CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                    var dataset = sqlHelper.SelectList("pr_SaveCompanyRFQCreators", sd);
                    // details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
                }


            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        private Response SavePRItems(PRItems pritems, string sessionid)
        {
            Response details = new Response();
            return details;
        }

        private Response GetWFStatusAndApproverNamePrivate(int P_WF_ID, int P_MODULE_ID, string P_WF_MODULE, List<PRWorkflowTrack> ListWT)
        {
            Response response = new Response();

            foreach (PRWorkflowTrack wt in ListWT)
            {
                if (wt.WF_ID == P_WF_ID && wt.MODULE_ID == P_MODULE_ID && wt.WF_STATUS == "HOLD")
                {
                    response.ErrorMessage = "HOLD";
                    response.Message = wt.APPROVER_NAME;
                }
                else if (wt.WF_ID == P_WF_ID && wt.MODULE_ID == P_MODULE_ID && wt.WF_STATUS == "REJECTED")
                {
                    response.ErrorMessage = "REJECTED";
                    response.Message = wt.APPROVER_NAME;
                }
                else if (wt.WF_ID == P_WF_ID && wt.MODULE_ID == P_MODULE_ID && wt.WF_STATUS == "PENDING")
                {
                    response.ErrorMessage = "PENDING";
                    response.Message = wt.APPROVER_NAME;
                    return response;
                }
                else if (wt.WF_ID == P_WF_ID && wt.MODULE_ID == P_MODULE_ID)
                {
                    response.ErrorMessage = "APPROVED";
                    response.Message = wt.APPROVER_NAME;
                }
            }
            return response;
        }

        private Response generatePdf(PRDetails prdetails, string sessionid)
        {
            Response genPdf = new Response();
            try
            {
                //long nowTicks = DateTime.Now.Ticks;
                //int margin = 16;
                //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateItemizedPO(vendorpo, req, vendorreq, customer, vendor), PdfSharp.PageSize.A4, margin);
                //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf"));
                //fileName = "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                //Response res = SaveAttachment(fileName);
                //fileName = res.ObjectID.ToString();
                //vendorpo.POLink = fileName;
            }
            catch (Exception exc)
            {
                logger.Error(exc, "Error in gen pdf");
            }

            return genPdf;
        }

        private string GenerateItemizedPO(Requirement req, List<POVendor> poVendors, VendorDetails vendor, string sessionID)
        {
            PRMServices prm = new PRMServices();
            UserDetails customer = prm.GetUserDetails(req.CustomerID, sessionID);
            UserDetails vendorObj = prm.GetUserDetails(vendor.VendorID, sessionID);
            string itemRows = string.Empty;
            double tax = 0;
            double totalPriceRev = 0;
            int[] itemsArray = poVendors.Select(p => p.ItemID).ToArray();
            List<RequirementItems> items = req.ListRequirementItems.Where(i => itemsArray.Contains(i.ItemID)).ToList();
            string POID = string.Empty;
            string Comments = string.Empty;
            foreach (POVendor item in poVendors)
            {
                Requirement reqForVendor = prm.GetRequirementData(req.RequirementID, vendor.VendorID, sessionID);
                RequirementItems currentItem = reqForVendor.ListRequirementItems.Where(it => it.ItemID == item.ItemID).FirstOrDefault();
                tax = vendor.Taxes;
                RequirementItems selectedItem = items.Where(i => i.ItemID == item.ItemID).FirstOrDefault();
                string tableRows = "<tr>";
                tableRows += "<td>" + item.ProductIDorName + "</td>";
                tableRows += "<td>" + selectedItem.ProductNo + "</td>";
                tableRows += "<td>" + selectedItem.ProductDescription + "</td>";
                tableRows += "<td>" + selectedItem.ProductBrand + "</td>";
                tableRows += "<td>" + (item.Price * item.VendorPOQuantity).ToString() + "</td>";
                tableRows += "</tr>";
                totalPriceRev += Convert.ToDouble(item.Price * item.VendorPOQuantity);
                itemRows += tableRows;
                POID += item.POID;
                Comments += item.Comments + "<br/>";
            }

            Requirement reqVendor = prm.GetRequirementData(req.RequirementID, vendorObj.UserID, sessionID);
            string taxRows = string.Empty;
            double totalPrice = totalPriceRev;
            foreach (RequirementTaxes taxItem in reqVendor.ListRequirementTaxes)
            {
                string tableRows = "<tr>";
                tableRows += "<td colspan=4>" + taxItem.TaxName + "</td>";
                tableRows += "<td>" + taxItem.TaxPercentage + "%</td>";
                tableRows += "</tr>";
                taxRows += tableRows;
                totalPrice += (totalPriceRev * taxItem.TaxPercentage) / 100;
            }
            totalPrice += 0;//vendor.RevVendorFreight;

            Credentials tinCred = prm.GetUserCredentials(req.CustomerID, sessionID).FirstOrDefault(v => v.FileType == "TIN");

            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");

            //DateTime dateTimeObj = new DateTime();
            //if (req.DeliveryTime != null)
            //{
            //    dateTimeObj = (DateTime)req.DeliveryTime;
            //}

            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "ItemizedPOText.html")),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString(),
                customer.Address.ToString(),
                customer.PhoneNum.ToString(),
                customer.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                POID.ToString(),
                vendorObj.CompanyName.ToString(), // 7
                vendorObj.Address.ToString(),
                vendorObj.PhoneNum.ToString(),
                vendorObj.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                req.Title.ToString(),
                itemRows,
                totalPriceRev.ToString(),
                totalPriceRev.ToString(),
                tax,
                totalPrice,
                !string.IsNullOrEmpty(vendorObj.Address) ? customer.FirstName + " " + customer.LastName + " - " + vendorObj.Address.ToString() : customer.FirstName + " " + customer.LastName + " - " + customer.Address.ToString(),
                poVendors[0].DeliveryAddress.ToString(),
                //dateTimeObj != null ? dateTimeObj.ToShortDateString() : "No Delivery Date Specified by the Customer",
                req.DeliveryTime != "" ? req.DeliveryTime.ToString() : "No Delivery Date Specified by the Customer",
                "",
                tinCred.CredentialID,
                customer.FirstName.ToString() + " " + customer.LastName.ToString(),
                vendorObj.FirstName.ToString() + " " + vendorObj.LastName.ToString(), // 24
                string.IsNullOrEmpty(Comments) ? "" : Comments,
                taxRows, 0
                );
            return html1;
        }
        #endregion Services

    }

}