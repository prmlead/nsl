﻿angular.module('prmApp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('template/pagination/pagination.html',
      "<ul class=\"pagination\">"+
      "<li ng-if=\"boundaryLinks\" ng-class=\"{disabled: noPrevious()}\">" +
      "<a href ng-click=\"selectPage(1, $event)\"><i class=\"fas fa-angle-left\"></i><i class=\"fas fa-angle-left\"></i>" +
      "</i></i>" +
      "</a>" +
      "</li>" +
      "<li ng-if=\"directionLinks\" ng-class=\"{disabled: noPrevious()}\">" +
      "<a href ng-click=\"selectPage(page - 1, $event)\"><i class=\"fas fa-angle-left\"></i>" +
      "</a>" +
      "</li>" +
      "<li ng-repeat=\"page in pages track by $index\" ng-class=\"{active: page.active}\">" +
      "<a href ng-click=\"selectPage(page.number, $event)\">{{page.text}}</a></li><li ng-if=\"directionLinks\" ng-class=\"{disabled: noNext()}\"><a href ng-click=\"selectPage(page + 1, $event)\"><i class=\"fas fa-angle-right\"></i></a></li><li ng-if=\"boundaryLinks\" ng-class=\"{disabled: noNext()}\"><a href ng-click=\"selectPage(totalPages, $event)\"><i class=\"fas fa-angle-right\"></i><i class=\"fas fa-angle-right\"></i></i></i></a></li></ul>"
  );

  $templateCache.put('template/profile-menu.html',
      "<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a data-ui-sref=\"pages.profile.profile-about\">About</a></li>" +
      "<li ng-if=\"showIsUserVendor()\" class=\"btn-wave\" data-ui-sref-active=\"active\"><a data- ui-sref=\"pages.profile.prof-info\">Professional Info</a></li>" +
      "<li ng-show=\"showAddNewReq()\" class=\"btn-wave\" data-ui-sref-active=\"active\"><a data-ui-sref=\"pages.profile.profile-timeline\">My Requirements</a></li>" +
      "<li ng-show=\"showAddNewReq()\" class=\"btn-wave\" data-ui-sref-active=\"active\"><a data-ui-sref=\"pages.profile.profile-settings\">Settings</a></li>" +
      "<li class=\"btn-wave\" ng-if=\"showIsUserVendor()\" data-ui-sref-active=\"active\"><a  data-ui-sref=\"pages.profile.profile-timeline_1\">Leads</a></li>" +
      //"<li class=\"btn-wave\" ng-if=\"showIsUserVendor()\" data-ui-sref-active=\"active\"> <a ng-click='showRequirementVendorMenu = !showRequirementVendorMenu' class=\"waves-effect\"><i class=\"fa fa-plus-square\"></i> <span class=\"hide-menu\"> Leads  <span class=\"fa arrow\"></span> </span></a>" +
      //      "<ul class=\"nav nav-second-level\" ng-if='showRequirementVendorMenu'>" +
      //          "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pages.profile.profile-timeline_1\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Reverse Auction</span></a> </li>" +
      //          "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/req-Fwd-Vendor-List\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Forward Auction</span></a> </li>" +
      //      "</ul>" +
      //"</li>" +
      "<li class=\"btn-wave\" ng-if=\"showIsSuperUser() && showAddNewReq()\" data-ui-sref-active=\"active\"><a  data-ui-sref=\"pages.profile.profile-timeline_SubUsers\">Sub-Users</a>" +
      "<li class=\"btn-wave\" ng-show=\"showAddNewReq()\" data-ui-sref-active=\"active\"><a  data-ui-sref=\"pages.profile.new-vendor\">My Vendors</a></li>" +
      "<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a  data-ui-sref=\"pages.profile.profile-password-management\">Password Mgmt</a></li>" +
      //"<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a  data-ui-sref=\"stores\">Store</a></li>" +
      //"<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a  data-ui-sref=\"managetecheval\">Tech Eval</a></li>" +
      //"<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a ng-show=\"showIsUserVendor() && isCatalogueEnabled()\"  data-ui-sref=\"pages.profile.vendor-catalog\">Catalogue</a></li>" +
      "<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a ng-show=\"showAddNewReq() && showIsSuperUser()\"  data-ui-sref=\"pages.profile.companyDepartments\">Departments</a></li>" +
      "<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a ng-show=\"showAddNewReq() && showIsSuperUser()\"  data-ui-sref=\"pages.profile.companyDesignations\">Designations</a></li>" +
      "<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a ng-show=\"showAddNewReq() && showIsSuperUser()\"  data-ui-sref=\"pages.profile.configuration\">Configurations</a></li>" +
      "<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a ng-show=\"showAddNewReq() && showIsSuperUser()\"  data-ui-sref=\"pages.profile.currencySettings\">Currency</a></li>"
   //   "<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a data-ui-sref=\"catalogMgmt\">Product Category</a></li>" +
   //data-ui-sref=\"companyDepartments\"
   //data-ui-sref=\"companyDesignations\"
   //"<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a data-ui-sref=\"Attribute\">Product Attribute</a></li>"
    );

    $templateCache.put('template/catalogProfile-menu.html',
   "<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a data-ui-sref=\"products\"><strong>PRODUCTS</strong></a></li>"+
   "<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a data-ui-sref=\"catalogMgmt\"><strong>Categories</strong></a></li>"   
   //"<li class=\"btn-wave\" data-ui-sref-active=\"active\"><a data-ui-sref=\"Attribute\"><strong>Attributes</strong></a></li>"   
);




  $templateCache.put('template/header.html',
      "<nav class=\"navbar navbar-default navbar-static-top m-b-0\">" +
      "<div class=\"navbar-header\">" +
      "<div class=\"row\">" +
      "<div class=\"col-md-3\">" +
      "<div class=\"top-left-part hidden-xs\">" +
      "<a class=\"logo\"> <b> <img src=\"js/resources/images/client_logo.png\" alt=\"home\" class=\"dark-logo\" style=\"width:100px;height:44px;\"  />" +
      "<img src=\"js/resources/images/client_logo.png\" alt=\"home\" class=\"light-logo\" /> </b> " +
      "</a>" +
      "</div>" +
      "</div>" +
      "<div class=\"col-md-3\" style =\"text-align:center;margin-top: 20px;margin-left:200px;border: 2px solid white;\">" +
      "<a href=\"https://support.prm360.com/support/home\" style =\"color:#fff;font-size: 14px;\" target=\"_blank\">" +
      "Click here to get help" +
      "</a>" +
      "</div>" +
      "<div data-ng-include=\"'views/toDoCalendar.html'\" style=\"position: fixed;right: 0;width:5% \" class=\" hidden-xs \" ></div>" +

      "<div class=\"pull-right\">" +
      "<div class=\"dropdown-primary dropdown\">" +
      "<div class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"true\">" +
      "<div class=\"pull-right\" style=\"width: 3rem;height: 3rem;border-radius: 50%;background: #fff;color: #000;text-align: center;margin: 8px 20px;cursor:pointer;\"><span style=\"font-size: 1.5rem;position: relative;line-height: 1;top: 0.75rem;\">{{currentUserDisplayNameLetter()}}</span></div>" +

      "</div>" +
      "<ul class=\"dropdown-menu dropdown-menu-right\" data-dropdown-in=\"fadeIn\" data-dropdown-out=\"fadeOut\" style=\"margin-top:50px\">" +
      "<li>" +
      "<a>" +
      "<div style=\"width: 3rem;height: 3rem;border-radius: 50%;background: #115980;color: #fff;text-align: center;margin: 5px 75px;\"><span style=\"font-size: 1.5rem;position: relative;line-height: 1;top: 0.75rem;\">{{currentUserDisplayNameLetter()}}</span></div>" +
      "<h4 style=\"text-align:center;color:#115980\">{{currentUserDisplayName()}}</h4>" +
      "<h5 style=\"text-align:center;color:#d93262\">{{currentUserDesignation()}}</h5>" +
      "<h6 style=\"text-align:center;\">{{currentUserEmail()}}</h5>" +
      "</a>" +
      "</li>" +
      "<li>" +
      "<a href=\"#/pages/profile/profile-about\">" +
      "<i class=\"fa fa-user\" style=\"color:#115980\"></i>&nbsp&nbsp&nbsp<span>Accounts Management</span>" +
      "</a>" +
      "</li>" +
      "<li>" +
      "<a href=\"#/pages/profile/profile-password-management\">" +
      "<i class=\"fa fa-lock\" style=\"color:#115980\"></i>&nbsp&nbsp&nbsp<span>Change Password</span>" +
      "</a>" +
      "</li>" +
      "<li>" +
      "<a role=\"button\" data-target=\"#feedbackModal2\" data-toggle=\"modal\" ng-show=\"!showIsUserVendor()\">" +
      "<i class=\"fa fa-question-circle\" style=\"color:#115980\"></i>&nbsp&nbsp&nbsp<span>Feedback</span>" +
      "</a>" +
      "</li>" +
      "<li>" +
      "<a role=\"button\" ng-click=\"logout()\">" +
      "<i class=\"fa fa-sign-out-alt\" style=\"color:#115980\"></i>&nbsp&nbsp&nbsp<span>Sign Out</span>" +
      "</a>" +
      "</li>" +
      "</ul>" +
      "</div>" +
      "</div>" +

      
      //"<div class=\"col-md-1 m-t-20 pull-right hidden-sm\" style=\"text-align:right;\">  <a ng-click=\"logout()\" style=\"color:#fff;\"> <i class=\"fa fa-sign-out-alt\" ></i> </a> </div>" +
      //"<div class=\"col-md-1 m-t-20 pull-right hiddden-sm\" style=\"text-align:right;\">  <a  href=\"#/communication\" style=\"color:#fff;\"> <i class=\"fa fa-paper-plane\" ></i> </a> </div>" +
      //"<div class=\"col-md-1 pull-right\" style=\"width: 3rem;height: 3rem;border-radius: 50%;background: #fff;color: #000;text-align: center;margin: 5px 0;\"><span style=\"font-size: 1.5rem;position: relative;line-height: 1;top: 0.75rem;right:0.5rem;\">DC</span></div>" +
      //"<div class=\"col-md-3 m-t-20 pull-right hidden-sm\" style=\"color:#fff;text-align:right;\"><strong>Welcome: {{currentUserDisplayName()}}</strong>&nbsp&nbsp<a href=\"#/pages/profile/profile-about\" title=\"My Accounts\" style=\"color:#fff;\"> <i class=\"fa fa-user\" ></i></a>&nbsp&nbsp" +
      //"<a role=\"button\" ng-show=\"!showIsUserVendor()\" href =\"#/communication\" title=\"Communication\" style=\"color:#fff;\"> <i class=\"fa fa-paper-plane\" ></i> </a>&nbsp&nbsp" +
      //"<a role=\"button\" data-target=\"#ReqSupportPopup\" title=\"Request Support\" style=\"color:#fff;\" data-toggle=\"modal\"> <i class=\"fa fa-headphones\" ></i> </a>&nbsp&nbsp " +
      //"<a role=\"button\" ng-click=\"logout()\" title=\"Sign Out\" style=\"color:#fff;\"> <i class=\"fa fa-sign-out-alt\" ></i> </a> </div>" +

      "<div class=\"col-md-3 m-t-20 pull-right\"  style=\"text-align:right;\"><a role=\"button\" ng-show=\"!showIsUserVendor()\" href =\"#/communication\" title=\"Communication\" style=\"color:#fff;\"> <i class=\"fa fa-paper-plane\" ></i> </a>&nbsp&nbsp" +
      "<a role=\"button\" data-target=\"#ReqSupportPopup\" title=\"Raise Support Request\" style=\"color:#fff;\" data-toggle=\"modal\"> <i class=\"fa fa-headphones\" ></i> </a>&nbsp&nbsp " +
      //"<a role=\"button\" data-target=\"#feedbackModal1\" title=\"Create Request\" style=\"color:#fff;\" data-toggle=\"modal\"> <i class=\"fa fa-ticket-alt\" ></i> </a></div> " +

      //"<div class=\"col-md-3 m-t-20 pull-right hidden-sm\" style=\"color:#fff;text-align:right;\"><strong>Welcome: {{currentUserDisplayName()}}</strong>&nbsp&nbsp<a href=\"#/pages/profile/profile-about\" title=\"My Accounts\" style=\"color:#fff;\"> <i class=\"fa fa-user\" ></i></a>&nbsp&nbsp" +
      //"<a role=\"button\" ng-show=\"!showIsUserVendor()\" href =\"#/communication\" title=\"Communication\" style=\"color:#fff;\"> <i class=\"fa fa-paper-plane\" ></i> </a>&nbsp&nbsp" +
      //"<a role=\"button\" data-target=\"#ReqSupportPopup\" title=\"Request Support\" style=\"color:#fff;\" data-toggle=\"modal\"> <i class=\"fa fa-headphones\" ></i> </a>&nbsp&nbsp " +
      //"<a role=\"button\" ng-click=\"logout()\" title=\"Sign Out\" style=\"color:#fff;\"> <i class=\"fa fa-sign-out-alt\" ></i> </a> </div>" +
      "<ul class=\"nav navbar-top-links navbar-right pull-right\">" +
      "<li ng-if=\"false\" ng-click=\"showUserNotifications()\" class=\"dropdown\"> <a class=\"dropdown-toggle\" role=\"button\" data-target=\"#UserNotificationDetails\" data-toggle=\"modal\"> <span style=\"font-size:18px; margin-right:10px;\"> Alerts</span> <i class=\"mdi mdi-check-circle\"></i><div class=\"notify\"><span class=\"heartbit\"></span><span class=\"point\"></span></div> </a> </li>" +
      "</ul>" +
      "<ul class=\"nav navbar-top-links navbar-left\">" +
      "<li> <a href=\"javascript:void(0)\" class=\"open-close waves-effect waves-light visible-xs\"><i class=\"ti-close ti-menu fa fa-bars\"></i></a></li></ul>  " +
      "<ul class=\"nav navbar-top-links navbar-right pull-right\">" +
      //"<li class=\"dropdown\"> <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\"> <span style=\"font-size:11px; margin-right:10px;\"> To-do</span> <i class=\"mdi mdi-check-circle\"></i><div class=\"notify\"><span class=\"heartbit\"></span><span class=\"point\"></span></div> </a> </li>" +
      //"<li class=\"dropdown\"> <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\"> Department Name<span class=\"caret\"></span> </a> </li>" +
      "</ul>" +
      //"<div data-ng-include=\"'views/toDoCalendar.html'\" style=\"position: fixed; top: 0; z - index: 9999; right: 0; width:100%; class=\" hidden-xs \" \"></div>"+
      "</div>" +
      "</nav>" +
      "<div class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" id=\"UserNotificationDetails\"> <div class=\"modal-dialog\" role=\"document\">   <div class=\"modal-content\">   <div class=\"modal-header\">   <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">X</span></button>   <h4 class=\"modal-title\">Notifications </h4>   </div>   <div class=\"modal-body\" style=\"max-height:400px;overflow-y:auto;\">    <div ng-repeat=\"notification in userNotifications\" style=\"padding-bottom:5px;margin:10px 0px 10px 0px;border-bottom:1px solid\" ng-bind-html=\"notification.displayMessage\"></div>   </div>   <div class=\"modal-footer\"> <div class='pull-left'>Total count: {{notificationsCount}}</div>  <button ng-show='false' type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button> </div>   </div> </div> </div>"+
      "<div class=\"modal\" id=\"feedbackModal1\" tabindex=\"-1\">" +
      "<div class=\"modal-dialog\" style=\"width: 60%;height:50%\">" +
      "<div class=\"modal-content\">" +
      "<iframe title=\"Feedback Form\" class=\"freshwidget-embedded-form\" id=\"freshwidget-embedded-form\" src=\"https://prm360help.freshdesk.com/widgets/feedback_widget/new?&widgetType=embedded&screenshot=No\" scrolling=\"no\" height=\"750\" width=\"950\" frameborder=\"0\" >" +
      "</iframe>" +
      "</div>" +
      "</div>" +
      "</div>" +

      "<div class=\"modal\" id=\"feedbackModal2\" tabindex=\"-1\">" +
      "<div class=\"modal-dialog\" style=\"width: 50%;height:50%\">" +
      "<div class=\"modal-content\">" +
      "<iframe src=\"https://docs.google.com/forms/d/e/1FAIpQLSfABfUjmQJopqohXZs4_sIiFCWAfamECLk14K5rhmZTA-VL1g/viewform?embedded=true\" width=\"800\" height=\"745\" frameborder=\"0\" marginheight=\"0\" marginwidth=\"0\">Loading�</iframe>" +
      "</div>" +
      "</div>" +
      "</div>" 

  );


$templateCache.put('template/sidebar-left.html',
    "<div class=\"navbar-default sidebar\" role=\"navigation\" data-ng-controller\"headerCtrl\">" +
    "<div class=\"sidebar-nav slimscrollsidebar\" >" +
    //"<div class=\"sidebar-head\" >" +
    //"<h3><span class=\"fa-fw open-close\" >" +
    //"<i class=\"ti-menu fa fa-bars hidden-xs\" ></i>" +
    //"<i class=\"ti-close fa fa-times visible-xs\" ></i>" +
    //"</span> <span class=\"hide-menu\" >Menu</span></h3> " +
    //"</div>" +
    "<ul class=\"nav\" id=\"side-menu\" >    " +
    "<li> <a href=\"#home\" class=\"waves-effect\" > <i class=\"fa fa-tachometer-alt\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Dashboard</span> </a> </li> " +
    "<li> <a href=\"#/pages/profile/profile-about\" class=\"waves-effect\" data-ui-sref-active=\"active\"> <i class=\"fa fa-user\" ></i> <span class=\"hide-menu\" >My Accounts</span> </a> </li>" +
    //"<li ng-show=\"showAddNewReq() && isUserEntitled(591164)\"> <a href=\"#/workflow-pending-approvals\" class=\"waves-effect\" > <i class=\"fa fa-tachometer-alt\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Pending Approvals</span> </a> </li>" +
    "<li ng-show=\"showAddNewReq() && isUserEntitled(591164)\"> <a ng-click='showPendingApproval = !showPendingApproval' class=\"waves-effect\"><i class=\"fa fa-tasks\"></i> <span class=\"hide-menu\"> Approvals <span class=\"fa arrow\"></span> </span></a>" +
        "<ul class=\"nav nav-second-level\" ng-if='showPendingApproval'>" +
            "<li style=\"margin-left: 20px;\" ng-show=\"showAddNewReq() && isUserEntitled(591164)\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/workflow-pending-approvals\"><i class=\"fa fa-tachometer-alt\"></i>&nbsp;&nbsp;<span class=\"hide-menu\">Pending Approvals</span></a> </li>" +
            "<li style=\"margin-left: 20px;\" ng-show=\"showAddNewReq() && isUserEntitled(591164)\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/approval-qcs-list\"><i class=\"fa fa-history\"></i>&nbsp;&nbsp;<span class=\"hide-menu\">Approval History</span></a> </li>" +
        "</ul>" +
    "</li>" +
    "<li ng-show=\"showAddNewReq()\"> <a href=\"#/list-pr\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-plus-square\" ></i> <span class=\"hide-menu\" >PR Management</span> </a> </li>" +
    //"<li ng-show=\"showAddNewReq()\"> <a href=\"#/list-po\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-plus-square\" ></i> <span class=\"hide-menu\" >PO Management</span> </a> </li>" +
    "<li> <a href=\"#/list-GRN\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-plus-square\" ></i> <span class=\"hide-menu\" >GRN Management</span> </a> </li>" +
    "<li> <a href=\"#/list-pendingPO\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-plus-square\" ></i> <span class=\"hide-menu\" >Pending PO Management</span> </a> </li>" +
    //"<li> <a href=\"#/vendorInvoices\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-file-alt\" ></i> <span class=\"hide-menu\" >Vendor Invoices</span> </a> </li>" +
    "<li ng-show=\"showAddNewReq()\"> <a href=\"#/paymentTracking\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-plus-square\" ></i> <span class=\"hide-menu\" >Payment Track</span> </a> </li>" +
    //"<li ng-show=\"showAddNewReq() && !isCatalogueEnabled() && isUserEntitled(582258)\"> <a href=\"#/save-requirement/\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-plus-square\" ></i> <span class=\"hide-menu\" >Create Requirement !C</span> </a> </li>" +
    //"<li ng-show=\"showAddNewReq() && isCatalogueEnabled() && isUserEntitled(582258)\"> <a href=\"#/save-requirementAdv/\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-plus-square\" ></i> <span class=\"hide-menu\" >Create Requirement</span> </a> </li>" +
    "<li ng-show=\"showAddNewReq() && isCatalogueEnabled() && isUserEntitled(582258)\"> <a href=\"#/save-requirementAdv1/\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-plus-square\" ></i> <span class=\"hide-menu\" >Create RFP</span> </a> </li>" +
   // "<li ng-show=\"showAddNewReq() && isCatalogueEnabled() && isUserEntitled(582258)\"> <a ng-click='showCreateRFQ = !showCreateRFQ' class=\"waves-effect\"><i class=\"fa fa-plus-square\"></i><span class=\"hide-menu\">Create Requirement <span class=\"fa arrow\"></span> </span></a>" +
   // "<ul class=\"nav nav-second-level\" ng-if='showCreateRFQ'>" +
   //"<li style=\"margin-left: 20px;\" ng-show=\"showAddNewReq() && isCatalogueEnabled() && isUserEntitled(582258)\"> <a  href=\"#/save-requirementAdv/\"><i class=\"fa fa-plus-square\"></i>&nbsp<span class=\"hide-menu\"> Create RFQ  </span></a>" +
   // "<li style=\"margin-left: 20px;\" ng-show=\"showAddNewReq() && isCatalogueEnabled() && isUserEntitled(582258)\"> <a  href=\"#/save-requirementAdv1/\"><i class=\"fa fa-plus-square\"></i>&nbsp<span class=\"hide-menu\"> Create RFP  </span></a>" +
   // "</ul>" +
   // "</li>" +

    //"<li ng-show=\"showAddNewReq()\"> <a ng-click='showRequirementMenu = !showRequirementMenu' class=\"waves-effect\"><i class=\"fa fa-plus-square\"></i> <span class=\"hide-menu\"> Create Req <span class=\"fa arrow\"></span> </span></a>" +
    //    "<ul class=\"nav nav-second-level\" ng-if='showRequirementMenu'>" +
    //        "<li ng-show=\"showAddNewReq() && isCatalogueEnabled() && isUserEntitled(582258)\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/save-requirementAdv/\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Reverse Auction</span></a> </li>" +
    //        //"<li ng-show=\"showAddNewReq() && isCatalogueEnabled() && isUserEntitled(582258)\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/save-tender/\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Tender Auction</span></a> </li>" +
    //    "</ul>"+
    //"</li>" +


    //"<li ng-show=\"showAddNewReq()\"> <a href=\"#/form/addnewlogistic/0\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-plus-square\" ></i> <span class=\"hide-menu\" >Create Logistics</span> </a> </li>" +
    "<li ng-show=\"showAddNewReq()\"> <a href=\"#products\" class=\"waves-effect\"> <i class=\"fa fa-upload\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Catalogue</span> </a> </li> " +
    //"<li> <a class=\"waves-effect\"><i class=\"fa fa-user\"></i> <span class=\"hide-menu\"> Profile <span class=\"fa arrow\"></span> </span></a>" +
    //    "<ul class=\"nav nav-second-level\">" +
    //        "<li> <a href=\"#\"><i class=\"fa-fw\">1</i><span class=\"hide-menu\">Profile</span></a> </li>" +
    //        "<li> <a href=\"#\"><i class=\"fa-fw\">2</i><span class=\"hide-menu\">Profile</span></a> </li>" +
    //        "<li> <a href=\"#\"><i class=\" fa-fw\">3</i><span class=\"hide-menu\">Profile</span></a> </li>" +
    //    "</ul>" +
    //"</li>" +
     //"<li ng-show=\"showAddNewReq()\"> <a href=\"#/products\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-folder\" ></i> <span class=\"hide-menu\" >Catalog</span> </a> </li>" +
   // "<li ng-show=\"showAddNewReq()\"> <a href=\"#/pages/profile/profile-timeline\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-file-alt\" ></i> <span class=\"hide-menu\" >Requirements</span> </a> </li>" +

    "<li ng-show=\"showAddNewReq() \"> <a  class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pages/profile/profile-timeline\"><i class=\"fa fa-file-alt\"></i> <span class=\"hide-menu\"> Requirements </span></a>" +
   // "<li> <a  class=\"waves-effect\"  data-ui-sref-active=\"active\" href=\"#/req-lot-list\"><i class=\"fa fa-file-alt\"></i> <span class=\"hide-menu\"> Lot Requirements </span></a>" +
    //"<li> <a ng-click='showPOMenu = !showPOMenu' class=\"waves-effect\"><i class=\"fa fa-file-alt\"></i> <span class=\"hide-menu\"> PO's <span class=\"fa arrow\"></span> </span></a>" +
    //    "<ul class=\"nav nav-second-level\" ng-if='showPOMenu'>" +
    //        "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pendingpos\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Pending PO's</span></a> </li>" +
    //        "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pendingasn\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Pending ASN's</span></a> </li>" +
    //"<li ng-show=\"showAddNewReq() \"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pendingcontracts\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Pending Contracts</span></a> </li>" +
    //"<li ng-show=\"showAddNewReq() \"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/matchPrices\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">3 Way Match</span></a> </li>" +
    //        "<li ng-show=\"showAddNewReq() \"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/invoiceaging\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Invoice Aging</span></a> </li>" +
    //        "<li ng-show=\"showAddNewReq() && isCatalogueEnabled() && isUserEntitled(582258)\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/save-tender/\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Tender Auction</span></a> </li>" +
    //    "</ul>"+
    //"</li>" +

    "<li ng-show=\" showIsSuperUser() && showAddNewReq()\"> <a ng-click='showPRPOSetup = !showPRPOSetup' class=\"waves-effect\"><i class=\"fa fa-bars\"></i> <span class=\"hide-menu\">Configuration <span class=\"fa arrow\"></span> </span></a>" +
    "<ul class=\"nav nav-second-level\" ng-if='showPRPOSetup'>" +
    "<li ng-show=\"showAddNewReq() && isUserEntitled(591160)\"> <a href=\"#/workflows\" class=\"waves-effect\" > <i class=\"fa-fw\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Workflows</span> </a> </li>" +
    "<li ng-show=\"showAddNewReq()\"> <a data-toggle=\"modal\" class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/templates\"> <i class=\"fa-fw\" ></i> <span class=\"hide-menu\" >Templates</span> </a> </li>" +
    //"<li ng-show=\"showAddNewReq()\"> <a href=\"#sapReportsOpenPO\" class=\"waves-effect\" > <i class=\"fa-fw\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >File Upload</span> </a> </li> " +
    "</ul>" +
    "</li>"+
    //"<li ng-show=\"showAddNewReq()\"> <a ng-click='showRequirementListMenu = !showRequirementListMenu' class=\"waves-effect\"><i class=\"fa fa-file-alt\"></i> <span class=\"hide-menu\"> Requirements  <span class=\"fa arrow\"></span> </span></a>" +
    //    "<ul class=\"nav nav-second-level\" ng-if='showRequirementListMenu'>" +
    //        "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pages/profile/profile-timeline\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Reverse Auction</span></a> </li>" +
    //        //"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/view-tenders\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Tender Auction</span></a> </li>" +
    //       // "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/req-fwd-List\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Forward Auction</span></a> </li>" +
    //        //"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/req-lot-list\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Lot Requirements</span></a> </li>" +
    //    "</ul>" +
    //"</li>" +

    "<li ng-show=\"!showAddNewReq()\"> <a href=\"#/pages/profile/profile-timeline_1\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-file-alt\" ></i> <span class=\"hide-menu\" >Leads</span> </a> </li>" +
    //"<li ng-show=\"!showAddNewReq()\"> <a target=\"_blank\" href=\"https://stg.cashflo.io/#/vendor/37u5s06c/create?sso=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MTQ0MDUwODAsIm9yZ0tleSI6IjM3dTVzMDZjIiwibWVtYmVyS2V5IjoibHd3YWJvNHUiLCJ0cm1zQWNjcHRkIjp0cnVlfQ.6vd5wrqnsPkTEwdYeU9T00-E1_TPBzd1cvoVkJ71fQg\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-file-alt\" ></i> <span class=\"hide-menu\" >Cash Flow</span> </a> </li>" +

    //"<li ng-show=\"!showAddNewReq()\"> <a ng-click='showRequirementVendorMenu = !showRequirementVendorMenu' class=\"waves-effect\"><i class=\"fa fa-file-alt\"></i> <span class=\"hide-menu\"> Leads  <span class=\"fa arrow\"></span> </span></a>" +
    //       "<ul class=\"nav nav-second-level\" ng-if='showRequirementVendorMenu'>" +
    //            "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pages/profile/profile-timeline_1\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Reverse Auction</span></a> </li>" +
    //           // "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/req-Fwd-Vendor-List\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Forward Auction</span></a> </li>" +
    //        "</ul>" +
    //"</li>" +

   // "<li> <a ng-show=\"!showIsUserVendor()\" ng-click='showCatalogReportsMenu = !showCatalogReportsMenu' class=\"waves-effect\"><i class=\"fa fa-file-alt\"></i> <span class=\"hide-menu\"> Reports  <span class=\"fa arrow\"></span> </span></a>" +
       // "<ul class=\"nav nav-second-level\" ng-if='showCatalogReportsMenu'>" +
           // "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/CatalogCompleteView\"><i class=\"fa-fw\"></i><span class=\"hide-menu\" style=\"color:#fff\">Catalog Report</span></a> </li>" +
       // "</ul>" +
       // "<ul class=\"nav nav-second-level\" ng-if='showCatalogReportsMenu'>" +
           // "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/materialReport\"><i class=\"fa-fw\"></i><span class=\"hide-menu\" style=\"color:#fff\">Product Reports</span></a> </li>" +
       // "</ul>" +
   // "</li>" +

    //"<li ng-show=\"showAddNewReq() && isUserEntitled(591160)\"> <a href=\"#/workflows\" class=\"waves-effect\" > <i class=\"fa fa-tachometer-alt\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >workflows</span> </a> </li>" +
    //"<li ng-show=\"showAddNewReq()\"> <a href=\"#sapReports\" class=\"waves-effect\" > <i class=\"fa fa-tachometer-alt\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Open PR</span> </a> </li> " +
    //"<li ng-show=\"showAddNewReq()\"> <a href=\"#sapHome\" class=\"waves-effect\" > <i class=\"fa fa-tachometer-alt\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Open PO</span> </a> </li> " +
    //"<li ng-show=\"showAddNewReq()\"> <a href=\"#sapReportsOpenPO\" class=\"waves-effect\" > <i class=\"fa fa-upload\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >File Upload</span> </a> </li> " +
   // "<li ng-show=\"showAddNewReq()\"> <a href=\"#sapShortagePRReports\" class=\"waves-effect\" > <i class=\"fa fa-table\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" > PR Report </span> </a> </li>" +
   // "<li ng-show=\"showAddNewReq()\"> <a href=\"#sapShortagePOReports\" class=\"waves-effect\" > <i class=\"fa fa-table\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" > PO Report </span> </a> </li>" +
    "<li> <a ng-show=\"showIsSuperUser() && showAddNewReq()\" ng-click='showLogsMenu = !showLogsMenu' class=\"waves-effect\"><i class=\"fa fa-file-alt\"></i> <span class=\"hide-menu\"> Logs  <span class=\"fa arrow\"></span> </span></a>" +
    "<ul class=\"nav nav-second-level\" ng-if='showLogsMenu'>" +
    "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/activeUsers\"><i class=\"fa-fw\"></i><span class=\"hide-menu\" style=\"color:#fff\">Active Users</span></a> </li>" +
    "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/emailLogs\"><i class=\"fa-fw\"></i><span class=\"hide-menu\" style=\"color:#fff\">Email Logs</span></a> </li>" +
   // "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/smsLogs\"><i class=\"fa-fw\"></i><span class=\"hide-menu\" style=\"color:#fff\">SMS Logs</span></a> </li>" +
    "</ul>" +
    "</li>" +
    //"<li> <a ng-show=\"showIsSuperUser() && showAddNewReq()\" ng-click='showOpsMenu = !showOpsMenu' class=\"waves-effect\"><i class=\"fa fa-file-alt\"></i> <span class=\"hide-menu\"> Utilization  <span class=\"fa arrow\"></span> </span></a>" +
    //"<ul class=\"nav nav-second-level\" ng-if='showOpsMenu'>" +
    //"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/opsReports\"><i class=\"fa-fw\"></i><span class=\"hide-menu\" style=\"color:#fff\">Reports</span></a> </li>" +
    //"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/opsVendorReqSearch\"><i class=\"fa-fw\"></i><span class=\"hide-menu\" style=\"color:#fff\">Vendor Req Search</span></a> </li>" +
    //"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/opsAsigneeTickets\"><i class=\"fa-fw\"></i><span class=\"hide-menu\" style=\"color:#fff\">Freshdesk Tickets</span></a> </li>" +
    //"</ul>" +
    //"</li>" +
    //"<li ng-show=\"showAddNewReq()\"> <a href=\"#/pages/profile/customerlogisticslist\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-file-alt\" ></i> <span class=\"hide-menu\" >Logistics</span> </a> </li>" +
    //"<li ng-show=\"!showAddNewReq()\"> <a href=\"#/pages/profile/vendorlogisticslist\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-file-alt\" ></i> <span class=\"hide-menu\" >Logistics</span> </a> </li>" +

   // "<li ng-show=\"showAddNewReq()\"> <a href=\"#sapReports\" class=\"waves-effect\" > <i class=\"fa fa-tachometer-alt\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Open PR</span> </a> </li> " +
    //"<li ng-show=\"showAddNewReq()\"> <a href=\"#sapHome\" class=\"waves-effect\" > <i class=\"fa fa-tachometer-alt\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Open PO</span> </a> </li> " +
   // "<li ng-show=\"showAddNewReq()\"> <a href=\"#sapReportsOpenPO\" class=\"waves-effect\" > <i class=\"fa fa-upload\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >File Upload</span> </a> </li> " +
   // "<li ng-show=\"showAddNewReq()\"> <a href=\"#sapShortagePRReports\" class=\"waves-effect\" > <i class=\"fa fa-table\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" > PR Report </span> </a> </li>" +
   // "<li ng-show=\"showAddNewReq()\"> <a href=\"#sapShortagePOReports\" class=\"waves-effect\" > <i class=\"fa fa-table\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" > PO Report </span> </a> </li>" +


    //"<li> <a href=\"#/apmcdashboard\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-file-alt\" ></i> <span class=\"hide-menu\" >APMC</span> </a> </li>" +
    //"<li> <a href=\"#/stores\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-archive\" ></i> <span class=\"hide-menu\" >Store</span> </a> </li>" +
    //"<li> <a href=\"#/managetecheval\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-tasks\" ></i> <span class=\"hide-menu\" >Tech Eval</span> </a> </li>" +
    //"<li> <a href=\"#\" class=\"waves-effect\" > <i class=\"fa fa-check\" ></i> <span class=\"hide-menu\" >Approvals</span> </a> </li>" +
    //"<li ng-show=\"showAddNewReq()\"> <a ng-click='showRealMenu = !showRealMenu' class=\"waves-effect\"><i class=\"fa fa-user\"></i><span class=\"hide-menu\">Real Time Pricing<span class=\"fa arrow\"></span></span></a>" +
    //   "<ul class=\"nav nav-second-level\" ng-if='showRealMenu'>" +
    //        "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pages/realtimeprice/1\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Chemicals and polymers</span></a> </li>" +
    //        "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pages/imports\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Imports</span></a> </li>" +
    //    "</ul>" +
    //"</li>" +

    // "<li> <a ng-click='showRealMenu = !showRealMenu' class=\"waves-effect\"><i class=\"fa fa-user\"></i> <span class=\"hide-menu\">Masters  <span class=\"fa arrow\"></span> </span></a>" +
    //    "<ul class=\"nav nav-second-level\" ng-if='showRealMenu'>" +
    //        "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pages/master/contact_detail\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Contact details</span></a> </li>" +
    //        "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pages/master/delivery_location\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Delivery location</span></a> </li>" +
    //        "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pages/master/delivery_term\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Delivery term</span></a> </li>" +
    //        "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pages/master/payment_term\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Payment term</span></a> </li>" +
    //        "<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/pages/master/general_term\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">General term</span></a> </li>" +

    //    "</ul>" +
    //"</li>" +
    //"<li ng-show=\"showAddNewReq()\"> <a href=\"#/pages/imports\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-upload\" ></i> <span class=\"hide-menu\" >Imports</span> </a> </li>" +
    //"<li ng-show=\"showAddNewReq()\"> <a data-toggle=\"modal\" class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/communication\"> <i class=\"fa fa-paper-plane\" ></i> <span class=\"hide-menu\" >Communication</span> </a> </li>" +
    //"<li ng-show=\"showAddNewReq()\"> <a data-toggle=\"modal\" class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/templates\"> <i class=\"fa fa-clone\" ></i> <span class=\"hide-menu\" >Templates</span> </a> </li>" +
   // "<li ng-show=\"showAddNewReq()\"> <a data-toggle=\"modal\" class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/customfields\"> <i class=\"fa fa-paper-plane\" ></i> <span class=\"hide-menu\" >Custom Fields</span> </a> </li>" +
    "<li ng-show=\"!showAddNewReq()\"> <a data-target=\"#ReqSupportPopup\" data-toggle=\"modal\" class=\"waves-effect\"> <i class=\"fa fa-headphones\" ></i> <span class=\"hide-menu\" >Request Support</span> </a> </li>" +
    "<li ng-show=\"!showAddNewReq()\"> <a class=\"waves-effect\" href=\"js/resources/images/client_document.pdf\"> <i class=\"fa fa-book\" ></i> <span class=\"hide-menu\" >User Manual</span> </a> </li>" +

    //"<li> <a href=\"#/workflows\" class=\"waves-effect\" > <i class=\"fa fa-tachometer-alt\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Workflows</span> </a> </li>" +

    "<li ng-show=\"showAddNewReq() && showIsSuperUser()\"> <a ng-click='showReportMenu = !showReportMenu' class=\"waves-effect\"><i class=\"fa fa-user\"></i> <span class=\"hide-menu\">Reports  <span class=\"fa arrow\"></span> </span></a>" +
        "<ul class=\"nav nav-second-level\" ng-if='showReportMenu'>" +
            "<li ng-show=\"showIsSuperUser() && showAddNewReq()\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/savingsDashBoard\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Savings</span></a> </li>" +
            "<li> <a class=\"waves-effect\"ng-show=\"showIsSuperUser() && showAddNewReq()\" data-ui-sref-active=\"active\" href=\"#/purchaserAnalysis\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Purchaser Analysis</span></a> </li>" +
            "<li> <a class=\"waves-effect\" ng-show=\"showIsSuperUser() && showAddNewReq()\" data-ui-sref-active=\"active\" href=\"#/productAlalysis/0/1\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Product Analysis</span></a> </li>" +
            "<li> <a class=\"waves-effect\" ng-show=\"showIsSuperUser() && showAddNewReq()\" data-ui-sref-active=\"active\" href=\"#/vendorViewProfile/0/1\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Vendor Analysis</span></a> </li>" +
            "<li> <a class=\"waves-effect\" ng-show=\"showIsSuperUser() && showAddNewReq()\" data-ui-sref-active=\"active\" href=\"#/anylaticsReports\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Utilization Report</span></a> </li>" +
            "<li> <a class=\"waves-effect\" ng-show=\"showIsSuperUser() && showAddNewReq()\" data-ui-sref-active=\"active\" href=\"#/openPRReport\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Open PR</span></a> </li>" +
            "<li> <a class=\"waves-effect\" ng-show=\"showIsSuperUser() && showAddNewReq()\" data-ui-sref-active=\"active\" href=\"#/openPOReport\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Open PO</span></a> </li>" +
            //"<li> <a class=\"waves-effect\" ng-show=\"showIsSuperUser() && showAddNewReq()\" data-ui-sref-active=\"active\" href=\"#/grnAnylaticsReport\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">GRN Dashboard</span></a> </li>" +
            //"<li ng-show=\"showIsSuperUser() && showAddNewReq()\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#sapShortagePRReports\"><i class=\"fa-fw\" ></i><span class=\"hide-menu\"> PR Report</span></a> </li>" +
            //"<li ng-show=\"showIsSuperUser() && showAddNewReq()\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#sapShortagePOReports\"><i class=\"fa-fw\" ></i><span class=\"hide-menu\"> PO Report </span></a> </li>" +
            // "<li ng-show=\"showAddNewReq()\"> <a href=\"#sapReports\" class=\"waves-effect\" > <i class=\"fa fa-fw\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Open PR</span> </a> </li> " +
            // "<li ng-show=\"showAddNewReq()\"> <a href=\"#sapHome\" class=\"waves-effect\" > <i class=\"fa fa-fw\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Open PO</span> </a> </li> " +
        "</ul>" +
    "</li>" +

    //  "<li ng-show=\"showAddNewReq()\"> <a ng-click='showReportMenu = !showReportMenu' class=\"waves-effect\"><i class=\"fa fa-user\"></i> <span class=\"hide-menu\">Reports  <span class=\"fa arrow\"></span> </span></a>" +
    //    "<ul class=\"nav nav-second-level\" ng-if='showReportMenu'>" +
    //        "<li ng-show=\"showIsSuperUser() && showAddNewReq()\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/supplier\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Supplier</span></a> </li>" +
    //        "<li ng-show=\"showIsSuperUser() && showAddNewReq()\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/savingsDashBoard\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Savings</span></a> </li>" +
    //        "<li ng-show=\"showIsSuperUser() && showAddNewReq()\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/contractSpend\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Contract Spend</span></a> </li>" +
    //        "<li ng-show=\"showIsSuperUser() && showAddNewReq()\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/averageProcure\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Average Procure</span></a> </li>" +
    //        "<li ng-show=\"showIsSuperUser() && showAddNewReq()\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/purchaseTurnOver\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Purchase Turnover</span></a> </li>" +
    //        "<li ng-show=\"showIsSuperUser() && showAddNewReq()\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#sapShortagePRReports\"><i class=\"fa-fw\" ></i><span class=\"hide-menu\"> PR Report</span></a> </li>" +
    //        "<li ng-show=\"showIsSuperUser() && showAddNewReq()\"> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#sapShortagePOReports\"><i class=\"fa-fw\" ></i><span class=\"hide-menu\"> PO Report </span></a> </li>" +
    //        "<li ng-show=\"showAddNewReq()\"> <a href=\"#sapReports\" class=\"waves-effect\" > <i class=\"fa fa-fw\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Open PR</span> </a> </li> " +
    //        "<li ng-show=\"showAddNewReq()\"> <a href=\"#sapHome\" class=\"waves-effect\" > <i class=\"fa fa-fw\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Open PO</span> </a> </li> " +
    //    "</ul>" +
    //"</li>" +
    //"<li> <a ng-click='showReportMenu = !showReportMenu' class=\"waves-effect\"><i class=\"fa fa-user\"></i> <span class=\"hide-menu\">Reports  <span class=\"fa arrow\"></span> </span></a>" +
    //"<ul class=\"nav nav-second-level\" ng-if='showReportMenu'>" +
    ////"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/supplier\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Supplier</span></a> </li>" +
    //"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/savingsDashBoard\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Savings</span></a> </li>" +
    ////"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/contractSpend\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Contract Spend</span></a> </li>" +
    ////"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/averageProcure\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Average Procure</span></a> </li>" +
    ////"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/purchaseTurnOver\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Purchase Turnover</span></a> </li>" +
    //"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/purchaserAnalysis\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Purchaser Analysis</span></a> </li>" +
    //"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/productAlalysis/0/1\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Product Analysis</span></a> </li>" +
    //"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/vendorViewProfile/0/1\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Vendor Analysis</span></a> </li>" +
    //"<li> <a class=\"waves-effect\" data-ui-sref-active=\"active\" href=\"#/anylatics\"><i class=\"fa-fw\"></i><span class=\"hide-menu\">Analytics</span></a> </li>" +

    //"</ul>" +
    //"</li>" +

   // "<li ng-show=\"showAddNewReq()\"> <a href=\"#/vendorInvoices\" class=\"waves-effect\" data-ui-sref-active=\"active\" > <i class=\"fa fa-file-alt\" ></i> <span class=\"hide-menu\" >Vendor Invoices</span> </a> </li>" +
    //"<li> <a href=\"#/cijList\" class=\"waves-effect\"> <i class=\"fa fa-th-list\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >CIJ List</span> </a> </li>" +
    //"<li> <a href=\"#/indentList\" class=\"waves-effect\" > <i class=\"fa fa-th-list\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Indent List</span> </a> </li>" +
    //"<li><a href=\"#/consalidatedReport\" class=\"waves-effect\"><i class=\"fa fa-tachometer-alt\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Consalidated Report</span></a></li>" +
    //"<li> <a class=\"waves-effect\" ng-click=\"logout()\"> <i class=\"fa fa-sign-out-alt\" data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" >Sign out</span> </a> </li>" +
    "<li> <a class=\"waves-effect\"> <i data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" ></span> </a> </li>" +
    "<li> <a class=\"waves-effect\"> <i data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" ></span> </a> </li>" +
    "<li> <a class=\"waves-effect\"> <i data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" ></span> </a> </li>" +
    "<li> <a class=\"waves-effect\"> <i data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" ></span> </a> </li>" +
    "<li> <a class=\"waves-effect\"> <i data-ui-sref-active=\"active\"></i> <span class=\"hide-menu\" ></span> </a> </li>" +

    "</ul></div></div >"
  );




  $templateCache.put('template/footer.html',
      "Copyright &copy; {{getCurrentYear()}} PRM360" +
      "<ul class=\"f-menu\">" +
      "<li><a target=\"_blank\" href=\"http:\/\/prm360.com\">PRM360</a></li>" +
      "<li><a href=\"http:\/\/prm360.com/#screenshots\" target=\"_blank\">Clients</a></li>" +
      "<li><a  target=\"_blank\" href=\"http:\/\/prm360.com/#video\">About Us</a></li>" +
      "<li><a href=\"http:\/\/prm360.com/#contact\"  target=\"_blank\">Contact Us</a></li>" +
      "</ul>"
  );



  //$templateCache.put('template/chat.html',
  //  "<div class=\"chat-search\"><div class=\"fg-line\"><input type=\"text\" class=\"form-control\" placeholder=\"Search People\"></div></div><div class=\"listview\"><a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left p-relative\"><img class=\"lv-img-sm\" src=\"img/profile-pics/2.jpg\" alt=\"\"> <i class=\"chat-status-busy\"></i></div><div class=\"media-body\"><div class=\"lv-title\">Jonathan Morris</div><small class=\"lv-small\">Available</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/1.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">David Belle</div><small class=\"lv-small\">Last seen 3 hours ago</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left p-relative\"><img class=\"lv-img-sm\" src=\"img/profile-pics/3.jpg\" alt=\"\"> <i class=\"chat-status-online\"></i></div><div class=\"media-body\"><div class=\"lv-title\">Fredric Mitchell Jr.</div><small class=\"lv-small\">Availble</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left p-relative\"><img class=\"lv-img-sm\" src=\"img/profile-pics/4.jpg\" alt=\"\"> <i class=\"chat-status-online\"></i></div><div class=\"media-body\"><div class=\"lv-title\">Glenn Jecobs</div><small class=\"lv-small\">Availble</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/5.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Bill Phillips</div><small class=\"lv-small\">Last seen 3 days ago</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/6.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Wendy Mitchell</div><small class=\"lv-small\">Last seen 2 minutes ago</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left p-relative\"><img class=\"lv-img-sm\" src=\"img/profile-pics/7.jpg\" alt=\"\"> <i class=\"chat-status-busy\"></i></div><div class=\"media-body\"><div class=\"lv-title\">Teena Bell Ann</div><small class=\"lv-small\">Busy</small></div></div></a></div>"
  //);

  //$templateCache.put('template/header-image-logo.html',
  //  "<ul class=\"header-inner clearfix\"><li id=\"menu-trigger\" data-target=\"mainmenu\" data-toggle-sidebar data-model-left=\"mactrl.sidebarToggle.left\" data-ng-class=\"{ 'open': mactrl.sidebarToggle.left === true }\"><div class=\"line-wrap\"><div class=\"line top\"></div><div class=\"line center\"></div><div class=\"line bottom\"></div></div></li><li class=\"hidden-xs\"><a href=\"#home\" class=\"m-l-10\" data-ng-click=\"mactrl.sidebarStat($event)\"><img src=\"img/demo/logo.png\" alt=\"\"></a></li><li class=\"pull-right\"><ul class=\"top-menu\"><li id=\"top-search\" data-ng-click=\"hctrl.openSearch()\"><a href=\"\"><span class=\"tm-label\">Search</span></a></li><li class=\"dropdown\" uib-dropdown><a uib-dropdown-toggle href=\"\"><span class=\"tm-label\">Messages</span></a><div class=\"dropdown-menu dropdown-menu-lg pull-right\"><div class=\"listview\"><div class=\"lv-header\">Messages</div><div class=\"lv-body\"><a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/1.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">David Belle</div><small class=\"lv-small\">Cum sociis natoque penatibus et magnis dis parturient montes</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/2.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Jonathan Morris</div><small class=\"lv-small\">Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/3.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Fredric Mitchell Jr.</div><small class=\"lv-small\">Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/4.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Glenn Jecobs</div><small class=\"lv-small\">Ut vitae lacus sem ellentesque maximus, nunc sit amet varius dignissim, dui est consectetur neque</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/4.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Bill Phillips</div><small class=\"lv-small\">Proin laoreet commodo eros id faucibus. Donec ligula quam, imperdiet vel ante placerat</small></div></div></a></div><a class=\"lv-footer\" href=\"\">View All</a></div></div></li><li class=\"dropdown hidden-xs\" uib-dropdown><a uib-dropdown-toggle href=\"\"><span class=\"tm-label\">Notification</span></a><div class=\"dropdown-menu dropdown-menu-lg pull-right\"><div class=\"listview\" id=\"notifications\"><div class=\"lv-header\">Notification<ul class=\"actions\"><li class=\"dropdown\"><a href=\"\" data-clear=\"notification\"><i class=\"zmdi zmdi-check-all\"></i></a></li></ul></div><div class=\"lv-body\"><a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/1.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">David Belle</div><small class=\"lv-small\">Cum sociis natoque penatibus et magnis dis parturient montes</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/2.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Jonathan Morris</div><small class=\"lv-small\">Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/3.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Fredric Mitchell Jr.</div><small class=\"lv-small\">Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/4.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Glenn Jecobs</div><small class=\"lv-small\">Ut vitae lacus sem ellentesque maximus, nunc sit amet varius dignissim, dui est consectetur neque</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/4.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Bill Phillips</div><small class=\"lv-small\">Proin laoreet commodo eros id faucibus. Donec ligula quam, imperdiet vel ante placerat</small></div></div></a></div><a class=\"lv-footer\" href=\"\">View Previous</a></div></div></li><li class=\"hidden-xs\"><a target=\"_blank\" href=\"https://wrapbootstrap.com/theme/superflat-simple-responsive-admin-theme-WB082P91H\"><span class=\"tm-label\">Link</span></a></li></ul></li></ul><!-- Top Search Content --><div id=\"top-search-wrap\"><div class=\"tsw-inner\"><i id=\"top-search-close\" data-ng-click=\"hctrl.closeSearch()\" class=\"zmdi zmdi-arrow-left\"></i> <input type=\"text\"></div></div>"
  //);

  //$templateCache.put('template/header-textual-menu.html',
  //  "<ul class=\"header-inner clearfix\"><li id=\"menu-trigger\" data-target=\"mainmenu\" data-toggle-sidebar data-model-left=\"mactrl.sidebarToggle.left\" data-ng-class=\"{ 'open': mactrl.sidebarToggle.left === true }\"><div class=\"line-wrap\"><div class=\"line top\"></div><div class=\"line center\"></div><div class=\"line bottom\"></div></div></li><li class=\"logo hidden-xs\"><a data-ui-sref=\"home\" data-ng-click=\"mactrl.sidebarStat($event)\">PRM360</a></li><li class=\"pull-right\"><ul class=\"top-menu\"><li id=\"top-search\" data-ng-click=\"hctrl.openSearch()\"><a href=\"\"><span class=\"tm-label\">Search</span></a></li><li class=\"dropdown\" uib-dropdown><a uib-dropdown-toggle href=\"\"><span class=\"tm-label\">Messages</span></a><div class=\"dropdown-menu dropdown-menu-lg pull-right\"><div class=\"listview\"><div class=\"lv-header\">Messages</div><div class=\"lv-body\"><a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/1.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">David Belle</div><small class=\"lv-small\">Cum sociis natoque penatibus et magnis dis parturient montes</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/2.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Jonathan Morris</div><small class=\"lv-small\">Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/3.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Fredric Mitchell Jr.</div><small class=\"lv-small\">Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/4.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Glenn Jecobs</div><small class=\"lv-small\">Ut vitae lacus sem ellentesque maximus, nunc sit amet varius dignissim, dui est consectetur neque</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/4.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Bill Phillips</div><small class=\"lv-small\">Proin laoreet commodo eros id faucibus. Donec ligula quam, imperdiet vel ante placerat</small></div></div></a></div><a class=\"lv-footer\" href=\"\">View All</a></div></div></li><li class=\"dropdown hidden-xs\" uib-dropdown><a uib-dropdown-toggle href=\"\"><span class=\"tm-label\">Notification</span></a><div class=\"dropdown-menu dropdown-menu-lg pull-right\"><div class=\"listview\" id=\"notifications\"><div class=\"lv-header\">Notification<ul class=\"actions\"><li class=\"dropdown\"><a href=\"\" data-clear=\"notification\"><i class=\"zmdi zmdi-check-all\"></i></a></li></ul></div><div class=\"lv-body\"><a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/1.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">David Belle</div><small class=\"lv-small\">Cum sociis natoque penatibus et magnis dis parturient montes</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/2.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Jonathan Morris</div><small class=\"lv-small\">Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/3.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Fredric Mitchell Jr.</div><small class=\"lv-small\">Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/4.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Glenn Jecobs</div><small class=\"lv-small\">Ut vitae lacus sem ellentesque maximus, nunc sit amet varius dignissim, dui est consectetur neque</small></div></div></a> <a class=\"lv-item\" href=\"\"><div class=\"media\"><div class=\"pull-left\"><img class=\"lv-img-sm\" src=\"img/profile-pics/4.jpg\" alt=\"\"></div><div class=\"media-body\"><div class=\"lv-title\">Bill Phillips</div><small class=\"lv-small\">Proin laoreet commodo eros id faucibus. Donec ligula quam, imperdiet vel ante placerat</small></div></div></a></div><a class=\"lv-footer\" href=\"\">View Previous</a></div></div></li><li class=\"hidden-xs\"><a target=\"_blank\" href=\"https://wrapbootstrap.com/theme/superflat-simple-responsive-admin-theme-WB082P91H\"><span class=\"tm-label\">Link</span></a></li></ul></li></ul><!-- Top Search Content --><div id=\"top-search-wrap\"><div class=\"tsw-inner\"><i id=\"top-search-close\" data-ng-click=\"hctrl.closeSearch()\" class=\"zmdi zmdi-arrow-left\"></i> <input type=\"text\"></div></div>"
  //);

  //$templateCache.put('template/sidebar-left.html',
  // // "<div class=\"sidebar-inner c-overflow\"><div class=\"profile-menu\"><a href=\"\" toggle-submenu><div class=\"profile-pic\"><img src=\"img/profile-pics/1.jpg\" alt=\"\"></div><div class=\"profile-info\" data-ng-controller=\"profileCtrl as pctrl\">{{pctrl.fullName}} <i class=\"zmdi zmdi-caret-down\"></i></div></a><ul class=\"main-menu\"><li><a data-ui-sref=\"pages.profile.profile-about\" data-ng-click=\"mactrl.sidebarStat($event)\"><i class=\"zmdi zmdi-account\"></i> View Profile</a></li><li><a ng-click=\"logout()\"><i class=\"zmdi zmdi-time-restore\"></i> Logout</a></li></ul></div><ul class=\"main-menu\"><li data-ui-sref-active=\"active\"><a data-ui-sref=\"home\" data-ng-click=\"mactrl.sidebarStat($event)\"><i class=\"zmdi zmdi-home\"></i> Home</a></li><li data-ui-sref-active=\"active\"><a data-ui-sref=\"pages.profile.profile-timeline\" data-ng-click=\"mactrl.sidebarStat($event)\"><i class=\"zmdi zmdi-home\"></i> View My Requirements</a></li></ul></div>"
  // "<div class=\"sidebar-inner c-overflow\"><ul class=\"main-menu\"><br /><li><a data-ui-sref=\"home\" data-ng-click=\"mactrl.sidebarStat($event)\"><i class=\"zmdi zmdi-home\"></i> HOME</a></li><li><a data-ui-sref=\"pages.profile.profile-about\" data-ng-click=\"mactrl.sidebarStat($event)\"><i class=\"zmdi zmdi-account\"></i> MY ACCOUNT</a></li><li><a ng-show=\"showAddNewReq()\" data-ui-sref=\"form.addnewrequirement\" data-ng-click=\"mactrl.sidebarStat($event)\"><i class=\"zmdi zmdi-plus-circle\"></i>ADD NEW REQUIREMENT</a></li><li  ng-if=\"showIsUserVendor();\"><a data-ui-sref=\"pages.profile.profile-timeline_1\" data-ng-click=\"mactrl.sidebarStat($event)\"><i class=\"zmdi zmdi-thumb-up\"></i>LEADS</a></li><li  ng-show=\"showAddNewReq()\"><a data-ui-sref=\"pages.profile.profile-timeline\" data-ng-click=\"mactrl.sidebarStat($event)\"><i class=\"zmdi zmdi-view-list\"></i>MY REQUIREMENTS</a></li>     <li ng-if=\"isUserEntitled(360364) || !showAddNewReq()\" role=\"button\"><a data-ng-click=\"mactrl.sidebarStat($event)\"  data-ui-sref=\"apmcdashboard\"> <i class=\"zmdi zmdi-nature-people\"></i>APMC</a></li>         <li><a role=\"button\" data-ng-click=\"mactrl.sidebarStat($event)\"><i class=\"zmdi zmdi-tag-close\"></i>CLOSE MENU</a></li>      <li style\"vertical-align: bottom\"><a role=\"button\" ng-click=\"logout()\"><i class=\"zmdi zmdi-power-off-setting\"></i> LOGOUT</a></li>  </ul></div>"
  //);

  //$templateCache.put('template/carousel/carousel.html',
  //  "<div ng-mouseenter=\"pause()\" ng-mouseleave=\"play()\" class=\"carousel\" ng-swipe-right=\"prev()\" ng-swipe-left=\"next()\"><ol class=\"carousel-indicators\" ng-show=\"slides.length > 1\"><li ng-repeat=\"slide in slides | orderBy:'index' track by $index\" ng-class=\"{active: isActive(slide)}\" ng-click=\"select(slide)\"></li></ol><div class=\"carousel-inner\" ng-transclude></div><a class=\"left carousel-control\" ng-click=\"prev()\" ng-show=\"slides.length > 1\"><span class=\"zmdi zmdi-chevron-left\"></span></a> <a class=\"right carousel-control\" ng-click=\"next()\" ng-show=\"slides.length > 1\"><span class=\"zmdi zmdi-chevron-right\"></span></a></div>"
  //);

  //$templateCache.put('template/datepicker/day.html',
  //  "<table role=\"grid\" aria-labelledby=\"{{::uniqueId}}-title\" aria-activedescendant=\"{{activeDateId}}\" class=\"dp-table dpt-day\"><thead><tr class=\"tr-dpnav\"><th><button type=\"button\" class=\"pull-left btn-dp\" ng-click=\"move(-1)\" tabindex=\"-1\"><i class=\"zmdi zmdi-long-arrow-left\"></i></button></th><th colspan=\"{{::5 + showWeeks}}\"><button id=\"{{::uniqueId}}-title\" role=\"heading\" aria-live=\"assertive\" aria-atomic=\"true\" type=\"button\" ng-click=\"toggleMode()\" ng-disabled=\"datepickerMode === maxMode\" tabindex=\"-1\" class=\"w-100 btn-dp\"><div class=\"dp-title\">{{title}}</div></button></th><th><button type=\"button\" class=\"pull-right btn-dp\" ng-click=\"move(1)\" tabindex=\"-1\"><i class=\"zmdi zmdi-long-arrow-right\"></i></button></th></tr><tr class=\"tr-dpday\"><th ng-if=\"showWeeks\" class=\"text-center\"></th><th ng-repeat=\"label in ::labels track by $index\" class=\"text-center\"><small aria-label=\"{{::label.full}}\">{{::label.abbr}}</small></th></tr></thead><tbody><tr ng-repeat=\"row in rows track by $index\"><td ng-if=\"showWeeks\" class=\"text-center h6\"><em>{{ weekNumbers[$index] }}</em></td><td ng-repeat=\"dt in row track by dt.date\" class=\"text-center\" role=\"gridcell\" id=\"{{::dt.uid}}\" ng-class=\"::dt.customClass\"><button type=\"button\" class=\"w-100 btn-dp btn-dpday btn-dpbody\" ng-class=\"{'dp-today': dt.current, 'dp-selected': dt.selected, 'dp-active': isActive(dt)}\" ng-click=\"select(dt.date)\" ng-disabled=\"dt.disabled\" tabindex=\"-1\"><span ng-class=\"::{'dp-day-muted': dt.secondary, 'dp-day-today': dt.current}\">{{::dt.label}}</span></button></td></tr></tbody></table>"
  //);

  //$templateCache.put('template/datepicker/month.html',
  //  "<table role=\"grid\" aria-labelledby=\"{{::uniqueId}}-title\" aria-activedescendant=\"{{activeDateId}}\" class=\"dp-table\"><thead><tr class=\"tr-dpnav\"><th><button type=\"button\" class=\"pull-left btn-dp\" ng-click=\"move(-1)\" tabindex=\"-1\"><i class=\"zmdi zmdi-long-arrow-left\"></i></button></th><th><button id=\"{{::uniqueId}}-title\" role=\"heading\" aria-live=\"assertive\" aria-atomic=\"true\" type=\"button\" ng-click=\"toggleMode()\" ng-disabled=\"datepickerMode === maxMode\" tabindex=\"-1\" class=\"w-100 btn-dp\"><div class=\"dp-title\">{{title}}</div></button></th><th><button type=\"button\" class=\"pull-right btn-dp\" ng-click=\"move(1)\" tabindex=\"-1\"><i class=\"zmdi zmdi-long-arrow-right\"></i></button></th></tr></thead><tbody><tr ng-repeat=\"row in rows track by $index\"><td ng-repeat=\"dt in row track by dt.date\" class=\"text-center\" role=\"gridcell\" id=\"{{::dt.uid}}\" ng-class=\"::dt.customClass\"><button type=\"button\" class=\"w-100 btn-dp btn-dpbody\" ng-class=\"{'dp-selected': dt.selected, 'dp-active': isActive(dt)}\" ng-click=\"select(dt.date)\" ng-disabled=\"dt.disabled\" tabindex=\"-1\"><span ng-class=\"::{'dp-day-today': dt.current}\">{{::dt.label}}</span></button></td></tr></tbody></table>"
  //);

  //$templateCache.put('template/datepicker/popup.html',
  //  "<ul class=\"dropdown-menu\" ng-keydown=\"keydown($event)\"><li ng-transclude></li><li ng-if=\"showButtonBar\" class=\"dp-actions clearfix\"><button type=\"button\" class=\"btn btn-link\" ng-click=\"select('today')\">{{ getText('current') }}</button> <button type=\"button\" class=\"btn btn-link\" ng-click=\"close()\">{{ getText('close') }}</button></li></ul>"
  //);

  //$templateCache.put('template/datepicker/year.html',
  //  "<table role=\"grid\" aria-labelledby=\"{{::uniqueId}}-title\" aria-activedescendant=\"{{activeDateId}}\" class=\"dp-table\"><thead><tr class=\"tr-dpnav\"><th><button type=\"button\" class=\"pull-left btn-dp\" ng-click=\"move(-1)\" tabindex=\"-1\"><i class=\"zmdi zmdi-long-arrow-left\"></i></button></th><th colspan=\"3\"><button id=\"{{::uniqueId}}-title\" role=\"heading\" aria-live=\"assertive\" aria-atomic=\"true\" type=\"button\" class=\"w-100 btn-dp\" ng-click=\"toggleMode()\" ng-disabled=\"datepickerMode === maxMode\" tabindex=\"-1\"><div class=\"dp-title\">{{title}}</div></button></th><th><button type=\"button\" class=\"pull-right btn-dp\" ng-click=\"move(1)\" tabindex=\"-1\"><i class=\"zmdi zmdi-long-arrow-right\"></i></button></th></tr></thead><tbody><tr ng-repeat=\"row in rows track by $index\"><td ng-repeat=\"dt in row track by dt.date\" class=\"text-center\" role=\"gridcell\" id=\"{{::dt.uid}}\"><button type=\"button\" class=\"w-100 btn-dp btn-dpbody\" ng-class=\"{'dp-selected': dt.selected, 'dp-active': isActive(dt)}\" ng-click=\"select(dt.date)\" ng-disabled=\"dt.disabled\" tabindex=\"-1\"><span ng-class=\"::{'dp-day-today': dt.current}\">{{::dt.label}}</span></button></td></tr></tbody></table>"
  //);

  //$templateCache.put('template/pagination/pager.html',
  //  "<ul class=\"pager\"><li ng-class=\"{disabled: noPrevious(), previous: align}\"><a href ng-click=\"selectPage(page - 1, $event)\">Previous</a></li><li ng-class=\"{disabled: noNext(), next: align}\"><a href ng-click=\"selectPage(page + 1, $event)\">Next</a></li></ul>"
  //);

  //$templateCache.put('template/tabs/tabset.html',
  //  "<div class=\"clearfix\"><ul class=\"tab-nav\" ng-class=\"{'tn-vertical': vertical, 'tn-justified': justified, 'tab-nav-right': right}\" ng-transclude></ul><div class=\"tab-content\"><div class=\"tab-pane ellipsis\" ng-repeat=\"tab in tabs\" ng-class=\"{active: tab.active}\" tab-content-transclude=\"tab\"></div></div></div>"
  //);

}]);
