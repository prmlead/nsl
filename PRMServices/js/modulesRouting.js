﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider

                //////////////
                //  STORE   //
                //////////////
                .state('stores', {
                    url: '/stores',
                    templateUrl: 'views/storeviews/stores.html'
                })
                .state('branches', {
                    url: '/stores/branches/:storeID',
                    templateUrl: 'views/storeviews/stores.html'
                })
                .state('addnewstore', {
                    url: '/addnewstore/:storeID',
                    templateUrl: 'views/storeviews/addnewstore.html',
                    params: { storeObj: null },
                    params: { companyStores: null }
                })


                .state('storeitems', {
                    url: '/storeitems/:storeID/:itemID',
                    templateUrl: 'views/storeviews/storeitems.html',
                    params: { storeObj: null },
                })
                .state('addnewstoreitem', {
                    url: '/addnewstoreitem/:storeID/:itemID',
                    templateUrl: 'views/storeviews/addnewstoreitem.html',
                    params: { itemObj: null }
                })

                .state('storeitemproperties', {
                    url: '/storeitemproperties/:compID/:itemID',
                    templateUrl: 'views/storeviews/storeitemproperties.html',
                    params: { itemObj: null }
                })

                .state('storeitemdetails', {
                    url: '/storeitemdetails/:itemID',
                    templateUrl: 'views/storeviews/storeitemdetails.html'
                })
                .state('storegin', {
                    url: '/storegin/:storeID',
                    templateUrl: 'views/storeviews/storegin.html',
                    params: { itemsArr: null }
                })
                .state('storegrn', {
                    url: '/storegrn/:storeID',
                    templateUrl: 'views/storeviews/storegrn.html',
                    params: { itemsArr: null, "linkgin": '' }
                })
                .state('storeginhistory', {
                    url: '/ginhistory/:storeID',
                    templateUrl: 'views/storeviews/storeginhistory.html'
                })
                .state('storegrnhistory', {
                    url: '/grnhistory/:storeID',
                    templateUrl: 'views/storeviews/storegrnhistory.html'
                })
                .state('storegindetails', {
                    url: '/gindetails/:storeID/:ginCode',
                    templateUrl: 'views/storeviews/storegindetails.html',
                    params: { ginObj: null }
                })
                .state('storegrndetails', {
                    url: '/grndetails/:storeID/:grnCode',
                    templateUrl: 'views/storeviews/storegrndetails.html',
                    params: { grnObj: null }
                })


                /////////////////
                //  TECHEVAL   //
                /////////////////

                .state('managetecheval', {
                    url: '/managetecheval',
                    templateUrl: 'views/techevalviews/managetecheval.html'
                })
                .state('savequestionnaire', {
                    url: '/savequestionnaire/:compID/:questionnaireID',
                    templateUrl: 'views/techevalviews/savequestionnaire.html'
                })
                .state('clonequestionnaire', {
                    url: '/savequestionnaire/:compID/:questionnaireID/:cloneData',
                    templateUrl: 'views/techevalviews/savequestionnaire.html'
                })
                .state('questions', {
                    url: '/questions/:reqID/:evalID',
                    templateUrl: 'views/techevalviews/techevalquestions.html'
                })
                .state('techevalresponses', {
                    url: '/techevalresponses/:evalID/:vendorID',
                    templateUrl: 'views/techevalviews/techevalresponses.html',
                    params: { techEvalObj: null }
                })

                .state('techeval', {
                    url: '/techeval/:reqID/:evalID',
                    templateUrl: 'views/techevalviews/techevaluation.html'
                })

                /////////////////
                //  REPORTING   //
                /////////////////

                .state('reports', {
                    url: '/reports/:reqID',
                    templateUrl: 'views/reportingviews/reports.html',
                })


                .state('sapReports', {
                    url: '/sapReports',
                    templateUrl: 'views/reportingviews/sapReports.html',
                })

                .state('sapReportsOpenPO', {
                    url: '/sapReportsOpenPO',
                    templateUrl: 'views/reportingviews/sapReportsOpenPO.html',
                })

                .state('sapHome', {
                    url: '/sapHome',
                    templateUrl: 'views/reportingviews/sapHome.html',
                })


                .state('sapShortagePOReports', {
                    url: '/sapShortagePOReports',
                    templateUrl: 'views/reportingviews/sapShortageReport.html',
                })

                .state('sapShortagePRReports', {
                    url: '/sapShortagePRReports',
                    templateUrl: 'views/reportingviews/sapShortagePRReport.html',
                })

                /////////////////
                //  PO GENERATE   //
                /////////////////

                .state('po', {
                    url: '/po/:reqID/:vendorID/:poID',
                    templateUrl: 'views/po/generate-po.html',
                })

                .state('po-list', {
                    url: '/po-list/:reqID/:vendorID/:poID',
                    templateUrl: 'views/po/po-List.html',
                })

                .state('desc-po', {
                    url: '/desc-po/:reqID',
                    templateUrl: 'views/po/generate-desc-po.html',
                })

                .state('desc-dispatch-challan', {
                    url: '/desc-dispatch-challan/:reqID/:poID',
                    templateUrl: 'views/po/desc-dispatch-challan.html',
                })

                .state('dispatchtrack', {
                    url: '/dispatchtrack/:reqID/:poOrderId/:dCode',
                    templateUrl: 'views/po/dispatchtrack.html',
                })

                .state('dtform', {
                    url: '/dtform/:reqID/:poOrderId/:dCode',
                    templateUrl: 'views/po/dispatchtrackform.html',
                })

                .state('asnForm', {
                    url: '/asnForm/:poOrderId/:dCode/:vendorCode',
                    templateUrl: 'views/po/dispatchtrackform.html',
                })

                .state('viewpo', {
                    url: '/viewpo/:reqID/:vendorID/:poID',
                    templateUrl: 'views/po/viewpo.html',
                })

                .state('paymentTrack', {
                    url: '/paymentTrack/:reqID/:vendorID/:poID',
                    templateUrl: 'views/po/paymentTrack.html',
                })

                .state('workflow', {
                    url: '/workflows',
                    templateUrl: 'views/workflow-views/workflows.html'
                })

                .state('workflow-status', {
                    url: '/workflow-status/:workflowID/:moduleID/:module',
                    templateUrl: 'views/workflow-views/workflow-status.html'
                })

                .state('workflow-approval', {
                    url: '/workflow-approval/:workflowTrackID/:moduleID/:wfstatus/:sessionID/:userID/:workflowID/:module',
                    templateUrl: 'views/workflow-views/workflow-approval.html'
                })

                .state('workflow-pending-approvals', {
                    url: '/workflow-pending-approvals',
                    templateUrl: 'views/workflow-views/workflow-pending-approvals.html'
                })


                // #region LOGISTICS

                .state('form.addnewlogistic', {
                    url: '/addnewlogistic/:Id',
                    templateUrl: 'views/Logistics/addNewLogistic.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('new-logistic-1', {
                    url: '/new-logistic-1/:Id',
                    templateUrl: 'views/Logistics/new-logistic-1.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('new-logistic-2', {
                    url: '/new-logistic-2/:Id',
                    templateUrl: 'views/Logistics/new-logistic-2.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('new-logistic-3', {
                    url: '/new-logistic-3/:Id',
                    templateUrl: 'views/Logistics/new-logistic-3.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('new-logistic-4', {
                    url: '/new-logistic-4/:Id',
                    templateUrl: 'views/Logistics/new-logistic-4.html',
                    params: {
                        reqObj: null
                    }
                })



                .state('pages.profile.customerlogisticslist', {
                    url: '/customerlogisticslist',
                    templateUrl: 'views/Logistics/customerLogisticsList.html',
                })

                .state('pages.profile.vendorlogisticslist', {
                    url: '/vendorlogisticslist',
                    templateUrl: 'views/Logistics/vendorLogisticsList.html',
                })


                .state('customerlogistic', {
                    url: '/customerlogistic/:Id',
                    templateUrl: 'views/Logistics/customerLogistic.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('vendorlogistic', {
                    url: '/vendorlogistic/:Id',
                    templateUrl: 'views/Logistics/vendorLogistic.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('lgstcsBidHistory', {
                    url: '/lgstcsBidHistory/:Id/:reqID',
                    templateUrl: 'views/Logistics/lgstcsBidHistory.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('lgstcsReminders', {
                    url: '/lgstcsReminders/:Id',
                    templateUrl: 'views/Logistics/lgstcsReminders.html',
                    params: {
                        reqObj: null
                    }
                })

                // #endregion LOGISTICS

                // #region APMC

                .state('margin-requirement', {
                    url: '/margin-requirement/:Id',
                    templateUrl: 'views/auction/margin-auction-page.html',
                    onEnter: function (store, $state) {
                        ////console.log('entry');
                        if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                            ////console.log('verified user');
                        } else {
                            $state.go('login');
                        }
                    },
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'css',
                                    insertBefore: '#app-level',
                                    files: [
                                        'vendors/bower_components/nouislider/jquery.nouislider.css',
                                        'vendors/farbtastic/farbtastic.css',
                                        'vendors/bower_components/summernote/dist/summernote.css',
                                        'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                        'vendors/bower_components/chosen/chosen.min.css'
                                    ]
                                },
                                {
                                    name: 'vendors',
                                    files: [
                                        'vendors/bower_components/ofline/offline.js',
                                        'vendors/bower_components/ofline/offline.js',
                                        'vendors/input-mask/input-mask.min.js',
                                        'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                        //'vendors/bower_components/moment/min/moment.min.js',
                                        'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                        'vendors/bower_components/summernote/dist/summernote.min.js',
                                        'vendors/fileinput/fileinput.min.js',
                                        'vendors/bower_components/chosen/chosen.jquery.js',
                                        'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                        'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'

                                    ]
                                }
                            ])
                        }
                    }
                })


                .state('manageapmc', {
                    url: '/manageapmc',
                    templateUrl: 'views/APMC/manage-apmc.html'
                })
                .state('saveapmc', {
                    url: '/saveapmc/:apmcID',
                    templateUrl: 'views/APMC/save-apmc.html'
                })
                .state('viewapmc', {
                    url: '/viewapmc/:apmcID',
                    templateUrl: 'views/APMC/view-apmc.html'
                })
                .state('apmcpurchasehistory', {
                    url: '/apmcpurchasehistory/:apmcnegid/:vendorid',
                    templateUrl: 'views/APMC/apmc-purchases.html'
                })
                .state('apmcdashboard', {
                    url: '/apmcdashboard',
                    templateUrl: 'views/APMC/apmc-dashboard.html'
                })
                .state('saveapmcinput', {
                    url: '/createapmcnegotiation',
                    templateUrl: 'views/APMC/save-apmc-input.html'
                })
                .state('apmcnegotiations', {
                    url: '/apmcnegotiations/:userID',
                    templateUrl: 'views/APMC/apmc-negotiationsList.html'
                })
                .state('apmcnegotiationdata', {
                    url: '/apmcnegotiationdata/:apmcnegID',
                    templateUrl: 'views/APMC/apmc-negotiation-data.html'
                })
                .state('new-requirement-1', {
                    url: '/new-requirement-1/:Id',
                    templateUrl: 'views/new-requirement/new-requirement-1.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('new-requirement-2', {
                    url: '/new-requirement-2/:Id',
                    templateUrl: 'views/new-requirement/new-requirement-2.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('new-requirement-3', {
                    url: '/new-requirement-3/:Id',
                    templateUrl: 'views/new-requirement/new-requirement-3.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('new-requirement-4', {
                    url: '/new-requirement-4/:Id',
                    templateUrl: 'views/new-requirement/new-requirement-4.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('save-requirement_autoconfigtest', {
                    url: '/save-requirement_autoconfigtest/:Id',
                    templateUrl: 'views/new-requirement/save-requirement_autoconfigtest.html',
                    params: {
                        reqObj: null
                    }
                })




                .state('save-requirement', {
                    url: '/save-requirement/:Id',
                    templateUrl: 'views/new-requirement/save-requirement.html',
                    params: {
                        reqObj: null
                    }
                })


                .state('save-requirementAdv', {
                    url: '/save-requirementAdv/:Id',
                    templateUrl: 'views/new-requirement/save-requirement_catalogue.html',
                    params: {
                        reqObj: null,
                        prDetail: null,
                        prDetailsList: null,
                        prItemsList: null,
                        selectedTemplate: null,
                        selectedPRNumbers: null
                    }
                })

                .state('save-requirementAdv1', {
                    url: '/save-requirementAdv1/:Id',
                    templateUrl: 'views/new-requirement/save-requirement_catalogue.html',
                    params: {
                        reqObj: null,
                        prDetail: null,
                        prDetailsList: null,
                        prItemsList: null,
                        selectedTemplate: null,
                        selectedPRNumbers: null,
                        isRFP: true
                    }
                })

                .state('pendingpos', {
                    url: '/pendingpos',
                    templateUrl: 'views/po/pending-po.html',
                    params: {
                        reqObj: null,
                        prDetail: null
                    }
                })

                .state('pendingasn', {
                    url: '/pendingasn',
                    templateUrl: 'views/po/pending-asn.html',
                    params: {
                        reqObj: null,
                        prDetail: null
                    }
                })

                .state('pendingcontracts', {
                    url: '/pendingcontracts',
                    templateUrl: 'views/po/pending-contracts.html',
                    params: {
                        contractProducts: null
                    }
                })

                .state('matchPrices', {
                    url: '/matchPrices',
                    templateUrl: 'views/po/matchPrices.html',
                    params: {
                        contractProducts: null
                    }
                })

                .state('invoiceaging', {
                    url: '/invoiceaging',
                    templateUrl: 'views/po/invoiceaging.html',
                    params: {
                        reqObj: null,
                        prDetail: null
                    }
                })



                // #endregion APMC

                .state('pages.settings_registration', {
                    url: '/configuration/registration',
                    templateUrl: 'views/configuration/Registration/RegistrationSetup.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('pages.real_time_price', {
                    url: '/realtimeprice/{typeId}',
                    templateUrl: 'views/realtimeprice/index.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('pages.logistic_pricecomparison', {
                    url: '/logistic/price_comparison/:reqId',
                    templateUrl: 'views/Logistics/LogisticsPriceComparison.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('pages.real_time_price_settings', {
                    url: '/configuration/real-time-price/',
                    templateUrl: 'views/realtimeprice/Configuration.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('pages.imports', {
                    url: '/imports',
                    templateUrl: 'views/import_view.html',
                    params: {
                        reqObj: null
                    }
                })

                //Contact detail
                .state('pages.master_contactdetails', {
                    url: '/master/contact_detail',
                    templateUrl: 'views/masters/ContactDetails/list.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('pages.master_contactdetail_add', {
                    url: '/master/contact_detail/add',
                    templateUrl: 'views/masters/ContactDetails/add.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('pages.master_contactdetail_edit', {
                    url: '/master/contact_detail/edit/:id',
                    templateUrl: 'views/masters/ContactDetails/edit.html',
                    params: {
                        reqObj: null
                    }
                })


                //Delivery location
                .state('pages.master_deliverylocations', {
                    url: '/master/delivery_location',
                    templateUrl: 'views/masters/deliverylocations/list.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('pages.master_deliverylocation_add', {
                    url: '/master/delivery_location/add',
                    templateUrl: 'views/masters/deliverylocations/add.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('pages.master_deliverylocation_edit', {
                    url: '/master/delivery_location/edit/:id',
                    templateUrl: 'views/masters/deliverylocations/edit.html',
                    params: {
                        reqObj: null
                    }
                })

                //Delivery term
                .state('pages.master_deliveryterms', {
                    url: '/master/delivery_term',
                    templateUrl: 'views/masters/deliveryterms/list.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('pages.master_deliveryterm_add', {
                    url: '/master/delivery_term/add',
                    templateUrl: 'views/masters/deliveryterms/add.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('pages.master_deliveryterm_edit', {
                    url: '/master/delivery_term/edit/:id',
                    templateUrl: 'views/masters/deliveryterms/edit.html',
                    params: {
                        reqObj: null
                    }
                })

                //Payment term
                .state('pages.master_paymentterms', {
                    url: '/master/payment_term',
                    templateUrl: 'views/masters/paymentterms/list.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('pages.master_paymentterm_add', {
                    url: '/master/payment_term/add',
                    templateUrl: 'views/masters/paymentterms/add.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('pages.master_paymentterm_edit', {
                    url: '/master/payment_term/edit/:id',
                    templateUrl: 'views/masters/paymentterms/edit.html',
                    params: {
                        reqObj: null
                    }
                })

                //General term
                .state('pages.master_generalterms', {
                    url: '/master/general_term',
                    templateUrl: 'views/masters/generalterms/list.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('pages.master_generalterm_add', {
                    url: '/master/general_term/add',
                    templateUrl: 'views/masters/generalterms/add.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('pages.master_generalterm_edit', {
                    url: '/master/general_term/edit/:id',
                    templateUrl: 'views/masters/generalterms/edit.html',
                    params: {
                        reqObj: null
                    }
                })


                .state('pages.config_requirement_form', {
                    url: '/configuration/requirement_form',
                    templateUrl: 'views/configuration/Requirement/Configuration.html',
                    params: {
                        reqObj: null
                    }
                })


                .state('pages.requirement_template', {
                    url: '/requirement/template',
                    templateUrl: 'views/requirements/templates/list.html',
                    params: {
                        reqObj: null
                    }
                })

                .state('pages.requirement_template_add', {
                    url: '/requirement/template/add',
                    templateUrl: 'views/requirements/templates/add.html',
                    params: {
                        reqObj: null
                    }
                })
                .state('pages.requirement_template_edit', {
                    url: '/requirement/template/edit/:id',
                    templateUrl: 'views/requirements/templates/edit.html',
                    params: {
                        reqObj: null
                    }
                })

                //#CB-0-2018-12-05
                //#region COUNTER BID
                .state('cb-customer', {
                    url: '/cb-customer/:reqID/:vendorID',
                    templateUrl: 'views/CounterBid/cb-customer.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('cb-vendor', {
                    url: '/cb-vendor/:reqID/:vendorID',
                    templateUrl: 'views/CounterBid/cb-vendor.html',
                    params: {
                        detailsObj: null
                    }
                })
                //#endregion COUNTER BID


                .state('qcs', {
                    url: '/qcs/:reqID/:qcsID',
                    templateUrl: 'views/reportingviews/qcs.html'
                })


                .state('list-qcs', {
                    url: '/list-qcs/:reqID',
                    templateUrl: 'views/reportingviews/list-qcs.html'
                })

                .state('qcsRM', {
                    url: '/qcsRM/:reqID/:itemID',
                    templateUrl: 'views/reportingviews/qcsRM.html'
                })

                .state('cost-comparisions-qcs', {
                    url: '/cost-comparisions-qcs/:reqID/:qcsID',
                    templateUrl: 'views/reportingviews/cost-comparisions-qcs.html'
                })

                .state('cost-comparisions-qcs-excel', {
                    url: '/cost-comparisions-qcs-excel/:reqID/:qcsID',
                    templateUrl: 'views/reportingviews/cost-comparisions-qcs-excel.html'
                })

                .state('import-qcs', {
                    url: '/import-qcs/:reqID/:qcsID',
                    templateUrl: 'views/reportingviews/import-qcs.html'
                })

                .state('import-qcs-excel', {
                    url: '/import-qcs-excel/:reqID/:qcsID',
                    templateUrl: 'views/reportingviews/import-qcs-excel.html'
                })

                .state('approval-qcs-list', {
                    url: '/approval-qcs-list',
                    templateUrl: 'views/reportingviews/approval-qcs-list.html'
                })

                .state('mac', {
                    url: '/mac/:reqID',
                    templateUrl: 'views/mac/mac-po.html'
                })

                //# Active Users Start
                .state('activeUsers', {
                    url: '/activeUsers',
                    templateUrl: 'views/activeUsers.html'
                })

                //# Active Users End

                //# Email Logs Start
                .state('emailLogs', {
                    url: '/emailLogs',
                    templateUrl: 'views/emailLogs.html'
                })

                .state('smsLogs', {
                    url: '/smsLogs',
                    templateUrl: 'views/smsLogs.html'
                })

                //# Email Logs End



                .state('companies', {
                    url: '/companies',
                    templateUrl: 'views/companies.html',
                    params: {
                        companyNames: null
                    }
                })

                .state('productSearchBestPrice', {
                    url: '/productSearchBestPrice',
                    templateUrl: 'views/productSearchBestPrice.html'
                });

        }]);