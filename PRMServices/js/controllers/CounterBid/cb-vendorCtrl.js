﻿prmApp

    .controller('cb-vendorCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout", "auctionsService", "fwdauctionsService",
        "userService", "SignalRFactory", "fileReader", "growlService", "signalRHubName", "PRMCustomFieldService",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout, auctionsService, fwdauctionsService,
            userService, SignalRFactory, fileReader, growlService, signalRHubName, PRMCustomFieldService) {

            $scope.spReqID = $stateParams.reqID;
            $scope.spVendorID = $stateParams.vendorID;
            $scope.sessionid = userService.getUserToken();
            $scope.userID = userService.getUserId();
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            $scope.hideFreeze = false;

            if ($scope.isCustomer) {
                $state.go('home');
                return;
            }

            /********** MAX REDUCTION CODE DISABLE *********/
            $scope.showMaxReductionValidation = false;
            $scope.maxReductionPercentage = 30;
            $scope.reductionAmountConfigure = (100 - $scope.maxReductionPercentage);
            /********** MAX REDUCTION CODE DISABLE *********/

            $scope.vendorIncoTerms = {};
            $scope.userIsOwner = false;
            $scope.bidComments = '';
            $scope.timerTimeLeft = 0;
            $scope.timerStyle = { 'color': '#000' };
            $scope.auctionItemVendor = [];
            $scope.productConfig = [];
            $scope.cbInitiated = false;
            $scope.cbOffered = false;
            $scope.cbOfferedMessage = '';
            $scope.cbOfferedObj = {};
            $scope.CBPricesAudit = {};
            $scope.cbStartedInputColour = {};
            $scope.bidHistoryExpand = {};
            $scope.CurrentPrice = 0;
            $scope.ReduceBidAmountBy = 0;
            $scope.NewPriceToBeQuoted = 0;
            $scope.isBidHistoryExpand = false;
            $scope.selectedTemplate = {};
            $scope.prmFieldMappingDetails = {};

            $scope.goToListItem = function () {
                $state.go('view-requirement', { 'Id': $scope.spReqID });
            };

            $scope.getData = function (applySignalR) {
                auctionsService.getrequirementdata({ "reqid": $stateParams.reqID, "sessionid": userService.getUserToken(), "userid": $stateParams.vendorID })
                    .then(function (response) {
                        if (response) {
                            auctionsService.GetCompanyConfiguration(response.custCompID, "ITEM_UNITS", userService.getUserToken())
                                .then(function (unitResponse) {
                                    $scope.companyItemUnits = unitResponse;
                                });
                        }
                        $scope.fieldValidation(response, applySignalR);
                        //$scope.setAuctionInitializer(response, applySignalR);
                    });
            };

            $scope.setAuctionInitializer = function (response, applySignalR) {
                $scope.auctionItem = response;

                $scope.CurrentPrice = $scope.auctionItem.auctionVendors[0].revVendorTotalPrice;

                $scope.auctionItem.auctionVendors[0].revfreightChargesDB = $scope.auctionItem.auctionVendors[0].revfreightCharges;
                $scope.auctionItem.auctionVendors[0].revpackingChargesDB = $scope.auctionItem.auctionVendors[0].revpackingCharges;
                $scope.auctionItem.auctionVendors[0].revinstallationChargesDB = $scope.auctionItem.auctionVendors[0].revinstallationCharges;

                $scope.auctionItem.auctionVendors[0].listRequirementItems.forEach(function (item, itemIndex) {
                    item.revUnitPriceDB = item.revUnitPrice;
                })

                if (!$scope.auctionItem ||
                    ($scope.auctionItem && $scope.auctionItem.auctionVendors &&
                        $scope.auctionItem.auctionVendors.length > 0 &&
                        $scope.auctionItem.auctionVendors[0].revVendorTotalPriceCB > 0 &&
                        $scope.auctionItem.auctionVendors[0].isQuotationRejected == 1) ||
                    $scope.auctionItem.IS_CB_ENABLED == 0 ||
                    $scope.auctionItem.IS_CB_COMPLETED == true) {

                    $scope.goToListItem();

                };

                if ($scope.auctionItem.auctionVendors[0].FREEZE_CB == false &&
                    $scope.auctionItem.auctionVendors[0].VEND_FREEZE_CB == false &&
                    $scope.auctionItem.auctionVendors[0].FREEZE_CB == false) {
                    $scope.cbStartedInputColour = {
                        'background-color': '#f5b2b2'
                    };
                }

                $scope.description = $scope.auctionItem.description.replace(/\u000a/g, "</br>");
                $scope.deliveryLocation = $scope.auctionItem.deliveryLocation.replace(/\u000a/g, "</br>");
                $scope.paymentTerms = $scope.auctionItem.paymentTerms.replace(/\u000a/g, "</br>");
                $scope.deliveryTime = $scope.auctionItem.deliveryTime.replace(/\u000a/g, "</br>");

                //var date = $scope.auctionItem.postedOn.split('+')[0].split('(')[1];
                //var newDate = new Date(parseInt(parseInt(date)));
                //$scope.auctionItem.postedOn = newDate.toString().replace('Z', '');

                $scope.auctionItem.postedOn = $scope.GetDateconverted($scope.auctionItem.postedOn);

                $scope.auctionItem.multipleAttachments = [];
                $scope.auctionItem.attFile = $scope.auctionItem.attachmentName;

                if ($scope.auctionItem.attFile != '' && $scope.auctionItem.attFile != null && $scope.auctionItem.attFile != undefined) {
                    var attchArray = $scope.auctionItem.attFile.split(',');
                    attchArray.forEach(function (att, index) {
                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: att
                        };
                        $scope.auctionItem.multipleAttachments.push(fileUpload);
                    })
                }


                if ($scope.auctionItem.auctionVendors[0] ) {
                    var vendor = $scope.auctionItem.auctionVendors[0];
                        if (!$scope.multipleAttachmentsList) {
                            $scope.multipleAttachmentsList = [];
                            $scope.bidAttachement = [];
                        }
                        if (!vendor.multipleAttachmentsList) {
                            vendor.multipleAttachmentsList = [];
                        }

                        if (vendor.multipleAttachments != '' && vendor.multipleAttachments != null && vendor.multipleAttachments != undefined) {
                            var multipleAttachmentsList = vendor.multipleAttachments.split(',');
                            vendor.multipleAttachmentsList = [];
                            $scope.bidAttachement = [];
                            $scope.multipleAttachmentsList = [];
                            multipleAttachmentsList.forEach(function (att, index) {
                                var fileUpload = {
                                    fileStream: [],
                                    fileName: '',
                                    fileID: att
                                };

                                vendor.multipleAttachmentsList.push(fileUpload);
                                $scope.multipleAttachmentsList.push(fileUpload);
                                $scope.bidAttachement.push(fileUpload);
                            });
                        }
                }

                if (applySignalR) {
                    var auctionItem = $scope.auctionItem;
                    $scope.auctionItemVendor = auctionItem;
                    $scope.getCalculatedPrices();
                }


            };

            $scope.getCalculatedPrices = function () {
                // $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.CB_TIME_LEFT);
                $scope.hideFreeze = false;

                $scope.itemLevelCalculations();

                $scope.auctionItemVendor.auctionVendors[0].revPrice = _.sumBy($scope.auctionItemVendor.auctionVendors[0].listRequirementItems, 'revitemPrice');
                $scope.auctionItemVendor.auctionVendors[0].revPriceCB = _.sumBy($scope.auctionItemVendor.auctionVendors[0].listRequirementItems, 'revitemPriceCB');



                if ($scope.auctionItemVendor.auctionVendors[0].revfreightCharges > $scope.auctionItemVendor.auctionVendors[0].revfreightChargesDB) {
                    swal("Error!", 'Please enter price less than ' + $scope.auctionItemVendor.auctionVendors[0].revfreightChargesDB);
                    $scope.auctionItemVendor.auctionVendors[0].revfreightCharges = $scope.auctionItemVendor.auctionVendors[0].revfreightChargesDB;
                }

                if ($scope.auctionItemVendor.auctionVendors[0].revpackingCharges > $scope.auctionItemVendor.auctionVendors[0].revpackingChargesDB) {
                    swal("Error!", 'Please enter price less than ' + $scope.auctionItemVendor.auctionVendors[0].revpackingChargesDB);
                    $scope.auctionItemVendor.auctionVendors[0].revpackingCharges = $scope.auctionItemVendor.auctionVendors[0].revpackingChargesDB;
                }

                if ($scope.auctionItemVendor.auctionVendors[0].revinstallationCharges > $scope.auctionItemVendor.auctionVendors[0].revinstallationChargesDB) {
                    swal("Error!", 'Please enter price less than ' + $scope.auctionItemVendor.auctionVendors[0].revinstallationChargesDB);
                    $scope.auctionItemVendor.auctionVendors[0].revinstallationCharges = $scope.auctionItemVendor.auctionVendors[0].revinstallationChargesDB;
                }


                var revfreightCharges = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].revfreightCharges);
                var freightChargesTaxPercentage = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].freightChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revfreightChargesWithTax = parseFloat(revfreightCharges +
                    ((revfreightCharges / 100) * freightChargesTaxPercentage));

                var revfreightChargesCB = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].revfreightChargesCB);
                var freightChargesTaxPercentage = 0 //parseFloat($scope.auctionItemVendor.auctionVendors[0].freightChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revfreightChargesWithTaxCB = parseFloat(revfreightChargesCB +
                    ((revfreightChargesCB / 100) * freightChargesTaxPercentage));


                var revpackingCharges = 0; // parseFloat($scope.auctionItemVendor.auctionVendors[0].revpackingCharges);
                var packingChargesTaxPercentage = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].packingChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revpackingChargesWithTax = parseFloat(revpackingCharges +
                    ((revpackingCharges / 100) * packingChargesTaxPercentage));

                var revpackingChargesCB = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].revpackingChargesCB);
                var packingChargesTaxPercentage = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].packingChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revpackingChargesWithTaxCB = parseFloat(revpackingChargesCB +
                    ((revpackingChargesCB / 100) * packingChargesTaxPercentage));


                var revinstallationCharges = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].revinstallationCharges);
                var installationChargesTaxPercentage = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].installationChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revinstallationChargesWithTax = parseFloat(revinstallationCharges +
                    ((revinstallationCharges / 100) * installationChargesTaxPercentage));

                var revinstallationChargesCB = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].revinstallationChargesCB);
                var installationChargesTaxPercentage = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].installationChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revinstallationChargesWithTaxCB = parseFloat(revinstallationChargesCB +
                    ((revinstallationChargesCB / 100) * installationChargesTaxPercentage));





                $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPriceCB = parseFloat($scope.auctionItemVendor.auctionVendors[0].revPriceCB);
                $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice = parseFloat($scope.auctionItemVendor.auctionVendors[0].revPrice);

                $scope.ReduceBidAmountBy = parseFloat($scope.CurrentPrice).toFixed(2) - parseFloat($scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice).toFixed(2);
                if ($scope.ReduceBidAmountBy != 0) {
                    $scope.hideFreeze = true;
                }
                if ($scope.ReduceBidAmountBy <= 0) {
                    $scope.ReduceBidAmountBy = 0;
                }
                $scope.NewPriceToBeQuoted = $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice;

            };

            $scope.itemLevelCalculations = function () {
                $scope.auctionItemVendor.auctionVendors[0].revPrice = 0;
                $scope.auctionItemVendor.auctionVendors[0].revPriceCB = 0;
                $scope.auctionItemVendor.auctionVendors[0].listRequirementItems.forEach(function (item, itemIndex) {

                    if (item.revUnitPrice > item.revUnitPriceDB) {
                        swal("Error!", 'Please enter price less than ' + item.revUnitPriceDB + ' of ' + item.productIDorName);
                        item.revUnitPrice = item.revUnitPriceDB;
                    }

                    //#region Item Level Calculations

                    item.itemPrice = item.unitPrice * item.productQuantity;
                    //item.cGst = item.Gst / 2;
                    //item.sGst = item.Gst / 2;
                    //item.iGst = 0;
                    item.cGst = item.cGst;
                    item.sGst = item.sGst;
                    item.iGst = item.iGst;
                    item.itemPrice = (item.itemPrice + ((item.itemPrice / 100) * (item.cGst + item.sGst + item.iGst)));
                    var itemFreightWithTax = 0;// (item.itemFreightCharges + ((item.itemFreightCharges / 100) * (item.itemFreightTAX)));
                    item.itemPrice = item.itemPrice + itemFreightWithTax;

                    var revUnitPrice = parseFloat(item.revUnitPrice);
                    var itemRevFreightCharges = parseFloat(item.itemRevFreightCharges);

                    item.revitemPrice = revUnitPrice * item.productQuantity;
                    //item.cGst = item.Gst / 2;
                    //item.sGst = item.Gst / 2;
                    //item.iGst = 0;
                    item.cGst = item.cGst;
                    item.sGst = item.sGst;
                    item.iGst = item.iGst;
                    item.revitemPrice = (item.revitemPrice + ((item.revitemPrice / 100) * (item.cGst + item.sGst + item.iGst)));
                    var revItemFreightWithTax = 0;//(itemRevFreightCharges + ((itemRevFreightCharges / 100) * (item.itemFreightTAX)));
                    item.revitemPrice = item.revitemPrice + revItemFreightWithTax;

                    item.revitemPrice = parseFloat(item.revitemPrice);

                    var revUnitPriceCB = parseFloat(item.revUnitPriceCB);
                    var itemRevFreightChargesCB = parseFloat(item.itemRevFreightChargesCB);

                    item.revitemPriceCB = revUnitPriceCB * item.productQuantity;
                    //item.cGst = item.Gst / 2;
                    //item.sGst = item.Gst / 2;
                    //item.iGst = 0;
                    item.cGst = item.cGst;
                    item.sGst = item.sGst;
                    item.iGst = item.iGst;
                    item.revitemPriceCB = (item.revitemPriceCB + ((item.revitemPriceCB / 100) * (item.cGst + item.sGst + item.iGst)));
                    var revItemFreightWithTaxCB = 0;//(itemRevFreightChargesCB + ((itemRevFreightChargesCB / 100) * (item.itemFreightTAX)));
                    item.revitemPriceCB = item.revitemPriceCB + revItemFreightWithTaxCB;

                    item.revitemPriceCB = parseFloat(item.revitemPriceCB);

                    //#endregion Item Level Calculations
                })
            };

            $scope.getData(true);

            $scope.precisionRound = function (number, precision) {
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            }

            $scope.SaveCBPrices = function (vendorID, value) {
                $scope.bidComments = validateStringWithoutSpecialCharacters($scope.bidComments);
                if (value == 0) {
                    if ($scope.ReduceBidAmountBy > (($scope.CurrentPrice) - (($scope.reductionAmountConfigure / 100) * ($scope.CurrentPrice))) && $scope.showMaxReductionValidation) {
                        swal("Maximum Reduction Error!",
                            "You are reducing more than " + $scope.maxReductionPercentage + " % of current bid amount. The Maximum reduction amount should not exceed more than 30% from current bid amount  " +
                            $scope.CurrentPrice + ". Incase if You want to Reduce more Please Do it in Multiple Bids", "error");
                        $scope.getData(true);
                        return false;
                    }

                    swal({
                        title: "Are you sure?",
                        text: "Your prices will be updated to customer.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        var params = {
                            auctionVendor: $scope.auctionItemVendor.auctionVendors[0],
                            userID: userService.getUserId(),
                            isCustomer: 0,
                            sessionID: userService.getUserToken(),
                            bidComments: $scope.bidComments,
                            vendorID: vendorID,
                            quotation : $scope.bidAttachement
                        }
                        auctionsService.SaveCBPrices(params)
                            .then(function (response) {
                                if (response.errorMessage != "") {
                                    /*growlService.growl(response.errorMessage, "inverse");*/
                                    swal("Error!", response.errorMessage, 'error');
                                } else {
                                    $scope.bidComments = '';
                                    $scope.recalculate('SAVE_CB_PRICE_VENDOR', $scope.spVendorID, false);
                                    //growlService.growl('Saved Successfully', "inverse");
                                    swal("Your prices submitted to customer successfully");
                                }
                            });
                    })

                } else if (value == 1) {

                    swal({
                        title: "Are you sure?",
                        text: "You are confirming that this is the last price you would be offering to the customer for this requirement. No further updates are allowed.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        var params = {
                            auctionVendor: $scope.auctionItemVendor.auctionVendors[0],
                            userID: userService.getUserId(),
                            isCustomer: 0,
                            sessionID: userService.getUserToken(),
                            bidComments: $scope.bidComments,
                            vendorID: vendorID,
                            quotation: $scope.bidAttachement
                        }
                        auctionsService.SaveCBPrices(params)
                            .then(function (response) {
                                if (response.errorMessage != "") {
                                    //growlService.growl(response.errorMessage, "inverse");
                                    swal("Error!", response.errorMessage, 'error');
                                } else {
                                    $scope.bidComments = '';
                                    $scope.recalculate('SAVE_CB_PRICE_VENDOR', $scope.spVendorID, false);
                                    //growlService.growl('Saved Successfully', "inverse");
                                    swal("Your prices submitted to customer successfully");
                                    $scope.FreezeRegretCounterBid(1);
                                }
                            });
                    })
                }
            };

            $scope.FreezeCounterBid = function (freezeValue) {

                var params = {
                    reqID: $scope.spReqID,
                    vendorID: $scope.spVendorID,
                    sessionID: userService.getUserToken(),
                    freezedBy: 'VENDOR',
                    freezeValue: freezeValue,
                    bidComments: $scope.auctionItem.auctionVendors[0].otherProperties,
                    quotation: $scope.bidAttachement
                }

                swal({
                    title: "Are you sure?",
                    text: "You are confirming that this is the last price you would be offering to the customer for this requirement. No further updates are allowed.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    auctionsService.FreezeCounterBid(params)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                                $scope.recalculate('FREEZE_CB_CUSTOMER', $scope.spVendorID, false);
                                growlService.growl('Saved Successfully', "inverse");
                            }
                        });
                });
            };


            $scope.FreezeRegretCounterBid = function (freezeValue) {

                var params = {
                    reqID: $scope.spReqID,
                    vendorID: $scope.spVendorID,
                    sessionID: userService.getUserToken(),
                    freezedBy: 'VENDOR',
                    freezeValue: freezeValue,
                    bidComments: $scope.auctionItem.auctionVendors[0].otherProperties,
                    quotation: $scope.bidAttachement
                }

                //swal({
                //    title: "Are you sure?",
                //    text: "You are confirming that this is the last price you would be offering to the customer for this requirement. No further updates are allowed.",
                //    type: "warning",
                //    showCancelButton: true,
                //    confirmButtonColor: "#F44336",
                //    confirmButtonText: "OK",
                //    closeOnConfirm: true
                //}, function () {
                auctionsService.FreezeCounterBid(params)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            $scope.recalculate('FREEZE_CB_CUSTOMER', $scope.spVendorID, false);
                            growlService.growl('Saved Successfully', "inverse");
                        }
                    });
                // });
            };

            $scope.GetCBPricesAudit = function () {
                auctionsService.GetCBPricesAudit($scope.spReqID, $scope.spVendorID, userService.getUserToken())
                    .then(function (response) {
                        $scope.CBPricesAudit = response;
                        $scope.CBPricesAudit.forEach(function (bid, bidIndex) {
                            console.log(bid);
                            if (bid && bid.CREATED_BY > 0 && $scope.spVendorID > 0 && bid.CREATED_BY != $scope.spVendorID) {
                                if (bidIndex == 0) {
                                    $scope.cbOffered = true;
                                    $scope.cbOfferedMessage = 'A new counter prices has been offered.';
                                    $scope.cbOfferedObj = bid;
                                }
                                $scope.cbInitiated = true;
                            }
                        })
                    });
            };

            $scope.GetCBPricesAudit();

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

            var requirementHub;

            $scope.recalculate = function (subMethodName, receiverId, showswal) {

                if (subMethodName == null || subMethodName == '' || subMethodName == undefined) {
                    subMethodName = '';
                }

                if (receiverId >= 0) {

                } else {
                    receiverId = -1;
                }

                var params = {};
                params.reqID = $scope.spReqID;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                var parties = $scope.spReqID + "$" + userService.getUserId() + "$" + userService.getUserToken() + "$" + subMethodName + "$" + receiverId;
                $scope.invokeSignalR('CheckRequirement', parties);
            }

            $scope.checkConnection = function () {
                if (requirementHub) {
                    return requirementHub.getStatus();
                } else {
                    return 0;
                }
            };

            $scope.reconnectHub = function () {
                if (requirementHub) {
                    if (requirementHub.getStatus() == 0) {
                        requirementHub.reconnect();
                        return true;
                    }
                } else {
                    requirementHub = SignalRFactory('', signalRHubName);
                }
            }

            $scope.invokeSignalR = function (methodName, params, callback) {
                if ($scope.checkConnection() == 1) {
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                } else {
                    $scope.reconnectHub();
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                }
            };

            $log.info('trying to connect to service');
            requirementHub = SignalRFactory('', signalRHubName);

            let signalRGroupName = 'requirementGroup' + $scope.spReqID + '_' + $scope.userID;
            if ($scope.isCustomer) {
                signalRGroupName = 'requirementGroup' + $scope.spReqID + '_CUSTOMER';
            }
            $scope.invokeSignalR('joinGroup', signalRGroupName);

            $log.info('connected to service');

            $scope.$on("$destroy", function () {
                $log.info('disconecting signalR');
                requirementHub.stop();
                $log.info('disconected signalR');
            });

            requirementHub.on('checkRequirement', function (items) {

                var applySignalR = false;

                console.log('-----------------OUT-------------------->');

                if (items.inv.subMethodName == 'UPDATE_CB_TIME_CUSTOMER') {
                    location.reload();
                    return;
                }

                if (items.inv.methodName == 'CheckRequirement' &&
                    (items.inv.callerID == $scope.spVendorID ||
                        items.inv.receiverId == $scope.spVendorID ||
                        items.inv.receiverId == 0 ||
                        items.inv.subMethodName == 'SAVE_CB_PRICE_TO_ALL_CUSTOMER')) {
                    console.log('----------------IN--------------------->');
                    console.log(items.inv.methodName);
                    console.log(items.inv.receiverId);
                    applySignalR = true;

                }

                if (items.inv.subMethodName != 'CB_STOP_QUOTATIONS_CUSTOMER') {
                    $scope.getData(applySignalR);
                    $scope.GetCBPricesAudit();
                }

            });

            $scope.$on('timer-tick', function (event, args) {
                var temp = event.targetScope.countdown;
                if (event.targetScope.seconds == 5 ||
                    event.targetScope.seconds == 15 ||
                    event.targetScope.seconds == 25 ||
                    event.targetScope.seconds == 35 ||
                    event.targetScope.seconds == 45 ||
                    event.targetScope.seconds == 55) {
                    //auctionsService.CheckSystemDateTime();
                }

                if (event.targetScope.days == 0 &&
                    event.targetScope.hours == 0 &&
                    event.targetScope.minutes == 0 &&
                    (event.targetScope.seconds == 3 || event.targetScope.seconds == 2)) {

                    //code before the pause
                    setTimeout(function () {
                        //do what you need here
                        location.reload();
                    }, 2000);

                }

                if (event.targetScope.days > 0 ||
                    event.targetScope.hours > 0 ||
                    event.targetScope.minutes > 0 ||
                    event.targetScope.seconds > 1) {
                    $scope.cbStartedInputColour = {
                        'background-color': '#f5b2b2'
                    };
                }
                if (event.targetScope.countdown <= 120) {
                    $scope.timerStyle = {
                        'color': '#f00',
                        '-webkit - animation': 'flash linear 1s infinite',
                        'animation': 'flash linear 1s infinite'
                    };
                }

                if (event.targetScope.countdown > 120 && $scope.auctionItem.status == 'NOTSTARTED') {
                    $scope.timerStyle = { 'color': '#000' };
                }
                if (event.targetScope.countdown > 120 && $scope.auctionItem.status != 'NOTSTARTED') {
                    $scope.timerStyle = { 'color': '#228B22' };
                }

            });

            $scope.bidHistoryExpand = {
                'max-height': '250px',
                'overflow': 'hidden'
            };

            $scope.bidHistoryExpandCollapse = function (expand) {
                $scope.isBidHistoryExpand = expand;
                if (expand) {
                    $scope.bidHistoryExpand = {
                        'max-height': '5000px',
                        'overflow': 'hidden'
                    };
                } else {
                    $scope.bidHistoryExpand = {
                        'max-height': '250px',
                        'overflow': 'hidden'
                    };
                }
            };

            $scope.fieldValidation = function (data, applySignalR) {
                console.log(data);
                if ((data.customerID == userService.getUserId() || data.superUserID == userService.getUserId() || data.customerReqAccess)) {
                    $scope.userIsOwner = true;
                }

                data.auctionVendors[0].listRequirementItems.forEach(function (item, index) {
                    item.isEdit = true;
                });

                if (data.auctionVendors.length > 0) {
                    data.auctionVendors.forEach(function (vendor, index) {
                        if (vendor.INCO_TERMS && !$scope.vendorIncoTerms[vendor.INCO_TERMS]) {
                            auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                                .then(function (response) {
                                    $scope.vendorIncoTerms[vendor.INCO_TERMS] = response;
                                    validateIncoTerms(data, response);
                                    $scope.setAuctionInitializer(data, applySignalR);
                                });
                        } else {
                            validateIncoTerms(data, $scope.vendorIncoTerms[vendor.INCO_TERMS]);
                            $scope.setAuctionInitializer(data, applySignalR);
                        }
                    });
                }
            };

            $scope.getRequirementSettings = function () {
                $scope.selectedTemplate.TEMPLATE_NAME = 'PRM_DEFAULT';
                auctionsService.getRequirementSettings({ "reqid": $scope.spReqID, "sessionid": $scope.sessionid })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var template = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'TEMPLATE_ID';
                            });

                            if (template && template.length > 0) {
                                $scope.selectedTemplate.TEMPLATE_ID = template[0].REQ_SETTING_VALUE;
                            }

                            $scope.GetPRMTemplateFields();
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var params = {
                    "templateid": $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0,
                    "templatename": $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : '',
                    "sessionid": $scope.sessionid
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });
                });
            };

            function validateIncoTerms(requirementData, incoTerms) {
                incoTerms.forEach(function (incoItem, itemIndexs) {
                    requirementData.auctionVendors[0].listRequirementItems.forEach(function (item, index) {
                        if (item.isCoreProductCategory == 0 && item.catalogueItemID == incoItem.ProductId) {
                            if ($scope.userIsOwner && incoItem.IS_CUSTOMER_EDIT) {
                                item.isEdit = false;
                            } else if (!$scope.userIsOwner && incoItem.IS_VENDOR_EDIT) {
                                item.isEdit = false;
                            }
                        } else if (item.isCoreProductCategory == 1) {
                            item.isEdit = false;
                        }

                    });
                });
            }

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }

            $scope.totalAttachmentMaxSize = 2097152;

            $scope.bidAttachement = [];

            $scope.getFile = function () {
                $scope.progress = 0;
                $scope.totalSize = 0;

                $scope.multipleAttachments = $("#attachement")[0].files;
                $scope.multipleAttachments = Object.values($scope.multipleAttachments)

                if ($scope.multipleAttachments && $scope.multipleAttachments.length > 0) {
                    $scope.multipleAttachments.forEach(function (item, index) {
                        $scope.totalSize = $scope.totalSize + item.size;
                    });
                }
                if ($scope.totalSize > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 2MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }

                $scope.multipleAttachments.forEach(function (item, index) {
                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;
                            if (!$scope.multipleAttachmentsList) {
                                $scope.multipleAttachmentsList = [];
                            }
                            var ifExists = _.findIndex($scope.multipleAttachmentsList, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                            if (ifExists > -1) {
                            } else {
                                $scope.multipleAttachmentsList.push(fileUpload);
                                $scope.bidAttachement.push(fileUpload);
                            }
                        });
                });

            };

            $scope.removeAttach = function (index) {
                $scope.multipleAttachmentsList.splice(index, 1);
                $scope.bidAttachement.splice(index, 1);
                $scope.auctionItem.auctionVendors[0].multipleAttachmentsList.splice(index, 1);

                var fi = document.getElementById('attachement');
                if (fi.files.length > 0) {
                }
                if ($scope.multipleAttachmentsList.length == 0) {
                    //$(".fileinput").fileinput("clear");
                }

            };

        }]);