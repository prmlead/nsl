﻿
prmApp
    .controller('emailLogsCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "$stateParams", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, $stateParams, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {
          
            $scope.userId = userService.getUserId();
            $scope.userObj = {
                //logoURL: '../js/resources/images/chart.png'
            };

            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            this.userdetails = {};
            $scope.sessionId = userService.getUserToken();
            $scope.companyID = userService.getUserCompanyId();

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 8;
            $scope.itemsPerPage2 = 8;
            $scope.maxSize = 8;
            $scope.status = 'ALL';   
            $scope.reqStatus = 'ALL';   
            $scope.reqType = 'ALL';   
            $scope.requirement = 'ALL';   
            $scope.type = '';
            $scope.temprequirements = [];
            $scope.tempStatusFilter = [];
            $scope.tempReqTypeFilter = [];
            $scope.Search = [];

            $scope.activeUsers = [];
            $scope.activeBuyers = [];
            $scope.activeUsersOnline = [];
            $scope.GetUsersLoginStatus = [];

            //var allObj = {
            //    reqStatus: "ALL"
            //}
            //$scope.reqStatus = 0;
            //$scope.reqStatusIndex.push(allObj);

            $scope.dateFilter = {
                reportToDate: moment().format('YYYY-MM-DD'),
                reportFromDate: moment().subtract(30, "days").format("YYYY-MM-DD")
            };

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };

            $scope.pageChanged = function () {
                //$scope.getAuctions();
            };
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };

            $scope.EmailLogs = [];
            $scope.EmailLogsTemp = [];

            $scope.GetEmailLogs = function () {
                
                
                auctionsService.GetEmailLogs($scope.dateFilter.reportFromDate, $scope.dateFilter.reportToDate,$scope.companyID, $scope.sessionId)
                    .then(function (response) {
                       
                        $scope.EmailLogs = response;
                       

                        $scope.EmailLogs.forEach(function (item, itemIndex) {

                            item.messageDate = userService.toLocalDate(item.messageDate);
                            if (item.requirement == 'VENDOR_REGISTER' || item.requirement == 'COMMUNICATIONS' || item.requirement == 'USER_REGISTER') {

                                item.reqStatus = '--';
                                item.reqType = -1;

                            }
                        })

                        $scope.EmailLogs1 = $scope.EmailLogs;
                        $scope.EmailLogsTemp = $scope.EmailLogs;
                        $scope.tempStatusFilter = angular.copy($scope.EmailLogs1);
                        $scope.tempReqTypeFilter = angular.copy($scope.EmailLogs1);
                        $scope.temprequirements = angular.copy($scope.EmailLogs1);
                        $scope.totalItems = $scope.EmailLogs.length;
                    })

            }

            $scope.GetEmailLogs();


            
            $scope.getStatusFilter = function (reqStatus, requirementMsgtype) {

                $scope.filterArray = [];
                if (reqStatus == 'ALL' && requirementMsgtype == 'ALL') {
                    $scope.EmailLogs = $scope.EmailLogs1;
                    $scope.EmailLogsTemp = $scope.EmailLogs1;

                } else if (reqStatus == 'ALL' && requirementMsgtype != 'ALL') {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        if (reqStatus == 'ALL') {
                            return (item.requirement).toLowerCase() == (requirementMsgtype).toLowerCase();
                        }
                    });

                    $scope.EmailLogs = $scope.filterArray;
                    $scope.EmailLogsTemp = $scope.filterArray;

                } else if (reqStatus != 'ALL' && requirementMsgtype == 'ALL') {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        if (requirementMsgtype == 'ALL') {
                            return (item.reqStatus).toLowerCase() == (reqStatus).toLowerCase();
                        }
                    });

                    $scope.EmailLogs = $scope.filterArray;
                    $scope.EmailLogsTemp = $scope.filterArray;

                } else if (reqStatus != 'ALL' && requirementMsgtype != 'ALL') {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        if ((item.reqStatus).toLowerCase() == (reqStatus).toLowerCase() && (item.requirement).toLowerCase() == (requirementMsgtype).toLowerCase()) {
                            return item;
                        }
                    });

                    $scope.EmailLogs = $scope.filterArray;
                    $scope.EmailLogsTemp = $scope.filterArray;
                }
                $scope.totalItems = $scope.EmailLogs.length;
            }

            
            $scope.getRequirementFilter = function (filterVal) {

                $scope.filterArray = [];
                if (filterVal == 'ALL') {
                    $scope.EmailLogs = $scope.temprequirements;
                    $scope.EmailLogsTemp = $scope.temprequirements;

                } else {
                    $scope.filterArray = $scope.temprequirements.filter(function (item) {
                        return item.requirement == filterVal;
                    });
                    $scope.EmailLogs = $scope.filterArray;
                    $scope.EmailLogsTemp = $scope.filterArray;
                    $scope.Search = $scope.filterArray;
                }

                $scope.totalItems = $scope.EmailLogs.length;

            }

            
            $scope.searchTable = function (str) {

                str = String(str).toUpperCase();
                $scope.EmailLogs = $scope.EmailLogsTemp.filter(function (req) {

                    req.FullName = req.firstName.concat(' ' + req.lastName);

                    return (String(req.reqID).includes(str) == true || String(req.email).toUpperCase().includes(str) == true || String(req.institution).toUpperCase().includes(str) == true ||
                        String(req.FullName).toUpperCase().includes(str) == true || String(req.requirement).toUpperCase().includes(str) == true || String(req.altEmail).toUpperCase().includes(str) == true);
                });

                $scope.totalItems = $scope.EmailLogs.length;
            }
        

            $scope.getReport = function (name) {
                reportingService.GetLogsTemplates(name, $scope.dateFilter.reportFromDate, $scope.dateFilter.reportToDate, $scope.companyID, $scope.sessionId);
            }




        }]);