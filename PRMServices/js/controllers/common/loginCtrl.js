prmApp
    .controller('loginCtrl', ["$timeout", "$state", "$scope", "growlService", "userService", "authService", "auctionsService", "$http", "domain", "$rootScope", "fileReader", "ngDialog", "$filter", 'SettingService',
        function ($timeout, $state, $scope, growlService, userService, authService, auctionsService, $http, domain, $rootScope, fileReader, ngDialog, $filter, SettingService) {


            $rootScope.isCalanderLoadFirstTime = 0;


            //Status
            $scope.login = 1;
            $scope.register = 0;
            $scope.otp = 0;
            $scope.otpvalue = 0;
            $scope.verification = 0;
            $scope.verificationObj = {};
            $scope.otpobj = {};
            $scope.register_vendor = 0;
            $scope.forgot = 0;
            $scope.forgotpassword = {};
            $scope.loggedIn = userService.isLoggedIn();
            $scope.userError = {};
            $scope.user = {
                userLoginIdValidationError: '',
                isMultipleCompany: false,
                isSingleCompany: false,
                multipleLoginDetailsSelectedLogin: {},
                isCompanyCheckDone: false
            };

            $scope.multipleLoginDetails = [];
            $scope.checkEmailUniqueResult = false;
            $scope.checkPhoneUniqueResult = false;
            $scope.checkCompanyUniqueResult = false;
            $scope.checkPANUniqueResult = false;
            $scope.checkTINUniqueResult = false;
            $scope.checkSTNUniqueResult = false;
            $scope.vendorregisterobj = $scope.registerobj = {};
            $scope.otpvalueValidation = false;
            $scope.otpvalueValidationEmpty = false;
            $scope.otpvalueValidationError = false;
            $scope.categories = [];
            $scope.subcategories = [];
            $scope.categoriesdata = [];
            $scope.selectedSubcategories = [];
            $scope.selectedCurrency = {};
            $scope.selectedTimezone = {};
            $scope.currencies = [];
            $scope.timezones = [];

            $scope.isMobile = (typeof window.orientation !== 'undefined' ? true : false);

            $scope.registrationbtn = function () {
                $scope.register = 1;
                $scope.registerobj = {};
                $scope.otp = $scope.forgot = $scope.register_vendor = $scope.login = 0;
                $scope.checkEmailUniqueResult = false;
                $scope.checkPhoneUniqueResult = false;
                $scope.checkCompanyUniqueResult = false;
                $scope.checkPANUniqueResult = false;
                $scope.checkTINUniqueResult = false;
                $scope.checkSTNUniqueResult = false;
            };
            $scope.loginbtn = function () {
                $scope.login = 1;
                $scope.user = {};
                $scope.otp = $scope.forgot = $scope.register_vendor = $scope.register = 0;
            };
            $scope.forgotbtn = function () {
                $scope.otp = $scope.login = $scope.register_vendor = $scope.register = 0;
                $scope.forgot = 1;
            };

            $scope.sendOTPagain = function () {
                userService.resendotp(userService.getUserId());
            };
            $scope.vendorregistrationbtn = function () {
                $scope.otp = $scope.login = $scope.forgot = $scope.register = 0;
                $scope.vendorregisterobj = { role: 'VENDOR', 'isSelf': true};
                $scope.register_vendor = 1;
                $scope.checkEmailUniqueResult = false;
                $scope.checkPhoneUniqueResult = false;
                $scope.checkCompanyUniqueResult = false;
                $scope.checkPANUniqueResult = false;
                $scope.checkTINUniqueResult = false;
                $scope.checkSTNUniqueResult = false;
            };


            $scope.PhoneValidate = function () {
                //console.log("siva  1111111---->");
                var phoneno = /^\d{10}$/;
                var input = $scope.registerobj.phoneNum;
                if (input = phoneno) {
                    //console.log("siva  2222222---->");
                    return true
                    //console.log("siva  2222222---->");

                }

            };

            function validate(evt) {
                var theEvent = evt || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[0-9]|\./;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            }


            $scope.PhoneValidate1 = function () {
                if ($scope.vendorregisterobj.phoneNum != "" && isNaN($scope.vendorregisterobj.phoneNum)) {
                    return false;
                }
            };


            $scope.EmailValidate = function () {
                var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
                var result = re.test($scope.registerobj.email);
                if (!result) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
                } else {
                    $scope.showMessage = false;
                    $scope.msg = '';
                }
            }


            $scope.EmailValidateVendor = function () {
                var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
                var result = re.test($scope.vendorregisterobj.email);
                if (!result) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
                } else {
                    $scope.showMessage = false;
                    $scope.msg = '';
                }
            };


            $scope.userregistration = function () {
                var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
                var result = re.test($scope.registerobj.email);
                if (isNaN($scope.registerobj.phoneNum)) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
                } else if (!result) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
                } else {
                    $scope.showMessage = false;
                    $scope.msg = '';
                }
                if (!$scope.showMessage) {
                    $scope.registerobj.username = $scope.registerobj.email;

                    var sCurrency = $("#currency").val();
                    var sCurrency = $("#currency").val();

                    $scope.registerobj.currency = $scope.selectedCurrency.key;
                    $scope.registerobj.timeZone = $scope.selectedTimezone.key;
                    userService.userregistration($scope.registerobj).then(function (error) {
                        $scope.loggedIn = userService.isLoggedIn();
                        if (error.errorMessage != "") {
                            $scope.showMessage = true;
                            $scope.msg = $scope.getErrMsg(error);
                        } else {
                            $scope.otp = 1;
                            $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                        }
                    });
                }
            };

            $scope.getCategories = function () {
                auctionsService.getCategories()
                    .then(function (response) {
                        $scope.categories = _.uniq(_.map(response, 'category'));
                        //$scope.subcategories = response;
                        $scope.categoriesdata = response;

                    });
            };

            $scope.getKeyValuePairs = function (parameter) {

                auctionsService.getKeyValuePairs(parameter)
                    .then(function (response) {
                        if (parameter == "CURRENCY") {
                            $scope.currencies = response;

                        } else if (parameter == "TIMEZONES") {
                            $scope.timezones = response;

                        }

                        $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                        $scope.selectedCurrency = $scope.selectedCurrency[0];
                        $scope.selectedTimezone = $filter('filter')($scope.timezones, { value: response.timeZone });
                        $scope.selectedTimezone = $scope.selectedTimezone[0];
                    })

            }

            $scope.getKeyValuePairs('CURRENCY');
            $scope.getKeyValuePairs('TIMEZONES');

            $scope.loadSubCategories = function () {
                $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.vendorregisterobj.category });
                /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
            }

            $scope.getCategories();

            $scope.docsVerification = function () {
                var params = { 'userID': userService.getUserId(), 'files': $scope.CredentialUpload, 'sessionID': userService.getUserToken() };
                $http({
                    method: 'POST',
                    url: domain + 'updatecredentials',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    //console.log(response);
                    if (response && response.data && response.data.errorMessage == "") {
                        $state.go('pages.profile.profile-about');
                        growlService.growl('Welcome to PRM360! Your credentials are being verified. Our associates will contact you as soon as it is done.', 'inverse');
                    } else {
                        //console.log(response.data[0].errorMessage);
                    }
                }, function (result) {
                    //console.log("there is no current auctions");
                });
            }

            $scope.verifyOTP = function () {
                $scope.otpvalue = $scope.otpobj.otp;
                $scope.otpvalue.phone = $scope.registerobj.phoneNum;
                if ($scope.otpvalue == "") {
                    $scope.otpvalueValidation = true;
                    $scope.otpvalueValidationEmpty = true;
                } else {
                    $scope.otpvalueValidationEmpty = false;
                    $scope.otpvalueValidation = false;
                }
                if (isNaN($scope.otpvalue)) {
                    $scope.otpvalueValidationError = true;
                    $scope.otpvalueValidation = true;
                } else {
                    $scope.otpvalueValidationError = false;
                    $scope.otpvalueValidation = false;
                }
                if (!$scope.otpvalueValidation) {
                    userService.verifyOTP($scope.otpvalue)
                        .then(function (response) {
                            if (response.errorMessage == "") {
                                if (response.userInfo.isOTPVerified == 1) {
                                    $scope.isOTPVerified = 1;

                                    swal("Done!", "Mobile OTP Verified successfully.", "success");
                                } else {
                                    $scope.isOTPVerified = 0;
                                    swal("Warning", "Please enter valid OTP", "warning");
                                }
                            } else {
                                swal("Warning", response.errorMessage, "warning");
                            }
                        });
                }
            };

            $scope.getFile1 = function (id, doctype, ext) {
                $scope.progress = 0;
                $scope.file = $("#" + id)[0].files[0];
                $scope.docType = doctype + "." + ext;
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        var bytearray = new Uint8Array(result);
                        var fileobj = {};
                        fileobj.fileStream = $.makeArray(bytearray);
                        fileobj.fileType = $scope.docType;
                        fileobj.isVerified = 0;
                        //$scope.verificationObj.attachmentName=$scope.file.name;
                        $scope.CredentialUpload.push(fileobj);
                    });
                ////console.log($scope.CredentialUpload);
            };

            $scope.ischangePhoneNumber = 0;

            $scope.changePhoneNumber = function () {
                $scope.newphonenumber = $("#newphonenumber").val();
                //console.log($scope.newphonenumber);
                if ($scope.newphonenumber == "") {
                    $scope.newphonenumber_errors = true;
                    $scope.newphonenumber_required_error = true;
                    return false;
                } else {
                    $scope.newphonenumber_errors = false;
                    $scope.newphonenumber_required_error = false;
                }
                if (isNaN($scope.newphonenumber)) {
                    $scope.newphonenumber_errors = true;
                    $scope.newphonenumber_validation_error = true;
                    return false;
                } else {
                    $scope.newphonenumber_errors = false;
                    $scope.newphonenumber_validation_error = false;
                }
                if (!$scope.newphonenumber_errors) {
                    var userinfo = userService.getUserObj();
                    userinfo.subcategories = [];
                    userinfo.phoneNum = $scope.newphonenumber;
                    userinfo.sessionID = userService.getUserToken();
                    userinfo.emailAddress = userinfo.email;
                    userinfo.aboutUs = "";
                    userinfo.companyName = "";
                    userinfo.logoFile = { "fileName": '', 'fileStream': "" };
                    userinfo.ischangePhoneNumber = 1;
                    userService.updateUser(userinfo)
                        .then(function (response) {
                            if (response == '') {
                                $scope.otp = 0;
                                $scope.login = 1;
                            }
                        });
                    $scope.changepasswordstatus = false;
                }
            };

            $scope.vendorregistration = function () {
                if (isNaN($scope.vendorregisterobj.phoneNum)) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
                } else {
                    $scope.showMessage = false;
                    $scope.msg = '';
                }
                $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;
                userService.vendorregistration($scope.vendorregisterobj).then(function (error) {
                    $scope.loggedIn = userService.isLoggedIn();
                    /*if (error) {
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    }else{
                       $scope.otp =1;
                       $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                    }*/
                    if (error.errorMessage != "") {
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    } else {
                        $scope.otp = 1;
                        $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                    }
                });
            };

            $scope.vendorregisterobj.panno = '';
            $scope.vendorregisterobj.vatno = '';
            $scope.vendorregisterobj.taxno = '';

            $scope.RegisterVendor = function () {
                //console.log($scope.vendorregisterobj);
                if (isNaN($scope.vendorregisterobj.phoneNum)) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
                } else {
                    $scope.showMessage = false;
                    $scope.msg = '';
                }
                if (!$scope.showMessage) {
                    $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;
                    userService.vendorregistration($scope.vendorregisterobj).then(function (error) {
                        $scope.loggedIn = userService.isLoggedIn();
                        if (error.errorMessage != "") {
                            $scope.showMessage = true;
                            $scope.msg = $scope.getErrMsg(error);
                        } else {
                            $scope.otp = 1;
                            $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                        }
                    });
                }
            };

            $scope.sub = {
                selectedSubcategories: []
            };

            $scope.vendorregisterobj.panno = '';
            $scope.vendorregisterobj.vatno = '';
            $scope.vendorregisterobj.taxno = '';


            $scope.RegistrationAdditonalField = [];

            $scope.getRegAddtionalFields = function () {
                SettingService.getRegistrationFields().then(function (response) {
                    if (response != null && response.length > 0)
                        $scope.RegistrationAdditonalField = response
                });
            };

            $scope.Attachements = [];
            $scope.onFileSelect = function ($files, $item, $modal) {

                var obj = {
                    Field: $item.Name,
                    Files: []
                }
                $scope.Attachements.push()
                for (var i in $files) {
                    fileReader.readAsDataUrl($files[i], $scope)
                        .then(function (result) {
                            var bytearray = new Uint8Array(result);
                            var fileobj = {};
                            fileobj.fileStream = $.makeArray(bytearray);
                            fileobj.fileType = $files[i].type;
                            fileobj.name = $files[i].name
                            fileobj.isVerified = 0;
                            //$scope.verificationObj.attachmentName=$scope.file.name;
                            obj.Files.push(fileobj);
                        });
                }
                $scope.Attachements.push(obj)


            }
            $scope.RegisterVendor1 = function (form) {

                //if (!form.$valid) {
                //    return false;
                //}

                //console.log("select sub categories" ,$scope.sub.selectedSubcategories);
                //console.log($scope.vendorregisterobj);
                if (isNaN($scope.vendorregisterobj.phoneNum)) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
                } else {
                    $scope.showMessage = false;
                    $scope.msg = '';
                }
                if (!$scope.showMessage) {
                    $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;

                    $scope.vendorregisterobj.currency = $scope.vendorregisterobj.currency.key;
                    $scope.vendorregisterobj.timeZone = $scope.vendorregisterobj.timeZone.key;
                    $scope.vendorregisterobj.attachments = $scope.Attachements;
                    //console.log($scope.selectedCurrency.key + "Currency Key");
                    //console.log($scope.selectedTimezone.key + "TimeZone Key");
                    $scope.vendorregisterobj.subcategories = _.map($scope.sub.selectedSubcategories, 'id');
                    if (!$scope.vendorregisterobj.subcategories) {
                        $scope.vendorregisterobj.subcategories = [];
                    }
                    if ($scope.vendorregisterobj.vatno == null) {
                        $scope.vendorregisterobj.vatno = '';
                    }
                    userService.vendorregistration1($scope.vendorregisterobj).then(function (error) {
                        $scope.loggedIn = userService.isLoggedIn();
                        if (error.errorMessage != "") {
                            $scope.showMessage = true;
                            $scope.msg = $scope.getErrMsg(error);
                        } else {
                            $scope.otp = 1;
                            $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                        }
                    });
                }
            }



            $scope.checkUserUniqueResult = function (idtype, inputvalue) {
                if (inputvalue == "" || inputvalue == undefined) {
                    return false;
                }
                $scope.checkPhoneUniqueResult = false;
                $scope.checkEmailUniqueResult = false;
                $scope.checkCompanyUniqueResult = false;
                $scope.checkPANUniqueResult = false;
                $scope.checkTINUniqueResult = false;
                $scope.checkSTNUniqueResult = false;
                userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                    if (idtype == "PHONE") {
                        $scope.checkPhoneUniqueResult = !response;
                    } else if (idtype == "EMAIL") {
                        $scope.checkEmailUniqueResult = !response;
                    } else if (idtype == "COMPANY") {
                        $scope.checkCompanyUniqueResult = !response;
                    }
                    else if (idtype == "PAN") {
                        $scope.checkPANUniqueResult = !response;
                    }
                    else if (idtype == "TIN") {
                        $scope.checkTINUniqueResult = !response;
                    }
                    else if (idtype == "STN") {
                        $scope.checkSTNUniqueResult = !response;
                    }
                });
            };
            $scope.forgotpasswordfunction = function () {
                userService.forgotpassword($scope.forgotpassword)
                    .then(function (response) {
                        if (response.data.errorMessage == "") {
                            $scope.forgotpassword = {};
                            swal("Done!", "Password reset link sent to your registerd Email.", "success");
                            $scope.login = 1;
                            $scope.user = {};
                            $scope.forgot = $scope.register_vendor = $scope.register = 0;
                        } else {
                            swal("Warning", "Please check the Email/Phone you have entered.", "warning");
                        }
                    });
            };



            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsConditions.html', width: 1000, height: 500 });
            };


            $scope.closeMsg = function () {
                $scope.showMessage = false;
            };

            $scope.changePassword = false;

            $scope.loginSubmit = function () {
                $scope.user.username = $scope.user.userLoginId;
                $scope.user.password = $scope.user.userLoginPassword;
                var source = "WEB";
                if (window.location.href && window.location.href.toLowerCase().indexOf('logincontainer') >= 0) {
                    source = "DESKTOP_CONTAINER";
                }

                $scope.LoginUserMethod("", source);
            };


            $scope.ispassword = false;
            $scope.checkpassword = function () {
                /*Change Mobile Number from 10-15 digits*/
                if ($scope.vendorregisterobj && $scope.vendorregisterobj.phoneNum && $scope.vendorregisterobj.password &&
                    $scope.vendorregisterobj.phoneNum == $scope.vendorregisterobj.password) {
                    $scope.ispassword = true;
                    //$scope.vendorregisterobj.password = '';
                    swal("Warning!", 'Phone No. and Password must not be same');
                }
                else {
                    $scope.ispassword = false;
                }
            }






            $scope.ResetPasswordByOTP = function (val) {
                auctionsService.ResetPasswordByOTP(val)
                    .then(function (response) {
                        $scope.resertResponse = response;
                        if ($scope.resertResponse.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            swal({
                                title: "Thanks!",
                                text: "Password Sent Successfully to Registered Email ID",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        }
                    })
            }


            $scope.disableGetOtp = false;

            $scope.countGetOtp = 0;

            $scope.getOTP = function (phone) {

                $scope.otp = '';
                //$scope.passCode = '';
                //$scope.confirmPassCode = '';

                $scope.countGetOtp = $scope.countGetOtp + 1;

                clearTimeout($scope.timerId);
                $scope.timerId = setInterval(countdown, 1000);
                $scope.disableGetOtp = true;

                auctionsService.getOTP(phone)
                    .then(function (response) {
                        $scope.resertResponse = response;
                        if ($scope.resertResponse.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            if ($scope.countGetOtp <= 1) {
                                $scope.disableGetOtp = false;
                            }

                            clearTimeout($scope.timerId);
                        }
                        else {
                            growlService.growl('OTP sent to ' + phone, "success");
                            setTimeout(function () {
                                $scope.$apply(function () {
                                    if ($scope.countGetOtp <= 1) {
                                        $scope.disableGetOtp = false;
                                    }
                                    clearTimeout($scope.timerId);
                                });
                            }, 60100);
                        }
                    });
            };

            $scope.timeLeft = 60;
            $scope.elem = document.getElementById('timer_div');

            function countdown() {
                if ($scope.timeLeft == -1) {
                    clearTimeout($scope.timerId);
                    // doSomething();
                } else {
                    $scope.elem.innerHTML = $scope.timeLeft + ' seconds remaining';
                    $scope.timeLeft--;
                }
            }

            $scope.otp = '';
            $scope.passCode = '';
            $scope.confirmPassCode = '';

            $scope.UpdatePassword = function () {

                if ($scope.disableChangePass == true) {
                    return;
                }

                var params = {
                    otp: $scope.otp,
                    passCode: $scope.passCode,
                    phone: $scope.user.username
                };

                auctionsService.UpdatePassword(params)
                    .then(function (response) {
                        $scope.changePasswordResponse = response;

                        if (response.objectID > 0) {
                            $scope.otp = '';
                            $scope.passCode = '';
                            $scope.confirmPassCode = '';
                            growlService.growl('Saved Successfully', "success");
                        }
                        else {
                            $scope.otp = '';
                            $scope.passCode = '';
                            $scope.confirmPassCode = '';
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            };

            $scope.disableChangePass = true;
            $scope.disableChangePassMsg = '';

            $scope.validateChangePass = function () {

                $scope.disableChangePass = false;
                $scope.disableChangePassMsg = '';

                if (!$scope.user || !$scope.user == undefined || !$scope.user.username || $scope.user.username == undefined || $scope.user.username == null || $scope.user.username == undefined) {
                    $scope.disableChangePass = true;
                    $scope.disableChangePassMsg = 'Please enter valid Phone';
                    return;
                }
                else if (!$scope.otp || $scope.otp == undefined || $scope.otp == null || $scope.otp == '' || $scope.otp.toString().trim().length != 6) {
                    $scope.disableChangePass = true;
                    $scope.disableChangePassMsg = 'OTP should be 6 digits.';
                    return;
                }
                else if (!$scope.passCode || $scope.passCode == undefined || $scope.passCode == null || $scope.passCode == '') {
                    $scope.disableChangePass = true;
                    $scope.disableChangePassMsg = 'Please enter Password.';
                    return;
                }
                else if ($scope.passCode.toString().trim().length < 6) {
                    $scope.disableChangePass = true;
                    $scope.disableChangePassMsg = 'Password length should be min 6.';
                    return;
                }
                else if ($scope.user.username == $scope.passCode.trim()) {
                    $scope.disableChangePass = true;
                    $scope.disableChangePassMsg = 'Phone and Password can not be same.';
                    return;
                }
                else if (!$scope.confirmPassCode || $scope.confirmPassCode == undefined || $scope.confirmPassCode == null || $scope.confirmPassCode == '' || $scope.confirmPassCode.toString().trim().length < 6) {
                    $scope.disableChangePass = true;
                    $scope.disableChangePassMsg = 'Please enter Confirm Password.';
                    return;
                }
                else if ($scope.passCode.trim() != $scope.confirmPassCode.trim()) {
                    $scope.disableChangePass = true;
                    $scope.disableChangePassMsg = 'Password does not match the confirm password.';
                    return;
                }

            };


            $scope.LoginUserMethod = function (userHashToken, source, ssoType) {
                $scope.user.source = source;
                $scope.user.userhashtoken = userHashToken;
                $scope.user.ssotoken = userHashToken;
                $scope.user.ssoType = ssoType;
                localStorage.removeItem('ssoaduser');
                localStorage.removeItem('ssojwttoken');
                if ($scope.user && $scope.user.multipleLoginDetailsSelectedLogin) {
                    $scope.user.selecteduid = $scope.user.multipleLoginDetailsSelectedLogin.superUserID ? $scope.user.multipleLoginDetailsSelectedLogin.superUserID : 0;
                    $scope.user.selectedcompid = $scope.user.multipleLoginDetailsSelectedLogin.companyID ? $scope.user.multipleLoginDetailsSelectedLogin.companyID : 0;
                }
                
                userService.login($scope.user).then(function (error) {
                    $scope.loggedIn = userService.isLoggedIn();
                    if (error === "Username(Phone number) and Password are same, kindly please change password." ||
                        error.errorMessage === "Username(Phone number) and Password are same, kindly please change password.") {
                        document.getElementById("ChangePasswordPopUpButton").click();
                        $scope.changePassword = true;
                        return;
                    }

                    if (error.errorMessage) {
                        error = 'Please Enter the correct credentials.';
                        $scope.showMessage = true;
                        $scope.msg = $scope.getErrMsg(error);
                    } else if (error.userInfo.credentialsVerified == 0 || error.userInfo.isOTPVerified == 0) {
                        $state.go('pages.profile.profile-about');
                    }
                });
            };

            var userHashToken = userService.getUserHashToken();
            if (window.location.href && window.location.href.toLowerCase().indexOf('logincontainer') >= 0 && userHashToken) {
                $scope.LoginUserMethod(userHashToken, "DESKTOP_CONTAINER", 'CONTAINER');
            }

            console.log(localStorage.getItem('ssojwttoken'));
            let ssoJWTToken = localStorage.getItem('ssojwttoken');
            if (ssoJWTToken) {
                $scope.LoginUserMethod(ssoJWTToken, "WEB", 'JWT');
            }

            let ssoADUser = localStorage.getItem('ssoaduser');
            if (ssoADUser) {
                $scope.LoginUserMethod(ssoADUser, "WEB", 'AD');
            }

            $scope.togglePassword = function () {
                var x = document.getElementById("exampleInputPassword1");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            };

            $scope.validateUserLogin = function () {
                $scope.user.username = '';
                $scope.user.userLoginIdValidationError = '';
                $scope.user.isMultipleCompany = false;
                $scope.user.isSingleCompany = false;
                $scope.user.isCompanyCheckDone = false;
                $scope.multipleLoginDetails = [];
                if ($scope.user && $scope.user.userLoginId && $scope.user.userLoginPassword) {
                    userService.validateUserLogin({ 'userLoginId': $scope.user.userLoginId }).then(function (response) {
                        if (response && response.length > 1) {
                            $scope.user.isCompanyCheckDone = true;
                            $scope.user.isMultipleCompany = true;
                            $scope.multipleLoginDetails = response;
                            //$scope.multipleLoginDetails.push({ 'companyID': 0, 'companyName': 'Select Company', 'superUserID': 0 });
                        } else if (response && response.length === 1) {
                            $scope.user.isCompanyCheckDone = true;
                            $scope.user.username = $scope.user.userLoginId;
                            $scope.user.password = $scope.user.userLoginPassword;
                            $scope.user.isSingleCompany = true;
                            $scope.LoginUserMethod('', 'WEB');
                        } else {
                            $scope.user.userLoginIdValidationError = 'Invalid Login-id.';
                        }
                    });
                } else {
                    $scope.user.userLoginIdValidationError = 'Invalid Login-id or Password.';
                }
            };

            $scope.selectMultipleLoginDetailsLogin = function () {
                if ($scope.user.multipleLoginDetailsSelectedLogin) {
                    $scope.user.username = $scope.user.userLoginId;
                    $scope.user.password = $scope.user.userLoginPassword;
                }
            };
        }]);