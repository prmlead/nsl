﻿//(function () { 
prmApp.controller('productEditCtrl', ['$scope', '$state', '$window', '$stateParams', '$filter', 'auctionsService', 'catalogService', 'userService', 'growlService', 'fileReader',
    'PRMPRServices', '$location',
    function ($scope, $state, $window, $stateParams, $filter, auctionsService, catalogService, userService, growlService, fileReader, PRMPRServices, $location) {

        $scope.productId = !$stateParams.productId ? 0 : $stateParams.productId;
        $scope.viewDetails = $stateParams.viewId;        
        $scope.compId = userService.getUserCompanyId();
        $scope.sessionid = userService.getUserToken();
        $scope.vendorsLoading = false;
        $scope.SelectedVendorsCount = 0;
        $scope.companyItemUnits = [];
        $scope.stateDetails = {
            plantList: [],
            poTemplate: ''
        };
        

        $scope.currentLoggedInUser = userService.getUserId();
        $scope.InitProdEdit = function () {
            $scope.checkProdEdit = 0;
            $scope.productEditObj = {
                prodId: $scope.productId,
                compId: $scope.compId,
                isValid: 0,
                ModifiedBy: userService.getUserId()
            };
            var param = {
                reqProduct: $scope.productEditObj,
                sessionID: userService.getUserToken()
            };

            catalogService.isProdEditAllowed(param)
                .then(function (response) {
                    if ($scope.viewDetails === "view") {
                        $scope.checkProdEdit = -1;
                    } else {
                        $scope.checkProdEdit = response.repsonseId;
                    }
                    $scope.getProductDetails();
                    $scope.getcategories();
                });
        };
        
        $scope.selectedProductContract = {
            isDisabled: false,
            isNew: true
        };

        $scope.selectedProductContractForPO = { };

        auctionsService.GetCompanyConfiguration($scope.compId, "ITEM_UNITS", userService.getUserToken())
            .then(function (unitResponse) {
                $scope.companyItemUnits = unitResponse;
            });

        $scope.InitProdEdit();
        $scope.VendorsTemp1 = [];
        $scope.vendorsList = [];
        $scope.vendorsListProduct = [];
        $scope.GetDateconverted = function (dateBefore) {
            if (dateBefore) {
                return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
            }
        };

        $scope.productDetails = null;
        $scope.getProductDetails = function () {
            catalogService.getproductbyid($scope.compId, $scope.productId)
                .then(function (response) {
                    $scope.getProductContracts();
                    $scope.productDetails = {
                        prodId: response.prodId,
                        prodName: response.prodName,
                        prodCode: response.prodCode,
                        prodDesc: response.prodDesc,
                        prodHSNCode: response.prodHSNCode,
                        prodQty: response.prodQty,
                        prodNo: response.prodNo,
                        dateModified: $scope.GetDateconverted(response.dateModified)
                    };

                    $scope.prodEditDetails = response;
                    $scope.prodEditDetails.VERTICAL_TYPE_TEMP = $scope.prodEditDetails.VERTICAL_TYPE;
                    if ($scope.prodEditDetails.isCommonItem == 1) {
                        $scope.prodEditDetails.isCommonItem = 'true';
                        $scope.prodEditDetails.isCommonItemTemp = 'true';
                    } else {
                        $scope.prodEditDetails.isCommonItem = 'false';
                        $scope.prodEditDetails.isCommonItemTemp = 'false';
                    }
                    $scope.prodEditDetails.isBlink = false;
                    

                    $scope.prodEditDetails.multipleAttachments = [];
                    var temp = response.itemAttachments;
                    if (!temp) {
                        var attchArray = temp.split(',');
                    }

                    if (attchArray) {
                        attchArray.forEach(function (att, index) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: att ? att : 0
                            };

                            $scope.prodEditDetails.multipleAttachments.push(fileUpload);
                        });
                    }
                    $scope.GetOtherInfo();
                });
        };
     
        $scope.editProductCotractInfo = function (contract) {
            $scope.selectedProductContract = contract;
            $scope.selectedProductContract.isNew = false;

            let vendor = {
                vendorId: contract.U_ID,
                selectedVendorCode: contract.selectedVendorCode
            };


            $scope.selectedProductContract.vendor = vendor;
            $scope.getVendorCodes(vendor);
        };

        $scope.deleteProductCotractInfo = function (contract) {
            contract.isValid = 0;
            contract.startTime = null;
            contract.endTime = null;
            var params = {
                "contract": contract
            };

            catalogService.SaveProductContract(params)
                .then(function (response) {
                    if (response) {
                        growlService.growl("Saved Successfully.", "success");
                        $scope.selectedProductContract = {
                            isDisabled: false,
                            isNew: true
                        };
                        $scope.getProductContracts();
                    }
                });
        };

        $scope.addProductContractInfo = function () {
            $scope.showContractSaveValidationError = '';
            $scope.selectedProductContract = {
                isDisabled: false,
                isNew: true
            };
        };

        $scope.SaveProductContractInfo = function () {
            $scope.showContractSaveValidationError = '';
            if (!($scope.selectedProductContract.number && $scope.selectedProductContract.value &&
                $scope.selectedProductContract.startTime && $scope.selectedProductContract.endTime && $scope.selectedProductContract.companyName
                && $scope.selectedProductContract.selectedVendorCode)) {
                $scope.showContractSaveValidationError = 'Please fill in required fields marked *';
            } else {
                let startDate = userService.toUTCTicks($scope.selectedProductContract.startTime);
                let startDateMoment = moment(startDate);

                let endDate = userService.toUTCTicks($scope.selectedProductContract.endTime);
                let endDateMoment = moment(endDate);
                if (endDate < startDate) {
                    $scope.showContractSaveValidationError = 'Start Date cannot be Greater than End Date.';
                }

                //if (!$scope.showContractSaveValidationError && $scope.selectedProductContract.availedQuantity > $scope.selectedProductContract.quantity) {
                //    $scope.showContractSaveValidationError = 'Availed Quantity cannot be greater than Quantity.';
                //}
            };
            if ($scope.selectedProductContract.quantity < $scope.selectedProductContract.availedQuantity) {
                $scope.showContractSaveValidationError = "Quantity should be greater then Availed Quantity";
                $scope.selectedProductContract.quantity = 0;
                $scope.selectedProductContract.availedQuantity = 0;
                return;
            }

            if (!$scope.showContractSaveValidationError) {
                if ($scope.selectedProductContract.startTime) {
                    let ts = userService.toUTCTicks($scope.selectedProductContract.startTime);
                    let m = moment(ts);
                    let contractStartDate = new Date(m);
                    let milliseconds = parseInt(contractStartDate.getTime() / 1000.0);
                    $scope.selectedProductContract.startTime = "/Date(" + milliseconds + "000+0530)/";
                } else {
                    $scope.selectedProductContract.startTime = null;
                }

                if ($scope.selectedProductContract.endTime) {
                    let ts = userService.toUTCTicks($scope.selectedProductContract.endTime);
                    let m = moment(ts);
                    let contractEndDate = new Date(m);
                    let milliseconds = parseInt(contractEndDate.getTime() / 1000.0);
                    $scope.selectedProductContract.endTime = "/Date(" + milliseconds + "000+0530)/";
                } else {
                    $scope.selectedProductContract.endTime = null;
                }

                $scope.selectedProductContract.ProductId = $scope.productId;
                $scope.selectedProductContract.vendorCode = $scope.selectedProductContract.selectedVendorCode;
                $scope.selectedProductContract.plantCode = ''//$scope.selectedProductContract.plantObj.id;
                $scope.selectedProductContract.plantName = '';//$scope.selectedProductContract.plantObj.name;
                $scope.selectedProductContract.user = $scope.currentLoggedInUser;
                $scope.selectedProductContract.isValid = 1;
                $scope.isValid = 1;
                var params = {
                    "contract": $scope.selectedProductContract
                };

                catalogService.SaveProductContract(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.selectedProductContract = {
                                isDisabled: false,
                                isNew: true
                            };
                            $scope.getProductContracts();
                        }
                    });
            }
        };

        $scope.getProductContracts = function () {
            if ($scope.productId) {
                catalogService.GetProductContracts($scope.productId, true)
                    .then(function (response) {
                        if (response) {
                            $scope.prodEditDetails.contractManagement = response;
                            if ($scope.prodEditDetails.contractManagement && $scope.prodEditDetails.contractManagement.length > 0) {
                                $scope.prodEditDetails.contractManagement.forEach(function (contract) {
                                    contract.startTime = userService.toLocalDate(contract.startTime);
                                    contract.endTime = userService.toLocalDate(contract.endTime);
                                });
                            }
                        }
                    });
            }
        };

        $scope.addCatalogue = function () {
            $window.scrollBy(0, 50);
            $scope.itemnumber = 0; //$scope.prodEditDetails.contractManagement.length;
            $scope.contractDetailsList =
            {
                pcId: 0,
                number: 0,
                value: 0,
                quantity: 0,
                availedQuantity: 0,
                startTime: '',
                endTime: '',
                U_ID: '',
                companyName: '',
                documents: '',
                isValid: 1,
                isOld: 0,
                errorColorNum: '1px solid #e4e7ea',
                errorColorVal: '1px solid #e4e7ea',
                errorColorQty: '1px solid #e4e7ea',
                errorColorSt: '1px solid #e4e7ea',
                errorColorNme: '1px solid #e4e7ea'
            };

            //$scope.prodEditDetails.contractManagement.push($scope.contractDetailsList);
        };

        $scope.contractIndexValue = 0;
        $scope.contractIndex = function (value) {
            $scope.contractIndexValue = value;
        };

        $scope.prodContractVendorSelected = function (vendor) {
            //vendor.userID, vendor.companyName
            $scope.selectedProductContract.companyName = vendor.companyName;
            $scope.selectedProductContract.vendorId = vendor.userID;
            $scope.selectedProductContract.selectedVendorCode = vendor.vendorCode;
            $scope.selectedProductContract.vendor = vendor;
            $scope.selectedProductContract.vendor.vendorId = vendor.userID;
            $scope.getVendorCodes($scope.selectedProductContract.vendor);
        };

        $scope.Attachements = [];
        $scope.onFileSelect = function ($files, $item, $modal) {

            var obj = {
                Field: $item.Name,
                Files: []
            };

            $scope.Attachements.push();
            for (var i in $files) {
                fileReader.readAsDataUrl($files[i], $scope)
                    .then(function (result) {
                        var bytearray = new Uint8Array(result);
                        var fileobj = {};
                        fileobj.fileStream = $.makeArray(bytearray);
                        fileobj.fileType = $files[i].type;
                        fileobj.name = $files[i].name
                        fileobj.isVerified = 0;
                        //$scope.verificationObj.attachmentName=$scope.file.name;
                        obj.Files.push(fileobj);
                    });
            }
            $scope.Attachements.push(obj)

        };


        $scope.getCompanyQtyItems = function () {
            catalogService.GetCompanyConfiguration($scope.compId, "ITEM_UNITS", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;
                });
        };

        var catNodes = [];
        $scope.nodes = [];
        $scope.selectedNodesView = [];
        $scope.getcategories = function () {
            catalogService.GetProductSubCategories($scope.productId, 0, $scope.compId)
                .then(function (response) {
                    $scope.nodes = response;
                    $scope.nodes1 = $scope.nodes;
                    $scope.getSelectedNodes();
                    //$scope.collapseAll();
                });

        };

        $scope.selectedNodes = '0';
        $scope.selectedCategories = '';
        $scope.selectChildNodes = function (childNode, ischecked) {
            childNode.nodeChecked = ischecked;
            childNode.nodes.forEach(function (item, index) {
                $scope.selectChildNodes(item, ischecked);
            });
        };

        $scope.selectParentNode = function (ischecked, selectedNodeScope) {
            if (selectedNodeScope) {
                var parentScope = selectedNodeScope.$parent;
                if (parentScope && parentScope.node && parentScope.node.catId > 0) {
                    if (ischecked) {
                        parentScope.node.nodeChecked = ischecked;
                    }
                    else {
                        if (parentScope.node.nodes && parentScope.node.nodes.length > 0) {
                            if ($filter('filter')(parentScope.node.nodes, { 'nodeChecked': true }).length <= 0) {
                                parentScope.node.nodeChecked = ischecked;
                            }
                        }
                        else {
                            parentScope.node.nodeChecked = ischecked;
                        }
                    }

                    $scope.selectParentNode(ischecked, parentScope);
                }
            }
        };

        $scope.checkChanged = function (selectedNode, selectedNodeScope) {
            $scope.selectChildNodes(selectedNode, selectedNode.nodeChecked);
            $scope.selectParentNode(selectedNode.nodeChecked, selectedNodeScope);
            $scope.getSelectedNodes();
        };

        $scope.toggle = function (scope) {
            scope.toggle();
        };

        $scope.productErrorMessage = '';
        $scope.productCodeErrorMessage = '';


        $scope.errorValidation = false;
        $scope.SaveProductDetails = function () {
            $scope.productErrorShow = $scope.errorValidation = false;
            $scope.productErrorMessage = $scope.productCodeErrorMessage = '';
            $scope.productObj = {
                prodId: $scope.productDetails.prodId,
                compId: $scope.compId,
                prodCode: $scope.prodEditDetails.prodCode,
                prodName: $scope.prodEditDetails.prodName,
                prodDesc: $scope.prodEditDetails.prodDesc,
                prodNo: $scope.prodEditDetails.prodNo,
                prodHSNCode: $scope.prodEditDetails.prodHSNCode,
                prodQty: $scope.prodEditDetails.prodQty,
                isValid: 1,
                prodAlternateUnits: $scope.prodEditDetails.prodAlternateUnits,
                unitConversion: $scope.prodEditDetails.unitConversion,
                shelfLife: $scope.prodEditDetails.shelfLife,
                productVolume: $scope.prodEditDetails.productVolume,
                ModifiedBy: userService.getUserId(),

                productGST: $scope.prodEditDetails.productGST,
                prefferedBrand: $scope.prodEditDetails.prefferedBrand,
                alternateBrand: $scope.prodEditDetails.alternateBrand,
                totPurchaseQty: $scope.prodEditDetails.totPurchaseQty,
                inTransit: $scope.prodEditDetails.inTransit,
                leadTime: $scope.prodEditDetails.leadTime,
                departments: $scope.prodEditDetails.departments,
                deliveryTerms: $scope.prodEditDetails.deliveryTerms,
                termsConditions: $scope.prodEditDetails.termsConditions,
                casNumber: $scope.prodEditDetails.casNumber,
                mfcdCode: $scope.prodEditDetails.mfcdCode,
                contractManagement: [], //$scope.prodEditDetails.contractManagement,
                multipleAttachments: $scope.prodEditDetails.multipleAttachments,
                listVendorDetails: [],
                isCommonItem: $scope.prodEditDetails.isCommonItem == 'true' ? 1 : 0,
                previousVerticalType: $scope.prodEditDetails.VERTICAL_TYPE
            };


            $scope.productObj.contractManagement.forEach(function (item, index) {
                if (item.isValid) {
                    if (!item.number) {
                        item.errorColorNum = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.value) {
                        item.errorColorVal = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.quantity) {
                        item.errorColorQty = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.startTime) {
                        item.errorColorSt = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.endTime) {
                        item.errorColorSe = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.companyName) {
                        item.errorColorNme = '1px solid red';
                        $scope.errorValidation = true;
                    }
                } else {
                    if (item.isValid === 0 && item.isOld === 0) {
                        $scope.productObj.contractManagement.splice(index, 1);
                    }
                }
                if (item.startTime) {
                    let ts = userService.toUTCTicks(item.startTime);
                    let m = moment(ts);
                    let contractStartDate = new Date(m);
                    let milliseconds = parseInt(contractStartDate.getTime() / 1000.0);
                    item.startTime = "/Date(" + milliseconds + "000+0530)/";
                } else {
                    item.startTime = null;
                }
                if (item.endTime) {
                    let ts = userService.toUTCTicks(item.endTime);
                    let m = moment(ts);
                    let contractEndDate = new Date(m);
                    let milliseconds = parseInt(contractEndDate.getTime() / 1000.0);
                    item.endTime = "/Date(" + milliseconds + "000+0530)/";
                } else {
                    item.endTime = null;
                }

            });

            if (!$scope.productObj.prodName) {
                $scope.productErrorShow = true;
                $scope.productErrorMessage = "please enter Product Name";
                return;
            }

            if ($scope.errorValidation) {
                return false;
            }

            $scope.searchvendorstring = '';

            var selectedVendors = $scope.vendorsList.filter(function (vendor) {
                return (vendor.isAssignedToProduct || vendor.isAssignedToProduct);
            });


            $scope.productObj.listVendorDetails = selectedVendors;
            var param = {
                reqProduct: $scope.productObj,
                sessionID: userService.getUserToken()
            };

            catalogService.updateProductDetails(param)
                .then(function (response) {
                    if (response.errorMessage) {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        $scope.SaveProductCategories();
                        growlService.growl("product updated Successfully.", "success");
                        location.reload();
                    }
                });
        };

        $scope.SaveIsCommonItem = function () {
            $scope.productErrorShow = $scope.errorValidation = false;
            $scope.productErrorMessage = $scope.productCodeErrorMessage = '';

            $scope.productObj = {
                prodId: $scope.productDetails.prodId,
                compId: $scope.compId,
                prodCode: $scope.prodEditDetails.prodCode,
                prodName: $scope.prodEditDetails.prodName,
                prodNo: $scope.prodEditDetails.prodNo,
                isCommonItem: $scope.prodEditDetails.isCommonItem == 'true' ? 1 : 0,
                previousVerticalType: $scope.prodEditDetails.VERTICAL_TYPE
            };
            var param = {
                reqProduct: $scope.productObj,
                sessionID: userService.getUserToken()
            };
            catalogService.SaveIsCommonItem(param)
                .then(function (response) {
                    if (response.errorMessage && response.responseId == -1) {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("product updated Successfully.", "success");
                        location.reload();
                    }
                });
        };

        $scope.blink = function (isCommon, type) {
            var blink = {};
            if (isCommon != $scope.prodEditDetails.isCommonItemTemp) {
                $scope.prodEditDetails.isBlink = true;
            } else {
                $scope.prodEditDetails.isBlink = false;
            }
            
            if ($scope.prodEditDetails.isBlink) {
                $scope.errMsg = "Value changed, please click here to save"
               var blinkr = document.getElementById('blink');
                $scope.prodEditDetails.blinkID = setInterval(function () {
                    blinkr.style.opacity = (blinkr.style.opacity == 0 ? 1 : 0);
                }, 600);
                
            } else {
                clearInterval($scope.prodEditDetails.blinkID);
            }
        }
       
        $scope.validateMatry = function () {
            $scope.prodEditDetails.contractManagement.forEach(function (item, index) {

               

                if (item.number) {
                    item.errorColorNum = '1px solid #e4e7ea';
                }
                if (item.value) {
                    item.errorColorVal = '1px solid #e4e7ea';
                }
                if (item.quantity) {
                    item.errorColorQty = '1px solid #e4e7ea';
                }
                if (item.startTime) {
                    item.errorColorSt = '1px solid #e4e7ea';
                }
                if (item.endTime) {
                    item.errorColorSe = '1px solid #e4e7ea';
                }
                if (item.companyName) {
                    item.errorColorNme = '1px solid #e4e7ea';
                }
            });
        };

        $scope.deleteProduct = function (prod) {

            swal({
                title: "Are You Sure!",
                text: 'Do You want to Delete the Item Permanently',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function () {
                $scope.productdelObj = {
                    prodId: prod.prodId,
                    compId: $scope.compId,
                    prodCode: prod.prodCode,
                    prodName: prod.prodName,
                    isValid: 0,
                    ModifiedBy: userService.getUserId()
                };

                var param = {
                    reqProduct: $scope.productdelObj,
                    sessionID: userService.getUserToken()
                };

                catalogService.deleteProduct(param)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("product has been deleted.", "success");
                            $scope.goToProducts();
                        }
                    });
            });
        };

        $(document).ready(function () {
            //$('select').selectize({
            //    sortField: 'text'
            //});
        });

        $scope.InactiveProduct = function (prod) {
            swal({
                title: "Are You Sure!",
                text: 'Do You want to Inactivate the Item',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function () {
                $scope.productdelObj = {
                    prodId: prod.prodId,
                    compId: $scope.compId,
                    prodCode: prod.prodCode,
                    prodName: prod.prodName,
                    isValid: 2,
                    ModifiedBy: userService.getUserId()
                };

                var param = {
                    reqProduct: $scope.productdelObj,
                    sessionID: userService.getUserToken()
                };

                catalogService.deleteProduct(param)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("product has been Inactivated.", "success");
                            $scope.goToProducts();
                        }
                    });
            });
        };

        $scope.goToProducts = function () {
            //var url = $state.href("productEdit", { "productId": prodId });
            $state.go("products");
            //window.open(url, '_blank');
        };

        $scope.SaveProductCategories = function () {
            var param = {
                prodId: $scope.productId,
                compId: $scope.compId,
                catIds: $scope.selectedNodes,
                user: userService.getUserId(),
                sessionId: userService.getUserToken()
            };

            catalogService.updateProductCategories(param)
                .then(function (response) {
                    if (response.errorMessage) {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("product details saved Successfully.", "success");
                        $scope.getProductDetails();
                    }
                });

        };

        $scope.getProperties = function () {
            $scope.Properties = [];
            catalogService.getProperties($scope.compId, $scope.productId, 0)
                .then(function (response) {
                    $scope.PropertiesRaw = response;
                    $scope.PropertiesRaw.forEach(function (item, index) {
                        var Property = {
                            "propId": item.propId,
                            "propName": item.propName,
                            "propDesc": item.propDesc,
                            "propDataType": item.propDataType,
                            "propChecked": !item.propValue ? false : true,
                            "propOptions": item.propOptions,
                            "propValue": item.propDataType === "multi" ? (!item.propValue ? item.propValue : item.propValue.split("^^")) : item.propValue,
                            "propIsValid": item.isValid,
                            "propModified": false
                        };

                        $scope.Properties.push(Property);
                    });
                });
        };

        $scope.propCheckChanged = function (selectedProp) {
            selectedProp.propModified = true;
            selectedProp.propIsValid = selectedProp.propChecked;
        };

        $scope.propModified = function (selectedProp) {
            selectedProp.propModified = true;
            selectedProp.propIsValid = selectedProp.propChecked;
        };

        $scope.saveProperties = function () {
            $scope.propertyobj = [];
            var callsave = false;
            $scope.Properties.forEach(function (item, index) {
                if (item.propChecked || item.propModified) {
                    callsave = true;
                    $scope.propertyobj.push({
                        entityId: $scope.productId,
                        companyId: $scope.compId,
                        propId: item.propId,
                        propValue: item.propValue ? (angular.isArray(item.propValue) ? item.propValue.join("^^") : item.propValue) : '',
                        isValid: item.propIsValid ? 1 : 0,
                        user: userService.getUserId()
                    });
                }
            });

            if (callsave) {
                var params = {
                    propertyobj: $scope.propertyobj,
                    sessionId: userService.getUserToken()
                };
                catalogService.saveEntityProperties(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("product attributes updated successfully.", "success");
                            $scope.getProductDetails();
                        }
                    });
            }
        };

        $scope.getSelectedNodes = function () {
            $scope.selectedCategories = [];
            $scope.selectedNodes = '0';

            $scope.nodes.forEach(function (item, index) {
                $scope.getSelectedNodesIteration(item, $scope.selectedCategories);
            });
        };

        $scope.getSelectedNodesIteration = function (selectedNode, selectedCat) {
            var selectedItem = { "catName": selectedNode.catName, "nodes": [] };
            if (selectedNode.nodeChecked) {

                $scope.selectedNodes += ',' + selectedNode.catId;

                if (selectedNode.nodes) {
                    selectedNode.nodes.forEach(function (item, index) {
                        if (item.nodeChecked) {
                            $scope.getSelectedNodesIteration(item, selectedItem.nodes);
                        }
                    });
                }

                selectedCat.push(selectedItem);
            }
        };

        $scope.multipleitemsAttachments = [];

        $scope.getFile = function () {
            $scope.progress = 0;
            //$scope.file = $("#attachement")[0].files[0];
            $scope.multipleitemsAttachments = $("#attachement")[0].files;
            $scope.multipleitemsAttachments = Object.values($scope.multipleitemsAttachments);
            $scope.multipleitemsAttachments.forEach(function (item, index) {
                fileReader.readAsDataUrl(item, $scope)
                    .then(function (result) {

                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: 0
                        };

                        var bytearray = new Uint8Array(result);

                        fileUpload.fileStream = $.makeArray(bytearray);
                        fileUpload.fileName = item.name;

                        if (!$scope.prodEditDetails.multipleAttachments) {
                            $scope.prodEditDetails.multipleAttachments = [];
                        }

                        $scope.prodEditDetails.multipleAttachments.push(fileUpload);

                    });
            });
        };

        $scope.ProductVendors = [];
        $scope.count = 0;
        $scope.GetProductVendors = function () {
            $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken() }
            catalogService.getproductVendors($scope.productId, userService.getUserToken())
                .then(function (response) {
                    $scope.ProductVendors = response;

                    $scope.vendorsList.forEach(function (vlItem, vlIndex) {
                        $scope.ProductVendors.forEach(function (plItem, plIIndex) {
                            if (vlItem.userID === plItem.userID) {
                                vlItem.isAssignedToProduct = true;
                            }
                        });
                    });
                });
        };

        $scope.searchvendorstring = '';
        $scope.totalItems1 = -1;
        $scope.searchVendors = function (value) {
            if (value) {
                value = String(value).toUpperCase();
                $scope.searchingVendors = value;
                $scope.vendorsLoading = true;
                var output = [];
                output = $scope.GetCompanyVendors(0, $scope.searchingVendors);
            } else {
                $scope.searchingVendors = '';
                $scope.GetCompanyVendors(0, $scope.searchingVendors);
            }
        };

        $scope.searchvendorprodstring = '';
        $scope.totalItemsProd1 = -1;
        $scope.searchVendorsProd = function (value) {
            value = String(value).toUpperCase();
            $scope.vendorsListProduct = $scope.VendorsTemp1.filter(function (item) {
                return (String(item.companyName).toUpperCase().includes(value)); //  || String(item.vendorCode).toUpperCase().includes(value) == true
            });
            $scope.totalItemsProd1 = $scope.vendorsListProduct.length;
            $scope.totalItemsProd = $scope.vendorsListProduct.length;
        };


        // Pagination For Overall Vendors//
        $scope.page = 0;
        $scope.PageSize = 50;
        $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
        $scope.totalCount = 0;
        // Pagination For Overall Vendors//


        $scope.GetCompanyVendors = function (IsPaging, searchString) {
            if (searchString) {
                $scope.fetchRecordsFrom = 0;
            }

            $scope.vendorsLoading = true;
            $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken(), "PageSize": $scope.fetchRecordsFrom, "NumberOfRecords": $scope.PageSize, "searchString": searchString ? searchString : "", PRODUCT_ID: +$scope.productId,IS_FROM:'CATALOGUE' };
            if (IsPaging === 1) {

                if ($scope.page > -1) {
                    $scope.page++;
                    $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
                    $scope.params.PageSize = $scope.fetchRecordsFrom;
                    $scope.params.searchString = searchString ? searchString : "";
                }

                if ($scope.totalCount !== $scope.vendorsList.length) {
                    userService.GetCompanyVendors($scope.params)
                        .then(function (response) {
                            if (response) {
                                for (var a in response) {
                                    $scope.vendorsList.push(response[+a]);
                                }
                                $scope.checkVendors($scope.vendorsList);
                                //$scope.vendorDisplayCollecton = [].concat($scope.vendorsList);
                                $scope.vendorsListProduct = $scope.vendorsList;
                                $scope.vendorsLoading = false;
                                $scope.totalCount = $scope.vendorsList[0].totalVendors;
                            }

                            $scope.VendorsTemp1 = $scope.vendorsList;
                        });
                }
            } else {
                userService.GetCompanyVendors($scope.params)
                    .then(function (response) {
                        $scope.vendorsList = response;
                        $scope.vendorsLoading = false;
                        $scope.vendorsListProduct = response;
                        $scope.VendorsTemp1 = $scope.vendorsList;
                        $scope.checkVendors($scope.vendorsList);
                    });
            }
        };

        $scope.GetCompanyVendors();
        //#region 

        $scope.ProductQuotationTemplate = [];

        $scope.hasSpecError = false;

        $scope.SaveProductQuotationTemplate = function (obj) {

            if (obj.HAS_SPECIFICATION) {
                obj.NAME = obj.NAME.replace(/\'/gi, "");
                obj.NAME = obj.NAME.replace(/\"/gi, "");
                obj.NAME = obj.NAME.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                obj.NAME = obj.NAME.replace(/(\r\n|\n|\r)/gm, "");
                obj.NAME = obj.NAME.replace(/\t/g, '');


                obj.DESCRIPTION = obj.DESCRIPTION.replace(/\'/gi, "");
                obj.DESCRIPTION = obj.DESCRIPTION.replace(/\"/gi, "");
                obj.DESCRIPTION = obj.DESCRIPTION.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                obj.DESCRIPTION = obj.DESCRIPTION.replace(/(\r\n|\n|\r)/gm, "");
                obj.DESCRIPTION = obj.DESCRIPTION.replace(/\t/g, '');
            }


            $scope.hasSpecError = false;
            if (!obj.HAS_SPECIFICATION) {
                $scope.hasSpecError = true;
                growlService.growl("Please Check Has Specification.", "inverse");
                return false;
            }

            var sameNameError = false;
            $scope.ProductQuotationTemplate.forEach(function (item, itemIndex) {
                if (obj.NAME.toUpperCase() === item.NAME.toUpperCase() && obj.T_ID === 0 && item.IS_VALID !== -1) {
                    sameNameError = true;
                }
            });

            if (sameNameError) { growlService.growl("Same name Error.", "inverse"); return false; }

            var params = {
                "productquotationtemplate": obj,
                "sessionid": userService.getUserToken()
            };

            catalogService.SaveProductQuotationTemplate(params)
                .then(function (response) {

                    if (response) {
                        $scope.GetProductQuotationTemplate();
                        growlService.growl("Saved Successfully.", "success");
                        $scope.resetTemplateObj();
                    }
                });
        };

        $scope.deleteProductQuotationTemplate = function (obj) {
            obj.IS_VALID = -1;
            var params = {
                "productquotationtemplate": obj,
                "sessionid": userService.getUserToken()
            };

            catalogService.SaveProductQuotationTemplate(params)
                .then(function (response) {
                    if (response) {
                        growlService.growl("Deleted Successfully.", "success");
                        $scope.resetTemplateObj();
                        if (obj.PRODUCT_ID > 0 && obj.index >= 0) {
                            $scope.GetProductQuotationTemplate(obj.PRODUCT_ID, obj.index, true);
                        }
                    }
                });
        };

        $scope.GetProductQuotationTemplate = function () {

            var params = {
                "catitemid": $scope.productId,
                "sessionid": userService.getUserToken()
            };

            catalogService.GetProductQuotationTemplate(params)
                .then(function (response) {

                    if (response) {
                        $scope.ProductQuotationTemplate = response;
                    }
                });
        };

        $scope.GetProductQuotationTemplate();


        $scope.resetTemplateObj = function () {
            $scope.QuotationTemplateObj = {
                T_ID: 0,
                PRODUCT_ID: $scope.productId,
                NAME: '',
                DESCRIPTION: '',
                HAS_SPECIFICATION: 0,
                HAS_PRICE: 0,
                HAS_QUANTITY: 0,
                CONSUMPTION: 1,
                UOM: '',
                HAS_TAX: 0,
                IS_VALID: 1,
                U_ID: userService.getUserId()
            };
        };

        $scope.resetTemplateObj();
        //#endregion


        $scope.productAlalysis = function (productId) {
            var url = $state.href("productAlalysis", { "productId": productId });
            window.open(url, '_blank');
        };

        $scope.checkVendors = function (vendorsArray) {
            vendorsArray.forEach(function (item, index) {
                if (item.IS_SELECTED === 1) {
                    item.isAssignedToProduct = true;
                }
            });
        };

        $scope.searchCategories = function (searchKeyword) {
            //$scope.filterArray = [];
            $scope.nodes = $scope.nodes1;
            if ($scope.searchCategories) {
                $scope.nodes = $scope.nodes.filter(function (node) {
                    return node.catName.toLowerCase().indexOf(searchKeyword.toLowerCase()) > -1;
                });

            }
        };

        $scope.getVendorCodes = function (vendor) {
            if (!vendor.vendorCodeList || vendor.vendorCodeList.length <= 0) {
                let params = {
                    'uid': vendor.vendorId,
                    'compid': $scope.compId,
                    'sessionid': $scope.sessionid
                };

                auctionsService.getVendorCodes(params)
                    .then(function (response) {
                        vendor.vendorCodeList = response;
                        if (vendor.vendorCodeList && vendor.vendorCodeList.length > 0 && !$scope.selectedProductContract.selectedVendorCode) {
                            vendor.selectedVendorCode = vendor.vendorCodeList[0].VENDOR_CODE;
                            $scope.selectedProductContract.selectedVendorCode = vendor.vendorCodeList[0].VENDOR_CODE;
                        }
                    });
            } else {
                if (vendor.vendorCodeList && vendor.vendorCodeList.length > 0 && !$scope.selectedProductContract.selectedVendorCode) {
                    vendor.selectedVendorCode = vendor.selectedVendorCode ? vendor.selectedVendorCode : vendor.vendorCodeList[0].VENDOR_CODE;
                    $scope.selectedProductContract.selectedVendorCode = vendor.vendorCodeList[0].VENDOR_CODE;
                }
            }
        };

        $scope.selectVendorCode = function (vendor) {
            console.log(vendor);
        };

        $scope.getFilterValues = function () {
            var params =
            {
                "compid": $scope.compId
            };

            $scope.stateDetails.plantList = [];

            PRMPRServices.getFilterValues(params)
                .then(function (response) {
                    if (response && response.length > 0) {
                        $scope.filterValues = response;
                        if ($scope.filterValues && $scope.filterValues.length > 0) {
                            $scope.filterValues.forEach(function (item, index) {
                                if (item.TYPE === 'PLANTS') {
                                    $scope.stateDetails.plantList.push({ id: item.NAME, name: item.ID + ' - ' + item.NAME });
                                }
                            });
                        }
                    }
                });
        };

        $scope.getFilterValues();

        $scope.generatePO = function (contract) {
            $scope.selectedProductContractForPO = contract;
        };

        $scope.navigateToPOForm = function () {
            //import
            if ($scope.selectedProductContractForPO) {
                angular.element('#templateSelection').modal('hide');
                let selectedTemplate = '';
                if ($scope.stateDetails.poTemplate === 'po-domestic-zsdm') {
                    selectedTemplate = 'ZSDM';
                } else if ($scope.stateDetails.poTemplate === 'po-import-zsim') {
                    selectedTemplate = 'ZSIM';
                } else if ($scope.stateDetails.poTemplate === 'po-bonded-wh') {
                    selectedTemplate = 'ZSBW';
                } else if ($scope.stateDetails.poTemplate === 'po-service-zssr') {
                    selectedTemplate = 'ZSSR';
                }

                $state.go(
                    $scope.stateDetails.poTemplate,
                    { 'contractDetails': $scope.selectedProductContractForPO, 'productDetails': $scope.productDetails, 'templateName': selectedTemplate, 'quoteLink': $location.absUrl() }
                );
            }
        };


        $scope.GetOtherInfo = function () {

            $scope.tableColumns = [];
            $scope.tableValues = [];
            var params = {
                "companyId": +$scope.compId,
                "value": $scope.prodEditDetails.prodCode,
                "sessionid": userService.getUserToken(),
                "type": "PRODUCT"
            };

            auctionsService.GetOtherInfo(params)
                .then(function (response) {
                    if (response && response.data) {
                        for (const [key, value] of Object.entries($.parseJSON(response.data))) {
                            var val = "";
                            $scope.tableColumns.push(key);
                            val = !value ? "-" : value;
                            $scope.tableValues.push(val);
                        }
                    }
                });
        };

    }]);
