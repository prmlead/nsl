﻿//(function () { 
prmApp.controller('CategoryProductsCtrl', ['$scope', 'catalogService', 'userService', 'growlService', function ($scope, catalogService, userService, growlService) {

    var catNodes = [];
    $scope.parentNodes = null;
    // $scope.collapsed = true;

    $scope.compId = userService.getUserCompanyId();
    $scope.catalogCompId = userService.getUserCatalogCompanyId();;
    $scope.getcategories = function () {
        catalogService.getcategories($scope.catalogCompId)
            .then(function (response) {
               // console.log(response);
                $scope.companyCatalog = response;
                
                $scope.companyCatalog.forEach(function (item, index) {
                    var cat = {
                        
                        "compId": item.compId,
                        "parentID": item.catParentId,
                        "id": item.catId,
                        "title": item.catName,
                        "catdesc": item.catDesc,
                        "subCatCount": item.subCatCount,
                        "childCollapsed" : true,
                        "nodeChecked": false,
                        "nodes": []
                    }
                    //console.log(cat);
                    catNodes.push(cat);
                   // console.log(cat);
                    $scope.parentNodes = catNodes;
                    //
                    /*[{
                        'compId': '1',

                        'id': '',
                        'title': 'Category Index',
                        'catdesc': '',
                        'nodes': catNodes,
                        'parentID': 0
                    }];*/
                })

                $scope.collapseAll();
            });

    }

    $scope.getsub = function (parentData) {
        $scope.productDetails = null;
        var nodeData = parentData;//.$modelValue;

        var subNodes = [];

        if (nodeData && nodeData.id && (nodeData.nodes == null || nodeData.nodes.length <=0)) {
            catalogService.getSubCatagories(nodeData.id, $scope.catalogCompId)
                .then(function (response) {
                    var subCatArray = response;
                    subCatArray.forEach(function (item, index) {
                        var subCat = {
                            "comId": item.compId,
                            "parentID": item.catParentId,
                            "id": item.catId,
                            "title": item.catName,
                            "catdesc": item.catDesc,
                            "subCatCount": item.subCatCount,
                            "childCollapsed": true,
                            "nodeChecked": false,
                            "nodes": []
                        }
                        subNodes.push(subCat);
                    })
                });

            nodeData.nodes = subNodes;
        }
    }

    $scope.getcategories();
    $scope.products = [];
    $scope.getProducts = function (nodeData) {
        $scope.productDetails = null;
        //var nodeData = catData;
        //alert(nodeData.id);
        var varproducts = [];
        if (nodeData) {
            catalogService.GetAllProductsByCategories($scope.catalogCompId, nodeData)
                .then(function (response) {
                    var prodArray = response;
                    prodArray.forEach(function (item, index) {
                        var product = {
                            "compId": item.compId,
                            "prodId": item.prodId,
                            "prodName": item.prodName
                        }
                        varproducts.push(product);
                    })
                });
        }
        $scope.products = varproducts;
    }
    $scope.productDetails = null;
    $scope.getProductDetails = function (prodDataNode) {
        var prodData = prodDataNode.product;
        $scope.productDetails = null;
        if (prodData && prodData.prodId) {
            catalogService.getproductbyid(prodData.compId, prodData.prodId)
                .then(function (response) {
                    $scope.productDetails = response;
                })

            }

        }

    var selectedNodes = '0';
    $scope.selectChildNodes = function (childNode, ischecked) {
        childNode.nodeChecked = ischecked;
        if (childNode.nodeChecked) {
            selectedNodes += ',' + childNode.id;
        }
        else {
            selectedNodes = selectedNodes.replace(","+childNode.id, "");
        }
        childNode.nodes.forEach(function (item, index) {
            $scope.selectChildNodes(item, ischecked);
        });
    }

    $scope.checkChanged = function (selectedNode) {
        $scope.selectChildNodes(selectedNode, selectedNode.nodeChecked);
        $scope.getProducts(selectedNodes);
        //alert(selectedNodes);
        
    }
    $scope.toggle = function (scope) {
        //selectedNode.childCollapsed = !selectedNode.childCollapsed;
        scope.toggle()

    };

    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.expandAll = function () {
        $scope.$broadcast('angular-ui-tree:expand-all');
    };

}]);
