﻿prmApp
.controller('listQCSCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService",
    "PRMPRServices", "reportingService", "workflowService", "$filter", "$rootScope",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService,
        PRMPRServices, reportingService, workflowService, $filter, $rootScope) {

        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();
        $scope.reqID = $stateParams.reqID;
        $scope.companyId = userService.getUserCompanyId();

        $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
        $scope.desigID = userService.getSelectedUserDesigID();

        $scope.enableQCSIfCreated = false;
        $scope.enableQCSDomestic = true;

        $scope.QCSList = [];
        $scope.QCSListTemp = [];
        $scope.InActiveQCSList = [];
        $scope.requirementDetails;
        $scope.getData = function () {
            reportingService.GetQcsPRDetails({ "reqID": $stateParams.reqID, "sessionid": $scope.sessionID })
                .then(function (response) {
                    if (response) {
                        $scope.requirementDetails = response;
                        var vendorDetails = _.filter($scope.requirementDetails.VENDOR_DETAILS, function (details) {
                            if (details.SELECTED_VENDOR_CURRENCY) {
                                return details.SELECTED_VENDOR_CURRENCY !== $scope.requirementDetails.REQ_CURRENCY;
                            }
                            
                        });

                        if (vendorDetails.length > 0) {
                            if ($scope.requirementDetails.REQ_CURRENCY !== vendorDetails[0].SELECTED_VENDOR_CURRENCY) {
                                $scope.enableQCSDomestic = false;
                            }
                        }
                       
                        $scope.GetQCSList();
                    }
                });
        };

        $scope.getData();

        $scope.GetQCSList = function () {
            var params = {
                "uid": $scope.userID,
                "reqid": $scope.reqID,
                "sessionid": userService.getUserToken()
            };
            reportingService.GetQCSList(params)
                .then(function (response) {
                    $scope.QCSList = response;
                    $scope.QCSList.forEach(function (qcs, qcsIndex) {
                        qcs.CREATED_DATE = userService.toLocalDate(qcs.CREATED_DATE);

                        if (qcs.IS_PRIMARY_ID == 1) {
                            qcs.isPrimary = 'PRIMARY';
                        } else {
                            qcs.isPrimary = 'SECONDARY';
                        }
                    });
                    $scope.QCSListTemp = $scope.QCSList;
                    $scope.QCSList = $filter('filter')($scope.QCSListTemp, { IS_VALID: 1 });

                    $scope.InActiveQCSList = $filter('filter')($scope.QCSListTemp, { IS_VALID: 0 });

                    $scope.disableCreationOfQCS($scope.QCSList, $scope.reqID);
                });
        };

       // $scope.GetQCSList();

        $scope.DeleteQcs = function (reqID, qcsID) {

            swal({
                title: "Are you sure?",
                //text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function () {
                    var params = {
                        "reqID": reqID,
                        "qcsID": qcsID,
                        "sessionid": userService.getUserToken(),
                        "U_ID": +$scope.userID
                    };
                    reportingService.DeleteQcs(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                growlService.growl("QCS Deleted successfully.", "success");
                                $scope.GetQCSList();
                            }

                        });
            });
        }

        $scope.ActivateQcs = function (reqID, qcsID) {

            swal({
                title: "Are you sure?",
                //text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function () {
                var params = {
                    "reqID": reqID,
                    "qcsID": qcsID,
                    "sessionid": userService.getUserToken(),
                    "U_ID": +$scope.userID
                };
                reportingService.ActivateQcs(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("QCS Activated successfully.", "success");
                            $scope.GetQCSList();
                        }

                    });
            });
        }

        $scope.goToSaveDomesticQCS = function (reqID, qcsID) {
            var url = $state.href("cost-comparisions-qcs", { "reqID": reqID, "qcsID": qcsID });
            window.open(url, '_self');
        };

        $scope.goToSaveImportQCS = function (reqID, qcsID) {
            var url = $state.href("import-qcs", { "reqID": reqID, "qcsID": qcsID });
            window.open(url, '_self');
        };


        $scope.qcsApprovalList = [];

        $scope.goToApprovalQCS = function () {

            var url = $state.href("approval-qcs-list");
            window.open(url, '_blank');
           
        };

        $scope.disableCreationOfQCS = function (arr,value) {

            $scope.enableQCSIfCreated = true;
            
            if ($scope.requirementDetails.REQ_STATUS == "Negotiation Ended") {
                $scope.enableQCSIfCreated = true;
            } else {
                if (arr && arr.length > 0) {
                    var req_id = _.findIndex(arr, function (requirement) { return requirement.REQ_ID === +value; });
                    if (req_id > -1) {
                        $scope.enableQCSIfCreated = false;
                    }
                }
            }
            
            
            return $scope.enableQCSIfCreated;
        };

        $scope.deletedOn = function (date) {
            return userService.toLocalDate(date);
        };

        /****** CODE FOR UPDATEING EBIT & BEST BID SAVINGS FOR EXISTING QCS  ******/
        //$scope.UpdateQCSSavings = function () {
        //    var savingsArr = [];

        //    var params = {
        //        "dbname": "hi",
        //        "compid": $scope.companyId,
        //        "sessionid": $scope.sessionID,
        //        "type": 'DOMESTIC'
        //    };
        //    reportingService.UpdateQCSSavings(params)
        //        .then(function (response) {
        //            $scope.finalArray = [];
        //            $scope.QCSDetailsTest = response;

        //            $scope.QCSDetailsTest.forEach(function (qcsItem, qcsIndex) {
        //                $scope.requirementDetails = { auctionVendors: [] };
        //                if (qcsItem.REQ_JSON)
        //                {
        //                    try {
        //                        $scope.requirementDetails = JSON.parse(qcsItem.REQ_JSON);

        //                        if (qcsItem) {
        //                            qcsItem.VENDOR_ITEMS_ASSIGNMENTS = [];
        //                            if (qcsItem.VENDOR_ITEM_ASSIGNMENT && qcsItem.VENDOR_ITEM_ASSIGNMENT.length > 0) {
        //                                var items = [];
        //                                qcsItem.VENDOR_ITEM_ASSIGNMENT.forEach(function (qcsVendItem, qcsVendItemIndex) {
        //                                    var itemObj =
        //                                    {
        //                                        QCS_ID: qcsItem.QCS_ID,
        //                                        assignedQty: qcsVendItem.ASSIGN_QTY,
        //                                        currencyfactor: _.find($scope.requirementDetails.auctionVendors, { vendorID: qcsVendItem.VENDOR_ID }).vendorCurrencyFactor,
        //                                        itemID: qcsVendItem.ITEM_ID,
        //                                        lppValue: 0
        //                                    }
        //                                    //$scope.requirementDetails.listRequirementItems.forEach(function (reqItem, reqItemIndex) {
        //                                    //    if (!reqItem.LPPValue) {
        //                                    //        reqItem.LPPValue = _.find(qcsItem.LPP_VALUE, { CATALOGUE_ITEM_ID: reqItem.catalogueItemID }) ? _.find(qcsItem.LPP_VALUE, { CATALOGUE_ITEM_ID: reqItem.catalogueItemID }).REV_UNIT_PRICE : 0;
        //                                    //    }
        //                                    //});
        //                                    //if (!reqItem.LPPValue) {
        //                                    //    reqItem.LPPValue = _.find(qcsItem.LPP_VALUE, { CATALOGUE_ITEM_ID: reqItem.catalogueItemID }) ? _.find(qcsItem.LPP_VALUE, { CATALOGUE_ITEM_ID: reqItem.catalogueItemID }).REV_UNIT_PRICE : 0;
        //                                    //}

        //                                    items.push(itemObj);
        //                                });

        //                                qcsItem.VENDOR_ITEMS_ASSIGNMENTS = retrieveVendorItems(items);

        //                                var obj = { BEST_BID_SAVINGS: 0, BEST_BID_SAVINGS_IN_REQUIRED_CURRENCY: 0, EBIT_SAVINGS: 0, EBIT_SAVINGS_IN_REQUIRED_CURRENCY: 0 };
        //                                var value = calculateBestBidSavings(qcsItem.VENDOR_ITEMS_ASSIGNMENTS);
        //                                if (+value > 0) {
        //                                    obj.BEST_BID_SAVINGS = +value;
        //                                    obj.BEST_BID_SAVINGS_IN_REQUIRED_CURRENCY = $scope.requirementDetails.currency === 'INR' ? +value : (+value * $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency));
        //                                }

        //                                var value = calculateEbitSavings(qcsItem.VENDOR_ITEMS_ASSIGNMENTS);
        //                                if (+value > 0) {
        //                                    obj.EBIT_SAVINGS = +value;
        //                                    obj.EBIT_SAVINGS_IN_REQUIRED_CURRENCY = $scope.requirementDetails.currency === 'INR' ? +value : (+value * $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency));
        //                                }

        //                                $scope.finalArray.push(obj);
        //                            }

        //                        }
        //                    } catch (e) {

        //                    }
        //                }
        //            });


        //            if ($scope.finalArray && $scope.finalArray.length > 0) {
        //                $scope.saveQCSSAVINGS();
        //            }
        //        });
        //};

        //function calculateBestBidSavings(items) {
        //    let bestBidItemWiseSavings = [];
        //    if (items && items.length > 0) {
        //        bestBidItemWiseSavings = _(items).groupBy('ITEM_ID') // group the items
        //            .map((objs, key) => ({
        //                'ITEM_ID': +key, // item id value
        //                'ITEM_WISE_BEST_BID_SAVINGS': _.sumBy(objs, 'BEST_BID_SAVINGS')
        //            })).value();
        //    }
        //    return _.sumBy(bestBidItemWiseSavings, 'ITEM_WISE_BEST_BID_SAVINGS');
        //}

        //function calculateEbitSavings(items) {
        //    let ebitItemWiseSavings = [];
        //    if (items && items.length > 0) {
        //        ebitItemWiseSavings = _(items).groupBy('ITEM_ID') // group the items
        //            .map((objs, key) => ({
        //                'ITEM_ID': +key, // item id value
        //                'ITEM_WISE_EBIT_SAVINGS': _.sumBy(objs, 'EBIT_SAVINGS')
        //            })).value();
        //    }
        //    return _.sumBy(ebitItemWiseSavings, 'ITEM_WISE_EBIT_SAVINGS');
        //}

        //function retrieveVendorItems(items) {
        //    var itemsFinal = [];
        //    if (items && items.length > 0)
        //    {
        //        var tempRFQDet = $scope.requirementDetails.auctionVendors;
        //        if (tempRFQDet && tempRFQDet.length > 0) {
        //            tempRFQDet.forEach(function (vendor, vendorIndex) {
        //                if (vendor.listRequirementItems && vendor.listRequirementItems.length > 0) {
        //                    vendor.listRequirementItems.forEach(function (vendItem, vendItemIndex) {
        //                        vendItem.unitPrice = vendItem.unitPrice * vendor.vendorCurrencyFactor;
        //                        vendItem.revUnitPrice = vendItem.revUnitPrice * vendor.vendorCurrencyFactor;
        //                    });
        //                }
        //            });
        //        }
        //        items.forEach(function (item, index) {
        //            var groupItemsBasedOnItemID = _.filter(_.flatten(_.map(tempRFQDet, 'listRequirementItems')), { itemID: item.itemID, isItemQuotationRejected: 0 }); // each item
        //            var getInitialUnitPriceItemGreaterThanZero = _.filter(groupItemsBasedOnItemID, function (item) { return item.unitPrice > 0; });
        //            var getRevUnitPriceItemGreaterThanZero = _.filter(groupItemsBasedOnItemID, function (item) { return item.revUnitPrice > 0; });
        //            var minLeastPrice = _.minBy(getInitialUnitPriceItemGreaterThanZero, 'unitPrice').unitPrice;
        //            var minLeastRevUnitPrice = _.minBy(getRevUnitPriceItemGreaterThanZero, 'revUnitPrice').revUnitPrice;
        //            var bbsprice = ((minLeastPrice * item.assignedQty) - (minLeastRevUnitPrice * item.assignedQty));
        //            var ebprice = (item.lppValue - (item.revUnitPrice * item.currencyfactor)) * item.assignedQty;
        //            var qcsAssignmentObj = {
        //                QCS_ID: item.QCS_ID,
        //                BEST_BID_SAVINGS: +bbsprice > 0 ? +bbsprice : 0,
        //                EBIT_SAVINGS: +ebprice > 0 ? +ebprice : 0
        //            };
        //            itemsFinal.push(qcsAssignmentObj);
        //        });
        //    }
        //    return itemsFinal;
        //}

        //$scope.GetCurrencyFactors = function () {
        //    auctionsService.GetCurrencyFactors($scope.userID, $scope.sessionID, $scope.companyId)
        //        .then(function (response) {
        //            $scope.currencyFactors = [];
        //            response.forEach(function (item) {
        //                if (item.type === 'currencyfactor') {
        //                    $scope.currencyFactors.push(item);
        //                }
        //            });
        //        });
        //};

        //$scope.GetCurrencyFactors();

        //$scope.getRequirementCurrencyFactor = function (currency) {
        //    let convertToLocalCurrencyFactor = 1;
        //    if ($scope.currencyFactors && $scope.currencyFactors.length > 0) {
        //        let reqCurrencyObj = $scope.currencyFactors.filter(function (curr) {
        //            return curr.currencyCode === currency;
        //        });

        //        if (reqCurrencyObj && reqCurrencyObj.length > 0 && reqCurrencyObj[0].currencyFactor !== 1) {
        //            convertToLocalCurrencyFactor = reqCurrencyObj[0].currencyFactor;
        //        }
        //    }

        //    return convertToLocalCurrencyFactor;
        //};


        //$scope.saveQCSSAVINGS = function () {
        //    alasql("CREATE TABLE TEMP_QCS_SAVINGS (BEST_BID_SAVINGS decimal(20, 8), BEST_BID_SAVINGS_IN_REQUIRED_CURRENCY decimal(20, 8), EBIT_SAVINGS decimal(20, 8), EBIT_SAVINGS_IN_REQUIRED_CURRENCY decimal(20, 8))");
        //    $scope.finalArray.forEach(function (item,index) {
        //        alasql("INSERT INTO TEMP_QCS_SAVINGS VALUES (" + item.BEST_BID_SAVINGS + "," + item.BEST_BID_SAVINGS_IN_REQUIRED_CURRENCY + "," + item.EBIT_SAVINGS + "," + item.EBIT_SAVINGS_IN_REQUIRED_CURRENCY +")");
        //    });
        //};

        //$scope.UpdateQCSSavings();

        /****** CODE FOR UPDATEING EBIT & BEST BID SAVINGS FOR EXISTING QCS  ******/

}]);