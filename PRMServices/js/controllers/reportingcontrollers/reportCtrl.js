﻿prmApp
    .controller('reportCtrl', ["$scope", "$http", "$state", "$window", "$timeout", "domain", "$filter", "$stateParams", "$timeout", "$log", "auctionsService", "userService", "reportingService",
        function ($scope, $http, $state, $window, $timeout, domain, $filter, $stateParams, $timeout, $log, auctionsService, userService, reportingService) {

        $scope.deliveryDateData = [];

        $scope.reqDetails = [];

        $scope.reqId = $stateParams.reqID;
        $scope.userId = userService.getUserId();

            $scope.liveBiddingChart = {
            credits: {
                enabled: false
            },
            options: {
                chart: {
                    type: 'line'
                }
            },
            series: [],
            title: {
                text: 'Bidding Report'
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
        }
        $scope.categories = [];
        $scope.shouldAdd = true;
        $scope.getLiveBiddingReport = function () {
            reportingService.getLiveBiddingReport($stateParams.reqID)
                .then(function (response) {
                    $scope.liveBiddingData = response;
                    $scope.liveBiddingData.forEach(function (item, index) {
                        var data = [];
                        item.arrayKeyValue.forEach(function (val, ind) {
                            if ($scope.shouldAdd) {
                                val.keyDateTime = userService.toLocalDate(val.keyDateTime);
                                $scope.categories.push(val.keyDateTime);
                            }
                            data.push(val.decimalVal);
                        })
                        $scope.shouldAdd = false;
                        $scope.liveBiddingChart.series.push({
                            name: item.vendor.firstName + " " + item.vendor.lastName,
                            data: data
                        });
                    });
                    $scope.liveBiddingChart.xAxis = {
                        title: {
                            text: 'Time'
                        },
                        categories: $scope.categories
                    }
                    $scope.liveBiddingChart.yAxis = {
                        title: {
                            text: 'Negotiation Price'
                        }
                    }
                    $scope.show = true;
                })
        }

        $scope.getLiveBiddingReport();

        $scope.GetReqDetails = function () {
            reportingService.GetReqDetails($stateParams.reqID)
                .then(function (response) {
                    $scope.reqDetails = response;

                    $scope.reqDetails.postedOn = userService.toLocalDate($scope.reqDetails.postedOn);
                    $scope.reqDetails.startTime = userService.toLocalDate($scope.reqDetails.startTime);
                    $scope.reqDetails.endTime = userService.toLocalDate($scope.reqDetails.endTime);

                    $log.info($scope.reqDetails);
                })
        }

        $scope.GetReqDetails();

        $scope.getItemWiseReport = function () {
            reportingService.getItemWiseReport($stateParams.reqID)
                .then(function (response) {
                    $scope.itemWiseData = response;
                })
        }

        $scope.getItemWiseReport();

        $scope.getDeliveryDateReport = function () {
            reportingService.getDeliveryDateReport($stateParams.reqID)
                .then(function (response) {
                    $scope.deliveryDateData = response;

                })
        }

        $scope.getDeliveryDateReport();

        $scope.getPaymentTermsReport = function () {
            reportingService.getPaymentTermsReport($stateParams.reqID)
                .then(function (response) {
                    $scope.paymentTermsData = response;
                })
        }

        $scope.getPaymentTermsReport();

        $scope.converToDate = function (date) {
            var fDate = userService.toLocalDate(date);

            if (fDate.indexOf('-9999') > -1) {
                fDate = "-";
            }

            return fDate;
        }



        $scope.printReport = function () {
            $scope.doPrint = true;
            $timeout(function () {
                $window.print();
                $scope.doPrint = false
            }, 1000);
        }





            $scope.getCustomerData = function (userId) {
                auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userId })
                    .then(function (response) {
                        $scope.auctionItem = response;
                        $scope.auctionItem.auctionVendors = _.filter($scope.auctionItem.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });
                        //if ($scope.auctionItem.auctionVendors.length > 0) {
                        //    $scope.getRequirementData();
                        //};

                    });
            };

            $scope.getVendorData = function (item) {
                auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': item.vendorID })
                    .then(function (response) {
                        $scope.auctionItemVendor = response;
                        item.auctionItemVendor = $scope.auctionItemVendor;
                    });
            };

            $scope.getRequirementData = function () {
                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    if (item.vendorID > 0) {
                        item.auctionItemVendor = [];
                        $scope.getVendorData(item);
                    }
                });
            };

            $scope.getCustomerData($scope.userId);


    }]);