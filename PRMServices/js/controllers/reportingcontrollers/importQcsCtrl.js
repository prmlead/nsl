﻿prmApp
    .controller('importQcsCtrl', ["$scope", "$state", "$log", "$stateParams", "$q", "userService", "auctionsService", "$window",
        "$timeout", "reportingService", "growlService", "workflowService", "PRMCustomFieldService", "catalogService", "PRMPOServices", "fileReader", '$location', "PRMPRServices", "$rootScope",
        function ($scope, $state, $log, $stateParams, $q, userService, auctionsService, $window,
            $timeout, reportingService, growlService, workflowService, PRMCustomFieldService, catalogService, PRMPOServices, fileReader, $location, PRMPRServices, $rootScope) {
            $scope.vendorWidth = 12;
            $scope.reqId = $stateParams.reqID;
            $scope.qcsID = +$stateParams.qcsID;
            $scope.requirementDetails = {};
            $scope.qcsRequirementDetails;
            $scope.isTechSpecExport = false;
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.loggedInUserName = userService.getFirstname() + " " + userService.getLastname();
            // $scope.isSuperUser = userService.getUserObj().isSuperUser;
            if (!$scope.isCustomer) {
                $state.go('home');
            }

            $scope.qcsTypeObj = {
                qcsType: 'IMPORT'
            };

            $scope.isQcsSaveDisable = false;
            //$scope.includeGstInCal = true;
            $scope.userId = userService.getUserId();
            $scope.sessionid = userService.getUserToken();
            $scope.companyId = userService.getUserCompanyId();
            $scope.editForm = false;
            $scope.currencyFactors = [];
            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.deptIDStr = '';
            $scope.desigIDStr = '';
            $scope.totalAttachmentMaxSize = 6291456;
            $scope.vendorSAPDetails = [];
            $scope.vendorAssignmentList = [];
            $scope.DraftPOList = [];
            $scope.vendorAssignmentList.push({
                qcsVendorItemId: 0,
                vendorID: 0,
                itemID: 0,
                item: null,
                assignedQty: 0,
                assignedPrice: 0,
                totalPrice: 0,
                poID: '',
                currency: ''
            });

            $scope.requirementItemContracts = [];

            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                            $scope.desigIDs = _.union($scope.desigIDs, [item1.desigID]);
                        }
                    });
                });
                $scope.deptIDStr = $scope.deptIDs.join(',');
                $scope.desigIDStr = $scope.desigIDs.join(',');
            }

            $scope.stateDetails = {
                poTemplate: 'po-domestic-zsdm'
            };

            $scope.QCSREQIDS = [];
            $scope.GetQCSIDS = function () {
                var params = {
                    "reqid": $scope.reqId,
                    "sessionid": userService.getUserToken()
                };
                reportingService.GetQCSIDS(params)
                    .then(function (response) {
                        $scope.QCSREQIDS = response;
                    });
            };
            $scope.GetQCSIDS();

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.qcsVendors = [];
            $scope.qcsPOVendors = [];
            $scope.qcsItems = [];
            $scope.qcsCoreItems = [];
            $scope.obj = {
                auctionVendors: []
            };
            $scope.objNew = {
                auctionVendors: []
            };
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'QCS';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/

            $scope.requirementSettings = [];
            $scope.selectedTemplate = {};
            $scope.prmFieldMappingDetails = {};

            $scope.QCSDetails = {
                QCS_ID: 0,
                REQ_ID: $scope.reqId,
                U_ID: userService.getUserId(),
                QCS_CODE: '',
                PO_CODE: '',
                RECOMMENDATIONS: '',
                UNIT_CODE: '',
                IS_TAX_INCLUDED: '',
                IS_VALID: 1
            };

            if ($scope.qcsID <= 0) {
                $scope.QCSDetails.IS_VALID = 1;
            }

            $scope.doPrint = false;

            $scope.printReport = function () {
                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false;
                }, 1000);
            };

            $scope.htmlToCanvasSaveLoading = false;

            $scope.IncoTermsEditableForOtherCharges = [];

            $scope.htmlToCanvasSave = function (format) {
                $scope.htmlToCanvasSaveLoading = true;
                setTimeout(function () {
                    try {
                        var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                        var canvas = document.createElement("canvas");
                        if (format === 'pdf') {
                            document.getElementById("widget").style["display"] = "";
                        }
                        else {
                            document.getElementById("widget").style["display"] = "inline-block";
                        }

                        html2canvas($("#widget"), {
                            onrendered: function (canvas) {
                                theCanvas = canvas;

                                const a = document.createElement("a");
                                a.style = "display: none";
                                a.href = canvas.toDataURL();

                                // Add Image to HTML
                                //document.body.appendChild(canvas);

                                /* Save As PDF */
                                if (format === 'pdf') {
                                    var imgData = canvas.toDataURL();
                                    var pdf = new jsPDF();
                                    pdf.addImage(imgData, 'JPEG', 0, 0);
                                    pdf.save(name);
                                }
                                else {
                                    a.download = name;
                                    a.click();
                                }

                                // Clean up 
                                //document.body.removeChild(canvas);
                                document.getElementById("widget").style["display"] = "";
                                $scope.htmlToCanvasSaveLoading = false;
                            }
                        });
                    }
                    catch (err) {
                        document.getElementById("widget").style["display"] = "";
                        $scope.htmlToCanvasSaveLoading = false;
                    }

                }, 500);

            };

            $scope.SaveQCSDetails = function (val) {
                let attachmentPromises = [];
                $scope.requirementDetails.qcsMultipleAttachments.forEach(function (fileObj, index) {
                    if (fileObj && fileObj.fileStream && fileObj.fileStream.length > 0 && !fileObj.fileID) {
                        let param = {
                            file: fileObj,
                            user: $scope.userId,
                            sessionid: $scope.sessionid
                        };

                        var promise = auctionsService.saveAttachment(param);
                        attachmentPromises.push(promise);
                    }
                });

                $q.all(attachmentPromises).then(function (responses) {
                    if (responses && responses.length > 0) {
                        responses.forEach(function (file, idx) {
                            file.fileStream = [];
                        });

                        $scope.requirementDetails.qcsMultipleAttachments = responses;
                    }

                    $scope.QCSDetails.QCS_TYPE = 'IMPORT';
                    if (!$scope.QCSDetails.QCS_CODE) {
                        $scope.QCSDetails.QCS_CODE = new Date().getUTCMilliseconds();
                    }

                    $scope.requirementDetails.auctionVendors.forEach(function (vendor, idx) {
                        vendor.listRequirementItems.forEach(function (item, itemIdx) {
                            item.PR_ITEM_ID_STRING = JSON.stringify(item.prlineItemarr);
                        });
                    });

                    $scope.QCSDetails.REQ_JSON = JSON.stringify($scope.requirementDetails);

                    if ($scope.QCSDetails.CREATED_USER == null) {
                        $scope.QCSDetails.CREATED_USER = $scope.userId;
                    }
                    if ($scope.QCSDetails.U_ID == 0 || $scope.QCSDetails.U_ID == null) {
                        $scope.QCSDetails.U_ID = $scope.userId;
                    }

                    $scope.QCSDetails.IS_TAX_INCLUDED = $scope.requirementDetails.includeGstInCal;
                    $scope.QCSDetails.REQ_TITLE = $scope.requirementDetails.title;

                    $scope.QCSDetails.APPROVER_RANGE = calculateApproverRangeFromLandedCost($scope.QCSDetails.REQ_JSON);
                    $scope.QCSDetails.APPROVAL_COUNT = $scope.workflowObj.workflowID ? calculateApproversCount($scope.workflowObj.workflowID) : 0;

                    var vendorAssignedItems = [];
                    var requirementDetails_temp = JSON.parse($scope.QCSDetails.REQ_JSON);
                    var requirementDetails_temp_reqItems = requirementDetails_temp.listRequirementItems;

                    if (requirementDetails_temp) {
                        requirementDetails_temp.auctionVendors.forEach(function (vendor, idx) {
                            vendor.listRequirementItems.forEach(function (item, itemIdx) {
                                if (item.qtyDistributed > 0) {
                                    vendorAssignedItems.push({
                                        QCS_ID: $stateParams.qcsID,
                                        REQ_ID: $stateParams.reqID,
                                        vendorID: vendor.vendorID,
                                        itemID: item.itemID,
                                        assignedQty: item.qtyDistributed,
                                        assignedPrice: item.revUnitPrice,
                                        totalPrice: (item.revUnitPrice * item.productQuantity),
                                        itemRank: item.itemRank,
                                        unitPrice: item.unitPrice,
                                        revUnitPrice: item.revUnitPrice,
                                        lppValue: _.find(requirementDetails_temp_reqItems, { itemID: item.itemID }).LPPValue ? _.find(requirementDetails_temp_reqItems, { itemID: item.itemID }).LPPValue : 0,
                                        currencyfactor: vendor.vendorCurrencyFactor,
                                        vendorRank: vendor.rank,
                                        vendorName: vendor.companyName,
                                        itemName: item.productIDorName
                                    });
                                }
                            });
                        });
                    }
                    $scope.QCSDetails.VENDOR_ITEM_ASSIGNMENT = retrieveVendorItems(vendorAssignedItems);

                    /****** BEST BID SAVINGS ******/
                    var value = calculateBestBidSavings($scope.QCSDetails.VENDOR_ITEM_ASSIGNMENT);
                    $scope.QCSDetails.SAVINGS = 0;
                    $scope.QCSDetails.SAVINGS_IN_REQUIRED_CURRENCY = 0;
                    if (+value > 0) {
                        $scope.QCSDetails.SAVINGS = +value;
                        $scope.QCSDetails.SAVINGS_IN_REQUIRED_CURRENCY = $scope.requirementDetails.currency === 'INR' ? +value : (+value * $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency));
                    }

                    $scope.QCSDetails.QCS_ASSIGNED_QTY_TOTAL = $scope.totalAssignedQty();
                    /****** EBIT SAVINGS ******/
                    var value = calculateEbitSavings($scope.QCSDetails.VENDOR_ITEM_ASSIGNMENT);
                    $scope.QCSDetails.SAVINGS1 = 0;
                    $scope.QCSDetails.SAVINGS1_IN_REQUIRED_CURRENCY = 0;
                    if (+value > 0) {
                        $scope.QCSDetails.SAVINGS1 = +value;
                        $scope.QCSDetails.SAVINGS1_IN_REQUIRED_CURRENCY = $scope.requirementDetails.currency === 'INR' ? +value : (+value * $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency));
                    }
                    /****** EBIT SAVINGS ******/
                    var emailString = "";
                    if ($scope.qcsID <= 0 && $scope.workflowObj.workflowID && $scope.workflowObj.workflowID > 0) {
                        emailString += "<table style=\"border:2px solid black;width:50%; border-collapse: collapse; table-layout: fixed; background-color: lightblue;\">";
                        emailString += "<tr><td style=\"border-right:1px solid black;border-bottom:1px solid black;text-align:center\"><strong>RFQ No:</strong></td><td style=\"border-bottom:1px solid black;text-align:center\">" + $scope.requirementDetails.requirementNumber + "</td></tr>";
                        emailString += "<tr><td style=\"border-right:1px solid black;border-bottom:1px solid black;text-align:center\"><strong>RFQ Title:</strong></td><td style=\"border-bottom:1px solid black;text-align:center\">" + $scope.requirementDetails.title + "</td></tr>";
                        emailString += "<tr><td style=\"border-right:1px solid black;border-bottom:1px solid black;text-align:center\"><strong>Requirement Location:</strong></td><td style=\"border-bottom:1px solid black;text-align:center\">" + $scope.requirementDetails.PLANTS + "</td></tr>";
                        emailString += "<tr><td style=\"border-right:1px solid black;border-bottom:1px solid black;text-align:center\"><strong>Total QCS:</strong></td><td style=\"border-bottom:1px solid black;text-align:center\">" + $scope.precisionRound(parseFloat($scope.QCSDetails.QCS_ASSIGNED_QTY_TOTAL), $rootScope.companyRoundingDecimalSetting) + "</td></tr>";
                        emailString += "<tr><td style=\"border-right:1px solid black;border-bottom:1px solid black;text-align:center\"><strong>Last Approved/Rejected by:</strong></td><td style=\"border-bottom:1px solid black;text-align:center\">" + '-' + "</td></tr>";
                        emailString += "<tr><td style=\"border-right:1px solid black;border-bottom:1px solid black;text-align:center\"><strong>Purchaser Comments:</strong></td><td style=\"border-bottom:1px solid black;text-align:center\">" + ($scope.requirementDetails.customerComment ? $scope.requirementDetails.customerComment : '-') + "</td></tr></table>";
                        emailString += "<br />";
                        emailString += "Please login: <a href='" + window.location.href + "'> QCS Details</a>";
                        emailString += "<br />";
                        emailString += "<br />";
                        emailString += "Item Wise Quantity";
                        emailString += "<br />";
                        emailString += "<br />";
                        emailString += "<table style=\"border: 2px solid black;width:50%;border-collapse: collapse;table-layout: fixed;background-color: lightblue;\">";
                        emailString += "<tr><td style=\"border-right: 1px solid black;border-bottom: 1px solid black;text-align: center\"><strong>Item Name</strong></td>";
                        emailString += "<td style=\"border-right: 1px solid black;border-bottom: 1px solid black;text-align: center\"><strong>Vendor Name</strong></td>";
                        emailString += "<td style=\"border-right: 1px solid black;border-bottom: 1px solid black;text-align: center\"><strong>Vendor Rank</strong></td>";
                        emailString += "<td style=\"border-right: 1px solid black;border-bottom: 1px solid black;text-align: center\"><strong>Order Qty</strong></td>";
                        emailString += "<td style=\"border-right: 1px solid black;border-bottom: 1px solid black;text-align: center\"><strong>Vendor Price</strong></td>";
                        emailString += "<td style=\"border-right: 1px solid black;border-bottom: 1px solid black;text-align: center\"><strong>Total Price</strong></td>";
                        $scope.QCSDetails.VENDOR_ITEM_ASSIGNMENT.forEach(function (vendorItemAssignment, vendorItemAssignmentIndex) {
                            emailString += "<tr><td style=\"border-right: 1px solid black; border-bottom: 1px solid black; text-align: center\">" + vendorItemAssignment.itemName + "</td>";
                            emailString += "<td style=\"border-right: 1px solid black; border-bottom: 1px solid black; text-align: center\">" + vendorItemAssignment.vendorName + "</td>";
                            emailString += "<td style=\"border-right: 1px solid black; border-bottom: 1px solid black; text-align: center\">" + vendorItemAssignment.Rank + "</td>";
                            emailString += "<td style=\"border-right: 1px solid black; border-bottom: 1px solid black; text-align: center\">" + $scope.precisionRound(vendorItemAssignment.ASSIGN_QTY, $rootScope.companyRoundingDecimalSetting) + "</td>";
                            emailString += "<td style=\"border-right: 1px solid black; border-bottom: 1px solid black; text-align: center\">" + $scope.precisionRound(parseFloat(vendorItemAssignment.ASSIGN_PRICE), $rootScope.companyRoundingDecimalSetting) + "</td>";
                            emailString += "<td style=\"border-right: 1px solid black; border-bottom: 1px solid black; text-align: center\">" + $scope.precisionRound(parseFloat(vendorItemAssignment.TOTAL_PRICE), $rootScope.companyRoundingDecimalSetting) + "</td></tr>";

                        });

                        emailString += "</table>";
                        $scope.QCSDetails.EMAIL_TEMPLATE = emailString;
                        $scope.QCSDetails.EMAIL_URL = window.location.href.substring(0, window.location.href.length - 1);
                    }

                    var params = {
                        "qcsdetails": $scope.QCSDetails,
                        "sessionid": userService.getUserToken()
                    };

                    if ($scope.workflowObj.workflowID) {
                        params.qcsdetails.WF_ID = $scope.workflowObj.workflowID;
                    }

                    //if (!params.qcsdetails.WF_ID > 0) {
                    //    growlService.growl('Please select Workflow', "inverse");
                    //    return;
                    //}
                    reportingService.SaveQCSDetails(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //if (vendorAssignedItems.length > 0) {
                                //    $scope.saveVendorAssignments(vendorAssignedItems, response.objectID);
                                //}
                                if (val == 1) {
                                    $scope.goToQCSList($scope.reqId);
                                    growlService.growl("Details saved successfully.", "success");
                                } else {
                                    var url = $state.href("import-qcs", { "reqID": $scope.reqId, "qcsID": response.objectID });
                                    window.open(url, '_self');
                                }
                            }
                        });
                });
            };

            $scope.isQCSImportFormdisabled = true;

            $scope.checkIsFormDisable = function () {
                $scope.isQCSImportFormdisabled = false;
                if ($scope.itemWorkflow.length == 0) {
                    $scope.isQCSImportFormdisabled = true;
                } else {
                    if (($scope.QCSDetails.CREATED_BY == +userService.getUserId() || $scope.QCSDetails.MODIFIED_BY == +userService.getUserId()) && $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                        $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
                        $scope.isQCSImportFormdisabled = true;
                    }
                }
            };

            $scope.GetQCSDetails = function () {
                var params = {
                    "qcsid": $scope.qcsID,
                    "sessionid": userService.getUserToken()
                };
                reportingService.GetQCSDetails(params)
                    .then(function (response) {
                        $scope.QCSDetails = response;

                        if ($scope.QCSDetails.WF_ID > 0) {
                            $scope.workflowObj.workflowID = $scope.QCSDetails.WF_ID;
                        }

                        if ($scope.QCSDetails.REQ_JSON) {
                            $scope.qcsRequirementDetails = JSON.parse($scope.QCSDetails.REQ_JSON);
                        }

                        $scope.getRequirementData();
                        $scope.getItemWorkflow();
                    });
            };

            if ($scope.qcsID > 0) {
                $scope.GetQCSDetails();
            }

            $scope.goToQCSList = function (reqID) {
                var url = $state.href("list-qcs", { "reqID": $scope.reqId });
                window.open(url, '_self');
            };

            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                //import
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule === $scope.WorkflowModule && item.isActive === 1) {
                                $scope.workflowList.push(item);
                                $scope.workflowListDeptWise.push(item);
                            }
                        });
                        var id = userService.getSelectedUserDepartmentDesignation().deptID;
                        if (userService.getUserObj().isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = [];
                            $scope.workflowListDeptWise.forEach(function (wf, idx) {
                                $scope.deptIDs.forEach(function (dep) {
                                    if (dep == wf.deptID) {
                                        $scope.workflowList.push(wf);
                                    }
                                });
                            });

                            //$scope.workflowList = $scope.workflowList.filter(function (item) {
                            //    return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                            //});
                        }
                    });
            };

            $scope.getWorkflows();

            $scope.getItemWorkflow = function () {
                workflowService.getItemWorkflow(0, $scope.qcsID, $scope.WorkflowModule)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        $scope.checkIsFormDisable();
                        //  $scope.checkIsPODisable();
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            var rejectedArr = _.filter($scope.itemWorkflow[0].WorkflowTracks, function (item) { return item.REJECTED_ORDER > 0 });
                            var rejectedOrder = 0;
                            if (rejectedArr && rejectedArr.length > 0) {
                                rejectedOrder = rejectedArr[0].REJECTED_ORDER;
                                var rejectedApprovedBy = _.find($scope.itemWorkflow[0].WorkflowTracks, { order: rejectedOrder }).approverDetails.approvedBy;
                                var rejectedModifiedDate = _.find($scope.itemWorkflow[0].WorkflowTracks, { order: rejectedOrder }).dateModified;
                            }

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                                if (rejectedOrder >= track.order) {
                                    track.approverDetails.approvedBy = rejectedApprovedBy;
                                    track.dateModified = rejectedModifiedDate;
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }
                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;
                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && !count) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && !count) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && !$scope.currentStep) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });

            };

            $scope.updateTrack = function (step, status) {
                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                step.IS_LAST_APPROVER = false;
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                    step.IS_LAST_APPROVER = true;
                } else {
                    $scope.disableAssignPR = true;
                }

                $scope.emailTemplate = document.getElementById("emailTable").innerHTML.replace(/<\!--.*?-->/g, "").replace(/ng-repeat="vendorAssignmentObj in vendorAssignmentList"*/g, "").replaceAll("\n", "");
                $scope.emailTemplate = $scope.emailTemplate.replace("URL_QCS", window.location.href);
                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (step.comments == null || step.comments === "" || step.comments == undefined) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionid;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;
                step.subModuleName = $scope.requirementDetails.title;
                step.subModuleID = $scope.reqId;
                step.IS_VALID = $scope.QCSDetails.LOCK_REQ;
                step.EMAIL_TEMPLATE = $scope.emailTemplate;
                step.EMAIL_URL = window.location.href;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            location.reload();
                            //$state.go('approval-qcs-list');
                        }
                    });
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionid }))
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    });
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };

            /*region end WORKFLOW*/


            $scope.editFormPage = function () {
                $scope.editForm = !$scope.editForm;
                $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                    $scope.fieldValidation(vendor);
                });
            };

            $scope.fieldValidation = function (vendor) {
                if ($scope.editForm && vendor.INCO_TERMS) {
                    auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                        .then(function (response) {
                            vendor.INCO_TERMS_CONFIG = response;

                        });
                } else {
                    vendor.isEdit = false;
                }
            };

            $scope.fieldValidationList = function (vendor) {
                if (vendor.INCO_TERMS) {
                    auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                        .then(function (response) {
                            vendor.INCO_TERMS_CONFIG = response;
                        });
                }
            };

            $scope.unitPriceCalculation = function (item) {
                item.revUnitPrice = item.unitPrice;
            };

            $scope.loadTable = false;

            $scope.getRequirementData = function () {
                auctionsService.getReportrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId(), 'excludePriceCap': 1 })
                    .then(function (response) {
                        $scope.getVendorItemAssignments();
                        $scope.qcsItems = [];
                        $scope.qcsCoreItems = [];
                        if (response.CB_TIME_LEFT > 0) {
                            response.status = "STARTED";

                        }

                        let productIds = _.uniq(_.map(response.listRequirementItems, 'catalogueItemID')).join();
                        auctionsService.getLPPByProductIds({ "compid": $scope.companyId, "sessionid": userService.getUserToken(), 'productids': productIds, 'ignorereqid': $scope.reqId ? $scope.reqId : '0' })
                            .then(function (requirementItemsLPPData) {

                                response.listRequirementItems.forEach(function (reqItem, index) {
                                    if (!$scope.qcsID) {
                                        if (requirementItemsLPPData && requirementItemsLPPData.length > 0) {
                                            let filteredLPPItem = requirementItemsLPPData.filter(function (lppItem) {
                                                return lppItem.CATALOGUE_ITEM_ID === reqItem.catalogueItemID;
                                            });

                                            if (filteredLPPItem && filteredLPPItem.length > 0 && filteredLPPItem[0].REV_UNIT_PRICE && !reqItem.LPPValue) {
                                                //reqItem.LPPValue = filteredLPPItem[0].REV_UNIT_PRICE;
                                                if ($scope.getRequirementCurrencyFactor(filteredLPPItem[0].REQ_CURRENCY) === $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency)) {
                                                    reqItem.LPPValue = filteredLPPItem[0].REV_UNIT_PRICE;
                                                } else {
                                                    reqItem.LPPValue = filteredLPPItem[0].REV_UNIT_PRICE * (filteredLPPItem[0].REQ_CURRENCY_FACTOR / $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency));
                                                }
                                            }
                                        }

                                    } else if ($scope.qcsID > 0 && $scope.qcsRequirementDetails) {
                                        var tempItem = _.filter($scope.qcsRequirementDetails.listRequirementItems, function (qcsReqItem) { return qcsReqItem.itemID === reqItem.itemID; });
                                        if (tempItem && tempItem.length > 0) {
                                            reqItem.LPPValue = tempItem[0].LPPValue;
                                        }
                                    }
                                });

                                //if (requirementItemsLPPData && requirementItemsLPPData.length > 0) {
                                //    response.listRequirementItems.forEach(function (reqItem, index) {
                                //        if (!$scope.qcsID) {
                                //            let filteredLPPItem = requirementItemsLPPData.filter(function (lppItem) {
                                //                return lppItem.CATALOGUE_ITEM_ID === reqItem.catalogueItemID;
                                //            });

                                //            if (filteredLPPItem && filteredLPPItem.length > 0 && filteredLPPItem[0].REV_UNIT_PRICE && !reqItem.LPPValue) {
                                //                reqItem.LPPValue = filteredLPPItem[0].REV_UNIT_PRICE;
                                //            }
                                //        } else if ($scope.qcsID > 0 && $scope.qcsRequirementDetails) {
                                //            var tempItem = _.filter($scope.qcsRequirementDetails.listRequirementItems, function (qcsReqItem) { return qcsReqItem.itemID === reqItem.itemID; });
                                //            if (tempItem && tempItem.length > 0) {
                                //                reqItem.LPPValue = tempItem[0].LPPValue;
                                //            }
                                //        }
                                //    });
                                //}
                            });

                        response.listRequirementItems.forEach(function (reqItem, index) {
                            reqItem.selectedItemNumArr = [];
                            reqItem.prlineItemarr = [];
                            //  reqItem.prlineItemarrTemp = reqItem.PR_ITEM_ID_STRING.split(',');
                            if (reqItem.isCoreProductCategory == 1 && reqItem.IS_SERVICE_PRODUCT == 1 && reqItem.PR_ITEM_ID_STRING) {
                                reqItem.prlineItemarr = JSON.parse(reqItem.PR_ITEM_ID_STRING);

                                reqItem.ITEM_PR_QUANTITY = _.sumBy(reqItem.prlineItemarr, 'REQUIRED_QUANTITY');
                            }
                            reqItem.isVisible = true;
                            var qcsItem = {
                                productId: reqItem.catalogueItemID,
                                itemID: reqItem.itemID,
                                itemName: reqItem.productIDorName,
                                productQuantity: reqItem.productQuantity,
                                isCoreProductCategory: reqItem.isCoreProductCategory,
                                prQuantity: reqItem.ITEM_PR_QUANTITY,
                                prNumber: reqItem.ITEM_PR_NUMBER,
                                PR_ITEM_ID_STRING: reqItem.PR_ITEM_ID_STRING,
                                IS_SERVICE_PRODUCT: reqItem.IS_SERVICE_PRODUCT,
                                isSelected: true
                            };

                            $scope.qcsItems.push(qcsItem);
                        });

                        $scope.qcsCoreItems = $scope.qcsItems.filter(function (qcsitem) {
                            return qcsitem.isCoreProductCategory > 0;
                        });

                        getProductContracts();

                        if (response) {
                            $scope.qcsVendors = [{
                                vendorID: 0,
                                vendorCompany: 'Select Vendor',
                                isSelected: true
                            }];

                            $scope.requirementDetails = response;
                            $scope.requirementDetails.qcsMultipleAttachments = [];
                            if ($scope.qcsRequirementDetails && $scope.qcsRequirementDetails.qcsMultipleAttachments && $scope.qcsRequirementDetails.qcsMultipleAttachments.length > 0) {
                                $scope.requirementDetails.qcsMultipleAttachments = $scope.qcsRequirementDetails.qcsMultipleAttachments;
                            }

                            $scope.requirementDetails.listRequirementItems.sort(function (a, b) {
                                return b.isCoreProductCategory - a.isCoreProductCategory;
                            });

                            $scope.vendorWidth = Math.floor(12 / ($scope.requirementDetails.auctionVendors.length + 1));
                            $scope.requirementDetails.auctionVendors = _.filter($scope.requirementDetails.auctionVendors, function (vendor) { return vendor.companyName !== 'PRICE_CAP'; });
                            if ($scope.qcsID > 0 && $scope.qcsRequirementDetails) {
                                $scope.requirementDetails.customerComment = $scope.qcsRequirementDetails.customerComment;
                                $scope.requirementDetails.includeGstInCal = $scope.qcsRequirementDetails.includeGstInCal;
                                $scope.requirementDetails.listRequirementItems.forEach(function (item, index) {
                                    item.maxHeight = '';
                                    var tempItem = _.filter($scope.qcsRequirementDetails.listRequirementItems, function (qcsReqItem) { return qcsReqItem.itemID === item.itemID; });
                                    if (tempItem && tempItem.length > 0) {
                                        item.qtyDistributed = tempItem[0].qtyDistributed;
                                    }
                                });



                                $scope.requirementDetails.auctionVendors.forEach(function (vendor, index) {
                                    vendor.isVisible = true;
                                    vendor.currencyRate = 1;
                                    var qcsVendor = {
                                        vendorID: vendor.vendorID,
                                        vendorCompany: vendor.companyName,
                                        vendorCurrency: vendor.selectedVendorCurrency,
                                        isSelected: true
                                    };

                                    $scope.qcsVendors.push(qcsVendor);

                                    vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                        if (vendorItem.productQuotationTemplateJson) {
                                            vendorItem.productQuotationTemplateJson = JSON.parse(vendorItem.productQuotationTemplateJson);
                                        }
                                    });

                                    var tempQCSVendor = _.filter($scope.qcsRequirementDetails.auctionVendors, function (qcsVendor) { return qcsVendor.vendorID === vendor.vendorID; });
                                    if (tempQCSVendor && tempQCSVendor.length > 0) {

                                        vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                            vendorItem.selectedItemNumArr = [];
                                            tempQCSVendor[0].listRequirementItems.forEach(function (I, idx) {
                                                if (vendor.vendorID === tempQCSVendor[0].vendorID && vendorItem.itemID == I.itemID && vendorItem.isCoreProductCategory && vendorItem.PR_ITEM_ID_STRING && vendorItem.PR_ITEM_ID_STRING != '' && vendorItem.PR_ITEM_ID_STRING != null && vendorItem.PR_ITEM_ID_STRING != undefined) {
                                                    vendorItem.prlineItemarr = JSON.parse(I.PR_ITEM_ID_STRING);
                                                    vendorItem.selectedItemNumArr = vendorItem.prlineItemarr;
                                                }
                                            })

                                        });

                                        vendor.currencyRate = tempQCSVendor[0].currencyRate;
                                        //vendor.currencyRate = tempQCSVendor[0].vendorCurrencyFactor;
                                        vendor.customerComment = tempQCSVendor[0].customerComment;
                                        vendor.revChargeAny = tempQCSVendor[0].revChargeAny;
                                        vendor.seaFreight = tempQCSVendor[0].seaFreight;
                                        vendor.insurance = tempQCSVendor[0].insurance;
                                        vendor.basicCustomDuty = tempQCSVendor[0].basicCustomDuty;
                                        vendor.antiDumping = tempQCSVendor[0].antiDumping;
                                        vendor.socialWelfareCharge = tempQCSVendor[0].socialWelfareCharge;
                                        vendor.clearingCharges = tempQCSVendor[0].clearingCharges;
                                        vendor.socialWelfareCharge = tempQCSVendor[0].socialWelfareCharge;
                                        vendor.localTransportCharges = tempQCSVendor[0].localTransportCharges;
                                        vendor.iGst = tempQCSVendor[0].iGst;

                                        auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                                            .then(function (response) {

                                                vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                                    var newArray = response.filter(function (res) { return res.ProductId == vendorItem.catalogueItemID; });
                                                    var tempQCSVendorItem = _.filter(tempQCSVendor[0].listRequirementItems, function (qcsVendorItem) { return qcsVendorItem.itemID === vendorItem.itemID; });
                                                    if (newArray && newArray.length > 0) {
                                                        if (newArray[0].IS_CUSTOMER_EDIT == 1) {

                                                            if (tempQCSVendorItem && tempQCSVendorItem.length > 0) {
                                                                vendorItem.vendorID = vendor.vendorID;
                                                                $scope.IncoTermsEditableForOtherCharges.push(vendorItem);
                                                                vendorItem.unitPrice = tempQCSVendorItem[0].unitPrice;
                                                                vendorItem.revUnitPrice = tempQCSVendorItem[0].revUnitPrice;
                                                            }
                                                        }
                                                    }

                                                    vendorItem.qtyDistributed = tempQCSVendorItem[0].qtyDistributed;
                                                });
                                            });
                                    }
                                });
                            } else {
                                $scope.requirementDetails.includeGstInCal = true;
                                $scope.requirementDetails.auctionVendors.forEach(function (vendor, index) {
                                    vendor.currencyRate = 1;
                                    vendor.isVisible = true;
                                    var qcsVendor = {
                                        vendorID: vendor.vendorID,
                                        vendorCompany: vendor.companyName,
                                        vendorCurrency: vendor.selectedVendorCurrency,
                                        isSelected: true
                                    };

                                    $scope.qcsVendors.push(qcsVendor);
                                    vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                        //$scope.displayLeastItemPriceColor();
                                        vendorItem.selectedItemNumArr = [];
                                        if (vendorItem.isCoreProductCategory && vendorItem.PR_ITEM_ID_STRING && vendorItem.PR_ITEM_ID_STRING != '' && vendorItem.PR_ITEM_ID_STRING != null && vendorItem.PR_ITEM_ID_STRING != undefined) {
                                            vendorItem.prlineItemarr = JSON.parse(vendorItem.PR_ITEM_ID_STRING);
                                            if (vendorItem.prlineItemarr.length == 1) {
                                                vendorItem.prlineItemarr[0].isChecked = true;
                                                vendorItem.selectedItemNumArr.push(vendorItem.prlineItemarr[0]);
                                            }
                                        }
                                        if (vendorItem.productQuotationTemplateJson) {
                                            vendorItem.productQuotationTemplateJson = JSON.parse(vendorItem.productQuotationTemplateJson);
                                        }
                                    });
                                });
                            }
                        }
                    });
            };

            if (!$scope.qcsID) {
                $scope.getRequirementData();
            }

            $scope.isQtyDistributedforItem = function (item, vendor) {
                var isTrue = false;
                if (vendor && vendor.isQuotationRejected == 0) {
                    var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
                    if (vendorItemPrices && vendorItemPrices.length > 0) {
                        if (vendorItemPrices[0] && vendorItemPrices[0].unitPrice > 0) {
                            return isTrue = true;
                        }

                    } else {
                        return isTrue = false;
                    }
                }
            }

            $scope.checkQuantity = function (item, vendor, servObj, isLineItem) {
                var assignedQTy = 0;
                let maxAssignQty = item.productQuantity;
                // let maxAssignQty = item.ITEM_PR_QUANTITY;
                item.error = '';
                $scope.isBreakLoop = false;
                var isAnyOtherVendors = 0;


                if (!maxAssignQty) {
                    maxAssignQty = item.productQuantity;
                }
                if (item.IS_SERVICE_PRODUCT == 1) {
                    $scope.requirementDetails.auctionVendors.forEach(function (vendor1, vendIdx) {
                        vendor1.listRequirementItems.forEach(function (vendorItem, idx) {
                            if (vendor1.vendorID != vendor.vendorID && servObj) {
                                isAnyOtherVendors = _.result(_.find(vendorItem.prlineItemarr, function (chr) {
                                    if (servObj.ITEM_ID == chr.ITEM_ID && servObj.ITEM_NUM == chr.ITEM_NUM && servObj.SERVICE_LINE_ITEM == chr.SERVICE_LINE_ITEM && chr.isChecked) {
                                        return chr.isChecked == true;
                                    }

                                }), 'ITEM_ID');
                            }
                            if (isAnyOtherVendors) {
                                swal("Warning!", "Already assigned to other vendor", "error");
                                servObj.isChecked = false;
                                $scope.isBreakLoop = true;
                                return;
                            } else {
                                if (vendorItem.itemID == item.itemID && vendor1.vendorID == vendor.vendorID && vendorItem.qtyDistributed != undefined) {
                                    assignedQTy += parseFloat(vendorItem.qtyDistributed);
                                }
                            }
                            if ($scope.isBreakLoop) {
                                return;
                            }

                        });
                        if ($scope.isBreakLoop) {
                            return;
                        }
                    });
                    if ($scope.qcsID > 0 && $scope.qcsRequirementDetails) {

                        $scope.qcsRequirementDetails.auctionVendors.forEach(function (vend, vendIdx) {

                            vend.listRequirementItems.forEach(function (it, itdx) {
                                if (it.itemID == item.itemID && vendor.vendorID == vend.vendorID) {
                                    // item.selectedItemNumArr = JSON.parse(it.PR_ITEM_ID_STRING);
                                    it.selectedItemNumArr = JSON.parse(it.PR_ITEM_ID_STRING);
                                }
                            })

                        })
                    }

                    var servMaxAssignQty = 0;
                    var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
                    if (vendorItemPrices[0].selectedItemNumArr && vendorItemPrices[0].selectedItemNumArr.length == 0 && !isLineItem) {
                        swal("Error!", "Please select line item", 'error');
                        var data = $scope.getVendorItemPrices(item, vendor);
                        $scope.requirementDetails.auctionVendors.forEach(function (vendor1, vendIdx) {
                            vendor1.listRequirementItems.forEach(function (vendorItem, idx) {
                                if (vendorItem.itemID == data.itemID && vendor.vendorID == vendor1.vendorID) {
                                    vendorItem.qtyDistributed = 0;
                                }
                            });
                        });
                        return;
                    }
                    if (isLineItem && servObj) {
                        if (servObj.isChecked) {
                            vendorItemPrices[0].selectedItemNumArr.push(servObj);
                        } else {
                            var tempindex = vendorItemPrices[0].selectedItemNumArr.indexOf(servObj);
                            if (tempindex > -1) {
                                vendorItemPrices[0].selectedItemNumArr.splice(tempindex, 1);

                                var data = $scope.getVendorItemPrices(item, vendor);
                                $scope.requirementDetails.auctionVendors.forEach(function (vendor1, vendIdx) {
                                    vendor1.listRequirementItems.forEach(function (vendorItem, idx) {
                                        if (vendorItem.itemID == data.itemID && vendor.vendorID == vendor1.vendorID) {
                                            vendorItem.qtyDistributed = 0;
                                        }
                                    });
                                });
                                assignedQTy = 0;
                                $scope.requirementDetails.auctionVendors.forEach(function (vendor, vendIdx) {
                                    vendor.listRequirementItems.forEach(function (vendorItem, idx) {
                                        if (vendorItem.itemID == data.itemID && vendorItem.qtyDistributed != undefined) {
                                            assignedQTy += parseFloat(vendorItem.qtyDistributed);
                                        }
                                    });
                                });
                            }
                        }
                    } else if (item.prlineItemarr.length == 1) {
                        assignedQTy = 0;
                        var data = $scope.getVendorItemPrices(item, vendor);
                        $scope.requirementDetails.auctionVendors.forEach(function (vendor, vendIdx) {
                            vendor.listRequirementItems.forEach(function (vendorItem, idx) {
                                if (vendorItem.itemID == data.itemID && vendorItem.qtyDistributed != undefined) {
                                    assignedQTy += parseFloat(vendorItem.qtyDistributed);
                                }
                            });
                        });
                    }
                    if (vendorItemPrices[0].selectedItemNumArr.length > 0) {
                        var data = _.uniqBy(vendorItemPrices[0].selectedItemNumArr, 'ITEM_NUM');
                        servMaxAssignQty = _.sumBy(data, 'REQUIRED_QUANTITY');
                    }

                    if (assignedQTy > servMaxAssignQty) {
                        var data = $scope.getVendorItemPrices(item, vendor);
                        $scope.requirementDetails.auctionVendors.forEach(function (vendor1, vendIdx) {
                            vendor1.listRequirementItems.forEach(function (vendorItem, idx) {
                                if (vendorItem.itemID == data.itemID && vendor.vendorID == vendor1.vendorID) {
                                    vendorItem.qtyDistributed = 0;
                                }
                            });
                        });

                        swal("Warning!", "Distributing Quantity should not exceed more than Line Item Quantity " + servMaxAssignQty, "error");
                        item.error = "Distributing Quantity should not exceed more than Line Item Quantity" + servMaxAssignQty;
                        $scope.isQcsSaveDisable = true;
                        return;
                    } else {
                        $scope.isQcsSaveDisable = false;
                    }


                } else if (item.IS_SERVICE_PRODUCT == 0) {
                    $scope.requirementDetails.auctionVendors.forEach(function (vendor, vendIdx) {
                        vendor.listRequirementItems.forEach(function (vendorItem, idx) {
                            if (vendorItem.itemID == item.itemID && vendorItem.qtyDistributed != undefined) {
                                assignedQTy += parseFloat(vendorItem.qtyDistributed);
                            }
                        });
                    });
                    if (assignedQTy > maxAssignQty) {
                        var data = $scope.getVendorItemPrices(item, vendor);
                        $scope.requirementDetails.auctionVendors.forEach(function (vendor1, vendIdx) {
                            vendor1.listRequirementItems.forEach(function (vendorItem, idx) {
                                if (vendorItem.itemID == data.itemID && vendor.vendorID == vendor1.vendorID) {
                                    vendorItem.qtyDistributed = 0;
                                }
                            });
                        });

                        swal("Warning!", "Distributing Quantity should not exceed more than RFQ Quantity " + maxAssignQty, "error");
                        item.error = "Distributing Quantity should not exceed more than RFQ Quantity" + maxAssignQty;
                        $scope.isQcsSaveDisable = true;
                        return;
                    } else {
                        $scope.isQcsSaveDisable = false;
                    }
                }

            };

            $scope.getVendorItemPrices = function (item, vendor) {
                var emptyObj = {
                    unitPrice: 0,
                    revUnitPrice: 0
                };
                var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
                if (vendorItemPrices && vendorItemPrices.length > 0) {
                    if (vendorItemPrices[0] && vendorItemPrices[0].productQuotationTemplateJson.length > 0) {
                        item.maxHeight = '200px';
                    }
                    return vendorItemPrices[0];
                } else {
                    return emptyObj;
                }

            };
            $scope.index = 0;
            $scope.getPRLineItems = function (item, vendor) {
                var arr = [];
                $scope.index++;
                if (vendor && item.isCoreProductCategory == 1 && vendor.isQuotationRejected == 0) {
                    var vendorItem = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
                    if (vendorItem && vendorItem.length > 0) {
                        if (vendorItem[0] && (vendorItem[0].PR_ITEM_ID_STRING != '' || vendorItem[0].PR_ITEM_ID_STRING != undefined || vendorItem[0].PR_ITEM_ID_STRING != null)) {
                            item.maxHeight = '200px';
                        }


                        return vendorItem[0].prlineItemarr;
                    } else {
                        return arr;
                    }
                }
                else {
                    return arr;
                }

            }

            $scope.getVendorTotalPriceWithoutTax = function (vendor, isAssignedQty) {
                var price = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    if (isItemVisible(item)) {
                        price += item.revUnitPrice * (isAssignedQty && item.isCoreProductCategory ? (item.qtyDistributed ? item.qtyDistributed : 0) : item.productQuantity);
                    }
                });

                return +price;
            };

            $scope.getVendorTotalLandingPrice = function (vendor) {
                var price = 0;
                price += $scope.getVendorTotalPriceWithoutTax(vendor);

                if (vendor.revChargeAny) {
                    price -= (+vendor.revChargeAny);
                }

                return price;
            };

            $scope.getVendorTotalUnitItemPrices = function (vendor) {
                var price = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    if (isItemVisible(item)) {
                        price += (+item.unitPrice);
                    }
                });

                return price;
            };

            $scope.getVendorTotalRevUnitItemPrices = function (vendor) {
                var price = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    if (isItemVisible(item)) {
                        price += (+item.revUnitPrice);
                    }
                });

                return price;
            };

            $scope.getVendorTotalTax = function (vendor, taxType) {
                var totalTax = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    if (isItemVisible(item)) {
                        if (taxType === 'CGST') {
                            totalTax += (item.revUnitPrice * (item.qtyDistributed ? item.qtyDistributed : item.productQuantity)) * (item.cGst / 100);
                        }

                        if (taxType === 'IGST') {
                            totalTax += (item.revUnitPrice * (item.qtyDistributed ? item.qtyDistributed : item.productQuantity)) * (item.iGst / 100);
                        }

                        if (taxType === 'SGST') {
                            totalTax += (item.revUnitPrice * (item.qtyDistributed ? item.qtyDistributed : item.productQuantity)) * (item.sGst / 100);
                        }
                    }
                });

                return totalTax;
            };

            $scope.isNonCoreItemEditable = function (item, vendor) {
                var isEditable = false;

                if (item && !item.isCoreProductCategory && vendor && vendor.INCO_TERMS && vendor.INCO_TERMS_CONFIG) {
                    vendor.INCO_TERMS_CONFIG.forEach(function (incoItem) {
                        vendor.listRequirementItems.forEach(function (vendorItem, itemIndex) {
                            if (item.catalogueItemID === incoItem.ProductId && item.catalogueItemID === vendorItem.catalogueItemID) {
                                if (incoItem.IS_CUSTOMER_EDIT) {
                                    isEditable = true;
                                }
                            }
                        });
                    });
                }

                return isEditable;
            };

            $scope.incoTermsPrice = function (vendor, type) {
                if (vendor.INCO_TERMS === type) {
                    return $scope.getVendorTotalPriceWithoutTax(vendor);
                } else {
                    return '';
                }
            };

            $scope.cifTotal = function (vendor, isAssignedQty) {
                var insurance = !vendor.insurance ? 0 : +vendor.insurance;
                var seaFreight = !vendor.seaFreight ? 0 : +vendor.seaFreight;
                var totalPrice = !$scope.getVendorTotalPriceWithoutTax(vendor, isAssignedQty) ? 0 : $scope.getVendorTotalPriceWithoutTax(vendor, isAssignedQty);
                return (insurance + totalPrice + seaFreight);
            };

            $scope.totalValueInINR = function (vendor, isAssignedQty) {
                var cifTotal = $scope.cifTotal(vendor, isAssignedQty);
                if (vendor.currencyRate <= 0) {
                    vendor.currencyRate = 1;
                }
                const currencyConversion = vendor.currencyRate ? +vendor.currencyRate : 1;
                return cifTotal * currencyConversion;
            };

            $scope.basicCustomDuty = function (vendor, isAssignedQty) {
                var tax = vendor.basicCustomDuty ? +vendor.basicCustomDuty : 0;
                return $scope.totalValueInINR(vendor, isAssignedQty) * (tax / 100);
            };

            $scope.socialWelfareTotal = function (vendor) {
                var tax = vendor.socialWelfareCharge ? +vendor.socialWelfareCharge : 0;
                return $scope.basicCustomDuty(vendor) * (tax / 100);
            };

            $scope.calculateImportIGST = function (vendor, isAssignedQty) {
                const currencyConversionRate = vendor.currencyRate ? +vendor.currencyRate : 1;
                var tax = vendor.iGst ? +vendor.iGst : 0;
                var dumping = vendor.antiDumping ? +vendor.antiDumping : 0;
                var socialWelfareCharge = vendor.socialWelfareCharge ? +vendor.socialWelfareCharge : 0;
                var totalINRValue = $scope.totalValueInINR(vendor, isAssignedQty) ? $scope.totalValueInINR(vendor, isAssignedQty) : 0;
                var basicCustomDuty = $scope.basicCustomDuty(vendor, isAssignedQty) ? $scope.basicCustomDuty(vendor, isAssignedQty) : 0;
                return (tax / 100) * (totalINRValue + basicCustomDuty + (dumping) + ((socialWelfareCharge / 100) * basicCustomDuty));
            };

            $scope.calculateTotal = function (vendor, includeGstInCal) {
                var totalPrice = 0, tax = 0;

                var locaTransportCharges = vendor.localTransportCharges ? +vendor.localTransportCharges : 0;
                var clearingCharges = vendor.clearingCharges ? +vendor.clearingCharges : 0;
                var dumping = vendor.antiDumping ? +vendor.antiDumping : 0;
                var socialWelfareCharge = vendor.socialWelfareCharge ? +vendor.socialWelfareCharge : 0;
                const currencyConversionRate = vendor.currencyRate ? +vendor.currencyRate : 1;
                var totalINRValue = $scope.totalValueInINR(vendor) ? $scope.totalValueInINR(vendor) : 0;
                var basicCustomDuty = $scope.basicCustomDuty(vendor) ? $scope.basicCustomDuty(vendor) : 0;

                totalPrice += totalINRValue + basicCustomDuty + (dumping * currencyConversionRate) + ((socialWelfareCharge / 100) * basicCustomDuty) + $scope.calculateImportIGST(vendor);

                if (!includeGstInCal) {
                    tax = $scope.calculateImportIGST(vendor);
                    totalPrice -= tax;
                }

                //vendor.landedCost = totalPrice + locaTransportCharges + clearingCharges;

                return totalPrice;
            };


            $scope.calculateLandedCost = function (vendor, includeGstInCal) {
                var landedCost = 0, tax = 0, totalPrice = 0;

                var locaTransportCharges = vendor.localTransportCharges ? +vendor.localTransportCharges : 0;
                var clearingCharges = vendor.clearingCharges ? +vendor.clearingCharges : 0;
                var dumping = vendor.antiDumping ? +vendor.antiDumping : 0;
                var socialWelfareCharge = vendor.socialWelfareCharge ? +vendor.socialWelfareCharge : 0;
                const currencyConversionRate = vendor.currencyRate ? +vendor.currencyRate : 1;
                var totalINRValue = $scope.totalValueInINR(vendor) ? $scope.totalValueInINR(vendor) : 0;
                var basicCustomDuty = $scope.basicCustomDuty(vendor) ? $scope.basicCustomDuty(vendor) : 0;

                totalPrice += totalINRValue + basicCustomDuty + (dumping * currencyConversionRate) + ((socialWelfareCharge / 100) * basicCustomDuty) + $scope.calculateImportIGST(vendor)
                    + locaTransportCharges + clearingCharges;

                if (!includeGstInCal) {
                    tax = $scope.calculateImportIGST(vendor);
                    totalPrice -= tax;
                }

                vendor.landedCost = totalPrice;
                landedCost = totalPrice;

                return landedCost;
            };

            $scope.calculateAssignedQtyLandedCost = function (vendor, includeGstInCal, currencyConversion) {
                var landedCost = 0, tax = 0, totalPrice = 0;

                var locaTransportCharges = vendor.localTransportCharges ? +vendor.localTransportCharges : 0;
                var clearingCharges = vendor.clearingCharges ? +vendor.clearingCharges : 0;
                var dumping = vendor.antiDumping ? +vendor.antiDumping : 0;
                var socialWelfareCharge = vendor.socialWelfareCharge ? +vendor.socialWelfareCharge : 0;
                const currencyConversionRate = vendor.currencyRate ? +vendor.currencyRate : 1;
                var totalINRValue = $scope.totalValueInINR(vendor, true) ? $scope.totalValueInINR(vendor, true) : 0;
                var basicCustomDuty = $scope.basicCustomDuty(vendor, true) ? $scope.basicCustomDuty(vendor, true) : 0;

                totalPrice += totalINRValue + basicCustomDuty + (dumping * currencyConversionRate) + ((socialWelfareCharge / 100) * basicCustomDuty) + $scope.calculateImportIGST(vendor, true)
                    + locaTransportCharges + clearingCharges;

                if (!includeGstInCal) {
                    tax = $scope.calculateImportIGST(vendor, true);
                    totalPrice -= tax;
                }

                vendor.assignedQtylandedCost = totalPrice;
                if (currencyConversion) {
                    const currencyConversion = vendor.vendorCurrencyFactor ? +vendor.vendorCurrencyFactor : 1;
                    vendor.assignedQtylandedCost = vendor.assignedQtylandedCost * currencyConversion;
                }

                landedCost = vendor.assignedQtylandedCost;
                return landedCost;
            };

            $scope.totalAssignedQty = function () {
                return _.sumBy($scope.requirementDetails.auctionVendors, function (o) { return o.assignedQtylandedCost });
            }


            $scope.calculateLandedCostForOtherCharges = function (vendor) {
                var totalPrice = 0;

                var locaTransportCharges = vendor.localTransportCharges ? +vendor.localTransportCharges : 0;
                var clearingCharges = vendor.clearingCharges ? +vendor.clearingCharges : 0;
                var dumping = vendor.antiDumping ? +vendor.antiDumping : 0;
                var socialWelfareCharge = vendor.socialWelfareCharge ? +vendor.socialWelfareCharge : 0;
                const currencyConversionRate = vendor.currencyRate ? +vendor.currencyRate : 1;
                var totalINRValue = $scope.totalValueInINR(vendor) ? $scope.totalValueInINR(vendor) : 0;
                var basicCustomDuty = $scope.basicCustomDuty(vendor) ? $scope.basicCustomDuty(vendor) : 0;

                totalPrice += totalINRValue + basicCustomDuty + (dumping * currencyConversionRate) + ((socialWelfareCharge / 100) * basicCustomDuty) + locaTransportCharges + clearingCharges;

                return totalPrice;
            };

            $scope.getOtherCharges = function (vendor) {
                var otherCharges = 0, totalPrice = 0;

                if (vendor) {
                    $scope.IncoTermsEditableForOtherCharges.forEach(function (incoItem, incoIndex) {
                        vendor.listRequirementItems.forEach(function (item, index) {
                            if (item.isCoreProductCategory == 0 && incoItem.catalogueItemID == item.catalogueItemID && vendor.vendorID == incoItem.vendorID && isItemVisible(item)) {
                                var locaTransportCharges = vendor.localTransportCharges ? +vendor.localTransportCharges : 0;
                                var clearingCharges = vendor.clearingCharges ? +vendor.clearingCharges : 0;
                                var dumping = vendor.antiDumping ? +vendor.antiDumping : 0;
                                var socialWelfareCharge = vendor.socialWelfareCharge ? +vendor.socialWelfareCharge : 0;
                                const currencyConversionRate = vendor.currencyRate ? +vendor.currencyRate : 1;
                                var totalINRValue = $scope.totalValueInINR(vendor) ? $scope.totalValueInINR(vendor) : 0;
                                var basicCustomDuty = $scope.basicCustomDuty(vendor) ? $scope.basicCustomDuty(vendor) : 0;

                                totalPrice += totalINRValue + basicCustomDuty + (dumping * currencyConversionRate) + ((socialWelfareCharge / 100) * basicCustomDuty) + locaTransportCharges + clearingCharges;
                            }
                        });
                    });

                    otherCharges += totalPrice;
                }

                return otherCharges;
            };

            $scope.getItemRank = function (item, vendor) {
                var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
                if (vendorItemPrices && vendorItemPrices.length > 0) {
                    return vendorItemPrices[0].itemRank;
                }
            };

            $scope.exportTechSpec = function () {
                setTimeout(function () {
                    tableToExcel('testTable', 'Comparitives');
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }, 3000);


            };

            $scope.isVendorVisible = function (qcsVendor) {
                qcsVendor.isSelected = !qcsVendor.isSelected;
                var vendorTemp = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.vendorID === qcsVendor.vendorID; });
                if (vendorTemp && vendorTemp.length > 0) {
                    vendorTemp[0].isVisible = qcsVendor.isSelected;
                }
            };

            $scope.isReqItemVisible = function (qcsItem) {
                qcsItem.isSelected = !qcsItem.isSelected;
                var itemTemp = _.filter($scope.requirementDetails.listRequirementItems, function (reqItem) { return reqItem.itemID === qcsItem.itemID; });
                if (itemTemp && itemTemp.length > 0) {
                    itemTemp[0].isVisible = qcsItem.isSelected;
                }
            };


            $scope.CalculateRankBasedOnLandingPrice = function (vendor) {
                vendor.landingPriceRank = 'NA';
                var validVendorsForRanking = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.landedCost > 0 && auctionVendor.isQuotationRejected === 0; });
                if (validVendorsForRanking && validVendorsForRanking.length > 0) {
                    var sortedVendors = _.orderBy(validVendorsForRanking, ['landedCost'], ['asc']);
                    var rank = _.findIndex(sortedVendors, function (vendor1) { return vendor1.vendorID === vendor.vendorID; });
                    if (rank >= 0) {
                        vendor.landingPriceRank = rank + 1;
                    }
                }

                return vendor.landingPriceRank;
            };

            $scope.UpdateOtherCharges = function () {

                if ($scope.workflowObj.workflowID) {
                    var requirementItem = [];
                    $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                        vendor.listRequirementItems.forEach(function (reqItem) {
                            if (reqItem.isCoreProductCategory == 1) { requirementItem.push(reqItem); }
                        });
                    });

                    var isQtyDistributed = false;
                    var qtydis = [];
                    if (requirementItem.length > 0) {
                        qtydis = requirementItem.filter(function (item) {
                            if (item.qtyDistributed == undefined || item.qtyDistributed == null || item.qtyDistributed == "") { } else { return item.qtyDistributed; }
                        });
                    }
                    if (qtydis.length > 0) {
                        $scope.SaveQCSDetails(1);
                        if ($scope.QCSREQIDS.length == 0) {
                            $scope.getCalculatedOtherCharges();
                        } else if ($scope.QCSREQIDS.length > 0) {
                            var Requirement = _.filter($scope.QCSREQIDS, function (req) { return req.IS_PRIMARY_ID === 1; });
                            if (Requirement[0].IS_PRIMARY_ID == $scope.QCSDetails.IS_PRIMARY_ID && $scope.qcsID > 0) {
                                $scope.getCalculatedOtherCharges();
                            }
                        }
                    } else {
                        swal("Error!", "Please validate Quantity Distribution for existing items", 'error');
                        return;
                    }
                } else {
                    $scope.SaveQCSDetails(1);
                    if ($scope.QCSREQIDS.length == 0) {
                        $scope.getCalculatedOtherCharges();
                    } else if ($scope.QCSREQIDS.length > 0) {
                        var Requirement = _.filter($scope.QCSREQIDS, function (req) { return req.IS_PRIMARY_ID === 1; });
                        if (Requirement[0].IS_PRIMARY_ID == $scope.QCSDetails.IS_PRIMARY_ID && $scope.qcsID > 0) {
                            $scope.getCalculatedOtherCharges();
                        }
                    }
                }

            };

            $scope.saveVendorOtherChargesObject = {
                vendorID: 0,
                DIFFERENTIAL_FACTOR: 0,
                requirementID: 0
            };

            $scope.getCalculatedOtherCharges = function () {

                $scope.saveVendorOtherCharges = [];

                $scope.requirementDetails.auctionVendors.forEach(function (item, index) {


                    item.DIFFERENTIAL_FACTOR = 0;
                    //item.DIFFERENTIAL_FACTOR = $scope.getOtherCharges(item);//$scope.calculateLandedCostForOtherCharges(item) - item.revVendorTotalPrice;
                    //item.DIFFERENTIAL_FACTOR = $scope.calculateLandedCost(item, $scope.requirementDetails.includeGstInCal) - item.revVendorTotalPrice;
                    item.DIFFERENTIAL_FACTOR = $scope.calculateLandedCost(item, $scope.requirementDetails.includeGstInCal) - item.revVendorTotalPrice;

                    $scope.saveVendorOtherChargesObject = {
                        vendorID: 0,
                        DIFFERENTIAL_FACTOR: 0,
                        requirementID: 0
                    };

                    $scope.saveVendorOtherChargesObject.vendorID = item.vendorID;
                    $scope.saveVendorOtherChargesObject.DIFFERENTIAL_FACTOR = item.DIFFERENTIAL_FACTOR;
                    $scope.saveVendorOtherChargesObject.requirementID = $scope.requirementDetails.requirementID;
                    $scope.saveVendorOtherCharges.push($scope.saveVendorOtherChargesObject);


                });

                var params =
                {
                    userID: userService.getUserId(),
                    vendorOtherChargesArr: $scope.saveVendorOtherCharges,
                    sessionID: userService.getUserToken()
                };

                auctionsService.saveVendorOtherCharges(params)
                    .then(function (response) {

                        //if (response.errorMessage === "") {
                        //    growlService.growl("Other Charges Updated Successfully", "success");
                        //} else {
                        //    growlService.growl("Other Charges Updation Failed", "inverse");
                        //}

                    });
            };

            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            $scope.routeToExcelView = function () {
                var url = $state.href("import-qcs-excel", { "reqID": $scope.reqId, "qcsID": $scope.qcsID });
                window.open(url, '_blank');
            };

            $scope.Validate = function (value, vendor) {
                if (+value <= 0) {
                    growlService.growl("Please enter value greater than zero", "inverse");
                    return vendor.currencyRate = 0.01;
                }
            };

            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                $scope.selectedTemplate.TEMPLATE_NAME = 'PRM_DEFAULT';
                auctionsService.getRequirementSettings({ "reqid": $scope.reqId, "sessionid": $scope.sessionid })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var template = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'TEMPLATE_ID';
                            });

                            if (template && template.length > 0) {
                                $scope.selectedTemplate.TEMPLATE_ID = template[0].REQ_SETTING_VALUE;
                            }
                        }

                        if ($scope.selectedTemplate.TEMPLATE_ID || $scope.selectedTemplate.TEMPLATE_NAME) {
                            $scope.GetPRMTemplateFields();
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var params = {
                    "templateid": $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0,
                    "templatename": $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : '',
                    "sessionid": $scope.sessionid
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });
                });
            };

            $scope.cloneItemVendorAssignment = function (itemVendorObj) {
                $scope.vendorAssignmentList.push({
                    qcsVendorItemId: 0,
                    vendorID: 0,
                    vendorName: 'Select Vendor',
                    itemID: itemVendorObj.itemID,
                    assignedQty: itemVendorObj.assignedQty,
                    assignedPrice: itemVendorObj.assignedPrice,
                    poID: ''
                });
            };

            $scope.saveVendorAssignments = function (items, qcsID) {
                var itemsFinal = [];
                if (qcsID == 0 || qcsID == undefined || qcsID == null || qcsID == '') {
                    QCSID = $stateParams.qcsID;
                } else {
                    QCSID = qcsID;
                }
                if (items && items.length > 0) {
                    items.forEach(function (item, index) {
                        itemsFinal.push({
                            QCS_ID: QCSID,
                            REQ_ID: $stateParams.reqID,
                            VENDOR_ID: item.vendorID,
                            ITEM_ID: item.itemID,
                            ASSIGN_QTY: item.assignedQty,
                            ASSIGN_PRICE: item.assignedPrice,
                            TOTAL_PRICE: item.totalPrice

                        });
                    });


                    var params =
                    {
                        items: itemsFinal,
                        sessionID: userService.getUserToken()
                    };

                    auctionsService.saveQCSVendorAssignments(params)
                        .then(function (response) {
                            //growlService.growl("Details saved successfully.", "success");
                        });
                }

            };

            $scope.getVendorItemAssignments = function () {
                $scope.qcsPOVendors = [];
                $scope.vendorAssignmentList = [];
                var params =
                {
                    qcsid: $stateParams.qcsID,
                    reqid: $stateParams.reqID,
                    userid: 0,
                    sessionid: userService.getUserToken()
                };

                auctionsService.getQCSVendorItemAssignments(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                let itemTemp = $scope.qcsItems.filter(function (qcsitem) {
                                    return qcsitem.itemID == item.ITEM_ID;
                                });

                                let vendorTemp = $scope.qcsVendors.filter(function (qcsvendor) {
                                    return qcsvendor.vendorID == item.VENDOR_ID;
                                });

                                $scope.vendorAssignmentList.push({
                                    qcsVendorItemId: item.QCS_VENDOR_ITEM_ID,
                                    vendorID: item.VENDOR_ID,
                                    vendorName: vendorTemp ? vendorTemp[0].vendorCompany : '',
                                    itemID: item.ITEM_ID,
                                    item: itemTemp ? itemTemp[0] : null,
                                    assignedQty: item.ASSIGN_QTY,
                                    assignedPrice: item.ASSIGN_PRICE,
                                    totalPrice: (item.ASSIGN_QTY * item.ASSIGN_PRICE), //item.TOTAL_PRICE,
                                    poID: item.PO_ID,
                                    currency: vendorTemp ? vendorTemp[0].vendorCurrency : ''
                                });
                            });
                            $scope.loadTable = true;
                        } else {
                            $scope.requirementDetails.listRequirementItems.forEach(function (reqItem, index) {
                                if (reqItem.isCoreProductCategory > 0) {
                                    $scope.requirementDetails.auctionVendors.forEach(function (reqVendor, index) {
                                        if ($scope.getItemRank(reqItem, reqVendor) == 1) {
                                            let itemTemp = $scope.qcsItems.filter(function (qcsitem) {
                                                return qcsitem.itemID == reqItem.itemID;
                                            });

                                            let vendorTemp = $scope.qcsVendors.filter(function (qcsvendor) {
                                                return qcsvendor.vendorID == reqVendor.vendorID;
                                            });

                                            //$scope.vendorAssignmentList.push({
                                            //    qcsVendorItemId: 0,
                                            //    vendorID: reqVendor.vendorID,
                                            //    vendorName: vendorTemp ? vendorTemp[0].vendorCompany : '',
                                            //    itemID: reqItem.itemID,
                                            //    assignedQty: reqItem.productQuantity,
                                            //    assignedPrice: $scope.getVendorItemPrices(reqItem, reqVendor).revUnitPrice,
                                            //    totalPrice: ($scope.getVendorItemPrices(reqItem, reqVendor).revUnitPrice * item.productQuantity),
                                            //    poID: ''
                                            //});
                                        }
                                    });
                                }
                            });
                            $scope.loadTable = true;
                        }
                    });
            };

            $scope.getItemRFQQuantity = function (itemId) {
                let quantity = 0;
                let itemTemp = $scope.qcsCoreItems.filter(function (item) {
                    return item.itemID == itemId;
                });

                if (itemTemp && itemTemp.length > 0) {
                    quantity = itemTemp[0].productQuantity;
                }

                return quantity;
            };

            $scope.qcsVendorItemQuantityChange = function (item) {
                let itemId = item.itemID;
                let assignedQty = 0;
                $scope.vendorAssignmentList.forEach(function (item1, index) {
                    if (item1.itemID == itemId) {
                        assignedQty = assignedQty + (+item.assignedQty);
                    }
                });


                let itemTemp = $scope.qcsCoreItems.filter(function (item) {
                    return item.itemID == itemId;
                });

                if (itemTemp && itemTemp.length > 0) {
                    itemTemp[0].usedQty = itemTemp[0].productQuantity - assignedQty;
                }

                console.log(itemTemp);
            };

            $scope.getItemVendorPrice = function (itemVendorObj) {
                if (itemVendorObj.vendorID && itemVendorObj.itemID) {

                    let reqItem = $scope.requirementDetails.listRequirementItems.filter(function (item) {
                        return item.itemID == itemVendorObj.itemID;
                    });

                    let reqVendor = $scope.requirementDetails.auctionVendors.filter(function (vendor) {
                        return vendor.vendorID == itemVendorObj.vendorID;
                    });

                    itemVendorObj.assignedPrice = $scope.getVendorItemPrices(reqItem[0], reqVendor[0]).revUnitPrice;
                }

                return itemVendorObj.assignedPrice;
            };

            $scope.selectVendorForItemVendor = function (vendor) {
                $scope.vendorAssignmentList.forEach(function (item, index) {
                    item.vendorID = vendor.vendorID;
                    item.assignedPrice = $scope.getItemVendorPrice({ itemID: item.itemID, vendorID: vendor.vendorID });
                });
            };

            $scope.goToVendorPo = function (vendor) {
                let itemsTemp = $scope.vendorAssignmentList.filter(function (qcsitem) {
                    return !qcsitem.poID && qcsitem.vendorID == vendor.vendorID;
                });

                if (itemsTemp && itemsTemp.length > 0) {
                    var url = $state.href("po", { "reqID": $stateParams.reqID, "vendorID": vendor.vendorID, "poID": 0 });
                    window.open(url, '_blank');
                } else {
                    growlService.growl("No items found to create PO.", "inverse");
                }
            };

            $scope.viewVendorPo = function (vendor) {
                var url = $state.href("po", { "reqID": $stateParams.reqID, "vendorID": vendor.vendorID, "poID": vendor.poID });
                window.open(url, '_blank');
            };

            //$scope.checkIsPODisable = function () {
            //    //let doShow = true;
            //    //if ($scope.itemWorkflow.length > 0) {
            //    //    if ($scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
            //    //        $scope.itemWorkflow[0].WorkflowTracks[0].status === "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
            //    //        doShow = false;
            //    //    }
            //    //}

            //    //return doShow;

            //    let doShow = false;
            //    var approverCount = 0;

            //    if ($scope.itemWorkflow.length > 0) {
            //        approverCount = $scope.itemWorkflow[0].WorkflowTracks.length;
            //        if ($scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
            //            $scope.itemWorkflow[0].WorkflowTracks[approverCount - 1].status === "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[approverCount - 1].order == approverCount && $scope.itemWorkflow[0].workflowID > 0) {
            //            doShow = true;
            //        }
            //    }

            //    if (doShow && $scope.requirementDetails && $scope.requirementDetails.listRequirementItems
            //        && $scope.requirementDetails.listRequirementItems.length > 0) {
            //        let prAssociatedItems = $scope.requirementDetails.listRequirementItems.filter(function (item) {
            //            return item.ITEM_PR_NUMBER;
            //        });

            //        if (!prAssociatedItems || prAssociatedItems.length <= 0) {
            //            doShow = false;
            //        }
            //    }

            //    return doShow;
            //};

            /* COMMON ITEMS SPLIT LOGIC */

            $scope.checkIsPODisable = function () {
                //let doShow = true;
                //if ($scope.itemWorkflow.length > 0) {
                //    if ($scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                //        $scope.itemWorkflow[0].WorkflowTracks[0].status === "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
                //        doShow = false;
                //    }
                //}

                //return doShow;

                let doShow = false;
                var approverCount = 0;

                if ($scope.itemWorkflow.length > 0) {
                    approverCount = $scope.itemWorkflow[0].WorkflowTracks.length;
                    if ($scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                        $scope.itemWorkflow[0].WorkflowTracks[approverCount - 1].status === "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[approverCount - 1].order == approverCount && $scope.itemWorkflow[0].workflowID > 0) {
                        doShow = true;
                    }
                }

                if (doShow && $scope.requirementDetails && $scope.requirementDetails.listRequirementItems
                    && $scope.requirementDetails.listRequirementItems.length > 0) {
                    let prAssociatedItems = $scope.requirementDetails.listRequirementItems.filter(function (item) {
                        return item.ITEM_PR_NUMBER;
                    });

                    if (!prAssociatedItems || prAssociatedItems.length <= 0) {
                        doShow = false;
                    }
                }

                if ($scope.DraftPOList && $scope.DraftPOList.length > 0) {
                    var assignedQty = _.sumBy($scope.vendorAssignmentList, 'assignedQty');
                    var poQty = _.sumBy($scope.DraftPOList, 'MENGE');
                    if (poQty >= assignedQty) {
                        doShow = false;
                    }
                }

                return doShow;
            };

            /* COMMON ITEMS SPLIT LOGIC */

            $scope.checkIsGeneratePOEnable = function () {
                let doShow = true;
                if ($scope.itemWorkflow.length > 0) {
                    if ($scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        var notApproved = $scope.itemWorkflow[0].WorkflowTracks.filter(function (item) {
                            return item.status !== "APPROVED";
                        });

                        if (notApproved && notApproved.length > 0) {
                            doShow = false;
                        }
                    }
                }

                if (!$scope.itemWorkflow || $scope.itemWorkflow.length <= 0) {
                    doShow = false;
                }

                return doShow;
            };

            $scope.workflowChange = function () {
                let qcsTotalQuantity = 0;
                let reqTotalQuantity = 0;

                $scope.qcsItems.forEach(function (item1, index1) {
                    reqTotalQuantity = reqTotalQuantity + item1.productQuantity;
                });

                $scope.vendorAssignmentList.forEach(function (item1, index1) {
                    qcsTotalQuantity = qcsTotalQuantity + item1.assignedQty;
                });

                if (reqTotalQuantity !== qcsTotalQuantity) {
                    swal({
                        title: "Are you sure?",
                        text: "Please validate requirement quantity & Vendor assigned Quantity.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {

                    });
                }
            };


            $scope.GetNewQuotations = function (newQuotations) {
                var params = {
                    "qcsdetails": $scope.QCSDetails,
                    "isNewQuotation": newQuotations,
                    "sessionid": userService.getUserToken()
                };

                reportingService.GetNewQuotations(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("New Quotations Requested successfully.", "success");
                        }
                    });
            };

            $scope.enableForFirstApprover = function (step, type) {

                var enable = false;
                if (type === 'WORKFLOW') {
                    if (step.order === 1 && (step.status === 'PENDING') && $scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID)) { // step.status === 'REJECTED' || 
                        enable = true;
                    }
                }
                return enable;
            };

            //$scope.navigateToPOForm = function () {
            //    //import
            //    angular.element('#templateSelection').modal('hide');
            //    let selectedTemplate = '';
            //    if ($scope.stateDetails.poTemplate === 'po-domestic-zsdm') {
            //        selectedTemplate = 'ZSDM';
            //    } else if ($scope.stateDetails.poTemplate === 'po-import-zsim') {
            //        selectedTemplate = 'ZSIM';
            //    } else if ($scope.stateDetails.poTemplate === 'po-bonded-wh') {
            //        selectedTemplate = 'ZSBW';
            //    } else if ($scope.stateDetails.poTemplate === 'po-service-zssr') {
            //        selectedTemplate = 'ZSSR';
            //    }

            //    $state.go($scope.stateDetails.poTemplate, { 'reqId': $scope.reqId, 'qcsId': $scope.qcsID, 'requirementDetails': $scope.requirementDetails, 'detailsObj': $scope.vendorAssignmentList, 'templateName': selectedTemplate, 'quoteLink': $location.absUrl() });
            //    //let url = $state.href($scope.stateDetails.poTemplate, { 'reqId': $scope.reqId, 'qcsId': $scope.qcsID, 'requirementDetails': $scope.requirementDetails,  'detailsObj': null });                
            //    //$window.open(url, '_blank');
            //};

            /* COMMON ITEMS SPLIT LOGIC */

            $scope.navigateToPOForm = function (value) {
                //import
                $scope.stateDetails.poTemplate = value;
                angular.element('#splitCommonItems').modal('hide');
                let selectedTemplate = '';
                if ($scope.stateDetails.poTemplate === 'po-domestic-zsdm') {
                    selectedTemplate = 'ZSDM';
                } else if ($scope.stateDetails.poTemplate === 'po-import-zsim') {
                    selectedTemplate = 'ZSIM';
                } else if ($scope.stateDetails.poTemplate === 'po-bonded-wh') {
                    selectedTemplate = 'ZSBW';
                } else if ($scope.stateDetails.poTemplate === 'po-service-zssr') {
                    selectedTemplate = 'ZSSR';
                } else if ($scope.stateDetails.poTemplate === 'po-template') {
                    selectedTemplate = 'PO';
                }

                $state.go($scope.stateDetails.poTemplate, { 'reqId': $scope.reqId, 'qcsId': $scope.qcsID, 'requirementDetails': $scope.requirementDetails, 'detailsObj': $scope.vendorAssignmentList, 'templateName': selectedTemplate, 'quoteLink': $location.absUrl(), "splitQtyItems": $scope.splitItemsArr, "qcsType": $scope.qcsTypeObj.qcsType });
                //let url = $state.href($scope.stateDetails.poTemplate, { 'reqId': $scope.reqId, 'qcsId': $scope.qcsID, 'requirementDetails': $scope.requirementDetails,  'detailsObj': null });                
                //$window.open(url, '_blank');
            };

            /* COMMON ITEMS SPLIT LOGIC */

            $scope.checkGMPValidity = function (item, vendor) {
                let isDisabled = true;
                if (item.ITEM_PR_NUMBER && item.IS_GMP_PR_ITEM > 0 && item.ITEM_PLANTS) {
                    if ($scope.vendorSAPDetails && $scope.vendorSAPDetails.length > 0) {
                        item.ITEM_PLANTS.split(",").forEach(function (itemPlant, index1) {
                            var filterSAPVendor = _.filter($scope.vendorSAPDetails, function (vendorTemp) { return vendorTemp.vendorId === vendor.vendorID && vendorTemp.plant === itemPlant; });
                            if (isDisabled && filterSAPVendor && filterSAPVendor.length > 0) {
                                isDisabled = false;
                            }
                        });
                    }
                } else {
                    isDisabled = false;
                }

                return isDisabled;
            };

            $scope.GetCurrencyFactors = function () {
                auctionsService.GetCurrencyFactors($scope.userId, $scope.sessionid, $scope.companyId)
                    .then(function (response) {
                        $scope.currencyFactors = [];
                        response.forEach(function (item) {
                            if (item.type === 'currencyfactor') {
                                $scope.currencyFactors.push(item);
                            }
                        });
                    });
            };

            $scope.GetCurrencyFactors();

            $scope.getFile = function () {
                $scope.progress = 0;
                let totalQCSAttachmentsSize = 0;
                //$scope.file = $("#attachement")[0].files[0];
                let multipleAttachments = $("#attachement")[0].files;
                multipleAttachments = Object.values(multipleAttachments);
                if (multipleAttachments && multipleAttachments.length > 0) {
                    multipleAttachments.forEach(function (item, index) {
                        totalQCSAttachmentsSize = totalQCSAttachmentsSize + item.size;
                    });
                }

                if (totalQCSAttachmentsSize > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }

                if (!$scope.requirementDetails.qcsMultipleAttachments) {
                    $scope.requirementDetails.qcsMultipleAttachments = [];
                }

                multipleAttachments.forEach(function (item, index) {
                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;
                            var ifExists = _.findIndex($scope.requirementDetails.qcsMultipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase(); });
                            if (ifExists < 0) {
                                $scope.requirementDetails.qcsMultipleAttachments.push(fileUpload);
                            }
                        });
                });
            };

            $scope.removeAttach = function (attachmentObj) {
                if (attachmentObj && attachmentObj.fileID) {
                    $scope.requirementDetails.qcsMultipleAttachments = $scope.requirementDetails.qcsMultipleAttachments.filter(function (fileObj) {
                        return fileObj.fileID !== attachmentObj.fileID;
                    });
                } else if (attachmentObj && attachmentObj.fileName) {
                    $scope.requirementDetails.qcsMultipleAttachments = $scope.requirementDetails.qcsMultipleAttachments.filter(function (fileObj) {
                        return fileObj.fileName !== attachmentObj.fileName;
                    });
                }
            };

            $scope.getRequirementCurrencyFactor = function (currency) {
                let convertToLocalCurrencyFactor = 1;
                if ($scope.currencyFactors && $scope.currencyFactors.length > 0) {
                    let reqCurrencyObj = $scope.currencyFactors.filter(function (curr) {
                        return curr.currencyCode === currency;
                    });

                    if (reqCurrencyObj && reqCurrencyObj.length > 0 && reqCurrencyObj[0].currencyFactor !== 1) {
                        convertToLocalCurrencyFactor = reqCurrencyObj[0].currencyFactor;
                    }
                }

                return convertToLocalCurrencyFactor;
            };

            $scope.showApprovedDate = function (date) {
                return userService.toLocalDate(date);
            };

            //$scope.viewPODetails = function (poObject) {
            //    var params = {
            //        "ponumber": poObject.PO_NO,
            //        "quotno": poObject.QUOT_NO,
            //        "sessionid": userService.getUserToken()
            //    };

            //    PRMPOServices.getPOItems(params)
            //        .then(function (response) {
            //            poObject.POItems = response;
            //            if (poObject.POItems && poObject.POItems.length > 0 && poObject.POItems[0].PO_TEMPLATE && poObject.POItems[0].PO_RAW_JSON) {
            //                let selectedTemplate = poObject.POItems[0].PO_TEMPLATE;
            //                let poRoute = ''
            //                if (selectedTemplate === 'ZSDM') {
            //                    poRoute = 'po-domestic-zsdm';
            //                } else if (selectedTemplate === 'ZSIM') {
            //                    poRoute = 'po-import-zsim';
            //                } else if (selectedTemplate === 'ZSBW') {
            //                    poRoute = 'po-bonded-wh';
            //                } else if (selectedTemplate === 'ZSSR') {
            //                    poRoute = 'po-service-zssr';
            //                }

            //                if (poRoute) {
            //                    $state.go(poRoute, {
            //                        'reqId': poObject.POItems[0].REQ_ID, 'qcsId': poObject.POItems[0].QCS_ID,
            //                        'quotId': poObject.QUOT_NO ? poObject.QUOT_NO : '',
            //                        'requirementDetails': JSON.parse(poObject.POItems[0].QCS_REQUIREMENT_JSON),
            //                        'detailsObj': JSON.parse(poObject.POItems[0].QCS_VENDOR_ASSIGNMENT_JSON),
            //                        'templateName': selectedTemplate,
            //                        'quoteLink': poObject.POItems[0].ZZQANO,
            //                        'poRawJSON': JSON.parse(poObject.POItems[0].PO_RAW_JSON)
            //                    });
            //                }
            //            }
            //        });
            //};

            //$scope.getPOList = function () {
            //    var params = {
            //        'compid': $scope.companyId,
            //        'template': '',
            //        'vendorid': 0,
            //        'status': 'DRAFT',
            //        'creator': '',
            //        'plant': '',
            //        'purchasecode': '',
            //        'search': '/import-qcs/' + $scope.reqId + '/' + $scope.qcsID,
            //        'sessionid': $scope.sessionid,
            //        'page': 0,
            //        'pageSize': 10
            //    };

            //    PRMPOServices.getPOGenerateDetails(params)
            //        .then(function (response) {
            //            $scope.DraftPOList = [];
            //            if (response && response.length > 0) {
            //                $scope.DraftPOList = response;
            //            }
            //        });
            //};

            //if ($scope.reqId && $scope.qcsID) {
            //    $scope.getPOList();
            //}

            /* COMMON ITEMS SPLIT LOGIC */

            $scope.viewPODetails = function (poObject) {
                var params = {
                    "ponumber": poObject.PO_NO,
                    "quotno": poObject.QUOT_NO,
                    "sessionid": userService.getUserToken()
                };

                PRMPOServices.getPOItems(params)
                    .then(function (response) {
                        poObject.POItems = response;
                        if (poObject.POItems && poObject.POItems.length > 0 && poObject.POItems[0].PO_TEMPLATE && poObject.POItems[0].PO_RAW_JSON) {
                            let selectedTemplate = poObject.POItems[0].PO_TEMPLATE;
                            let poRoute = ''
                            if (selectedTemplate === 'ZSDM') {
                                poRoute = 'po-domestic-zsdm';
                            } else if (selectedTemplate === 'ZSIM') {
                                poRoute = 'po-import-zsim';
                            } else if (selectedTemplate === 'ZSBW') {
                                poRoute = 'po-bonded-wh';
                            } else if (selectedTemplate === 'ZSSR') {
                                poRoute = 'po-service-zssr';
                            } else if (selectedTemplate === 'PO') {
                                poRoute = 'po-template';
                            }

                            if (poRoute) {
                                $state.go(poRoute, {
                                    'reqId': poObject.POItems[0].REQ_ID, 'qcsId': poObject.POItems[0].QCS_ID,
                                    'quotId': poObject.QUOT_NO ? poObject.QUOT_NO : '',
                                    'requirementDetails': JSON.parse(poObject.POItems[0].QCS_REQUIREMENT_JSON),
                                    'detailsObj': JSON.parse(poObject.POItems[0].QCS_VENDOR_ASSIGNMENT_JSON),
                                    'templateName': selectedTemplate,
                                    'quoteLink': poObject.POItems[0].ZZQANO,
                                    'poRawJSON': JSON.parse(poObject.POItems[0].PO_RAW_JSON)
                                });
                            }
                        }
                    });
            };

            $scope.getPOList = function () {
                //var params = {
                //    'compid': $scope.companyId,
                //    'template': '',
                //    'vendorid': 0,
                //    'status': 'DRAFT',
                //    'creator': '',
                //    'plant': '',
                //    'purchasecode': '',
                //    'search': '/cost-comparisions-qcs/' + $scope.reqId + '/' + $scope.qcsID,
                //    'sessionid': $scope.sessionID,
                //    'page': 0,
                //    'pageSize': 10
                //};

                //PRMPOServices.getPOGenerateDetails(params)
                //    .then(function (response) {
                //        $scope.DraftPOList = [];
                //        if (response && response.length > 0) {
                //            $scope.DraftPOList = response;
                //        }
                //    });

                var params = {
                    'compid': $scope.companyId,
                    'reqid': $scope.reqId,
                    'qcsId': $scope.qcsID,
                    'sessionid': $scope.sessionid
                };


                PRMPOServices.getSplitPODetails(params)
                    .then(function (response) {
                        $scope.DraftPOList = [];
                        if (response && response.length > 0) {
                            $scope.DraftPOList = response;
                            $scope.DraftPOList.forEach(function (po, poidx) {
                                $scope.requirementPRItems.forEach(function (pr) {
                                    if (po.PR_NUMBER === pr.PR_NUMBER && po.PO_LINE_ITEM === pr.ITEM_NUM) {
                                        po.PR_QTY = pr.REQUIRED_QUANTITY;
                                    }
                                })
                            })
                        }
                    });

            };

            //if ($scope.reqId && $scope.qcsID) {
            //    $scope.getPOList();
            //}

            /* COMMON ITEMS SPLIT LOGIC */

            function calculateApproverRangeFromLandedCost(json) {
                var approverObject = JSON.parse(json);
                let reqCurrency = ($scope.requirementDetails && $scope.requirementDetails.currency) ? $scope.requirementDetails.currency : null;
                var convertToLocalCurrencyFactor = 1;
                if ($scope.currencyFactors && $scope.currencyFactors.length > 0 && reqCurrency) {
                    let reqCurrencyObj = $scope.currencyFactors.filter(function (curr) {
                        return curr.currencyCode === reqCurrency;
                    });

                    if (reqCurrencyObj && reqCurrencyObj.length > 0 && reqCurrencyObj[0].currencyFactor !== 1) {
                        convertToLocalCurrencyFactor = reqCurrencyObj[0].currencyFactor;
                    }
                }

                //var vendor = _.orderBy(approverObject.auctionVendors, ['assignedQtylandedCost'], ['desc']);
                //var vendor = _.filter(approverObject.auctionVendors, function (vendor) { return vendor.landingPriceRank === 1; });
                //if (vendor.length > 0) {
                //const currencyConversion = vendor[0].vendorCurrencyFactor ? +vendor[0].vendorCurrencyFactor : 1;
                //var value = vendor[0].assignedQtylandedCost;// * currencyConversion;
                //return value;
                //}
                let localCurrencyValue = (_.sumBy(approverObject.auctionVendors, 'assignedQtylandedCost')) * convertToLocalCurrencyFactor;
                return localCurrencyValue;
            }

            function getProductContracts() {
                let coreItems = $scope.qcsItems.filter(function (item) {
                    return item.isCoreProductCategory;
                });

                let productIds = '';
                if (coreItems && coreItems.length > 0) {
                    productIds = _.map(coreItems, 'productId');
                }

                if (productIds) {
                    catalogService.GetProductContracts(productIds.join(), userService.getUserToken())
                        .then(function (response) {
                            $scope.requirementItemContracts = response;
                        });
                }
            }

            function isItemVisible(item) {
                let isVisible = true;
                var requirementItem = _.filter($scope.qcsItems, function (qcsItem) { return qcsItem.itemID === item.itemID; });
                if (requirementItem && requirementItem.length > 0 && !requirementItem[0].isSelected) {
                    isVisible = false;
                }

                return isVisible;
            }

            function getVendorSAPDetails() {
                auctionsService.getVendorSAPDetails({ vendorid: 0, reqid: $scope.reqId, sessionid: userService.getUserToken() })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.vendorSAPDetails = response;
                        }
                    });
            }

            getVendorSAPDetails();

            /* COMMON ITEMS SPLIT LOGIC */


            $scope.poObj = {
                //PR_ITEM_ID: 0,
                //vendorName: '',
                //ITEM_ID: 0,
                //QTY: 0,
                "PR_ID": 0,
                "PR_NUMBER": '',
                "ITEM_NAME": '',
                "VENDOR_ID": 0
            };

            $scope.GetPRItemsByRequirementId = function () {

                PRMPRServices.GetPRItemsByReqId({ reqid: $scope.reqId })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementPRItems = response;
                            if ($scope.reqId && $scope.qcsID) {
                                $scope.getPOList();
                            }
                        }
                    });
            };
            $scope.GetPRItemsByRequirementId();

            $scope.splitItemsArr = [];
            $scope.minHeight = 0;

            $scope.splitCommonItems = function (tempobj) {

                if (tempobj.QTY == '' || tempobj.QTY == undefined || tempobj.QTY == null || tempobj.QTY == 0) {
                    $scope.qtyErr = 'Please enter Quantity.';
                    return;
                }

                var productIds = _($scope.requirementPRItems)
                    .map('PRODUCT_ID')
                    .value();

                let arr = productIds;
                const filtered = arr.filter((el, index) => arr.indexOf(el) !== index);
                const duplicatesProductIds = [...new Set(filtered)];
                var TotalCommonItemQty = 0;
                $scope.requirementPRItems.forEach(function (prItem, prItemIndex) {
                    if (productIds && productIds.length > 0) {
                        duplicatesProductIds.forEach(function (item, index) {
                            if (item === prItem.PRODUCT_ID) {
                                TotalCommonItemQty += prItem.REQUIRED_QUANTITY;
                            }
                        });
                    }
                });

                $scope.qtyErr = '';
                var AssignedTotalCommonItemQty = 0;
                if ($scope.splitItemsArr && $scope.splitItemsArr.length > 0) {
                    let currentPrNumber = _.find($scope.requirementPRItems, { ITEM_ID: tempobj.PR_ITEM_ID }).PR_NUMBER;
                    let currentPRQTy = _.find($scope.requirementPRItems, { ITEM_ID: tempobj.PR_ITEM_ID }).REQUIRED_QUANTITY;
                    let temp_pr_Qty = 0;
                    $scope.splitItemsArr.forEach(function (split, spIdx) {
                        if (split.PR_NUMBER == currentPrNumber && split.PR_ITEM_ID == tempobj.PR_ITEM_ID) {
                            temp_pr_Qty += +split.QTY;
                        }
                    });

                    var assqty = _.find($scope.vendorAssignmentList, function (vendor) {
                        if (vendor.vendorID === tempobj.VENDOR_ID && vendor.itemID === tempobj.ITEM_ID) {
                            return vendor.assignedQty;
                        }
                    });

                    var assvendorqty = _.sumBy($scope.splitItemsArr, function (st) {
                        if (st.VENDOR_ID === tempobj.VENDOR_ID && st.ITEM_ID === tempobj.ITEM_ID) {
                            st.TEMP_QTY = 0;
                            st.TEMP_QTY = +st.QTY;
                            return st.TEMP_QTY;
                        }
                    });
                    if ((assvendorqty + +tempobj.QTY) > +assqty.assignedQty) {
                        $scope.qtyErr = 'Assign quantity is utilised for other PR.';
                        return false;
                    }

                    if (currentPRQTy < (temp_pr_Qty + +tempobj.QTY)) {
                        tempobj.QTY = 0;
                        //  $scope.qtyErr = 'Can\'t Add more than the PR quantity.';
                        $scope.qtyErr = 'PR Quantity already added to vendor.';
                        return;
                    }
                    var obj = {
                        PR_ITEM_ID: tempobj.PR_ITEM_ID,
                        VERTICAL_TYPE: _.find($scope.requirementPRItems, { ITEM_ID: tempobj.PR_ITEM_ID }).VERTICAL_TYPE,
                        VENDOR_ID: tempobj.VENDOR_ID, ITEM_ID: tempobj.ITEM_ID,
                        PR_NUMBER: _.find($scope.requirementPRItems, { ITEM_ID: tempobj.PR_ITEM_ID }).PR_NUMBER,
                        PR_QTY: _.find($scope.requirementPRItems, { ITEM_ID: tempobj.PR_ITEM_ID }).REQUIRED_QUANTITY,
                        ITEM_NUM: _.find($scope.requirementPRItems, { ITEM_ID: tempobj.PR_ITEM_ID }).ITEM_NUM,
                        VENDOR_NAME: _.find($scope.vendorAssignmentList, { vendorID: tempobj.VENDOR_ID }).vendorName,
                        ITEM_NAME: _.find($scope.vendorAssignmentList, { itemID: tempobj.ITEM_ID }).item.itemName,
                        QTY: tempobj.QTY,
                        PRODUCT_ID: tempobj.PRODUCT_ID,
                        IS_COMMON_ITEM: tempobj.IS_COMMON_ITEM
                    };
                    let isItemExists = false;
                    let selectedQty = _.sumBy($scope.splitItemsArr, function (o) { return parseFloat(o.QTY); });
                    //isItemExists = _.some($scope.splitItemsArr, function (item) {
                    //    var checkStr = tempobj.PRODUCT_ID.toString().concat("-", tempobj.VENDOR_ID.toString(), "-", tempobj.ITEM_ID.toString());
                    //    var itemStr = item.PRODUCT_ID.toString().concat("-", item.VENDOR_ID.toString(), "-", item.ITEM_ID.toString());
                    //    return (item && checkStr === itemStr);
                    //});
                    isItemExists = _.some($scope.splitItemsArr, function (item) {
                        var tempObjPRNum = _.find($scope.requirementPRItems, { ITEM_ID: tempobj.PR_ITEM_ID }).PR_NUMBER;
                        var itemPRNum = _.find($scope.requirementPRItems, { ITEM_ID: item.PR_ITEM_ID }).PR_NUMBER;
                        var checkStr = tempobj.PRODUCT_ID.toString().concat("-", tempobj.VENDOR_ID.toString(), "-", tempobj.ITEM_ID.toString(), "-", tempObjPRNum.toString());
                        var itemStr = item.PRODUCT_ID.toString().concat("-", item.VENDOR_ID.toString(), "-", item.ITEM_ID.toString(), "-", itemPRNum.toString());
                        return (item && checkStr === itemStr);
                    });
                    if (isItemExists) {
                        // $scope.qtyErr = 'Can\'t Assign more than Total Item Quantity.';  && selectedQty != TotalCommonItemQty  && item.PR_ITEM_ID === tempobj.PR_ITEM_ID
                        $scope.splitItemsArr.forEach(function (item, index) {
                            if (item.VENDOR_ID === tempobj.VENDOR_ID && item.ITEM_ID === tempobj.ITEM_ID && item.PRODUCT_ID === tempobj.PRODUCT_ID && item.PR_ITEM_ID === tempobj.PR_ITEM_ID) {
                                item.TEMP_QTY = 0;
                                item.TEMP_QTY = +item.QTY;
                                var qty = _.find($scope.vendorAssignmentList, function (vendor) {
                                    if (vendor.vendorID === tempobj.VENDOR_ID && vendor.itemID === tempobj.ITEM_ID) {
                                        return vendor.assignedQty;
                                    }
                                });
                                if ((item.TEMP_QTY + +tempobj.QTY) > +qty.assignedQty) {
                                    $scope.qtyErr = 'Can\'t Add more than the assigned quantity.';
                                }
                                //else if ((item.TEMP_QTY + +tempobj.QTY) === +qty.assignedQty) {
                                //    $scope.qtyErr = 'Can\'t Assign total assigned quantity to one item.';
                                //}
                                else {
                                    item.QTY = item.TEMP_QTY + +tempobj.QTY;
                                }
                            }
                        });
                    } else {
                        $scope.splitItemsArr.push(obj);
                        var length = $scope.splitItemsArr.length * 43;
                        $scope.minHeight = 'height:' + length + 'px';
                    }
                } else {
                    var obj = {
                        PR_ITEM_ID: tempobj.PR_ITEM_ID,
                        VERTICAL_TYPE: _.find($scope.requirementPRItems, { ITEM_ID: tempobj.PR_ITEM_ID }).VERTICAL_TYPE,
                        VENDOR_ID: tempobj.VENDOR_ID, ITEM_ID: tempobj.ITEM_ID,
                        PR_NUMBER: _.find($scope.requirementPRItems, { ITEM_ID: tempobj.PR_ITEM_ID }).PR_NUMBER,
                        PR_QTY: _.find($scope.requirementPRItems, { ITEM_ID: tempobj.PR_ITEM_ID }).REQUIRED_QUANTITY,
                        ITEM_NUM: _.find($scope.requirementPRItems, { ITEM_ID: tempobj.PR_ITEM_ID }).ITEM_NUM,
                        VENDOR_NAME: _.find($scope.vendorAssignmentList, { vendorID: tempobj.VENDOR_ID }).vendorName,
                        ITEM_NAME: _.find($scope.vendorAssignmentList, { itemID: tempobj.ITEM_ID }).item.itemName,
                        QTY: tempobj.QTY,
                        PRODUCT_ID: tempobj.PRODUCT_ID,
                        IS_COMMON_ITEM: tempobj.IS_COMMON_ITEM
                    };
                    $scope.splitItemsArr.push(obj);
                    var length = $scope.splitItemsArr.length * 43;
                    $scope.minHeight = 'height:' + length + 'px';
                }
                tempobj.QTY = 0;
            };

            $scope.showPopUp = false;

            $scope.addNonCommonItems = function () {
                var productIds = _($scope.requirementPRItems)
                    .map('PRODUCT_ID')
                    .value();

                let arr = productIds;
                const filtered = arr.filter((el, index) => arr.indexOf(el) !== index);
                const duplicatesProductIds = [...new Set(filtered)];

                var obj = {};
                $scope.TotalCommonItemQty = 0;
                $scope.requirementPRItems.forEach(function (prItem, prItemIndex) {
                    prItem.IS_COMMON_ITEM = false;
                    if (productIds && productIds.length > 0) {
                        duplicatesProductIds.forEach(function (item, index) {
                            if (item === prItem.PRODUCT_ID && prItem.IsServiceProduct == 0) {
                                prItem.IS_COMMON_ITEM = true;
                                $scope.TotalCommonItemQty += prItem.REQUIRED_QUANTITY;
                            }
                        });
                    }

                    if (!prItem.IS_COMMON_ITEM) {
                        $scope.vendorAssignmentList.forEach(function (vendorAssignItem, vendorAssignItemIndex) {
                            if (!vendorAssignItem.IS_COMMON_ITEM) {
                                vendorAssignItem.IS_COMMON_ITEM = false;
                                obj.IS_COMMON_ITEM = false;
                                if (_.indexOf(vendorAssignItem.item.prNumber.split(","), prItem.PR_NUMBER) >= 0 && (vendorAssignItem.item.productId === prItem.PRODUCT_ID)) {
                                    obj.PR_ITEM_ID = prItem.ITEM_ID;
                                    obj.VERTICAL_TYPE = prItem.VERTICAL_TYPE;
                                    obj.VENDOR_ID = vendorAssignItem.vendorID;
                                    obj.ITEM_ID = vendorAssignItem.itemID;
                                    obj.QTY = vendorAssignItem.assignedQty;
                                    obj.PRODUCT_ID = vendorAssignItem.item.productId;
                                    obj.PR_NUMBER = vendorAssignItem.item.prNumber;
                                }
                            }
                            // $scope.splitItemsArr = [];
                            if (obj && obj.PR_ITEM_ID && obj.PR_ITEM_ID > 0) {
                                $scope.splitCommonItems(obj);
                            }
                        });
                        //// $scope.splitItemsArr = [];
                        // if (obj && obj.PR_ITEM_ID && obj.PR_ITEM_ID > 0) {
                        //     $scope.splitCommonItems(obj);
                        // }
                    } else {
                        $scope.vendorAssignmentList.forEach(function (vendorAssignItem, vendorAssignItemIndex) {
                            vendorAssignItem.IS_COMMON_ITEM = false;
                            obj.IS_COMMON_ITEM = false;
                            if (vendorAssignItem.item.productId === prItem.PRODUCT_ID) {
                                vendorAssignItem.IS_COMMON_ITEM = true;
                                $scope.poObj.IS_COMMON_ITEM = true;
                            }
                        });
                    }
                });

                $scope.showPopUp = true;
            };

            $scope.displayCommonValues = function (arr, type) {
                let displayArr = [];
                if (arr && arr.length > 0) {
                    if (type === 'PR') {
                        displayArr = _.filter($scope.requirementPRItems, function (prItems) { return prItems.IS_COMMON_ITEM; });
                    } else if (type === 'VENDOR' || type === 'ITEM') {
                        displayArr = _.filter($scope.vendorAssignmentList, function (vendorItem) { return vendorItem.IS_COMMON_ITEM; });
                        displayArr = _.uniqBy(displayArr, function (o) { return o.vendorID; });
                        if (type === 'ITEM') {
                            displayArr = _.uniqBy(displayArr, function (o) { return o.item.itemName; });
                        }
                    }
                }
                return displayArr;

            };

            //$scope.AssignedQtyForItem = 0;

            $scope.validateQuantity = function (obj, type) {
                $scope.qtyErr = '';
                var qty = _.find($scope.vendorAssignmentList, function (vendor) {
                    if (vendor.vendorID === obj.VENDOR_ID && vendor.itemID === obj.ITEM_ID) {
                        return vendor.assignedQty;
                    }
                });
                var productId = _.find($scope.requirementPRItems, { ITEM_ID: obj.PR_ITEM_ID }).PRODUCT_ID;
                $scope.prqty = _.find($scope.requirementPRItems, { ITEM_ID: obj.PR_ITEM_ID }).REQUIRED_QUANTITY;
                $scope.prRemainingQty = 0;
                obj.PRODUCT_ID = +productId;
                $scope.AssignedQtyForItem = qty ? +qty.assignedQty : 0;

                $scope.requirementPRItems.forEach(function (item, index) {
                    if (item.ITEM_ID === obj.PR_ITEM_ID && item.PRODUCT_ID === obj.PRODUCT_ID) {
                        item.TEMP_QTY = 0;
                        item.TEMP_QTY = +obj.QTY;
                        item.REMAINING_QUANTITY = item.REQUIRED_QUANTITY - item.TEMP_QTY;

                        if (obj.QTY > item.REQUIRED_QUANTITY) {
                            obj.QTY = 0;
                            $scope.qtyErr = 'Quantity Can\'t be greater than the PR Qty.';
                            return;
                        }
                        else if (obj.QTY <= item.REQUIRED_QUANTITY) {
                            let isItemExists = false;
                            isItemExists = _.some($scope.splitItemsArr, function (item) {
                                var itemPRID = _.find($scope.requirementPRItems, { ITEM_ID: item.PR_ITEM_ID }).PR_ITEM_ID;
                                return (item && itemPRID === obj.PR_ITEM_ID);
                            });
                            if (isItemExists) {
                                $scope.splitItemsArr.forEach(function (item1, index) {
                                    if (item1.VENDOR_ID === obj.VENDOR_ID && item1.ITEM_ID === obj.ITEM_ID && item1.PRODUCT_ID === obj.PRODUCT_ID) { //  && item1.PR_ITEM_ID === obj.PR_ITEM_ID
                                        item1.TEMP_QTY = 0;
                                        item1.TEMP_QTY = +item1.QTY;
                                        if ((item1.TEMP_QTY + +obj.QTY) > item1.PR_QTY) {
                                            obj.QTY = 0;
                                            $scope.qtyErr = 'Quantity Can\'t be greater than the PR Qty.';
                                            //  $scope.qtyErr = 'PR Quantity already added to vendor.';
                                            return;
                                        } else
                                            if ((item1.TEMP_QTY + +obj.QTY) > $scope.AssignedQtyForItem) {
                                                obj.QTY = 0;
                                                $scope.qtyErr = 'Quantity Can\'t be greater than the Assigned Qty.';
                                                return;
                                            }
                                    }
                                })
                            } else {
                                if (obj.QTY > $scope.AssignedQtyForItem) {
                                    obj.QTY = 0;
                                    $scope.qtyErr = 'Quantity Can\'t be greater than the Assigned Qty.';
                                }
                            }

                            //if (obj.QTY > $scope.AssignedQtyForItem) {
                            //    obj.QTY = 0;
                            //    $scope.qtyErr = 'Quantity Can\'t be greater than the Assigned Qty.';
                            //}

                        }
                    }

                })
            };

            $scope.changePOObj = function () {
                $scope.poObj = {
                    PR_ITEM_ID: 0,
                    vendorName: '',
                    ITEM_ID: 0,
                    QTY: 0
                };
            }

            $scope.DeleteOnlyCommonItems = function (index) {
                $scope.splitItemsArr.splice(index, 1);
            };

            $scope.clearSplitCommonArray = function () {
                $scope.splitItemsArr = [];

                $scope.poObj = {
                    PR_ITEM_ID: 0,
                    vendorName: '',
                    ITEM_ID: 0,
                    QTY: 0
                };
            }

            /* COMMON ITEMS SPLIT LOGIC */

            function calculateApproversCount(selectedWorkflowID) {
                let count = 0;
                count = _.find($scope.workflowListTemp, { workflowID: selectedWorkflowID }).WorkflowStages.length;
                return count;
            }


            function retrieveVendorItems(items) {
                var itemsFinal = [];
                if (items && items.length > 0) {
                    var tempRFQDet = $scope.requirementDetails.auctionVendors;
                    if (tempRFQDet && tempRFQDet.length > 0) {
                        tempRFQDet.forEach(function (vendor, vendorIndex) {
                            if (vendor.listRequirementItems && vendor.listRequirementItems.length > 0) {
                                vendor.listRequirementItems.forEach(function (vendItem, vendItemIndex) {
                                    vendItem.unitPrice = vendItem.unitPrice * vendor.vendorCurrencyFactor;
                                    vendItem.revUnitPrice = vendItem.revUnitPrice * vendor.vendorCurrencyFactor;
                                });
                            }
                        });
                    }
                    items.forEach(function (item, index) {
                        var groupItemsBasedOnItemID = _.filter(_.flatten(_.map(tempRFQDet, 'listRequirementItems')), { itemID: item.itemID, isItemQuotationRejected: 0 }); // each item
                        var getInitialUnitPriceItemGreaterThanZero = _.filter(groupItemsBasedOnItemID, function (item) { return item.unitPrice > 0; });
                        var getRevUnitPriceItemGreaterThanZero = _.filter(groupItemsBasedOnItemID, function (item) { return item.revUnitPrice > 0; });
                        var minLeastPrice = _.minBy(getInitialUnitPriceItemGreaterThanZero, 'unitPrice').unitPrice;
                        var minLeastRevUnitPrice = _.minBy(getRevUnitPriceItemGreaterThanZero, 'revUnitPrice').revUnitPrice;
                        var bbsprice = ((minLeastPrice * item.assignedQty) - (minLeastRevUnitPrice * item.assignedQty));
                        var ebprice = (item.lppValue - (item.revUnitPrice * item.currencyfactor)) * item.assignedQty;

                        var qcsAssignmentObj = {
                            QCS_ID: 0,
                            REQ_ID: $stateParams.reqID,
                            VENDOR_ID: item.vendorID,
                            ITEM_ID: item.itemID,
                            ASSIGN_QTY: item.assignedQty,
                            ASSIGN_PRICE: item.assignedPrice,
                            TOTAL_PRICE: item.totalPrice,
                            BEST_BID_SAVINGS: +bbsprice > 0 ? +bbsprice : 0,
                            EBIT_SAVINGS: +ebprice > 0 ? +ebprice : 0,
                            Rank: item.vendorRank,
                            vendorName: item.vendorName,
                            itemName: item.itemName
                        };
                        itemsFinal.push(qcsAssignmentObj);
                        //$scope.vendorAssignmentList1.push(qcsAssignmentObj);
                    });
                }
                return itemsFinal;
            }

            function calculateBestBidSavings(items) {
                let bestBidItemWiseSavings = [];
                if (items && items.length > 0) {
                    bestBidItemWiseSavings = _(items).groupBy('ITEM_ID') // group the items
                        .map((objs, key) => ({
                            'ITEM_ID': +key, // item id value
                            'ITEM_WISE_BEST_BID_SAVINGS': _.sumBy(objs, 'BEST_BID_SAVINGS')
                        })).value();
                }
                return _.sumBy(bestBidItemWiseSavings, 'ITEM_WISE_BEST_BID_SAVINGS');
            }


            function calculateEbitSavings(items) {
                let ebitItemWiseSavings = [];
                if (items && items.length > 0) {
                    ebitItemWiseSavings = _(items).groupBy('ITEM_ID') // group the items
                        .map((objs, key) => ({
                            'ITEM_ID': +key, // item id value
                            'ITEM_WISE_EBIT_SAVINGS': _.sumBy(objs, 'EBIT_SAVINGS')
                        })).value();
                }
                return _.sumBy(ebitItemWiseSavings, 'ITEM_WISE_EBIT_SAVINGS');
            }

            $scope.getVendorRank = function (vendorId) {
                return $scope.requirementDetails.auctionVendors && $scope.requirementDetails.auctionVendors.length > 0 ? (_.find($scope.requirementDetails.auctionVendors, { vendorID: vendorId }).rank) : 0;
            };

            $scope.precisionRound = function (number, precision) {
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            };


            $scope.showPOGeneratePopUp = function () {
                $scope.PR_NUMBERS_ARR = [];
                $scope.PR_ITEMS_ARR = [];
                $scope.ASSIGNED_VENDORS_ARR = [];
                $scope.splitItemsArr = [];

                $scope.requirementPRItems.forEach(function (prItem, prItemIndex) {
                    $scope.vendorAssignmentList.forEach(function (vendorAssignItem, vendorAssignItemIndex) {
                        var obj = {};
                        if (_.indexOf(vendorAssignItem.item.prNumber.split(","), prItem.PR_NUMBER) >= 0 && (vendorAssignItem.item.productId === prItem.PRODUCT_ID)) {
                            if (vendorAssignItem.item.prNumber.indexOf(',') > -1) {
                                var prs = vendorAssignItem.item.prNumber.split(',');
                                prs.forEach(function (pr, prIndex) {
                                    var prObj =
                                    {
                                        "PR_NUMBER": pr,
                                        "ITEM_NAME": prItem.ITEM_NAME,
                                        "VENDOR_NAME": vendorAssignItem.vendorName
                                    };
                                    var itemFound = _.findIndex($scope.PR_NUMBERS_ARR, function (item) { return item.PR_NUMBER === prObj.PR_NUMBER });
                                    if (itemFound <= -1) {
                                        $scope.PR_NUMBERS_ARR.push(prObj);
                                    }
                                    var itemFound = _.findIndex($scope.PR_ITEMS_ARR, function (item) { return item.ITEM_NAME === prObj.ITEM_NAME });
                                    if (itemFound <= -1) {
                                        $scope.PR_ITEMS_ARR.push(prObj);
                                    }
                                    var itemFound = _.findIndex($scope.ASSIGNED_VENDORS_ARR, function (item) { return item.VENDOR_NAME === prObj.VENDOR_NAME });
                                    if (itemFound <= -1) {
                                        $scope.ASSIGNED_VENDORS_ARR.push(prObj);
                                    }
                                });
                            } else {
                                obj.PR_NUMBER = prItem.PR_NUMBER;
                                obj.PR_QTY = prItem.REQUIRED_QUANTITY,
                                    obj.ITEM_NUM = prItem.ITEM_NUM,
                                    obj.PR_ITEM_ID = prItem.ITEM_ID;
                                obj.VERTICAL_TYPE = prItem.VERTICAL_TYPE;
                                obj.VENDOR_ID = vendorAssignItem.vendorID;
                                obj.VENDOR_NAME = vendorAssignItem.vendorName,
                                    obj.ITEM_NAME = vendorAssignItem.item.itemName;
                                obj.QTY = vendorAssignItem.assignedQty;
                                obj.PRODUCT_ID = vendorAssignItem.item.productId;
                                obj.ITEM_ID = vendorAssignItem.itemID;
                                $scope.splitItemsArr.push(obj);
                            }
                        }
                    });
                });
                //$scope.PR_ITEMS_ARR = _.uniqBy($scope.PR_ITEMS_ARR, 'ITEM_NAME');
                angular.element('#splitCommonItems').modal('show');
            };


            $scope.assignNewObj = function (poObj) {
                poObj.QTY = 0;
                if (poObj.PR_NUMBER && poObj.ITEM_NAME && poObj.VENDOR_NAME) {
                    var prID = _.find($scope.requirementPRItems, { PR_NUMBER: poObj.PR_NUMBER }).PR_ID;
                    var prItemObj = _.find($scope.requirementPRItems, { PR_NUMBER: poObj.PR_NUMBER, ITEM_NAME: poObj.ITEM_NAME });
                    $scope.newSelectedObj = {};
                    if (prItemObj) {
                        $scope.newSelectedObj.PR_NUMBER = poObj.PR_NUMBER;
                        $scope.newSelectedObj.ITEM_NAME = poObj.ITEM_NAME;
                        $scope.newSelectedObj.VENDOR_NAME = poObj.VENDOR_NAME;
                        $scope.newSelectedObj.PR_QTY = prItemObj.REQUIRED_QUANTITY;
                        $scope.newSelectedObj.ITEM_NUM = prItemObj.ITEM_NUM;
                        $scope.newSelectedObj.PR_ITEM_ID = prItemObj.ITEM_ID;
                        $scope.newSelectedObj.VERTICAL_TYPE = prItemObj.VERTICAL_TYPE;
                        $scope.newSelectedObj.PRODUCT_ID = prItemObj.PRODUCT_ID;
                    }

                    $scope.vendorAssignmentList.forEach(function (vendorAssignItem, vendorAssignItemIndex) {
                        if (_.indexOf(vendorAssignItem.item.prNumber.split(","), poObj.PR_NUMBER) >= 0 &&
                            vendorAssignItem.item.productId === $scope.newSelectedObj.PRODUCT_ID &&
                            vendorAssignItem.vendorName === poObj.VENDOR_NAME) {
                            $scope.newSelectedObj.VENDOR_ID = vendorAssignItem.vendorID;
                            $scope.newSelectedObj.QTY = vendorAssignItem.assignedQty;
                            $scope.newSelectedObj.ASSIGNED_QTY = vendorAssignItem.assignedQty;
                            $scope.newSelectedObj.ITEM_ID = vendorAssignItem.itemID;
                        }
                    });
                }
            };


            $scope.addToSplitItemsArr = function (poObj) {
                $scope.showQtyErrorMessage = '';
                let TOTAL_PR_QUANTITY = 0;
                let TOTAL_ASSIGNED_PRODUCT_QUANTITY = 0;
                let TOTAL_ASSIGNED_VENDOR_QUANTITY = 0;
                let AVAILABLE_QTY = 0;
                if (poObj.PR_NUMBER && poObj.ITEM_NAME && poObj.VENDOR_NAME && poObj.QTY) {
                    var itemFound = _.findIndex($scope.splitItemsArr, function (item) {
                        return item.PR_NUMBER === poObj.PR_NUMBER &&
                            item.PRODUCT_ID === $scope.newSelectedObj.PRODUCT_ID &&
                            item.VENDOR_ID === $scope.newSelectedObj.VENDOR_ID
                    });

                    TOTAL_PR_QUANTITY = _.find($scope.requirementPRItems, { PR_NUMBER: poObj.PR_NUMBER, ITEM_NAME: poObj.ITEM_NAME }).REQUIRED_QUANTITY;

                    $scope.splitItemsArr.forEach(function (splitItem, splitItemIndex) {
                        if (splitItem.PR_NUMBER === poObj.PR_NUMBER && splitItem.ITEM_NAME === poObj.ITEM_NAME) {
                            TOTAL_ASSIGNED_PRODUCT_QUANTITY += +splitItem.QTY;
                        }

                        if (splitItem.PR_NUMBER === poObj.PR_NUMBER && splitItem.VENDOR_NAME === poObj.VENDOR_NAME && splitItem.ITEM_NAME === poObj.ITEM_NAME) {
                            TOTAL_ASSIGNED_VENDOR_QUANTITY += +splitItem.QTY;
                        }
                    });

                    //console.log("pr qty>>>" + TOTAL_PR_QUANTITY);
                    //console.log("assigned qty>>>>" + TOTAL_ASSIGNED_PRODUCT_QUANTITY);
                    //console.log("vendor assigned qty>>>>" + TOTAL_ASSIGNED_VENDOR_QUANTITY);

                    AVAILABLE_QTY = (TOTAL_PR_QUANTITY - TOTAL_ASSIGNED_PRODUCT_QUANTITY);
                    //console.log("AVAILABLE_QTY" + AVAILABLE_QTY);
                    if (AVAILABLE_QTY <= 0 || (+poObj.QTY > AVAILABLE_QTY)) {
                        $scope.showQtyErrorMessage = 'Quantity utilised.';
                        poObj.QTY = 0;
                        return;
                    }

                    if ((TOTAL_ASSIGNED_VENDOR_QUANTITY + +poObj.QTY) > $scope.newSelectedObj.ASSIGNED_QTY) {
                        $scope.showQtyErrorMessage = 'Quantity utilised.';
                        poObj.QTY = 0;
                        return;
                    }

                    if (itemFound >= 0) {
                        var availableQty = +$scope.splitItemsArr[itemFound].QTY
                        availableQty += +poObj.QTY;
                        $scope.splitItemsArr[itemFound].QTY = availableQty;
                    } else {
                        $scope.newSelectedObj.QTY = poObj.QTY;
                        $scope.splitItemsArr.push($scope.newSelectedObj);
                    }
                } else {
                    if (!poObj.PR_NUMBER) {
                        $scope.showQtyErrorMessage = 'Please Select PR';
                        return;
                    }
                    if (!poObj.ITEM_NAME) {
                        $scope.showQtyErrorMessage = 'Please Select Item';
                        return;
                    }
                    if (!poObj.VENDOR_NAME) {
                        $scope.showQtyErrorMessage = 'Please Select Vendor';
                        return;
                    }
                    if (!poObj.QTY || !poObj.QTY <= 0) {
                        $scope.showQtyErrorMessage = 'Please Enter Quantity';
                        return;
                    }

                }
            };

            $scope.validateQty = function (poObj) {
                $scope.showQtyErrorMessage = '';
                if (poObj.QTY > $scope.newSelectedObj.ASSIGNED_QTY) {
                    $scope.showQtyErrorMessage = 'Cannot assign more than the Assigned Quantity.';
                    poObj.QTY = 0;
                }
                if (poObj.QTY > $scope.newSelectedObj.PR_QTY) {
                    $scope.showQtyErrorMessage = 'Cannot enter more than the PR Quantity.';
                    poObj.QTY = 0;
                }
            };

            $scope.calculateSavings = function (vendor, istaxChecked) {
                vendor.SAVINGS = 0;
                if (vendor && !istaxChecked) {
                    vendor.INITIAL_PRICE_NO_TAX = 0;
                    vendor.REV_PRICE_NO_TAX = 0;
                    vendor.listRequirementItems.forEach(function (vendorReqItem, vendorReqItemIndex) {
                        vendor.INITIAL_PRICE_NO_TAX += vendorReqItem.unitPrice * vendorReqItem.productQuantity;
                        vendor.REV_PRICE_NO_TAX += vendorReqItem.revUnitPrice * vendorReqItem.productQuantity;
                    });
                    vendor.INITIAL_PRICE_NO_TAX = (vendor.INITIAL_PRICE_NO_TAX * vendor.vendorCurrencyFactor);
                    vendor.REV_PRICE_NO_TAX = (vendor.REV_PRICE_NO_TAX * vendor.vendorCurrencyFactor);
                    vendor.SAVINGS = (vendor.INITIAL_PRICE_NO_TAX - vendor.REV_PRICE_NO_TAX);
                } else {
                    vendor.SAVINGS = ((vendor.initialPrice * vendor.vendorCurrencyFactor + vendor.DIFFERENTIAL_FACTOR) - (vendor.totalRunningPrice * vendor.vendorCurrencyFactor + vendor.DIFFERENTIAL_FACTOR));
                }
                return vendor.SAVINGS;
            };

        }]);