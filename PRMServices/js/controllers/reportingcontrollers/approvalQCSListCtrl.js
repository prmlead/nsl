﻿prmApp
    .controller('approvalQCSListCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService",
    "PRMPRServices", "reportingService","workflowService",
    function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService,
        PRMPRServices, reportingService, workflowService) {

        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();
        
        $scope.qcsApprovalList = [];
        $scope.qcsApprovalList1 = [];
        $scope.qcsApprovalListTemporary = [];
        $scope.deptIDs = [];
        $scope.desigIDs = [];
        $scope.deptIDStr = '';
        $scope.desigIDStr = '';

        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 8;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
            /*PAGINATION CODE*/



        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                        $scope.desigIDs = _.union($scope.desigIDs, [item1.desigID]);
                    }
                });
            });
            $scope.deptIDStr = $scope.deptIDs.join(',');
            $scope.desigIDStr = $scope.desigIDs.join(',');
        }


        $scope.getQCSApprovalList = function () {

            var params = {
                "userid": $scope.userID,
                "deptid": $scope.deptIDStr,
                "desigid": $scope.desigIDStr,
                "type": "QCS"
            };
            workflowService.GetApprovalList(params)
                .then(function (response) {
                    $scope.qcsApprovalList = response;
                    //$scope.qcsApprovalList1 = response;
                    //$scope.qcsApprovalListTemporary = response;

                    $scope.qcsApprovalList = $scope.qcsApprovalList.filter(function (item, index) {
                        return item.USER_ID == $scope.userID;
                    });

                    $scope.qcsApprovalList.forEach(function (qcs, qcsIndex) {
                        qcs.APPROVAL_DATE = userService.toLocalDate(qcs.APPROVAL_DATE);
                    });

                    $scope.qcsApprovalList1 = $scope.qcsApprovalList;
                    $scope.qcsApprovalListTemporary = $scope.qcsApprovalList;

                    //$scope.qcsApprovalList1 = $scope.qcsApprovalList1.filter(function (item, index) {
                    //    return item.USER_ID == $scope.userID;
                    //});

                    //$scope.qcsApprovalListTemporary = $scope.qcsApprovalListTemporary.filter(function (item, index) {
                    //    return item.USER_ID == $scope.userID;
                    //});

                    //$scope.qcsApprovalList.forEach(function (qcs, qcsIndex) {
                    //    qcs.APPROVAL_DATE = userService.toLocalDate(qcs.APPROVAL_DATE);
                    //});

                    //$scope.qcsApprovalList1.forEach(function (qcs, qcsIndex) {
                    //    qcs.APPROVAL_DATE = userService.toLocalDate(qcs.APPROVAL_DATE);
                    //});

                    //$scope.qcsApprovalListTemporary.forEach(function (qcs, qcsIndex) {
                    //    qcs.APPROVAL_DATE = userService.toLocalDate(qcs.APPROVAL_DATE);
                    //});
                    
                    $scope.totalItems = $scope.qcsApprovalList.length;

                });
        };


        $scope.getQCSApprovalList();



        $scope.setFilters = function (search) {

            if (search) {
                $scope.qcsApprovalList = $scope.qcsApprovalList1.filter(function (qcs) {
                    return (String(qcs.MODULE_ID).indexOf(search) >= 0
                            || qcs.MODULE_TYPE.toLowerCase().indexOf(search.toLowerCase()) >= 0
                            || String(qcs.REFER_MODULE_LINK_ID).indexOf(search) >= 0
                            || qcs.REFER_MODULE_LINK_NAME.toLowerCase().indexOf(search.toLowerCase()) >= 0);
                });
                $scope.totalItems = $scope.qcsApprovalList.length;
            } else {
                $scope.qcsApprovalList = [];
                $scope.qcsApprovalList = $scope.qcsApprovalListTemporary;
                $scope.totalItems = $scope.qcsApprovalListTemporary.length;
            }


        };

        $scope.routeToModule = function (module,moduleID) {

            if (module && moduleID) {
                var approvalObj = { isFromApproval: true };

                return module == 'DOMESTIC' ? "cost-comparisions-qcs({ reqID: qcs.REFER_MODULE_LINK_ID, qcsID: qcs.MODULE_ID},'costcomparisonsObj':approvalObj)" : "import-qcs({ reqID: qcs.REFER_MODULE_LINK_ID, qcsID: qcs.MODULE_ID },'importObj':approvalObj)";
            }
        };

        $scope.navigateToQCS = function (qcs) {
            let url = "";
            if (qcs.MODULE_TYPE === 'DOMESTIC') {
                url = $state.href("cost-comparisions-qcs", { "reqID": qcs.REFER_MODULE_LINK_ID, "qcsID": qcs.MODULE_ID });
            } else {
                url = $state.href("import-qcs", { "reqID": qcs.REFER_MODULE_LINK_ID, "qcsID": qcs.MODULE_ID });
            }

            window.open(url, '_blank');
        };

}]);