﻿
prmApp
    .controller('consalidatedBasePriceReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.consalidatedBasePriceReport = [];

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;
            $scope.revisedUserL1 = [];


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            //$scope.reportFromDate = '';
            //$scope.reportToDate = '';

            $scope.reportToDate = moment().format('YYYY-MM-DD');
            $scope.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");


            $scope.getConsalidatedBasePriceReport = function () {
                $scope.errMessage = '';



                reportingService.getconsolidatedbasepricereport($scope.reportFromDate, $scope.reportToDate)
                    .then(function (response) {
                        $scope.consalidatedBasePriceReport = response;

                        $scope.totalItems = $scope.consalidatedBasePriceReport.length;
                        $scope.consalidatedBasePriceReport.forEach(function (item, index) {
                            item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                            item.reqPostedOn = $scope.GetDateconverted(item.reqPostedOn);
                            item.startTime = $scope.GetDateconverted(item.startTime);

                            if (String(item.startTime).includes('9999')) {
                                item.startTime = '';
                            }

                        })

                    })




            }

            $scope.getConsalidatedBasePriceReport();

            $scope.GetReport = function () {
                //alasql.fn.handleDate = function (date) {
                //    return new moment(date).format("MM/DD/YYYY");
                //}

                // $scope.consalidatedReport = $scope.consalidatedReport.revisedUserL1;
               

                alasql('SELECT requirementID as [Requirement ID],title as [Requirement Title], ' +
                    'closed as Status, ' +
                    'reqPostedOn as [Posted On], quotationFreezTime as [Freez Time], startTime as [Negotiation scheduled time],IL1_initBasePrice as [Initial Least Base Price], ' +
                    'RL1_companyName as [L1 Company Name], RL1_initBasePrice as [L1 Initial Base Price], RL1_revBasePrice as [L1 Rev Base Price], ' + 
                    'RL2_companyName as [L2 Company Name], RL2_initBasePrice as [L2 Initial Base Price], RL2_revBasePrice as [L2 Rev Base Price],  ' +
                    'basePriceSavings as [Base Price Savings],savingsPercentage as [Savings %] ' +
                    'INTO XLSX(?, { headers: true, sheetid: "ConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["ConsolidatedReport.xlsx", $scope.consalidatedBasePriceReport]);


            }
            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };


        }]);