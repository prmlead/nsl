﻿
prmApp
    .controller('consalidatedReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {

            $scope.consolidatedReport = [];
            $scope.U_ID = userService.getUserId();
            $scope.COMP_ID = userService.getUserCompanyId();
            $scope.SESSION_ID = userService.getUserToken();

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;
            

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getConsolidatedReport(($scope.currentPage - 1), 10);
            };

            $scope.pageChanged = function () {
            };

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            $scope.consolidatedExcelReport = [];

            $scope.reportDatesObj =
            {
                "FROM_DATE": moment().subtract(30, "days").format("YYYY-MM-DD"),
                "TO_DATE": moment().format('YYYY-MM-DD')
            };

            $scope.getConsolidatedReport = function (recordsFetchFrom, pageSize)
            {
                if (!$scope.downloadExcel) {
                    $scope.consolidatedReport = [];
                }
                var params =
                {
                    "U_ID": $scope.U_ID,
                    "COMP_ID": $scope.COMP_ID,
                    "SESSION_ID": $scope.SESSION_ID,
                    "FROM_DATE": $scope.reportDatesObj.FROM_DATE,
                    "TO_DATE": $scope.reportDatesObj.TO_DATE,
                    "PageSize": recordsFetchFrom * pageSize,
                    "NumberOfRecords": pageSize
                };

                reportingService.getConsolidatedReport(params)
                    .then(function (response) {

                        if (response)
                        {
                            if (response && response.length > 0) {
                                response.forEach(function (item,index) {
                                    if (item.IS_CB_ENABLED != 1) {
                                        if (item.START_TIME) {
                                            item.START_TIME = $scope.GetDateconverted(item.START_TIME);
                                        } else {
                                            item.START_TIME = '';
                                        }
                                    } else {
                                        item.START_TIME = new moment(item.START_TIME).format("DD-MM-YYYY HH:mm");
                                        // return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                                    }
                                    item.QUOTATION_FREEZ_TIME = $scope.GetDateconverted(item.QUOTATION_FREEZ_TIME);
                                    item.REQ_POSTED_ON = $scope.GetDateconverted(item.REQ_POSTED_ON);
                                    item.END_TIME = $scope.GetDateconverted(item.END_TIME);
                                    if (item.VENDOR_PRICES && item.VENDOR_PRICES.length > 0) {
                                        item.VENDOR_PRICES.forEach(function (vendItem, vendItemIndex) {
                                            if (vendItem.RN === 1) {
                                                //item.L1_INITIAL_TOTAL_PRICE = vendItem.INITIAL_TOTAL_PRICE;
                                                item.L1_REV_TOTAL_PRICE = vendItem.REV_TOTAL_PRICE;
                                                item.L1_COMP_NAME = vendItem.COMP_NAME;
                                            }
                                            if (vendItem.RN === 2) {
                                                item.L2_REV_TOTAL_PRICE = vendItem.REV_TOTAL_PRICE;
                                                item.L2_COMP_NAME = vendItem.COMP_NAME;
                                            }
                                        });
                                    }
                                    item.TOTAL_VENDORS.forEach(function (vendItem, vendItemIndex) {
                                        if (vendItem.REQ_ID === item.REQ_ID) {
                                            item.QUOTATION_STATUS = (vendItem.TOTAL_VENDORS - item.QUOTES_TO_BE_RECEIVED) + "/" + vendItem.TOTAL_VENDORS;
                                        }
                                    });
                                    item.L1_INITIAL_TOTAL_PRICE = 0;
                                    if (item.L1_INITIAL_PRICES)
                                    {
                                        item.L1_INITIAL_TOTAL_PRICE = item.L1_INITIAL_PRICES.INITIAL_TOTAL_PRICE;
                                    }
                                    item.savingsPercentage = 0;
                                    if (item.L1_REV_TOTAL_PRICE)
                                    {
                                        item.savingsPercentage = (100 - (item.L1_REV_TOTAL_PRICE / item.L1_INITIAL_TOTAL_PRICE) * 100).toFixed(2);
                                    }
                                    // item.SAVINGS = item.L1_REV_TOTAL_PRICE - item.L1_INITIAL_TOTAL_PRICE;
                                });
                                $scope.consolidatedReportList = response;
                                if (!$scope.downloadExcel) {
                                    $scope.consolidatedReportList.forEach(function (item, index) {
                                        $scope.consolidatedReport.push(item);
                                    });
                                    $scope.totalItems = $scope.consolidatedReport[0].TOTAL_ROWS;
                                } else {
                                    $scope.consolidatedReportList.forEach(function (item, index) {
                                        $scope.consolidatedReportExcel.push(item);
                                    });
                                    downloadConsolidatedExcel();
                                }
                            }
                        }
                    });
            };

            $scope.getConsolidatedReport(0,10);

            $scope.downloadExcel = false;
            $scope.GetReport = function () {
                $scope.consolidatedReportExcel = [];
                $scope.downloadExcel = true;
                $scope.getConsolidatedReport(0, 0);
            };



            function downloadConsolidatedExcel() {
                var oldConsolidatedReport = JSON.stringify($scope.consolidatedReportExcel).replace(/null/g, '"0"');
                var newReport = JSON.parse(oldConsolidatedReport);
                alasql('SELECT REQ_NUMBER as [Req Number],REQ_TITLE as [Title],REQ_CURRENCY as [Requirement_Currency],REQ_POSTED_ON as [Posted On],QUOTATION_FREEZ_TIME as [Freeze Time],QUOTATION_STATUS as [Quotation Status], ' +
                    'START_TIME as [Scheduled],L1_INITIAL_TOTAL_PRICE as [Initial Total Price],L1_COMP_NAME as [L1 Company Name], L1_REV_TOTAL_PRICE as [L1 Rev Price], L2_COMP_NAME as [L2 Company Name],' +
                    'L2_REV_TOTAL_PRICE as [L2 Rev Price],SAVINGS as [Savings],savingsPercentage as [Savings %],' +
                    'EBIT_SAVINGS_IN_INR as [EBIT Savings(INR)],EBIT_SAVINGS_IN_REQ_CURRENCY as [EBIT Savings(RFQ POSTED CURRENCY)], REQ_STATUS as [STATUS]'+                  
                    'INTO XLSX(?, { headers: true, sheetid: "ConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["ConsolidatedReport.xlsx", newReport]);
                $scope.downloadExcel = false;
            }
           

           
            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

        }]);