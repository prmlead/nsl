﻿prmApp
    .controller('techevalresponsesCtrl', ["$scope", "$state", "$stateParams", "$log", "$window", "userService", "growlService", "techevalService", "fileReader",
        function ($scope, $state, $stateParams, $log, $window, userService, growlService, techevalService, fileReader) {
            $scope.reqID = 0;
            $scope.totalMarks = 0;
            $scope.evalID = $stateParams.evalID == "" || !$stateParams.evalID ? 0 : $stateParams.evalID;
            $scope.vendorID = $stateParams.vendorID == "" ? 0 : $stateParams.vendorID;
            $scope.compID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();
            $scope.userID = userService.getUserId();
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            $scope.load = false;
            $scope.answers = [];
            $scope.responses = [];
            $scope.userResponse = '';
            $scope.techEvalObj = $stateParams.techEvalObj;
            $log.info($scope.techEvalObj);

            $scope.buttonDisabled = false;



            if (!$scope.techEvalObj) {
                techevalService.GetTechEvalution($scope.evalID, $scope.reqID, $scope.vendorID)
                    .then(function (response) {
                        $scope.techEvalObj = response[0];
                    });
            }


            $scope.saveRequestDetails = function (isApproved) {

                if ($scope.techEvalObj) {
                    $scope.techEvalObj.isApproved = isApproved;
                    $scope.techEvalObj.modifiedBy = $scope.userID;
                    //$scope.techEvalObj.comments = $scope.userID;
                    var params = {
                        techeval: $scope.techEvalObj
                    };

                    techevalService.saveTechApproval(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                growlService.growl("Saved Successfully.", "success");
                                $window.history.back();
                            }
                        })
                }
            };

            $scope.totalUserMarks = function () {
                var marks = 0;
                if ($scope.load) {
                    $scope.answers.forEach(function (item, index) {
                        marks = marks + item.userMarks;
                    });
                }

                return marks;
            }

            $scope.getResponses = function () {
                techevalService.getresponses($scope.reqID, $scope.evalID, $scope.vendorID)
                    .then(function (response) {
                        $scope.answers = response;
                        $scope.answers.forEach(function (item, index) {
                            $scope.totalMarks = $scope.totalMarks + item.userMarks;
                            item.vendor.userID = $scope.userID;
                            item.answerArray = [];
                            if (item.questionDetails.type == 'CHECK_BOX' || item.questionDetails.type == 'RADIO_BUTTON') {
                                item.questionDetails.optionsArray = item.questionDetails.options.split("$~");
                                if (item.answerText != "") {
                                    item.answerArray = item.answerText.split("$~");
                                }
                            }
                            else {
                                item.questionDetails.optionsArray = [];
                            }
                        });

                        $scope.load = true;
                    });
            };

            $scope.getResponses();

            $scope.toggle = function (option, list, qid) {
                $scope.answers.forEach(function (item, index) {
                    if (item.questionDetails.questionID == qid) {
                        if (item.answerArray.indexOf(option) > -1) {
                            item.answerArray.splice(item.answerArray.indexOf(option), 1);
                        }
                        else {
                            item.answerArray.push(option);
                        }
                    }
                });
            };


            $scope.radio = function (option, qid) {
                $scope.answers.forEach(function (item, index) {
                    if (item.questionDetails.questionID == qid) {
                        item.answerText = option;
                    }
                });
            };

            $scope.saveresponse = function () {
                $scope.answers.forEach(function (item, index) {
                    if (item.questionDetails.type == 'CHECK_BOX') {
                        item.answerText = '';
                        item.answerArray.forEach(function (item2, index2) {
                            item.answerText += item2 + '$~';
                        });

                        item.answerText = item.answerText.slice(0, -2);
                        var tem = item.answerText;
                    }
                });

                $scope.answers.isValid = 1;
                $scope.buttonDisabled = true;

                var params = {
                    answers: $scope.answers
                };

                techevalService.saveresponse(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            if ($scope.isCustomer) {
                                growlService.growl("Evaluated Successfully.", "success");
                                $scope.getResponses();
                                $window.history.back();
                            }
                            else {
                                growlService.growl("Answers Saved Successfully.", "success");
                                $scope.getResponses();
                                $window.history.back();
                            }
                        }
                    })
            };



            $scope.cancelResponse = function (isApproved) {
                $window.history.back();
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        var bytearray = new Uint8Array(result);
                        var arrayByte = $.makeArray(bytearray);
                        var ItemFileName = $scope.file.name;
                        var index = _.indexOf($scope.answers, _.find($scope.answers, function (o) { return o.answerID == id; }));
                        var obj = $scope.answers[index];
                        obj.attachment = arrayByte;
                        obj.attachmentName = ItemFileName;
                        $scope.answers.splice(index, 1, obj);
                    });
            }
        }]);