﻿prmApp
    .controller('vendorInvoices1', function ($scope, $state, $stateParams, userService, growlService,
        workflowService,
        auctionsService, fileReader, $log, $window, poService) {
        

        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
       
        $scope.compID = userService.getUserCompanyId();

        $scope.payment = {
            credits: {
                enabled: false
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                width: 425,
                height:240
            },
            title: {
                text: 'Payment'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            legend: {
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'top',
                x: 0,
                y: 40
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Received',
                    y: 60
                }, {
                    name: 'OutStanding',
                    y: 40
                }]
            }]
        }


        $scope.status = {
            credits: {
                enabled: false
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                width: 425,
                height: 240
            },
            title: {
                text: 'Status'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            legend: {
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'top',
                x: 0,
                y: 40
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: '50 Paid',
                    y: 50
                }, {
                    name: '0 paid',
                    y: 0
                }, {
                    name: '7 Unpaid',
                    y: 7
                }, {
                    name: '42 Overdue',
                    y: 30
                }, {
                    name: '4 Unconfirmed',
                    y: 4
                }]
            }]
        }

        



    });