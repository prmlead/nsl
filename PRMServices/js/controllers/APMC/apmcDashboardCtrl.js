﻿prmApp
    .controller('apmcDashboardCtrl', ["$scope", "$stateParams", "$state", "$window", "$log", "$filter", "$http", "domain", "userService", "growlService", "apmcService", "SignalRFactory", "fileReader", function ($scope, $stateParams, $state, $window, $log, $filter, $http, domain, userService, growlService, apmcService, SignalRFactory, fileReader) {
        $scope.companyID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.currentID = userService.getUserId();
        $scope.isCustomer = userService.getUserType() == 'CUSTOMER';
        $scope.isSuperUser = userService.getUserObj().isSuperUser;

        $scope.quantityNegotiationID = 0;

        $scope.showInclusive = false;

        $scope.vendorQuantityObj = {
            price: 0,
            quantity: 0,
            showDelete: false,
            vendorID: $scope.currentID,
            userID: $scope.currentID
        }

        $scope.vendorQuantityList = []

        $scope.units = [
            { unitName: 'MT' },
            { unitName: 'BAGS' },
            { unitName: 'QUINTALS' }
        ]

        $scope.getFile = function () {
            $scope.progress = 0;
            var quotation = $("#quotation")[0].files[0];
            if (quotation != undefined && quotation != '') {
                $scope.file = $("#quotation")[0].files[0];
                $log.info($("#quotation")[0].files[0]);
            }
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    var bytearray = new Uint8Array(result);
                    if (quotation != undefined && quotation != '') {
                        $scope.apmcDashboard.apmcNegotiationList[0].fileStream = $.makeArray(bytearray);
                        $scope.apmcDashboard.apmcNegotiationList[0].fileName = $scope.file.name;
                    }
                });
        };

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {

                    if (id == "excelquotation") {
                        var bytearray = new Uint8Array(result);
                        $scope.quotationAttachment = $.makeArray(bytearray);
                        $scope.uploadquotationsfromexcel();
                    }
                    else {
                        var bytearray = new Uint8Array(result);
                        var arrayByte = $.makeArray(bytearray);
                        var ItemFileName = $scope.file.name;
                        var index = _.indexOf($scope.auctionItemVendor.listRequirementItems, _.find($scope.auctionItemVendor.listRequirementItems, function (o) {
                            return o.productSNo == id;
                        }));
                        var obj = $scope.auctionItemVendor.listRequirementItems[index];
                        obj.itemAttachment = arrayByte;
                        obj.attachmentName = ItemFileName;
                        $scope.auctionItemVendor.listRequirementItems.splice(index, 1, obj);
                    }
                });
        }


        $scope.apmcvendorlist = [];
        $scope.apmcDashboard = {
            apmcNegotiationList: []
        };
        $scope.prevNegotiation = {};

        var apmcHub = SignalRFactory('', 'apmcHub')

        $scope.getapmcvendorlist = function () {
            apmcService.getapmcvendorlist()
                .then(function (response) {
                    $scope.apmcvendorlist = response;
                })
        }

        $scope.checkConnection = function () {
            if (apmcHub) {
                return apmcHub.getStatus();
            } else {
                return 0;
            }
        };

        $scope.auctionItemVendor = [];

        $scope.reconnectHub = function () {
            if (apmcHub) {
                if (apmcHub.getStatus() == 0) {
                    apmcHub.reconnect();
                    return true;
                }
            } else {
                apmcHub = SignalRFactory('', 'apmcHub');
            }
        }

        $scope.addQuantity = function () {
            var newQuantity = {
                quantity: 0,
                price: 0,
                vendorID: $scope.currentID,
                userID: $scope.currentID,
                apmcNegotiationID: $scope.quantityNegotiationID,
                showDelete: true
            }
            $scope.vendorQuantityList.push(newQuantity);
        }

        $scope.priceLimitError = false;

        $scope.savevendorquantitylist = function () {

            $scope.priceLimitError = false;

            if ($scope.vendorQuantityList && $scope.vendorQuantityList.length > 1) {
                var price = $scope.vendorQuantityList[0].price + 10;
                $scope.vendorQuantityList.forEach(function (item, index) {
                    if (item.price > price) {
                        item.priceLimitError = 'This price is not acceptable, please contact the customer.';
                        $scope.priceLimitError = true;
                    }
                })
            }

            if ($scope.priceLimitError == false) {
                $scope.apmcDashboard.apmcNegotiationList[0].userID = userService.getUserId();
                var params = {
                    apmcNegotiation: $scope.apmcDashboard.apmcNegotiationList[0]
                }
                apmcService.saveapmcnegotiation(params)
                    .then(function (response) {
                        var params = {
                            list: $scope.vendorQuantityList
                        }
                        apmcService.savevendorquantitylist(params)
                            .then(function (response) {
                                if (response.errorMessage != '') {
                                    growlService.growl(response.errorMessage, "inverse");
                                }
                                else {
                                    growlService.growl("Saved Successfully.", "success");
                                    $scope.getapmcdashboard();
                                }
                            });

                    })
            }
        }

        $scope.deleteQuantity = function (quantity) {
            $scope.vendorQuantityList.splice($scope.vendorQuantityList.indexOf(quantity), 1);
        }

        $scope.invokeSignalR = function (methodName, params, callback) {
            if ($scope.checkConnection() == 1) {
                apmcHub.invoke(methodName, params, function (req) {
                    if (callback) {
                        callback();
                    }
                })
            } else {
                $scope.reconnectHub();
                apmcHub.invoke(methodName, params, function (req) {
                    if (callback) {
                        callback();
                    }
                })
            }
        }

        $scope.selectVendor = function (vendor) {
            vendor.isSelected = true;
        }

        $scope.createNewNegotiation = function () {
            //$state.go('saveapmcinput');

            var url = $state.href("saveapmcinput", {});
            window.open(url, '_blank');
        }

        $scope.saveapmcinput = function () {
            $scope.apmcDashboard.apmcInput.company = {
                companyID: userService.getUserCompanyId()
            }
            $scope.apmcDashboard.apmcInput.userID = userService.getUserId();
            var params = {
                apmcInput: $scope.apmcDashboard.apmcInput
            }
            apmcService.saveapmcinput(params)
                .then(function (response) {
                    $scope.selectedapmcs = $filter('filter')($scope.apmcvendorlist, { isSelected: true });
                    for (i = 0; i < $scope.selectedapmcs.length; i++) {
                        $scope.selectedapmcs[i].apmcNegotiationID = response.objectID;
                        $scope.selectedapmcs[i].custPriceInclFinal = $scope.apmcDashboard.apmcInput.inputPrice;
                        $scope.selectedapmcs[i].vendorCompany = {
                            vendorID: $scope.selectedapmcs[i].company.companyID
                        }
                    }
                    var params = {
                        apmcNegotiationList: $scope.selectedapmcs,
                        apmcNegotiationID: response.objectID,
                        userID: userService.getUserId()
                    }
                    apmcService.saveapmcnegotiationlist(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                growlService.growl("Saved Successfully.", "success");
                                $scope.getapmcdashboard();
                            }
                        })
                })
        }

        $scope.getapmcdashboard = function (source) {
            apmcService.getapmcnegotiation(0)
                .then(function (response) {
                    $scope.apmcDashboard = response;
                    $scope.quantityNegotiationID = $scope.apmcDashboard.apmcInput.apmcNegotiationID;
                    $scope.vendorQuantityObj.apmcNegotiationID = $scope.quantityNegotiationID;
                    $scope.vendorQuantityObj.price = $scope.apmcDashboard.apmcNegotiationList[0].vendorPriceExclFinal;
                    if ($scope.vendorQuantityList.length == 0) {
                        $scope.vendorQuantityList.push($scope.vendorQuantityObj);
                    }
                    if ($scope.apmcDashboard.apmcInput == null || $scope.apmcDashboard.apmcInput.apmcNegotiationID == 0) {
                        $scope.showAllColumns = false;
                        //$scope.apmcDashboard.apmcNegotiationList = $scope.apmcvendorlist;
                    }
                    else {
                        if (source != 'signalr') {
                            $scope.invokeSignalR('JoinGroup', 'apmcHub' + $scope.apmcDashboard.apmcInput.apmcNegotiationID);
                        }
                        $scope.showAllColumns = true;
                        $scope.getapmcaudit($scope.apmcDashboard.apmcInput.apmcNegotiationID, $scope.apmcDashboard.apmcNegotiationList[0].vendor.userID);
                        if ($scope.apmcDashboard.apmcNegotiationList[0].isNewNegotiationStarted == 1) {
                            apmcService.getapmcnegotiation($scope.apmcDashboard.apmcNegotiationList[0].prevNegotiationID)
                                .then(function (response2) {
                                    $scope.prevNegotiation = response2;
                                    $scope.quantityNegotiationID = $scope.prevNegotiation.apmcInput.apmcNegotiationID;
                                    $scope.vendorQuantityObj.apmcNegotiationID = $scope.quantityNegotiationID;
                                    $scope.vendorQuantityObj.price = $scope.prevNegotiation.apmcNegotiationList[0].vendorPriceExclFinal;
                                    if ($scope.vendorQuantityList.length == 0) {
                                        $scope.vendorQuantityList.push($scope.vendorQuantityObj);
                                    }
                                    $scope.getapmcaudit($scope.prevNegotiation.apmcInput.apmcNegotiationID, $scope.prevNegotiation.apmcNegotiationList[0].vendor.userID);
                                })
                        }
                    }
                });
        }

        $scope.getapmcaudit = function (apmcNegotiationID, vendorID) {
            apmcService.getapmcaudit(apmcNegotiationID, vendorID)
                .then(function (response) {
                    $scope.apmcaudit = response;
                    $scope.apmcaudit.forEach(function (item, index) {
                        item.created = new moment(item.created).format("DD-MM-YYYY HH:mm:ss");
                    });
                })
        }

        $scope.getapmcvendorlist();
        $scope.getapmcdashboard();

        $scope.goToAPMCEdit = function (apmcID) {
            //$state.go("saveapmc", { "apmcID": apmcID });

            var url = $state.href("saveapmc", { "apmcID": apmcID });
            window.open(url, '_blank');
        }

        $scope.gotomanageapmc = function () {
            //$state.go("manageapmc");
            var url = $state.href("manageapmc", {});
            window.open(url, '_blank');
        }

        $scope.saveapmcnegotiation = function (apmcPrice) {
            apmcPrice.userID = userService.getUserId();
            var params = {
                apmcNegotiation: apmcPrice
            }
            apmcService.saveapmcnegotiation(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                        $scope.getapmcdashboard();
                    }
                })
        }

        $scope.gotovendorpurchasehistory = function (apmcnegid, vendorID) {
            //$state.go('apmcpurchasehistory', { apmcnegid: apmcnegid, vendorid: vendorID });

            var url = $state.href("apmcpurchasehistory", { apmcnegid: apmcnegid, vendorid: vendorID });
            window.open(url, '_blank');
        }

        $scope.goToAPMCNegotiations = function () {
            //$state.go("apmcnegotiations", { "userID": userService.getUserId() });

            var url = $state.href("apmcnegotiations", { "userID": userService.getUserId() });
            window.open(url, '_blank');
        }

        $scope.freezeapmcnegotiation = function (apmcPrice) {
            apmcPrice.isFrozen = 1;
            var params = {
                apmcNegotiation: apmcPrice
            }
            apmcService.saveapmcnegotiation(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                        location.reload();
                    }
                })
        }

        apmcHub.on('refreshDashboard', function (negotiation) {
            $scope.getapmcdashboard('signalr');
        })
    }]);