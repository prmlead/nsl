﻿prmApp
    .controller('apmcPurchasesCtrl', ["$scope", "$stateParams", "$state", "$window", "$log", "$filter", "$http", "domain", "userService", "growlService", "apmcService", 
        function ($scope, $stateParams, $state, $window, $log, $filter, $http, domain, userService, growlService, apmcService) {
            $scope.getapmcvendorquantitylist = function () {
                apmcService.getapmcvendorquantitylist($stateParams.apmcnegid, $stateParams.vendorid)
                    .then(function (response) {
                        $scope.vendorQuantityHistory = response;
                        $scope.vendorQuantityHistory.forEach(function (item, index) {
                            item.dateCreated = new moment(item.dateCreated).format("DD-MM-YYYY HH:mm");
                        });                    

                    })
            }
            $scope.getapmcvendorquantitylist();

            $scope.apmcaudit = [];

            $scope.getapmcaudit = function () {
                apmcService.getapmcaudit($stateParams.apmcnegid, $stateParams.vendorid)
                    .then(function (response) {
                        $scope.apmcaudit = response;

                        console.log('===============>');
                        console.log($scope.apmcaudit);
                        console.log('===============>');

                        $scope.apmcaudit.forEach(function (item, index) {
                            item.created = new moment(item.created).format("DD-MM-YYYY HH:mm:ss");
                        });
                    })
            }

            $scope.getapmcaudit();

        }]);