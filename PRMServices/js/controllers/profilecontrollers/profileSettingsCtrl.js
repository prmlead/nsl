﻿prmApp
    .controller('profileSettingsCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {

            $scope.NegotiationSettingsValidationMessage = '';
            $scope.prSettings = [];
            $scope.NegotiationSettings = [];
            $scope.checkFrom = '';
            $scope.checkTo = '';
            $scope.minDateMoment = moment();
            $scope.NegotiationSettings = {
                reqFromDate: moment().subtract(30, "days").format("DD/MM/YYYY"),
                reqToDate: moment().format('DD/MM/YYYY'),
                bestPriceFromDate: moment().subtract(30, "days").format("DD/MM/YYYY"),
                bestPriceToDate: moment().format('DD/MM/YYYY'),
                disableLocalGSTFeature: false
            };

            $scope.checkValidFrom = '';
            $scope.checkValidTo = '';

            $scope.weekDays = [{
                name: 'Sunday',
                isSelected: false
            }, {
                name: 'Monday',
                isSelected: false
            }, {
                name: 'Tuesday',
                isSelected: false
            }, {
                name: 'Wednesday',
                isSelected: false
            }, {
                name: 'Thursday',
                isSelected: false
            }, {
                name: 'Friday',
                isSelected: false
            }, {
                name: 'Saturday',
                isSelected: false
            }];

            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

            $scope.callGetUserDetails = function () {
                $log.info("IN GET USER DETAILS");
                userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            $scope.userDetails = response;
                            $scope.NegotiationSettings = $scope.userDetails.NegotiationSettings;
                            var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                            $scope.days = parseInt(duration[0]);
                            duration = duration[1];
                            duration = duration.split(":", 4);
                            $scope.hours = parseInt(duration[0]);
                            $scope.mins = parseInt(duration[1]);
                            $scope.NegotiationSettings.bestPriceFromDate = userService.toLocalDate($scope.NegotiationSettings.bestPricefromDate);
                            $scope.NegotiationSettings.bestPriceToDate = userService.toLocalDate($scope.NegotiationSettings.bestPricetoDate);
                            $scope.NegotiationSettings.reqFromDate = userService.toLocalDate($scope.NegotiationSettings.fromDate);
                            $scope.NegotiationSettings.reqToDate = userService.toLocalDate($scope.NegotiationSettings.toDate);
                            $scope.NegotiationSettings.disableLocalGSTFeature = $scope.userDetails.NegotiationSettings.disableLocalGSTFeature;

                            $scope.checkValidFrom = $scope.NegotiationSettings.reqFromDate;
                            $scope.checkValidTo = $scope.NegotiationSettings.reqToDate;
                        }
                    });
            };

            $scope.callGetUserDetails();

            function WithoutTime(dateTime) {
                var date = new Date(dateTime);
                date.setHours(0, 0, 0, 0);
                return date;
            }

            $scope.NegotiationTimeValidation = function () {
                $scope.NegotiationSettingsValidationMessage = '';
                $scope.userFromValidation = false;
                $scope.userToValidation = false;
                $scope.bestPriceuserFromValidation = false;
                $scope.bestPriceuserToValidation = false;

                if (!$scope.NegotiationSettings.minReductionAmount) {
                    $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Min.Amount reduction';
                    return;
                }

                if (!$scope.NegotiationSettings.rankComparision) {
                    $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Rank Comparision price';
                    return;
                }

                if ($scope.NegotiationSettings.minReductionAmount >= 0 && $scope.NegotiationSettings.rankComparision > $scope.NegotiationSettings.minReductionAmount) {
                    $scope.NegotiationSettingsValidationMessage = 'Please enter Valid Rank Comparision price less than Min. Amount reduction';
                    return;
                }
                if (!$scope.days && $scope.days !== 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
                    return;
                }
                if (!$scope.hours && $scope.hours !== 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (!$scope.mins && $scope.mins !== 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minutes';
                    return;
                }
                if ($scope.mins >= 60) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minutes';
                    return;
                }
                if ($scope.hours > 24) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if ($scope.hours === 24 && $scope.mins > 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minuts';
                    return;
                }
                if ($scope.mins < 5 && $scope.hours === 0 && $scope.days === 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minutes';
                    return;
                }

                if (!$scope.NegotiationSettings.reqFromDate) {
                    $scope.FromValidation = true;
                    return false;
                }

                if (!$scope.NegotiationSettings.reqToDate) {
                    $scope.ToValidation = true;
                    return false;
                }

                if (!$scope.NegotiationSettings.bestPriceFromDate) {
                    $scope.bestPriceFromValidation = true;
                    return false;
                }

                if (!$scope.NegotiationSettings.bestPriceToDate) {
                    $scope.bestPriceToValidation = true;
                    return false;
                }

                var CurrentDate = new moment();

                var CurrentDate = WithoutTime(CurrentDate);

                var bestPriceToDate = new Date(moment(userService.toUTCTicks($scope.NegotiationSettings.bestPriceToDate)));
                var bestPriceToDateMilliseconds = parseInt(bestPriceToDate.getTime() / 1000.0);
                $scope.bestPriceTovalidity = "/Date(" + bestPriceToDateMilliseconds + "000+0530)/";


                var bestPriceFromDate = new Date(moment(userService.toUTCTicks($scope.NegotiationSettings.bestPriceFromDate)));
                var bestPriceFromDateMilliseconds = parseInt(bestPriceFromDate.getTime() / 1000.0);
                $scope.bestPriceFromvalidity = "/Date(" + bestPriceFromDateMilliseconds + "000+0530)/";

                var validFromBestPriceTemp = moment($scope.NegotiationSettings.bestPriceFromDate, "DD-MM-YYYY HH:mm");

                var validToBestPriceTemp = moment($scope.NegotiationSettings.bestPriceToDate, "DD-MM-YYYY HH:mm");

                var From = new Date(moment(userService.toUTCTicks($scope.NegotiationSettings.reqFromDate)));
                var milliseconds = parseInt(From.getTime() / 1000.0);
                $scope.validityFrom = "/Date(" + milliseconds + "000+0530)/";

                var To = new Date(moment(userService.toUTCTicks($scope.NegotiationSettings.reqToDate)));
                milliseconds = parseInt(To.getTime() / 1000.0);
                $scope.validityTo = "/Date(" + milliseconds + "000+0530)/";

                var validFromTemp = moment($scope.NegotiationSettings.reqFromDate, "DD-MM-YYYY HH:mm");
                var validToTemp = moment($scope.NegotiationSettings.reqToDate, "DD-MM-YYYY HH:mm");

                if (!$scope.checkValidFrom) {
                    if (validFromTemp >= validToTemp) {
                        $scope.userFromValidation = true;
                    } else if (validToTemp < CurrentDate) {
                        $scope.userToValidation = true;
                    } else if (validFromBestPriceTemp >= validToBestPriceTemp) {
                        $scope.bestPriceuserFromValidation = true;
                    } else if (validToBestPriceTemp < CurrentDate) {
                        $scope.bestPriceuserToValidation = true;
                    }
                } else if ($scope.checkValidFrom) {
                    if (validToTemp < CurrentDate || validFromTemp >= validToTemp) {
                        $scope.userToValidation = true;
                    }

                    if (validToBestPriceTemp < CurrentDate) { //  || validToBestPriceTemp >= validToTemp
                        $scope.bestPriceuserToValidation = true;
                    }
                }
                
                if ($scope.userFromValidation && $scope.userFromValidation || $scope.userToValidation && $scope.userToValidation ||
                    $scope.bestPriceuserFromValidation || $scope.bestPriceuserToValidation) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Validate All Fields';
                    return;
                }
            };

            $scope.saveNegotiationSettings = function () {
                $scope.NegotiationTimeValidation();

                if ($scope.NegotiationSettingsValidationMessage == '') {
                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                    var params = {
                        "NegotiationSettings": {
                            "userID": userService.getUserId(),
                            "minReductionAmount": $scope.NegotiationSettings.minReductionAmount,
                            "rankComparision": $scope.NegotiationSettings.rankComparision,
                            "negotiationDuration": $scope.NegotiationSettings.negotiationDuration,
                            "sessionID": userService.getUserToken(),
                            "compInterest": $scope.NegotiationSettings.compInterest,
                            "fromDate": $scope.validityFrom,
                            "toDate": $scope.validityTo,
                            "bestPricefromDate": $scope.bestPriceFromvalidity,
                            "bestPricetoDate": $scope.bestPriceTovalidity,
                            "disableLocalGSTFeature": $scope.NegotiationSettings.disableLocalGSTFeature
                        }
                    };
                    $http({
                        method: 'POST',
                        url: domain + 'savenegotiationsettings',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data && (response.data.errorMessage == "" || response.data.errorMessage == null)) {
                            //$scope.NegotiationSettings = response.data;
                            growlService.growl('Successfully Saved', 'success');
                        }
                        else {
                            growlService.growl('Not Saved', 'inverse');
                        }
                    });
                }
            };

            $scope.goTocatergory = function () {
                var url = $state.href('category', {});
                $window.open(url, '_blank');
            };

            $scope.goToworkflow = function () {
                var url = $state.href('workflow', {});
                $window.open(url, '_blank');
            };

            $scope.savePRSettings = function () {
                var days = '';
                var tempDays = [];
                var selectedDays = $scope.weekDays.filter(function (day) {
                    return day.isSelected;
                });

                if (selectedDays && selectedDays.length > 0) {
                    selectedDays.forEach(function (obj, itemIndex) {
                        tempDays.push(obj.name);
                    });

                    days = _.join(tempDays, ';');
                }

                $scope.prSettings[0].configValue = days;
                $scope.prSettings[0].configText = days;
                var params = {
                    "listCompanyConfiguration": $scope.prSettings,
                    "sessionID": userService.getUserToken()
                };
                $http({
                    method: 'POST',
                    url: domain + 'savecompanyconfiguration',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data && !response.data.errorMessage) {
                        growlService.growl('Successfully Saved', 'success');
                        location.reload();
                    }
                    else {
                        growlService.growl('Not Saved', 'inverse');
                    }
                });
            };

            $scope.getPRSettings = function () {
                auctionsService.GetCompanyConfiguration(userService.getUserCompanyId(), "PR_SETTINGS", userService.getUserToken())
                    .then(function (response) {
                        $scope.prSettings = response;
                        if (!$scope.prSettings || $scope.prSettings.length <= 0) {
                            $scope.prSettings = [{
                                "compConfigID": 0,
                                "compID": userService.getUserCompanyId(),
                                "configKey": "PR_SETTINGS",
                                "configValue": "",
                                "configText": '',
                                "isValid": true
                            }];
                        } else {
                            var days = $scope.prSettings[0].configValue;

                            if (days) {
                                var tempDays = days.split(";");
                                tempDays.forEach(function (obj, itemIndex) {
                                    var weekDay = $scope.weekDays.filter(function (day) {
                                        return day.name === obj;
                                    });

                                    if (weekDay && weekDay.length > 0) {
                                        weekDay[0].isSelected = true;
                                    }
                                });
                            }
                        }
                    });
            };

            $scope.getPRSettings();

        }]);