prmApp

    .controller('viewprofileCtrl1', ["$scope", "$http", "$state", "domain", "$filter", "$stateParams", "$timeout", "auctionsService", "userService", "SignalRFactory", "fileReader", "growlService", "$window",
        function ($scope, $http, $state, domain, $filter, $stateParams, $timeout, auctionsService, userService, SignalRFactory, fileReader, growlService, $window) {
            $scope.userId = $stateParams.Id;
            $scope.vendorGstDetailsObj = {
                general: {},
                filing: {},
                isError: false,
                error: 'test'
            };
            $scope.vendObj = {
                vendorName: ''
            }
            $scope.viewProfileUserId = $stateParams.Id;
            $scope.callID = $stateParams.callID;
            $scope.userObj = {
                //logoURL: '../js/resources/images/chart.png'
            };

            this.userdetails = {};
            $scope.userRatings = [];
            $scope.isCommentPosted = false;
            $scope.sessionid = userService.getUserToken();
            $scope.ratingRequirement = 0;
            $scope.userOverallRating = 0;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            $scope.subcategories = '';
            $scope.categories = '';
            $scope.isValidToSubmitReview = false;

            var today = moment();
            //$scope.FromDate = today.add('days', -30).format('YYYY-MM-DD');
            //today = moment().format('YYYY-MM-DD');
            //$scope.ToDate = today;

            $scope.dateobj = {
                FromDate: today.add('days', -30).format('YYYY-MM-DD'),
                ToDate: moment().format('YYYY-MM-DD')
            }

            $scope.VEND_NAME = '';
            // Pagination For Overall Vendors//
            $scope.page = 0;
            $scope.PageSize = 50;
            $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
            $scope.totalCount = 0;
            // Pagination For Overall Vendors//
            var loginUserData = userService.getUserObj();
            $scope.vendorsList = [];
            $scope.GetCompanyVendors = function (IsPaging, searchString, fieldType) {
                if (searchString) {
                    $scope.fetchRecordsFrom = 0;
                }

                $scope.isOTPVerified = loginUserData.isOTPVerified;
                $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
                $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken(), "PageSize": $scope.fetchRecordsFrom, "NumberOfRecords": $scope.PageSize, "searchString": searchString ? searchString : "" };
                if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
                    userService.GetCompanyVendors($scope.params)
                        .then(function (response) {
                            $scope.vendorsList = response;
                            if ($scope.vendorsList.length > 0 && $scope.vendorsList) {
                                $scope.vendorDisplayCollecton = [].concat($scope.vendorsList);
                                //$scope.totalCount = $scope.vendorsList[0].totalVendors;
                            }

                            if (!$scope.vendorDisplayCollecton) {
                                $scope.vendorDisplayCollecton = [];
                            }

                            $scope.fillingProduct($scope.vendorDisplayCollecton, fieldType, searchString);
                        });
                }
            };

            $scope.autofillProduct = function (VendName, fieldType) {
                $scope['ItemSelected_'] = false;
                var output = [];
                if (VendName && VendName != '' && VendName.length > 0) {
                    $scope.searchingVendor = angular.lowercase(VendName);
                    $scope.userproductsLoading = true;
                    $scope.GetCompanyVendors(0, $scope.searchingVendor, fieldType);
                } else {
                    $scope.VEND_NAME = '';
                    $scope.productIDorNameDisplay = false;
                }
            };

            $scope.fillingProduct = function (output, fieldType, searchString) {

                output = $scope.vendorDisplayCollecton.filter(function (vend) {

                    return vend;
                });


                if (fieldType == 'VENDOR') {
                    $scope["filterVendors_"] = output;

                }

            }
            $scope.fillTextbox = function (selVend) {
                $scope['ItemSelected_'] = true;
                $scope.VEND_NAME = selVend.companyName.toUpperCase();
                $scope.productIDorNameDisplay = true;
                $scope.viewProfileUserId = selVend.userID;
                $scope.userId = selVend.userID;
                $scope.vendObj.vendorName = $scope.VEND_NAME;
                //$scope. = $scope.VEND_NAME;
                $scope['filterVendors_'] = null;

            }
            $scope.getAnalysis = function () {

                $scope.getProfileDetails();
                $scope.GetVendorRatings();
                $scope.vendorstatistics();
                $scope.getAlternateContacts();
                $scope.getUserCredentials();
                $scope.getuserinfo();
                $scope.GetOnlineStatus();
                $scope.GetVendorProducts();
                $scope.vendorstatistics();
                $scope.getactiveleads();
                $scope.GetVendorAnalysis();
                $scope.productIDorNameDisplay = false;
            }

            
            $scope.vendorStatistics = {};

            /*getuserdetails START*/
            $scope.getProfileDetails = function () {
                userService.getProfileDetails({ "userid": $scope.userId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.userObj = response;
                        if ($scope.userObj.userGSTInfo && $scope.userObj.userGSTInfo.length > 0) {
                            $scope.userObj.selectedGSTObj = $scope.userObj.userGSTInfo[0];
                            $scope.getGSTFilingDetails($scope.userObj.selectedGSTObj.gstNumber);
                            $scope.getGSTCompanyDetails($scope.userObj.selectedGSTObj.gstNumber);
                        } else {
                            $scope.vendorGstDetailsObj.isError = true;
                            $scope.vendorGstDetailsObj.error = 'Vendor GST number missing.';
                        }
                        var data = response.establishedDate;
                        var date = new Date(parseInt(data.substr(6)));
                        $scope.userObj.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();



                        for (i = 0; i < $scope.userObj.subcategories.length; i++) {

                            $scope.categories = '';

                            $scope.subcategories += $scope.userObj.subcategories[i].subcategory + "; ";

                        }

                        if ($scope.userObj.subcategories) {

                            $scope.userObj.subcategories.forEach(function (item, index) {

                                var n = false;
                                n = $scope.categories.includes(item.category + ";");
                                if (n == false) {
                                    $scope.categories += item.category + "; ";
                                }
                            });
                        }


                        if ($scope.categories == '') {
                            $scope.categories = $scope.userObj.category + "; ";
                        }
                        $scope.userObj.createdOn = new moment($scope.userObj.createdOn).format("DD-MM-YYYY");
                        $scope.userObj.oemKnownSince = new moment($scope.userObj.oemKnownSince).format("DD-MM-YYYY");
                        $scope.userObj.aboutUs = $scope.userObj.aboutUs.replace(/\u000a/g, "<br />");
                        $scope.userObj.address = $scope.userObj.address.replace(/\u000a/g, "<br />");



                    });
            }

            if ($scope.callID == 0) {
                $scope.getProfileDetails();
            }

            /*getuserdetails END*/
            $scope.GetVendorRatings = function () {
                userService.GetVendorRatings($scope.userId)
                    .then(function (response) {
                        $scope.userRatings = response;
                        $scope.userRatings.forEach(function (item, index) {
                            item.DATE_CREATED = $scope.GetDateconverted(item.DATE_CREATED);
                            item.overalRating = parseInt((item.DELIERY_RATING + item.EMERGENCY_RATING + item.QUALITY_RATING
                                + item.RESPONSE_RATING + item.SERVICE_RATING
                                + item.KEY_CLIENT_TELE_RATING + item.SUPPLY_CAPACITY_RATING
                                + item.PRICE_TRANSPARENCY_RATING + item.SAFETY_RATING
                                /*+ item.COMMENTS_RATING*/
                            ) / 9);
                            $scope.userOverallRating = $scope.userOverallRating + ((item.DELIERY_RATING + item.EMERGENCY_RATING + item.QUALITY_RATING
                                + item.RESPONSE_RATING + item.SERVICE_RATING
                                + item.KEY_CLIENT_TELE_RATING + item.SUPPLY_CAPACITY_RATING
                                + item.PRICE_TRANSPARENCY_RATING + item.SAFETY_RATING
                                /*+ item.COMMENTS_RATING*/
                            ) / 9);
                        });

                        if ($scope.userRatings && $scope.userRatings.length > 0) {
                            $scope.userOverallRating = parseInt($scope.userOverallRating / $scope.userRatings.length);
                        }

                    });
            }
            if ($scope.callID == 0) {
                $scope.GetVendorRatings();
            }


            $scope.getNumber = function (num) {
                return new Array(num);
            };

            $scope.saveUserComments = function () {
                var vendorRating = {};
                vendorRating.rId = 0;
                vendorRating.reqId = $scope.ratingRequirement;
                vendorRating.revieweeId = $scope.userId;
                vendorRating.rating = $scope.userRatingsJson;
                vendorRating.comments = $scope.review;
                vendorRating.keyClientTele = $scope.keyClientTele;
                vendorRating.supplyCapacity = $scope.supplyCapacity;
                vendorRating.priceTransparency = $scope.priceTransparency;
                vendorRating.safety = $scope.safety;
                vendorRating.ratingJSON = JSON.stringify($scope.userRatingsJson);
                userService.SaveVendorRating(vendorRating)
                    .then(function (response) {
                        if (response && response.errorMessage == '') {
                            $scope.isCommentPosted = true;
                            growlService.growl("Review Saved successfully.", "success");
                            location.reload();
                        } else {
                            growlService.growl("Review failed to save.", "inverse");
                        }
                    });
            };

            $scope.addnewdetailsobj = {
                alternateID: 0,
                altUserID: $scope.userId,
                userID: $scope.userId,
                firstName: '',
                lastName: '',
                department: '',
                designation: '',
                email: '',
                phoneNum: '',
                companyId: userService.getUserCompanyId(),
                companyName: $scope.userObj.companyName,
                isPrimary: 0,
                isValid: 1,
                sessionID: userService.getUserToken()
            }



            $scope.addNewDetails = function () {
                $scope.addnewdetailsobj.companyName = $scope.userObj.companyName;

                var params = {
                    "user": $scope.addnewdetailsobj
                };

                auctionsService.newDetails(params)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Data Saved successfully.");
                            this.addUser = 0;
                            $state.reload();
                        }
                    });

            };

            $scope.getAlternateContacts = function () {
                auctionsService.getalternatecontacts($scope.userId, userService.getUserCompanyId(), userService.getUserToken())
                    .then(function (response) {
                        $scope.alternateContacts = response;
                    });
            };
            if ($scope.callID == 0) {
                $scope.getAlternateContacts();
            }


            $scope.isDelete = false;

            $scope.editAltContacts = function (param, isValid) {

                //window.scrollTo(0, 250);

                var elmnt = document.getElementById("contactInfo");
                elmnt.scrollIntoView();


                $scope.isDelete = false;

                if (isValid == 1) {
                    $scope.isDelete = false;
                }
                else {
                    $scope.isDelete = true;
                }

                $scope.show = true;
                $scope.addnewdetailsobj.alternateID = param.alternateID;
                $scope.addnewdetailsobj.firstName = param.firstName;
                $scope.addnewdetailsobj.lastName = param.lastName;
                $scope.addnewdetailsobj.email = param.email;
                $scope.addnewdetailsobj.phoneNum = parseInt(param.phoneNum);
                $scope.addnewdetailsobj.department = param.department;
                $scope.addnewdetailsobj.designation = param.designation;
                $scope.addnewdetailsobj.isValid = isValid;
            }


            $scope.show = false;

            $scope.display = function (val) {

                $scope.addnewdetailsobj = {
                    alternateID: 0,
                    altUserID: $scope.userId,
                    userID: $scope.userId,
                    firstName: '',
                    lastName: '',
                    department: '',
                    designation: '',
                    lastName: '',
                    email: '',
                    phoneNum: '',
                    companyId: userService.getUserCompanyId(),
                    companyName: $scope.userObj.companyName,
                    isPrimary: 0,
                    isValid: 1,
                    sessionID: userService.getUserToken()
                }

                $scope.isDelete = false;
                $scope.show = val;
            }




            $scope.isGSTNumber = '';
            $scope.gstFile = 0;
            $scope.getUserCredentials = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getusercredentials?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.CredentialsResponce = response.data;
                    $scope.isGSTVerified = 0;
                    $scope.CredentialsResponce.forEach(function (item, index) {
                        if (item.fileType == 'STN') {
                            $scope.isGSTVerified = item.isVerified;
                            $scope.gstNumber = item.credentialID;
                            if (item.fileLink > 0) {
                                $scope.gstFile = item.fileLink;
                            }

                        }

                    })


                })
            };
            if ($scope.callID == 0) {
                $scope.getUserCredentials();
            }


            $scope.getuserinfo = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getuserinfo?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.userinfo = response.data;


                })
            };

            $scope.getuserinfo();

            $scope.onlineStatusObj = 'Not logged in';
            $scope.GetOnlineStatus = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getonlinestatus?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.onlineStatusObj = response.data;
                })
            };
            if ($scope.callID == 0) {
                $scope.GetOnlineStatus();
            }


            $scope.myAuctions = [];
            $scope.changeOnHover = true; // default test value
            $scope.maxValue = 5; // default test value
            $scope.userRatingsJson = {
                deliveryValue: 0,
                qualityValue: 0,
                emergencyValue: 0,
                serviceValue: 0,
                responseValue: 0,
                keyClienteleValue: 0,
                supplyCapacityValue: 0,
                priceTransparencyValue: 0,
                safetyValue: 0,
                commentsValue: 0
            }

            $scope.vendorProducts = [];
            $scope.GetVendorProducts = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getvendorproducts?sessionid=' + userService.getUserToken() + "&userid=" + $scope.userId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.vendorProducts = response.data;
                })
            };

            if ($scope.callID == 0) {
                $scope.GetVendorProducts();
            }

            $scope.vendorstatistics = function () {
                auctionsService.vendorstatistics($scope.userId, userService.getUserToken())
                    .then(function (response) {
                        $scope.vendorStatistics = response;
                    });
            }
            if ($scope.callID == 0) {
                $scope.vendorstatistics();
            }


            $scope.getactiveleads = function () {
                auctionsService.getactiveleads({ "userid": $scope.viewProfileUserId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myAuctions = response;
                    });
            }
            $scope.getactiveleads();

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };

            $scope.validateUserReview = function () {
                if ($scope.userRatingsJson.deliveryValue > 0 && $scope.userRatingsJson.qualityValue > 0 && $scope.userRatingsJson.emergencyValue > 0
                    && $scope.userRatingsJson.serviceValue > 0 && $scope.userRatingsJson.responseValue > 0
                    && $scope.userRatingsJson.keyClienteleValue > 0 && $scope.userRatingsJson.supplyCapacityValue > 0
                    && $scope.userRatingsJson.priceTransparencyValue > 0 && $scope.userRatingsJson.safetyValue > 0
                    /*&& $scope.userRatingsJson.commentsValue > 0*/
                    && $scope.review != '') {
                    $scope.isValidToSubmitReview = true;
                }
            };

            $scope.getGSTFilingDetails = function (gst) {
                $scope.vendorGstDetailsObj.isError = false;
                $scope.vendorGstDetailsObj.filing = null;
                userService.getGSTDetails({ "gstin": gst, "year": '2021-22', "sessionid": userService.getUserToken() })
                    .then(function (response) {

                        var vendorGSTFilingDetailsJson = response;//'{"error":false,"data":{"EFiledlist":[{"valid":"Y","mof":"ONLINE","dof":"21-12-2019","ret_prd":"112019","rtntype":"GSTR3B","arn":"AA3611192835414","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"10-12-2019","ret_prd":"102019","rtntype":"GSTR3B","arn":"AA361019397525T","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"09-12-2019","ret_prd":"092019","rtntype":"GSTR1","arn":"AA360919514416U","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"09-12-2019","ret_prd":"092019","rtntype":"GSTR3B","arn":"AA3609195140138","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"23-09-2019","ret_prd":"082019","rtntype":"GSTR3B","arn":"AA360819275807D","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"17-09-2019","ret_prd":"072019","rtntype":"GSTR3B","arn":"AA3607193931202","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"062019","rtntype":"GSTR1","arn":"AA360619397611P","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"042019","rtntype":"GSTR3B","arn":"AA360419423368X","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"052019","rtntype":"GSTR3B","arn":"AA360519397330X","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"27-07-2019","ret_prd":"062019","rtntype":"GSTR3B","arn":"AA360619397246K","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"25-05-2019","ret_prd":"032019","rtntype":"GSTR1","arn":"AA360319530586X","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"22-05-2019","ret_prd":"032019","rtntype":"GSTR3B","arn":"AA360319525963U","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"25-04-2019","ret_prd":"022019","rtntype":"GSTR3B","arn":"AA3602194018174","status":"Filed"},{"valid":"Y","mof":"ONLINE","dof":"22-04-2019","ret_prd":"012019","rtntype":"GSTR3B","arn":"AA360119415081L","status":"Filed"}]}}';
                        console.log(JSON.parse(vendorGSTFilingDetailsJson));
                        if (vendorGSTFilingDetailsJson) {
                            let tempObj = JSON.parse(vendorGSTFilingDetailsJson);
                            if (!tempObj.error) {
                                $scope.vendorGstDetailsObj.filing = tempObj.data.EFiledlist;
                            } else {
                                $scope.vendorGstDetailsObj.isError = true;
                                $scope.vendorGstDetailsObj.error = tempObj.data.error.message;
                            }
                        }
                    });
            };

            $scope.getGSTCompanyDetails = function (gst) {
                $scope.vendorGstDetailsObj.isError = false;
                $scope.vendorGstDetailsObj.general = null;
                userService.getGSTDetails({ "gstin": gst, "year": '', "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        var vendorGSTDetailsJson = response;//'{"error":false,"data":{"stjCd":"TG137","lgnm":"ACADS360 INDIA PRIVATE LIMITED","stj":"RAJENDRANAGAR - II","dty":"Regular","adadr":[],"cxdt":"","gstin":"36AANCA9052E1ZK","nba":["Service Provision"],"lstupdt":"16/09/2019","rgdt":"01/07/2017","ctb":"Private Limited Company","pradr":{"addr":{"bnm":"RADHAKRISHANA NAGAR","st":"HYDERGUDA","loc":"ATTAPUR","bno":"3-4-174/21/2","dst":"Hyderabad","stcd":"Telangana","city":"","flno":"","lt":"","pncd":"500048","lg":""},"ntr":"Service Provision"},"tradeNam":"M/S ACADS360 INDIA RIVATE LIMITED","sts":"Active","ctjCd":"YN0401","ctj":"ATTAPUR"}}';
                        console.log(JSON.parse(vendorGSTDetailsJson));
                        if (vendorGSTDetailsJson) {
                            let tempObj = JSON.parse(vendorGSTDetailsJson);
                            if (!tempObj.error) {
                                $scope.vendorGstDetailsObj.general = tempObj.data;
                            } else {
                                $scope.vendorGstDetailsObj.isError = true;
                                $scope.vendorGstDetailsObj.error = tempObj.message;
                            }
                        }
                    });
            };

            $scope.gstSelectInfoChange = function () {
                $scope.getGSTFilingDetails($scope.userObj.selectedGSTObj.gstNumber);
                $scope.getGSTCompanyDetails($scope.userObj.selectedGSTObj.gstNumber);
            };

            $scope.GetVendorAnalysis = function () {
                var params = {
                    "u_id": userService.getUserId(),
                    "vendID": $scope.userId,
                    "fromDate": $scope.dateobj.FromDate,
                    "toDate": $scope.dateobj.ToDate,
                    "callID": $scope.callID,
                    "sessionid": userService.getUserToken()
                };
                auctionsService.GetVendorAnalysis(params)
                    .then(function (response) {
                        $scope.vendorAnalysis = response;
                    });
            };
            if ($scope.callID == 0) {
                $scope.GetVendorAnalysis();
            }

            $scope.getAnalysis();

            //$scope.goTovendorAnalysys = function (prodId) {

            //    var url = $state.href('vendorViewProfile', { "Id": $scope.viewProfileUserId });
            //    $window.open(url, '_blank');
            //};

        }]);