﻿prmApp

    .controller('myApprovalsCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$window", "$log", "$stateParams", "$timeout", "auctionsService", "fwdauctionsService", "userService", "SignalRFactory", "fileReader", "growlService", "workflowService",
        function ($scope, $http, $state, domain, $filter, $window, $log, $stateParams, $timeout, auctionsService, fwdauctionsService, userService, SignalRFactory, fileReader, growlService, workflowService) {

            $scope.compID = userService.getUserCompanyId();
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();

            $scope.GetMyWorkflows = function () {
                workflowService.GetMyWorkflows($scope.userID, 'PENDING')
                    .then(function (response) {
                        $scope.listWorkflows = response;
                        console.log($scope.listWorkflows);
                    })
            }

            $scope.GetMyWorkflows();


            $scope.cancelClick = function () {
                $window.history.back();
            }

            $scope.goToCIJ = function (id) {
                var url = $state.href('cij', { "cijID": id });
                window.open(url, '_blank');
            }


            $scope.goToCIJView = function (id) {
                var url = $state.href('cijView', { "cijID": id });
                window.open(url, '_blank');
            }

            $scope.goToIndent = function (id) {
                var url = $state.href('materialsPurchaseIndent', { "indentID": id });
                window.open(url, '_blank');
            }

        }]);