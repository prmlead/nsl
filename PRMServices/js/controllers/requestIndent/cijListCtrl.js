﻿prmApp

    .controller('cijListCtrl', function ($scope, $http, $state, domain, $filter, $window, $log, $stateParams, $timeout, auctionsService, fwdauctionsService, userService,
        SignalRFactory, fileReader, growlService, workflowService, cijIndentService) {

        $scope.cijList = [];

        $scope.compID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();

        $scope.GetCIJList = function () {
            cijIndentService.GetCIJList(userService.getUserId(), $scope.sessionID)
                .then(function (response) {
                    $scope.cijList1 = [];
                    $scope.cijList1 = response;


                    $scope.cijList1.forEach(function (item, index) {
                        var cijObj = $.parseJSON(item.cij);
                        item.cij = cijObj;

                        if (item.cijType > 0) {

                            item.cijTypeName = "NEW";
                        }
                        else if (item.cijType == 0) {
                            item.cijTypeName = "REPLACEMENT";
                        }

                        if (item.cij.assetType > 0) {

                            item.cijassetTypeName = "Non-Medical";
                        }
                        else if (item.cij.assetType == 0) {
                            item.cijassetTypeName = "Medical";
                        }
                    });

                    $scope.cijList = $scope.cijList1;
                    $scope.tempCijLists = angular.copy($scope.cijList);
                    $scope.totalItems = $scope.cijList.length;

                })
        }

        $scope.cancelCIJ = function (cij, isValid) {
            cij.isValid = isValid;
            cij.sessionID = userService.getUserToken();
            cij.cij = JSON.stringify(cij.cij);
            var params = {
                stringcij: cij
            };

            cijIndentService.deleteCIJ(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("CIJ Saved Successfully.", "success");

                        for (var i = 0; i < $scope.cijList.length; i++) {
                            try {
                                if ($scope.cijList[i].cijCode == cij.cijCode) {
                                    $scope.cijList[i].cij = $.parseJSON($scope.cijList[i].cij);
                                    break;
                                }
                            }
                            catch (e) {

                                console.log('errror  ' + e);
                            }
                        }
                    }
                });
        }
        $scope.statusChange = function (assetType) {
            console.log(assetType);
            /* if (assetType != '') {
                 $scope.cijList = $filter('filter')($scope.cijList, { 'cij.assetType': assetType });
                 console.log($scope.cijList);
 
             }*/

        }

        $scope.GetCIJList();


        $scope.cancelClick = function () {
            $window.history.back();
        }

        $scope.goToCIJ = function (id) {
            var url = $state.href('cij', { "cijID": id });
            window.open(url, '_self');
        }


        $scope.goToCIJView = function (id) {
            var url = $state.href('cijView', { "cijID": id });
            window.open(url, '_self');
        }




        $scope.totalItems = 0;
        $scope.totalLeads = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 8;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
        };

        $scope.cijListFilter = function (filterVal) {
            $scope.filterArray = [];
            if (filterVal == 'ALL') {
                $scope.cijList = $scope.cijList1;

            } else {
                $scope.filterArray = $scope.cijList1.filter(function (item) {
                    return item.cij.assetType == filterVal;
                });
                $scope.cijList = $scope.filterArray;

            }

            $scope.totalItems = $scope.cijList.length;
        }



        $scope.GetItemsList = function (moduleID, module, index) {
            cijIndentService.GetItemsList(moduleID, module, $scope.sessionID)
                .then(function (response) {
                    $scope.ItemsList = response;
                    $scope.cijList[index].ItemsList = [];
                    $scope.cijList[index].ItemsList = $scope.ItemsList;
                })
        };



        $scope.SaveUserStatus = function (module, id, status) {

            var params = {
                module: module,
                id: id,
                status: status,
                userId: userService.getUserId(),
                sessionID: userService.getUserToken()
            }

            workflowService.SaveUserStatus(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                        
                    }

                })
        }

        $scope.searchTable = function (str) {

            $scope.cijList = $scope.cijList1.filter(function (req) {
                return (String(req.cijCode).includes(str) == true || String(req.cijTypeName).includes(str) == true || String(req.cijassetTypeName).includes(str) == true || String(req.cij.approximateCostRange).includes(str) == true || String(req.cij.proposedTotallInvestment.totalCost).includes(str) == true || String(req.requestedBy).includes(str) == true || String(req.status).includes(str) == true || String(req.currentApproverName).includes(str) == true);
            });

            $scope.totalItems = $scope.cijList.length;
        }

    });