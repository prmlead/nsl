﻿prmApp
    .controller('pendingAsnCtrl', function ($scope, $state, $stateParams, userService, growlService,
        workflowService,
        auctionsService, fileReader, $log, $window, poService) {
        $scope.sessionid = userService.getUserToken();    
        $scope.MyPendingPOS = [];
        $scope.MyPendingPOS1 = [];
        $scope.MyPendingPOS2 = [];

        $scope.MyPendingASNS = [];
        $scope.MyPendingASNS1 = [];

        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.asnCount = 0;

        $scope.filters = {
            categoryStatus: '',
            departmentStatus: '',
            productName: '',
            supplierName: '',
            dispatchCode: '',
            ASNdate: '',
            searchKeyword:''
        };

        $scope.compID = userService.getUserCompanyId();
        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5; //Number of pager buttons to show
        
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };


        $scope.totalItems1 = 0;
        $scope.currentPage1 = 1;
        $scope.itemsPerPage1 = 10;
        $scope.maxSize1 = 5; //Number of pager buttons to show

        $scope.setPage1 = function (pageNo) {
            $scope.currentPage1 = pageNo;
        };

        $scope.pageChanged1 = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };


    /*PAGINATION CODE*/

        $scope.supplierArr = [];
        $scope.itemArr = [];
        $scope.deptArr = [];
        $scope.catArr = [];
        $scope.dispatchArr = [];

       



        var today = moment();
        $scope.dashboardFromDate = today.add('days', -30).format('YYYY-MM-DD');
        today = moment().format('YYYY-MM-DD');
        $scope.dashboardToDate = today;

        $scope.filterStatusArr = [];

        $scope.getMiniItems = function () {
            var params = {
                userid: userService.getUserId(),
                sessionid: userService.getUserToken(),
                fromDate: $scope.dashboardFromDate,
                toDate: $scope.dashboardToDate,
                filterDepts: ''
            }


            auctionsService.getDashboardStats(params)
                .then(function (response) {
                    $scope.dashboardStats = response;

                    if (response && response.asnList && response.asnList.length > 2) {

                        $scope.asnCount = response.asnList.length;

                        response.asnList.forEach(function (asn, index) {
                            //asn.dispatchDate = new moment(asn.dispatchDate).format("DD-MM-YYYY");
                            //asn.poCreatedDate = new moment(asn.poCreatedDate).format("DD-MM-YYYY");
                            //asn.poDeliveryDate = new moment(asn.poDeliveryDate).format("DD-MM-YYYY");
                            //asn.expectedDeliveryDate = new moment(asn.expectedDeliveryDate).format("DD-MM-YYYY");

                            asn.dispatchDate = userService.toLocalDateOnly(asn.dispatchDate);
                            asn.poCreatedDate = userService.toLocalDateOnly(asn.poCreatedDate);
                            asn.poDeliveryDate = userService.toLocalDateOnly(asn.poDeliveryDate);
                            asn.expectedDeliveryDate = userService.toLocalDateOnly(asn.expectedDeliveryDate);

                            if (String(asn.dispatchDate).includes('9999')) {
                                asn.dispatchDate = '';
                            }
                            if (String(asn.poCreatedDate).includes('9999')) {
                                asn.poCreatedDate = '';
                            }
                            if (String(asn.poDeliveryDate).includes('9999')) {
                                asn.poDeliveryDate = '';
                            }
                            if (String(asn.expectedDeliveryDate).includes('9999')) {
                                asn.expectedDeliveryDate = '';
                            }

                           

                            if ($scope.supplierArr.indexOf(asn.supplierName) == -1) {
                                $scope.supplierArr.push(asn.supplierName);

                            }

                            if ($scope.dispatchArr.indexOf(asn.dispatchCode) == -1) {
                                $scope.dispatchArr.push(asn.dispatchCode);

                            }
                            var itemArr = asn.itemName.split(',');
                            itemArr.forEach(function (item) {
                                if ($scope.itemArr.indexOf(item) == -1) {
                                    $scope.itemArr.push(item);
                                }
                            })

                            var catArr = asn.category.split(',');
                            catArr.forEach(function (item) {
                                if ($scope.catArr.indexOf(item) == -1) {
                                    $scope.catArr.push(item);
                                }
                            })

                            var deptArr = asn.department.split(',');
                            deptArr.forEach(function (dept) {
                                if ($scope.deptArr.indexOf(dept) == -1) {
                                    $scope.deptArr.push(dept);
                                }
                            })
                           
                        });

                        $scope.MyPendingASNS = response.asnList;
                        if ($state.current.name === 'matchPrices') {
                            $scope.threeWayMatchArray = {
                                poItemsEntity: [],
                                Temp: []
                            };
                            $scope.MyPendingASNS.forEach(function (asnItem, asnIndex) {
                                $scope.displayThreeWayMatch(asnItem);
                            });
                        } else {
                        $scope.MyPendingASNS1 = $scope.MyPendingASNS;

                        $scope.totalItems = response.asnList.length;
                        }

                    }
                    

                });
        };

        $scope.status = '';

        $scope.setFilters1 = function () {
            var filterArray = [];
            $scope.threeWayMatchArray.poItemsEntity = $scope.threeWayMatchArray.Temp;
            if ($scope.status) {

                filterArray = $scope.threeWayMatchArray.poItemsEntity.filter(function (item) {
                    return $scope.status === 'filtered' ? item.displayColor === 'orangered' : item;
                });
                $scope.threeWayMatchArray.poItemsEntity = filterArray;

            }

            if ($scope.postedFromDate) {
                filterArray = $scope.threeWayMatchArray.poItemsEntity.filter(function (item) {
                    return ((item.asnDate.split(' ')[0]) >= ($scope.postedFromDate))

                });
                $scope.threeWayMatchArray.poItemsEntity = filterArray;
            }
            if ($scope.postedToDate) {
                filterArray = $scope.threeWayMatchArray.poItemsEntity.filter(function (item) {
                    return ((item.asnDate.split(' ')[0]) <= ($scope.postedToDate))

                });
                $scope.threeWayMatchArray.poItemsEntity = filterArray;
            }
            $scope.totalMatch = $scope.threeWayMatchArray.poItemsEntity.length;

        };

        $scope.GetDateconverted = function (dateBefore) {
            if (dateBefore) {
                return userService.toLocalDate(dateBefore);
            }
        };

        $scope.failureCount = 0;
        $scope.successCount = 0;
        $scope.totalMatch = 0;


        $scope.totalMatch = 0;
        $scope.currentPageMatch = 1;
        $scope.itemsPerPageMatch = 10;
        $scope.maxSizeMatch = 10;

        $scope.pageChangedMatch = function () {
        };

        $scope.displayThreeWayMatch = function (asnItem) {
            if (asnItem.dispatchCode) {
                poService.getDispatchTrack(asnItem.poOrderId, asnItem.dispatchCode)
                    .then(function (response) {
                        if (response && response.length > 0) {

                            response.forEach(function (item, index) {
                                if (item && item.poItemsEntity.length > 0) {
                                    item.poItemsEntity.forEach(function (poItemEntity, poItemEntityIndex) {
                                        poItemEntity.asnCode = asnItem.dispatchCode;
                                        poItemEntity.asnDate = asnItem.dispatchDate;
                                        poItemEntity.poCreatedDate = asnItem.poCreatedDate;
                                        if (poItemEntity.dispatchQuantity > 0) {
                                            if (poItemEntity.vendorPOQuantity && poItemEntity.vendorPOQuantity && poItemEntity.dispatchQuantity && poItemEntity.dispatchQuantity && poItemEntity.sumRecivedQuantity && poItemEntity.sumRecivedQuantity > 0) {

                                                if (poItemEntity.vendorPOQuantity !== poItemEntity.dispatchQuantity || poItemEntity.vendorPOQuantity !== poItemEntity.sumRecivedQuantity) {
                                                    //if (item.vendorPOQuantity != item.dispatchQuantity != item.sumRecivedQuantity) {
                                                    poItemEntity.displayColor = 'orangered';
                                                    $scope.failureCount += 1;
                                                } else {
                                                    $scope.successCount += 1;
                                                }
                                            }
                                            $scope.threeWayMatchArray.poItemsEntity.push(poItemEntity);
                                            $scope.threeWayMatchArray.Temp.push(poItemEntity);
                                            $scope.totalMatch = $scope.threeWayMatchArray.poItemsEntity.length
                                        }
                                    });

                                }

                            });
                        }
                        $scope.threeWayMatchArray.poItemsEntity.forEach(function (item, index) {
                            

                            
                        });
                        //$scope.threeWayMatchArray.push(asnItem.dispatchTable.poItemsEntity);

                    });

            }

        };



        $scope.purchaseOrders = [];
        $scope.poNumbers = [];
        $scope.deliverySchedulePoNumbers = [];
        $scope.deliverySchedulesStr = "";
        $scope.deliverySchedulesStrTemp = "";


        $scope.totalPurchaseOrdersCount = 0;
        $scope.totaldeliverySchedulesCount = 0;
        $scope.getPurchaseOrders = function () {
            poService.getAssignedPO()
                .then(function (response) {
                    if (response && response.length > 0) {
                        $scope.purchaseOrders = response;
                        $scope.totalPurchaseOrdersCount = $scope.purchaseOrders[0].TOTAL_COUNT;

                        $scope.purchaseOrders.forEach(function (item, index) {
                            $scope.poNumbers.push(item.PURCHASE_ORDER_ID);
                        });

                        var mySet = new Set($scope.poNumbers)
                        $scope.poNumbers = Array.from(mySet)


                        $scope.deliverySchedulesStr = "";
                        $scope.deliverySchedulesStrTemp = "";

                        $scope.deliverySchedulesStrTemp = $scope.poNumbers;
                        $scope.deliverySchedulesStr = $scope.deliverySchedulesStrTemp.join(',');
                        if ($scope.deliverySchedulesStr && $scope.deliverySchedulesStr != null) {
                            $scope.getDeliverySchedules($scope.deliverySchedulesStr);
                        }
                    }
                });
        };

        $scope.getDeliverySchedules = function (poNumbers) {
            poService.getDeliverySchedules(poNumbers)
                .then(function (response) {
                    $scope.deliverySchedules = response;

                    $scope.deliverySchedules.forEach(function (item, index) {
                        item.DELIVERY_DATE = userService.toLocalDate(item.DELIVERY_DATE);
                    });
                   //$scope.totaldeliverySchedulesCount = $scope.deliverySchedules[0].TOTAL_COUNT;

                    $scope.totalItems1 = $scope.deliverySchedules.length;
                   
                });
        };


        if (!$scope.isVendor) {
            $scope.getMiniItems();
        } else {
            $scope.getPurchaseOrders();
        }
        
        $scope.goToPO = function (reqID,poID,vendorID) {
            var url = $state.href("po-list", { "reqID": reqID, "vendorID": vendorID, "poID": 0 });
            window.open(url, '_blank');
        }
        $scope.goToDispatchTrackForm = function (reqID, purchaseID, dispatchCode) {
            var url = $state.href("dtform", { reqID: reqID, "poOrderId": purchaseID, "dCode": dispatchCode });
            window.open(url, '_blank');
        };

       

        $scope.setFilters = function () {
            console.log($scope.MyPendingASNS1.length)
            $scope.filterArray = [];
            $scope.MyPendingASNS = $scope.MyPendingASNS1;
            if ($scope.filters.searchKeyword) {
                $scope.filterArray = $scope.MyPendingASNS.filter(function (item) {
                    return (item.itemName.toLowerCase().indexOf($scope.filters.searchKeyword.toLowerCase()) >= 0 ||
                        item.poOrderId.toLowerCase().indexOf($scope.filters.searchKeyword.toLowerCase()) >= 0)
                });
                $scope.MyPendingASNS = $scope.filterArray;
            }
            if ($scope.filters.departmentStatus) {
                console.log($scope.filters.departmentStatus);

                $scope.filterArray = $scope.MyPendingASNS.filter(function (item) {
                    return item.department.split(',').indexOf($scope.filters.departmentStatus) >= 0;
                });
                $scope.MyPendingASNS = $scope.filterArray;



            }
            if ($scope.filters.categoryStatus) {
                console.log($scope.filters.categoryStatus);
                $scope.filterArray = $scope.MyPendingASNS.filter(function (item) {
                    return item.category.split(',').indexOf($scope.filters.categoryStatus) >= 0;;
                });
                $scope.MyPendingASNS = $scope.filterArray;

            }
            if ($scope.filters.productName) {
                $scope.filterArray = $scope.MyPendingASNS.filter(function (item) {
                    return item.itemName.split(',').indexOf($scope.filters.productName) >= 0;
                });
                $scope.MyPendingASNS = $scope.filterArray;
            }

            if ($scope.filters.supplierName) {
                console.log($scope.filters.supplierName)
                $scope.filterArray = $scope.MyPendingASNS.filter(function (item) {
                    return item.supplierName == $scope.filters.supplierName;
                });
                console.log($scope.filterArray.length)
                $scope.MyPendingASNS = $scope.filterArray;
            }
           
            if ($scope.filters.ASNdate) {
                $scope.filterArray = $scope.MyPendingASNS.filter(function (item) {
                    
                    return item.expectedDeliveryDate == $scope.filters.ASNdate;
                });
                $scope.MyPendingASNS = $scope.filterArray;
            }
            if ($scope.filters.dispatchCode) {

                $scope.filterArray = $scope.MyPendingASNS.filter(function (item) {
                    return item.dispatchCode == $scope.filters.dispatchCode;
                });

                $scope.MyPendingASNS = $scope.filterArray;
            }

           

        }

    });