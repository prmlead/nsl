﻿prmApp
    .controller('pendingPoCtrl', function ($scope, $state, $stateParams, userService, growlService,
        workflowService,
        auctionsService, fileReader, $log, $window,poService) {
        
        $scope.MyPendingPOS = [];
        $scope.MyPendingPOS1 = [];
        $scope.MyPendingPOS2 = [];

        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;

        $scope.filters = {
            categoryStatus: '',
            departmentStatus: '',
            productName: '',
            supplierName: '',
            expDeliveryDate: '',
            searchKeyword:''


        };
        

        $scope.compID = userService.getUserCompanyId();
        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5; //Number of pager buttons to show
        
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
        /*PAGINATION CODE*/
        $scope.supplierArr = [];
        $scope.itemArr = [];
        $scope.DeptCategories = [];
        $scope.catArr = [];
        $scope.deptArr = [];

        //$scope.GetFiltersOnLoadData = function () {
        //    poService.GetFiltersOnLoadData()
        //        .then(function (response) {
        //            $scope.DeptCategories = response;

        //            //$scope.catArr = $scope.catArr.concat(response.filter(function (item) { return item.type === 'CATEGORY' }));
        //            //$scope.deptArr = $scope.deptArr.concat(response.filter(function (item) { return item.type === 'DEPARTMENT' }));

        //            $scope.DeptCategories.forEach(function (deptCatItem, deptCatIndex) {
        //                if (deptCatItem.type === 'CATEGORY') {
        //                    $scope.catArr.push(deptCatItem);
        //                } else if (deptCatItem.type === 'DEPARTMENT') {
        //                    $scope.deptArr.push(deptCatItem);
        //                }
        //            });
        //            $scope.filters.categoryStatus = '';
        //            $scope.filters.departmentStatus = '';

        //            $scope.GetMyPendingApprovals();

        //        });
        //};





        $scope.GetMyPendingApprovals = function () {
            poService.GetMyPendingPOS($scope.isVendor)
                .then(function (response) {
                    $scope.allPOs = [];
                    $scope.pendingPOs = [];
                    $scope.posNotInitiated = 0;
                    $scope.MyPendingPOS = response;
                    $scope.partialDeliverables = 0;
                    $scope.awaitingReceipt = 0;

                    $scope.MyPendingPOS.forEach(function (item, index) {

                        if ($scope.supplierArr.indexOf(item.SUPPLIER_NAME) == -1) {
                            $scope.supplierArr.push(item.SUPPLIER_NAME);

                        }
                        if (item.ITEMS) {
                            var itemArr = item.ITEMS.split(',');
                            itemArr.forEach(function (item) {
                                if ($scope.itemArr.indexOf(item) == -1) {
                                    $scope.itemArr.push(item);
                                }
                            })
                        }
                        
                        if (item.CATEGORIES) {
                            var catArr = item.CATEGORIES ? item.CATEGORIES.split(',') : [];
                            catArr.forEach(function (item) {
                                if ($scope.catArr.indexOf(item) == -1) {
                                    $scope.catArr.push(item);
                                }
                            })
                        }

                        if (item.DEPARTMENTS) {
                            var deptArr = item.DEPARTMENTS ? item.DEPARTMENTS.split(',') : [];
                            deptArr.forEach(function (dept) {
                                if ($scope.deptArr.indexOf(dept) == -1) {
                                    $scope.deptArr.push(dept);
                                }
                            })
                        }
                       

                        if ($scope.allPOs.indexOf(item.PURCHASE_ORDER_ID) == -1) {
                            if (item.PO_IS_SEND_TO_VENDOR == 0) {
                                $scope.posNotInitiated += 1;
                            }
                            $scope.allPOs.push(item.PURCHASE_ORDER_ID);
                        }

                        if ($scope.pendingPOs.indexOf(item.PURCHASE_ORDER_ID) == -1 && item.PO_STATUS == 'PENDING') {
                            $scope.pendingPOs.push(item.PURCHASE_ORDER_ID);
                        }

                        if (item.DELIVERY_STATUS == "PARTIAL") {
                            $scope.partialDeliverables += 1;
                        }

                        if (item.AWAITING_RECEIPT > 0) {
                            $scope.awaitingReceipt += 1;
                        }

                        item.EXPECTED_DELIVERY_DATE = userService.toLocalDateOnly(item.EXPECTED_DELIVERY_DATE);
                        item.CREATED = userService.toLocalDate(item.CREATED);
                       
                        var dateArr = item.DELIVERY_DATE ? item.DELIVERY_DATE.split(',') : [];
                        var dateArr1 = [];
                        dateArr.forEach(function (item) {
                            if (item) {
                                dateArr1.push(userService.toLocalDate(item))
                            }
                        })
                        item.DELIVERY_DATE = dateArr1.toString()
                        
                        

                       

                    })

                    $scope.MyPendingPOS = $scope.MyPendingPOS.filter(function (item) {
                        return item.PO_STATUS == 'PENDING';
                    })
                    $scope.MyPendingPOS1 = response.filter(function (item) {
                        return item.PO_STATUS == 'PENDING';
                    });
                   
                    $scope.MyPendingPOS2 = response.filter(function (item) {
                        return item.PO_STATUS == 'PENDING';
                    });

                    
                    /*PAGINATION CODE START*/
                    $scope.totalItems = $scope.MyPendingPOS.length;
                    /*PAGINATION CODE END*/
                });
        };

        $scope.GetMyPendingApprovals();
       // $scope.GetFiltersOnLoadData()
        
        $scope.goToPO = function (reqID,poID,vendorID) {
            var url = $state.href("po-list", { "reqID": reqID, "vendorID": vendorID, "poID": poID });
            window.open(url, '_blank');
        }

        $scope.setFilters = function () {
            console.log(1)
            $scope.filterArray = [];
            $scope.MyPendingPOS = $scope.MyPendingPOS1;
            if ($scope.filters.searchKeyword) {
                $scope.filterArray = $scope.MyPendingPOS.filter(function (item) {
                    return (item.ITEMS.toLowerCase().indexOf($scope.filters.searchKeyword.toLowerCase()) >= 0 ||
                        item.PURCHASE_ORDER_ID.toLowerCase().indexOf($scope.filters.searchKeyword.toLowerCase()) >= 0 )
                });
                $scope.MyPendingPOS = $scope.filterArray;
            }
            if ($scope.filters.departmentStatus) {
                console.log($scope.filters.departmentStatus);

                $scope.filterArray = $scope.MyPendingPOS.filter(function (item) {
                    return item.DEPARTMENTS.split(',').indexOf($scope.filters.departmentStatus) >= 0;
                });
                $scope.MyPendingPOS = $scope.filterArray;



            }
            if ($scope.filters.categoryStatus) {
                console.log($scope.filters.categoryStatus);
                $scope.filterArray = $scope.MyPendingPOS.filter(function (item) {
                    return item.CATEGORIES.split(',').indexOf($scope.filters.categoryStatus) >= 0; ;
                });
                $scope.MyPendingPOS = $scope.filterArray;

            }
            if ($scope.filters.productName) {
                $scope.filterArray = $scope.MyPendingPOS.filter(function (item) {
                    return item.ITEMS.split(',').indexOf($scope.filters.productName) >= 0;
                });
                $scope.MyPendingPOS = $scope.filterArray;
            }

            if ($scope.filters.supplierName) {
                $scope.filterArray = $scope.MyPendingPOS.filter(function (item) {
                    return item.SUPPLIER_NAME == $scope.filters.supplierName;
                });
                $scope.MyPendingPOS = $scope.filterArray;
            }
            if ($scope.filters.expDeliveryDate) {
                $scope.filterArray = $scope.MyPendingPOS.filter(function (item) {

                    return item.EXPECTED_DELIVERY_DATE == $scope.filters.expDeliveryDate;
                });
                $scope.MyPendingPOS = $scope.filterArray;
            }
           
            

        }

    });