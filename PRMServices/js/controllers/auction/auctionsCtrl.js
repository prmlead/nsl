prmApp
    // =========================================================================
    // Auction Tiles
    // =========================================================================
    .controller('auctionsCtrl', ["$timeout", "$state", "$scope", "$log", "growlService", "userService", "auctionsService", "$http", "$rootScope",
        "SignalRFactory", "signalRHubName", "logisticServices", "$filter", "store", "poService", "workflowService",
        function ($timeout, $state, $scope, $log, growlService, userService, auctionsService, $http, $rootScope, $filter, SignalRFactory, signalRHubName, logisticServices, store, poService,
            workflowService) {
            $scope.myHotAuctionsLoaded = false;
            $scope.sessionid = userService.getUserToken();
            $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
            $scope.totalPendingApprovals = 0;

            //#region ListUserDepartmentDesignations
            if (!$scope.isVendor) {
                $scope.SelectedDeptId = 0;
                $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
                if (userService.getSelectedUserDepartmentDesignation()) {
                    $scope.SelectedUserDepartmentDesignation = userService.getSelectedUserDepartmentDesignation();
                }
                $scope.SelectedDeptId = $scope.SelectedUserDepartmentDesignation.deptID;
            }

            $scope.changeUserDepartmentDesignation = function () {
                $scope.SelectedUserDepartmentDesignation = $scope.ListUserDepartmentDesignations.filter(function (udd) {
                    return (udd.deptID == $scope.SelectedDeptId);
                });

                $scope.SelectedUserDepartmentDesignation = $scope.SelectedUserDepartmentDesignation[0];
                store.set('SelectedUserDepartmentDesignation', $scope.SelectedUserDepartmentDesignation);

                console.log('SelecteduserDepartmentDesignations-------------------------->START');
                console.log(userService.getSelectedUserDepartmentDesignation().deptID);
                console.log('SelecteduserDepartmentDesignations-------------------------->END');
                location.reload();
            };
            //#endregion ListUserDepartmentDesignations

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            $scope.compID = userService.getUserCompanyId();
            // console.log('------------------------------------------$scope.compID');
            //console.log($scope.compID);
            //console.log('------------------------------------------$scope.compID');
            //if ($scope.compID == 0) {
            //    location.reload();
            //}

            console.log('-----------------------------------------------$scope.compID');
            console.log($scope.compID);
            console.log('$scope.compID-----------------------------------------------');

            /*$scope.myAuctionsLoaded = false;*/
            $scope.todaysAuctionsLoaded = false;
            $scope.myHotAuctionsLoaded = false;
            $scope.scheduledAuctionsLoaded = false;
            /*$scope.myAuctions = [];*/
            /*$scope.myAuctions = [];*/
            $scope.todaysAuctionsLoaded = false;
            $scope.scheduledAuctionsLoaded = false;
            $scope.hotLimit = 4;
            $scope.todaysLimit = 4;
            $scope.scheduledLimit = 4;
            $scope.loggedIn = userService.isLoggedIn();
            $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
            $log.info(userService.getUserType());
            $scope.scheduledAuctionsMessage = $scope.todayAuctionsMessage = $scope.hotAuctionsMessage = "Loading data, please wait.";

            $scope.urgencyChartShow = false;
            $scope.categoryChartShow = false;
            $scope.quotationRangeChartShow = false;
            $scope.monthlyDetailsChartShow = false;
            $scope.requirementDetailsChartShow = false;
            $scope.transactionDetailsChartShow = false;

            $scope.reqUrgency = {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    //width: 470,
                    //height: 300
                    width: 300,
                    height: 300
                },
                title: {
                    text: 'Top 5 Savings Trend'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        size: 175,
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}',
                            style: {
                                color: 'black'
                            }
                        }
                        // Clickable dashboard //
                        ,
                        events: {
                            click: function (event) {
                                if (!$scope.isVendor) {
                                    $scope.goToRequirements('UNCONFIRMED', event.point.name);
                                }
                            }
                        }
                        // Clickable dashboard //
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: []
                }]
            }

            $scope.reqCategory = {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    width: 470,
                    height: 300
                },
                title: {
                    text: 'Pending Requirements by Category'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}',
                            style: {
                                color: 'black'
                            }
                        }
                    }
                    // Clickable dashboard //
                    ,
                    events: {
                        click: function (event) {
                            $scope.goToRequirements('UNCONFIRMED', event.point.name);
                        }
                    }
                    // Clickable dashboard //
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: []
                }]
            };

            $scope.quotationRange = {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    //width: 470,
                    //height: 300
                    width: 300,
                    height: 300
                },
                title: {
                    text: 'Top 5 Category Spend'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        size: 175,
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}',
                            style: {
                                color: 'black'
                            }
                        }
                        // Clickable dashboard //
                        ,
                        events: {
                            click: function (event) {
                                if (!$scope.isVendor) {
                                    $scope.goToRequirements('UNCONFIRMED', event.point.name);
                                }
                            }
                        }
                        // Clickable dashboard //
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: []
                }]
            }

            $scope.monthlyDetails = {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'line',
                    width: 450,

                    height: 300
                },
                title: {
                    text: 'Monthly Average Savings'
                },
                subtitle: {
                    text: 'Source: Requirements'
                },
                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: 'Price (INR)'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Requirement Value',
                    data: []
                }, {
                    name: 'Savings Achieved',
                    data: []
                }]
            };;

            $scope.requirementDetails = {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'column',
                    width: 550,

                    height: 290

                },
                title: {
                    text: 'Monthly Average Utilization'
                },
                subtitle: {
                    text: 'Source: Requirements'
                },
                xAxis: {

                    categories: []

                },
                yAxis: {

                    title: {
                        text: 'Requriments'
                    }

                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: '',
                    data: []
                }, {
                        name: '',
                        data: []
                    }
                ]
            };

            $scope.transactionDetails = {
                credits: {
                    enabled: false
                },
                chart: {
                    type: 'areaspline',
                    width: 450,

                    height: 350
                },
                title: {
                    text: ' My Transactional Trend'
                },
                subtitle: {
                    text: 'Source: My Transactional Trend Vs Organizational Trend'
                },
                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: 'RFQ counts'
                    }

                },

                plotOptions: {
                    areaspline: {
                        lineWidth: 5,
                        states: {
                            hover: {
                                lineWidth: 3
                            }
                        },
                        marker: {
                            enabled: false,
                            states: {
                                hover: {
                                    enabled: true
                                }
                            }
                        },
                    }
                },
                series: [{
                    name: 'Organizational Trend',
                    data: []
                }, {
                    name: 'My Transactional Trend',
                    data: []
                }],
                navigation: {
                    menuItemStyle: {
                        fontSize: '10px'
                    }
                }
            };



            $scope.reduceTime = function (timerId, time) {
                addCDSeconds(timerId, time);
            };

            $scope.stopBid = function (item) {
                $scope.myHotAuctions[0].TimeLeft = 60;
            };

            $scope.options = {
                loop: true,
                dots: false,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
                responsive: {
                    0: {
                        items: 1,
                    },
                    600: {
                        items: 3,
                    },
                    1000: {
                        items: 4,
                        loop: false
                    }
                }
            };
            $scope.data = {
                userID: 1,
                sessionID: 1,
                section: "CURRENT"
            };

            $scope.myHotAuctions = [];
            $scope.scheduledAuctions = [];
            $scope.todaysAuctions = [];
            $log.info($scope.loggedIn);



            $scope.currentUserObj = userService.getUserObj();
            $log.info($scope.currentUserObj);

            $scope.totalAuctionsPendingPercentage = 0;
            $scope.totalAuctionsCompletedPercentage = 0;
            $scope.totalTurnaroundTime = 0;
            $scope.totalTurnaroundTimeArray = [];
            $scope.liveAuctionsTableLimit = 50;

            $scope.totalSavedAmount = 0;
            $scope.totalSavedPercentage = 0;
            $scope.totalTurnover = 0;

            $scope.D = 0;
            $scope.H = 0;
            $scope.M = 0;

            var today = moment();
            $scope.dashboardFromDate = today.add('days', -30).format('YYYY-MM-DD');
            today = moment().format('YYYY-MM-DD');
            $scope.dashboardToDate = today;

            $scope.userDepartments = [];

            $scope.loadUserDepartments = function () {
                auctionsService.GetUserDepartments(userService.getUserId(), userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.userDepartments = response;
                            $log.info('$scope.userDepartments');
                            $log.info($scope.userDepartments);
                        }

                    });
            };

            $scope.getMiniItems = function () {
                if ($scope.loggedIn) {

                    $scope.loadUserDepartments();
                    var deptId = store.get('SelectedUserDepartmentDesignation') && store.get('SelectedUserDepartmentDesignation').deptID ? store.get('SelectedUserDepartmentDesignation').deptID : 0;
                    var params = {
                        userid: userService.getUserId(),
                        sessionid: userService.getUserToken(),
                        fromDate: $scope.dashboardFromDate,
                        toDate: $scope.dashboardToDate,
                        filterDepts: deptId
                    };

                    auctionsService.getDashboardStatsNew(params)
                        .then(function (response) {
                            $scope.dashboardStats = response;
                            var factor = Math.pow(10, 2);

                            $scope.totalSavedAmount = $scope.dashboardStats.totalSaved;
                            $scope.totalSavedAmount = (Math.round($scope.totalSavedAmount * factor) / factor);


                            $scope.totalTurnover = $scope.dashboardStats.totalTurnover;
                            $scope.totalTurnover = Math.round($scope.totalTurnover * factor) / factor;
                           
                            $scope.totalSavedPercentage = (($scope.dashboardStats.totalSaved / $scope.dashboardStats.totalTurnover) * 100);
                            $scope.totalSavedPercentage = Math.round($scope.totalSavedPercentage * factor) / factor;

                            $scope.totalOpenRFQsPercentage = parseFloat(($scope.dashboardStats.TOTAL_OPEN_RFQS / $scope.dashboardStats.TOTAL_RFQS) * 100).toFixed(2);
                            $scope.totalTurnaroundTime = parseFloat($scope.dashboardStats.totalTurnaroundTime / 60).toFixed(2);

                            $scope.D = parseInt($scope.dashboardStats.totalTurnaroundTime / (24 * 60));
                            $scope.H = parseInt(($scope.dashboardStats.totalTurnaroundTime % (24 * 60)) / 60);
                            $scope.M = parseInt(($scope.dashboardStats.totalTurnaroundTime % (24 * 60)) % 60);

                            $scope.totalTurnaroundTimeArray = $scope.totalTurnaroundTime.toString().split('.');

                            $scope.totalTurnaroundTimePercentage = parseFloat((($scope.dashboardStats.totalTurnaroundTime / (60 * 24)) / $scope.dashboardStats.totalAuctionsCompleted) * 100).toFixed(2);

                            let data = [];
                            if (response && response.stats.length > 0) {
                                response.stats[0].arrayPair.forEach(function (keyVal, index) {
                                    let statObj = {};
                                    statObj.name = keyVal.key1.split(' (')[0];
                                    statObj.y = keyVal.value1;
                                    if (keyVal.value1 > 0) {
                                        $scope.urgencyChartShow = true;
                                    }
                                    data.push(statObj);
                                });
                            }
                            $scope.reqUrgency.series[0].data = data;

                            data = [];
                            if (response && response.stats.length > 3) {
                                response.stats[3].arrayPair.forEach(function (keyVal, index) {
                                    var statObj = {};
                                    statObj.name = keyVal.key1;
                                    statObj.y = keyVal.value1;
                                    data.push(statObj);
                                });
                            }

                            $scope.reqCategory.series[0].data = data;
                            if (data.length > 0) {
                                $scope.categoryChartShow = true;
                            }


                            data = [];
                            if (response && response.stats.length > 1) {
                                response.stats[1].arrayPair.forEach(function (keyVal, index) {
                                    var statObj = {};
                                    statObj.name = keyVal.key1;
                                    statObj.y = keyVal.value1;
                                    data.push(statObj);
                                });
                            }

                            $scope.quotationRange.series[0].data = data;
                            if (data.length > 0) {
                                $scope.quotationRangeChartShow = true;
                            }
                            if (response && response.stats.length > 2) {
                                console.log(response.stats)
                                response.stats[2].arrayPair.forEach(function (keyVal, index) {
                                    //$scope.monthlyDetails.series[0].data.push($filter('numToCurrency')(keyVal.decimalVal, { currentCurrency: 'INR' }));

                                    $scope.monthlyDetails.xAxis.categories.push(keyVal.key1);

                                    $scope.monthlyDetails.series[0].data.push(keyVal.decimalVal);
                                    $scope.monthlyDetails.series[1].data.push(keyVal.decimalVal1);
                                });
                            }
                            if (data.length > 0) {
                                $scope.monthlyDetailsChartShow = true;
                            }

                            if (response && response.asnList && response.asnList.length > 0) {
                                response.asnList.forEach(function (asn, index) {
                                    asn.dispatchDate = new moment(asn.dispatchDate).format("DD-MM-YYYY");
                                });
                            }
                        });

                    //auctionsService.getauctions({ "section": "CURRENT", "userid": userService.getUserId(), "sessionid": userService.getUserToken(), "limit": $scope.todaysLimit })
                    //    .then(function (response) {
                    //        $scope.myHotAuctions = response;
                    //        $scope.myHotAuctionsLoaded = true;
                    //        if ($scope.myHotAuctions.length > 0) {
                    //            $scope.myHotAuctionsLoaded = true;
                    //            //$(".todayauctions").owlCarousel(options);
                    //        } else {
                    //            $scope.myHotAuctionsLoaded = false;
                    //            $scope.myHotAuctionsMessage = "There are no Negotiations currently running for you.";
                    //        }
                    //    });

                    //auctionsService.getauctions({ "section": "TODAYS", "userid": userService.getUserId(), "sessionid": userService.getUserToken(), "limit": $scope.todaysLimit })
                    //    .then(function (response) {
                    //        $scope.todaysAuctions = response;
                    //        $scope.todaysAuctionsLoaded = true;
                    //        if ($scope.todaysAuctions.length > 0) {
                    //            $scope.todaysAuctionsLoaded = true;
                    //            //$(".todayauctions").owlCarousel(options);
                    //        } else {
                    //            $scope.todaysAuctionsLoaded = false;
                    //            $scope.todayAuctionsMessage = "There are no Negotiations scheduled today for you.";
                    //        }
                    //    });
                    //auctionsService.getauctions({ "section": "SCHEDULED", "userid": userService.getUserId(), "sessionid": userService.getUserToken(), "limit": $scope.scheduledLimit })
                    //    .then(function (response) {
                    //        $scope.scheduledAuctions = response;
                    //        $scope.scheduledAuctionsLoaded = true;
                    //        if ($scope.scheduledAuctions.length > 0) {
                    //            $scope.scheduledAuctionsLoaded = true;
                    //            // $(".scheduledauctions").owlCarousel(options);
                    //        } else {
                    //            $scope.scheduledAuctionsLoaded = false;
                    //            $scope.scheduledAuctionsMessage = "There are no Negotiations scheduled for you in the future.";
                    //        }
                    //    });
                }
            };

            $scope.purchaseOrders = [];
            $scope.poNumbers = [];
            $scope.deliverySchedulePoNumbers = [];
            $scope.deliverySchedulesStr = "";
            $scope.deliverySchedulesStrTemp = "";


            $scope.totalPurchaseOrdersCount = 0;
            $scope.totaldeliverySchedulesCount = 0;
            $scope.getPurchaseOrders = function () {
                poService.getAssignedPO()
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.purchaseOrders = response;
                            $scope.totalPurchaseOrdersCount = $scope.purchaseOrders[0].TOTAL_COUNT;

                            $scope.purchaseOrders.forEach(function (item, index) {
                                $scope.poNumbers.push(item.PURCHASE_ORDER_ID);
                            });
                            $scope.deliverySchedulesStr = "";
                            $scope.deliverySchedulesStrTemp = "";

                            $scope.deliverySchedulesStrTemp = $scope.poNumbers;
                            $scope.deliverySchedulesStr = $scope.deliverySchedulesStrTemp.join(',');
                            if ($scope.deliverySchedulesStr && $scope.deliverySchedulesStr != null) {
                                $scope.getDeliverySchedules($scope.deliverySchedulesStr);
                            }
                        }
                    });
            };

            $scope.getDeliverySchedules = function (poNumbers) {
                poService.getDeliverySchedules(poNumbers)
                    .then(function (response) {
                        $scope.deliverySchedules = response;

                        $scope.deliverySchedules.forEach(function (item, index) {
                            item.DELIVERY_DATE = userService.toLocalDate(item.DELIVERY_DATE);
                        });
                        $scope.totaldeliverySchedulesCount = $scope.deliverySchedules[0].TOTAL_COUNT;
                    });
            };
            
            $scope.getMiniItems();

            if ($scope.isVendor) {
                $scope.getPurchaseOrders();
            }
           

            $scope.addItem = function () {
                var obj = {
                    auctionTimerId: 1000,
                    title: "test",
                    price: 11244354,
                    bids: 24,
                    auctionEnds: '123465'
                }
                $scope.myHotAuctions.push(obj);
            }



            $scope.reduceTime = function (timerId, time) {
                addCDSeconds(timerId, time);
            }

            $scope.stopBid = function (item) {
                $scope.myHotAuctions[0].TimeLeft = 60;
            }

            $scope.changeScheduledAuctionsLimit = function () {
                $scope.scheduledLimit = 8;
                $scope.getMiniItems();
            }

            $scope.changeHotAuctionsLimit = function () {
                $scope.hotLimit = 8;
                $scope.getMiniItems();
            }

            $scope.changeTodaysAuctionsLimit = function () {
                $scope.todaysLimit = 8;
                $scope.getMiniItems();
            }

            $scope.totalItems = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 5;

            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 5;

            $scope.maxSize = 5; //Number of pager buttons to show

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                //console.log('Page changed to: ' + $scope.currentPage);
            };


            $scope.myAuctionsLoaded = false;
            $scope.myAuctions = [];
            $scope.myActiveLeads = [];

            $scope.myActiveLeadsEnded = [];
            $scope.myActiveLeadsNotStarted = [];

            var loginUserData = userService.getUserObj();



            $scope.totalLeads = 0;
            $scope.totalLeads2 = 0;
            $scope.myLogistics = [];

            // Clickable dashboard //
            $scope.goToRequirements = function (status, priority) {
                $state.go('pages.profile.profile-timeline', { 'userId': userService.getUserId(), "filters": { status: status, priority: priority, fromDate: $scope.dashboardFromDate, endDate: $scope.dashboardToDate } });
            };
            // Clickable dashboard //
            $scope.goToPos = function () {
                $state.go('pendingpos', { 'userId': userService.getUserId() });
            };
            $scope.goToPendingContracts = function () {
                $state.go('pendingcontracts', { 'userId': userService.getUserId() });
            };
            $scope.goToPendingApprovals = function () {
                $state.go('workflow-pending-approvals', { 'userId': userService.getUserId() });
            };

            $scope.goToConsolidateReport = function () {
                // $state.go('consalidated-report');
                var url = $state.href("consalidatedReport");
                window.open(url, '_self');
            };

            $scope.goToDispatchTrackForm = function (reqID, purchaseID, dispatchCode) {
                var url = $state.href("dtform", { reqID: reqID, "poOrderId": purchaseID, "dCode": dispatchCode });
                window.open(url, '_blank');
            };

            //Pending Approvals
            $scope.GetMyPendingApprovals = function () {
                $scope.totalPendingApprovals = 0;
                let listUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
                let deptIDStr = '';
                let desigIDStr = '';
                let desigIDs = [];
                let deptIDs = [];
                if (listUserDepartmentDesignations && listUserDepartmentDesignations.length > 0) {
                    listUserDepartmentDesignations.forEach(function (item, index) {
                        deptIDs.push(item.deptID);
                        item.listDesignation.forEach(function (item1, index1) {
                            if (item1.isAssignedToUser && item1.isValid) {
                                desigIDs.push(item1.desigID);
                                desigIDs = _.union(desigIDs, [item1.desigID]);
                            }
                        });
                    });

                    deptIDStr = deptIDs.join(',');
                    desigIDStr = desigIDs.join(',');
                }

                workflowService.GetMyPendingApprovals(deptIDStr, desigIDStr)
                    .then(function (response) {
                        if (response) {
                            $scope.totalPendingApprovals = response.length;
                        }
                    });
            };

            $scope.GetMyPendingApprovals();
            //^Pending Approvals

            //Customer Chart Data

            $scope.chartData = {};

            $scope.getChartData = function () {
                var params = {
                    userid: userService.getUserId(),
                    sessionid: userService.getUserToken(),
                    fromDate: $scope.dashboardFromDate,
                    toDate: $scope.dashboardToDate
                    
                }
                auctionsService.getCustomerDashboard(params)
                    .then(function (response) {
                        $scope.chartData = response;

                        if (response && response.chartSeries.length > 0) {
                            console.log(response.chartSeries)
                            response.chartSeries.forEach(function (obj, index) {
                                $scope.requirementDetails.series[index].name = obj.name;
                                $scope.requirementDetails.series[index].data = obj.data;
                            });
                            $scope.requirementDetailsChartShow = true;
                        }

                        if (response && response.months.length > 0) {
                            response.months.forEach(function (data) {
                                $scope.requirementDetails.xAxis.categories.push(data);
                            });
                        }

                        if (response && (response.myReqCount.length > 0 || response.totalReqCount.length > 0)) {
                            $scope.transactionDetails.series[1].data = response.myReqCount;
                            $scope.transactionDetails.series[0].data = response.totalReqCount;
                            $scope.transactionDetails.xAxis.categories = response.trendMonths;
                            $scope.transactionDetailsChartShow = true;
                        }
                    });
            }
            if (!$scope.isVendor){
                $scope.getChartData();
            }
            //Customer Chart Data
           
        }]);