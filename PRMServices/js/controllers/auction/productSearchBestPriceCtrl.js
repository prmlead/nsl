﻿prmApp

    .controller('productSearchBestPriceCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$stateParams", "$timeout", "auctionsService", "userService", "SignalRFactory", "fileReader", "growlService", "$window", "catalogService",
        function ($scope, $http, $state, domain, $filter, $stateParams, $timeout, auctionsService, userService, SignalRFactory, fileReader, growlService, $window, catalogService) {


            $scope.searchString = '';

            // Pagination For User Products//
            $scope.userProductsPage = 0;
            $scope.userProductsPageSize = 200;
            $scope.userProductsfetchRecordsFrom = $scope.userProductsPage * $scope.userProductsPageSize;
            $scope.userProductstotalCount = 0;
            // Pagination For User Products//

            $scope.productsList = [];
            $scope.nonCoreproductsList = [];
            $scope.productId = 0;
            $scope.getUserProducts = function (IsPaging, index, searchString, fieldType, field) {

                catalogService.getUserProducts(0, 200, searchString, false, false, false).then(function (response) {
                    $scope.productsList = response;
                    $scope.productsList.forEach(function (item) {
                        item.isChecked = false;
                    })
                    $scope.userproductsLoading = false;
                });
            };



            $scope.selectedProduct = [];
            $scope.selectedProduct.showTable = false;




            $scope.getPreviousItemPrice = function (id) {
                $scope.selectedProduct = [];

                if (!$scope.selectedProduct.itemLastPriceArr) {
                    $scope.selectedProduct.itemLastPriceArr = [];
                }

                $scope.selectedProduct.showTable = true;
                $scope.selectedProduct.itemPreviousPrice = {};
                $scope.selectedProduct.itemPreviousPrice.lastPrice = -1;



                $scope.productsList.forEach(function (item) {
                    if (item.prodId == id) {
                        $scope.selectedProduct.compID = userService.getUserCompanyId();
                        $scope.selectedProduct.productNo = item.prodNo;
                        $scope.selectedProduct.productIDorName = item.prodName;
                        $scope.selectedProduct.productBrand = '';
                        $scope.selectedProduct.sessionID = userService.getUserToken();
                    }
                })


                auctionsService.getPreviousItemPrice($scope.selectedProduct)
                    .then(function (response) {
                        if (response && response.errorMessage === '') {
                            $scope.selectedProduct.itemPreviousPrice.lastPrice = Number(response.initialPrice);
                            $scope.selectedProduct.itemPreviousPrice.lastPriceDate = userService.toLocalDate(response.currentTime);
                            $scope.selectedProduct.itemPreviousPrice.lastPriceVendor = response.companyName;

                            if ($scope.selectedProduct.itemPreviousPrice.lastPrice) {
                                $scope.selectedProduct.itemLastPriceArr = _.filter($scope.selectedProduct.itemLastPriceArr, function (item) {
                                    return !item.isBestPrice;
                                });

                                $scope.selectedProduct.showTable = true;

                                $scope.selectedProduct.itemLastPriceArr.unshift({
                                    isBestPrice: true,
                                    requirementID: '',
                                    unitPrice: $scope.selectedProduct.itemPreviousPrice.lastPrice,
                                    quantity: '',
                                    companyName: $scope.selectedProduct.itemPreviousPrice.lastPriceVendor,
                                    currentTime: $scope.selectedProduct.itemPreviousPrice.lastPriceDate
                                });
                            }
                        }
                    });
            };

        }]);