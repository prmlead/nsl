﻿prmApp

    .controller('bidHistoryCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout", "auctionsService", "fwdauctionsService", "userService", "SignalRFactory", "fileReader", "growlService",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout, auctionsService, fwdauctionsService, userService, SignalRFactory, fileReader, growlService) {
            $scope.bidhistory = {};
            $scope.bidhistory.uID = $stateParams.Id;
            $scope.bidhistory.reqID = $stateParams.reqID;
            $log.info($stateParams.isfwd);
            $scope.isForwardBidding = $stateParams.isfwd == 'true' ? true : false;

            $scope.userID = userService.getUserId();
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            $scope.bidHistory = {};
            $scope.CovertedDate = '';
            $scope.Name = 'No previous bids';

            $scope.show = false;
            $scope.show1 = false;

            $scope.data = [];
            $scope.categories = [];
            $scope.auctionItem = [];

            $scope.renderChart = function (response) {
                $scope.bidHistory = response;

                if ($scope.bidHistory.length > 0) {
                    $scope.Name = $scope.bidHistory[0].firstName + ' ' + $scope.bidHistory[0].lastName;
                }

                $scope.bidHistory.forEach(function (item, index) {
                    $scope.data.push(item.bidAmount);
                    item.createdTime = userService.toLocalDate(item.createdTime);
                    //var time = new moment(item.createdTime).format("HH:mm");
                    $scope.categories.push(item.createdTime.toString());
                });

                $scope.startTime = $scope.bidHistory[0].createdTime;
                $scope.endTime = $scope.bidHistory[$scope.bidHistory.length - 1].createdTime;

                $scope.chartOptions = {
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: 'Requirement Bid History Graph'
                    },
                    xAxis: {
                        categories: $scope.categories,
                        title: {
                            text: 'Time ( Start Time: ' + $scope.startTime + ' - End Time: ' + $scope.endTime + ')'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Bid Price'
                        }
                    },

                    series: [{
                        name: $scope.Name,
                        data: $scope.data


                    }]
                };

                $scope.show = true;
            };

            $scope.GetBidHistory = function () {
                if ($scope.isForwardBidding) {
                    fwdauctionsService.GetBidHistory({ "reqid": $scope.bidhistory.reqID, 'userid': $scope.bidhistory.uID, "sessionid": userService.getUserToken() })
                        .then(function (response) {
                            $scope.renderChart(response);
                        });
                }
                else {
                    auctionsService.GetBidHistory({ "reqid": $scope.bidhistory.reqID, 'userid': $scope.bidhistory.uID, "sessionid": userService.getUserToken() })
                        .then(function (response) {
                            $scope.renderChart(response);
                        });
                }
            };

            $scope.GetBidHistory();

            $scope.GetDateconverted = function (dateBefore) {

                var date = dateBefore.split('+')[0].split('(')[1];
                var newDate = new Date(parseInt(parseInt(date)));
                $scope.CovertedDate = newDate.toString().replace('Z', '');
                return $scope.CovertedDate;

            };

            $scope.getData = function () {
                auctionsService.getrequirementdata({ "reqid": $scope.bidhistory.reqID, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        if (response) {
                            $scope.auctionItem = response;
                        }
                    });
            };

            $scope.getData();

            $scope.CBPricesAudit = {};

            $scope.GetCBPricesAudit = function () {
                auctionsService.GetCBPricesAudit($scope.bidhistory.reqID, $scope.bidhistory.uID, userService.getUserToken())
                    .then(function (response) {
                        $scope.CBPricesAudit = response;

                        $scope.CBPricesAudit.forEach(function (history, historyIndex) {

                            if ($scope.isCustomer) {
                                if ($scope.bidhistory.uID == history.CREATED_BY) {
                                    history.isTargetBid = true;
                                } else {
                                    history.isTargetBid = false;
                                }
                            }
                            else {
                                if ($scope.userID != history.CREATED_BY) {
                                    history.isTargetBid = true;
                                } else {
                                    history.isTargetBid = false;
                                }
                            }
                        });
                    });
            };

            $scope.GetCBPricesAudit();

            $scope.ProductFilter = [];
            $scope.ItemrenderChart = function (response, itemID, SelectedItem) {
                $scope.categories1 = [];
                if (SelectedItem) {
                    if (SelectedItem.isChecked) {
                        $scope.ProductFilter.push(SelectedItem);
                        $scope.categories1 = [];
                        $scope.itemLevelOptions.series = [];
                        $scope.show1 = false;
                    } else {
                        var tempIdx = $scope.ProductFilter.indexOf(SelectedItem);
                        if (tempIdx > -1) {
                            $scope.ProductFilter.splice(tempIdx, 1);
                        }
                    }
                }

                $scope.displaybidHistory = [];
                $scope.ItembidHistory = response;
                $scope.ItembidHistoryTemp = angular.copy(response);
                $scope.ItembidHistoryTemp.forEach(function (item, itemIdx) {
                    if ($scope.ProductFilter.length > 0) {
                        $scope.ProductFilter.forEach(function (product) {
                            if (product.itemID == item.itemID) {
                                item.bidHistory.forEach(function (bid) {
                                    // bid.createdTime = new moment(bid.createdTime).format("DD-MM-YYYY HH:mm");
                                    bid.createdTime = userService.toLocalDate(bid.createdTime);
                                    $scope.displaybidHistory.push(bid);
                                    $scope.categories1.push(bid.createdTime);
                                });
                            }
                        });
                    } else {
                        if (item && item.bidHistory.length > 0) {
                            item.bidHistory.forEach(function (bid) {
                                // bid.createdTime = new moment(bid.createdTime).format("DD-MM-YYYY HH:mm");
                                bid.createdTime = userService.toLocalDate(bid.createdTime);
                                $scope.displaybidHistory.push(bid);
                                $scope.categories1.push(bid.createdTime);
                            });
                        }
                    }
                });

                $scope.displaybidHistory = $scope.displaybidHistory.sort(function (a, b) {
                    a = new Date(a.createdTime);
                    b = new Date(b.createdTime);
                    return a - b;
                });

                $scope.categories1 = $scope.categories1 ? $scope.categories1.sort() : '';
                $scope.startTime = $scope.categories1[0] ? $scope.categories1[0] : '';
                $scope.endTime = $scope.categories1 ? $scope.categories1[$scope.categories1.length - 1] : '';
                $scope.itemLevelOptions = {
                    credits: {
                        enabled: false
                    },
                    options: {
                        chart: {
                            type: 'line'
                        }
                    },
                    series: [],
                    title: {
                        text: 'Item Bid History Graph'
                    },
                    xAxis: {
                        categories: $scope.categories1,
                        title: {
                            text: 'Time ( Start Time: ' + $scope.startTime + ' - End Time: ' + $scope.endTime + ')'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Item Bid Price'
                        }
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: false
                            },
                            enableMouseTracking: true
                        }
                    }
                }

                $scope.ItembidHistory.forEach(function (item, index) {

                    if ($scope.ProductFilter.length > 0) {
                        $scope.ProductFilter.forEach(function (product) {
                            if (product.itemID == item.itemID) {
                                var data = [];

                                item.bidHistory.forEach(function (bid, bidIdx) {
                                    data.push(bid.bidAmount);
                                });


                                $scope.itemLevelOptions.series.push({

                                    name: item.productIDorName,
                                    data: data
                                });
                            }
                        });
                    } else {
                        var data = [];

                        item.bidHistory.forEach(function (bid, bidIdx) {
                            data.push(bid.bidAmount);
                        });


                        $scope.itemLevelOptions.series.push({

                            name: item.productIDorName,
                            data: data
                        });
                    }


                });

                if ($scope.ItembidHistory.length > 0) {
                    $scope.Name1 = $scope.ItembidHistory[0].bidHistory[0] ? $scope.ItembidHistory[0].bidHistory[0].companyName : '';
                }

                $scope.show1 = true;
            };



            $scope.bidresponse = [];
            $scope.bidresponsetemp = [];
            $scope.GetItemBidHistory = function () {
                auctionsService.GetItemBidHistory({ "reqid": $scope.bidhistory.reqID, 'userid': $scope.bidhistory.uID, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.bidresponse = response;
                        $scope.bidresponsetemp = angular.copy(response);
                        $scope.ItemrenderChart(response, false, '');
                    });
            };

            $scope.GetItemBidHistory();
            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

        }]);