﻿prmApp
    // =========================================================================
    // Auction Tiles
    // =========================================================================
    .controller('anylaticsCtrl', ["$timeout", "$state", "$scope", "$log", "growlService", "userService", "auctionsService", "$http", "$rootScope",
        "SignalRFactory", "signalRHubName", "logisticServices", "$filter", "store", "poService", "workflowService", "reportingService",
        function ($timeout, $state, $scope, $log, growlService, userService, auctionsService, $http, $rootScope, $filter, SignalRFactory, signalRHubName, logisticServices, store, poService,
            workflowService, reportingService) {

            $scope.anylaticsFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
            $scope.anylaticsToDate = moment().format('YYYY-MM-DD');

            $scope.departmentWiseGraphShow = false;
            $scope.categoryWiseGraphShow = false;
            $scope.departmentWiseLevelGraphShow = false;
            $scope.categoryWiseLevelGraphShow = false;
            $scope.TitleText = '';
            $scope.deptTitleText = '';
            $scope.catTileText = '';
            $scope.UserSpendTitleText = '';
            $scope.ContributeSpendTitleText = '';
            $scope.UserSpentCatTitleText = '';
            $scope.DeptWiseCatTitleText = '';
            $scope.type = '';

            $scope.getAnalyticsTotalData = function () {
                var params = {
                    "fromDate": $scope.anylaticsFromDate,
                    "toDate": $scope.anylaticsToDate,
                    "compid": userService.getUserCompanyId()
                }

                reportingService.GetPRMAnalyticsTotals(params)
                    .then(function (response) {
                        $scope.analyticsTotalData = response;
                        $scope.departmentWiseLevelGraphShow = false;

                    });
            }
            $scope.getAnalyticsTotalData();
            
            $scope.getDeprtmentCategoryData = function (type) {
                $scope.type = type;
                if (type === 'totalSpend') {
                    var params = {
                        "fromDate": $scope.anylaticsFromDate,
                        "toDate": $scope.anylaticsToDate,
                        "compid": userService.getUserCompanyId()
                    }
                    reportingService.GetPRMAnalyticsSpend(params)
                        .then(function (response) {
                            $scope.analyticsTotalSpendData = response;
                            $scope.departmentWiseGraph.series = [];
                            $scope.categoryWiseGraph.series = [];
                            $scope.analyticsTotalSpendData.DepartmentSpend.forEach(function (item) {
                                $scope.departmentWiseGraph.series.push({
                                    'name': item.key1,
                                    'y': item.decimalVal
                                });
                            });
                            $scope.analyticsTotalSpendData.CategorySpend.forEach(function (item) {
                                $scope.categoryWiseGraph.series.push({
                                    'name': item.key1,
                                    'y': item.decimalVal
                                });
                            });
                            $scope.deptTitleText = 'Department Wise Total Spend';
                            $scope.catTileText = 'Category Wise Total Spend';
                            $('#upateChartButton').click();

                            $scope.departmentWiseGraphShow = true;
                            $scope.categoryWiseGraphShow = true;
                            $scope.departmentWiseLevelGraphShow = false;
                            $scope.categoryWiseLevelGraphShow = false;


                        });

                } else if (type === 'totalSavings') {

                    var params1 = {
                        "fromDate": $scope.anylaticsFromDate,
                        "toDate": $scope.anylaticsToDate,
                        "compid": userService.getUserCompanyId()
                    }

                    reportingService.GetPRMAnalyticsSavings(params1)
                        .then(function (response) {
                            $scope.analyticsTotalSavingsData = response;
                            $scope.departmentWiseGraph.series = [];
                            $scope.categoryWiseGraph.series = [];
                            $scope.analyticsTotalSavingsData.DepartmentSavings.forEach(function (item) {
                                $scope.departmentWiseGraph.series.push({
                                    'name': item.key1,
                                    'y': item.decimalVal
                                });
                            });
                            $scope.analyticsTotalSavingsData.CategorySavings.forEach(function (item) {
                                $scope.categoryWiseGraph.series.push({
                                    'name': item.key1,
                                    'y': item.decimalVal
                                });
                            });
                            $scope.deptTitleText = 'Department Wise Total Savings';
                            $scope.catTileText = 'Category Wise Total Savings';
                            $('#upateChartButton').click();

                            $scope.departmentWiseGraphShow = true;
                            $scope.categoryWiseGraphShow = true;
                            $scope.departmentWiseLevelGraphShow = false;
                            $scope.categoryWiseLevelGraphShow = false;
                        });
                } else {

                    var params2 = {
                        "fromDate": $scope.anylaticsFromDate,
                        "toDate": $scope.anylaticsToDate,
                        "compid": userService.getUserCompanyId()
                    }

                    reportingService.GetPRMAnalyticsPlants(params2)
                       .then(function (response) {
                            $scope.analyticsTotalPlantsData = response;
                            $scope.departmentWiseGraph.series = [];
                            $scope.categoryWiseGraph.series = [];
                            $scope.analyticsTotalPlantsData.DepartmentPlants.forEach(function (item) {
                                $scope.departmentWiseGraph.series.push({
                                    'name': item.key1,
                                    'y': item.value1
                                });
                            });
                            $scope.analyticsTotalPlantsData.CategoryPlants.forEach(function (item) {
                                $scope.categoryWiseGraph.series.push({
                                    'name': item.key1,
                                    'y': item.value1
                                });
                            });
                            $scope.deptTitleText = 'Department Wise Total Plants';
                            $scope.catTileText = 'Category Wise Total Plants';
                            $('#upateChartButton').click();

                            $scope.departmentWiseGraphShow = true;
                            $scope.categoryWiseGraphShow = true;
                            $scope.departmentWiseLevelGraphShow = false;
                            $scope.categoryWiseLevelGraphShow = false;
                       });
                }
            }

            $scope.getAnalyticsData = function (type, text, value) {
                $scope.type1 = type
                $scope.departmentWiseUserDataGraph.series = [];
                $scope.departmentWiseCategoryDataGraph.series = [];
                $scope.departmentWiseContributeGraph.series = [];
                $scope.departmentWiseUserDataGraph.xAxis.categories = [];
                $scope.departmentWiseCategorySpend = [];
                $scope.categoryWiseDepartmentSpend = [];
                if (type === 'totalSpendDept') {
                    $scope.analyticsTotalSpendData.DepartmentUserSpend.forEach(function (item) {
                        if (item.key1 == text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.decimalVal]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }

                    });
                   
                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalSpendData.DepartmentCategorySpend.filter(function (item) {
                        return item.key1 === text;
                    });

                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_SPEND
                    }, {
                        'name': text,
                        'y': value
                    });

                    $scope.TitleText = 'Department Wise Total Spend';
                    $scope.UserSpendTitleText = 'Department Wise User Spend';
                    $scope.DeptWiseCatTitleText = 'Department Wise Category Total Spend'
                    $scope.ContributeSpendTitleText = 'Department Wise Contribute Spend';
                    $scope.UserSpentCatTitleText = 'User Wise Category in department Spend';

                } else if (type === 'totalSpendCat') {
                    $scope.analyticsTotalSpendData.CategoryUserSpend.forEach(function (item) {
                        if (item.key1 == text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.decimalVal]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }

                    })
                    
                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalSpendData.DepartmentCategorySpend.filter(function (item) {
                        return item.value === text;
                    });
                   
                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_SPEND
                    }, {
                        'name': text,
                        'y': value
                    });

                    $scope.TitleText = 'Category Wise Total Spend';
                    $scope.UserSpendTitleText = 'Category Wise User Spend';
                    $scope.DeptWiseCatTitleText = 'Category Wise Department Total Spend'
                    $scope.ContributeSpendTitleText = 'Category Wise Contribute Spend';
                    $scope.UserSpentCatTitleText = 'User Wise Department in Category Spend';


                } else if (type === 'totalSavingsDept') {
                    $scope.analyticsTotalSavingsData.DepartmentUserSavings.forEach(function (item) {
                        if (item.key1 == text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.decimalVal]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }

                    });
                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalSavingsData.DepartmentCategorySavings.filter(function (item) {
                        return item.key1 === text;
                    });


                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_SAVINGS
                    }, {
                        'name': text,
                        'y': value
                    });
                    $scope.TitleText = 'Department Wise Total Savings';
                    $scope.UserSpendTitleText = 'Department Wise User Savings';
                    $scope.DeptWiseCatTitleText = 'Department Wise category Total Savings'
                    $scope.ContributeSpendTitleText = 'Department Wise Contribute Savings';
                    $scope.UserSpentCatTitleText = 'User Wise Category in department Savings';


                } else if (type === 'totalSavingsCat') {
                    $scope.analyticsTotalSavingsData.CategoryUserSavings.forEach(function (item) {
                        if (item.key1 == text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.decimalVal]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }

                    });
                    
                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalSavingsData.DepartmentCategorySavings.filter(function (item) {
                        return item.value === text;
                    });

                   
                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_SAVINGS
                    }, {
                        'name': text,
                        'y': value
                    });
                    $scope.TitleText = 'Category Wise Total Savings';
                    $scope.UserSpendTitleText = 'Category Wise User Savings';
                    $scope.DeptWiseCatTitleText = 'Category Wise Department Total Savings'
                    $scope.ContributeSpendTitleText = 'Category Wise Contribute Savings';
                    $scope.UserSpentCatTitleText = 'User Wise Department in Category Savings';


                } else if (type === 'totalPlantsDept') {
                    $scope.analyticsTotalPlantsData.DepartmentUserPlants.forEach(function (item) {
                        if (item.key1 == text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.value1]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }

                    });
                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalPlantsData.DepartmentCategoryPlants.filter(function (item) {
                        return item.key1 === text;
                    });
                   
                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_PLANTS
                    }, {
                        'name': text,
                        'y': value
                    });
                    $scope.TitleText = 'Plant-Department Wise';
                    $scope.UserSpendTitleText = 'Plant-Department Wise User';
                    $scope.DeptWiseCatTitleText = 'Department Wise Category Total Plants'
                    $scope.ContributeSpendTitleText = 'Plant-Department Wise Contribute';
                    $scope.UserSpentCatTitleText = 'Plant-User Wise Category in department';

                } else {
                    $scope.analyticsTotalPlantsData.CategoryUserPlants.forEach(function (item) {
                        if (item.key1 == text) {
                            $scope.departmentWiseUserDataGraph.series.push({
                                'name': item.value,
                                'data': [item.value1]
                            });
                            $scope.departmentWiseUserDataGraph.xAxis.categories.push(item.value);
                        }

                    });
                   
                    $scope.departmentWiseCategorySpend = $scope.analyticsTotalPlantsData.DepartmentCategoryPlants.filter(function (item) {
                        return item.value === text;
                    });
                   
                    $scope.departmentWiseContributeGraph.series.push({
                        'name': 'Total',
                        'y': $scope.analyticsTotalData.PARAM_TOTAL_PLANTS
                    }, {
                        'name': text,
                        'y': value
                    });
                    $scope.TitleText = 'Plant-Category Wise ';
                    $scope.UserSpendTitleText = 'Plant-Category Wise User';
                    $scope.DeptWiseCatTitleText = 'Category Wise Department Total Plants'
                    $scope.ContributeSpendTitleText = 'Plant-Category Wise Contribute';
                    $scope.UserSpentCatTitleText = 'Plant-User Wise Department in Category';

                }

                $('#upateChartButton2').click();

                $scope.departmentWiseLevelGraphShow = true;
                $scope.categoryWiseLevelGraphShow = false;


            }



            $scope.departmentWiseGraph = {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    events: {
                        load: function () {

                            var chart = this,
                                series = []
                            $('#upateChartButton').click(function () {
                                series = $scope.departmentWiseGraph.series
                                chart.update({
                                    series: [{ data: series }],
                                    title: {
                                        text: $scope.deptTitleText
                                    }
                                }, true, true);


                            });


                        }
                    }
                },
                title: {
                    text: 'Department Wise'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer'
                    },
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    //$scope.departmentWiseData(this.y);
                                    $scope.getAnalyticsData($scope.type + 'Dept', this.name, this.y)
                                    //$scope.departmentWiseLevelGraphShow = true;


                                }
                            }
                        }
                    }
                },

                series: []


            }


            $scope.categoryWiseGraph = {

                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];

                            $('#upateChartButton').click(function () {
                                series = $scope.categoryWiseGraph.series
                                chart.update({
                                    series: [{ data: series }],
                                    title: {
                                        text: $scope.catTileText
                                    }
                                }, true, true);


                            });

                        }
                    }
                },
                title: {
                    text: 'Category Wise'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer'
                    },
                    series: {
                        cursor: 'pointer'
                        //point: {
                        //    events: {
                        //        click: function () {
                        //            $scope.departmentWiseData(this.y);
                        //            $scope.getAnalyticsData($scope.type + 'Cat', this.y)
                        //            $('#upateChartButton').click();

                        //        }
                        //    }
                        //}
                    }

                },
                series: []
            }


            $scope.departmentWiseUserDataGraph = {
                chart: {
                    type: 'column',
                    width: 450,

                    height: 400,
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];
                            $('#upateChartButton2').click(function () {
                                series = $scope.departmentWiseUserDataGraph.series
                                categories = $scope.departmentWiseUserDataGraph.xAxis.categories
                                chart.update({
                                    series: series,
                                    xAxis: { categories: categories },
                                    title: {
                                        text: $scope.UserSpendTitleText
                                    }
                                }, true, true);
                            });
                        }
                    }

                },
                title: {
                    text: $scope.UserSpendTitleText
                },

                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: 'Users'
                    }

                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                series: []
            }

            $scope.departmentWiseCategoryDataGraph = {
                credits: {
                    enabled: false

                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    width: 1200,

                    height: 400,
                    events: {
                        load: function () {
                            var chart = this,
                                series = [];
                            $('#upateChartButton2').click(function () {
                                series = $scope.departmentWiseCategoryDataGraph.series

                                chart.update({
                                    series: [{ data: series }],

                                    title: {
                                        text: $scope.DeptWiseCatTitleText
                                    }
                                }, true, true);
                            });
                        }
                    }

                },
                title: {
                    text: $scope.DeptWiseCatTitleText
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer'
                    },
                    series: {
                        cursor: 'pointer'
                        //point: {
                        //    events: {
                        //        click: function () {
                        //            $scope.categoryWiseData(this.y);
                        //            $scope.categoryWiseLevelGraphShow = true;
                        //            if ($scope.DeptWiseCatTitleText.split(' ').indexOf('Department') > -1) {
                        //                $scope.getAnalyticsData($scope.type + 'Dept', this.name)
                        //            } else {
                        //                $scope.getAnalyticsData($scope.type + 'Cat', this.name)
                        //            }

                        //            $('#upateChartButton').click();
                        //        }
                        //    }
                        //}
                    }

                },
                series: []
            }

            $scope.departmentWiseContributeGraph = {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    width: 450,

                    height: 400,
                    events: {
                        load: function () {


                            var chart = this,
                                series = [];
                            $('#upateChartButton2').click(function () {
                                series = $scope.departmentWiseContributeGraph.series

                                chart.update({
                                    series: [{ data: series }],

                                    title: {
                                        text: $scope.ContributeSpendTitleText
                                    }
                                }, true, true);
                            });
                        }
                    }

                },
                title: {
                    text: $scope.ContributeSpendTitleText
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }

                },
                series: []
            }

            $scope.categoryWiseUserGraph = {
                chart: {
                    type: 'bar',
                    events: {
                        load: function () {

                            var chart = this,
                                series = [];
                            $('#upateChartButton').click(function () {
                                chart.update({
                                    title: {
                                        text: $scope.UserSpentCatTitleText
                                    }

                                }, true, true);
                            });
                        }
                    }

                },
                title: {
                    text: $scope.UserSpentCatTitleText
                },

                xAxis: {
                    categories: ['user1', 'user2', 'user3', 'user4', 'user5'],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ' millions'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    },
                    series: {
                        stacking: 'normal'
                    }
                },
                legend: {
                    reversed: true
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'John',
                    data: [5, 3, 4, 7, 2]
                }, {
                    name: 'Jane',
                    data: [2, 2, 3, 2, 1]
                }, {
                    name: 'Joe',
                    data: [3, 4, 4, 2, 5]
                }]
            }


        }]);