prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('formCatalogueCtrl', ["$state", "$stateParams", "$scope", "$rootScope",  "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "fwdauctionsService", "catalogService",
        "workflowService", "PRMPRServices", "PRMCustomFieldService", "reportingService", "PRMCustomFieldService", "$modal",
        function ($state, $stateParams, $scope, $rootScope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, techevalService, fwdauctionsService, catalogService,
            workflowService, PRMPRServices, PRMCustomFieldService, reportingService, PRMCustomFieldService, $modal) {
            $scope.showCatalogQuotationTemplate = true;
            $scope.userID = userService.getUserId();
            $scope.cloneId = ($stateParams.reqObj || {}).cloneId;
            $scope.isClone = ($stateParams.reqObj && $stateParams.reqObj.cloneId);
            $scope.selectedPRNumbers = $stateParams.selectedPRNumbers;
            $scope.prmTemplates = [];
            $scope.selectedTemplate = {};
            $scope.requirementPRStatus = {
                MESSAGE: '',
                showStatus: false
            };
            $scope.totalAttachmentMaxSize = 6291456;
            $scope.totalRequirementSize = 0;
            $scope.totalRequirementItemSize = 0;
            $scope.userproductsLoading = false;
            $scope.prmFieldMappingDetails = {};
            $scope.requirementSettings = [];
            $scope.reqId = $stateParams.Id;
            $scope.contractPRItems = [];
            $scope.templateSubItems = [];
            $scope.currentSystemDate = null;
            $scope.widgetStates = {
                MIN: 0,
                MAX: 1,
                PIN: -1
            };
            $scope.isCasNumber = false;
            $scope.isMfcdCode = false;
            if ($stateParams.Id) {
                $scope.stateParamsReqID = $stateParams.Id;
                $scope.postRequestLoding = true;
                $scope.showCatalogQuotationTemplate = false;
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
                $scope.desigID = userService.getSelectedUserDesigID();
                $scope.deptTypeID = userService.getSelectedUserDepartmentDesignation().deptTypeID;

            } else {
                $scope.stateParamsReqID = 0;
                $scope.postRequestLoding = false;
                $scope.showCatalogQuotationTemplate = true;
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
                $scope.desigID = userService.getSelectedUserDesigID();
                $scope.deptTypeID = userService.getSelectedUserDepartmentDesignation().deptTypeID;

            };
            
            $scope.INCO_TERMS = [];
            $scope.companyItemUnits = [];
            $scope.customFieldList = [];
            $scope.selectedcustomFieldList = [];
            $scope.otherRequirementItems = {
                INSTALLATION_CHARGES: true,
                PACKAGING: true,
                FREIGHT_CHARGES: true
            };
            var selectedItemIds = [];
            $scope.selectAllCheckBox = 0;
            if (!$scope.reqId) {
                $scope.selectAllCheckBox = 1;
            }
            $scope.selectAllQuotationTemplate = function (product,value) {
                
                if (value === 1) {
                    product.productQuotationTemplate.forEach(function (item) {
                        item.IS_VALID = 1;
                    })
                }
                else {
                    product.productQuotationTemplate.forEach(function (item) {
                        item.IS_VALID = 0;
                    })
                }
            }
        
            
            $scope.SelectedVendorsCount = 0;            
            $scope.isTechEval = false;
            $scope.isForwardBidding = false;
            $scope.allCompanyVendors = [];
            $scope.selectedProducts = [];

            if ($stateParams.selectedTemplate && $stateParams.selectedTemplate.TEMPLATE_ID) {
                $scope.selectedTemplate = $stateParams.selectedTemplate;
            }

            var curDate = new Date();
            var today = moment();
            var tomorrow = today.add('days', 1);
            $scope.subcategories = [];
            $scope.sub = {
                selectedSubcategories: [],
            }
            $scope.selectedCurrency = {};
            $scope.currencies = [];
            $scope.questionnaireList = [];
            $scope.selectedSubcategories = [];
            $scope.companyCatalogueList = [];
            $scope.selectVendorShow = true;
            $scope.isEdit = false;
            $scope.filterVendorCountries = [];
            $scope.vendorCountries = [];
            //Input Slider
            this.nouisliderValue = 4;
            this.nouisliderFrom = 25;
            this.nouisliderTo = 80;
            this.nouisliderRed = 35;
            this.nouisliderBlue = 90;
            this.nouisliderCyan = 20;
            this.nouisliderAmber = 60;
            this.nouisliderGreen = 75;

            //Color Picker
            this.color = '#03A9F4';
            this.color2 = '#8BC34A';
            this.color3 = '#F44336';
            this.color4 = '#FFC107';

            $scope.Vendors = [];
            $scope.VendorsTemp = [];
            $scope.categories = [];
            $scope.selectedA = [];
            $scope.selectedB = [];
            $scope.showCategoryDropdown = false;
            $scope.checkVendorPhoneUniqueResult = false;
            $scope.checkVendorEmailUniqueResult = false;
            $scope.checkVendorPanUniqueResult = false;
            $scope.checkVendorTinUniqueResult = false;
            $scope.checkVendorStnUniqueResult = false;
            $scope.showFreeCreditsMsg = false;
            $scope.showNoFreeCreditsMsg = false;
            $scope.formRequest = {
                productNameSearch: false,
                productCodeSearch: false,
                productNoSearch: false,
                isTabular: true,
                isRFP: false,
                auctionVendors: [],
                listRequirementItems: [],
                isQuotationPriceLimit: false,
                quotationPriceLimit: 0,
                quotationFreezTime: '',
                deleteQuotations: false,
                expStartTimeName: '',
                isDiscountQuotation: 0,
                isRevUnitDiscountEnable: 0,
                multipleAttachments: [],
                customerEmails: [],
                contractStartTime: '',
                contractEndTime: '',
                isContract: false,
                isRFQ: 1,
                biddingType: 'REVERSE',
                projectName: '',
                projectDetails: '',
                generalTC: '',
                deleteQuotations: false,
                disableDeleteQuotations: false,
                selectedPRItemIds: '',
                PR_ITEM_ID_STRING: '',
                SERV_LINE_ITEM_STRING: ''
            };

            //If create RFP is clicked//
            if ($stateParams.isRFP) {
                $scope.formRequest.isRFP = true;
            }
            //If create RFP is clicked//

            $scope.Vendors.city = "";
            $scope.Vendors.quotationUrl = "";
            $scope.vendorsLoaded = false;
            $scope.requirementAttachment = [];
            $scope.selectedProjectId = 0;
            $scope.selectedProject = {};
            $scope.branchProjects = [
                { PROJECT_ID: "RAW MATERIALS", PROJECT_CODE: "RAW MATERIALS"},
                { PROJECT_ID: "CAPEX", PROJECT_CODE: "CAPEX" },
                { PROJECT_ID: "SOLAR", PROJECT_CODE: "SOLAR" }
            ];

            $scope.changeProject = function () {
                console.log($scope.selectedProjectId);
                var filterdProjects = _.filter($scope.branchProjects, function (o) {
                    return o.PROJECT_ID == $scope.selectedProjectId;
                });
                if (filterdProjects && filterdProjects.length > 0) {
                    $scope.selectedProject = filterdProjects[0];
                }
            }

            $scope.sessionid = userService.getUserToken();

            $scope.selectedQuestionnaire == {}

            $scope.formRequest.indentID = 0;

            $scope.cijList = [];

            $scope.indentList = [];
            $scope.companyCurrencies = [];

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.formRequest.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'QUOTATION';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/
            
            $scope.compID = userService.getUserCompanyId();
            $scope.companyConfigStr = '';
            $scope.companyConfigurationArr = ['INCO', 'ITEM_UNITS', 'COUNTRY'];
            $scope.companyConfigStr = $scope.companyConfigurationArr.join(',').toString();
            $scope.companyConfigList = [];

            //$scope.vendorCountries = [];

            $scope.nonCoreproductsList = [];
            $scope.requirementAnalysisData = [];
            
            $scope.GetCompanyCurrencies = function () {
                auctionsService.GetCompanyCurrencies($scope.compID, userService.getUserToken())
                    .then(function (response) {
                        $scope.companyCurrencies = response;
                        $scope.companyCurrencies.forEach(function (currency, index) {
                            currency.StartTimeLocal = new Date(parseInt(currency.startTime.substr(6)));
                            currency.EndTimeLocal = new Date(parseInt(currency.endTime.substr(6)));
                        });
                    });
            };

            $scope.GetCompanyCurrencies();
            
            auctionsService.GetCompanyConfiguration($scope.compID, $scope.companyConfigStr, userService.getUserToken())
                .then(function (unitResponse) {

                    $scope.companyConfigList = unitResponse;
                    var countriesTemp = [];
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey == 'INCO') {
                            $scope.INCO_TERMS.push(item);
                        } else if (item.configKey == 'ITEM_UNITS') {
                            $scope.companyItemUnits.push(item);
                        } else if (item.configKey == 'COUNTRY') {
                            countriesTemp.push({ id: index, name: item.configValue });
                            $scope.vendorCountries = countriesTemp;
                        }
                    });

                });
            
            catalogService.GetNonCoreProducts($scope.compID, userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.nonCoreproductsList = unitResponse;
                    $scope.getUserProducts(0, '', 0, '','');
                });

            $scope.getPreviousItemPrice = function (itemDetails, index) {
                if (!itemDetails.itemLastPriceArr) {
                    itemDetails.itemLastPriceArr = [];
                }

                itemDetails.showTable = true;
                itemDetails.itemPreviousPrice = {};
                itemDetails.itemPreviousPrice.lastPrice = -1;
                itemDetails.bestPriceEnable = index;
                $log.info(itemDetails);
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                auctionsService.getPreviousItemPrice(itemDetails)
                .then(function (response) {
                    if (response && response.errorMessage === '') {
                        itemDetails.itemPreviousPrice.lastPrice = Number(response.initialPrice);
                        itemDetails.itemPreviousPrice.lastPriceDate = userService.toLocalDate(response.currentTime);
                        itemDetails.itemPreviousPrice.lastPriceVendor = response.companyName;

                        if (itemDetails.itemPreviousPrice.lastPrice) {
                            itemDetails.itemLastPriceArr = _.filter(itemDetails.itemLastPriceArr, function (item) {
                                return !item.isBestPrice;
                            });

                            itemDetails.bestLastPriceEnable = index;
                            itemDetails.showTable = true;

                            itemDetails.itemLastPriceArr.unshift({
                                isBestPrice: true,
                                requirementID: '',
                                unitPrice: itemDetails.itemPreviousPrice.lastPrice,
                                quantity: '',
                                companyName: itemDetails.itemPreviousPrice.lastPriceVendor,
                                currentTime: itemDetails.itemPreviousPrice.lastPriceDate
                            });
                        }
                    }
                });               
            };

            $scope.dispalyLastprices = function (product, val) {
                product.showTable = val;
            }

            $scope.GetLastPrice = function (itemDetails, index) {
                if (!itemDetails.itemLastPriceArr) {
                    itemDetails.itemLastPriceArr = [];
                }

                let bestPriceArray = _.filter(itemDetails.itemLastPriceArr, function (item) {
                    return item.isBestPrice;
                });

                itemDetails.itemLastPriceArr = [];
                itemDetails.bestLastPriceEnable = index;
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                auctionsService.GetLastPrice(itemDetails)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            itemDetails.itemLastPriceArr = response;
                            itemDetails.itemLastPriceArr.forEach(function (item, index) {
                                item.currentTime = userService.toLocalDate(item.currentTime);
                                item.isBestPrice = false;
                            });
                        }

                        if (bestPriceArray && bestPriceArray.length > 0) {
                            itemDetails.itemLastPriceArr.unshift(bestPriceArray[0]);
                        }
                    });
            };

            $scope.budgetValidate = function () {
                if ($scope.formRequest.budget != "" && (isNaN($scope.formRequest.budget) || $scope.formRequest.budget.indexOf('.') > -1)) {
                    $scope.postRequestLoding = false;
                    swal({
                        title: "Error!",
                        text: "Please enter valid budget, budget should be greater than 1,00,000.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {

                        });

                    $scope.formRequest.budget = "";
                }
            };

            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsAddNewReq.html', width: 1000, height: 500 });
            };

            $scope.changeCategory = function () {
                $scope.selectedSubCategoriesList = [];
                $scope.formRequest.auctionVendors = [];
                $scope.loadSubCategories();
            }

            $scope.getCreditCount = function () {
                userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.userDetails = response;
                        if (response.creditsLeft) {
                            $scope.showFreeCreditsMsg = true;
                        } else {
                            $scope.showNoFreeCreditsMsg = true;
                        }
                    });
            }



            $scope.VendorsList = [];
            $scope.VendorsTempList1 = [];

            $scope.getvendors = function (catObj) {
                let countryFilter = '';

                if ($scope.filterVendorCountries && $scope.filterVendorCountries.length > 0) {
                    countryFilter = _.map($scope.filterVendorCountries, 'name').join();
                }
                $scope.ShowDuplicateVendorsNames = [];
                $scope.VendorsList = [];
                $scope.VendorsTempList1 = [];
                $scope.vendorsLoaded = false;
                var category = '';

                category = catObj.catCode;

                var params = { 'Categories': category, 'locations': countryFilter, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getvendors',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            // filter vendors who has contracts
                            if ($scope.contractPRItems && $scope.contractPRItems.length > 0) {
                                let filteredVendors = _.filter(response.data, function (venObj) {
                                    return _.findIndex($scope.contractPRItems, (item) => { return item.CONTRACT_VENDOR === venObj.vendorID }) < 0;
                                });
                                response.data = filteredVendors;
                            }
                            
                            
                            $scope.VendorsList = response.data;
                            $scope.VendorsTempList = $scope.VendorsList;
                            $scope.Vendors.forEach(function (item, index) {
                                $scope.VendorsTempList.forEach(function (item1, index1) {
                                    if (item.vendorID == item1.vendorID) {
                                        $scope.ShowDuplicateVendorsNames.push(item1);
                                        $scope.VendorsList.splice(index1, 1);
                                    }
                                })
                            });

                            if ($scope.formRequest.auctionVendors.length > 0) {
                                $scope.formRequest.auctionVendors.forEach(function (item1, index1) {
                                    $scope.VendorsTempList.forEach(function (item2, index2) {
                                        if (item1.vendorID == item2.vendorID) {
                                            $scope.ShowDuplicateVendorsNames.push(item2);
                                            $scope.VendorsList.splice(index2, 1);
                                        }
                                    });
                                });
                            }


                            $scope.VendorsTempList1 = $scope.VendorsList;

                            if ($scope.ShowDuplicateVendorsNames.length > 0) {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            } else {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            }

                        } else {
                            $scope.VendorsList = [];
                        }

                        if ($scope.searchCategoryVendorstring != '') {
                            $scope.searchingCategoryVendors($scope.searchCategoryVendorstring);
                        } else {
                            $scope.searchingCategoryVendors('');
                        }
                    }

                }, function (result) {
                });
            };

            $scope.getReqQuestionnaire = function () {


            }

            $scope.getQuestionnaireList = function () {
                techevalService.getquestionnairelist(0)
                    .then(function (response) {
                        $scope.questionnaireList = $filter('filter')(response, { reqID: 0 });
                        if ($stateParams.Id && $stateParams.Id > 0) {
                            techevalService.getreqquestionnaire($stateParams.Id, 1)
                                .then(function (response) {
                                    $scope.selectedQuestionnaire = response;

                                    if ($scope.selectedQuestionnaire && $scope.selectedQuestionnaire.evalID > 0) {
                                        $scope.isTechEval = true;
                                    }

                                    $scope.questionnaireList.push($scope.selectedQuestionnaire);
                                })
                        }
                    })
            };

            $scope.isRequirementPosted = 0;
            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));
                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    } else {
                    }
                }, function (result) {
                });
                
            };


            $scope.getCurrencies = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getkeyvaluepairs?parameter=CURRENCY&compid=' + $scope.compID,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.currencies = response.data;
                            if (!$scope.formRequest.currency) {
                                $scope.formRequest.currency = 'INR';
                            }

                            $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: $scope.formRequest.currency });
                            $scope.selectedCurrency = $scope.selectedCurrency[0];
                            
                            $scope.getCreditCount();
                        }
                    } else {
                    }
                }, function (result) {
                });
            };

            


            $scope.getData = function () {
                
                // $scope.getCategories();

                if ($stateParams.Id) {
                    var id = $stateParams.Id;
                    $scope.isEdit = true;


                    auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), "userid": userService.getUserId() })
                        .then(function (response) {
                            $scope.GetReqPRItems();
                            $scope.selectedProjectId = response.projectId;
                            var category = response.category[0];
                            response.category = category;
                            response.taxes = parseInt(response.taxes);
                            //response.paymentTerms = parseInt(response.paymentTerms);
                            $scope.formRequest = response;
                            loadPRData();
                            $scope.formRequest.isBiddingTypeSpot =  $scope.formRequest.biddingType === 'SPOT' ? true : false;
                            $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: $scope.formRequest.currency });
                            $scope.selectedCurrency = $scope.selectedCurrency[0];
                            //$scope.getPRNumber($scope.formRequest.PR_ID);
                            $scope.itemSNo = $scope.formRequest.itemSNoCount;
                            //$scope.GetIncoTerms();
                            $scope.formRequest.checkBoxEmail = true;
                            $scope.formRequest.checkBoxSms = true;
                            $scope.loadSubCategories();
                            $scope.getSubUserData();
                            $scope.isRequirementPosted = $scope.formRequest.auctionVendors.length;
                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }
                            $scope.formRequest.attFile = response.attachmentName;
                            if ($scope.formRequest.attFile != '' && $scope.formRequest.attFile != null && $scope.formRequest.attFile != undefined) {
                                var attchArray = $scope.formRequest.attFile.split(',');
                                attchArray.forEach(function (att, index) {

                                    var fileUpload = {
                                        fileStream: [],
                                        fileName: '',
                                        fileID: att
                                    };

                                    $scope.formRequest.multipleAttachments.push(fileUpload);
                                });
                            }

                            $scope.selectedSubcategories = response.subcategories.split(",");
                            for (i = 0; i < $scope.selectedSubcategories.length; i++) {
                                for (j = 0; j < $scope.subcategories.length; j++) {
                                    if ($scope.selectedSubcategories[i] == $scope.subcategories[j].subcategory) {
                                        $scope.subcategories[j].ticked = true;
                                    }
                                }
                            }

                            //$scope.getvendors();
                            $scope.selectSubcat();
                            $scope.formRequest.attFile = response.attachmentName;
                            $scope.formRequest.quotationFreezTime = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.quotationFreezTimeNew = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.urgency.push(urgency);

                            $scope.formRequest.expStartTime = userService.toLocalDate($scope.formRequest.expStartTime);
                            $scope.SelectedVendors = $scope.formRequest.auctionVendors;
                            $scope.SelectedVendorsCount = $scope.formRequest.auctionVendors.length;
                            $scope.formRequest.auctionVendors.forEach(function (vendor, vendorIndex) {
                                vendor.isSelected = true;
                            });

                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIndex) {
                                item.AddMultipleQuantities = [];
                                item.CategoryId = item.CATEGORY_ID;
                                item.isNonCoreProductCategory = !item.isCoreProductCategory; //Try to clean up keep only one property
                                if (item.productQuotationTemplateJson && item.productQuotationTemplateJson != '' && item.productQuotationTemplateJson != null && item.productQuotationTemplateJson != undefined) {
                                    item.productQuotationTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                                    item.productQuotationTemplate = JSON.parse(item.productQuotationTemplateJson);

                                    /******** Changed for Multi Quantity *******/
                                    $scope.GetProductQuotationTemplate(item.catalogueItemID, itemIndex);
                                    /******** Changed for Multi Quantity *******/
                                }
                                else {
                                    item.productQuotationTemplateArray = [];
                                }

                                if (item.productQuotationTemplate && item.productQuotationTemplate.length > 0) {
                                    item.productQuotationTemplate.pop();
                                }
                            });

                            $scope.postRequestLoding = false;
                        });
                }

            };

            $scope.showSimilarNegotiationsButton = function (value, searchstring) {
                $scope.showSimilarNegotiations = value;
                if (!value) {
                    $scope.CompanyLeads = {};
                }
                if (value) {
                    if (searchstring.length < 3) {
                        $scope.CompanyLeads = {};
                    }
                    if (searchstring.length > 2) {
                        $scope.searchstring = searchstring;
                        $scope.GetCompanyLeads(searchstring);
                    }
                }
                return $scope.showSimilarNegotiations;
            }

            $scope.showSimilarNegotiations = false;

            $scope.CompanyLeads = {};

            $scope.searchstring = '';

            $scope.GetCompanyLeads = function (searchstring) {
                if (searchstring.length < 3) {
                    $scope.CompanyLeads = {};
                }
                if ($scope.showSimilarNegotiations && searchstring.length > 2) {
                    $scope.searchstring = searchstring;
                    var params = { "userid": userService.getUserId(), "searchstring": $scope.searchstring, "searchtype": 'Title', "sessionid": userService.getUserToken() };
                    auctionsService.GetCompanyLeads(params)
                        .then(function (response) {
                            $scope.CompanyLeads = response;
                            $scope.CompanyLeads.forEach(function (item, index) {
                                item.postedOn = userService.toLocalDate(item.postedOn);
                            })
                        });
                }
            }


            $scope.changeScheduledAuctionsLimit = function () {
                $scope.scheduledLimit = 8;
                $scope.getMiniItems();
            }

            $scope.loadSubCategories = function () {
                $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.formRequest.category });
                /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
            }

            $scope.selectedSubCategoriesList = [];

            $scope.selectSubcat = function (subcat) {

                $scope.selectedSubCategoriesList = [];

                if (!$scope.isEdit) {
                    $scope.formRequest.auctionVendors = [];
                }
                $scope.vendorsLoaded = false;
                var category = [];
                var count = 0;
                var succategory = "";
                $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
                selectedcount = $scope.sub.selectedSubcategories.length;
                if (selectedcount > 0) {
                    succategory = _.map($scope.sub.selectedSubcategories, 'id');
                    category.push(succategory);

                    $scope.selectedSubCategoriesList = succategory;

                    //$scope.formRequest.category = category;
                    if ($scope.formRequest.category != undefined) {
                        var params = { 'Categories': succategory, 'sessionID': userService.getUserToken(), 'count': selectedcount, 'uID': userService.getUserId(), evalID: $scope.selectedQuestionnaire ? $scope.selectedQuestionnaire.evalID : 0 };
                        $http({
                            method: 'POST',
                            url: domain + 'getvendorsbycatnsubcat',
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' },
                            data: params
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    $scope.Vendors = response.data;
                                    $scope.vendorsLoaded = true;
                                    for (var j in $scope.formRequest.auctionVendors) {
                                        for (var i in $scope.Vendors) {
                                            if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                                $scope.Vendors.splice(i, 1);
                                            }
                                        }
                                    }

                                    $scope.VendorsTemp = $scope.Vendors;
                                    $scope.searchVendors('');

                                }
                                //$scope.formRequest.auctionVendors =[];
                            } else {
                            }
                        }, function (result) {
                        });
                    }
                } else {
                    //$scope.getvendors();
                }

            }

            $scope.getData();

            $scope.checkVendorUniqueResult = function (idtype, inputvalue) {


                if (idtype == "PHONE") {
                    $scope.checkVendorPhoneUniqueResult = false;
                } else if (idtype == "EMAIL") {
                    $scope.checkVendorEmailUniqueResult = false;
                }
                else if (idtype == "PAN") {
                    $scope.checkVendorPanUniqueResult = false;
                }
                else if (idtype == "TIN") {
                    $scope.checkVendorTinUniqueResult = false;
                }
                else if (idtype == "STN") {
                    $scope.checkVendorStnUniqueResult = false;
                }

                if (inputvalue == "" || inputvalue == undefined) {
                    return false;
                }

                userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                    if (idtype == "PHONE") {
                        $scope.checkVendorPhoneUniqueResult = !response;
                    } else if (idtype == "EMAIL") {
                        $scope.checkVendorEmailUniqueResult = !response;
                    }
                    else if (idtype == "PAN") {
                        $scope.checkVendorPanUniqueResult = !response;
                    }
                    else if (idtype == "TIN") {
                        $scope.checkVendorTinUniqueResult = !response;
                    }
                    else if (idtype == "STN") {
                        $scope.checkVendorStnUniqueResult = !response;
                    }
                });
            };

            $scope.selectForA = function (vendor) {
                $scope.getVendorCodes(vendor);
                vendor.isSelected = true;
                if ($scope.formRequest.auctionVendors && $scope.formRequest.auctionVendors.length > 0) {
                    $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.vendorID !== vendor.vendorID; });
                } else {
                    $scope.formRequest.auctionVendors = [];
                }

                $scope.formRequest.auctionVendors.push(vendor);
                $scope.GetRequirementVendorAnalysis();
                var tempVendor = _.filter($scope.Vendors, function (x) { return x.vendorID === vendor.vendorID; });
                if (tempVendor && tempVendor.length > 0) {
                    tempVendor[0].isSelected = true;
                }

                var unSelectedVendors = _.filter($scope.Vendors, function (x) { return !x.isSelected; });
                if (unSelectedVendors && unSelectedVendors.length > 0) {
                    $scope.selectAllVendors = false;
                }
            };

            $scope.selectForB = function (vendor) {
                vendor.isSelected = false;
                $scope.GetRequirementVendorAnalysis();
                $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.vendorID !== vendor.vendorID; });
                var tempVendor = _.filter($scope.Vendors, function (x) { return x.vendorID === vendor.vendorID; });
                if (tempVendor && tempVendor.length > 0) {
                    tempVendor[0].isSelected = false;
                }

                var unSelectedVendors = _.filter($scope.Vendors, function (x) { return !x.isSelected; });
                if (unSelectedVendors && unSelectedVendors.length > 0) {
                    $scope.selectAllVendors = false;
                }
            };

            $scope.multipleAttachments = [];
            $scope.getFile = function () {
                $scope.progress = 0;
                $scope.totalRequirementSize = 0;
                //$scope.file = $("#attachement")[0].files[0];
                $scope.multipleAttachments = $("#attachement")[0].files;
                $scope.multipleAttachments = Object.values($scope.multipleAttachments);
                if ($scope.multipleAttachments && $scope.multipleAttachments.length > 0) {
                    $scope.multipleAttachments.forEach(function (item, index) {
                        $scope.totalRequirementSize = $scope.totalRequirementSize + item.size;
                    });
                }

                if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }

                $scope.multipleAttachments.forEach(function (item, index) {
                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;
                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }
                            
                            var ifExists = _.findIndex($scope.formRequest.multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                            if (ifExists > -1) {
                            } else {
                                $scope.formRequest.multipleAttachments.push(fileUpload);
                                
                            }

                        });
                });

            };


            $scope.newVendor = {};
            $scope.Attaachmentparams = {};
            $scope.deleteAttachment = function (reqid) {
                $scope.Attaachmentparams = {
                    reqID: reqid,
                    userID: userService.getUserId()
                }
                auctionsService.deleteAttachment($scope.Attaachmentparams)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Attachment deleted Successfully", "inverse");
                            $scope.getData();
                        }
                    });
            }

            $scope.newVendor.panno = "";
            $scope.newVendor.vatNum = "";
            $scope.newVendor.serviceTaxNo = "";

            $scope.addVendor = function () {
                $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
                $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                //$scope.emailRegx = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
                var addVendorValidationStatus = false;
                $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
                    $scope.companyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
                    $scope.firstvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
                    $scope.lastvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                    $scope.contactvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if ($scope.newVendor.contactNum.length != 10) {
                    $scope.contactvalidationlength = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
                    $scope.emailvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if (!$scope.emailRegx.test($scope.newVendor.email)) {
                    $scope.emailregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vendorcurrency == "" || $scope.newVendor.vendorcurrency === undefined) {
                    $scope.vendorcurrencyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.panno != "" && $scope.newVendor.panno != undefined && !$scope.panregx.test($scope.newVendor.panno)) {
                    $scope.panregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vatNum != "" && $scope.newVendor.vatNum != undefined && $scope.newVendor.vatNum.length != 11) {
                    $scope.tinvalidation = true;
                    addVendorValidationStatus = true;
                }

                if ($scope.newVendor.serviceTaxNo != "" && $scope.newVendor.serviceTaxNo != undefined && $scope.newVendor.serviceTaxNo.length != 15) {
                    $scope.stnvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.formRequest.category == "" || $scope.formRequest.category === undefined) {
                    $scope.categoryvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
                    addVendorValidationStatus = true;
                }
                if (addVendorValidationStatus) {
                    return false;
                }
                var vendCAtegories = [];
                $scope.newVendor.category = $scope.formRequest.category;
                vendCAtegories.push($scope.newVendor.category);
                var params = {
                    "register": {
                        "firstName": $scope.newVendor.firstName,
                        "lastName": $scope.newVendor.lastName,
                        "email": $scope.newVendor.email,
                        "phoneNum": $scope.newVendor.contactNum,
                        "username": $scope.newVendor.contactNum,
                        "password": $scope.newVendor.contactNum,
                        "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                        "isOTPVerified": 0,
                        "category": $scope.newVendor.category,
                        "userType": "VENDOR",
                        "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                        "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                        "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                        "referringUserID": userService.getUserId(),
                        "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                        "errorMessage": "",
                        "sessionID": "",
                        "userID": 0,
                        "department": "",
                        "currency": $scope.newVendor.vendorcurrency.key,
                        "altPhoneNum": $scope.newVendor.altPhoneNum,
                        "altEmail": $scope.newVendor.altEmail,
                        "subcategories": $scope.selectedSubCategoriesList
                    }
                };
                $http({
                    method: 'POST',
                    url: domain + 'register',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if ((response && response.data && response.data.errorMessage == "") || response.data.errorMessage == 'User assigned to Company.') {
                        $scope.formRequest.auctionVendors.push({ vendorName: $scope.newVendor.firstName + " " + $scope.newVendor.lastName, companyName: $scope.newVendor.companyName, vendorID: response.data.objectID });
                        $scope.GetRequirementVendorAnalysis();
                        $scope.newVendor = null;
                        $scope.newVendor = {};
                        //$scope.addVendorForm.$setPristine();
                        $scope.addVendorShow = false;
                        $scope.selectVendorShow = true;
                        $scope.newVendor = {};
                        $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                        growlService.growl("Vendor Added Successfully.", 'inverse');
                    } else if (response && response.data && response.data.errorMessage) {
                        growlService.growl(response.data.errorMessage, 'inverse');
                    } else {
                        growlService.growl('Unexpected Error Occurred', 'inverse');
                    }
                });
            }

            
            $scope.formRequest.checkBoxEmail = true;
            $scope.formRequest.checkBoxSms = true;
            //$scope.postRequestLoding = false;
            $scope.formRequest.urgency = 'High (Will be Closed in 2 Days)';
            $scope.formRequest.deliveryLocation = '';
            $scope.formRequest.paymentTerms = '';
            $scope.formRequest.isSubmit = 0;


            $scope.titleValidation = $scope.attachmentNameValidation = false;
            $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = $scope.incoTermValidation = false;
            $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

            $scope.postRequest = function (isSubmit, pageNo, navigateToView, stage) {

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = $scope.incoTermValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

                $scope.postRequestLoding = true;
                $scope.formRequest.isSubmit = isSubmit;

                if (isSubmit == 1 || pageNo == 1) {
                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if (isSubmit == 1) {
                        if ($scope.formRequest.isTabular) {
                            $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                if (!item.attachmentName || item.attachmentName == '') {
                                    $scope.attachmentNameValidation = true;
                                    return false;
                                }
                            })

                        }
                        if (!$scope.formRequest.isTabular) {
                            if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                                $scope.descriptionValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                        }
                    }
                }

                if (isSubmit == 1) {
                    
                    if ($scope.generalTC == null || $scope.generalTC == '' || $scope.generalTC == undefined) {
                        $scope.incoTermValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    
                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }

                if (!$scope.postRequestLoding) {
                    return false;
                }


                if (pageNo != 4) {
                    $scope.textMessage = "Save as Draft.";
                }

                let isValidVendorCurrency = true;
                let inValidVendorCurrencyValue = '';
                $scope.formRequest.auctionVendors.forEach(function (vendor, itemIndexs) {
                    if (vendor && vendor.vendorCurrency) {
                        let selecVendorCurrencyTemp = _.filter($scope.companyCurrencies, function (currItem) { return currItem.currency === vendor.vendorCurrency; });
                        if (selecVendorCurrencyTemp && selecVendorCurrencyTemp.length > 0) {                           
                            if (selecVendorCurrencyTemp[0].StartTimeLocal > $scope.currentSystemDate || selecVendorCurrencyTemp[0].EndTimeLocal < $scope.currentSystemDate) {
                                if (!inValidVendorCurrencyValue.includes(vendor.vendorCurrency)) {
                                    inValidVendorCurrencyValue += vendor.vendorCurrency + ',';
                                }
                                if (isValidVendorCurrency) {
                                    isValidVendorCurrency = false;
                                }
                            }
                        }
                    }
                });

                if ($scope.formRequest.checkBoxEmail == true && $scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email and sms invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxEmail == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an SMS invite to all the vendors selected above.";
                }
                else if (pageNo == 4) {
                    $scope.textMessage = "This will not send an SMS or EMAIL the vendors selected above.";
                }

                if ($scope.stateParamsReqID && !$scope.formRequest.deleteQuotations && pageNo == 4 && !$scope.isClone) {
                    $scope.textMessage = "Please select \"Request New Quotations\" to vendors if you modify any details in Requirement item specifications / Quantity. Click \"Cancel\" to select the option.";
                }

                $scope.formRequest.currency = $scope.selectedCurrency.value;
                $scope.formRequest.timeZoneID = 190;
                $scope.postRequestLoding = false;
                $scope.formRequest.isDiscountQuotation = $scope.formRequest.isDiscountQuotation === 3 ? 0 : $scope.formRequest.isDiscountQuotation;
                //This above line is just to know whether it's a spot bidding and would not have any downstream impact
                
                if (!isValidVendorCurrency) {
                    inValidVendorCurrencyValue = inValidVendorCurrencyValue.replace(/,\s*$/, "");
                    $scope.postRequestLoding = false;
                    swal({
                        title: "Error!",
                        title: "Selected vendor currency (" + inValidVendorCurrencyValue + ") conversion rate has expired, please validate.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return false;
                        });
                } else {


                    swal({
                        title: "Are you sure?",
                        text: $scope.textMessage,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        $scope.postRequestLoding = true;
                        var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                        var m = moment(ts);
                        var quotationDate = new Date(m);
                        var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                        $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000++530)/";
                        // this post request



                        var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                        var m = moment(ts);
                        var quotationDate = new Date(m);
                        var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                        $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";


                        if ($scope.formRequest.biddingType === 'SPOT') {
                            $scope.formRequest.quotationFreezTime = $scope.formRequest.expStartTime;
                        }

                        $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                        $scope.formRequest.customerID = userService.getUserId();
                        $scope.formRequest.customerFirstname = userService.getFirstname();
                        $scope.formRequest.customerLastname = userService.getLastname();
                        $scope.formRequest.isClosed = "NOTSTARTED";
                        $scope.formRequest.prNumbers = $scope.selectedPRNumbers ? $scope.selectedPRNumbers : '';
                        $scope.formRequest.endTime = "";
                        $scope.formRequest.sessionID = userService.getUserToken();
                        $scope.formRequest.subcategories = "";
                        $scope.formRequest.budget = 100000;
                        $scope.formRequest.custCompID = userService.getUserCompanyId();
                        $scope.formRequest.customerCompanyName = userService.getUserCompanyId();
                        $scope.formRequest.generalTC = (($scope.generalTC || {}).configValue || '');

                        var category = [];
                        $scope.formRequest.category = category;
                        /***** Adding Multiple Quantity ******/
                        var multipleQuantites = [];

                        multipleQuantites = $scope.formRequest.listRequirementItems.filter(function (item) {
                            if (item.AddMultipleQuantities && item.AddMultipleQuantities.length > 0 && item.AddMultipleQuantities[0].productSNo === item.productSNo) {
                                return item.AddMultipleQuantities;
                            }
                        });

                        if (multipleQuantites && multipleQuantites.length > 0) {
                            multipleQuantites.forEach(function (multipleQtyItem, multipleQtyIndex) {
                                multipleQtyItem.AddMultipleQuantities.forEach(function (item, index) {
                                    if (item.productQuantity && item.productQuantity > 0) {
                                        var productCount = $scope.formRequest.listRequirementItems.length;
                                        item.productSNo = parseInt(productCount + 1);
                                        $scope.formRequest.listRequirementItems.push(item);
                                    }
                                })
                            })
                        }
                        
                        /***** Adding Multiple Quantity ******/

                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIndexs) {
                                if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                                    item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                        subItem.dateCreated = "/Date(1561000200000+0530)/";
                                        subItem.dateModified = "/Date(1561000200000+0530)/";
                                    })
                                }
                                if (item.productQuotationTemplate && item.productQuotationTemplate.length > 0) {
                                    var filterPriceValidSubItems = item.productQuotationTemplate.filter(function (subItem, subItemIndex) {
                                        return subItem.HAS_PRICE && subItem.IS_VALID;
                                    });

                                    if (filterPriceValidSubItems && filterPriceValidSubItems.length > 0) {
                                        item.ceilingPrice = 0;
                                        item.ceilingPrice = _.sumBy(filterPriceValidSubItems, 'CEILING_PRICE');
                                    }
                                }

                            });


                            $scope.formRequest.cloneID = 0;
                            if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                                $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                                $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });
                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    $scope.formRequest.listRequirementItems.forEach(function (item, itemIndexs) {
                                        item.ceilingPrice = 0;
                                        if (item.productQuotationTemplate && item.productQuotationTemplate.length > 0) {
                                            item.productQuotationTemplate.forEach(function (subItem, subItemIndex) {
                                                if (subItem.CEILING_PRICE > 0 && subItem.HAS_PRICE) {
                                                    subItem.CEILING_PRICE = 0;
                                                }
                                            });
                                            item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplate);
                                        }
                                    });
                                }
                            }

                            if (selectedItemIds && selectedItemIds.length > 0) {
                                $scope.formRequest.selectedPRItemIds = selectedItemIds.join(',');
                            }

                            $scope.formRequest.templateId = $scope.selectedTemplate.TEMPLATE_ID;
                            $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                if (item.isNonCoreProductCategory) {
                                    item.productQuotationTemplateJson = '';
                                }
                            });

                            $scope.formRequest.auctionVendors.forEach(function (vendor, index) {
                                if (vendor.listRequirementItems) {
                                    vendor.listRequirementItems.forEach(function (item, index) {
                                        if (item.isNonCoreProductCategory) {
                                            item.productQuotationTemplateJson = '';
                                        }
                                    });
                                }
                            });

                            auctionsService.postrequirementdata($scope.formRequest)
                                .then(function (response) {
                                    if (response.objectID != 0) {
                                        if ($scope.selectedProject) {
                                            $scope.selectedProject.sessionID = userService.getUserToken();
                                            $scope.selectedProject.requirement = {};
                                            $scope.selectedProject.requirement.requirementID = response.objectID;
                                            var param = { item: $scope.selectedProject };
                                        }

                                        $scope.saveRequirementSetting(response.objectID, 'TEMPLATE_ID', $scope.selectedTemplate.TEMPLATE_ID);
                                        $scope.SaveReqDepartments(response.objectID);
                                        $scope.saveRequirementCustomFields(response.objectID);
                                        if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                            techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                                .then(function (response1) {
                                                    $scope.selectedQuestionnaire = response1;
                                                    if ($scope.isTechEval) {
                                                        $scope.selectedQuestionnaire.reqID = response.objectID;
                                                    }
                                                    else {
                                                        $scope.selectedQuestionnaire.reqID = 0;
                                                    }
                                                    $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                    var params = {
                                                        questionnaire: $scope.selectedQuestionnaire
                                                    }
                                                    techevalService.assignquestionnaire(params)
                                                        .then(function (response) {
                                                        })
                                                })
                                        }
                                        if (stage) {
                                            $state.go('save-requirementAdv', { Id: response.objectID });
                                        }
                                        swal({
                                            title: "Done!",
                                            text: "Requirement Saved Successfully",
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                //$scope.postRequestLoding = false;
                                                //$state.go('view-requirement');

                                                if (navigateToView && stage == undefined) {
                                                    $state.go('view-requirement', { 'Id': response.objectID });
                                                } else {
                                                    if ($scope.stateParamsReqID > 0 && stage == undefined) {
                                                        location.reload();
                                                    }
                                                    else {
                                                        if (stage == undefined) {
                                                            //$state.go('form.addnewrequirement', { 'Id': response.objectID });
                                                            $state.go('save-requirementAdv', { Id: response.objectID });
                                                        }
                                                    }
                                                }
                                            });
                                    }
                                });
                    });
                    //$scope.postRequestLoding = false;
                }
            };


            $scope.ItemFile = '';
            $scope.itemSNo = 1;
            $scope.ItemFileName = '';

            $scope.itemnumber = $scope.formRequest.listRequirementItems.length;

            $scope.requirementItems =
                {
                    productSNo: $scope.itemSNo++,
                    ItemID: 0,
                    productIDorName: '',
                    productNo: '',
                    productDescription: '',
                    productQuantity: 0,
                    productBrand: '',
                    othersBrands: '',
                    isDeleted: 0,
                    itemAttachment: '',
                    attachmentName: '',
                    itemMinReduction: 0,
                    splitenabled: 0,
                    fromrange: 0,
                    torange: 0,
                    requiredQuantity: 0,
                    productCode: '',
                    CategoryId: '',
                productDeliveryDetails: '',
                VERTICAL_TYPE: ''
                }

            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
            $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                item.AddMultipleQuantities = [];
            });

            $scope.AddItem = function () {
                $window.scrollBy(0, 50);
                const maxSnoItem = _.maxBy($scope.formRequest.listRequirementItems, 'productSNo');
                $scope.itemnumber = maxSnoItem ? maxSnoItem.productSNo : 1;
                $scope.requirementItems =
                    {
                        productSNo: $scope.itemnumber + 1,
                        ItemID: 0,
                        productIDorName: '',
                        productNo: '',
                        productDescription: '',
                        productQuantity: 0,
                        productBrand: '',
                        othersBrands: '',
                        isDeleted: 0,
                        itemAttachment: '',
                        attachmentName: '',
                        itemMinReduction: 0,
                        splitenabled: 0,
                        fromrange: 0,
                        torange: 0,
                        requiredQuantity: 0,
                        productCode: '',
                        productDeliveryDetails: '',
                    VERTICAL_TYPE: ''
                    }

                $scope.formRequest.listRequirementItems.push($scope.requirementItems);

                //Widget Management
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    if (item.productCode) {
                        item.widgetState = 0;
                    }
                    if (!item.AddMultipleQuantities) {
                        item.AddMultipleQuantities = [];
                    }
                });
                //^Widget Management
            };

            $scope.handleWindowState = function (product, state) {
                if (product.productIDorName !== '') { //item.productIDorName !== ''
                    product.widgetState = state;
                } else {
                    swal("Error!", "Item details cannot be empty", "error");
                }
            }

            $scope.AddOtherRequirementItem = function (nonCoreProduct) {
                var item = {};
                item = nonCoreProduct;
                if (item) {
                    var index = 0;
                    if (!$scope.formRequest.listRequirementItems[$scope.formRequest.listRequirementItems.length - 1].productCode) {
                        $scope.AddItem();
                        index = $scope.formRequest.listRequirementItems.length - 1;

                    } else {
                        $scope.AddItem();
                        index = $scope.formRequest.listRequirementItems.length - 1;
                    }

                    item.isNonCoreProductCategory = 1;
                    $scope.fillTextbox(item, index,'');
                }
            };

            $scope.deleteItem = function (product, isSubQuantity) {
              
                if ($scope.reqId && !$scope.isClone) {
                    $scope.formRequest.deleteQuotations = true;
                    $scope.formRequest.disableDeleteQuotations = true;
                }

                if (!$scope.isEdit || $scope.isClone) {
                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 1) {
                        $scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.productSNo !== product.productSNo; });

                        if (!isSubQuantity && !$scope.formRequest.isRFP) {
                            $scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.catalogueItemID !== product.catalogueItemID; });
                        }
                    }
                } else {
                    product.isDeleted = 1;
                    if (!isSubQuantity) {
                        var tempItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.catalogueItemID === product.catalogueItemID; });
                        if (tempItems && tempItems.length > 0) {
                            tempItems.forEach(function (tempItem, index) {
                                tempItem.isDeleted = 1;
                            });
                        }
                    }
                }

                $scope.nonCoreproductsList.forEach(function (item, index) {
                    if(item.prodNo === product.productNo) {
                        item.doHide = false;
                    }
                });
                
                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length === 1 
                    && $scope.formRequest.listRequirementItems[0].isNonCoreProductCategory === 1) {
                    location.reload();
                }

                $scope.postRequestLoding = false;
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                $scope.totalRequirementItemSize = 0;
                if (id != "itemsAttachment" && $scope.file) {
                    var index = _.indexOf($scope.formRequest.listRequirementItems, _.find($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == id; }));
                    if (index >= 0) {
                        var listItem = $scope.formRequest.listRequirementItems[index];
                        listItem.fileSize = $scope.file.size;

                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                            if (item.fileSize) {
                                $scope.totalRequirementItemSize = $scope.totalRequirementItemSize + item.fileSize;
                            }
                        });
                    }
                }

                if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "itemsAttachment") {
                            if (ext != "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.formRequest.itemsAttachment = $.makeArray(bytearray);
                            if (!$scope.isEdit) {
                                $scope.formRequest.listRequirementItems = [];
                            }

                            $scope.postRequest(0, 1, false);
                        }

                        if (id == "requirementItemsSveAttachment") {
                            if (ext != "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }

                            var bytearray = new Uint8Array(result);
                            $scope.uploadRequirementItemsSaveExcel($.makeArray(bytearray));
                            $scope.file = [];
                            $scope.file.name = '';
                            return;
                        }

                        if (id != "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.formRequest.listRequirementItems, _.find($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == id; }));
                            var obj = $scope.formRequest.listRequirementItems[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.formRequest.listRequirementItems.splice(index, 1, obj);
                        }
                    });
            }

            $scope.pageNo = 1;

            $scope.nextpage = function (pageNo) {
                $scope.tableValidation = false;
                $scope.tableValidationMsg = '';
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = departmentsValidation = $scope.incoTermValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.remindersTimeIntervalValidation = $scope.questionnaireValidation = false;

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = $scope.expNegotiationTimeValidation = false;

                
                if (pageNo == 1 || pageNo == 4) {
                    if ($scope.isEdit) {
                        $scope.INCO_TERMS.forEach(function (term, termIdx) {
                            if (term.configValue + '-' + term.configText === $scope.formRequest.generalTC) {
                                $scope.generalTC = term;
                            }
                        })
                    }
                    
                   
                    if ($scope.reqDepartments == null || $scope.reqDepartments == undefined || $scope.reqDepartments.length == 0) {
                        $scope.GetReqDepartments();
                    }
                    
                    if ($scope.currencies == null || $scope.currencies == undefined || $scope.currencies.length == 0) {
                        //$scope.getCurrencies();
                    }

                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    var requirementItems = $scope.formRequest.listRequirementItems.filter(function (item) {
                        return !item.isDeleted;
                    });

                    if (!requirementItems || requirementItems.length <= 0) {
                        swal("Verify", "Cannot create requirement with no items.", "error");
                        return false;
                    }

                    if ($scope.formRequest.isTabular) {
                        $scope.selectedProducts = [];
                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {

                            if ($scope.tableValidation || item.isDeleted) {
                                return false;
                            }

                            var sno = parseInt(index) + 1;

                            if (!$scope.formRequest.isRFP) {
                                if (item.productCode == null || item.productCode == '') {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter ' + $scope.prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL + ' for item: ' + sno;
                                    return;
                                }
                            }

                            if (item.isNonCoreProductCategory == 1) {

                            } else if ($scope.formRequest.isRFP) {
                                if ($scope.isEdit && item.isCoreProductCategory == 1) {
                                    if (item.VERTICAL_TYPE == null || item.VERTICAL_TYPE == '') {
                                        $scope.tableValidation = true;
                                        $scope.tableValidationMsg = 'Please select vertical type for item: ' + sno;
                                        return;
                                    }
                                } else {
                                    if ((item.VERTICAL_TYPE == null || item.VERTICAL_TYPE == '')) {
                                        $scope.tableValidation = true;
                                        $scope.tableValidationMsg = 'Please select vertical type for item: ' + sno;
                                        return;
                                    }
                                }
                               
                            }
                            
                            if (item.productIDorName == null || item.productIDorName == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter ' + $scope.prmFieldMappingDetails.PRODUCT_NAME.FIELD_LABEL + ' for item: ' + sno;
                                return;
                            }
                            else {
                                let productId = item.catalogueItemID ? item.catalogueItemID : item.productId;
                                $scope.selectedProducts.push(productId);
                                var productQuotationTemplateFiltered = _.filter(item.productQuotationTemplate, function (o) {
                                    return o.IS_VALID == 1;
                                });

                                if (productQuotationTemplateFiltered != null && productQuotationTemplateFiltered != undefined && productQuotationTemplateFiltered.length > 0) {
                                    item.productQuotationTemplateJson = JSON.stringify(productQuotationTemplateFiltered);
                                } else {
                                    item.productQuotationTemplateJson = '';
                                }
                            }

                            if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product ' + $scope.prmFieldMappingDetails.PRODUCT_QUANTITY.FIELD_LABEL + ' for item: ' + sno;
                                return;
                            }
                            if (item.productQuantityIn == null || item.productQuantityIn == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product ' + $scope.prmFieldMappingDetails.PRODUCT_UNITS.FIELD_LABEL + ' for item: ' + sno;
                                return;
                            }
                            if ($scope.formRequest.isSplitEnabled == true) {
                                if (item.fromrange == null || item.fromrange == '' || item.fromrange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter From Range for item: ' + sno;
                                    return;
                                }
                                if (item.torange == null || item.torange == '' || item.torange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter To Range for item: ' + sno;
                                    return;
                                }

                                if (parseFloat(item.fromrange) > parseFloat(item.torange)) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please make sure that To Range is higher than From Range for item: ' + sno;
                                    return;
                                }
                            }
                        });

                        if ($scope.tableValidation) {
                            return false;
                        }

                    }
                    if (!$scope.formRequest.isTabular) {
                        if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                            $scope.descriptionValidation = true;
                            $scope.postRequestLoding = false;
                            return false;
                        }
                    }
                     

                    var phn = '';
                    if ($scope.userObj && $scope.userObj.phoneNum)
                    {
                        phn = $scope.userObj.phoneNum;
                        $scope.formRequest.phoneNum = phn;
                        $scope.formRequest.contactDetails = phn;
                    }
                    else {
                        $scope.formRequest.phoneNum = '';
                        $scope.formRequest.contactDetails = '';
                    }

                                       

                }
                
                if (pageNo == 2 || pageNo == 4) {
                    if (!$scope.questionnaireList == null || $scope.questionnaireList.length == 0) {
                        //$scope.getQuestionnaireList(); //Enable on Need basis
                    }


                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        var d = new Date();
                        d.setHours(18, 00, 00);
                        d = new moment(d).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.quotationFreezTime = d;
                        
                    }

                    
                    if ($scope.formRequest.expStartTime == null || $scope.formRequest.expStartTime == '') {
                        var dt = new Date();
                        var tomorrowNoon = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 1, 12, 0, 0);
                        tomorrowNoon = new moment(tomorrowNoon).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.expStartTime = tomorrowNoon;

                    }

                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.formRequest.noOfQuotationReminders = 3;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.formRequest.remindersTimeInterval = 2;
                    }                    

                    $scope.incoTermValidation = false;
                    if (!$scope.generalTC) {
                        $scope.incoTermValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if ($scope.reqDepartments.length > 0) {

                        $scope.departmentsValidation = true;

                        $scope.reqDepartments.forEach(function (item, index) {
                            if (item.isValid == true)
                                $scope.departmentsValidation = false;
                        });

                        if ($scope.departmentsValidation == true) {
                            return false;
                        };
                    }
                }

                if (pageNo == 3 || pageNo == 4) {
                    $scope.GetRequirementVendorAnalysis();
                    $scope.GetRequirementVendorCodes();
                    $scope.getproductvendors();
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    

                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= 0) && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    var ts = moment($scope.formRequest.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var quotationFreezTime = new Date(m);

                    var ts = moment($scope.formRequest.expStartTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var expStartTime = new Date(m);

                    auctionsService.getdate()
                        .then(function (GetDateResponse) {
                            var CurrentDateToLocal = userService.toLocalDate(GetDateResponse);
                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";
                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));
                            $scope.currentSystemDate = CurrentDate;
                            if ($scope.formRequest.biddingType === 'SPOT') {
                                quotationFreezTime = new Date(moment(moment(moment(), "DD-MM-YYYY HH:mm").valueOf()));
                            }

                            if (quotationFreezTime < CurrentDate) {
                                $scope.quotationFreezTimeValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                            else
                                if (quotationFreezTime >= expStartTime) {
                                    $scope.expNegotiationTimeValidation = true;
                                    $scope.postRequestLoding = false;
                                    return false;
                                }

                            if ($scope.selectedCurrency) {
                                let selecCurrencyTemp = _.filter($scope.companyCurrencies, function (currItem) { return currItem.currency === $scope.selectedCurrency.value; });
                                if (selecCurrencyTemp && selecCurrencyTemp.length > 0) {
                                    if (quotationFreezTime > selecCurrencyTemp[0].StartTimeLocal && expStartTime > selecCurrencyTemp[0].StartTimeLocal &&
                                        expStartTime <= selecCurrencyTemp[0].EndTimeLocal && expStartTime <= selecCurrencyTemp[0].EndTimeLocal) {

                                    } else {

                                        swal({
                                            title: "Error!",
                                            title: "Selected currency conversion rate has expired, please validate.",
                                            type: "error",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                return false;
                                            });

                                        return false;
                                    }
                                }
                            }
                            
                            $scope.pageNo = $scope.pageNo + 1;
                        });

                }

                if (pageNo == 4) {
                    $scope.formRequest.subcategoriesView = '';
                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategoriesView += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }
                    $scope.pageNo = $scope.pageNo + 1;
                }

                if (pageNo != 3) {
                    $scope.pageNo = $scope.pageNo + 1;
                }
            }

            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;
            }

            $scope.gotopage = function (pageNo) {
                $scope.pageNo = pageNo;

                
                return $scope.pageNo;
            }

            $scope.reqDepartments = [];
            $scope.userDepartments = [];
            $scope.GetReqDepartments = function () {
                $scope.reqDepartments = [];
                auctionsService.GetReqDepartments(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDepartments = response;

                            $scope.noDepartments = true;

                            $scope.reqDepartments.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });

                            if ($scope.reqDepartments && $scope.reqDepartments.length > 0 && $scope.reqDepartments.length === 1) {
                                $scope.reqDepartments[0].isValid = true;
                            }
                        }
                    });
            }

            $scope.SaveReqDepartments = function (reqID) {

                $scope.reqDepartments.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDepartments,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                } else {
                    auctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                }
            };

            $scope.noDepartments = false;


            $scope.noDepartmentsFunction = function (value) {
                $scope.departmentsValidation = false;
                if (value == 'NA') {
                    $scope.reqDepartments.forEach(function (item, index) {
                        item.isValid = false;
                    });
                }
                else {
                    $scope.reqDepartments.forEach(function (item, index) {
                        if (item.isValid == true)
                            $scope.noDepartments = false;
                    });
                }
            };

            $scope.reqDeptDesig = [];
            $scope.GetReqDeptDesig = function () {
                $scope.reqDeptDesig = [];
                auctionsService.GetReqDeptDesig(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDeptDesig = response;
                            $scope.noDepartments = true;
                            $scope.reqDeptDesig.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });
                        }
                    });
            };

            $scope.SaveReqDeptDesig = function (reqID) {
                $scope.reqDeptDesig.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDeptDesig,
                    sessionID: userService.getUserToken()
                };

                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDeptDesig(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });
                } else {
                    auctionsService.SaveReqDeptDesig(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });
                }
            };

            $scope.AssignVendorToCompany = function (vendorPhone, vendorEmail) {

                if ($scope.newVendor.altPhoneNum == undefined) {
                    $scope.newVendor.altPhoneNum = '';
                }
                if ($scope.newVendor.altEmail == undefined) {
                    $scope.newVendor.altEmail = '';
                }

                var params = {
                    userID: userService.getUserId(),
                    vendorPhone: vendorPhone,
                    vendorEmail: vendorEmail,
                    sessionID: userService.getUserToken(),
                    category: $scope.formRequest.category,
                    subCategory: $scope.selectedSubCategoriesList,
                    altPhoneNum: $scope.newVendor.altPhoneNum,
                    altEmail: $scope.newVendor.altEmail
                };

                auctionsService.AssignVendorToCompany(params)
                    .then(function (response) {

                        $scope.vendor = response;

                        if ($scope.vendor.errorMessage == '' || $scope.vendor.errorMessage == 'VENDOR ADDED SUCCESSFULLY') {
                            $scope.formRequest.auctionVendors.push({ vendorName: $scope.vendor.firstName + ' ' + $scope.vendor.lastName, companyName: $scope.vendor.email, vendorID: $scope.vendor.userID });
                            $scope.GetRequirementVendorAnalysis();
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl('Vendor Added Successfully', "success");
                        }
                        else {
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl($scope.vendor.errorMessage, "inverse");
                        }
                    });
            };

            $scope.searchvendorstring = '';
            $scope.searchVendors = function (value) {
                $scope.Vendors = _.filter($scope.VendorsTemp, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
            };

            $scope.exportItemsToExcel = function () {
                var name = 'REQUIREMENT_SAVE';
                reportingService.downloadTemplate(name, userService.getUserId(), $scope.stateParamsReqID, ($scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0));
            };

            $scope.cancelClick = function () {
                $window.history.back();
            };

            $scope.paymentRadio = false;
            $scope.paymentlist = [];
            $scope.addpaymentvalue = function () {
                var listpaymet =
                    {
                        reqTermsID: 0,
                        reqID: $scope.stateParamsReqID,
                        userID: userService.getUserId(),
                        reqTermsDays: 0,
                        reqTermsPercent: 0,
                        reqTermsType: 'PAYMENT',
                        paymentType: '+',
                        isRevised: 0
                    };
                $scope.paymentlist.push(listpaymet);
            };

            $scope.delpaymentvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }

                $scope.paymentlist.splice(val, 1);
            };

            $scope.resetpayment = function () {
                $scope.paymentlist = [];
            };

            $scope.deliveryRadio = false;
            $scope.deliveryList = [];

            $scope.adddeliveryvalue = function () {
                var deliveryObj =
                    {
                        reqTermsID: 0,
                        reqID: $scope.stateParamsReqID,
                        userID: userService.getUserId(),
                        reqTermsDays: 0,
                        reqTermsPercent: 0,
                        reqTermsType: 'DELIVERY',
                        isRevised: 0
                    };
                $scope.deliveryList.push(deliveryObj);
            };

            $scope.deldeliveryvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }
                $scope.deliveryList.splice(val, 1);
            };

            $scope.resetdelivery = function () {
                $scope.deliveryList = [];
            };

            $scope.listRequirementTerms = [];
            $scope.SaveRequirementTerms = function (reqID) {
                $scope.listRequirementTerms = [];
                $scope.deliveryList.forEach(function (item, index) {
                    item.reqID = reqID;
                    $scope.listRequirementTerms.push(item);
                });

                $scope.paymentlist.forEach(function (item, index) {

                    item.reqID = reqID;

                    if (item.paymentType == '-') {
                        item.reqTermsDays = -(item.reqTermsDays);
                    }

                    $scope.listRequirementTerms.push(item);
                });

                var params = {
                    "listRequirementTerms": $scope.listRequirementTerms,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });
                } else {
                    auctionsService.SaveRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });
                }
            };

            $scope.GetRequirementTerms = function () {
                auctionsService.GetRequirementTerms(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;

                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });
                        }
                    });
            }

            $scope.listTerms = [];
            $scope.DeleteRequirementTerms = function () {
                var params = {
                    "listTerms": $scope.listTerms,
                    sessionID: userService.getUserToken()
                };

                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                } else {
                    auctionsService.DeleteRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                }
            };

            $scope.paymentTypeFunction = function (type) {
                if (type == 'ADVANCE') {

                }
                if (type == 'POST') {

                }
            }

            $scope.GetCIJList = function () {
                auctionsService.GetCIJList($scope.compID, $scope.sessionid)
                    .then(function (response) {
                        $scope.cijList = response;

                        $scope.cijList.forEach(function (item, index) {
                            var cijObj = $.parseJSON(item.cij);
                            item.cij = cijObj;
                        });


                    });
            }

            $scope.GetIndentList = function () {
                auctionsService.GetIndentList(userService.getUserCompanyId(), userService.getUserToken())
                    .then(function (response) {
                        $scope.indentList = response;
                    })
            }

            $scope.changeIndent = function (val) {
            };

            $scope.formRequest.category = '';

            $scope.goToStage = function (stage, currentStage) {
                $scope.postRequestLoding = true;
                $scope.tableValidation = $scope.validationFailed = false;
                if (stage == 2 && currentStage == 1) {

                }
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                        $scope.tableValidation = true;
                        $scope.validationFailed = true;
                    }
                });
                if ($scope.tableValidation) {
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                    $scope.titleValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if (currentStage == 2 && stage > currentStage) {
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if (!$scope.generalTC) {
                        $scope.incoTermValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (currentStage == 3 && stage > currentStage) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    
                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }

                    if ($scope.formRequest.noOfQuotationReminders > 0)
                    {
                        $scope.formRequest.noOfQuotationReminders = parseInt($scope.formRequest.noOfQuotationReminders);
                    }
                    if ($scope.formRequest.remindersTimeInterval > 0) {
                        $scope.formRequest.remindersTimeInterval = parseInt($scope.formRequest.remindersTimeInterval);
                    }


                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (!$scope.postRequestLoding) {
                    return false;
                } else {
                    $scope.postRequestLoding = true;
                    var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000+0530)/";
                    var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";
                    $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                    $scope.formRequest.customerID = userService.getUserId();
                    $scope.formRequest.customerFirstname = userService.getFirstname();
                    $scope.formRequest.customerLastname = userService.getLastname();
                    $scope.formRequest.isClosed = "NOTSTARTED";
                    $scope.formRequest.endTime = "";
                    $scope.formRequest.sessionID = userService.getUserToken();
                    $scope.formRequest.subcategories = "";
                    $scope.formRequest.budget = 100000;
                    $scope.formRequest.custCompID = userService.getUserCompanyId();
                    $scope.formRequest.customerCompanyName = userService.getUserCompanyId();
                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }

                    var category = [];
                    if ($scope.formRequest.category) {
                        category.push($scope.formRequest.category);
                    }

                    $scope.formRequest.category = category;
                    $scope.formRequest.cloneID = 0;
                    if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                        $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                        if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIndexs) {
                                item.ceilingPrice = 0;
                            });
                        }
                    }

                    $scope.formRequest.templateId = $scope.selectedTemplate.TEMPLATE_ID;
                    auctionsService.postrequirementdata($scope.formRequest)
                        .then(function (response) {
                            if (response.objectID != 0) {
                                if ($scope.selectedProject) {
                                    $scope.selectedProject.sessionID = userService.getUserToken();
                                    $scope.selectedProject.requirement = {};
                                    $scope.selectedProject.requirement.requirementID = response.objectID;
                                    var param = { item: $scope.selectedProject };
                                }

                                $scope.saveRequirementSetting(response.objectID, 'TEMPLATE_ID', $scope.selectedTemplate.TEMPLATE_ID);
                                $scope.SaveReqDepartments(response.objectID);
                                if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                    techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                        .then(function (response1) {
                                            $scope.selectedQuestionnaire = response1;
                                            if ($scope.isTechEval) {
                                                $scope.selectedQuestionnaire.reqID = response.objectID;
                                            }
                                            else {
                                                $scope.selectedQuestionnaire.reqID = 0;
                                            }
                                            $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                            var params = {
                                                questionnaire: $scope.selectedQuestionnaire
                                            }
                                            techevalService.assignquestionnaire(params)
                                                .then(function (response) {
                                                })
                                        })
                                }
                                if (stage) {
                                    $state.go('save-requirement', { Id: response.objectID });
                                }
                            }
                        });
                }
                //$scope.postRequest(0, stage - 1, false, stage);                
            }

            $scope.goBack = function (stage, reqID) {
                $state.go('save-requirement', { Id: reqID });
            }

            $scope.removeAttach = function (index) {
                $scope.formRequest.multipleAttachments.splice(index, 1);
            }

            // Pagination For User Products//
                $scope.userProductsPage = 0;
                $scope.userProductsPageSize = 200;
                $scope.userProductsfetchRecordsFrom = $scope.userProductsPage * $scope.userProductsPageSize;
                $scope.userProductstotalCount = 0;
            // Pagination For User Products//

            $scope.productsList = [];
            $scope.nonCoreproductsList = [];
            $scope.getUserProducts = function (IsPaging, searchString, index, fieldType, field) {

                if ($scope.productsList.length <= 0 && !searchString) {
                    if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0) {
                        $scope.nonCoreproductsList.forEach(function (product, index) {
                            if (!$scope.isEdit) {
                                product.doHide = true;
                                $scope.AddOtherRequirementItem(product);
                            }
                        });
                    }

                } else {
                    let isNameSearch = false;
                    if (fieldType === 'NAME') {
                        isNameSearch = true;
                    }

                    catalogService.getUserProducts($scope.userProductsfetchRecordsFrom, $scope.userProductsPageSize, searchString ? encodeURIComponent(searchString) : "", $scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS, $scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD, isNameSearch).then(function (response) {
                        $scope.productsList = response;
                        $scope.userproductsLoading = false;
                        $scope.fillingProduct($scope.productsList, index, fieldType, searchString, $scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS, $scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD);
                    });
                }
            };

            // in form catalogue ctrl
            $scope.fillTextbox = function (selProd, index, field, field_1) {
                if ($scope.reqId) {
                    $scope.formRequest.deleteQuotations = true;
                    $scope.formRequest.disableDeleteQuotations = true;
                }

                let existingCatalogItemId = 0;
                if ($scope.formRequest.listRequirementItems[index].catalogueItemID) {
                    existingCatalogItemId = $scope.formRequest.listRequirementItems[index].catalogueItemID;
                }

                if (selProd.CONTRACT_VENDOR > 0) {
                    $scope.contractPRItems.push({
                        CONTRACT_VENDOR: selProd.CONTRACT_VENDOR,
                        PRODUCT_ID: selProd.prodId
                    });

                    swal({
                        title: "Pending Contract Items found in PR",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Continue With Requirement",
                        cancelButtonText: "Show Contracts",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                        function (isConfirm) {
                            if (!isConfirm) {
                                $scope.showContractProducts($scope.contractPRItems);
                            }
                        });
                }

                $scope['ItemSelected_' + index] = true;
                $scope.formRequest.listRequirementItems[index].productIDorName = selProd.prodName;
                $scope.formRequest.listRequirementItems[index].catalogueItemID = selProd.prodId;
                $scope.formRequest.listRequirementItems[index].isCoreProductCategory = selProd.isCoreProductCategory;

                $scope.formRequest.listRequirementItems[index].hsnCode = selProd.prodHSNCode;
                $scope.formRequest.listRequirementItems[index].productDescription = selProd.prodDesc;
                $scope.formRequest.listRequirementItems[index].productCode = selProd.prodCode;
                $scope.formRequest.listRequirementItems[index].productNo = selProd.prodNo;
                if ($scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS) {
                    $scope.formRequest.listRequirementItems[index].productCode = selProd.casNumber;
                }
                if ($scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD) {
                    $scope.formRequest.listRequirementItems[index].productNo = selProd.mfcdCode;
                }
                

                $scope.formRequest.listRequirementItems[index].productQuantityIn = selProd.prodQty;
                if (selProd.isNonCoreProductCategory) {
                    $scope.formRequest.listRequirementItems[index].productQuantity = 1;
                }
                $scope.formRequest.listRequirementItems[index].isNonCoreProductCategory = selProd.isNonCoreProductCategory;
                $scope.formRequest.listRequirementItems[index].CategoryId = selProd.categoryId;
                $scope.formRequest.listRequirementItems[index].itemLastPriceArr = [];
                $scope.formRequest.listRequirementItems[index].showTable = false;
                $scope['filterProducts_' + index] = null;
                $scope["filterProducts_Code_" + index] = null;
                $scope["filterProducts_Code1_" + index] = null;

                if (!selProd.isNonCoreProductCategory || selProd.isCoreProductCategory) {
                    $scope.GetProductQuotationTemplate(selProd.prodId, index, true);
                } else {
                    $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                        if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                    });
                }

                if (existingCatalogItemId && existingCatalogItemId !== selProd.prodId) {
                    if (!$scope.isEdit) {
                        if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 1) {
                            $scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.catalogueItemID !== existingCatalogItemId; });
                        }
                    } else {
                        var tempItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.catalogueItemID === existingCatalogItemId; });
                        if (tempItems && tempItems.length > 0) {
                            tempItems.forEach(function (tempItem, index) {
                                tempItem.isDeleted = 1;
                            });
                        }
                    }
                }
            };

            $scope.autofillProduct = function (prodName, index, fieldType, field) {
                if (fieldType === 'CODEMAIN' || fieldType === 'NAME' || fieldType === 'CODE') {
                    $scope.formRequest.productCodeSearch = (fieldType === 'CODEMAIN');
                    $scope.formRequest.productNameSearch = (fieldType === 'NAME');
                    $scope.formRequest.productNoSearch = (fieldType === 'CODE');
                    $window.onclick = function (event) {
                        $scope.formRequest.productCodeSearch = false;
                        $scope.formRequest.productNameSearch = false;
                        $scope.formRequest.productNoSearch = false;
                        $window.onclick = null;
                        $scope.$apply();
                    };
                }


                $scope['ItemSelected_' + index] = false;
                var output = [];
                if (prodName && prodName != '' && prodName.length > 0) {
                    $scope.searchingUserProducts = angular.lowercase(prodName);
                    $scope.userproductsLoading = true;
                    $scope.getUserProducts(0, $scope.searchingUserProducts, index, fieldType,field);
                }
            }

            $scope.onBlurProduct = function (index) {
                if ($scope['ItemSelected_' + index] == false) {
                    if (!$scope.formRequest.isRFP)
                    {
                        $scope.formRequest.listRequirementItems[index].productIDorName = "";
                        $scope.formRequest.listRequirementItems[index].productNo = "";
                        $scope.formRequest.listRequirementItems[index].productCode = "";
                    }

                }
                //$scope['filterProducts_' + index] = null;
            }

            $scope.formSplitChange = function () {
                var splitenabled = $scope.formRequest.isSplitEnabled;
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    item.splitenabled = splitenabled;
                    if (splitenabled) {
                        item.productQuantity = 1;
                        $scope.formRequest.isDiscountQuotation = 0;
                    }
                });
            }

            $scope.onSplitChange = function (index) {
                if ($scope.formRequest.listRequirementItems[index].splitenabled == true) {
                    $scope.formRequest.listRequirementItems[index].productQuantity = 1;
                }
            }


            $scope.fillingProduct = function (output, index, fieldType, searchString) {

                output = $scope.productsList.filter(function (product) {                   
                    return product.isCoreProductCategory === 1;
                });


                if (fieldType == 'NAME') {
                    $scope["filterProducts_" + index] = output;
                   // $scope.formRequest.listRequirementItems[index].productIDorName = searchString;
                }
                else if (fieldType == 'CODE') {
                    $scope["filterProducts_Code_" + index] = output;
                   // $scope.formRequest.listRequirementItems[index].productNo = searchString;
                }
                else if (fieldType == 'CODEMAIN') {
                    $scope["filterProducts_Code1_" + index] = output;
                   // $scope.formRequest.listRequirementItems[index].productCode = searchString;
                }
            }


            $scope.getCatalogCategories = function () {
                auctionsService.getcatalogCategories(userService.getUserId())
                    .then(function (response) {
                        $scope.companyCatalogueList = response;

                        $scope.catName = $scope.companyCatalogueList[0];
                        $scope.searchCategoryVendors($scope.catName);

                    });
            };

            $scope.searchCategoryVendors = function (str) {
                //var filterText = angular.lowercase(str);
                //$scope.getvendors(filterText);
                $scope.getvendors(str);

            };

            $scope.selectCompanyVendors = [];

            $scope.getproductvendors = function () {
                $scope.selectedProducts = [];
                $scope.Vendors = [];

                    var params = { 'products': $scope.selectedProducts, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendorsbyproducts',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                // filter vendors who has contracts
                                if ($scope.contractPRItems && $scope.contractPRItems.length > 0) {
                                    let filteredVendors = _.filter(response.data, function (venObj) {
                                        return _.findIndex($scope.contractPRItems, (item) => { return item.CONTRACT_VENDOR === venObj.vendorID }) < 0;
                                    });

                                    response.data = filteredVendors;
                                }

                                $scope.Vendors = response.data;

                                if (type === 'REQUIREMENT' && vendorId > 0) {
                                    var filteredFromAddVendor = $scope.Vendors.filter(function (allVendors) {
                                        return vendorId === allVendors.vendorID;
                                    });
                                    if (filteredFromAddVendor && filteredFromAddVendor.length > 0) {
                                        $scope.selectForA(filteredFromAddVendor[0]);
                                    }
                                }


                                $scope.selectCompanyVendors = response.data;
                                $scope.vendorsLoaded = true;
                                $scope.formRequest.auctionVendors.forEach(function (actionVendor, index) {
                                    var filteredVendor = $scope.Vendors.filter(function (allVendor) {
                                        return actionVendor.vendorID === allVendor.vendorID;
                                    });

                                    if (filteredVendor && filteredVendor.length > 0) {
                                        filteredVendor[0].isSelected = true;
                                    }
                                });

                                var unSelectedVendors = _.filter($scope.Vendors, function (x) { return !x.isSelected; });
                                if (unSelectedVendors && unSelectedVendors.length > 0) {
                                    $scope.selectAllVendors = false;
                                }

                                $scope.VendorsTemp = $scope.Vendors;
                                if ($scope.searchvendorstring != '') {
                                    $scope.searchVendors($scope.searchvendorstring);
                                } else {
                                    $scope.searchVendors('');
                                }
                            }
                            //$scope.getvendorswithoutcategories();
                            $scope.getCatalogCategories();
                        } else {
                        }
                    }, function (result) {
                    });
                //}

                //$scope.getCatalogCategories();

            };

            $scope.getCurrencies();
            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                            }
                        });
                    });
            };
            //form Ctrl
           // $scope.getWorkflows();

            $scope.getItemWorkflow = function () {
                workflowService.getItemWorkflow(0, $stateParams.Id, $scope.WorkflowModule)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                                if (track.status == 'APPROVED' || track.status == 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status == 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }



                                if (track.status == 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });

            };

            $scope.updateTrack = function (step, status) {

                $scope.commentsError = '';

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            //location.reload();
                            //     $state.go('list-pr');
                        }
                    })
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.formRequest.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    })
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                                disable = true;
                            }
                            else if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                })

                return disable;
            };

            /*region end WORKFLOW*/

            $scope.getPRNumber = function (pr_ID) {
                var params = {
                    "prid": pr_ID,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {
                        $scope.PRDetails = response;
                        if ($scope.PRDetails.PR_ID == pr_ID) {
                            //$scope.formRequest.PR_ID = $scope.PRDetails.PR_ID;
                            $scope.formRequest.PR_NUMBER = $scope.PRDetails.PR_NUMBER;
                        }
                    })
            };

            $scope.prConsolidatedItems = [];
            $scope.fillReqItems = function (pr, type) {
                if (type === 'PR') {
                    $scope.formRequest.PR_ID = pr.PR_ID;
                    if (pr.isSelected) {
                        $scope.GetPRItemsList(pr, type);
                    } else if (!pr.isSelected) {
                        $scope.GetPRItemsList(pr, type);
                    }
                } else {
                    $scope.GetPRItemsList(pr, type);
                }
            };
            function groupBy(list, keyGetter) {
                const map = new Map();
                list.forEach((item) => {
                    const key = keyGetter(item);
                    const collection = map.get(key);
                    if (!collection) {
                        map.set(key, [item]);
                    } else {
                        collection.push(item);
                    }
                });
                return map;
            }
            $scope.prDeleteItemsArr = [];
            $scope.GetPRItemsList = function (pr, type) {
                if (type === 'PR') {
                    if (pr && pr.PR_ID > 0) {
                        var params = {
                            "prid": pr.PR_ID,
                            "sessionid": userService.getUserToken()
                        }
                        
                        PRMPRServices.getpritemslist(params)
                            .then(function (response) {
                                $scope.PRItemsList = response;
                                //---------------------28
                                $scope.TempPRItems = [];
                                $scope.TempPRItems1 = [];
                                var isService = false;
                                for (let i = 0; i < $scope.PRItemsList.length; i++) {
                                    $scope.PRItemsList[i].REQUIRED_QUANTITY_TEMP = 0;
                                    for (let j = i ; j < $scope.PRItemsList.length; j++) {
                                        if ($scope.PRItemsList[i].ProductId == $scope.PRItemsList[j].ProductId && ($scope.PRItemsList[i].CategoryName === "Service Group" || $scope.PRItemsList[i].CategoryName === "Services" || $scope.PRItemsList[i].CategoryName === "Assets Group")) {
                                            isService = true;
                                            $scope.TempPRItems = groupBy($scope.PRItemsList, item => item.ITEM_CODE_COMBINATION);

                                        }
                                    }
                                }
                                $scope.TempPRItems.forEach(function (arrObj) {
                                    if (arrObj.length > 1) {
                                        
                                        arrObj[0].PR_ITEM_ID_STRING_arr = [];
                                        arrObj.forEach(function (O) {
                                            var obj = {
                                                isChecked: false,
                                                SERVICE_LINE_ITEM: O.SERVICE_LINE_ITEM,
                                                ITEM_NUM: O.ITEM_NUM,
                                                ITEM_ID: O.ITEM_ID,
                                                REQUIRED_QUANTITY: O.REQUIRED_QUANTITY
                                            };
                                            arrObj[0].PR_ITEM_ID_STRING_arr.push(obj);
                                        })
                                        arrObj[0].REQUIRED_QUANTITY = _.sumBy(arrObj, 'REQUIRED_QUANTITY');
                                        arrObj[0].PR_ITEM_ID_STRING = JSON.stringify(arrObj[0].PR_ITEM_ID_STRING_arr);
                                        //arrObj[0].PR_ITEM_ID_STRING = _.chain(arrObj)
                                        //    .map((n) => {
                                        //        return n.ITEM_NUM;
                                        //    })
                                        //    .join(',')
                                        //    .value();
                                        //arrObj[0].SERV_LINE_ITEM_STRING = _.chain(arrObj)
                                        //    .map((n) => {
                                        //        return n.SERVICE_LINE_ITEM;
                                        //    })
                                        //    .join(',')
                                        //    .value();
                                        $scope.TempPRItems1.push(arrObj[0]);
                                    } else if (arrObj.length = 1) {
                                        arrObj[0].PR_ITEM_ID_STRING_arr = [];
                                        arrObj.forEach(function (O) {
                                            var obj = {
                                                isChecked: false,
                                                SERVICE_LINE_ITEM: O.SERVICE_LINE_ITEM,
                                                ITEM_NUM: O.ITEM_NUM,
                                                ITEM_ID: O.ITEM_ID,
                                                REQUIRED_QUANTITY: O.REQUIRED_QUANTITY
                                            };
                                            arrObj[0].PR_ITEM_ID_STRING_arr.push(obj);
                                        })
                                        arrObj[0].PR_ITEM_ID_STRING = JSON.stringify(arrObj[0].PR_ITEM_ID_STRING_arr);
                                        //arrObj[0].PR_ITEM_ID_STRING = _.chain(arrObj)
                                        //    .map((n) => {
                                        //        return n.ITEM_NUM;
                                        //    })
                                        //    .join(',')
                                        //    .value();
                                        //arrObj[0].SERV_LINE_ITEM_STRING = _.chain(arrObj)
                                        //    .map((n) => {
                                        //        return n.SERVICE_LINE_ITEM;
                                        //    })
                                        //    .join(',')
                                        //    .value();
                                        $scope.TempPRItems1.push(arrObj[0]);
                                    }
                                })
                                //---------------------28

                                //Filter only selected PRs


                                let selectedPRItems = _.filter($scope.PRItemsList, function (prItem) {
                                    return _.findIndex(pr.PRItems, function (prObjItem) {
                                        return prObjItem.isSelected && prObjItem.ITEM_ID === prItem.ITEM_ID;
                                    }) >= 0;
                                });
                                //---------------------28
                                if (isService) {
                                    $scope.PRItemsList = $scope.TempPRItems1;
                                } else {
                                    $scope.PRItemsList = selectedPRItems;
                                }
                                //$scope.PRItemsList = selectedPRItems;
                                //---------------------28

                                //Map CAS or MFCD
                                $scope.PRItemsList.forEach(function (prItem, idx) {
                                    let temp = _.filter(pr.PRItems, function (prItem1) {
                                        return prItem1.isSelected && prItem1.ITEM_ID === prItem.ITEM_ID;
                                    });
                                   
                                    selectedItemIds.push(prItem.ITEM_ID.toString());
                                    
                                    if (temp && temp.length > 0) {
                                        prItem.ITEM_CODE = temp[0].ITEM_CODE;
                                        prItem.ITEM_NUM = temp[0].ITEM_NUM;
                                    }
                                });

                                $scope.prDeleteItemsArr = [];

                                if (!pr.isSelected) {
                                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                        $scope.PRItemsList.forEach(function (prItem, prIndex) {
                                            $scope.formRequest.listRequirementItems.forEach(function (rfqItem, rfqIndex) {
                                                if (prItem.PRODUCT_ID == rfqItem.productId) {
                                                    var deductQuantity = rfqItem.productQuantity - prItem.REQUIRED_QUANTITY;
                                                    rfqItem.productQuantity = deductQuantity;
                                                    if (rfqItem.productQuantity <= 0) {
                                                        $scope.prDeleteItemsArr.push(rfqItem);
                                                    }
                                                } else {
                                                    if (prItem.PR_ID == rfqItem.PR_ID) {
                                                        $scope.prDeleteItemsArr.push(rfqItem);
                                                    }
                                                }
                                            });
                                        });

                                        if ($scope.prDeleteItemsArr && $scope.prDeleteItemsArr.length > 0) {
                                            $scope.prDeleteItemsArr.forEach(function (item, index) {
                                                $scope.deleteItem(item);
                                            });
                                        }
                                        return;
                                    }
                                }

                                // First check whether this PR exists in requirement items or not if exists then reduce the quantity and return the item//


                                
                                var validPRItems = $scope.PRItemsList.filter(function (prItem) {
                                    return (!prItem.REQ_ID || prItem.REQ_ID <= 0);
                                });

                                if (!validPRItems || validPRItems.length <= 0) {
                                    swal("Warning!", "All items in PR already linked to existing Requirement.", "error");
                                }

                                $scope.contractPRItems = [];
                                $scope.contractPRItems = $scope.PRItemsList.filter(function (prItem) {
                                    return prItem.CONTRACT_VENDOR > 0;
                                });

                                // filter vendors who has contracts
                                if ($scope.contractPRItems && $scope.contractPRItems.length > 0) {
                                    swal({
                                        title: "Pending Contract Items found in PR",
                                        text: "",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Continue With Requirement",
                                        cancelButtonText: "Show Contracts",
                                        closeOnConfirm: true,
                                        closeOnCancel: true
                                    },
                                        function (isConfirm) {
                                            if (!isConfirm) {
                                                $scope.showContractProducts($scope.contractPRItems);
                                            }
                                        });
                                }

                                $scope.PRItemsList.forEach(function (item, index) {
                                    let isServicePRItem = false;
                                    if (!item.REQ_ID || item.REQ_ID <= 0) {
                                        if (item.PR_ITEM_TYPE && (item.PR_ITEM_TYPE === 'SERVICE GROUP' || item.PR_ITEM_TYPE === 'SERVICES' || item.PR_ITEM_TYPE === 'Assets Group')) {
                                            isServicePRItem = true;
                                        }
                                        if (!$scope.formRequest.projectName || $scope.formRequest.projectName.indexOf(pr.PROJECT_TYPE + ';') < 0) {
                                            if (pr.PROJECT_TYPE) {
                                               // $scope.formRequest.projectName = $scope.formRequest.projectName + pr.PROJECT_TYPE + ';';
                                            }
                                        }

                                        if (!$scope.formRequest.deliveryLocation || $scope.formRequest.deliveryLocation.indexOf(pr.PLANT_NAME + ';') < 0) {
                                            if (pr.PLANT_NAME) {
                                                $scope.formRequest.deliveryLocation = $scope.formRequest.deliveryLocation + pr.PLANT_NAME + ';';
                                            }
                                        }

                                        if (pr.REQUISITIONER_EMAIL) {
                                            if (!_.some($scope.formRequest.customerEmails, { 'mail': pr.REQUISITIONER_EMAIL, 'userID': 0, 'status': 1 })) {
                                                if (!$scope.formRequest.customerEmails) {
                                                    $scope.formRequest.customerEmails = [];
                                                }

                                                $scope.formRequest.customerEmails.push({ 'mail': pr.REQUISITIONER_EMAIL, 'userID': 0, 'status': 1 });
                                            }
                                        }

                                        if (item.DEPT_CODE) {
                                            var filterdDept = _.filter($scope.reqDepartments, function (o) {
                                                return o.deptCode == item.DEPT_CODE;
                                            });

                                            if (filterdDept && filterdDept.length > 0) {
                                                filterdDept.isValid = true;
                                            }
                                        }

                                        $scope.requirementItems =
                                        {
                                            productSNo: ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length) ? index + 1 : $scope.formRequest.listRequirementItems.length + 1,
                                            ItemID: 0,
                                            productIDorName: item.ITEM_NAME,
                                            productNo: '',//item.ITEM_NUM,
                                            hsnCode: item.HSN_CODE,
                                            productDescription: ((item.ITEM_DESCRIPTION ? item.ITEM_DESCRIPTION : '') + (item.ITEM_DESC ? ', ' + item.ITEM_DESC : '') + (item.ITEM_TEXT ? ', ' + item.ITEM_TEXT : '') + (item.PROJECT_TYPE ? ', ' + item.PROJECT_TYPE : '')).trim(),
                                            productQuantity: item.REQUIRED_QUANTITY,
                                            productQuantityIn: item.UOM ? item.UOM : item.UNITS,
                                            productBrand: item.BRAND,
                                            othersBrands: '',
                                            isDeleted: 0,
                                            productImageID: item.ATTACHMENTS,
                                            attachmentName: '',
                                            //budgetID: 0,
                                            //bcInfo: '',
                                            productCode: item.ITEM_CODE,
                                            splitenabled: 0,
                                            fromrange: 0,
                                            torange: 0,
                                            requiredQuantity: 0,
                                            I_LLP_DETAILS: "",
                                            itemAttachment: "",
                                            itemMinReduction: 0,
                                            productId: item.PRODUCT_ID,
                                            PR_ID: item.PR_ID,
                                            PR_ITEM_ID: item.ITEM_ID,
                                            PR_QUANTITY: item.REQUIRED_QUANTITY,
                                            isCoreProductCategory: 1,
                                            catalogueItemID: item.PRODUCT_ID,
                                            VERTICAL_TYPE: item.VERTICAL_TYPE,
                                            PR_ITEM_ID_STRING: item.PR_ITEM_ID_STRING,
                                            SERV_LINE_ITEM_STRING: item.SERV_LINE_ITEM_STRING,
                                            //productDeliveryDetails: pr.PLANT_NAME + "," + pr.PLANT_LOCATION + " " + "Delivery Details:" + userService.toLocalDate(item.DELIVERY_DATE)
                                            productDeliveryDetails: (pr.PLANT_NAME ? pr.PLANT_NAME : '') + "," + (pr.PLANT_LOCATION ? pr.PLANT_LOCATION : ''),
                                            PLANT_CODE : pr.PLANT

                                        };

                                        if ($scope.requirementItems.productImageID == null || $scope.requirementItems.productImageID == '') {
                                            $scope.requirementItems.productImageID = 0;
                                        } else {
                                            $scope.requirementItems.productImageID == item.ATTACHMENTS;
                                        }

                                        if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                            let existingItem = $scope.formRequest.listRequirementItems.filter(function (currentItem) {
                                                return currentItem.productId == $scope.requirementItems.productId && currentItem.PLANT_CODE == pr.PLANT;
                                            });

                                            if (!isServicePRItem && existingItem && existingItem.length > 0) {
                                                existingItem[0].productQuantity += $scope.requirementItems.productQuantity;
                                                existingItem[0].PR_QUANTITY += item.REQUIRED_QUANTITY;
                                                existingItem[0].PR_ID = existingItem[0].PR_ID + ',' + item.PR_ID;
                                                existingItem[0].PR_ITEM_ID = existingItem[0].PR_ITEM_ID + ',' + item.ITEM_ID;
                                            } else {
                                                $scope.formRequest.listRequirementItems.push($scope.requirementItems);
                                            }
                                        } else {
                                            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
                                        }


                                        //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                        if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                            $scope.formRequest.listRequirementItems.forEach(function (snoItem, snoIndex) {
                                                snoItem.productSNo = snoIndex + 1;
                                            })
                                        }

                                        //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                            var selProd = $scope.productsList.filter(function (prod) {
                                                //return prod.prodCode == item.productIDorName;//prodName
                                                return prod.prodCode == item.productCode;
                                            });

                                            if (selProd && selProd != null && selProd.length > 0) {
                                                $scope.autofillProduct(item.productIDorName, index)
                                                $scope.fillTextbox(selProd[0], index, '');
                                            }

                                            $scope.GetProductQuotationTemplate($scope.requirementItems.productId, $scope.requirementItems.productSNo-1, true); // Assign template based sub items
                                        });
                                    }
                                });
                                
                            });
                        
                        console.log("ids start");
                        console.log(selectedItemIds);
                        console.log("ids end");
                        $scope.addNoneCoreItems();
                    }
                } else {
                    if (pr && pr.length > 0) {
                        $scope.PRItemsList = pr;

                        $scope.formRequest.listRequirementItems = [];

                        $scope.PRItemsList.forEach(function (item, index) {
                            if (!item.REQ_ID || item.REQ_ID <= 0) {
                                let currentPRItemsIds = [];
                                let plants = '';
                                let plansArr = [];
                                //if (item.ItemPRS) {
                                //    item.ItemPRS.forEach(function (item, index) {
                                //        plants = item.PLANT_NAME;
                                //        if (!plansArr.includes(item.PLANT_NAME)) {
                                //            plansArr.push(item.PLANT_NAME);
                                //        }
                                //        if (item.REFERRED_PR_ITEM_IDS) {
                                //            selectedItemIds.push(item.REFERRED_PR_ITEM_IDS.toString());
                                //            currentPRItemsIds.push(item.REFERRED_PR_ITEM_IDS.toString());
                                //        }
                                //    });

                                //    plants = plansArr && plansArr.length > 0 ? plansArr.join() : '';
                                //}
                                if (item.SELECTED_PR_ITEM_IDS && item.SELECTED_PR_ITEM_IDS.length > 0)
                                {
                                    item.SELECTED_PR_ITEM_IDS.forEach(function (prItemId,prItemIndex) {
                                        currentPRItemsIds.push(prItemId);
                                    });
                                }

                                $scope.requirementItems =
                                {
                                    productSNo: index + 1,
                                    ItemID: 0,
                                    productIDorName: item.ITEM_NAME,
                                    productNo: '',//item.ITEM_NUM,
                                    hsnCode: item.HSN_CODE,
                                    productDescription: ((item.ITEM_DESCRIPTION ? item.ITEM_DESCRIPTION : '') + (item.ITEM_DESC ? ', ' + item.ITEM_DESC : '') + (item.ITEM_TEXT ? ', ' + item.ITEM_TEXT : '') + (item.PR_NOTE ? ', ' + item.PR_NOTE : '') + (item.PROJECT_TYPE ? ', ' + item.PROJECT_TYPE : '')).trim(),
                                    productQuantity: item.SELECTED_QUANTITY,
                                    productQuantityIn: item.UNITS ? item.UNITS : item.UOM,
                                    productBrand: item.BRAND,
                                    othersBrands: '',
                                    isDeleted: 0,
                                    productImageID: item.ATTACHMENTS,
                                    attachmentName: '',
                                    //budgetID: 0,
                                    //bcInfo: '',
                                    productCode: item.ITEM_CODE,
                                    splitenabled: 0,
                                    fromrange: 0,
                                    torange: 0,
                                    requiredQuantity: 0,
                                    I_LLP_DETAILS: "",
                                    itemAttachment: "",
                                    itemMinReduction: 0,
                                    productId: item.PRODUCT_ID,
                                    CategoryId: item.CATEGORY_ID,
                                    PR_ID: item.PR_ID,
                                    PR_ITEM_ID: _(currentPRItemsIds).join(),
                                    PR_QUANTITY: item.SELECTED_QUANTITY,
                                    isCoreProductCategory: 1,
                                    catalogueItemID: item.PRODUCT_ID,
                                    VERTICAL_TYPE: item.VERTICAL_TYPE,
                                    productDeliveryDetails: item.PLANT_NAME,
                                    PLANT_CODE: item.PLANT
                                    //,productDeliveryDetails: pr.PLANT + "Delivery Details:" + userService.toLocalDate(item.DELIVERY_DATE)
                                };

                                if ($scope.requirementItems.productImageID == null || $scope.requirementItems.productImageID == '') {
                                    $scope.requirementItems.productImageID = 0;
                                } else {
                                    $scope.requirementItems.productImageID == item.ATTACHMENTS;
                                }

                                $scope.formRequest.listRequirementItems.push($scope.requirementItems);

                                //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    $scope.formRequest.listRequirementItems.forEach(function (snoItem, snoIndex) {
                                        snoItem.productSNo = snoIndex + 1;
                                    });

                                    $scope.GetProductQuotationTemplate($scope.requirementItems.productId, $scope.requirementItems.productSNo - 1, true); // Assign template based sub items
                                }
                            }
                        });

                        $scope.formRequest.PR_ID = _($scope.PRItemsList).map('PR_ID').value().join();
                        $scope.addNoneCoreItems();
                    }
                }
            };


            $scope.addNoneCoreItems = function () {
                if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0) {
                    $scope.nonCoreproductsList.forEach(function (product, index) {
                        if (!$scope.isEdit) {
                            product.doHide = true;
                            $scope.AddOtherRequirementItem(product);
                        }
                    });
                }
            };

            function assignTemplateSubItems(productId, index) {
                var currentItem = $scope.formRequest.listRequirementItems[index];
                if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0) {
                    currentItem.productQuotationTemplate = [];
                }

                $scope.templateSubItems.forEach(function (subItem, index) {
                    let temp = currentItem.productQuotationTemplate.filter(function (template) {
                        return (template.T_ID === subItem.T_ID || template.NAME === subItem.NAME);
                    });

                    if (!temp || temp.length <= 0) {
                        subItem.PRODUCT_ID = productId;
                        currentItem.productQuotationTemplate.push(subItem);
                    }                    
                });
            };

            $scope.GetProductQuotationTemplate = function (productId, index, productChange) {
                var params = {
                    "catitemid": productId,
                    "sessionid": userService.getUserToken()
                };

                console.log(index);
                var currentItem = $scope.formRequest.listRequirementItems[index];
                console.log(currentItem);
                if (currentItem && !currentItem.isNonCoreProductCategory) {
                    catalogService.GetProductQuotationTemplate(params)
                        .then(function (response) {
                            console.log(response)
                            if (response) {
                                currentItem.productQuotationTemplateMaster = response;
                                if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0 ||
                                    !$scope.isEdit) {
                                    currentItem.productQuotationTemplate = response;
                                    if ($scope.isEdit) {
                                        currentItem.productQuotationTemplate.forEach(function (templateItem, itemIndexs) {
                                            templateItem.IS_VALID = 0;
                                        });
                                    }
                                }
                                else if (currentItem.productQuotationTemplateMaster && currentItem.productQuotationTemplateMaster.length > 0) {
                                    if (productChange) {
                                        currentItem.productQuotationTemplate = currentItem.productQuotationTemplateMaster;
                                    } else {
                                        currentItem.productQuotationTemplateMaster.forEach(function (templateItem, itemIndexs) {
                                            if (templateItem && templateItem.NAME) {
                                                var filterResult = currentItem.productQuotationTemplateArray.filter(function (template) {
                                                    return (template.NAME === templateItem.NAME);
                                                });

                                                if (!filterResult || filterResult.length <= 0) {
                                                    templateItem.IS_VALID = 0;
                                                    currentItem.productQuotationTemplate.unshift(templateItem);
                                                }
                                            }
                                        });
                                    }
                                }

                                $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                                    if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                                });

                                //Assign Template Sub-Items
                                if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0) {
                                    currentItem.productQuotationTemplate = [];
                                }

                                $scope.templateSubItems.forEach(function (subItem, index) {
                                    let temp = currentItem.productQuotationTemplate.filter(function (template) {
                                        return (template.NAME === subItem.NAME);
                                    });

                                    if (!temp || temp.length <= 0) {
                                        subItem.PRODUCT_ID = productId;
                                        let temp = JSON.stringify(subItem);
                                        currentItem.productQuotationTemplate.push(JSON.parse(temp));
                                    }
                                });
                                //^Assign Template Sub-Items
                            }
                        });
                }

            };

            //Below product sub item
            $scope.SaveProductQuotationTemplate = function (obj) {
                var sameNameError = false;
                obj.currentProductQuotationTemplate.forEach(function (item, itemIndex) {
                    console.log(item)
                    if (obj.NAME.toUpperCase() == item.NAME.toUpperCase() && obj.T_ID == 0 && item.IS_VALID!==-1) {
                        sameNameError = true;
                    }
                });

                if (sameNameError) { growlService.growl("Same name Error.", "inverse"); return false; }

                if (obj.HAS_SPECIFICATION) {
                    obj.NAME = obj.NAME.replace(/\'/gi, "");
                    obj.NAME = obj.NAME.replace(/\"/gi, "");
                    obj.NAME = obj.NAME.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    obj.NAME = obj.NAME.replace(/(\r\n|\n|\r)/gm, "");
                    obj.NAME = obj.NAME.replace(/\t/g, '');


                    obj.DESCRIPTION = obj.DESCRIPTION.replace(/\'/gi, "");
                    obj.DESCRIPTION = obj.DESCRIPTION.replace(/\"/gi, "");
                    obj.DESCRIPTION = obj.DESCRIPTION.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    obj.DESCRIPTION = obj.DESCRIPTION.replace(/(\r\n|\n|\r)/gm, "");
                    obj.DESCRIPTION = obj.DESCRIPTION.replace(/\t/g, '');
                }


                var params = {
                    "productquotationtemplate": obj,
                    "sessionid": userService.getUserToken()
                };

                catalogService.SaveProductQuotationTemplate(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.resetTemplateObj();
                            if (obj.PRODUCT_ID > 0 && obj.index >= 0) {
                                $scope.GetProductQuotationTemplate(obj.PRODUCT_ID, obj.index, true);
                            }
                        }
                    });
            };

            $scope.deleteProductQuotationTemplate = function (obj) {
                obj.IS_VALID = -1;
                var params = {
                    "productquotationtemplate": obj,
                    "sessionid": userService.getUserToken()
                };

                catalogService.SaveProductQuotationTemplate(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Deleted Successfully.", "success");
                            $scope.resetTemplateObj(); 
                            if (obj.PRODUCT_ID > 0 && obj.index >= 0) {
                                $scope.GetProductQuotationTemplate(obj.PRODUCT_ID,obj.index,true);
                            }
                        }
                    });
            };
            

            $scope.resetTemplateObj = function (product, index) {
                if (!product) {
                    product = { catalogueItemID: 0, productQuotationTemplate: []};
                }
                if (!index) {
                    index = 0;
                }

                $scope.QuotationTemplateObj = {
                    currentProductQuotationTemplate: product.productQuotationTemplate,
                    index: index,
                    T_ID: 0,
                    PRODUCT_ID: product.catalogueItemID,
                    NAME: '',
                    DESCRIPTION: '',
                    HAS_SPECIFICATION: 0,
                    HAS_PRICE: 0,
                    HAS_QUANTITY: 0,
                    CONSUMPTION: 1,
                    UOM: '',
                    HAS_TAX: 0,
                    IS_VALID: 1,
                    U_ID: userService.getUserId()
                };
            };

            $scope.resetTemplateObj();
            //^Above product sub item

            $scope.loadCustomFields = function () {
                var params = {
                    "compid": userService.getUserCompanyId(),
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.getCustomFieldList(params)
                    .then(function (response) {
                        $scope.customFieldList = response;
                            if ($scope.isEdit) {
                                params = {
                                    "compid": userService.getUserCompanyId(),
                                    "fieldmodule": 'REQUIREMENT',
                                    "moduleid": $stateParams.Id,
                                    "sessionid": userService.getUserToken()
                                };

                                PRMCustomFieldService.GetCustomFieldsByModuleId(params)
                                .then(function (response) {
                                    $scope.selectedcustomFieldList = response;
                                    $scope.selectedcustomFieldList.forEach(function (cfItem, index) {
                                        cfItem.IS_SELECTED == 1;

                                        var cfTempField = _.filter($scope.customFieldList, function (field) {
                                                return field.CUST_FIELD_ID == cfItem.CUST_FIELD_ID;
                                            });
                                        if(cfTempField && cfTempField.length > 0){
                                            cfTempField[0].IS_SELECTED = 1;
                                            cfTempField[0].FIELD_VALUE = cfItem.FIELD_VALUE;
                                            cfTempField[0].MODULE_ID = cfItem.MODULE_ID;
                                        }
                                    });
                                });
                             }
                        });
                };

            //$scope.loadCustomFields();

            $scope.addCustomFieldToFrom = function (field) {
                $scope.selectedcustomFieldList = _.filter($scope.customFieldList, function (field) {
                    return field.IS_SELECTED == 1;
                });
            };

            $scope.validateSubItemQuantity = function (product, productQuotationTemplate){
                if (!productQuotationTemplate.CONSUMPTION) {
                    productQuotationTemplate.CONSUMPTION = 1;
                } else {
                    product.productQuantity = 1;
                }
            };

            
            $scope.saveRequirementCustomFields = function(requirementId) {
                if (requirementId) {
                     $scope.selectedcustomFieldList.forEach(function (item, index) {
                        item.MODULE_ID = requirementId;
                        item.ModifiedBy = $scope.userID;
                    });

                    var params = {
                        "details": $scope.selectedcustomFieldList,
                        "sessionid": userService.getUserToken()
                    };

                    PRMCustomFieldService.saveCustomFieldValue(params)
                        .then(function (response) {
                        
                        });
                }
            };
            
            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var template = $scope.selectedTemplate && $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : 'PRM-DEFAULT';
                var params = {
                    "templateid": 0,
                    "templatename": template,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;                        
                    });

                    if ($scope.prmFieldMappingDetails)
                    {
                        var searchForCas,searchForMfcd = false;
                        searchForCas = $scope.prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL.toLowerCase().contains("cas number");
                        searchForMfcd = $scope.prmFieldMappingDetails.PRODUCT_NUMBER.FIELD_LABEL.toLowerCase().contains("mfcd code");
                        $scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS = searchForCas;
                        $scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD = searchForMfcd;
                    }

                    //$scope.changeFeilds($scope.prmFieldMappingDetails);

                });

                if ($scope.selectedTemplate.TEMPLATE_ID) {
                    getTemplateSubItems($scope.selectedTemplate.TEMPLATE_ID);
                }
            };



            $scope.changeFeilds = function (data) {
                if (data.PRODUCT_CODE) {
                    if (data.PRODUCT_CODE.FIELD_LABEL == "CAS Number") {
                        data.PRODUCT_CODE.isCasNumber = true;
                    } else {
                        data.PRODUCT_CODE.isCasNumber = false;
                    }
                }
                if (data.PRODUCT_NUMBER) {
                    if (data.PRODUCT_NUMBER.FIELD_LABEL == "MFCD Code") {
                        data.PRODUCT_NUMBER.isMfcdCode = true;
                    } else {
                        data.PRODUCT_NUMBER.isMfcdCode = false;
                    }
                }
            }

            if (!$scope.reqId) {
                $scope.GetPRMTemplateFields();
            }

            $scope.GetPRMTemplates = function (defaultTemplateId) {
                PRMCustomFieldService.GetTemplates().then(function (response) {
                    $scope.prmTemplates = response;
                    if ($scope.prmTemplates && $scope.prmTemplates.length > 0) {
                        if (defaultTemplateId) {
                            let defaultTemplate = _.filter($scope.prmTemplates, function (template) { return template.TEMPLATE_ID === +defaultTemplateId; });
                            if (defaultTemplate && defaultTemplate.length > 0) {
                                $scope.selectedTemplate = defaultTemplate[0];
                            }
                        } else {
                            let defaultTemplate = _.filter($scope.prmTemplates, function (template) { return template.TEMPLATE_NAME === 'PRM-DEFAULT'; });
                            if (defaultTemplate && defaultTemplate.length > 0) {
                                $scope.selectedTemplate = defaultTemplate[0];
                            }
                        }

                        $scope.GetPRMTemplateFields();
                    }
                });
            };

            $scope.searchingCategoryVendors = function (value) {
                if (value) {
                    $scope.VendorsList = _.filter($scope.VendorsTempList1, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                }
            };

            $scope.selectFormCategorySearch = function (vendor) {
                $scope.getVendorCodes(vendor);
                var error = false;
                $scope.formRequest.auctionVendors.forEach(function (SV, SVIndex) {
                    if (SV.vendorID == vendor.vendorID) {
                        error = true;
                        growlService.growl("Vendor already selected", "inverse");
                    }
                });

                if (error == true) {
                    return;
                }

                $scope.Vendors.forEach(function (UV, UVIndex) {
                    if (UV.vendorID == vendor.vendorID) {
                        error = true;
                    }
                });

                if (error == false) {
                    vendor.isFromGlobalSearch = 1;
                    vendor.isSelected = true;
                    $scope.formRequest.auctionVendors.push(vendor);
                    $scope.GetRequirementVendorAnalysis();
                    if ($scope.Vendors && $scope.Vendors.length > 0) {
                        $scope.Vendors = $scope.Vendors.filter(function (x) {
                            return x.vendorID !== vendor.vendorID;
                        });
                    } else {
                        $scope.Vendors = [];
                    }

                    $scope.Vendors.push(vendor);

                    $scope.VendorsList = $scope.VendorsList.filter(function (obj) {
                        return obj.vendorID !== vendor.vendorID;
                    });

                    if ($scope.globalVendors) {
                        $scope.globalVendors = $scope.globalVendors.filter(function (obj) {
                            return obj != vendor;
                        });
                    }

                    if ($scope.VendorsTemp1) {
                        $scope.VendorsTemp1 = $scope.VendorsTemp1.filter(function (obj) {
                            return obj != vendor;
                        });
                    }

                    $scope.ShowDuplicateVendorsNames.push(vendor);
                }
                else {
                    $scope.selectForA(vendor);
                    vendor.isChecked = false;
                }
            };

            // Category Based Vendors //
            $scope.VendorsList = [];
            $scope.VendorsTempList1 = [];

            $scope.getCategoryVendors = function (str) {
                $scope.ShowDuplicateVendorsNames = [];
                $scope.VendorsList = [];
                $scope.VendorsTempList1 = [];
                $scope.vendorsLoaded = false;
                var category = '';

                category = catObj.catCode;

                var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getVendorsbyCategories',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.VendorsList = response.data;
                            $scope.VendorsTempList = $scope.VendorsList;
                            $scope.Vendors.forEach(function (item, index) {
                                $scope.VendorsTempList.forEach(function (item1, index1) {
                                    if (item.vendorID == item1.vendorID) {
                                        $scope.ShowDuplicateVendorsNames.push(item1);
                                        $scope.VendorsList.splice(index1, 1);
                                    }
                                })
                            });

                            if ($scope.formRequest.auctionVendors.length > 0) {
                                $scope.formRequest.auctionVendors.forEach(function (item1, index1) {
                                    $scope.VendorsTempList.forEach(function (item2, index2) {
                                        if (item1.vendorID == item2.vendorID) {
                                            $scope.ShowDuplicateVendorsNames.push(item2);
                                            $scope.VendorsList.splice(index2, 1);
                                        }
                                    });
                                });
                            }


                            $scope.VendorsTempList1 = $scope.VendorsList;

                            if ($scope.ShowDuplicateVendorsNames.length > 0) {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            } else {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            }

                        } else {
                            $scope.VendorsList = [];
                        }

                        if ($scope.searchCategoryVendorstring != '') {
                            $scope.searchingCategoryVendors($scope.searchCategoryVendorstring);
                        } else {
                            $scope.searchingCategoryVendors('');
                        }


                    } else {

                    }
                }, function (result) {
                });

            }
            // Category Based Vendors //

            $scope.validateLineItemsList = function () {
                if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0 && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                    $scope.nonCoreproductsList.forEach(function (nonCoreProduct, index) {
                        var filterItems = $scope.formRequest.listRequirementItems.filter(function (product) {
                            return product.catalogueItemID === nonCoreProduct.prodId;
                        });

                        if (filterItems && filterItems.length > 0) {
                            nonCoreProduct.doHide = true;
                        }
                    });
                }
            };

            $('.selected-items-box').bind('click', function(e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });

            $scope.createProduct = function (product, index) {
                $scope.itemProductNameErrorMessage = '';
                $scope.itemProductUnitsErrorMessage = '';


                if (!product.productIDorName || !product.productQuantityIn) {

                    if (!product.productIDorName) {
                        $scope.itemProductNameErrorMessage = 'Please Enter Product Name.';
                    } else if (!product.productQuantityIn) {
                        $scope.itemProductUnitsErrorMessage = 'Please Enter Units.';
                    }
                    return
                }
                if (product.VERTICAL_TYPE == null || product.VERTICAL_TYPE == '') {
                    $scope.itemProductNameErrorMessage = 'Please select vertical type';
                    return;
                }

                $scope.productObj = {
                    prodId: 0,
                    prodCode: $scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS ? '' : product.productCode,
                    casNumber: $scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS ? product.productCode : '',
                    compId: userService.getUserCompanyId(),
                    prodName: product.productIDorName,
                    prodNo: $scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD ? '' : product.productNo,
                    mfcdCode: $scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD ? product.productNo : '',
                    prodQty: product.productQuantityIn,
                    prodHSNCode: product.hsnCode,
                    prodDesc: product.productDescription,
                    VERTICAL_TYPE: product.VERTICAL_TYPE,
                    isValid: 1,
                    ModifiedBy: userService.getUserId()
                };

                var params = {
                    reqProduct: $scope.productObj,
                    sessionID: userService.getUserToken()
                }

                catalogService.addproduct(params)
                    .then(function (response) {

                        if (response.repsonseId != -1) {
                            $scope.postRequestLoding = false;
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl("product Added Successfully.", "success");
                                $scope.postRequestLoding = false;
                                $scope.itemProductNameErrorMessage = '';
                                $scope.itemProductUnitsErrorMessage = '';
                                var productID = response.repsonseId;
                                $scope.formRequest.listRequirementItems[index].catalogueItemID = productID;
                                $scope.formRequest.listRequirementItems[index].isNewItem = true;
                                $scope.GetProductQuotationTemplate(productID, index, true);
                            }
                        } else {
                            if (response.errorMessage != '') {
                                $scope.postRequestLoding = true;
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        }                        
                    });
            };

            $scope.selectAll = function (allVendors) {
                if (allVendors) {
                    $scope.Vendors.forEach(function (vendorItem, vendorIndex) {
                        if (!vendorItem.isSelected) {
                            $scope.selectForA(vendorItem);
                        }
                    });

                    $scope.selectAllVendors = true;
                } else {
                    $scope.formRequest.auctionVendors.forEach(function (vendorItem, vendorIndex) {
                        if (vendorItem.isSelected) {
                            $scope.selectForB(vendorItem);
                        }
                    });

                    $scope.selectAllVendors = false;
                }
            };

            $scope.getSubUserData = function () {
                userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        response.forEach(function (cust, index) {
                            if ($scope.formRequest.customerEmails.length > 0) {
                                $scope.formRequest.customerEmails.forEach(function (item, itmIndex) {
                                    if (item.userID == cust.userID && item.status == 1) {
                                        cust.isSelected = true;
                                    } else if ((item.userID == cust.userID && item.status == 0) || !cust.isSelected) {
                                        cust.isSelected = false;
                                    }
                                })
                            } else {
                                cust.isSelected = false;
                            }
                        });

                        $scope.subUsers = $filter('filter')(response, { isValid: true });
                    });
            };

            $scope.getSubUserData();

            $scope.customerEmails = [];
            $scope.searchCustomers = function (value) {
                $scope.customerEmails = [];
                if (value) {
                    $scope.customerEmails = _.filter($scope.subUsers, function (item) { return item.email.toUpperCase().indexOf(value.toUpperCase()) > -1; });

                } else {
                    $scope.customerEmails = [];
                }
            }

            $scope.addCustomerEmails = function (mail, id, action) {

                var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (id) {
                    $scope.subUsers.forEach(function (user, index) {
                        if (user.userID == id && user.isSelected == false) {
                            user.isSelected = true;
                            $scope.formRequest.customerEmails.push({ 'mail': mail, 'userID': id, 'status': 1 });
                        } else if (user.userID == id && user.isSelected == true) {
                            user.isSelected = false;
                            $scope.formRequest.customerEmails.forEach(function (cust, custIndex) {
                                if (cust.userID == id) {
                                    cust.status = 0;
                                }
                            })
                        }
                    });

                } else {
                    var intInc = 0;
                    $scope.formRequest.customerEmails.forEach(function (cust, index) {
                        if (cust.mail.trim() == mail.trim() && cust.status == 1 && action == 1) {
                            growlService.growl("email already added", "inverse");
                            intInc++;
                        }
                    });

                    if (action == 1 && intInc == 0) {
                        if (!emailRegex.test(mail)) {
                            growlservice.growl("please enter a valid email", "inverse");
                        } else {
                            $scope.formRequest.customerEmails.push({ 'mail': mail, 'userID': id, 'status': 1 });
                            $scope.searchcustomerstring = "";
                        }
                    } else if (action != 1) {
                        $scope.formRequest.customerEmails.forEach(function (cust, custIndex) {
                            if (cust.mail == mail) {
                                cust.status = 0;
                            }
                        })
                    }
                }
            }

            $scope.Vendors1 = [];
            $scope.globalVendorsLoading = false;
            $scope.getvendorswithoutcategories = function (str) {
                $scope.vendorsLoaded = false;
                $scope.Vendors1 = [];
                $scope.globalVendorsLoading = true;

                var verticals = (_.uniq(_.map($scope.formRequest.listRequirementItems, function (o) {
                    if (o.VERTICAL_TYPE != "" || o.VERTICAL_TYPE != undefined) return o.VERTICAL_TYPE;
                }))).join(',');

                var params = { 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0, 'sessionID': userService.getUserToken(), 'searchString': str, 'verticals': verticals };
                $http({
                    method: 'POST',
                    url: domain + 'getvendorswithoutcategories',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {

                            // filter vendors who has contracts
                            if ($scope.contractPRItems && $scope.contractPRItems.length > 0) {
                                let filteredVendors = _.filter(response.data, function (venObj) {
                                    return _.findIndex($scope.contractPRItems, (item) => { return item.CONTRACT_VENDOR === venObj.vendorID }) < 0;
                                });
                                response.data = filteredVendors;
                            }

                            $scope.Vendors1 = response.data;                           
                            $scope.vendorsLoaded = true;
                            if ($scope.formRequest.auctionVendors.length > 0) {
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors1) {
                                        if ($scope.Vendors1[i].vendor.phoneNum == $scope.formRequest.auctionVendors[j].vendor.phoneNum && $scope.Vendors1[i].COMPANY_CODE == $scope.formRequest.auctionVendors[j].COMPANY_CODE) {
                                            $scope.Vendors1.splice(i, 1);
                                        }
                                    }
                                }
                            }
                            if ($scope.Vendors.length > 0) {
                                for (var j in $scope.Vendors) {
                                    for (var i in $scope.Vendors1) {
                                        if ($scope.Vendors1[i].vendor.phoneNum == $scope.Vendors[j].vendor.phoneNum) {
                                            $scope.Vendors1.splice(i, 1);
                                        }
                                    }
                                }
                            }
                            $scope.globalVendorsLoading = false;
                            //console.log($scope.Vendors1)
                            $scope.VendorsTemp1 = $scope.Vendors1;
                            $scope.globalVendors = $scope.VendorsTemp1;
                            //$scope.totalItems = $scope.globalVendors.length;
                            $scope.searchVendorsAllCategory('');

                        } else {
                            $scope.globalVendors = [];
                            $scope.globalVendorsLoading = false;
                        }
                    }

                }, function (result) {
                });

            };

            $scope.searchGlobalVendors = function (str) {

                if (!str) {
                    $scope.globalVendors = [];

                } else {
                    var strTemp = str;
                    strTemp = String(strTemp).toUpperCase();
                    $scope.getvendorswithoutcategories(str);
                    $scope.serchVendorString = str;
                }
            }
            

            $scope.searchVendorsAllCategory = function (value) {
                $scope.Vendors1 = _.filter($scope.VendorsTemp1, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
            };

            $scope.clearGobalVendors = function () {
                $scope.globalVendors = [];
                $scope.serchVendorString = "";
            };

            $scope.uploadRequirementItemsSaveExcel = function (fileContent) {
                var params = {
                    reqID: $scope.auctionItem ? $scope.auctionItem.requirementID : 0,
                    isrfp: $scope.formRequest ? $scope.formRequest.isRFP : false,
                    userID: userService.getUserId(),
                    compId: userService.getUserCompanyId(),
                    sessionID: userService.getUserToken(),
                    requirementItemsAttachment: fileContent,
                    templateid: $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0
                };
                auctionsService.uploadRequirementItemsSaveExcel(params)
                    .then(function (response) {
                        $("#requirementItemsSveAttachment").val(null);
                        if (response && response.length > 0) {

                            if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                $scope.formRequest.listRequirementItems.forEach(function (reqItem, index) {
                                    if (!reqItem.isNonCoreProductCategory && reqItem.productCode) {
                                        var filteredReqItems = response.filter(function (excelItem) {
                                            return (reqItem.productId == excelItem.catalogueItemID || reqItem.catalogueItemID == excelItem.catalogueItemID);
                                        });

                                        if (filteredReqItems && filteredReqItems.length <= 0) {
                                            reqItem.isDeleted = 1;
                                        }
                                    }
                                });
                            }

                            response.forEach(function (item, index) {
                                let maxSnoItemNo = 1;
                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    maxSnoItemNo = $scope.formRequest.listRequirementItems.length;
                                    maxSnoItem = _.maxBy($scope.formRequest.listRequirementItems, 'productSNo');
                                    maxSnoItemNo = maxSnoItem ? maxSnoItem.productSNo : $scope.formRequest.listRequirementItems.length;
                                }

                                $scope.itemnumber = maxSnoItemNo;
                                //let itemIndex = $scope.formRequest.listRequirementItems ? $scope.formRequest.listRequirementItems.length : 1;
                                let tempRequirementItem =
                                {
                                    productSNo: $scope.itemnumber + 1,
                                    ItemID: 0,
                                    productIDorName: item.productIDorName,
                                    productNo: item.productNo,
                                    hsnCode: item.hsnCode,
                                    productDescription: item.productDescription,
                                    productQuantity: item.productQuantity,
                                    productQuantityIn: item.productQuantityIn,
                                    productBrand: item.productBrand,
                                    productDeliveryDetails: item.productDeliveryDetails,
                                    othersBrands: '',
                                    isDeleted: 0,
                                    productImageID: 0,
                                    attachmentName: '',
                                    //budgetID: 0,
                                    //bcInfo: '',
                                    productCode: item.productCode,
                                    splitenabled: 0,
                                    fromrange: 0,
                                    torange: 0,
                                    requiredQuantity: 0,
                                    I_LLP_DETAILS: "",
                                    itemAttachment: "",
                                    itemMinReduction: 0,
                                    isCoreProductCategory: 1,
                                    VERTICAL_TYPE: item.VERTICAL_TYPE,
                                    productId: item.catalogueItemID,
                                    catalogueItemID: item.catalogueItemID
                                };

                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    var existingItem = $scope.formRequest.listRequirementItems.filter(function (currentItem) {
                                        return (currentItem.productId == tempRequirementItem.productId || currentItem.catalogueItemID == tempRequirementItem.productId);
                                    });

                                    if (existingItem && existingItem.length > 0 && !$scope.formRequest.isRFP) {
                                        existingItem[0].productQuantity = tempRequirementItem.productQuantity;
                                        existingItem[0].isDeleted = 0;
                                    } else {
                                        $scope.formRequest.listRequirementItems.push(tempRequirementItem);
                                    }
                                } else {
                                    $scope.formRequest.listRequirementItems.push(tempRequirementItem);
                                }
                            });

                            if ($scope.formRequest && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                if (!$scope.formRequest.listRequirementItems[0].productCode) {
                                    $scope.formRequest.listRequirementItems[0].isDeleted = 1;
                                }

                                $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                                    if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                                });

                                let index = 0;
                                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                    if (!item.isDeleted && !item.isNonCoreProductCategory) {
                                        $scope.GetProductQuotationTemplate(item.productId, index, true);
                                    }
                                    index++;
                                });
                            }
                            
                            swal("Verify", "Please verify and Submit the requirement!", "success");
                        }
                    });
            };

            $scope.saveRequirementSetting = function (reqId, setting, settingValue) {
                let param = {
                    requirementSetting: {
                        REQ_ID: reqId,
                        REQ_SETTING: setting, //'TEMPLATE_ID',
                        REQ_SETTING_VALUE: settingValue, //$scope.selectedTemplate.TEMPLATE_ID,
                        USER: $scope.userID,
                        sessionID: $scope.sessionid
                    }
                };

                auctionsService.saveRequirementSetting(param)
                    .then(function (response) {
                        
                    });
            };

            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                if ($scope.reqId) {
                    auctionsService.getRequirementSettings({ "reqid": $scope.reqId, "sessionid": $scope.sessionid })
                        .then(function (response) {
                            $scope.requirementSettings = response;
                            if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                                var template = $scope.requirementSettings.filter(function (setting) {
                                    return setting.REQ_SETTING === 'TEMPLATE_ID';
                                });

                                if (template && template.length > 0) {
                                    $scope.formRequest.templateId = +template[0].REQ_SETTING_VALUE;
                                    $scope.GetPRMTemplates(template[0].REQ_SETTING_VALUE);
                                }
                            }
                        });
                } else {
                    let templateId = 0;
                    if ($scope.selectedTemplate && $scope.selectedTemplate) {
                        templateId = $scope.selectedTemplate.TEMPLATE_ID;
                    }

                    $scope.GetPRMTemplates(templateId);
                }
            };

            $scope.getRequirementSettings();

            $scope.showContractProducts = function (contactsProducts) {
                $rootScope.PRPendingContracts = contactsProducts;
                let contractPopUpModalInstance = $modal.open({
                    templateUrl: 'pending-contracts-template-temporary.html',
                    controllerAs: '_modalCtrl',
                    appendTo: 'body',
                    size: 'lg',
                    resolve: {
                        data: {
                            contractProducts: contactsProducts
                        }
                    }
                });

                return contractPopUpModalInstance;
            };

            $scope.AddMultiQty1 = function (product, quantity, index) {
                if ($scope.reqId) {
                    $scope.formRequest.deleteQuotations = true;
                    $scope.formRequest.disableDeleteQuotations = true;
                }

                if (quantity && quantity > 0) {
                    let tempJsonString = JSON.stringify(product);
                    let tempJsonObj = JSON.parse(tempJsonString);
                    tempJsonObj.productSNo = ($scope.formRequest.listRequirementItems.length + 1);
                    tempJsonObj.itemID = 0;
                    $scope.formRequest.listRequirementItems.push(tempJsonObj);
                } else {
                    swal("Error!", "Quantity Should be greater than zero.", "error");
                }
            };

            $scope.mapSimilarProducts = function (initialProduct, relatedItem) {
                relatedItem.productQuantityIn = initialProduct.productQuantityIn;
                if (relatedItem.similarProduct) {
                    relatedItem.similarProduct.push(initialProduct.productSNo + '-' + relatedItem.productSNo);
                } else {
                    relatedItem.similarProduct = [];
                    relatedItem.similarProduct.push(initialProduct.productSNo + '-' + relatedItem.productSNo);
                }
            };

            $scope.AddMultiQty = function (product, quantity, index) {
                if (quantity && quantity > 0) {
                    var productQuantityObj = {
                        productSNo: product.productSNo,
                        ItemID: 0,
                        productIDorName: product.productIDorName,
                        productNo: product.productNo,
                        productDescription: product.productDescription,
                        productBrand: product.productBrand,
                        othersBrands: product.othersBrands,
                        isDeleted: product.isDeleted,
                        itemAttachment: product.itemAttachment,
                        attachmentName: product.attachmentName,
                        itemMinReduction: product.itemMinReduction,
                        splitenabled: product.splitenabled,
                        fromrange: product.fromrange,
                        torange: product.torange,
                        requiredQuantity: product.requiredQuantity,
                        productCode: product.productCode,
                        hsnCode: product.hsnCode,
                        isNonCoreProductCategory: product.isNonCoreProductCategory,
                        catalogueItemID: product.catalogueItemID,
                        productQuantityIn: product.productQuantityIn,
                        productTempCount: $scope.formRequest.listRequirementItems[index].AddMultipleQuantities.length + 1
                    }
                    productQuantityObj.productQuantity = quantity;
                    $scope.formRequest.listRequirementItems[index].AddMultipleQuantities.push(productQuantityObj);
                } else {
                    swal("Error!", "Quantity Should be greater than zero.", "error");
                }
            };

            $scope.deleteMultiQtyItem = function (product, multiQtySNo, mainProdIdx) {
                if (product.AddMultipleQuantities && product.AddMultipleQuantities.length > 0) {
                    var foundIndex = _.findIndex($scope.formRequest.listRequirementItems[mainProdIdx].AddMultipleQuantities, function (multiQty) { return multiQty.productTempCount === multiQtySNo && multiQty.catalogueItemID === product.catalogueItemID; });
                    if (foundIndex >= 0) {
                        $scope.formRequest.listRequirementItems[mainProdIdx].AddMultipleQuantities.splice(foundIndex, 1);
                    }

                }
            };

            $scope.SelectedCategories = [];
            $scope.GetRequirementVendorAnalysis = function () {
                let requirementItems = $scope.formRequest.listRequirementItems.filter(function (item) {
                    return !item.isNonCoreProductCategory;
                });
                let vendorsList = $scope.formRequest.auctionVendors.filter(function (vendor) {
                    return vendor.isSelected;
                });

                let itemids = _.map(requirementItems, 'catalogueItemID');
                let vendorids = _.map(vendorsList, 'vendorID');

                auctionsService.getRequirementVendorAnalysis({ "vendorids": vendorids.join(), "itemids": itemids.join(), "sessionid": $scope.sessionid })
                    .then(function (response) {
                        let totalAvgAmount = 0;
                        let totalL1Amount = 0;
                        $scope.requirementAnalysisData = response;
                        if ($scope.requirementAnalysisData && $scope.requirementAnalysisData.length > 0) {
                            let analysisDetails = $scope.requirementAnalysisData.filter(function (detail) {
                                return (detail.key1 !== 'MIN_QUOTATION_TIME' && detail.key1 !== 'MAX_QUOTATION_TIME');
                            });

                            if (analysisDetails && analysisDetails.length > 0) {
                                analysisDetails.forEach(function (detail, index) {
                                    let filteredItems = $scope.formRequest.listRequirementItems.filter(function (item) {
                                        return !item.isNonCoreProductCategory && item.catalogueItemID === detail.key;
                                    });

                                    if (filteredItems && filteredItems.length > 0) {
                                        filteredItems.forEach(function (itemTemp, index) {
                                            totalAvgAmount = totalAvgAmount + (itemTemp.productQuantity * detail.decimalVal);
                                            totalL1Amount = totalL1Amount + (itemTemp.productQuantity * detail.decimalVal1);
                                        });
                                    }
                                });                            
                            }
                        }

                        $scope.requirementAnalysisData.push({
                            'key': 'QUOTATION_DETAILS',
                            'decimalVal': totalAvgAmount,
                            'decimalVal1': totalL1Amount
                        });
                    });
            };

            $scope.GetRequirementVendorCodes = function () {
                let vendorsList = $scope.formRequest.auctionVendors.filter(function (vendor) {
                    return vendor.isSelected;
                });

                let vendorids = _.map(vendorsList, 'vendorID');
                auctionsService.getRequirementVendorCodes({ "vendorids": vendorids.join(), "sessionid": $scope.sessionid })
                    .then(function (response) {
                        console.log(response);
                        $scope.formRequest.auctionVendors.forEach(function (vendor, index1) {
                            let vendorCodes = _.filter(response, function (vendorCode) {
                                return vendorCode.VENDOR_ID === vendor.vendorID;
                            });

                            if (vendorCodes && vendorCodes.length > 0) {
                                vendor.vendorCodeList = vendorCodes;
                                if (vendor.vendorCodeList && vendor.vendorCodeList.length > 0) {
                                    vendor.selectedVendorCode = vendor.selectedVendorCode ? vendor.selectedVendorCode : vendor.vendorCodeList[0].VENDOR_CODE;
                                }
                            } else {
                                vendor.vendorCodeList = [];
                            }
                        });
                    });
            };

            $scope.getMomentTime = function (value, units) {
                return moment().add(units, value).format("DD-MMM-YYYY hh:mm:ss");
            };

            $scope.GetReqPRItems = function () {
                $scope.requirementPRStatus.MESSAGE = '';
                $scope.requirementPRStatus.showStatus = false;

                var params = {
                    "reqid": $scope.reqId,
                    "sessionid": userService.getUserToken()
                };

                PRMPRServices.GetPRItemsByReqId(params)
                    .then(function (response) {
                        console.log(response);
                        let inValidItems = _.filter(response, function (prItem) {
                            return prItem.IS_VALID <= 0;
                        });

                        if (inValidItems && inValidItems.length > 0 && false) {
                            swal({
                                title: "Linked PR items are been marked deleted.",
                                text: "",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Continue With Requirement, it will de-link PR items.",
                                cancelButtonText: "Ignore",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        PRMPRServices.DeLinkInValidPRItems(params)
                                            .then(function (response) {
                                                growlService.growl("In-Valid PR Items are de-linked with Requirement.", "success");
                                            });
                                    } else {
                                        $state.go('home');
                                    }
                                });
                        }

                        let modiFiedItems = _.filter(response, function (prItem) {
                            return prItem.IS_MODIFIED > 0;
                        });

                        if (modiFiedItems && modiFiedItems.length > 0) {
                            $scope.requirementPRStatus.MESSAGE = 'Please validate the PRs added to the RFQ, ';
                            $scope.requirementPRStatus.showStatus = true;

                            modiFiedItems.forEach(function (item, itemIndexs) {
                                $scope.requirementPRStatus.MESSAGE = $scope.requirementPRStatus.MESSAGE + '(' + item.PR_NUMBER + ' - ' + item.ITEM_NUM + ' - ' + item.ITEM_NAME + ' modified on ' + userService.toLocalDate(item.MODIFIED_DATE) + '), ';
                            });

                            $scope.requirementPRStatus.MESSAGE = $scope.requirementPRStatus.MESSAGE + ' the requirement last updated time (' + userService.toLocalDate($scope.formRequest.dateModified) + '). Please review the changes and do the needful.';
                        }
                    });
            };

            function getTemplateSubItems(templateId) {
                var params = {
                    "templateid": templateId,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.getTemplateSubItem(params)
                    .then(function (response) {
                        if (response) {
                            $scope.templateSubItems = response;
                            if ($scope.templateSubItems && $scope.templateSubItems.length > 0) {
                                let tempIndex = -1000;
                                $scope.templateSubItems.forEach(function (subItem, index) {
                                    subItem.T_ID = tempIndex - 1;
                                    subItem.PRODUCT_ID = 0;
                                });

                                if ($scope.formRequest && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    $scope.formRequest.listRequirementItems.forEach(function (currentItem, itemIndex) {
                                        if (!currentItem.isNonCoreProductCategory && currentItem.isCoreProductCategory && currentItem.isDeleted <= 0 && currentItem.catalogueItemID) {
                                            if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0) {
                                                currentItem.productQuotationTemplate = [];
                                            }

                                            currentItem.productQuotationTemplate = _.filter(currentItem.productQuotationTemplate, function (subItem) {
                                                return subItem.T_ID > 0;
                                            });

                                            $scope.templateSubItems.forEach(function (subItem, index) {
                                                let temp = currentItem.productQuotationTemplate.filter(function (template) {
                                                    return (template.NAME === subItem.NAME);
                                                });

                                                if (!temp || temp.length <= 0) {
                                                    subItem.PRODUCT_ID = currentItem.catalogueItemID;
                                                    let temp = JSON.stringify(subItem);
                                                    currentItem.productQuotationTemplate.push(JSON.parse(temp));
                                                }
                                            });
                                        }
                                    });
                                }
                            } else {
                                if ($scope.formRequest && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    $scope.formRequest.listRequirementItems.forEach(function (currentItem, itemIndex) {
                                        if (!currentItem.isNonCoreProductCategory && currentItem.isCoreProductCategory && currentItem.isDeleted <= 0 && currentItem.catalogueItemID) {
                                            if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0) {
                                                currentItem.productQuotationTemplate = [];
                                            }
                                            currentItem.productQuotationTemplate = _.filter(currentItem.productQuotationTemplate, function (subItem) {
                                                return subItem.T_ID > 0;
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    });
            }
            
            $scope.isSpot = function (isSpot) {
                if (isSpot) {
                    $scope.formRequest.biddingType = 'SPOT';
                } else {
                    $scope.formRequest.biddingType = 'REVERSE';
                }
            };

            $scope.displaySerialNumber = function (product) {
                var arr = $scope.formRequest.listRequirementItems.filter(function (item, index) { return item.isDeleted === 0 });
                var index = _.findIndex(arr, function (reqItem) { return reqItem.catalogueItemID === product.catalogueItemID && reqItem.PLANT_CODE === product.PLANT_CODE; });
                return index + 1;
            };

            $scope.getVendorCodes = function (vendor) {
                if (!vendor.vendorCodeList || vendor.vendorCodeList.length <= 0) {
                    let params = {
                        'uid': vendor.vendorID,
                        'compid': $scope.compID,
                        'sessionid': userService.getUserToken()
                    };

                    auctionsService.getVendorCodes(params)
                        .then(function (response) {
                            vendor.vendorCodeList = response;
                            if (vendor.vendorCodeList && vendor.vendorCodeList.length > 0) {
                                vendor.selectedVendorCode = vendor.vendorCodeList[0].VENDOR_CODE;
                            }
                        });
                } else {
                    if (vendor.vendorCodeList && vendor.vendorCodeList.length > 0) {
                        vendor.selectedVendorCode = vendor.selectedVendorCode ? vendor.selectedVendorCode : vendor.vendorCodeList[0].VENDOR_CODE;
                    }
                }
            };

            $scope.selectVendorCode = function (vendor) {
                console.log(vendor);
            };

            function loadPRData() {
                if ($stateParams.prDetailsList && $stateParams.prDetailsList.length > 0) {
                    $scope.formRequest.PR_ID = $stateParams.prDetailsList[0].PR_ID;
                    $scope.formRequest.PR_NUMBER = $stateParams.prDetailsList[0].PR_NUMBER;
                    if (!$scope.reqId) {
                        $scope.formRequest.listRequirementItems = [];
                    }
                    
                    $stateParams.prDetailsList.forEach(function (prDetail) {
                        $scope.fillReqItems(prDetail, 'PR');
                    });
                } else if ($stateParams.prItemsList && $stateParams.prItemsList.length > 0) {
                    $scope.fillReqItems($stateParams.prItemsList, 'ITEM');
                }
            }

            if (!$scope.reqId) {
                loadPRData();
            }
            
        }]);