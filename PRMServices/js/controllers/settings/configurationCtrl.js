﻿prmApp
    .controller('configurationCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "$http", "domain","PRMPRServices",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, $http, domain, PRMPRServices) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            $scope.totalItems1 = 0;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage1 = 10;
            $scope.maxSize1 = 5;


            $scope.setPage1 = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged1 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
        /*PAGINATION CODE*/
            $scope.showConfiguration = false;
            $scope.showPlant = false;

            $scope.showTable = function (val) {
                if (val == 1) {
                    $scope.showConfiguration = true;
                    $scope.showPlant = false;
                } else {
                    $scope.showConfiguration = false;
                    $scope.showPlant = true;
                } 
            }

            $scope.tableView = function (val) {
                if (val == 'DOCUMENT_TYPE') {
                    $scope.type = 'DOCUMENT_TYPE';
                    $scope.GetCompanyConfigurations();
                } else {
                    $scope.type = 'TAX_CODES';
                    $scope.showConfiguration = true;
                    $scope.showPlant = false;
                    $scope.GetCompanyConfigurations();
                }
            }


            //Configuration//

            $scope.companyConfigurations = [];
            $scope.companyConfigurations1 = [];
           
            $scope.configuration = {
                compConfigID:0,
                compID: 0,
                configKey: '',
                configText: '',
                configType: '',
                configValue: '',
                isValid: true,
            };

          

            $scope.addnewconfigView = false;

            $scope.GetCompanyConfigurations = function () {
                auctionsService.GetCompanyConfiguration(userService.getUserCompanyId(), $scope.type, userService.getUserToken())
                    .then(function (response) {
                        $scope.companyConfigurations = response;
                        $scope.companyConfigurations1 = response;
                        $scope.totalItems = $scope.companyConfigurations.length;
                    })
            }



            $scope.SaveCompanyConfigurations = function () {
                $scope.configuration.compID = userService.getUserCompanyId();;
                $scope.configuration.userID = $scope.userID;
                $scope.configuration.configKey = $scope.type;
                var params = {
                    "listCompanyConfiguration": [$scope.configuration],
                    "sessionID": userService.getUserToken()
                };
                //auctionsService.SaveCompanyConfiguration(params)
                $http({
                    method: 'POST',
                    url: domain + 'savecompanyconfiguration',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                })
                   .then(function (response) {
                        if (response && response.data && !response.data.errorMessage) {
                            growlService.growl('Successfully Saved', 'success');
                            $scope.GetCompanyConfigurations();
                            $scope.addnewconfigView = false;
                            $scope.configuration = {
                                compConfigID: 0,
                                compID: 0,
                                configKey: '',
                                configText: '',
                                configType: '',
                                configValue: '',
                                isValid: true,
                            };
                        }
                        else {
                            growlService.growl(response.data.errorMessage, "inverse");
                        }
                    });

            };

            $scope.editConfiguration = function (companyConfiguration) {
                $scope.addnewconfigView = true;
                $scope.configuration = companyConfiguration;
               
                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            };

            $scope.closeEditConfiguration = function () {
                $scope.addnewconfigView = false;
                $scope.configuration = {
                    compConfigID: 0,
                    compID: 0,
                    configKey: '',
                    configText: '',
                    configType: '',
                    configValue: '',
                    isValid: true,
                };
                
            };

            //Configuration//

            $scope.companyPlants = [];
            $scope.companyPlants1 = [];

            $scope.plant = {
                FIELD_PK_ID: 0,
                FIELD_VALUE: '',
                FIELD_NAME: ''
            };


            $scope.disablefields = false;

            $scope.GetCompanyERPFieldMappings = function () {
                var params = {
                    "type": "'PLANTS'"
                };
                PRMPRServices.getprfieldmapping(params)
                    .then(function (response) {
                        $scope.companyPlants = response;
                        $scope.companyPlants1 = response;
                        $scope.totalItems1 = $scope.companyPlants.length;
                    })
            }

            $scope.GetCompanyERPFieldMappings();


            $scope.SaveCompanyERPFieldMappings = function () {
                $scope.plant.FIELD_TYPE = 'PLANTS';
                var params = {
                    "listCompanyERPFieldMapping": [$scope.plant],
                    "sessionID": userService.getUserToken()
                };
                $http({
                    method: 'POST',
                    url: domain + 'savecompanyerpfieldmapping',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                })
                    .then(function (response) {
                        if (response && response.data && !response.data.errorMessage) {
                            growlService.growl('Successfully Saved', 'success');
                            $scope.GetCompanyERPFieldMappings();
                            $scope.addnewplantView = false;
                            $scope.disablefields = false;
                            $scope.plant = {
                                FIELD_PK_ID: 0,
                                FIELD_VALUE: '',
                                FIELD_NAME: ''
                            };
                        }
                        else {
                            growlService.growl(response.data.errorMessage, "inverse");
                        }
                    });

            };

            $scope.editPlant = function (companyPlant) {
                if (companyPlant.FIELD_PK_ID == 0) {
                    $scope.disablefields = false;
                } else {
                    $scope.disablefields = true;
                }
                $scope.addnewplantView = true;
                $scope.plant = companyPlant;

                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            };

            $scope.closeEditPlant = function () {
                $scope.disablefields = false;
                $scope.addnewplantView = false;
                $scope.plant = {
                    FIELD_PK_ID: 0,
                    FIELD_VALUE: '',
                    FIELD_NAME: ''
                };

            };
        }]);