prmApp

    .service('catalogService', ["catalogDomain", "userService", "httpServices", "$q", "SAPIntegrationServicesDomain", function (catalogDomain, userService, httpServices, $q, SAPIntegrationServicesDomain) {
        //var domain = 'http://182.18.169.32/services/';
        var catalogService = this;
        catalogService.myCatalog = [];
        //storeService.savestore = function (params) {
        //    let url = storeDomain + 'savestore';
        //    return httpServices.post(url, params);
        //};


        catalogService.getcategories = function (compId, PageSize, NumberOfRecords) {
            let url = catalogDomain + 'getcategories?compId=' + compId + '&sessionid=' + userService.getUserToken() + '&PageSize=' + PageSize + '&NumberOfRecords=' + NumberOfRecords;
            return httpServices.get(url);
        };

        catalogService.GetMaterialProducts = function (compId) {
            let url = catalogDomain + 'getmaterialproducts?compId=' + compId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.GetNonCoreProducts = function (compId) {
            let url = catalogDomain + 'getNonCoreproducts?compId=' + compId + '&userId=' + userService.getUserId() + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.GetMyPendingContracts = function () {
            let url = catalogDomain + 'getpendingcontracts?compId=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.GetProductContracts = function (productids, includeavailed) {
            if (includeavailed) {
                includeavailed = true;
            } else {
                includeavailed = false;
            }
            let url = catalogDomain + 'getproductcontracts?productids=' + productids + '&includeavailed=' + includeavailed + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getSubCatagories = function (parentCatId, compId) {
            let url = catalogDomain + 'getSubCategories?parentCatId=' + parentCatId + '&companyId=' + compId + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.GetProductSubCategories = function (prodId, parentCatId, compId) {
            let url = catalogDomain + 'GetProductSubCategories?prodId=' + prodId+'&parentCatId=' + parentCatId + '&companyId=' + compId + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getVendorCategories = function (vendorId, parentCatId, compId, type, PageSize, NumberOfRecords, searchString) {
            let url = catalogDomain + 'GetVendorCategories?vendorId=' + vendorId + '&parentCatId=' + parentCatId + '&companyId=' + compId + '&sessionid=' + userService.getUserToken() + '&type=' + type + '&PageSize=' + PageSize + '&NumberOfRecords=' + NumberOfRecords + '&searchString=' + searchString;
            return httpServices.get(url);
        };

        catalogService.getVendorProducts = function (vendorId, compId) {
            let url = catalogDomain + 'GetVendorProducts?vendorId=' + vendorId + '&companyId=' + compId + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getUserMyCatalogFile = function (userid) {
            let url = catalogDomain + 'getvendormycatalogfile?userid=' + userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getproductVendors = function (prodId, sessionId) {
            let url = catalogDomain + 'getproductVendors?productID=' + prodId + '&sessionId=' + sessionId;
            return httpServices.get(url);
        };

        catalogService.addcategory = function (params) {
            let url = catalogDomain + 'addcategory';
            return httpServices.post(url,params);
        };

        
        catalogService.updatecategory = function (params) {
            let url = catalogDomain + 'updatecategory';
            return httpServices.post(url, params);
        };

        catalogService.deletecategory = function (params) {
            let url = catalogDomain + 'deletecategory';
            return httpServices.post(url, params);
        };
        
        catalogService.getproductbyid = function (compId,prodId) {
            let url = catalogDomain + 'getproductbyid?compId=' + compId + '&prodId=' + prodId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getProducts = function (compId, PageSize, NumberOfRecords, searchString) {
            let url = catalogDomain + 'GetProducts?compId=' + compId + '&sessionId=' + userService.getUserToken() + '&PageSize=' + PageSize + '&NumberOfRecords=' + NumberOfRecords + '&searchString=' + searchString;
            return httpServices.get(url);
        };

        catalogService.getUserProducts = function (compId, userId) {
            let url = catalogDomain + 'GetUserProducts?compId=' + compId + '&userId=' + userId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getproductsbyfilter = function (params) {
            let url = catalogDomain + 'getproductsbyfilters';
            return httpServices.post(url, params);
        };

        catalogService.GetProductsByCategory = function (compId,catId) {
            let url = catalogDomain + 'GetProductsByCategory?compId=' + compId + '&catIds='+catId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };
        catalogService.GetAllProductsByCategories = function (compId, catId) {
            let url = catalogDomain + 'GetAllProductsByCategories?compId=' + compId + '&catIds=' + catId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.addproduct = function (params) {
            let url = catalogDomain + 'addproduct';
            return httpServices.post(url, params);
        };
        catalogService.updateProductDetails = function (params) {
            let url = catalogDomain + 'updateproduct';
            return httpServices.post(url, params);
        };
        catalogService.SaveIsCommonItem = function (params) {
            let url = catalogDomain + 'saveiscommonitem';
            return httpServices.post(url, params);
        };
        catalogService.isProdEditAllowed = function (params) {
            let url = catalogDomain + 'isProdudtEditAllowed';
            return httpServices.post(url, params);
        };
        catalogService.deleteProduct = function (params) {
            let url = catalogDomain + 'DeleteProduct';
            return httpServices.post(url, params);
        };
        catalogService.updateProductCategories = function (params) {
            let url = catalogDomain + 'updateproductcategories';
            return httpServices.post(url, params);
        };

        catalogService.getAttributes = function (compId) {
            let url = catalogDomain + 'GetAttribute?CompanyId=' + compId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getProperties = function (compId, entityId, entityType) {
            let url = catalogDomain + 'GetProperties?CompanyId=' + compId + '&entityId=' + entityId+'&entityType=' + entityType+'&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.savecatalogproperty = function (params) {
            let url = catalogDomain + 'SaveCatalogProperty';
            return httpServices.post(url, params);
        };

        catalogService.saveEntityProperties = function (params) {
            let url = catalogDomain + 'saveEntityProperties';
            return httpServices.post(url, params);
        };

        catalogService.importentity = function (params) {
            let url = catalogDomain + 'importentity';
            return httpServices.post(url, params);
        };

        catalogService.saveBulkProductContracts = function (params) {
            let url = catalogDomain + 'savebulkproductcontracts';
            return httpServices.post(url, params);
        };

        catalogService.importcataloguecategories = function (params) {
            let url = catalogDomain + 'importcataloguecategories';
            return httpServices.post(url, params);
        };

        catalogService.importvendoritemcategories = function (params) {
            let url = catalogDomain + 'importvendoritemcategories';
            return httpServices.post(url, params);
        };
        

        //catalogService.savefilters = function (params) {
        //    let url = catalogDomain + 'addfilter';
        //    return httpServices.post(url, params);
        //};

        catalogService.saveVendorCatalog = function (params) {
            let url = catalogDomain + 'saveVendorCatalog';
            return httpServices.post(url, params);
        };

        catalogService.saveVendorCatalog1 = function (params) {
            let url = catalogDomain + 'saveVendorCatalog1';
            return httpServices.post(url, params);
        };

        catalogService.GetCompanyConfiguration = function (compid, configkey, sessionid) {
            let url = catalogDomain + 'getcompanyconfiguration?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        catalogService.GetProdVendorData = function (params) {
            let url = catalogDomain + 'getprodvendordata?vendorid=' + params.vendorid + '&catitemid=' + params.catitemid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };


        catalogService.SaveProductQuotationTemplate = function (params) {
            let url = catalogDomain + 'saveproductquotationtemplate';
            return httpServices.post(url, params);
        };

        catalogService.SaveProductContract = function (params) {
            let url = catalogDomain + 'saveproductcontact';
            return httpServices.post(url, params);
        };

        catalogService.GetProductQuotationTemplate = function (params) {
            let url = catalogDomain + 'getproductquotationtemplate?catitemid=' + params.catitemid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        catalogService.getUserProducts = function (PageSize, NumberOfRecords, searchString,isCas,isMfcd, isnamesearch) {
            var def = $q.defer();
            catalogService.myCatalog = [];
            if (catalogService.myCatalog && catalogService.myCatalog.length > 0) {
                def.resolve(catalogService.myCatalog);
            } else {

                var params = {
                    catCompID: userService.getUserCatalogCompanyId(),
                    userId: userService.getUserId(),
                    sessionId: userService.getUserToken()
                };

                httpServices.get(catalogDomain + 'getuserproducts?compId=' + params.catCompID + '&userId=' + params.userId.trim() + ' &sessionId=' + params.sessionId +
                    '&PageSize=' + PageSize + '&NumberOfRecords=' + NumberOfRecords + '&searchString=' + searchString + '&isCas=' + isCas + '&isMfcd=' + isMfcd + '&isnamesearch=' + isnamesearch)
                    .then(function (response) {
                        def.resolve(response);
                        if (response && response.length > 0) {
                            var ListUserProducts = response;
                            catalogService.myCatalog = ListUserProducts;
                        }
                    });
            }

            return def.promise;
        };

        catalogService.GetProdDataReport = function (params) {
            let url = catalogDomain + 'getproddatareport?reportType=' + params.reportType + '&catitemid=' + params.catitemid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        catalogService.GetMaterialDetailsFromSAP = function (params) {
            let url = SAPIntegrationServicesDomain + 'getmaterialfromsap?materialcode=' + params.productCode + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        
        return catalogService;
    }]);