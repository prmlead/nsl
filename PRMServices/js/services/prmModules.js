prmApp

    // =========================================================================
    // CALENDAR WIDGET
    // =========================================================================

    .directive('fullCalendar', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.fullCalendar({
                    contentHeight: 'auto',
                    theme: true,
                    header: {
                        right: '',
                        center: 'prev, title, next',
                        left: ''
                    },
                    defaultDate: '2014-06-12',
                    editable: true,
                    events: [
                        {
                            title: 'All Day',
                            start: '2014-06-01',
                            className: 'bgm-cyan'
                        },
                        {
                            title: 'Long Event',
                            start: '2014-06-07',
                            end: '2014-06-10',
                            className: 'bgm-orange'
                        },
                        {
                            id: 999,
                            title: 'Repeat',
                            start: '2014-06-09',
                            className: 'bgm-lightgreen'
                        },
                        {
                            id: 999,
                            title: 'Repeat',
                            start: '2014-06-16',
                            className: 'bgm-blue'
                        },
                        {
                            title: 'Meet',
                            start: '2014-06-12',
                            end: '2014-06-12',
                            className: 'bgm-teal'
                        },
                        {
                            title: 'Lunch',
                            start: '2014-06-12',
                            className: 'bgm-gray'
                        },
                        {
                            title: 'Birthday',
                            start: '2014-06-13',
                            className: 'bgm-pink'
                        },
                        {
                            title: 'Google',
                            url: 'http://google.com/',
                            start: '2014-06-28',
                            className: 'bgm-bluegray'
                        }
                    ]
                });
            }
        }
    })


    // =========================================================================
    // MAIN CALENDAR
    // =========================================================================

    .directive('calendar', function ($compile) {
        return {
            restrict: 'A',
            scope: {
                select: '&',
                actionLinks: '=',
            },
            link: function (scope, element, attrs) {

                var date = new Date();
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();

                //Generate the Calendar
                element.fullCalendar({
                    header: {
                        right: '',
                        center: 'prev, title, next',
                        left: ''
                    },

                    theme: true, //Do not remove this as it ruin the design
                    selectable: true,
                    selectHelper: true,
                    editable: true,

                    //Add Events
                    events: [
                        {
                            title: 'Hangout with friends',
                            start: new Date(y, m, 1),
                            allDay: true,
                            className: 'bgm-cyan'
                        },
                        {
                            title: 'Meeting with client',
                            start: new Date(y, m, 10),
                            allDay: true,
                            className: 'bgm-red'
                        },
                        {
                            title: 'Repeat Event',
                            start: new Date(y, m, 18),
                            allDay: true,
                            className: 'bgm-blue'
                        },
                        {
                            title: 'Semester Exam',
                            start: new Date(y, m, 20),
                            allDay: true,
                            className: 'bgm-green'
                        },
                        {
                            title: 'Soccor match',
                            start: new Date(y, m, 5),
                            allDay: true,
                            className: 'bgm-purple'
                        },
                        {
                            title: 'Coffee time',
                            start: new Date(y, m, 21),
                            allDay: true,
                            className: 'bgm-orange'
                        },
                        {
                            title: 'Job Interview',
                            start: new Date(y, m, 5),
                            allDay: true,
                            className: 'bgm-dark'
                        },
                        {
                            title: 'IT Meeting',
                            start: new Date(y, m, 5),
                            allDay: true,
                            className: 'bgm-cyan'
                        },
                        {
                            title: 'Brunch at Beach',
                            start: new Date(y, m, 1),
                            allDay: true,
                            className: 'bgm-purple'
                        },
                        {
                            title: 'Live TV Show',
                            start: new Date(y, m, 15),
                            allDay: true,
                            className: 'bgm-orange'
                        },
                        {
                            title: 'Software Conference',
                            start: new Date(y, m, 25),
                            allDay: true,
                            className: 'bgm-blue'
                        },
                        {
                            title: 'Coffee time',
                            start: new Date(y, m, 30),
                            allDay: true,
                            className: 'bgm-orange'
                        },
                        {
                            title: 'Job Interview',
                            start: new Date(y, m, 30),
                            allDay: true,
                            className: 'bgm-dark'
                        },
                    ],

                    //On Day Select
                    select: function (start, end, allDay) {
                        scope.select({
                            start: start,
                            end: end
                        });
                    }
                });


                //Add action links in calendar header
                element.find('.fc-toolbar').append($compile(scope.actionLinks)(scope));
            }
        }
    })


    //Change Calendar Views
    .directive('calendarView', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('click', function () {
                    $('#calendar').fullCalendar('changeView', attrs.calendarView);
                })
            }
        }
    });prmApp




    // =========================================================================
    // SWEATALERT
    // =========================================================================

    //Basic
    .directive('swalBasic', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.click(function () {
                    swal("Here's a message!");
                });
            }
        }
    })

    //A title with a text under
    .directive('swalText', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.click(function () {
                    swal("Here's a message!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis")

                });
            }
        }
    })

    //Success Message
    .directive('swalSuccess', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.click(function () {
                    swal("Good job!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis", "success")

                });
            }
        }
    })

    //Warning Message
    .directive('swalWarning', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.click(function () {
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    }, function () {
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    });
                });
            }
        }
    })

    //Parameter
    .directive('swalParams', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.click(function () {
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        } else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                    });
                });
            }
        }
    })

    //Custom Image
    .directive('swalImg', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.click(function () {
                    swal({
                        title: "Sweet!",
                        text: "Here's a custom image.",
                        imageUrl: "img/thumbs-up.png"
                    });
                });
            }
        }
    })

    //Auto Close Timer
    .directive('swalTimer', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.click(function () {
                    swal({
                        title: "Auto close alert!",
                        text: "I will close in 2 seconds.",
                        timer: 2000,
                        showConfirmButton: false
                    });
                });
            }
        }
    })



    // =========================================================================
    // GROWL
    // =========================================================================

    .directive('growlDemo', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                function notify(from, align, icon, type, animIn, animOut) {
                    $.growl({
                        icon: icon,
                        title: ' Bootstrap Growl ',
                        message: 'Turning standard Bootstrap alerts into awesome notifications',
                        url: ''
                    }, {
                            element: 'body',
                            type: type,
                            allow_dismiss: true,
                            placement: {
                                from: from,
                                align: align
                            },
                            offset: {
                                x: 20,
                                y: 85
                            },
                            spacing: 10,
                            z_index: 1031,
                            delay: 2500,
                            timer: 1000,
                            url_target: '_blank',
                            mouse_over: false,
                            animate: {
                                enter: animIn,
                                exit: animOut
                            },
                            icon_type: 'class',
                            template: '<div data-growl="container" class="alert" role="alert">' +
                            '<button type="button" class="close" data-growl="dismiss">' +
                            '<span aria-hidden="true">&times;</span>' +
                            '<span class="sr-only">Close</span>' +
                            '</button>' +
                            '<span data-growl="icon"></span>' +
                            '<span data-growl="title"></span>' +
                            '<span data-growl="message"></span>' +
                            '<a href="#" data-growl="url"></a>' +
                            '</div>'
                        });
                }

                element.on('click', function (e) {
                    e.preventDefault();

                    var nFrom = attrs.from;
                    var nAlign = attrs.align;
                    var nIcons = attrs.icon;
                    var nType = attrs.type;
                    var nAnimIn = attrs.animationIn;
                    var nAnimOut = attrs.animationOut;

                    notify(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut);

                })


            }
        }
    });prmApp.factory("fileReader",
                   ["$q", "$log", function ($q, $log) {
 
        var onLoad = function(reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };
 
        var onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };
 
        var onProgress = function(reader, scope) {
            return function (event) {
                scope.$broadcast("fileProgress",
                    {
                        total: event.total,
                        loaded: event.loaded
                    });
            };
        };
 
        var getReader = function(deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };
 
        var readAsDataURL = function (file, scope) {
            var deferred = $q.defer();
             
            var reader = getReader(deferred, scope);         
            reader.readAsArrayBuffer(file);
             
            return deferred.promise;
        };
 
        return {
            readAsDataUrl: readAsDataURL  
        };
    }]);

prmApp

    .directive('fgLine', function () {
        return {
            restrict: 'C',
            link: function (scope, element) {
                if ($('.fg-line')[0]) {
                    $('body').on('focus', '.form-control', function () {
                        $(this).closest('.fg-line').addClass('fg-toggled');
                    })

                    $('body').on('blur', '.form-control', function () {
                        var p = $(this).closest('.form-group');
                        var i = p.find('.form-control').val();

                        if (p.hasClass('fg-float')) {
                            if (i.length == 0) {
                                $(this).closest('.fg-line').removeClass('fg-toggled');
                            }
                        }
                        else {
                            $(this).closest('.fg-line').removeClass('fg-toggled');
                        }
                    });
                }

            }
        }

    })



    // =========================================================================
    // AUTO SIZE TEXTAREA
    // =========================================================================

    .directive('autoSize', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                if (element[0]) {
                    autosize(element);
                }
            }
        }
    })


    // =========================================================================
    // BOOTSTRAP SELECT
    // =========================================================================

    .directive('selectPicker', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                //if (element[0]) {
                element.selectpicker();
                //}
            }
        }
    })


    // =========================================================================
    // INPUT MASK
    // =========================================================================

    .directive('inputMask', function () {
        return {
            restrict: 'A',
            scope: {
                inputMask: '='
            },
            link: function (scope, element) {
                element.mask(scope.inputMask.mask);
            }
        }
    })


    // =========================================================================
    // COLOR PICKER
    // =========================================================================

    .directive('colordPicker', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                $(element).each(function () {
                    var colorOutput = $(this).closest('.cp-container').find('.cp-value');
                    $(this).farbtastic(colorOutput);
                });

            }
        }
    })



    // =========================================================================
    // PLACEHOLDER FOR IE 9 (on .form-control class)
    // =========================================================================

    .directive('formControl', function () {
        return {
            restrict: 'C',
            link: function (scope, element, attrs) {
                if (angular.element('html').hasClass('ie9')) {
                    $('input, textarea').placeholder({
                        customClass: 'ie9-placeholder'
                    });
                }
            }

        }
    });prmApp.directive('mediaElement', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {
            element.mediaelementplayer();
        }
    }

});


prmApp.directive('lightbox', function () {
    return {
        restrict: 'C',
        link: function (scope, element) {
            element.lightGallery({
                enableTouch: true
            });
        }
    }

});prmApp
.factory('SignalRFactory',
    ["$http", "$rootScope", "$location", "Hub", "$timeout", "$q","$log", "signalR",
    function ($http, $rootScope, $location, Hub, $timeout, $q, $log, signalR) {
        function backendFactory(serverUrl, hubName) {
            var isConnected = false;
            var connection = $.hubConnection(signalR);
            var proxy = connection.createHubProxy(hubName);
            proxy.on('checkRequirement', function(req) {
                return req;
            });
            proxy.on('checkComment', function(obj){
                return obj;
            })
            connection.start().done(function () {
                isConnected = true;
                $log.info("Angular Service Created");
            });
            return {
                on: function (eventName, callback) {
                    if (connection.state == 1) {
                        proxy.on(eventName, function (result) {
                            $rootScope.$apply(function () {
                                if (callback) {
                                    callback(result);
                                }
                            });
                        });
                    } else {
                        connection.start().done(function () {
                            proxy.on(eventName, function (result) {
                                $rootScope.$apply(function () {
                                    if (callback) {
                                        callback(result);
                                    }
                                });
                            });
                        })
                    }
                    
                },
                off: function (eventName, callback) {
                    proxy.off(eventName, function (result) {
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback(result);
                            }
                        });
                    });
                },
                invoke: function (methodName, args, callback) {
                    if (connection.state == 1) {
                        proxy.invoke(methodName, args)
                        .done(function (result) {
                            $rootScope.$apply(function () {
                                if (callback) {
                                    callback(result);
                                }
                            });
                        });
                    } else {
                        connection.start().done(function () {
                            proxy.invoke(methodName, args)
                            .done(function (result) {
                                $rootScope.$apply(function () {
                                    if (callback) {
                                        callback(result);
                                    }
                                });
                            });
                        })
                    }
                    
                },
                stop: function () {
                    //////////console.log(connection.stop());
                    connection.stop();
                    isConnected = false;
                },
                getStatus: function () {
                    return connection.state;
                },
                reconnect: function(){
                    connection.start().done(function () {
                        isConnected = true;
                        $log.info("Angular Service Reconnected");
                        return true;
                    });
                },
                connection: connection
            };
            
        };

        return backendFactory;
}]);prmApp

    // =========================================================================
    // LAYOUT
    // =========================================================================
    
    .directive('changeLayout', function(){
        
        return {
            restrict: 'A',
            scope: {
                changeLayout: '='
            },
            
            link: function(scope, element, attr) {
                
                //Default State
                if(scope.changeLayout === '1') {
                    element.prop('checked', true);
                }
                
                //Change State
                element.on('change', function(){
                    if(element.is(':checked')) {
                        localStorage.setItem('ma-layout-status', 1);
                        scope.$apply(function(){
                            scope.changeLayout = '1';
                        })
                    }
                    else {
                        localStorage.setItem('ma-layout-status', 0);
                        scope.$apply(function(){
                            scope.changeLayout = '0';
                        })
                    }
                })
            }
        }
    })



    // =========================================================================
    // MAINMENU COLLAPSE
    // =========================================================================

    .directive('toggleSidebar', function(){

        return {
            restrict: 'A',
            scope: {
                modelLeft: '=',
                modelRight: '='
            },
            
            link: function(scope, element, attr) {
                element.on('click', function(){
 
                    if (element.data('target') === 'mainmenu') {
                        if (scope.modelLeft === false) {
                            scope.$apply(function(){
                                scope.modelLeft = true;
                            })
                        }
                        else {
                            scope.$apply(function(){
                                scope.modelLeft = false;
                            })
                        }
                    }
                    
                    if (element.data('target') === 'chat') {
                        if (scope.modelRight === false) {
                            scope.$apply(function(){
                                scope.modelRight = true;
                            })
                        }
                        else {
                            scope.$apply(function(){
                                scope.modelRight = false;
                            })
                        }
                        
                    }
                })
            }
        }
    
    })
    

    
    // =========================================================================
    // SUBMENU TOGGLE
    // =========================================================================

    .directive('toggleSubmenu', function(){

        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.click(function(){
                    element.next().slideToggle(200);
                    element.parent().toggleClass('toggled');
                });
            }
        }
    })


    // =========================================================================
    // STOP PROPAGATION
    // =========================================================================
    
    .directive('stopPropagate', function(){
        return {
            restrict: 'C',
            link: function(scope, element) {
                element.on('click', function(event){
                    event.stopPropagation();
                });
            }
        }
    })

    .directive('aPrevent', function(){
        return {
            restrict: 'C',
            link: function(scope, element) {
                element.on('click', function(event){
                    event.preventDefault();
                });
            }
        }
    })


    // =========================================================================
    // PRINT
    // =========================================================================
    
    .directive('print', function(){
        return {
            restrict: 'A',
            link: function(scope, element){
                element.click(function(){
                    window.print();
                })   
            }
        }
    })


    // =========================================================================
    // REMOVE INCREMENT DECREMENT IN INPUT FEILD NUMBER
    // =========================================================================

    .directive('input', function () {
        return {
            restrict: 'E',
            scope: {
                type: '@'
            },
            link: function (scope, element) {
                if (scope.type == 'number') {
                    element.on('focus', function () {
                        angular.element(this).on('mousewheel', function (e) {
                            e.preventDefault();
                        });
                    });
                    element.on('blur', function () {
                        angular.element(this).off('mousewheel');
                    });
                }
            }
        }
    })


    .directive('miniItems', function(){
        return{
            restrict: 'EA',
            scope:
            {
                item:'=',
                timeleft:'='
            },
            templateUrl:'views/mini-item.html',
            controller: ['$scope', '$element', '$attrs', '$timeout', 'userService', '$http', 'auctionsService', 'SignalRFactory', 'domain', function ($scope, $element, $attrs, $timeout, userService, $http, auctionsService, SignalRFactory, domain) {
                // var requirementHub = SignalRFactory('', 'requirementHub');
                $scope.countdown1 = 2;
                $scope.auctionStarted = $scope.item.auctionStarted;
                $scope.stopBids = function(){
                    swal({
                        title: "Are you sure?",
                        text: "The Auction will be stopped after one minute.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, Stop Bids!",
                        closeOnConfirm: false
                    }, function () {                        
                        $scope.$broadcast('timer-set-countdown-seconds', 60);
                        $scope.disableButtons();
                        var params={};
                        params.reqID=$scope.item.auctionID;
                        params.sessionID=userService.getUserToken();
                        params.userID=userService.getUserId();
                        params.newTicks=60;
                        var parties = params.reqID + "$" + params.userID + "$" + params.newTicks + "$" + params.sessionID;
                        auctionsService.updatebidtime(params).then(function(req){
                            //console.log('requirement updated for item' + req.requirementID);
                        });
                        // auctionsService.updatebidtime(params);
                        // swal("Done!", "Auction time reduced to oneminute.", "success");
                    });
                };
                $scope.updateTime = function(timerId,time){
                    var isDone = false;
                    $scope.$on('timer-tick', function (event, args) {
                        if(!isDone){
                            addCDSeconds($scope.item.auctionID,time);
                            isDone = true;
                            var params={};
                            params.reqID=$scope.item.auctionID;
                            params.sessionID=userService.getUserToken();
                            params.userID=userService.getUserId();
                            params.newTicks=($scope.countdownVal + time);
                            var parties = params.reqID + "$" + params.userID + "$" + params.newTicks + "$" + params.sessionID;
                            auctionsService.updatebidtime(params).then(function(req){
                                //console.log('requirement updated for item' + req.requirementID);
                            });
                            //auctionsService.updatebidtime(params);
                        }                        
                    });
                }
                
                $scope.$on('timer-tick', function (event, args) {
                    $scope.countdownVal = event.targetScope.countdown;
                    if(event.targetScope.countdown < 61 && !$scope.disableStopBids && !$scope.disablereduceTime){
                        $timeout($scope.disableButtons(),1000);
                    }
                    if (event.targetScope.countdown <= 0) {
                        if ($scope.auctionStarted && ($scope.item.status == "CLOSED" || $scope.item.status == "STARTED")) {
                            //$scope.item.minPrice = $scope.item.auctionVendors[0].runningPrice;
                            /*if (($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() ) && $scope.vendorRank == 1) {
                                swal("Negotiation Completed!", "Congratulations! you are the least bidder for this requirement. Your price is : " + $scope.auctionItem.minPrice + "\n Customer would be reaching out you shortly. All the best!", "success");
                            } else if (($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() ) && $scope.vendorRank != 1) {
                                swal("Negotiation Completed!", "Bidding Completed.\n Thank You for your interest on this requirement. You ranked " + $scope.vendorRank + " in this requirement. Thank you for your participation.", "success");
                            } 
                            else*/
                            if (($scope.item.customerID == userService.getUserId())) {
                                var params = {};
                                params.reqID = $scope.item.auctionID;
                                params.sessionID = userService.getUserToken();
                                params.userID = userService.getUserId();
                                //var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                                $http({
                                    method: 'POST',
                                    url: domain + 'endnegotiation',
                                    headers: { 'Content-Type': 'application/json' },
                                    encodeURI: true,
                                    data: params
                                }).then(function (response) {
                                    window.location.reload();
                                });
                                //requirementHub.invoke('EndNegotiation', parties, function () {
                                //    $scope.$broadcast('timer-set-countdown-seconds', 0);
                                //    //swal("Negotiation Completed!", "Congratulations! you procurement process is now completed. " + $scope.toprankerName + " is the least bider with the value " + $scope.auctionItem.minPrice + " \n Your savings through PRM :" + ($scope.vendorInitialPrice - $scope.auctionItem.minPrice), "success");
                                //});

                            }
                        } else if ($scope.item.status == "NOTSTARTED") {
                            window.location.reload();
                        }

                        //$scope.getData();
                    }
                    if (event.targetScope.countdown <= 120) {
                        $scope.timerStyle = { 'color': '#f00' };
                    }
                    if (event.targetScope.countdown > 120 && $scope.item.status == 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#000' };
                    }
                    if (event.targetScope.countdown > 120 && $scope.item.status != 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#228B22' };
                    }
                    if (event.targetScope.countdown <= 60 && $scope.item.status == 'STARTED') {
                        $scope.disableDecreaseButtons = true;
                    }
                    if (event.targetScope.countdown > 60 && $scope.item.status == 'STARTED') {
                        $scope.disableDecreaseButtons = false;
                    }
                    if (event.targetScope.countdown <= 0 && $scope.item.status == 'NOTSTARTED') {
                        $scope.showTimer = false;

                        //$scope.getData();
                        window.location.reload();
                    }
                    if (event.targetScope.countdown > 0) {
                        //$scope.getData();
                        $scope.disableAddButton = false;
                    }
                });
                
                $scope.disableButtons = function(){
                    $scope.disablereduceTime = true;
                    $scope.disableStopBids = true;
                }                
                $scope.userType = userService.getUserType();
                if($scope.item.customerID!=userService.getUserId()){
                    $scope.customerBtns=false;   
                    $scope.vendorBtns = true; 
                } else {
                    $scope.customerBtns=true;  
                    $scope.vendorBtns = false;
                }
                $scope.cancelBid = function(itemId){
                    swal({
                        title: "Are you sure?",
                        text: "The Auction will be cancelled and vendors will be notified.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, Cancel.",
                        closeOnConfirm: false
                    }, function () {                        
                        $scope.$broadcast('timer-set-countdown-seconds', 0);
                        $scope.disableButtons();
                        var params={};
                        params.reqID=$scope.item.auctionID;
                        params.sessionID=userService.getUserToken();
                        params.userID=userService.getUserId();
                        params.newTicks=-1;
                        var parties = params.reqID + "$" + params.userID + "$" + params.newTicks + "$" + params.sessionID;
                        auctionsService.updatebidtime(params).then(function(req){
                            //console.log('requirement updated for item' + req.requirementID);
                        });
                        // auctionsService.updatebidtime(params);
                        // swal("Done!", "Requirement cancelled.", "success");
                    });
                }
            }]
        }
    })

    .directive('miniItemsCompanyLeads', function () {
        return {
            restrict: 'EA',
            scope:
            {
                item: '=',
                timeleft: '='
            },
            templateUrl: 'views/mini-item-company-leads.html',
            controller: ['$scope', '$element', '$attrs', '$timeout', 'userService', 'auctionsService', function ($scope, $element, $attrs, $timeout, userService, auctionsService) {
                $scope.countdown1 = 2;
                $scope.auctionStarted = $scope.item.auctionStarted;
                $scope.stopBids = function () {
                    swal({
                        title: "Are you sure?",
                        text: "The Auction will be stopped after one minute.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, Stop Bids!",
                        closeOnConfirm: false
                    }, function () {
                        $scope.$broadcast('timer-set-countdown-seconds', 60);
                        $scope.disableButtons();
                        var params = {};
                        params.reqID = $scope.item.auctionID;
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        params.newTicks = 60
                        ; auctionsService.updatebidtime(params);
                        swal("Done!", "Auction time reduced to oneminute.", "success");
                    });
                };
                $scope.updateTime = function (timerId, time) {
                    var isDone = false;
                    $scope.$on('timer-tick', function (event, args) {
                        if (!isDone) {
                            addCDSeconds($scope.item.auctionID, time);
                            isDone = true;
                            var params = {};
                            params.reqID = $scope.item.auctionID;
                            params.sessionID = userService.getUserToken();
                            params.userID = userService.getUserId();
                            params.newTicks = ($scope.countdownVal + time);
                            auctionsService.updatebidtime(params);
                        }
                    });
                }

                $scope.$on('timer-tick', function (event, args) {
                    $scope.countdownVal = event.targetScope.countdown;
                    if (event.targetScope.countdown < 61 && !$scope.disableStopBids && !$scope.disablereduceTime) {
                        $timeout($scope.disableButtons(), 1000);
                    }
                });

                $scope.disableButtons = function () {
                    $scope.disablereduceTime = true;
                    $scope.disableStopBids = true;
                }
                $scope.userType = userService.getUserType();
                if ($scope.userType == "VENDOR") {
                    $scope.customerBtns = false;
                    $scope.vendorBtns = true;
                } else {
                    $scope.customerBtns = true;
                    $scope.vendorBtns = false;
                }
                $scope.cancelBid = function (itemId) {
                    swal({
                        title: "Are you sure?",
                        text: "The Auction will be cancelled and vendors will be notified.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, Cancel.",
                        closeOnConfirm: false
                    }, function () {
                        $scope.$broadcast('timer-set-countdown-seconds', 0);
                        $scope.disableButtons();
                        var params = {};
                        params.reqID = $scope.item.auctionID;
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        params.newTicks = -1;
                        ; auctionsService.updatebidtime(params);
                        swal("Done!", "Requirement cancelled.", "success");
                    });
                }
            }]
        }
    })




   .directive('miniItemsTechevaluation', function () {
       return {
           restrict: 'EA',
           scope:
           {
               item: '=',
               callbackFn: '=method',
               callbackFn2: '=method2',
               callbackFn3: '=method3'

           },
           templateUrl: 'views/techevalviews/mini-item-techevaluation.html',
           controller: ['$scope', '$element', '$stateParams', '$attrs', '$timeout', 'userService', 'techevalService', 'fileReader',
               function ($scope, $element, $stateParams, $attrs, $timeout, userService, techevalService, fileReader) {
               $scope.evalID = $stateParams.evalID == "" ? 0 : $stateParams.evalID;

               $scope.compID = userService.getUserCompanyId();
               $scope.sessionID = userService.getUserToken();
               $scope.isCustomer = userService.getUserType();
               
               
               $scope.answers = [
                   //answer = $scope.answer
               ];
               
               $scope.directiveToggle = function (option, list, qid) {
                   $scope.callbackFn(option, list, qid);
               };
               
               $scope.directiveRadio = function (option, qid) {
                   $scope.callbackFn2(option, qid);
               };
               
               $scope.directiveDesc = function (option, qid) {
                   $scope.callbackFn3(option, qid);
               };

               $scope.doCheck = function (option, list) {
                   if (list.indexOf(option) > -1) {
                       return true;
                   }
                   else
                   {
                       return false;
                   }
               };

               $scope.getFile = function () {

                   $scope.file = $("#attachment_" + $scope.item.answerID)[0].files[0];

                   fileReader.readAsDataUrl($scope.file, $scope)
                       .then(function (result) {
                           var bytearray = new Uint8Array(result);
                           $scope.item.attachment = $.makeArray(bytearray);
                           $scope.item.attachmentName = $scope.file.name;
                       });
               };

               $scope.getFile1 = function (id, itemid, ext) {
                   $scope.file = $("#" + id)[0].files[0];
                   fileReader.readAsDataUrl($scope.file, $scope)
                       .then(function (result) {
                           var bytearray = new Uint8Array(result);
                           var arrayByte = $.makeArray(bytearray);
                           var ItemFileName = $scope.file.name;
                           var index = _.indexOf($scope.answers, _.find($scope.answers, function (o) { return o.answerID == id; }));
                           var obj = $scope.item;
                           obj.attachment = arrayByte;
                           obj.attachmentName = ItemFileName;
                           $scope.answers.splice(index, 1, obj);
                       });
               }

           }]
       }
    })
    .directive('miniItemsTechevalQuestions', function () {
        return {
            restrict: 'EA',
            scope:
            {
                item: '=',
                callbackFn: '=method',
                callbackFn2: '=method2',
                callbackFn3: '=method3'

            },
            templateUrl: 'views/techevalviews/mini-item-techeval-questions.html',
            controller: ['$scope', '$element', '$stateParams', '$attrs', '$timeout', 'userService', 'techevalService',
                function ($scope, $element, $stateParams, $attrs, $timeout, userService, techevalService) {
                    $scope.evalID = $stateParams.evalID == "" ? 0 : $stateParams.evalID;

                    $scope.compID = userService.getUserCompanyId();
                    $scope.sessionID = userService.getUserToken();
                    $scope.isCustomer = userService.getUserType();


                    $scope.answers = [
                        //answer = $scope.answer
                    ];

                    $scope.deleteQuestion = function (item) {
                        $scope.callbackFn(item);
                    };

                    $scope.editQuestion = function (item) {
                        $scope.callbackFn2(item);
                    };

                    $scope.directiveDesc = function (option, qid) {
                        $scope.callbackFn3(option, qid);
                    };

                    $scope.doCheck = function (option, list) {
                        if (list.indexOf(option) > -1) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    };

                }]
        }
    })
    .directive('prmConfigSelect', function ($timeout, $log, userService, auctionsService) { //http://jsfiddle.net/ndxbxrme/hprn3d9b/2/
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                configKey1: '=configkey',
                ngModel: '='
            },
            template: '<div class="prmDropdown" ng-click="open=!open" ng-class="{open:open}"><div ng-repeat="thing in list" style="top: {{($index + 1) * height}}px; -webkit-transition-delay: {{(list.length - $index) * 0.03}}s; z-index: {{list.length - $index}}" ng-hide="!open" ng-click="update(thing)" ng-class="{selected:selected===thing.configValue}"><span>{{thing.configText}}</span></div><span class="title" style="top: 0px; z-index: {{list.length + 1}}"><span>{{selectedText}}</span></span><span class="clickscreen" ng-hide="!open">&nbsp;</span></div>',
            replace: true,
            link: function (scope, elem, attrs, ngModel) {
                attrs.$observe('configkey', function (key) {

                    //scope.list = [
                    //    { compConfigID: 0, configKey: key, configValue: 'Select Value', configText: 'Select Value', CompID: 0 }
                    //];
                    scope.list = [];

                    auctionsService.GetCompanyConfiguration(userService.getUserCompanyId(), key, userService.getUserToken())
                        .then(function (response) {
                            //ngModel.$setViewValue("Select Value");
                            scope.list = scope.list.concat(response);
                            modalFunc();
                        });
                });

                scope.height = elem[0].offsetHeight;
                var modalFunc = function () {
                    scope.selected = ngModel.$modelValue;
                    $.each(scope.list, function (index, value) {
                        if (value.configValue == ngModel.$modelValue) {
                            scope.selectedText = value.configText;
                        }
                    });

                };

                scope.$watch('ngModel', modalFunc);
                scope.update = function (thing) {
                    ngModel.$setViewValue(thing.configValue);
                    ngModel.$render();
                };

                


            }
        };
    })
    .directive('numberInputVal', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                var v = 0;
                scope.$watch(attrs.ngModel, function (val) {
                    v = val;
                });
                var maxValue = (attrs.max == undefined || attrs.max == null) ? 0 : Number(attrs.max);
                var minValue = (attrs.min === undefined || attrs.min === null) ? 0 : Number(attrs.min);
                v = (v === undefined || v === null) ? 0 : v;
                element.bind('blur', function () {
                    if (v==null || v=="" || isNaN(v) || v < minValue || v > maxValue) {
                        ngModel.$setViewValue(0);
                        ngModel.$render()
                    }
                    else {
                        ngModel.$setViewValue(v);
                        ngModel.$render()
                    }
                });
            }
        };
    })

    .directive('escClear', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                var v = 0;
                scope.$watch(attrs.ngModel, function (val) {
                    v = val;
                });
                var maxValue = (attrs.max == undefined || attrs.max == null) ? 0 : Number(attrs.max);
                var minValue = (attrs.min === undefined || attrs.min === null) ? 0 : Number(attrs.min);
                v = (v === undefined || v === null) ? 0 : v;
                element.bind('keydown keypress', function (event) {
                    if (event.which === 27) { // 27 = esc key
                        ngModel.$setViewValue("");
                        ngModel.$render()
                        event.preventDefault();
                    }
                });
            }
        };
    })

    .directive('dateTimePicker', function () {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      recipient: '='
    },link: function(scope, element, attrs, itemCtrl) {
      var input = element.find('input');
      
      input.datetimepicker({
        format: "mm/dd/yyyy hh:ii"
      });
 
      element.bind('blur keyup change', function(){
        itemCtrl.startTime = input.val();
      });
    }
  }
})
.directive("owlCarousel", function() {
    return {
        restrict: 'E',
        transclude: false,
        link: function (scope) {
            scope.initCarousel = function(element) {
              // provide any default options you want
                var defaultOptions = {
                };
                var customOptions = scope.$eval($(element).attr('data-options'));
                // combine the two options objects
                for(var key in customOptions) {
                    defaultOptions[key] = customOptions[key];
                }
                // init carousel
                $(element).owlCarousel(defaultOptions);
            };
        }
    };
})
.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}])

.directive('owlCarouselItem', [function() {
    return {
        restrict: 'A',
        transclude: false,
        link: function(scope, element) {
          // wait for the last item in the ng-repeat then call init
            if(scope.$last) {
                scope.initCarousel(element.parent());
            }
        }
    };
}])
.directive("ngFileSelect",function(){
    return {
        link: function ($scope, el) {
            el.bind("change", function (e) {
                $scope.file = (e.srcElement || e.target).files[0];
                let fileName = ($scope.file && $scope.file.name) ? $scope.file.name.toLowerCase() : '';
                var allowedExtns = ['xlsx', 'xlx', 'pdf', 'txt', 'jpeg', 'jpg', 'png', 'gif', 'docx', 'doc'];
                var contains = allowedExtns.some(substring => fileName.includes(substring));
                if (contains) {
                    $scope.getFile();
                }
            });
        }
    };
}).directive("ngFileSelectDocs",function(){

  return {
    link: function($scope,el){
      el.bind("change", function(e){
        var id=e.currentTarget.attributes['id'].nodeValue;
        var doctype=e.currentTarget.attributes['doctype'].nodeValue;
        $scope.file = (e.srcElement || e.target).files[0];
        var ext = $scope.file.name.split('.').pop();  
        $scope.getFile1(id,doctype,ext);
      })      
    }    
  }
}).directive("ngFileSelectItems", function () {

    return {
        link: function ($scope, el) {
            el.bind("change", function (e) {
                var allowedExtns = ['xlsx', 'xlx', 'pdf', 'txt', 'jpeg', 'jpg', 'png', 'gif', 'docx', 'doc'];
                var id = e.currentTarget.attributes['id'].nodeValue;
                var itemid = e.currentTarget.attributes['itemid'].nodeValue;
                $scope.file = (e.srcElement || e.target).files[0];
                let fileName = ($scope.file && $scope.file.name) ? $scope.file.name.toLowerCase() : '';
                var contains = allowedExtns.some(substring => fileName.includes(substring));
                if (contains) {
                    var ext = $scope.file.name.split('.').pop();
                    $scope.getFile1(id, itemid, ext);
                }
            });
        }
    };
})

.directive('pwCheck', [function () {
    return {
      require: 'ngModel',
      link: function (scope, elem, attrs, ctrl) {
        var firstPassword = '#' + attrs.pwCheck;
        elem.add(firstPassword).on('keyup', function () {
          scope.$apply(function () {
            var v = elem.val()===$(firstPassword).val();
            ctrl.$setValidity('pwmatch', v);
          });
        });
      }
    }
  }]).directive("ngUniqueBlade", function(userService) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      element.bind('blur', function (e) {
        if (!ngModel || !element.val()) return;
        var type = attrs.ngUnique;
        var currentValue = element.val();
        userService.checkUniqueValue(type, currentValue)
          .then(function (unique) {
            //since the Ajax call was made
            if (currentValue == element.val()) {
              ngModel.$setValidity('unique', unique);
              scope.$broadcast('show-errors-check-validity');
            }
          });
      });
    }
  }
}).directive("ngUnique", function(userService) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      element.bind('blur', function (e) {
        if (!ngModel || !element.val()) return;
        var type = attrs.ngUnique;
        var currentValue = element.val();
        userService.checkUniqueValue(type, currentValue)
            .then(function (unique) {
            //since the Ajax call was made
                if (unique) {
                    ngModel.$setValidity('unique', unique);
                    scope.$broadcast('show-errors-check-validity');
                }
            });
        });
    }
  }
}).directive("stringToNumber", function (userService) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value);
            });
        }
    }
}).filter('ctime', function () {

  return function(jsonDate){
    var date='';
    if(jsonDate!=undefined )
    date = parseInt(jsonDate.substr(6));
    return date;
  };

})

.filter('numToCurrency', function($sce){
    
    return function (amount, currentCurrency) {
        if(isNaN(amount))
        {
            return amount;
        }

        if (currentCurrency && currentCurrency != 'INR')
        {
            var res = Number(amount.toFixed(2)).toLocaleString();
            var htmlCode = '&#36; ';
            if (currentCurrency == "USD") {
                htmlCode = '<span class="fa fa-dollar"></span> ';//'&#36; ';
            }
            else if (currentCurrency == "EUR") {
                htmlCode = '<span class="fa fa-eur"></span> ';//'&#8364; ';
            }
            else if (currentCurrency == "JPY") {
                htmlCode = '<span class="fa fa-cny"></span> ';//'&#165; ';
            }
            else if (currentCurrency == "GBP") {
                htmlCode = '<span class="fa fa-gbp"></span> ';//'&#163; ';
            }
            else if (currentCurrency == "CNY") {
                htmlCode = '<span class="fa fa-cny"></span> ';//'&#165; ';
            }

            return $sce.trustAsHtml(htmlCode + res);
        }
        else {
            x = amount;
            if (!x || isNaN(x)) {
                x = 0;
            }
            x = x.toString();
            var afterPoint = '';
            if (x.indexOf('.') > 0)
                afterPoint = x.substring(x.indexOf('.'), x.length);
            x = Math.floor(x);
            x = x.toString();
            var lastThree = x.substring(x.length - 3);
            var otherNumbers = x.substring(0, x.length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;

            var htmlCode = '<span class="fa fa-inr"></span> ';//'&#36; ';
            return $sce.trustAsHtml(htmlCode + res);
        }
  };

});