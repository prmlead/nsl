﻿using System;
using System.Text;
using Sustainsys.Saml2;
using System.IdentityModel.Services;

namespace PRMServices
{
    public partial class ssouser : System.Web.UI.Page
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        protected void Page_Load(object sender, EventArgs e)
        {

            

            string samlContent = string.Empty;
            logger.Info("Starting");
            Response.Write("<br/><br/>Starting");

            logger.Info("URL:" + Request.UrlReferrer);
            Response.Write("<br/>URL:" + Request.UrlReferrer);

            logger.Info("Request:" + Request.Form.Count);
            Response.Write("<br/>Request:" + Request.Form.Count);

            foreach (string entry in Request.Form)
            {
                logger.Info(entry + "----" + Request.Form[entry]);
                Response.Write("<br/>" + entry + "----" + Request.Form[entry]);

                if (entry.Equals("SAMLResponse", StringComparison.InvariantCultureIgnoreCase))
                {
                    samlContent = Request.Form[entry];
                }
            }


            if(!string.IsNullOrEmpty(samlContent))
            {
                byte[] samlData = Convert.FromBase64String(samlContent);
                Response.Write("after base 64 \n");
                logger.Info("after base 64:" + samlData);
                Response.Write(samlData);

                // read back into a UTF string
                string samlAssertion = Encoding.UTF8.GetString(samlData);
                Response.Write("saml assertion \n");
                logger.Info("saml assertion:" + samlAssertion);
                Response.Write(samlAssertion);
            }

            
        }
    }
}