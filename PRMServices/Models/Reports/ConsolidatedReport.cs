﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ConsolidatedReport : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "requirementNumber")]
        public string RequirementNumber { get; set; }

        [DataMember(Name = "requirementCurrency")]
        public string RequirementCurrency { get; set; }

        [DataMember(Name = "uID")]
        public int UID { get; set; }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                { return title; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { title = value; }
            }
        }

        [DataMember(Name = "reqPostedOn")]
        public DateTime? ReqPostedOn { get; set; }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "savings")]
        public double Savings { get; set; }

        [DataMember(Name = "noOfVendorsInvited")]
        public int NoOfVendorsInvited { get; set; }

        [DataMember(Name = "noOfvendorsParticipated")]
        public int NoOfvendorsParticipated { get; set; }


        [DataMember(Name = "quotationFreezTime")]
        public DateTime? QuotationFreezTime { get; set; }


        [DataMember(Name = "isNegotiationEnded")]
        public int IsNegotiationEnded { get; set; }


        [DataMember(Name = "closed")]
        public string Closed { get; set; }

        [DataMember(Name = "reqCategory")]
        public string ReqCategory { get; set; }


        string l1Name = string.Empty;
        [DataMember(Name = "L1Name")]
        public string L1Name
        {
            get
            {
                if (!string.IsNullOrEmpty(l1Name))
                { return l1Name; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { l1Name = value; }
            }
        }

        string l2Name = string.Empty;
        [DataMember(Name = "L2Name")]
        public string L2Name
        {
            get
            {
                if (!string.IsNullOrEmpty(l2Name))
                { return l2Name; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { l2Name = value; }
            }
        }


        [DataMember(Name = "l1RevPrice")]
        public double L1RevPrice { get; set; }


        [DataMember(Name = "l2RevPrice")]
        public double L2RevPrice { get; set; }

        [DataMember(Name = "l1InitialBasePrice")]
        public double L1InitialBasePrice { get; set; }

        [DataMember(Name = "l2InitialBasePrice")]
        public double L2InitialBasePrice { get; set; }


        [DataMember(Name = "initialL1BasePrice")]
        public double InitialL1BasePrice { get; set; }

        //[DataMember(Name = "initialUser")]
        //public RequirementUsersByRank InitialUser { get; set; }

        //[DataMember(Name = "revisedUserL1")]
        //public RequirementUsersByRank RevisedUserL1 { get; set; }

        //[DataMember(Name = "revisedUserL2")]
        //public RequirementUsersByRank RevisedUserL2 { get; set; }


        #region Initial L1 START

        [DataMember(Name = "IL1_uID")]
        public int IL1_UID { get; set; }

        [DataMember(Name = "IL1_revBasePrice")]
        public double IL1_RevBasePrice { get; set; }

        [DataMember(Name = "IL1_initBasePrice")]
        public double IL1_InitBasePrice { get; set; }

        [DataMember(Name = "IL1_l1InitialBasePrice")]
        public double IL1_L1InitialBasePrice { get; set; }

        [DataMember(Name = "IL1_vendTotalPrice")]
        public double IL1_VendTotalPrice { get; set; }

        [DataMember(Name = "IL1_rank")]
        public int IL1_Rank { get; set; }

        [DataMember(Name = "IL1_quantity")]
        public double IL1_Quantity { get; set; }

        //[DataMember(Name = "IL1_companyName")]
        //public string IL1_CompanyName { get; set; }

        string IL1_companyName = string.Empty;
        [DataMember(Name = "IL1_companyName")]
        public string IL1_CompanyName
        {
            get
            {
                if (!string.IsNullOrEmpty(IL1_companyName))
                { return IL1_companyName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { IL1_companyName = value; }
            }
        }

        [DataMember(Name = "IL1_selectedVendorCurrency")]
        public string IL1_SelectedVendorCurrency { get; set; }

        #endregion Initial L1 END

        #region Revised RL1 START

        [DataMember(Name = "RL1_uID")]
        public int RL1_UID { get; set; }

        [DataMember(Name = "RL1_revBasePrice")]
        public double RL1_RevBasePrice { get; set; }

        [DataMember(Name = "RL1_initBasePrice")]
        public double RL1_InitBasePrice { get; set; }

        [DataMember(Name = "RL1_l1InitialBasePrice")]
        public double RL1_L1InitialBasePrice { get; set; }

        [DataMember(Name = "RL1_revVendTotalPrice")]
        public double RL1_RevVendTotalPrice { get; set; }

        [DataMember(Name = "RL1_rank")]
        public int RL1_Rank { get; set; }

        [DataMember(Name = "RL1_quantity")]
        public double RL1_Quantity { get; set; }

        //[DataMember(Name = "RL1_companyName")]
        //public string RL1_CompanyName { get; set; }

        string RL1_companyName = string.Empty;
        [DataMember(Name = "RL1_companyName")]
        public string RL1_CompanyName
        {
            get
            {
                if (!string.IsNullOrEmpty(RL1_companyName))
                { return RL1_companyName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { RL1_companyName = value; }
            }
        }

        [DataMember(Name = "RL1_selectedVendorCurrency")]
        public string RL1_SelectedVendorCurrency { get; set; }

        #endregion Revised RL1 END

        #region Revised RL2 START

        [DataMember(Name = "RL2_uID")]
        public int RL2_UID { get; set; }

        [DataMember(Name = "RL2_revBasePrice")]
        public double RL2_RevBasePrice { get; set; }

        [DataMember(Name = "RL2_initBasePrice")]
        public double RL2_InitBasePrice { get; set; }

        [DataMember(Name = "RL2_l1InitialBasePrice")]
        public double RL2_L1InitialBasePrice { get; set; }

        [DataMember(Name = "RL2_revVendTotalPrice")]
        public double RL2_RevVendTotalPrice { get; set; }

        [DataMember(Name = "RL2_rank")]
        public int RL2_Rank { get; set; }

        [DataMember(Name = "RL2_quantity")]
        public double RL2_Quantity { get; set; }

        //[DataMember(Name = "RL2_companyName")]
        //public string RL2_CompanyName { get; set; }

        string RL2_companyName = string.Empty;
        [DataMember(Name = "RL2_companyName")]
        public string RL2_CompanyName
        {
            get
            {
                if (!string.IsNullOrEmpty(RL2_companyName))
                { return RL2_companyName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { RL2_companyName = value; }
            }
        }

        [DataMember(Name = "RL2_selectedVendorCurrency")]
        public string RL2_SelectedVendorCurrency { get; set; }

        #endregion Revised RL2 END


        [DataMember(Name = "basePriceSavings")]
        public double BasePriceSavings { get; set; }

        [DataMember(Name = "savingsPercentage")]
        public double SavingsPercentage { get; set; }

        [DataMember(Name = "selectedVendorCurrency")]
        public string SelectedVendorCurrency { get; set; }

        [DataMember(Name = "l1BasePrice")]
        public double L1BasePrice { get; set; }

        [DataMember(Name = "l2BasePrice")]
        public double L2BasePrice { get; set; }

        [DataMember(Name = "productQuantity")]
        public double ProductQuantity { get; set; }

        [DataMember(Name = "productQuantityIn")]
        public string ProductQuantityIn { get; set; }

        [DataMember(Name = "POSTED_BY_USER")]
        public string POSTED_BY_USER { get; set; }

        [DataMember(Name = "REQ_CURRENCY")]
        public string REQ_CURRENCY { get; set; }
        
        [DataMember(Name = "EBIT_SAVINGS_IN_INR")]
        public double EBIT_SAVINGS_IN_INR { get; set; }

        [DataMember(Name = "EBIT_SAVINGS_IN_REQ_CURRENCY")]
        public double EBIT_SAVINGS_IN_REQ_CURRENCY { get; set; }
    }
    [DataContract]
    public class ConsolidatedReportNew
    {
        [DataMember][DataNames("REQ_ID")]
        public int REQ_ID { get; set; }
        
        [DataMember][DataNames("REQ_TITLE")]
        public string REQ_TITLE { get; set; }

        [DataMember][DataNames("REQ_POSTED_ON")]
        public DateTime? REQ_POSTED_ON { get; set; }

        [DataMember][DataNames("QUOTATION_FREEZ_TIME")]
        public DateTime? QUOTATION_FREEZ_TIME { get; set; }

        [DataMember] [DataNames("START_TIME")]
        public DateTime? START_TIME { get; set; }

        [DataMember] [DataNames("IS_CB_ENABLED")]
        public int IS_CB_ENABLED { get; set; }


        [DataMember][DataNames("END_TIME")]
        public DateTime? END_TIME { get; set; }


        [DataMember] [DataNames("REQ_STATUS")]
        public string REQ_STATUS { get; set; }

        [DataMember][DataNames("QUOTES_TO_BE_RECEIVED")]

        public int QUOTES_TO_BE_RECEIVED { get; set; }


        [DataMember][DataNames("TOTAL_ROWS")]
        public int TOTAL_ROWS { get; set; }


        [DataMember][DataNames("REQ_NUMBER")]
        public string REQ_NUMBER { get; set; }

        [DataMember][DataNames("SAVINGS")]
        public double SAVINGS { get; set; }


        [DataMember][DataNames("REQ_CURRENCY")]
        public string REQ_CURRENCY { get; set; }


        [DataMember][DataNames("EBIT_SAVINGS_IN_INR")]

        public double EBIT_SAVINGS_IN_INR { get; set; }


        [DataMember][DataNames("EBIT_SAVINGS_IN_REQ_CURRENCY")]
        public double EBIT_SAVINGS_IN_REQ_CURRENCY { get; set; }


        [DataMember][DataNames("VENDOR_PRICES")]
        public List<VendorPrices> VENDOR_PRICES { get; set; }

        [DataMember]
        [DataNames("L1_INITIAL_PRICES")]
        public VendorPrices L1_INITIAL_PRICES { get; set; }

        [DataMember][DataNames("TOTAL_VENDORS")]
        public List<TotalVendors> TOTAL_VENDORS { get; set; }


    }
    public class VendorPrices
    {
        [DataMember][DataNames("INITIAL_TOTAL_PRICE")]
        public double INITIAL_TOTAL_PRICE { get; set; }

        [DataMember][DataNames("REV_TOTAL_PRICE")]
        public double REV_TOTAL_PRICE { get; set; }

        [DataMember][DataNames("RN")]
        public int RN { get; set; }

        [DataMember][DataNames("COMP_NAME")]
        public string COMP_NAME { get; set; }

        [DataMember][DataNames("REQ_ID")]
        public int REQ_ID { get; set; }
    }

    public class TotalVendors
    {

        [DataMember][DataNames("TOTAL_VENDORS")]
        public int TOTAL_VENDORS { get; set; }

        [DataMember][DataNames("REQ_ID")]
        public int REQ_ID { get; set; }
    }
}

