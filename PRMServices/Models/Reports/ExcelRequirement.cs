﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ExcelRequirement : Entity
    {
        [DataMember(Name = "reqItems")]
        public ExcelRequirementItems[] ReqItems { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "requirementNumber")]
        public string RequirementNumber { get; set; }

        [DataMember(Name = "requirementCurrency")]
        public string RequirementCurrency { get; set; }

        [DataMember(Name = "reqVendors")]
        public ExcelVendorDetails[] ReqVendors { get; set; }

        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "postedByFirstName")]
        public string PostedByFirstName { get; set; }

        [DataMember(Name = "postedByLastName")]
        public string PostedByLastName { get; set; }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                { return title; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { title = value; }
            }
        }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "postedOn")]
        public DateTime? PostedOn { get; set; }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "savings")]
        public Double Savings { get; set; }

        [DataMember(Name = "currentDate")]
        public DateTime? CurrentDate { get; set; }

        [DataMember(Name = "preSavings")]
        public Double PreSavings { get; set; }

        [DataMember(Name = "postSavings")]
        public Double PostSavings { get; set; }

        [DataMember(Name = "IS_CB_ENABLED")]
        public int IS_CB_ENABLED { get; set; }

        [DataMember(Name = "IS_CB_COMPLETED")]
        public int IS_CB_COMPLETED { get; set; }

        string prCode = string.Empty;
        [DataMember(Name = "prCode")]
        public string PrCode
        {
            get
            {
                if (!string.IsNullOrEmpty(prCode))
                { return prCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { prCode = value; }
            }
        }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "isTechScoreReq")]
        public int IsTechScoreReq { get; set; }

        [DataMember(Name = "deliverLocation")]
        public string DeliverLocation { get; set; }

        [DataMember(Name = "CURRENCY_RATE_US")]
        public double CURRENCY_RATE_US { get; set; }

        [DataMember(Name = "CURRENCY_RATE_EURO")]
        public double CURRENCY_RATE_EURO { get; set; }

        [DataMember(Name = "DATETIME_NOW")]
        public DateTime? DATETIME_NOW { get; set; }

        [DataMember(Name = "custFirstName")]
        public string CustFirstName { get; set; }

        [DataMember(Name = "custLastName")]
        public string CustLastName { get; set; }
    }
}