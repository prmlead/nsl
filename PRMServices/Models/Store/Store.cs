﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class Store : ResponseAudit
    {
        [DataMember(Name = "storeID")]
        public int StoreID { get; set; }

        [DataMember(Name = "companyID")]
        public int CompanyID { get; set; }

        [DataMember(Name = "isMainBranch")]
        public int IsMainBranch { get; set; }

        [DataMember(Name = "mainStoreID")]
        public int MainStoreID { get; set; }

        string storeCode = string.Empty;
        [DataMember(Name = "storeCode")]
        public string StoreCode
        {
            get
            {
                if (!string.IsNullOrEmpty(storeCode))
                {
                    return storeCode;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    storeCode = value;
                }
            }
        }

        string storeDescription = string.Empty;
        [DataMember(Name = "storeDescription")]
        public string StoreDescription
        {
            get
            {
                if (!string.IsNullOrEmpty(storeDescription))
                {
                    return storeDescription;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    storeDescription = value;
                }
            }
        }

        string storeInCharge = string.Empty;
        [DataMember(Name = "storeInCharge")]
        public string StoreInCharge
        {
            get
            {
                if (!string.IsNullOrEmpty(storeInCharge))
                {
                    return storeInCharge;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    storeInCharge = value;
                }
            }
        }

        string storeDetails = string.Empty;
        [DataMember(Name = "storeDetails")]
        public string StoreDetails
        {
            get
            {
                if (!string.IsNullOrEmpty(storeDetails))
                {
                    return storeDetails;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    storeDetails = value;
                }
            }
        }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        string storeAddress = string.Empty;
        [DataMember(Name = "storeAddress")]
        public string StoreAddress
        {
            get
            {
                if (!string.IsNullOrEmpty(storeAddress))
                {
                    return storeAddress;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    storeAddress = value;
                }
            }
        }

        [DataMember(Name = "storeBranches")]
        public int StoreBranches { get; set; }
    }
}