﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class CustomProperty : ResponseAudit
    {
        [DataMember(Name = "custPropID")]
        public int CustPropID { get; set; }

        [DataMember(Name = "companyID")]
        public int CompanyID { get; set; }

        string _custPropCode = string.Empty;
        [DataMember(Name = "custPropCode")]
        public string CustPropCode
        {
            get
            {
                if (!string.IsNullOrEmpty(_custPropCode))
                {
                    return _custPropCode;
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _custPropCode = value;
                }
            }
        }

        string _custPropDesc = string.Empty;
        [DataMember(Name = "custPropDesc")]
        public string CustPropDesc
        {
            get
            {
                if (!string.IsNullOrEmpty(_custPropDesc))
                {
                    return _custPropDesc;
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _custPropDesc = value;
                }
            }
        }

        string _custPropType = string.Empty;
        [DataMember(Name = "custPropType")]
        public string CustPropType
        {
            get
            {
                if (!string.IsNullOrEmpty(_custPropType))
                {
                    return _custPropType;
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _custPropType = value;
                }
            }
        }

        string _custPropDefault = string.Empty;
        [DataMember(Name = "custPropDefault")]
        public string CustPropDefault
        {
            get
            {
                if (!string.IsNullOrEmpty(_custPropDefault))
                {
                    return _custPropDefault;
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _custPropDefault = value;
                }
            }
        }

        [DataMember(Name = "entityID")]
        public int EntityID { get; set; }

        string _custPropValue = string.Empty;
        [DataMember(Name = "custPropValue")]
        public string CustPropValue
        {
            get
            {
                if (!string.IsNullOrEmpty(_custPropValue))
                {
                    return _custPropValue;
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _custPropValue = value;
                }
            }
        }

        string _moduleName = string.Empty;
        [DataMember(Name = "moduleName")]
        public string ModuleName
        {
            get
            {
                if (!string.IsNullOrEmpty(_moduleName))
                {
                    return _moduleName;
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _moduleName = value;
                }
            }
        }

    }
}