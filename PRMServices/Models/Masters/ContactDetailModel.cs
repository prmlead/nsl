﻿using PRMServices.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models.Masters
{
    public class ContactDetailModel   : BaseEntityModel
    {

        public string Title { get; set; }

        public string Details { get; set; }

        public bool AllowEdit { get; set; }

        public bool Active { get; set; }

        public bool LimitedToProject { get; set; }

        public bool LimitedToUser { get; set; }

        public bool LimitedToDepartment { get; set; }

        public int[] AllowedUsers { get; set; }

        public int[] AllowedDepartments { get; set; }

        public int[] AllowedProjects { get; set; }

    }
}