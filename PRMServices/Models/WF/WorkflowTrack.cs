﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class WorkflowTrack : ResponseAudit
    {
        [DataMember(Name = "trackID")]
        public int TrackID { get; set; }

        [DataMember(Name = "approverID")]
        public int ApproverID { get; set; }

        [DataMember(Name = "approver")]
        public CompanyDesignations Approver { get; set; }

        [DataMember(Name = "department")]
        public CompanyDepartments Department { get; set; }

        [DataMember(Name = "workflow")]
        public Workflow WorkflowDetails { get; set; }

        string status = string.Empty;
        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                return string.IsNullOrEmpty(status) ? string.Empty : status;
            }
            set
            {
                status = value;
            }
        }

        [DataMember(Name = "order")]
        public int Order { get; set; }

        [DataMember(Name = "moduleID")]
        public int ModuleID { get; set; }

        [DataMember(Name = "REJECTED_ORDER")]
        public int REJECTED_ORDER { get; set; }

        string comments = string.Empty;
        [DataMember(Name = "comments")]
        public string Comments
        {
            get
            {
                return string.IsNullOrEmpty(comments) ? string.Empty : comments;
            }
            set
            {
                comments = value;
            }
        }

        string indNo = string.Empty;
        [DataMember(Name = "indNo")]
        public string IndNo
        {
            get
            {
                return string.IsNullOrEmpty(indNo) ? string.Empty : indNo;
            }
            set
            {
                indNo = value;
            }
        }

        string moduleCode = string.Empty;
        [DataMember(Name = "moduleCode")]
        public string ModuleCode
        {
            get
            {
                return string.IsNullOrEmpty(moduleCode) ? string.Empty : moduleCode;
            }
            set
            {
                moduleCode = value;
            }
        }

        string moduleName = string.Empty;
        [DataMember(Name = "moduleName")]
        public string ModuleName
        {
            get
            {
                return string.IsNullOrEmpty(moduleName) ? string.Empty : moduleName;
            }
            set
            {
                moduleName = value;
            }
        }

        [DataMember(Name = "approverDetails")]
        public User ApproverDetails { get; set; }


        string subModuleName = string.Empty;
        [DataMember(Name = "subModuleName")]
        public string SubModuleName
        {
            get
            {
                return string.IsNullOrEmpty(subModuleName) ? string.Empty : subModuleName;
            }
            set
            {
                subModuleName = value;
            }
        }


        [DataMember(Name = "subModuleID")]
        public int SubModuleID { get; set; }

        [DataMember(Name = "IS_LAST_APPROVER")]
        public bool IS_LAST_APPROVER { get; set; }

        [DataMember(Name = "IS_VALID")]
        public int IS_VALID { get; set; }

        [DataMember(Name = "EMAIL_TEMPLATE")]
        public String EMAIL_TEMPLATE { get; set; }

        [DataMember(Name = "EMAIL_URL")]
        public String EMAIL_URL { get; set; }
        

    }
}