﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class Workflow : ResponseAudit
    {
        [DataMember(Name = "workflowID")]
        public int WorkflowID { get; set; }

        [DataMember(Name = "companyID")]
        public int CompanyID { get; set; }

        //[DataMember(Name = "deptID")]
        //public new int DeptID { get; set; }

        string title = string.Empty;
        [DataMember(Name = "workflowTitle")]
        public string WorkflowTitle
        {
            get
            {
                return string.IsNullOrEmpty(title) ? string.Empty : title;
            }
            set
            {
                title = value;
            }
        }

        string details = string.Empty;
        [DataMember(Name = "WorkflowDetails")]
        public string WorkflowDetails
        {
            get
            {
                return string.IsNullOrEmpty(details) ? string.Empty : details;
            }
            set
            {
                details = value;
            }
        }

        string module = string.Empty;
        [DataMember(Name = "WorkflowModule")]
        public string WorkflowModule
        {
            get
            {
                return string.IsNullOrEmpty(module) ? string.Empty : module;
            }
            set
            {
                module = value;
            }
        }

        [DataMember(Name = "WorkflowStages")]
        public WorkflowStages[] Stages { get; set; }

        [DataMember(Name = "WorkflowTracks")]
        public WorkflowTrack[] Tracks { get; set; }

        [DataMember(Name = "WorkflowTrack")]
        public WorkflowTrack Track { get; set; }

        [DataMember(Name = "indent")]
        public MPIndent Indent { get; set; }

        [DataMember(Name = "trackWfId")]
        public int TrackWfId { get; set; }

        [DataMember(Name = "isActive")]
        public int IsActive { get; set; }

    }
}