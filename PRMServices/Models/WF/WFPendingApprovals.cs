﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class WFPendingApprovals : ResponseAudit
    {
        [DataMember(Name = "workflowID")]
        public int WorkflowID { get; set; }

        [DataMember(Name = "companyID")]
        public int CompanyID { get; set; }

        string workflowTitle = string.Empty;
        [DataMember(Name = "workflowTitle")]
        public string WorkflowTitle
        {
            get
            {
                return string.IsNullOrEmpty(workflowTitle) ? string.Empty : workflowTitle;
            }
            set
            {
                workflowTitle = value;
            }
        }

        string workflowStatus = string.Empty;
        [DataMember(Name = "workflowStatus")]
        public string WorkflowStatus
        {
            get
            {
                return string.IsNullOrEmpty(workflowStatus) ? string.Empty : workflowStatus;
            }
            set
            {
                workflowStatus = value;
            }
        }

        string workflowModule = string.Empty;
        [DataMember(Name = "workflowModule")]
        public string WorkflowModule
        {
            get
            {
                return string.IsNullOrEmpty(workflowModule) ? string.Empty : workflowModule;
            }
            set
            {
                workflowModule = value;
            }
        }

        [DataMember(Name = "workflowModuleID")]
        public int WorkflowModuleID { get; set; }

        string workflowModuleCode = string.Empty;
        [DataMember(Name = "workflowModuleCode")]
        public string WorkflowModuleCode
        {
            get
            {
                return string.IsNullOrEmpty(workflowModuleCode) ? string.Empty : workflowModuleCode;
            }
            set
            {
                workflowModuleCode = value;
            }
        }

        string workflowModuleUserStatus = string.Empty;
        [DataMember(Name = "workflowModuleUserStatus")]
        public string WorkflowModuleUserStatus
        {
            get
            {
                return string.IsNullOrEmpty(workflowModuleUserStatus) ? string.Empty : workflowModuleUserStatus;
            }
            set
            {
                workflowModuleUserStatus = value;
            }
        }

        [DataMember(Name = "workflowModuleViewID")]
        public int WorkflowModuleViewID { get; set; }

        [DataMember(Name = "reqId")]
        public int ReqId { get; set; }

        string qcsWorkFlow = string.Empty;
        [DataMember(Name = "qcsWorkFlow")]
        public string QcsWorkFlow
        {
            get
            {
                return string.IsNullOrEmpty(qcsWorkFlow) ? string.Empty : qcsWorkFlow;
            }
            set
            {
                qcsWorkFlow = value;
            }
        }

        string creatorName = string.Empty;
        [DataMember(Name = "creatorName")]
        public string CreatorName
        {
            get
            {
                return string.IsNullOrEmpty(creatorName) ? string.Empty : creatorName;
            }
            set
            {
                creatorName = value;
            }
        }
    }
}