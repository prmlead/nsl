﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class CompanyDesignations : ResponseAudit
    {        
        //DEPT_ID, DEPT_CODE, DEPT_DESC, COMP_ID, CREATED_BY, MODIFIED_BY, DATE_CREATED, DATE_MODIFIED 

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "desigID")]
        public int DesigID { get; set; }

        string _desigCode = string.Empty;
        [DataMember(Name = "desigCode")]
        public string DesigCode
        {
            get
            {
                return _desigCode;
            }
            set
            {
                _desigCode = value;
            }
        }

        string _desigDesc = string.Empty;
        [DataMember(Name = "desigDesc")]
        public string DesigDesc {
            get
            {
                return _desigDesc;
            }
            set
            {
                _desigDesc = value;
            }
        }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "selectedDesigType")]
        public CompanyDeptDesigTypes SelectedDesigType { get; set; }
    }
}