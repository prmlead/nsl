﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class DatabaseEntity : Entity
    {

        string table_schema = string.Empty;
        [DataMember(Name = "table_schema")]
        public string TABLE_SCHEMA
        {
            get
            {
                return table_schema;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    table_schema = value;
                }
            }
        }

        string table_name = string.Empty;
        [DataMember(Name = "table_name")]
        public string TABLE_NAME
        {
            get
            {
                return table_name;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    table_name = value;
                }
            }
        }

        string column_name = string.Empty;
        [DataMember(Name = "column_name")]
        public string COLUMN_NAME
        {
            get
            {
                return column_name;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    column_name = value;
                }
            }
        }

        string column_type = string.Empty;
        [DataMember(Name = "column_type")]
        public string COLUMN_TYPE
        {
            get
            {
                return column_type;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    column_type = value;
                }
            }
        }

        string column_default = string.Empty;
        [DataMember(Name = "column_default")]
        public string COLUMN_DEFAULT
        {
            get
            {
                return column_default;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    column_default = value;
                }
            }
        }

    }
}