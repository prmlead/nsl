﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class CurrencyFactors :Entity
    {
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "currencyFactorID")]
        public int CurrencyFactorID { get; set; }

        string currencyCode = string.Empty;
        [DataMember(Name = "currencyCode")]
        public string CurrencyCode { get; set; }

        string type = string.Empty;
        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "currencyFactor")]
        public decimal CurrencyFactor { get; set; }

        DateTime startDate = DateTime.MaxValue;
        [DataMember(Name = "startDate")]
        public DateTime? StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    startDate = value.Value;
                }
            }
        }

        DateTime endDate = DateTime.MaxValue;
        [DataMember(Name = "endDate")]
        public DateTime? EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    endDate = value.Value;
                }
            }
        }


        DateTime dateModified = DateTime.MaxValue;
        [DataMember(Name = "dateModified")]
        public DateTime? DateModified
        {
            get
            {
                return dateModified;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    dateModified = value.Value;
                }
            }
        }


        [DataMember(Name = "modifiedUser")]
        public string ModifiedUser { get; set; }
    }
}