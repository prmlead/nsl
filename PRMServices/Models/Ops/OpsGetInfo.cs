﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace PRMServices.Models
{
    [DataContract]
    public class OpsGetInfo : Entity
    {
        [DataMember(Name = "vendorName")]
        public string VendorName { get; set; }

        [DataMember(Name = "vendorPhoneNo")]
        public string VendorPhoneNo { get; set; }

        [DataMember(Name = "vendorAlternateNo")]
        public string VendorAlternateNo { get; set; }

        [DataMember(Name = "vendorMailID")]
        public string VendorMailID { get; set; }
    }
    
}