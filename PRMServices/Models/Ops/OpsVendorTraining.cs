﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models

{
    [DataContract]
    public class OpsVendorTraining : Entity 
    {
        [DataMember(Name = "VT_ID")]
        public int VT_ID { get; set; }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "reqID")]
        public int ReqID { get; set; }

        [DataMember(Name = "DATE_CREATED")]
        public DateTime? DATE_CREATED { get; set; }

        [DataMember(Name = "DATE_MODIFIED")]
        public DateTime? DATE_MODIFIED { get; set; }

        [DataMember(Name = "reqPostedDate")]
        public string ReqPostedDate { get; set; }

        [DataMember(Name = "trainingTime")]
        public DateTime? TrainingTime { get; set; }

        [DataMember(Name = "trainedContactName")]
        public string TrainedContactName { get; set; }

        [DataMember(Name = "contactPhone")]
        public string ContactPhone { get; set; }

        [DataMember(Name = "contactEmail")]
        public string ContactEmail { get; set; }

        [DataMember(Name = "vendorPhone")]
        public string VendorPhone { get; set; }

        [DataMember(Name = "vendorEmail")]
        public string VendorEmail { get; set; }

        [DataMember(Name = "trainerName")]
        public string TrainerName { get; set; }

        [DataMember(Name = "trainerID")]
        public string TrainerID { get; set; }

        [DataMember(Name = "tickectNo")]
        public string TickectNo { get; set; }

        [DataMember(Name = "comments")]
        public string Comments { get; set; }

        [DataMember(Name = "trainingMode")]
        public string TrainingMode { get; set; }

        [DataMember(Name = "agentID")]
        public string AgentID { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "clientName")]
        public string ClientName { get; set; }
    }
}