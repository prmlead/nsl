﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models


    {
    [DataContract]
    public class OpsClientVendorInfo : Entity 
    {
        [DataMember(Name = "vendorOnline")]
        public int VendorOnline { get; set; }

        [DataMember(Name = "newVendors")]
        public int NewVendors { get; set; }

        [DataMember(Name = "vendorTraining")]
        public int VendorTraining { get; set; }

        [DataMember(Name = "vendorSupport")]
        public int VendorSupport { get; set; }

     

        //[DataMember(Name = "dateModified")]
        //public DateTime DateModified { get; set; }

        //[DataMember(Name = "createdBy")]
        //public int CreatedBy { get; set; }

        //[DataMember(Name = "modifiedBy")]
        //public int ModifiedBy { get; set; }

        //[DataMember(Name = "isValid")]
        //public int IsValid { get; set; }

        //[DataMember(Name = "clientType")]
        //public string ClientType { get; set; }

        //[DataMember(Name = "clientStatus")]
        //public string ClientStatus { get; set; }

        //[DataMember(Name = "clientDB")]
        //public string ClientDB { get; set; }





    }
}