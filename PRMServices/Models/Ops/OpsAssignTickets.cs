﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models

{
    [DataContract]
    public class OpsAssignTickets : Entity 
    {
        
        [DataMember(Name = "FD_ID")]
        public int FD_ID { get; set; }

        [DataMember(Name = "source")]
        public int Source { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "created_at")]
        public string Created_at { get; set; }

        [DataMember(Name = "updated_at")]
        public string Updated_at { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "to_emails")]
        public string To_emails { get; set; }

        [DataMember(Name = "tags")]
        public string Tags { get; set; }

        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "sourceName")]
        public string SourceName { get; set; }

         

        [DataMember(Name = "tickectID")]
        public string TickectID { get; set; }

        [DataMember(Name = "comments")]
        public string Comments { get; set; }

        [DataMember(Name = "trainingMode")]
        public string TrainingMode { get; set; }


        [DataMember(Name = "responderName")]
        public string ResponderName { get; set; }

        [DataMember(Name = "responder_id")]
        public string Responder_id { get; set; }

        [DataMember(Name = "requester_id")]
        public string Requester_id { get; set; }

        [DataMember(Name = "group_id")]
        public string Group_id { get; set; }

        [DataMember(Name = "priority")]
        public string Priority { get; set; }

        [DataMember(Name = "refreshTime")]
        public string RefreshTime { get; set; }


        [DataMember(Name = "DATE_CREATED")]
        public DateTime? DATE_CREATED { get; set; }
    }
}