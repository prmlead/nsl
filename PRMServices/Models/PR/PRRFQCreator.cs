﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class PRRFQCreator : Entity
    {

        
        [DataMember] [DataNames("PR_ID")] public int PR_ID { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("U_NAME")] public string U_NAME { get; set; }
        [DataMember] [DataNames("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
        [DataMember] [DataNames("MODULE_ID")] public int MODULE_ID { get; set; }
        [DataMember] [DataNames("IS_ASSIGNED")] public int IS_ASSIGNED { get; set; }
        [DataMember] [DataNames("DEPT_ID")] public int DEPT_ID { get; set; }


        //DEPT_ID
    }
}