﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class Entity
    {
        string eMessage = string.Empty;
        [DataMember(Name = "errorMessage")]
        public string ErrorMessage
        {
            get
            {
                return this.eMessage;
            }
            set
            {
                this.eMessage = value;
            }
        }

        string sessionId = string.Empty;
        [DataMember(Name = "sessionID")]
        public string SessionID
        {
            get
            {
                return this.sessionId;
            }
            set
            {
                this.sessionId = value;
            }
        }

        private DateTime currentTime = DateTime.Now;
        [DataMember(Name = "currentTime")]
        public DateTime CurrentTime
        {
            get { return currentTime; }
            set { currentTime = value; }
        }

        [DataMember(Name = "entityID")]
        public int EntityID { get; set; }

        //private DateTime createdDate = DateTime.Now;
        //[DataMember(Name = "createdDate")]
        //public DateTime CreatedDate
        //{
        //    get { return createdDate; }
        //    set { createdDate = value; }
        //}

        //private DateTime modifiedDate = DateTime.Now;
        //[DataMember(Name = "modifiedDate")]
        //public DateTime ModifiedDate
        //{
        //    get { return modifiedDate; }
        //    set { modifiedDate = value; }
        //}

        //[DataMember(Name = "createdBy")]
        //public int CreatedBy { get; set; }

        //[DataMember(Name = "modifiedBy")]
        //public int ModifiedBy { get; set; }

    }
}