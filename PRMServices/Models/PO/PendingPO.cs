﻿
using System;
using System.Runtime.Serialization;
using PRM.Core.Common;
using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class PendingPO : ResponseAudit
    {
        [DataMember(Name = "PURCHASE_ORDER_ID")]
        [DataNames("PURCHASE_ORDER_ID")]
        public string PURCHASE_ORDER_ID { get; set; }

        [DataMember(Name = "PO_STATUS")]
        [DataNames("PO_STATUS")]
        public string PO_STATUS { get; set; }
        
        [DataMember(Name = "TOTAL_COUNT")]
        [DataNames("TOTAL_COUNT")]
        public int TOTAL_COUNT { get; set; }

        [DataMember(Name = "EXPECTED_DELIVERY_DATE")]
        [DataNames("EXPECTED_DELIVERY_DATE")]
        public DateTime? EXPECTED_DELIVERY_DATE
        {
            get; set;
        }

        [DataMember(Name = "PO_ID")]
        [DataNames("PO_ID")]
        public int PO_ID { get; set; }

        [DataMember(Name = "REQ_ID")]
        [DataNames("REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember(Name = "VENDOR_ID")]
        [DataNames("VENDOR_ID")]
        public int VENDOR_ID { get; set; }

        [DataMember(Name = "PO_TOTAL_PRICE")]
        [DataNames("PO_TOTAL_PRICE")]
        public decimal PO_TOTAL_PRICE { get; set; }

        [DataMember(Name = "CREATED")]
        [DataNames("CREATED")]
        public DateTime? CREATED
        {
            get; set;
        }

        [DataMember(Name = "DELIVERY_DATE")]
        [DataNames("DELIVERY_DATE")]
        public string DELIVERY_DATE
        {
            get; set;
        }

        [DataMember(Name = "REQ_CURRENCY")]
        [DataNames("REQ_CURRENCY")]
        public string REQ_CURRENCY { get; set; }

        [DataMember(Name = "SUPPLIER_NAME")]
        [DataNames("SUPPLIER_NAME")]
        public string SUPPLIER_NAME { get; set; }

        [DataMember(Name = "NO_OF_ITEMS")]
        [DataNames("NO_OF_ITEMS")]
        public int NO_OF_ITEMS { get; set; }

        [DataMember(Name = "PO_IS_SEND_TO_VENDOR")]
        [DataNames("PO_IS_SEND_TO_VENDOR")]
        public int PO_IS_SEND_TO_VENDOR { get; set; }

        [DataMember(Name = "NO_OF_POS")]
        [DataNames("NO_OF_POS")]
        public int NO_OF_POS { get; set; }

        [DataMember(Name = "PRODUCT_ID")]
        [DataNames("PRODUCT_ID")]
        public string PRODUCT_ID { get; set; }

        [DataMember(Name = "ITEMS")]
        [DataNames("ITEMS")]
        public string ITEMS { get; set; }

        [DataMember(Name = "CATEGORIES")]
        [DataNames("CATEGORIES")]
        public string CATEGORIES { get; set; }

        [DataMember(Name = "DEPARTMENTS")]
        [DataNames("DEPARTMENTS")]
        public string DEPARTMENTS { get; set; }

        [DataMember(Name = "AWAITING_RECEIPT")]
        [DataNames("AWAITING_RECEIPT")]
        public int AWAITING_RECEIPT { get; set; }

        [DataMember(Name = "DELIVERY_STATUS")]
        [DataNames("DELIVERY_STATUS")]
        public string DELIVERY_STATUS { get; set; }


    }
}