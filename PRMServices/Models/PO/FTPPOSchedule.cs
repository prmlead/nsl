﻿
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PRM.Core.Common;
using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class FTPPOSchedule : ResponseAudit
    {

        
        [DataMember] [DataNames("PO_SCHEDULE_ID")] public int PO_SCHEDULE_ID { get; set; }
        [DataMember] [DataNames("IS_SERVICE_PRODUCT")] public int IS_SERVICE_PRODUCT { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("PRODUCT_ID")] public int PRODUCT_ID { get; set; }
        [DataMember] [DataNames("PLANT")] public string PLANT { get; set; }
        [DataMember] [DataNames("DESCRIPTION")] public string DESCRIPTION { get; set; }
        [DataMember] [DataNames("CATEGORY_ID")] public string CATEGORY_ID { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("PRM_PO_NUMBER")] public string PRM_PO_NUMBER { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("PO_DATE")] public DateTime? PO_DATE { get; set; }
        [DataMember] [DataNames("PO_CREATOR")] public string PO_CREATOR { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string PAYMENT_TERMS { get; set; }
        [DataMember] [DataNames("TAX_CODE")] public string TAX_CODE { get; set; }
        [DataMember] [DataNames("TAX_CODE_DESC")] public string TAX_CODE_DESC { get; set; }
        [DataMember] [DataNames("PO_RELEASE_DATE")] public DateTime? PO_RELEASE_DATE { get; set; }
        [DataMember] [DataNames("DELIVERY_DATE")] public DateTime? DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("MAT_TYPE")] public string MAT_TYPE { get; set; }
        [DataMember] [DataNames("CITY")] public string CITY { get; set; }
        [DataMember] [DataNames("REGION")] public string REGION { get; set; }
        [DataMember] [DataNames("HSN_CODE")] public string HSN_CODE { get; set; }
        [DataMember] [DataNames("ORDER_QTY")] public decimal ORDER_QTY { get; set; }
        [DataMember] [DataNames("GROSS_PRICE")] public decimal GROSS_PRICE { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("ALTERNATIVE_UOM")] public string ALTERNATIVE_UOM { get; set; }
        [DataMember] [DataNames("ALTERNATIVE_UOM_QTY")] public decimal ALTERNATIVE_UOM_QTY { get; set; }
        [DataMember] [DataNames("NET_PRICE")] public decimal NET_PRICE { get; set; }
        [DataMember] [DataNames("VALUE")] public decimal VALUE { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string CURRENCY { get; set; }
        [DataMember] [DataNames("FREIGHT")] public decimal FREIGHT { get; set; }
        [DataMember] [DataNames("PENDING_QTY")] public decimal PENDING_QTY { get; set; }
        [DataMember] [DataNames("PR_ITEM_ID")] public int PR_ITEM_ID { get; set; }
        [DataMember] [DataNames("ITEM_TEXT_PO")] public string ITEM_TEXT_PO { get; set; }
        [DataMember] [DataNames("REL_IND")] public string REL_IND { get; set; }
        [DataMember] [DataNames("PR_REQUISITIONER")] public string PR_REQUISITIONER { get; set; }
        [DataMember] [DataNames("DOC_TYPE")] public string DOC_TYPE { get; set; }
        [DataMember] [DataNames("SERVICE_CODE")] public string SERVICE_CODE { get; set; }
        [DataMember] [DataNames("SERVICE_DESC")] public string SERVICE_DESC { get; set; }
        [DataMember] [DataNames("SERVICE_LINE")] public string SERVICE_LINE { get; set; }
        [DataMember] [DataNames("MISC_CHARGES")] public decimal MISC_CHARGES { get; set; }
        [DataMember] [DataNames("PACKING_CHARGES")] public decimal PACKING_CHARGES { get; set; }
        [DataMember] [DataNames("PO_CONTRACT")] public string PO_CONTRACT { get; set; }
        [DataMember] [DataNames("VALID_FROM")] public DateTime? VALID_FROM { get; set; }
        [DataMember] [DataNames("VALID_TO")] public DateTime? VALID_TO { get; set; }
        [DataMember] [DataNames("DELIVERY_COMPLETED")] public string DELIVERY_COMPLETED { get; set; }
        [DataMember] [DataNames("DELETED_INDICATOR")] public string DELETED_INDICATOR { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("VENDOR_COMPANY")] public string VENDOR_COMPANY { get; set; }
        [DataMember] [DataNames("PO_STATUS")] public string PO_STATUS { get; set; }
        [DataMember] [DataNames("DELIVERY_STATUS")] public string DELIVERY_STATUS { get; set; }
        [DataMember] [DataNames("LAST_RECEIPT_DATE")] public DateTime? LAST_RECEIPT_DATE { get; set; }
        [DataMember] [DataNames("ROW_ID")] public int ROW_ID { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_STATUS")] public int VENDOR_ACK_STATUS { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_COMMENTS")] public string VENDOR_ACK_COMMENTS { get; set; }
        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE")] public DateTime? VENDOR_EXPECTED_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("CGST")] public decimal CGST { get; set; }
        [DataMember] [DataNames("SGST")] public decimal SGST { get; set; }
        [DataMember] [DataNames("IGST")] public decimal IGST { get; set; }
        [DataMember] [DataNames("CESS")] public decimal CESS { get; set; }
        [DataMember] [DataNames("TCS")] public decimal TCS { get; set; }
        [DataMember] [DataNames("PO_SENT_STATUS")] public string PO_SENT_STATUS { get; set; }
        [DataMember] [DataNames("VERTICAL_TYPE")] public string VERTICAL_TYPE { get; set; }
        [DataMember] [DataNames("SAP_USER_ID")] public string SAP_USER_ID { get; set; }
        [DataMember] [DataNames("MATERIAL")] public string MATERIAL { get; set; }
        [DataMember] [DataNames("MATERIAL1")] public string MATERIAL1 { get; set; }
        [DataMember] [DataNames("MATERIAL_GROUP")] public string MATERIAL_GROUP { get; set; }
        [DataMember] [DataNames("VENDOR_PHONE")] public string VENDOR_PHONE { get; set; }
        [DataMember] [DataNames("VENDOR_EMAIL")] public string VENDOR_EMAIL { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("REQ_NUMBER")] public string REQ_NUMBER { get; set; }
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("COMPANY_CODE")] public string COMPANY_CODE { get; set; }
        [DataMember] [DataNames("PURCHASE_ORGANISATION")] public string PURCHASE_ORGANISATION { get; set; }
        [DataMember] [DataNames("REV_UNIT_DISCOUNT")] public decimal REV_UNIT_DISCOUNT { get; set; }
        [DataMember] [DataNames("REV_UNIT_DISCOUNT_VALUE")] public decimal REV_UNIT_DISCOUNT_VALUE { get; set; }

        [DataMember] [DataNames("FTPPOScheduleItems")] public List<FTPPOScheduleItems> FTPPOScheduleItems { get; set; }


    }

    [DataContract]
    public class FTPPOScheduleItems : ResponseAudit
    {
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("PR_ID")] public int PR_ID { get; set; }
        [DataMember] [DataNames("PR_CHANGE_DATE")] public DateTime? PR_CHANGE_DATE { get; set; }
        [DataMember] [DataNames("RELEASE_DATE")] public DateTime? RELEASE_DATE { get; set; }
        [DataMember] [DataNames("MATERIAL_DELIVERY_DATE")] public DateTime? MATERIAL_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("REQUESITION_DATE")] public DateTime? REQUESITION_DATE { get; set; }
        [DataMember] [DataNames("REQUISITIONER_NAME")] public string REQUISITIONER_NAME { get; set; }
        [DataMember] [DataNames("VERTICAL_TYPE")] public string VERTICAL_TYPE { get; set; }
        [DataMember] [DataNames("PLANT_CODE")] public string PLANT_CODE { get; set; }
        [DataMember] [DataNames("PURCHASE_GROUP_CODE")] public string PURCHASE_GROUP_CODE { get; set; }
        [DataMember] [DataNames("PR_CREATOR_NAME")] public string PR_CREATOR_NAME { get; set; }
        [DataMember] [DataNames("REQUIRED_QUANTITY")] public decimal REQUIRED_QUANTITY { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }

        [DataMember] [DataNames("ACCOUNT_ASSIGNMENT_CATEGORY")] public string ACCOUNT_ASSIGNMENT_CATEGORY { get; set; }
        [DataMember] [DataNames("SERVICE_LINE_ITEM")] public string SERVICE_LINE_ITEM { get; set; }
        [DataMember] [DataNames("PR_ITEM_TYPE")] public string PR_ITEM_TYPE { get; set; }
        [DataMember] [DataNames("ITEM_NUM")] public string ITEM_NUM { get; set; }
        [DataMember] [DataNames("GL_ACCOUNT")] public string GL_ACCOUNT { get; set; }
        [DataMember] [DataNames("ORDER_NO")] public string ORDER_NO { get; set; }
    }
}