﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class cr_ProductAnalysis : Entity
    {
        [DataMember(Name = "LEAST_ITEM_L1_PRICE")]
        public double LEAST_ITEM_L1_PRICE { get; set; }

        [DataMember(Name = "RFQ_VENDORS")]
        public int RFQ_VENDORS { get; set; }

        

        [DataMember(Name = "MaterialSourced")]
        public List<MaterialSourced> MaterialSourced { get; set; }

        [DataMember(Name = "FrequencySourcing")]
        public List<FrequencySourcing> FrequencySourcing { get; set; }

        [DataMember(Name = "vendorShare")]
        public List<VendorShare> VendorShare { get; set; }
    }

    public class MaterialSourced
    {
        [DataMember(Name = "ITEM_BASED_UNITS")]
        public int ITEM_BASED_UNITS { get; set; }

        [DataMember(Name = "ITEM_UOM")]
        public string ITEM_UOM { get; set; }
    }

    [DataContract]
    public class FrequencySourcing
    {
        [DataMember(Name = "MONTH_NAME")]
        public string MONTH_NAME { get; set; }

        [DataMember(Name = "MONTH_REQ")]
        public int MONTH_REQ { get; set; }

        
    }

    [DataContract]
    public class VendorShare
    {
        [DataMember(Name = "U_ID")]
        public int U_ID { get; set; }

        [DataMember(Name = "VENDOR_NAME")]
        public string VENDOR_NAME { get; set; }

        [DataMember(Name = "VENDOR_SHARE_PRICE")]
        public double VENDOR_SHARE_PRICE { get; set; }
    }
}