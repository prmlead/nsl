﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Common;
using PRM.Core.Models;

namespace PRMServices.Models.Catalog
{
    [DataContract]
    public class ProductQuotationTemplate : EntityExt
    {
        [DataMember] [DataNames("T_ID")] public int T_ID { get; set; }
        [DataMember] [DataNames("PRODUCT_ID")] public int PRODUCT_ID { get; set; }
        [DataMember] [DataNames("NAME")] public string NAME { get; set; }
        [DataMember] [DataNames("DESCRIPTION")] public string DESCRIPTION { get; set; }
        [DataMember] [DataNames("HAS_SPECIFICATION")] public int HAS_SPECIFICATION { get; set; }
        [DataMember] [DataNames("HAS_PRICE")] public int HAS_PRICE { get; set; }
        [DataMember] [DataNames("HAS_QUANTITY")] public int HAS_QUANTITY { get; set; }
        [DataMember] [DataNames("HAS_TAX")] public int HAS_TAX { get; set; }
        [DataMember] [DataNames("IS_VALID")] public int IS_VALID { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public decimal UNIT_PRICE { get; set; }
        [DataMember] [DataNames("REV_UNIT_PRICE")] public decimal REV_UNIT_PRICE { get; set; }

        [DataMember] [DataNames("CONSUMPTION")] public decimal CONSUMPTION { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("REV_CONSUMPTION")] public decimal REV_CONSUMPTION { get; set; }

        [DataMember] [DataNames("BULK_PRICE")] public decimal BULK_PRICE { get; set; }
        [DataMember] [DataNames("REV_BULK_PRICE")] public decimal REV_BULK_PRICE { get; set; }

        [DataMember] [DataNames("IS_CALCULATED")] public int IS_CALCULATED { get; set; }

        [DataMember] [DataNames("CEILING_PRICE")] public decimal CEILING_PRICE { get; set; }

        //Vendor Attributes
        [DataMember] [DataNames("SPECIFICATION")] public string SPECIFICATION { get; set; }
        [DataMember] [DataNames("TAX")] public decimal TAX { get; set; }
    }
}