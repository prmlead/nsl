﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Common;

namespace PRMServices.Models.Vendor
{
    [DataContract]
    public class VendorItemAssignment
    {

        [DataMember]
        [DataNames("QCS_VENDOR_ITEM_ID")]
        public int QCS_VENDOR_ITEM_ID { get; set; }

        [DataMember]
        [DataNames("QCS_ID")]
        public int QCS_ID { get; set; }

        [DataMember]
        [DataNames("REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember]
        [DataNames("VENDOR_ID")]
        public int VENDOR_ID { get; set; }

        [DataMember]
        [DataNames("ITEM_ID")]
        public int ITEM_ID { get; set; }

        [DataMember]
        [DataNames("ASSIGN_QTY")]
        public double ASSIGN_QTY { get; set; }

        [DataMember]
        [DataNames("ASSIGN_PRICE")]
        public double ASSIGN_PRICE { get; set; }

        [DataMember]
        [DataNames("TOTAL_PRICE")]
        public double TOTAL_PRICE { get; set; }

        [DataMember]
        [DataNames("PO_ID")]
        public string PO_ID { get; set; }
    }
}