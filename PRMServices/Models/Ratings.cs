﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Common;

namespace PRMServices.Models.Vendor
{
    [DataContract]
    public class Ratings
    {
        [DataMember] [DataNames("R_ID")] public string R_ID { get; set; }
        [DataMember] [DataNames("REQ_ID")] public string REQ_ID { get; set; }
        [DataMember] [DataNames("REQ_TITLE")] public string REQ_TITLE { get; set; }
        [DataMember] [DataNames("REVIEWEE_ID")] public int REVIEWEE_ID { get; set; }
        [DataMember] [DataNames("REVIEWER_ID")] public int REVIEWER_ID { get; set; }
        [DataMember] [DataNames("REVIEWEE_DISP_NAME")] public string REVIEWEE_DISP_NAME { get; set; }
        [DataMember] [DataNames("REVIEWER_DISP_NAME")]public string REVIEWER_DISP_NAME { get; set; }
        [DataMember] [DataNames("DELIERY_RATING")] public decimal DELIERY_RATING { get; set; }
        [DataMember] [DataNames("QUALITY_RATING")]  public decimal QUALITY_RATING { get; set; }
        [DataMember] [DataNames("EMERGENCY_RATING")] public decimal EMERGENCY_RATING { get; set; }
        [DataMember] [DataNames("SERVICE_RATING")] public decimal SERVICE_RATING { get; set; }
        [DataMember] [DataNames("RESPONSE_RATING")] public decimal RESPONSE_RATING { get; set; }
        [DataMember] [DataNames("COMMENTS")] public string COMMENTS { get; set; }
        [DataMember] [DataNames("KEY_CLIENT_TELE")] public string KEY_CLIENT_TELE { get; set; }
        [DataMember] [DataNames("SUPPLY_CAPACITY")] public string SUPPLY_CAPACITY { get; set; }
        [DataMember] [DataNames("PRICE_TRANSPARENCY")] public string PRICE_TRANSPARENCY { get; set; }
        [DataMember] [DataNames("SAFETY")] public string SAFETY { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")]  public DateTime DATE_MODIFIED { get; set; }
        [DataMember] [DataNames("RATING_JSON")] public string RATING_JSON { get; set; }
        [DataMember] [DataNames("KEY_CLIENT_TELE_RATING")] public decimal KEY_CLIENT_TELE_RATING { get; set; }
        [DataMember] [DataNames("SUPPLY_CAPACITY_RATING")] public decimal SUPPLY_CAPACITY_RATING { get; set; }
        [DataMember] [DataNames("PRICE_TRANSPARENCY_RATING")] public decimal PRICE_TRANSPARENCY_RATING { get; set; }
        [DataMember] [DataNames("SAFETY_RATING")] public decimal SAFETY_RATING { get; set; }
        [DataMember] [DataNames("COMMENTS_RATING")] public decimal COMMENTS_RATING { get; set; }
    }
}