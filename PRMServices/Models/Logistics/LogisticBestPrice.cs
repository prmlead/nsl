﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class LogisticBestPrice : Entity
    {
        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "initialPrice")]
        public double InitialPrice { get; set; }
       
        string companyName = string.Empty;
        [DataMember(Name = "companyName")]
        public string CompanyName
        {
            get
            {
                return this.companyName;
            }
            set
            {
                this.companyName = value;
            }
        }

        [DataMember(Name = "airline")]
        public string Airline { get; set; }

        string productIDorName = string.Empty;
        [DataMember(Name = "productIDorName")]
        public string ProductIDorName
        {
            get
            {
                if (!string.IsNullOrEmpty(productIDorName))
                { return productIDorName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productIDorName = value; }
            }
        }

        string modeOfShipment = string.Empty;
        [DataMember(Name = "modeOfShipment")]
        public string ModeOfShipment
        {
            get
            {
                if (!string.IsNullOrEmpty(modeOfShipment))
                { return modeOfShipment; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { modeOfShipment = value; }
            }
        }

        string natureOfGoods = string.Empty;
        [DataMember(Name = "natureOfGoods")]
        public string NatureOfGoods
        {
            get
            {
                if (!string.IsNullOrEmpty(natureOfGoods))
                { return natureOfGoods; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { natureOfGoods = value; }
            }
        }

        string storageCondition = string.Empty;
        [DataMember(Name = "storageCondition")]
        public string StorageCondition
        {
            get
            {
                if (!string.IsNullOrEmpty(storageCondition))
                { return storageCondition; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { storageCondition = value; }
            }
        }

        string portOfLanding = string.Empty;
        [DataMember(Name = "portOfLanding")]
        public string PortOfLanding
        {
            get
            {
                if (!string.IsNullOrEmpty(portOfLanding))
                { return portOfLanding; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { portOfLanding = value; }
            }
        }

        string finalDestination = string.Empty;
        [DataMember(Name = "finalDestination")]
        public string FinalDestination
        {
            get
            {
                if (!string.IsNullOrEmpty(finalDestination))
                { return finalDestination; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { finalDestination = value; }
            }
        }

        [DataMember(Name = "netWeight")]
        public double NetWeight { get; set; }

        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

    }
}