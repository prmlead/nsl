﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class RunningAuction : Entity
    {
        [DataMember(Name = "auctionID")]
        public int AuctionID { get; set; }

        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "postedOn")]
        public DateTime PostedOn { get; set; }

        [DataMember(Name = "auctionEnds")]
        public DateTime AuctionEnds { get; set; }

        [DataMember(Name = "auctionTimerId")]
        public int AuctionTimerId { get; set; }

        [DataMember(Name = "bids")]
        public int Bids { get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }

        [DataMember(Name = "auctionStarted")]
        public bool AuctionStarted { get; set; }

        [DataMember(Name = "attachment")]
        public string Attachment { get; set; }

        [DataMember(Name = "rank")]
        public string Rank { get; set; }

        [DataMember(Name = "quotationUrl")]
        public string QuotationUrl { get; set; }

        [DataMember(Name = "quotation")]
        public string Quotation { get; set; }

        [DataMember(Name = "inclusiveTax")]
        public int InclusiveTax { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "currency")]
        public string Currency { get; set; }

        string negotiationDuretion = string.Empty;
        [DataMember(Name = "negotiationDuretion")]
        public string NegotiationDuretion
        {
            get
            {
                if (!string.IsNullOrEmpty(negotiationDuretion))
                {
                    return negotiationDuretion;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    negotiationDuretion = value;
                }
            }
        }

        [DataMember(Name = "NegotiationSettings")]
        public NegotiationSettings NegotiationSettings { get; set; }



        string reqType = string.Empty;
        [DataMember(Name = "reqType")]
        public string ReqType
        {
            get
            {
                if (!string.IsNullOrEmpty(reqType))
                {
                    return reqType;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    reqType = value;
                }
            }
        }

        [DataMember(Name = "priceCap")]
        public double PriceCap { get; set; }

    }
}