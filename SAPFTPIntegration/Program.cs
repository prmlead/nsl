﻿using OfficeOpenXml;
using PRMServices;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using CORE = PRM.Core.Common;

namespace SAPFTPIntegration
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static char csvDelimiter = '|';
        private static string _archiveFolder = ConfigurationManager.AppSettings["ArchiveFolder"].ToString();
        private static string _errorFolder = ConfigurationManager.AppSettings["ErrorFolder"].ToString();
        private static string _stageFolder = ConfigurationManager.AppSettings["StageFolder"].ToString();
        private static string _processFolder = ConfigurationManager.AppSettings["ProcessFolder"].ToString();

        private static string _ExecutedFilesFolder = ConfigurationManager.AppSettings["ExecutedFilesFolder"].ToString();
        private static string _ExecutedFilesFolderMaterial = ConfigurationManager.AppSettings["ExecutedFilesFolderMaterial"].ToString();
        private static string _ExecutedFilesFolderVendor = ConfigurationManager.AppSettings["ExecutedFilesFolderVendor"].ToString();


        private static string SAPFTPPRFilePattern = "PR";
        private static string SAPFTPVEndorFilePattern = "VEND";
        private static string SAPFTPGRNFilePattern = "PRM_SAP_GR";
        private static string SAPFTPPOFilePattern = "PRM_SAP_PO";
        private static string SAPFTPPORESPONSEPattern = "POR";
        private static string SAPFTPMATERIALFilePattern = "MAT";

        static void Main(string[] args)
        {
            string jobType = ConfigurationManager.AppSettings["JOB_TYPE"].ToString();
            string clientFolderName = ConfigurationManager.AppSettings["ClientFolderName"];
            logger.Info("JOB_TYPE:" + jobType);

            NetworkCredential credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserId"], ConfigurationManager.AppSettings["UserPwd"]);
            string url = ConfigurationManager.AppSettings["Host"];

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PR_JOB")
            {
                DownloadFtpDirectory(url + clientFolderName + "/PR_Data/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPPRFilePattern);
                ProcessPRFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "VENDOR_JOB")
            {
                DownloadFtpDirectory(url + clientFolderName + "/VEND_Data/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPVEndorFilePattern);
                ProcessVendorFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "GRN_JOB")
            {
                DownloadFtpDirectory(url + clientFolderName + "/GR_Data/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPGRNFilePattern);
                ProcesGRNFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PO_JOB")
            {
                DownloadFtpDirectory(url + clientFolderName + "/PO_Data/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPPOFilePattern);
                ProcesPOScheduleFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "MATERIAL_JOB")
            {
                DownloadFtpDirectory(url + clientFolderName + "/MAT_Data/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPMATERIALFilePattern);
                ProcessMaterialsFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "SAVE_PO_JOB")
            {
                // DownloadFtpDirectory(url + "/PO_Data/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPPOFilePattern);
                string poUrl = url + clientFolderName + "/PO_Data/";
                ProcesSavePOFiles(poUrl, credentials);
            }
            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "POR")
            {
                DownloadFtpDirectory(url + clientFolderName + "/PO_RESPONSE/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPPORESPONSEPattern);
                //string poUrl = url + clientFolderName + "/PO_RESPONSE/";
                GetPOResponseFromFTP();
            }
        }

        private static void ProcesSavePOFiles(string url, NetworkCredential credentials)
        {
            char[] charsToTrim = { ' ' };

            string POSCHParams = ConfigurationManager.AppSettings["POSCHParams"];
            string CID = POSCHParams.Split(';')[0];
            string UID = POSCHParams.Split(';')[1];
            int COMP_ID = Convert.ToInt32(CID);
            int USER_ID = Convert.ToInt32(UID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<FTPPOSchedule> details = new List<FTPPOSchedule>();
            List<FTPPOSchedule> SeedsData = new List<FTPPOSchedule>();
            List<FTPPOSchedule> SugarsData = new List<FTPPOSchedule>();
            List<FTPPOSchedule> TextilesData = new List<FTPPOSchedule>();
            List<FTPPOScheduleItems> itemdetails = new List<FTPPOScheduleItems>();
            sd.Add("P_COMP_ID", COMP_ID);
            sd.Add("P_U_ID", USER_ID);
            CORE.DataNamesMapper<FTPPOSchedule> mapper = new CORE.DataNamesMapper<FTPPOSchedule>();
            CORE.DataNamesMapper<FTPPOScheduleItems> itemmapper = new CORE.DataNamesMapper<FTPPOScheduleItems>();
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            var dataset = sqlHelper.SelectList("sap_GetPOScheduleList", sd);
            details = mapper.Map(dataset.Tables[0]).ToList();
            itemdetails = itemmapper.Map(dataset.Tables[1]).ToList();
            if (details != null && details.Count > 0 && dataset.Tables[1].Rows.Count > 0)
            {
                foreach (FTPPOSchedule PO in details)
                {
                    PO.FTPPOScheduleItems = new List<FTPPOScheduleItems>();
                    PO.FTPPOScheduleItems = itemdetails.Where(i => i.PR_NUMBER == PO.PR_NUMBER).ToList();
                    string vT = PO.FTPPOScheduleItems.Count > 0 ? PO.FTPPOScheduleItems[0].VERTICAL_TYPE.Trim(charsToTrim) : "";
                    //PO.VERTICAL_TYPE = vT;
                    if (PO.VERTICAL_TYPE == "SEEDS")
                    {
                        SeedsData.Add(PO);
                    }
                    else if (PO.VERTICAL_TYPE == "SUGARS")
                    {
                        SugarsData.Add(PO);
                    }
                    else if (PO.VERTICAL_TYPE == "TEXTILES")
                    {
                        TextilesData.Add(PO);
                    }
                }

                SeedsData = SeedsData.Where(d => d.FTPPOScheduleItems.Count > 0).ToList();
                SugarsData = SugarsData.Where(d => d.FTPPOScheduleItems.Count > 0).ToList();
                TextilesData = TextilesData.Where(d => d.FTPPOScheduleItems.Count > 0).ToList();
                details = details.Where(d => d.FTPPOScheduleItems.Count > 0).ToList();
            }

            if (SeedsData.Count > 0)
            {
                string Excel = MakeCsvFile(SeedsData);
                if (!string.IsNullOrEmpty(Excel))
                {
                    string updatePOQuery = string.Empty;
                    updatePOQuery += $@"update POScheduleDetails SET PO_SENT_STATUS = 1,PO_STATUS = 'SUBMITTED' where PO_SCHEDULE_ID in ({Excel});";
                    sqlHelper.ExecuteNonQuery_IUD(updatePOQuery);
                }
            }
            if (SugarsData.Count > 0)
            {
                string Excel = MakeCsvFile(SugarsData);
                if (!string.IsNullOrEmpty(Excel))
                {
                    string updatePOQuery = string.Empty;
                    updatePOQuery += $@"update POScheduleDetails SET PO_SENT_STATUS = 1,PO_STATUS = 'SUBMITTED' where PO_SCHEDULE_ID in ({Excel});";
                    sqlHelper.ExecuteNonQuery_IUD(updatePOQuery);
                }
            }
            if (TextilesData.Count > 0)
            {
                string Excel = MakeCsvFile(TextilesData);
                if (!string.IsNullOrEmpty(Excel))
                {
                    string updatePOQuery = string.Empty;
                    updatePOQuery += $@"update POScheduleDetails SET PO_SENT_STATUS = 1,PO_STATUS = 'SUBMITTED' where PO_SCHEDULE_ID in ({Excel});";
                    sqlHelper.ExecuteNonQuery_IUD(updatePOQuery);
                }
            }
        }
        private static string MakeCsvFile(List<FTPPOSchedule> data)
        {
            List<string> columns = "WERKS,MATNR,TXZ01,MATKL,LIFNR,NAME1,TELF2,SMTP_ADDR,EBELN,M_PO_NUM,EBELP,AEDAT,ERNAM,ZTERM,MWSKZ,TEXT1,FGDAT,EINDT,MTART,CITY1,BEZEI,STEUC,MENGE,MEINS,NETPR,NETWR,WAERS,M_FREIGHT,OBMNG,BNFPO,BANFN,FRGDT,M_PRLINETEXT,LFDAT,M_ITEMTEXTPO,FRGKX,AFNAM,BSART,SRVPOS,KTEXT1,M_MISCCHARGES,M_PACKINGCHARGES,ANFNR,EKORG,EKGRP,BEDNR,KNTTP,AUFNR,SAKTO,EXTROW,TBTWR,MENGE,EPSTP,BUKRS,SAP_USER_ID,REV_UNIT_DISCOUNT,REV_UNIT_DISCOUNT_VALUE,VERTICAL_TYPE".Split(',').ToList();
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;
            var distinctPRMPONum = data.GroupBy(elem => elem.PRM_PO_NUMBER).Select(group => group.First());
            List<string> PRNumbers1 = new List<string>();
            string result = "";
            try
            {
                string PRString1 = "";
                foreach (FTPPOSchedule prmpo in distinctPRMPONum)
                {
                    List<FTPPOSchedule> groupdata = new List<FTPPOSchedule>();
                    groupdata = data.Where(item => item.PRM_PO_NUMBER == prmpo.PRM_PO_NUMBER).ToList();

                    char Delimiter = '|';
                    ExcelPackage ExcelPkg = new ExcelPackage();
                    ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add(groupdata[0].VERTICAL_TYPE + "_PO_DATA");
                    if (columns.Count > 0)
                    {
                        string csvDelimitedString = "";

                        long tick = DateTime.UtcNow.Ticks;
                        string fileExtension = ConfigurationManager.AppSettings["POFileextension"];
                        string fileName = "PO_" + groupdata[0].PRM_PO_NUMBER + "_" + tick + fileExtension;
                        string locationPath = "";
                        if (groupdata[0].VERTICAL_TYPE == "SEEDS")
                        {
                            locationPath = ConfigurationManager.AppSettings["PODataSeeds"];
                        }
                        if (groupdata[0].VERTICAL_TYPE == "SUGARS")
                        {
                            locationPath = ConfigurationManager.AppSettings["PODataSugars"];
                        }
                        if (groupdata[0].VERTICAL_TYPE == "TEXTILES")
                        {
                            locationPath = ConfigurationManager.AppSettings["PODataTextiles"];
                        }
                        if (!File.Exists(locationPath + fileName))
                        {
                            for (int i = 0; i <= (columns.Count - 1); i++)
                            {
                                var row = columns[i];
                                if (!string.IsNullOrEmpty(row))
                                {
                                    csvDelimitedString += row + Delimiter;
                                }
                            }
                            csvDelimitedString = csvDelimitedString.Substring(0, csvDelimitedString.Length - (",").Length);
                            csvDelimitedString = csvDelimitedString + Environment.NewLine;
                            File.WriteAllText(locationPath + fileName, csvDelimitedString);
                        }

                        string PRString = "";
                        if (groupdata.Count > 0)
                        {
                            List<string> PRNumbers = new List<string>();

                            foreach (FTPPOSchedule po in groupdata)
                            {
                                string dataString = "";
                                PRNumbers.Add(Convert.ToString(po.PO_SCHEDULE_ID));
                                dataString += (!string.IsNullOrEmpty(po.PLANT) ? po.PLANT : "") + Delimiter;
                                dataString += (po.IS_SERVICE_PRODUCT == 0 ? (!string.IsNullOrEmpty(po.MATERIAL) ? po.MATERIAL : "") : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.DESCRIPTION) ? po.DESCRIPTION : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.MATERIAL_GROUP) ? po.MATERIAL_GROUP : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.VENDOR_CODE) ? po.VENDOR_CODE : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.VENDOR_COMPANY) ? po.VENDOR_COMPANY : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.VENDOR_PHONE) ? po.VENDOR_PHONE : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.VENDOR_EMAIL) ? po.VENDOR_EMAIL : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.PO_NUMBER) ? po.PO_NUMBER : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.PRM_PO_NUMBER) ? po.PRM_PO_NUMBER : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.PO_LINE_ITEM) ? po.PO_LINE_ITEM : "") + Delimiter;
                                string poDate = Convert.ToString(po.PO_DATE);
                                dataString += (!string.IsNullOrEmpty(poDate) ? poDate : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.FTPPOScheduleItems[0].PR_CREATOR_NAME) ? po.FTPPOScheduleItems[0].PR_CREATOR_NAME : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.PAYMENT_TERMS) ? po.PAYMENT_TERMS : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.TAX_CODE) ? po.TAX_CODE : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.TAX_CODE_DESC) ? po.TAX_CODE_DESC : "") + Delimiter;
                                string poReleaseDate = Convert.ToString(po.PO_RELEASE_DATE);
                                string deliveryDate = Convert.ToString(po.DELIVERY_DATE);
                                string materialDeliveryDate = Convert.ToString(po.FTPPOScheduleItems[0].MATERIAL_DELIVERY_DATE);
                                materialDeliveryDate = materialDeliveryDate.Contains("1900") ? null : materialDeliveryDate;
                                dataString += (!string.IsNullOrEmpty(poReleaseDate) ? poReleaseDate : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(materialDeliveryDate) ? materialDeliveryDate : null) + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.MAT_TYPE) ? po.MAT_TYPE : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.CITY) ? po.CITY : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.REGION) ? po.REGION : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.HSN_CODE) ? po.HSN_CODE : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.ORDER_QTY)) ? Convert.ToString(po.ORDER_QTY) : "") + Delimiter;//menge
                                dataString += (!string.IsNullOrEmpty(po.UOM) ? po.UOM : "") + Delimiter;
                                dataString += Math.Round(po.NET_PRICE, 4).ToString() + Delimiter;
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.VALUE)) ? Convert.ToString(po.VALUE) : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.CURRENCY)) ? Convert.ToString(po.CURRENCY) : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.FREIGHT)) ? Convert.ToString(po.FREIGHT) : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.PENDING_QTY)) ? Convert.ToString(po.PENDING_QTY) : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.PO_LINE_ITEM)) ? Convert.ToString(po.PO_LINE_ITEM) : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.PR_NUMBER)) ? Convert.ToString(po.PR_NUMBER) : "") + Delimiter;
                                // dataString += (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].REQUIRED_QUANTITY)) ? Convert.ToString(po.FTPPOScheduleItems[0].REQUIRED_QUANTITY) : "") + Delimiter;//menge
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].RELEASE_DATE)) ? Convert.ToString(po.FTPPOScheduleItems[0].RELEASE_DATE) : "") + Delimiter;
                                dataString += "" + Delimiter; //(!string.IsNullOrEmpty(Convert.ToString(po.PR_ITEM_ID)) ? Convert.ToString(po.PR_ITEM_ID) : "") + Delimiter; // PR Line Text
                                dataString += (!string.IsNullOrEmpty(materialDeliveryDate) ? materialDeliveryDate : null) + Delimiter;
                                dataString += "" + Delimiter; // (!string.IsNullOrEmpty(po.DESCRIPTION) ? po.DESCRIPTION : "")
                                dataString += "" + Delimiter; // (!string.IsNullOrEmpty(Convert.ToString(po.PR_ITEM_ID)) ? Convert.ToString(po.PR_ITEM_ID) : "") + Delimiter; //Rel Ind
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].REQUISITIONER_NAME)) ? Convert.ToString(po.FTPPOScheduleItems[0].REQUISITIONER_NAME) : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(po.DOC_TYPE) ? po.DOC_TYPE : "") + Delimiter;
                                dataString += (po.IS_SERVICE_PRODUCT == 1 ? (!string.IsNullOrEmpty(po.MATERIAL) ? po.MATERIAL : "") : "") + Delimiter;
                                dataString += (po.IS_SERVICE_PRODUCT == 1 ? (!string.IsNullOrEmpty(po.SERVICE_DESC) ? po.SERVICE_DESC : "") : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.MISC_CHARGES)) ? Convert.ToString(po.MISC_CHARGES) : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.PACKING_CHARGES)) ? Convert.ToString(po.PACKING_CHARGES) : "") + Delimiter;

                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.REQ_NUMBER)) ? Convert.ToString(po.REQ_NUMBER) : "") + Delimiter; // ANFNR
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.PURCHASE_ORGANISATION)) ? Convert.ToString(po.PURCHASE_ORGANISATION) : "") + Delimiter; // EKORG
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].PURCHASE_GROUP_CODE)) ? Convert.ToString(po.FTPPOScheduleItems[0].PURCHASE_GROUP_CODE) : "") + Delimiter;//EKGRP
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.REQ_ID)) ? Convert.ToString(po.REQ_ID) : "") + Delimiter; // BEDNR
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].ACCOUNT_ASSIGNMENT_CATEGORY)) ? Convert.ToString(po.FTPPOScheduleItems[0].ACCOUNT_ASSIGNMENT_CATEGORY) : "") + Delimiter;//KNTTP
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].ORDER_NO)) ? Convert.ToString(po.FTPPOScheduleItems[0].ORDER_NO) : "") + Delimiter; // AUFNR
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].GL_ACCOUNT)) ? Convert.ToString(po.FTPPOScheduleItems[0].GL_ACCOUNT) : "") + Delimiter; // SAKTO
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.SERVICE_LINE)) ? Convert.ToString(po.SERVICE_LINE) : "") + Delimiter;//EXTROW
                                dataString += Math.Round(po.GROSS_PRICE, 4).ToString() + Delimiter; // TBTWR
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.ORDER_QTY)) ? Convert.ToString(po.ORDER_QTY) : "") + Delimiter; //MENGE
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.FTPPOScheduleItems[0].PR_ITEM_TYPE)) ? Convert.ToString(po.FTPPOScheduleItems[0].PR_ITEM_TYPE) : "") + Delimiter; //EPSTP


                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.COMPANY_CODE)) ? Convert.ToString(po.COMPANY_CODE) : "") + Delimiter;
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.SAP_USER_ID)) ? Convert.ToString(po.SAP_USER_ID) : "") + Delimiter;
                                dataString += Math.Round(po.REV_UNIT_DISCOUNT, 4).ToString() + Delimiter;
                                dataString += Math.Round(po.REV_UNIT_DISCOUNT_VALUE, 4).ToString() + Delimiter;
                                dataString += (!string.IsNullOrEmpty(Convert.ToString(po.VERTICAL_TYPE)) ? Convert.ToString(po.VERTICAL_TYPE) : "") + Environment.NewLine;
                                
                                File.AppendAllText(locationPath + fileName, dataString);
                            }
                            PRNumbers1 = PRNumbers1.Concat(PRNumbers).ToList();
                            PRString = string.Join(",", PRNumbers);
                            // PRString = "";
                        }

                        string readText = File.ReadAllText(locationPath + fileName);
                        File.WriteAllText(locationPath + fileName, readText);


                    }
                    PRString1 = string.Join(",", PRNumbers1);
                    result = PRString1;
                }

            }
            catch (Exception ex)
            {
                logger.Error("Error occured:" + ex.Message);
                result = "0";
            }
            return result;
        }
        private static void ProcessPRFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPPRFilePattern))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv");

                        string executedFiles = _ExecutedFilesFolder + "\\" + Path.GetFileName(filepath);
                        executedFiles = executedFiles.Replace(".xls", ".csv");

                        try
                        {

                            List<string> columns = "PLANT,PLANT_CODE,PURCHASE_GROUP_CODE,PURCHASE_GROUP_NAME,REQUESITION_DATE,PR_RELEASE_DATE,PR_CHANGE_DATE,PR_NUMBER,REQUISITIONER_NAME,REQUISITIONER_EMAIL,ITEM_OF_REQUESITION,MATERIAL_GROUP_CODE,MATERIAL_GROUP_DESC,MATERIAL_CODE,MATERIAL_TYPE,MATERIAL_HSN_CODE,MATERIAL_DESCRIPTION,SHORT_TEXT,UOM,QTY_REQUIRED,MATERIAL_DELIVERY_DATE,PR_TYPE,PR_TYPE_DESC,PR_NOTE,PR_CREATOR_NAME,PROJECT_DESCRIPTION,PROFIT_CENTER,ALTERNATE_UOM,ALTERNATE_QTY,SECTION_HEAD,SERVICE_CODE,SERVICE_CODE_DESCRIPTION,SERVICE_QTY,SERVICE_UNIT_OF_MEASURE,SERVICE_LINE_ITEM,ITEM_CATEGORY,ACCOUNT_ASSIGNMENT_CATEGORY,GL_ACCOUNT,ORDER_NO,EBAKZ,LOEKZ,ERDAT,OTHER_INFO1,VERTICAL_TYPE".Split(',').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"PR records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(5000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }
                                    //  PLANT | PLANT_CODE | PURCHASE_GROUP_CODE | PURCHASE_GROUP_NAME | REQUESITION_DATE | REQUISITIONER_EMAIL | PR_RELEASE_DATE | PR_CHANGE_DATE | PR_NUMBER | 
                                    //  PR_CREATOR_NAME | REQUISITIONER_NAME | ITEM_OF_REQUESITION | MATERIAL_GROUP_CODE | MATERIAL_GROUP_DESC | MATERIAL_CODE | MATERIAL_TYPE | MATERIAL_HSN_CODE | 
                                    //  MATERIAL_DESCRIPTION | SHORT_TEXT | UOM | QTY_REQUIRED | MATERIAL_DELIVERY_DATE | PR_TYPE | PR_TYPE_DESC | PR_NOTE | Project_Description | PROFIT_CENTER | 
                                    //  Alternate_UOM | Alternate_QTY | Section Head | SERVICE_CODE | SERVICE_CODE_DESCRIPTION | SERVICE_QTY | SERVICE_UNIT_OF_MEASURE | SERVICE_LINE_ITEM | ITEM_CATEGORY | 
                                    //  ACCOUNT_ASSIGNMENT_CATEGORY | DELETION_STATUS | CHANGED_ON_DATE | PR_STATUS_CLOSED | VERTICAL_TYPE

                                    string query = @"INSERT INTO [dbo].[SAP_PR_DETAILS] ([JOB_ID], [PLANT], [PLANT_CODE], [PURCHASE_GROUP_CODE], [PURCHASE_GROUP_NAME], [REQUESITION_DATE], [PR_RELEASE_DATE], 
                                    [PR_CHANGE_DATE], [PR_NUMBER], [REQUISITIONER_NAME], [REQUISITIONER_EMAIL], [ITEM_OF_REQUESITION], [MATERIAL_GROUP_CODE], [MATERIAL_GROUP_DESC], [MATERIAL_CODE], [MATERIAL_TYPE], 
                                    [MATERIAL_HSN_CODE], [MATERIAL_DESCRIPTION], [SHORT_TEXT], [UOM], [QTY_REQUIRED], [MATERIAL_DELIVERY_DATE], [PR_TYPE], [PR_TYPE_DESC], [PR_NOTE],
                                    [PR_CREATOR_NAME],[PROJECT_DESCRIPTION],[PROFIT_CENTER],[ALTERNATE_UOM],[ALTERNATE_QTY],[SECTION_HEAD],[SERVICE_CODE],[SERVICE_CODE_DESCRIPTION],[SERVICE_QTY],
                                    [SERVICE_UNIT_OF_MEASURE],[SERVICE_LINE_ITEM],[ITEM_CATEGORY],[ACCOUNT_ASSIGNMENT_CATEGORY],[EBAKZ],
                                    [LOEKZ], [ERDAT], [DATE_CREATED], [OTHER_INFO1],[VERTICAL_TYPE],[GL_ACCOUNT],[ORDER_NO]) VALUES ";



                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["PLANT"] = RemoveFirstandLastChar(row, "PLANT");
                                        newRow["PLANT_CODE"] = GetDataRowVaue(row, "PLANT_CODE");
                                        newRow["PURCHASE_GROUP_CODE"] = GetDataRowVaue(row, "PURCHASE_GROUP_CODE");
                                        newRow["PURCHASE_GROUP_NAME"] = GetDataRowVaue(row, "PURCHASE_GROUP_NAME");
                                        newRow["REQUESITION_DATE"] = !string.IsNullOrWhiteSpace(GetDataRowVaue(row, "REQUESITION_DATE")) ? Convert.ToDateTime(GetDataRowVaue(row, "REQUESITION_DATE")).ToString("yyyy-MM-dd") : GetDataRowVaue(row, "REQUESITION_DATE");
                                        newRow["PR_RELEASE_DATE"] = !string.IsNullOrWhiteSpace(GetDataRowVaue(row, "PR_RELEASE_DATE")) ? Convert.ToDateTime(GetDataRowVaue(row, "PR_RELEASE_DATE")).ToString("yyyy-MM-dd") : GetDataRowVaue(row, "PR_RELEASE_DATE");
                                        newRow["PR_CHANGE_DATE"] = !string.IsNullOrWhiteSpace(GetDataRowVaue(row, "PR_CHANGE_DATE")) ? Convert.ToDateTime(GetDataRowVaue(row, "PR_CHANGE_DATE")).ToString("yyyy-MM-dd") : GetDataRowVaue(row, "PR_CHANGE_DATE");
                                        newRow["PR_NUMBER"] = GetDataRowVaue(row, "PR_NUMBER");
                                        newRow["REQUISITIONER_NAME"] = GetDataRowVaue(row, "REQUISITIONER_NAME");
                                        newRow["REQUISITIONER_EMAIL"] = GetDataRowVaue(row, "REQUISITIONER_EMAIL");
                                        newRow["ITEM_OF_REQUESITION"] = GetDataRowVaue(row, "REQUISITION_ITEM");
                                        newRow["MATERIAL_GROUP_CODE"] = GetDataRowVaue(row, "MATERIAL_GROUP_CODE");
                                        newRow["MATERIAL_GROUP_DESC"] = GetDataRowVaue(row, "MATERIAL_GROUP_DESC");
                                        newRow["MATERIAL_CODE"] = GetDataRowVaue(row, "MATERIAL_CODE");
                                        newRow["MATERIAL_TYPE"] = GetDataRowVaue(row, "MATERIAL_TYPE");
                                        newRow["MATERIAL_HSN_CODE"] = GetDataRowVaue(row, "MATERIAL_HSN_CODE");
                                        newRow["MATERIAL_DESCRIPTION"] = GetDataRowVaue(row, "MATERIAL_DESCRIPTION");
                                        newRow["SHORT_TEXT"] = GetDataRowVaue(row, "SHORT_TEXT");
                                        //  newRow["ITEM_TEXT"] = GetDataRowVaue(row, "ITEM_TEXT");
                                        newRow["UOM"] = GetDataRowVaue(row, "UOM");
                                        newRow["QTY_REQUIRED"] = GetDataRowVaue(row, "QTY_REQUIRED")?.Replace(",", "");
                                        newRow["MATERIAL_DELIVERY_DATE"] = !string.IsNullOrWhiteSpace(GetDataRowVaue(row, "MATERIAL_DELIVERY_DATE")) ? Convert.ToDateTime(GetDataRowVaue(row, "MATERIAL_DELIVERY_DATE")).ToString("yyyy-MM-dd") : GetDataRowVaue(row, "MATERIAL_DELIVERY_DATE");
                                        //  newRow["LEAD_TIME"] = GetDataRowVaue(row, "LEAD_TIME");
                                        newRow["PR_TYPE"] = GetDataRowVaue(row, "PR_TYPE");
                                        newRow["PR_TYPE_DESC"] = GetDataRowVaue(row, "PR_TYPE_DESC");
                                        newRow["PR_NOTE"] = GetDataRowVaue(row, "PR_NOTE");
                                        // new 
                                        newRow["PR_CREATOR_NAME"] = GetDataRowVaue(row, "PR_CREATOR_NAME");
                                        newRow["PROJECT_DESCRIPTION"] = GetDataRowVaue(row, "PROJECT_DESCRIPTION");
                                        newRow["PROFIT_CENTER"] = GetDataRowVaue(row, "PROFIT_CENTER");
                                        newRow["ALTERNATE_UOM"] = GetDataRowVaue(row, "ALTERNATE_UOM");
                                        newRow["ALTERNATE_QTY"] = GetDataRowVaue(row, "ALTERNATE_QTY");
                                        newRow["SECTION_HEAD"] = GetDataRowVaue(row, "SECTION_HEAD");
                                        newRow["SERVICE_CODE"] = GetDataRowVaue(row, "SERVICE_CODE");
                                        newRow["SERVICE_CODE_DESCRIPTION"] = GetDataRowVaue(row, "SERVICE_CODE_DESCRIPTION");
                                        newRow["SERVICE_QTY"] = GetDataRowVaue(row, "SERVICE_QTY");
                                        newRow["SERVICE_UNIT_OF_MEASURE"] = GetDataRowVaue(row, "SERVICE_UNIT_OF_MEASURE");
                                        newRow["SERVICE_LINE_ITEM"] = GetDataRowVaue(row, "SERVICE_LINE_ITEM");
                                        newRow["ITEM_CATEGORY"] = GetDataRowVaue(row, "ITEM_CATEGORY");
                                        newRow["ACCOUNT_ASSIGNMENT_CATEGORY"] = GetDataRowVaue(row, "ACCOUNT_ASSIGNMENT_CATEGORY");
                                        newRow["EBAKZ"] = GetDataRowVaue(row, "PR_STATUS_CLOSED");
                                        // new
                                        newRow["LOEKZ"] = GetDataRowVaue(row, "DELETION_STATUS");
                                        newRow["ERDAT"] = !string.IsNullOrWhiteSpace(GetDataRowVaue(row, "CHANGED_ON_DATE")) ? Convert.ToDateTime(GetDataRowVaue(row, "CHANGED_ON_DATE")).ToString("yyyy-MM-dd") : GetDataRowVaue(row, "CHANGED_ON_DATE"); // PR_DATE
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;
                                        newRow["OTHER_INFO1"] = fileName;
                                        newRow["VERTICAL_TYPE"] = GetVerticalTypeDataRowVaue(row, "VERTICAL_TYPE");
                                        newRow["GL_ACCOUNT"] = GetDataRowVaue(row, "GL_ACCOUNT");
                                        newRow["ORDER_NO"] = GetDataRowVaue(row, "ORDER_NO");

                                        query += $@"('{guid}',";
                                        query += $@"'{RemoveFirstandLastChar(row, "PLANT")}',";
                                        query += $@"'{GetDataRowVaue(row, "PLANT_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PURCHASE_GROUP_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PURCHASE_GROUP_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUESITION_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_RELEASE_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_CHANGE_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_NUMBER")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUISITIONER_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUISITIONER_EMAIL")}',";
                                        query += $@"'{GetDataRowVaue(row, "REQUISITION_ITEM")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_GROUP_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_GROUP_DESC")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_TYPE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_HSN_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_DESCRIPTION")}',";
                                        query += $@"'{GetDataRowVaue(row, "SHORT_TEXT")}',";
                                        // query += $@"'{GetDataRowVaue(row, "ITEM_TEXT")}',";
                                        query += $@"'{GetDataRowVaue(row, "UOM")}',";
                                        query += $@"'{GetDataRowVaue(row, "QTY_REQUIRED").Replace(",", "")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_DELIVERY_DATE")}',";
                                        //  query += $@"'{GetDataRowVaue(row, "LEAD_TIME")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_TYPE")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_TYPE_DESC")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_NOTE")}',";
                                        // new
                                        query += $@"'{GetDataRowVaue(row, "PR_CREATOR_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "PROJECT_DESCRIPTION")}',";
                                        query += $@"'{GetDataRowVaue(row, "PROFIT_CENTER")}',";
                                        query += $@"'{GetDataRowVaue(row, "ALTERNATE_UOM")}',";
                                        query += $@"'{GetDataRowVaue(row, "ALTERNATE_QTY")}',";
                                        query += $@"'{GetDataRowVaue(row, "SECTION_HEAD")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_CODE_DESCRIPTION")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_QTY")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_UNIT_OF_MEASURE")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_LINE_ITEM")}',";
                                        query += $@"'{GetDataRowVaue(row, "ITEM_CATEGORY")}',";
                                        query += $@"'{GetDataRowVaue(row, "ACCOUNT_ASSIGNMENT_CATEGORY")}',";
                                        query += $@"'{GetDataRowVaue(row, "PR_STATUS_CLOSED")}',";

                                        // new
                                        query += $@"'{GetDataRowVaue(row, "DELETION_STATUS")}',";
                                        query += $@"'{GetDataRowVaue(row, "CHANGED_ON_DATE")}',";
                                        query += $@"GETUTCDATE(),";
                                        query += $@"'{fileName}',";
                                        query += $@"'{GetVerticalTypeDataRowVaue(row, "VERTICAL_TYPE")}'),"; // VERTICAL_TYPE
                                        query += $@"'{GetDataRowVaue(row, "GL_ACCOUNT")}'),";
                                        query += $@"'{GetDataRowVaue(row, "ORDER_NO")}'),";


                                        tempDT.Rows.Add(newRow);
                                    }

                                    query = query.TrimEnd(',');
                                    //bizClass.ExecuteNonQuery_IUD(query);
                                    logger.Debug("END GetPRDetails QUERY: " + query);
                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_PR_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_pr_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                           // var ticks1 = DateTime.Now.Ticks;
                           // executedFiles = executedFiles.Replace(".csv", "_" + ticks1 + ".csv");
                            File.Move(archiveFolder, executedFiles);

                            File.Delete(ConfigurationManager.AppSettings["PRRUNDATA"] + "\\" + fileName);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }
        private static void ProcessVendorFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPVEndorFilePattern))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv");

                        string executedFiles = _ExecutedFilesFolderVendor + "\\" + Path.GetFileName(filepath);
                        executedFiles = executedFiles.Replace(".xls", ".csv");

                        try
                        {

                            List<string> columns = "NAMEV,NAME1,SMTP_ADDR,TELF1,NAME2,WAERS,KTOKK,TXT30,LIFNR,WERKS,STCD3,TELF2,CITY1,LAND1,ERDAT,ZTERM,TEXT1,EKORG,VEN_CLASS,SPERQ,SPERM,LOEVM,ERDAT1,SPERR,LOEVM1,ERDAT2,SPERM1,LOEVM2,UTIME,BRSCH,ZAHLS,BUKRS,VERTICAL_TYPE,OTHER_INFO1".Split(',').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"Vendor records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(1000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }


                                    string query = @"INSERT INTO [dbo].[SAP_VENDOR_DETAILS] ([JOB_ID], [NAMEV], [NAME1], [SMTP_ADDR], [TELF1], [NAME2],[WAERS],[KTOKK],[TXT30], [LIFNR], [WERKS], [STCD3], [TELF2],
                                    [CITY1],[LAND1], [ERDAT],[ZTERM],[TEXT1],[EKORG],[VEN_CLASS],[SPERQ],[SPERM],[LOEVM],[ERDAT1],[SPERR],[LOEVM1],[ERDAT2],[SPERM1],[LOEVM2], [UTIME], 
                                    [BRSCH],[ZAHLS],[BUKRS],[VERTICAL_TYPE],
                                    [OTHER_INFO1], [DATE_CREATED]) VALUES ";

                                    foreach (var row in chunk)
                                    {
                                        // Vendor First Name|Vendor Last Name|Vendor Email|Vendor PhoneNumber|Vendor Company Name|Vendor Currency|Account Group|Account Description|Vendor ERP Code|Plant|Vendor GST Number|
                                        //   Vendor Alternate phone number|Vendor Vendor Location|Vendor Country Details|Vendor Creation Date in SAP|Payment Terms Code of the Vendors|Payment Terms Desc of the Vendors|
                                        // Vendor Purchase Organization Data|Vendor GST indicator|Posting Block client level|Purch Block client level|Vendor deleted at cleint level|Created on Company code level|
                                        // Co code post block|Co.cde deletion flag|Created on Purchase organization level|Purch block for purchasing organization|Delete flag for purchasing organization|Updated time|
                                        //   Vendor MSME|VENDOR PAYMENT BLOCK|COMPANY_CODE|VERTICAL_TYPE


                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["NAMEV"] = GetDataRowVaue(row, "VENDOR_FIRST_NAME");
                                        newRow["NAME1"] = GetDataRowVaue(row, "VENDOR_LAST_NAME");
                                        newRow["SMTP_ADDR"] = GetDataRowVaue(row, "VENDOR_EMAIL");
                                        newRow["TELF1"] = GetDataRowVaue(row, "VENDOR_PHONENUMBER");
                                        newRow["NAME2"] = GetDataRowVaue(row, "VENDOR_COMPANY_NAME");
                                        newRow["WAERS"] = GetDataRowVaue(row, "VENDOR_CURRENCY");
                                        newRow["KTOKK"] = GetDataRowVaue(row, "ACCOUNT_GROUP");
                                        newRow["TXT30"] = GetDataRowVaue(row, "ACCOUNT_DESCRIPTION");
                                        newRow["LIFNR"] = GetDataRowVaue(row, "VENDOR_ERP_CODE");
                                        //  newRow["MATNR"] = GetDataRowVaue(row, "VENDOR_MATERIAL_CODE");
                                        newRow["WERKS"] = RemoveFirstandLastChar(row, "PLANT");
                                        newRow["STCD3"] = GetDataRowVaue(row, "VENDOR_GST_NUMBER");
                                        newRow["TELF2"] = GetDataRowVaue(row, "VENDOR_ALTERNATE_PHONE_NUMBER");

                                        //newRow["VEN_ALT_FIR"] = GetDataRowVaue(row, "ALT_FIRST_NAME");
                                        //newRow["VEN_ALT_LAS"] = GetDataRowVaue(row, "ALT_LAST_NAME");
                                        //newRow["SMTP_ADDR1"] = GetDataRowVaue(row, "ALTEMAIL");
                                        //newRow["TEL_NUMBER"] = GetDataRowVaue(row, "ALTPHONE_NO");

                                        newRow["CITY1"] = GetDataRowVaue(row, "VENDOR_VENDOR_LOCATION");
                                        newRow["LAND1"] = GetDataRowVaue(row, "VENDOR_COUNTRY_DETAILS");
                                        newRow["ERDAT"] = GetDataRowVaue(row, "VENDOR_CREATION_DATE_IN_SAP");
                                        newRow["ZTERM"] = GetDataRowVaue(row, "PAYMENT_TERMS_CODE_OF_THE_VENDORS");
                                        newRow["TEXT1"] = GetDataRowVaue(row, "PAYMENT_TERMS_DESC_OF_THE_VENDORS");
                                        newRow["EKORG"] = GetDataRowVaue(row, "VENDOR_PURCHASE_ORGANIZATION_DATA");
                                        newRow["VEN_CLASS"] = GetDataRowVaue(row, "VENDOR_GST_INDICATOR");
                                        newRow["SPERQ"] = GetDataRowVaue(row, "POSTING_BLOCK_CLIENT_LEVEL");
                                        newRow["SPERM"] = GetDataRowVaue(row, "PURCH_BLOCK_CLIENT_LEVEL");
                                        newRow["LOEVM"] = GetDataRowVaue(row, "VENDOR_DELETED_AT_CLEINT_LEVEL");
                                        newRow["ERDAT1"] = GetDataRowVaue(row, "CREATED_ON_COMPANY_CODE_LEVEL");
                                        newRow["SPERR"] = GetDataRowVaue(row, "CO_CODE_POST_BLOCK");
                                        newRow["LOEVM1"] = GetDataRowVaue(row, "CO_CDE_DELETION_FLAG");
                                        newRow["ERDAT2"] = GetDataRowVaue(row, "CREATED_ON_PURCHASE_ORGANIZATION_LEVEL");
                                        newRow["SPERM1"] = GetDataRowVaue(row, "PURCH_BLOCK_FOR_PURCHASING_ORGANIZATION");
                                        newRow["LOEVM2"] = GetDataRowVaue(row, "DELETE_FLAG_FOR_PURCHASING_ORGANIZATION");
                                        newRow["UTIME"] = GetDataRowVaue(row, "UPDATED_TIME");
                                        newRow["BRSCH"] = GetDataRowVaue(row, "VENDOR_MSME");
                                        newRow["ZAHLS"] = GetDataRowVaue(row, "VENDOR_PAYMENT_BLOCK");
                                        newRow["BUKRS"] = GetDataRowVaue(row, "COMPANY_CODE");
                                        newRow["VERTICAL_TYPE"] = GetVerticalTypeDataRowVaue(row, "VERTICAL_TYPE");


                                        //newRow["TYPE_CODE"] = GetDataRowVaue(row, "TYPE_CODE");
                                        //newRow["TYPE_DESC"] = GetDataRowVaue(row, "TYPE_DESC");
                                        //newRow["BANK_ACCOUNT_NO"] = GetDataRowVaue(row, "BANK_ACCOUNT_NO");
                                        //newRow["IFSC_CODE"] = GetDataRowVaue(row, "IFSC_CODE");
                                        //newRow["BANK_NAME"] = GetDataRowVaue(row, "BANK_NAME");
                                        //newRow["PAN_NUMBER"] = GetDataRowVaue(row, "PAN_NUMBER");

                                        newRow["OTHER_INFO1"] = fileName;
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;

                                        query += $@"('{guid}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_FIRST_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_LAST_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_EMAIL")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_PHONENUMBER")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_COMPANY_NAME")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_CURRENCY")}',";
                                        query += $@"'{GetDataRowVaue(row, "ACCOUNT_GROUP")}',";
                                        query += $@"'{GetDataRowVaue(row, "ACCOUNT_DESCRIPTION")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_ERP_CODE")}',";
                                        // query += $@"'{GetDataRowVaue(row, "VENDOR_MATERIAL_CODE")}',";
                                        query += $@"'{RemoveFirstandLastChar(row, "PLANT")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_GST_NUMBER")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_ALTERNATE_PHONE_NUMBER")}',";

                                        //query += $@"'{GetDataRowVaue(row, "ALT_FIRST_NAME")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ALT_LAST_NAME")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ALTEMAIL")}',";
                                        //query += $@"'{GetDataRowVaue(row, "ALTPHONE_NO")}',";

                                        query += $@"'{GetDataRowVaue(row, "VENDOR_VENDOR_LOCATION")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_COUNTRY_DETAILS")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_CREATION_DATE_IN_SAP")}',";
                                        query += $@"'{GetDataRowVaue(row, "PAYMENT_TERMS_CODE_OF_THE_VENDORS")}',";
                                        query += $@"'{GetDataRowVaue(row, "PAYMENT_TERMS_DESC_OF_THE_VENDORS")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_PURCHASE_ORGANIZATION_DATA")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_GST_INDICATOR")}',";
                                        query += $@"'{GetDataRowVaue(row, "POSTING_BLOCK_CLIENT_LEVEL")}',";
                                        query += $@"'{GetDataRowVaue(row, "PURCH_BLOCK_CLIENT_LEVEL")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_DELETED_AT_CLEINT_LEVEL")}',";
                                        query += $@"'{GetDataRowVaue(row, "CREATED_ON_COMPANY_CODE_LEVEL")}',";
                                        query += $@"'{GetDataRowVaue(row, "CO_CODE_POST_BLOCK")}',";
                                        query += $@"'{GetDataRowVaue(row, "CO_CDE_DELETION_FLAG")}',";
                                        query += $@"'{GetDataRowVaue(row, "CREATED_ON_PURCHASE_ORGANIZATION_LEVEL")}',";
                                        query += $@"'{GetDataRowVaue(row, "PURCH_BLOCK_FOR_PURCHASING_ORGANIZATION")}',";
                                        query += $@"'{GetDataRowVaue(row, "DELETE_FLAG_FOR_PURCHASING_ORGANIZATION")}',";
                                        query += $@"'{GetDataRowVaue(row, "UPDATED_TIME")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_MSME")}',";
                                        query += $@"'{GetDataRowVaue(row, "VENDOR_PAYMENT_BLOCK")}',";
                                        query += $@"'{GetDataRowVaue(row, "COMPANY_CODE")}',";
                                        query += $@"'{GetVerticalTypeDataRowVaue(row, "VERTICAL_TYPE")}',";


                                        //query += $@"'{GetDataRowVaue(row, "TYPE_CODE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "TYPE_DESC")}',";
                                        //query += $@"'{GetDataRowVaue(row, "BANK_ACCOUNT_NO")}',";
                                        //query += $@"'{GetDataRowVaue(row, "IFSC_CODE")}',";
                                        //query += $@"'{GetDataRowVaue(row, "BANK_NAME")}',";
                                        //query += $@"'{GetDataRowVaue(row, "PAN_NUMBER")}',";

                                        query += $@"'{fileName}',";
                                        query += $@"GETUTCDATE()),";

                                        tempDT.Rows.Add(newRow);
                                    }

                                    query = query.TrimEnd(',');
                                    //bizClass.ExecuteNonQuery_IUD(query);
                                    logger.Debug("END GetVENDORDetails QUERY: " + query);
                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_VENDOR_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_vendor_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                           // var ticks1 = DateTime.Now.Ticks;
                           // executedFiles = executedFiles.Replace(".csv", "_" + ticks1 + ".csv");
                            File.Move(archiveFolder, executedFiles);

                            File.Delete(ConfigurationManager.AppSettings["VEND_RUNDATA"] + "\\" + fileName);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }
        private static void ProcesGRNFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPGRNFilePattern))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv");

                        try
                        {
                            List<string> columns = @"BUDAT;ZZSBILL_ENTRY;ZZBILL_ENTRY;ZZDATE_SBILL_ENTRY;SGTXT;WAERS;BLDAT;VFDAT;WRBTR;WRBTRSpecified;ZZGATE_ENT_DATE;ZZGATE_ENTRY;CHARG;LFBNR;BUDAT1;ZEILE;MBLNR;MJAHR;MENGE;MENGESpecified;WRBTR1;ZZBILL_DOC;ZZLR_DATE;ZZLR_NUMBER;ZZLUT_NUM;HSDAT;MAKTX;MATNR;BWART;WERKS;MENGE1;RDATE;SRVPOS;LFBNR1;LFPOS;STATUS;LGORT;MEINS;CHARG1;LIFNR;NAME1;LAND1;PS_PSP_PNR;EBELN;EBELP;CPUDT_MKPF;CPUTM_MKPF;OTHER_INFO1".Split(';').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"Vendor records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["BUDAT"] = GetDataRowVaue(row, "ACUTAL_DELIVERY_DATE");
                                        newRow["WAERS"] = GetDataRowVaue(row, "CURRENCY");
                                        newRow["BLDAT"] = GetDataRowVaue(row, "DOCUMENT_DATE");
                                        newRow["VFDAT"] = GetDataRowVaue(row, "EXPIRY_DATE");
                                        newRow["WRBTR"] = string.IsNullOrEmpty(GetDataRowVaue(row, "FRIEGHT_VALUE")) ? "0" : GetDataRowVaue(row, "FRIEGHT_VALUE");
                                        newRow["CHARG"] = GetDataRowVaue(row, "GR_BATCH_NO");
                                        newRow["LFBNR"] = GetDataRowVaue(row, "GR_REFERENCE");
                                        newRow["BUDAT1"] = GetDataRowVaue(row, "GRN_DT");
                                        newRow["ZEILE"] = GetDataRowVaue(row, "GRN_LINE_ITEM");
                                        newRow["MBLNR"] = GetDataRowVaue(row, "GRN_NO");
                                        newRow["MJAHR"] = GetDataRowVaue(row, "GR_YEAR");
                                        newRow["MENGE"] = string.IsNullOrEmpty(GetDataRowVaue(row, "GRN_QTY")) ? "0" : GetDataRowVaue(row, "GRN_QTY");
                                        newRow["WRBTR1"] = string.IsNullOrEmpty(GetDataRowVaue(row, "GRN_VALUE")) ? "0" : GetDataRowVaue(row, "GRN_VALUE");
                                        newRow["HSDAT"] = GetDataRowVaue(row, "MANUFACTURING_DATE");
                                        newRow["MAKTX"] = GetDataRowVaue(row, "MAT_DESCRIPTION");
                                        newRow["MATNR"] = GetDataRowVaue(row, "MATERIAL");
                                        newRow["BWART"] = GetDataRowVaue(row, "MV_TYPE");
                                        newRow["WERKS"] = RemoveFirstandLastChar(row, "PLANT");
                                        newRow["MENGE1"] = string.IsNullOrEmpty(GetDataRowVaue(row, "PO_QUANTITY")) ? "0" : GetDataRowVaue(row, "PO_QUANTITY");
                                        newRow["RDATE"] = GetDataRowVaue(row, "RETEST_DATE");
                                        newRow["SRVPOS"] = GetDataRowVaue(row, "SERVICE_CODE");
                                        newRow["LFBNR1"] = GetDataRowVaue(row, "SERVICE_ENTRY_SHEET");
                                        newRow["LFPOS"] = GetDataRowVaue(row, "SERVICE_LINE_ITEM");
                                        newRow["STATUS"] = GetDataRowVaue(row, "STATUS");
                                        newRow["LGORT"] = GetDataRowVaue(row, "STORAGE_LOCATION");
                                        newRow["MEINS"] = GetDataRowVaue(row, "UOM");
                                        newRow["CHARG1"] = GetDataRowVaue(row, "VENDOR_BATCH_NO");
                                        newRow["LIFNR"] = GetDataRowVaue(row, "VENDOR_CODE");
                                        newRow["NAME1"] = GetDataRowVaue(row, "VENDOR_NAME");
                                        newRow["LAND1"] = GetDataRowVaue(row, "VENDORS_COUNTRY");
                                        newRow["PS_PSP_PNR"] = GetDataRowVaue(row, "WBS_ELEMENT");
                                        newRow["EBELN"] = GetDataRowVaue(row, "PO_NUMBER");
                                        newRow["EBELP"] = GetDataRowVaue(row, "PO_LINE_ITEM");
                                        newRow["CPUDT_MKPF"] = GetDataRowVaue(row, "GR_CREATION_DATE");
                                        newRow["CPUTM_MKPF"] = GetDataRowVaue(row, "GR_CREATION_TIME");
                                        newRow["OTHER_INFO1"] = fileName;
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;

                                        tempDT.Rows.Add(newRow);
                                    }

                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_GRN_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_grn_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }
        private static void ProcesPOScheduleFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPPOFilePattern))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv");

                        try
                        {
                            List<string> columns = @"WERKS;MATNR;TXZ01;MATKL;LIFNR;NAME1;TELF1;SMTP_ADDR;EBELN;EBELP;AEDAT;ERNAM;ZTERM;MWSKZ;TEXT1;UDATE;EINDT;MTART;ORT01;REGIO;J_1BNBM;ORD_QTY;MEINS;MEINS1;MENGE1;MENGE1Specified;EFFWR;EFFWRSpecified;NETWR1;NETWR1Specified;WAERS;KNUMV;KNUMVSpecified;MENGE2;MENGE2Specified;BANFN;BNFPO;MENGE3;MENGE3Specified;UDATE1;LTEXT1;LFDAT;LTEXT2;FRGKZ;AFNAM;BSART;SRVPOS;ASKTX;KNUMV1;KNUMV1Specified;KNUMV2;KNUMV2Specified;BSTYP;KDATB;KDATE;AEDAT1;ELIKZ;LOEKZ;OTHER_INFO1".Split(';').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"PO SCH records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;

                                        newRow["WERKS"] = RemoveFirstandLastChar(row, "PLANT");
                                        newRow["MATNR"] = GetDataRowVaue(row, "MATERIAL");
                                        newRow["TXZ01"] = GetDataRowVaue(row, "DESCRIPTION");
                                        newRow["MATKL"] = GetDataRowVaue(row, "MATERIAL_GROUP");
                                        newRow["LIFNR"] = GetDataRowVaue(row, "VENDOR");
                                        newRow["NAME1"] = GetDataRowVaue(row, "VENDOR_NAME");
                                        newRow["TELF1"] = GetDataRowVaue(row, "VENDOR_PRIMARY_PHONE_NUMBER");
                                        newRow["SMTP_ADDR"] = GetDataRowVaue(row, "VENDOR_PRIMARY_EMAIL");
                                        newRow["EBELN"] = GetDataRowVaue(row, "PO_NUMBER");
                                        newRow["EBELP"] = GetDataRowVaue(row, "PO_LINE_ITEM");
                                        newRow["AEDAT"] = GetDataRowVaue(row, "PO_DATE");
                                        newRow["ERNAM"] = GetDataRowVaue(row, "PO_CREATOR");
                                        newRow["ZTERM"] = GetDataRowVaue(row, "PAYMENT_TERMS");
                                        newRow["MWSKZ"] = GetDataRowVaue(row, "TAX_CODE");
                                        newRow["TEXT1"] = GetDataRowVaue(row, "TAX_CODE_DESCRIPTION");
                                        newRow["UDATE"] = GetDataRowVaue(row, "PO_RELEASE_DATE");
                                        newRow["EINDT"] = GetDataRowVaue(row, "DELIVERY_DATE");
                                        newRow["MTART"] = GetDataRowVaue(row, "MATERIAL_TYPE");
                                        newRow["ORT01"] = GetDataRowVaue(row, "CITY");
                                        newRow["REGIO"] = GetDataRowVaue(row, "REGION_DESC");
                                        newRow["J_1BNBM"] = GetDataRowVaue(row, "HSN_CODE");
                                        newRow["ORD_QTY"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "ORD_QTY")) ? GetDataRowVaue(row, "ORD_QTY") : "0";
                                        newRow["MEINS"] = GetDataRowVaue(row, "UOM");
                                        newRow["MEINS1"] = GetDataRowVaue(row, "ALTERNATIVE_UOM");
                                        newRow["MENGE1"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "QTY_IN_ALTERNATE_UOM")) ? GetDataRowVaue(row, "QTY_IN_ALTERNATE_UOM") : "0";
                                        newRow["EFFWR"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "NET_PRICE")) ? GetDataRowVaue(row, "NET_PRICE") : "0";
                                        newRow["NETWR1"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "VALUE_INR")) ? GetDataRowVaue(row, "VALUE_INR") : "0";
                                        newRow["WAERS"] = GetDataRowVaue(row, "CURRENCY");
                                        newRow["KNUMV"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "FREIGHT")) ? GetDataRowVaue(row, "FREIGHT") : "0";
                                        newRow["MENGE2"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "PEND_QTY")) ? GetDataRowVaue(row, "PEND_QTY") : "0";
                                        newRow["BANFN"] = GetDataRowVaue(row, "PR_LINE");
                                        newRow["BNFPO"] = GetDataRowVaue(row, "PR_NUM");
                                        newRow["MENGE3"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "PR_QTY")) ? GetDataRowVaue(row, "PR_QTY") : "0";
                                        newRow["UDATE1"] = GetDataRowVaue(row, "PR_RELEASE_DATE");
                                        newRow["LTEXT1"] = GetDataRowVaue(row, "PR_LINE_TEXT");
                                        newRow["LFDAT"] = GetDataRowVaue(row, "PR_DELV.DATE");
                                        newRow["LTEXT2"] = GetDataRowVaue(row, "ITEM_TEXT_PO");
                                        newRow["FRGKZ"] = GetDataRowVaue(row, "REL_IND");
                                        newRow["AFNAM"] = GetDataRowVaue(row, "PR_REQUISITIONER");
                                        newRow["BSART"] = GetDataRowVaue(row, "DOC_TYPE");
                                        newRow["SRVPOS"] = GetDataRowVaue(row, "SERVICE_CODE");
                                        newRow["ASKTX"] = GetDataRowVaue(row, "SERVICE_DESCRIPTION");
                                        newRow["KNUMV1"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "MISC_CHARGES")) ? GetDataRowVaue(row, "MISC_CHARGES") : "0";
                                        newRow["KNUMV2"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "PACKING_CHARGES")) ? GetDataRowVaue(row, "PACKING_CHARGES") : "0";
                                        newRow["BSTYP"] = GetDataRowVaue(row, "PO_CONTRACT");
                                        newRow["KDATB"] = GetDataRowVaue(row, "VALID_FROM");
                                        newRow["KDATE"] = GetDataRowVaue(row, "VALID_TO");
                                        newRow["ELIKZ"] = GetDataRowVaue(row, "DELIVERY_COMPLETION_INDICATOR");
                                        newRow["LOEKZ"] = GetDataRowVaue(row, "DELETED_INDICATOR");
                                        newRow["OTHER_INFO1"] = fileName;
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;

                                        tempDT.Rows.Add(newRow);
                                    }

                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_PO_SCHEDULE_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_po_schedule_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }
        private static void ProcessMaterialsFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();

                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPMATERIALFilePattern))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv");

                        string executedFiles = _ExecutedFilesFolderMaterial + "\\" + Path.GetFileName(filepath);
                        executedFiles = executedFiles.Replace(".xls", ".csv");

                        try
                        {

                            List<string> columns = "MATKL,WGBEZ,MATNR,MTART,STEUC,MAKTX,MEINS,CASNR,MEINH,TAXIM,LVORM,MMSTA,MSTAE,ERSDA,LAEDA,WERKS,ASNUM,ASKTX,MEINS1,ASTYP,MATKL1,LVORM1,ERDAT1,AEDAT1,BEST_PRICE,VERTICAL_TYPE".Split(',').ToList(); ;
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);

                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"Material records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(5000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));

                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    string query = @"INSERT INTO [dbo].[SAP_MATERIAL_DETAILS] ([JOB_ID], [MATKL],[WGBEZ],[MATNR],[MTART],[STEUC],[MAKTX],[MEINS],[CASNR],[MEINH],[TAXIM],
                                                [LVORM],[MMSTA],[MSTAE],[ERSDA],[LAEDA],[WERKS],[ASNUM],[ASKTX],[MEINS1],[ASTYP],[MATKL1],[LVORM1],[ERDAT1],[AEDAT1],[BEST_PRICE],[VERTICAL_TYPE]) VALUES ";

                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["MATKL"] = GetDataRowVaue(row, "MATERIAL_GROUP_CODE");
                                        newRow["WGBEZ"] = GetDataRowVaue(row, "MATERIAL_GROUP_DESC");
                                        newRow["MATNR"] = GetDataRowVaue(row, "MATERIAL_CODE");
                                        newRow["MTART"] = GetDataRowVaue(row, "MATERIAL_TYPE");
                                        newRow["STEUC"] = GetDataRowVaue(row, "MATERIAL_HSN_CODE");
                                        newRow["MAKTX"] = GetDataRowVaue(row, "MATERIAL_DESCRIPTION");
                                        newRow["MEINS"] = GetDataRowVaue(row, "UOM");
                                        newRow["CASNR"] = GetDataRowVaue(row, "CAS_NUMBER");
                                        newRow["MEINH"] = GetDataRowVaue(row, "ALTERNATE_UOM1");
                                        newRow["TAXIM"] = GetDataRowVaue(row, "TAX_INDICATOR");
                                        newRow["LVORM"] = GetDataRowVaue(row, "MATERIAL_DELETION_AT_PLANT_LEVEL");
                                        newRow["MMSTA"] = GetDataRowVaue(row, "MATERIAL_BLOCKED_STATUS_AT_PLANT_LEVEL");
                                        newRow["MSTAE"] = GetDataRowVaue(row, "MATERIAL_BLOCKED_STATUS_AT_CLIENT_LEVEL");
                                        newRow["ERSDA"] = GetDataRowVaue(row, "MATERIAL_CREATED_DATE");
                                        newRow["LAEDA"] = GetDataRowVaue(row, "MATERIAL_CHANGED_ON");
                                        newRow["WERKS"] = RemoveFirstandLastChar(row, "PLANT");
                                        newRow["ASNUM"] = GetDataRowVaue(row, "SERVICE_CODE");
                                        newRow["ASKTX"] = GetDataRowVaue(row, "SERVICE_CODE_DESC");
                                        newRow["MEINS1"] = GetDataRowVaue(row, "SERVICE_UOM");
                                        newRow["ASTYP"] = GetDataRowVaue(row, "SERVICE_CATEGORY");
                                        newRow["MATKL1"] = GetDataRowVaue(row, "SERVICE_MATERIAL_GROUP");
                                        newRow["LVORM1"] = GetDataRowVaue(row, "SERVICE_DELETION_STATUS");
                                        newRow["ERDAT1"] = GetDataRowVaue(row, "SERVICE_CODE_CREATED_ON");
                                        newRow["AEDAT1"] = GetDataRowVaue(row, "SERVICE_CODE_CHANGED_ON");
                                        newRow["BEST_PRICE"] = Convert.ToDecimal(GetDataRowVaue(row, "BEST_PRICE"));
                                        newRow["VERTICAL_TYPE"] = GetVerticalTypeDataRowVaue(row, "VERTICAL_TYPE");


                                        newRow["DATE_CREATED"] = DateTime.UtcNow;

                                        query += $@"('{guid}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_GROUP_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_GROUP_DESC")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_TYPE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_HSN_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_DESCRIPTION")}',";
                                        query += $@"'{GetDataRowVaue(row, "UOM")}',";
                                        query += $@"'{GetDataRowVaue(row, "CAS_NUMBER")}',";
                                        query += $@"'{GetDataRowVaue(row, "ALTERNATE_UOM1")}',";
                                        query += $@"'{GetDataRowVaue(row, "TAX_INDICATOR")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_DELETION_AT_PLANT_LEVEL")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_BLOCKED_STATUS_AT_PLANT_LEVEL")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_BLOCKED_STATUS_AT_CLIENT_LEVEL")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_CREATED_DATE")}',";
                                        query += $@"'{GetDataRowVaue(row, "MATERIAL_CHANGED_ON")}',";
                                        query += $@"'{RemoveFirstandLastChar(row, "PLANT")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_CODE")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_CODE_DESC")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_UOM")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_CATEGORY")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_MATERIAL_GROUP")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_DELETION_STATUS")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_CODE_CREATED_ON")}',";
                                        query += $@"'{GetDataRowVaue(row, "SERVICE_CODE_CHANGED_ON")}',";
                                        query += $@"'{Convert.ToDecimal(GetDataRowVaue(row, "BEST_PRICE"))}',";
                                        query += $@"'{GetVerticalTypeDataRowVaue(row, "VERTICAL_TYPE")}',";
                                        query += $@"GETUTCDATE()),";

                                        tempDT.Rows.Add(newRow);
                                    }
                                    query = query.TrimEnd(',');
                                    //bizClass.ExecuteNonQuery_IUD(query);
                                    logger.Debug("END GetMaterialDetails QUERY: " + query);
                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_MATERIAL_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }
                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_material_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                           // var ticks1 = DateTime.Now.Ticks;
                           // executedFiles = executedFiles.Replace(".csv", "_" + ticks1 + ".csv");
                            File.Move(archiveFolder, executedFiles);

                            File.Delete(ConfigurationManager.AppSettings["MAT_RUNDATA"] + "\\" + fileName);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }
        private static void DownloadFtpDirectory(string url, NetworkCredential credentials, string localPath, string jobType)
        {
            FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(url);
            listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            listRequest.Credentials = credentials;

            List<string> lines = new List<string>();
            
            using (FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse())
            using (Stream listStream = listResponse.GetResponseStream())
            using (StreamReader listReader = new StreamReader(listStream))
            {
                while (!listReader.EndOfStream)
                {
                    lines.Add(listReader.ReadLine());
                }
            }
            foreach (string line in lines)
            {
                try
                {
                    string[] tokens = line.Split(new[] { ' ' }, 9, StringSplitOptions.RemoveEmptyEntries);
                    string name = tokens[8];

                    string localFilePath = Path.Combine(localPath, name);
                    string fileUrl = url + name;

                    if (tokens[2] == "<DIR>" && false)
                    {
                        if (!Directory.Exists(localFilePath))
                        {
                            Directory.CreateDirectory(localFilePath);
                        }

                        DownloadFtpDirectory(fileUrl + "/", credentials, localFilePath, jobType);
                    }
                    else if (tokens[2] != "<DIR>" && !string.IsNullOrWhiteSpace(name) && name.ToLower().Contains(".csv"))
                    {
                        logger.Info("FILEURL:" + fileUrl);
                        if (fileUrl.ToUpper().Contains(jobType))
                        {
                            FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                            downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                            downloadRequest.Credentials = credentials;
                            using (FtpWebResponse downloadResponse = (FtpWebResponse)downloadRequest.GetResponse())
                            using (Stream sourceStream = downloadResponse.GetResponseStream())
                            using (Stream targetStream = File.Create(localFilePath))
                            {
                                byte[] buffer = new byte[10240];
                                int read;
                                while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    targetStream.Write(buffer, 0, read);
                                }
                            }

                            FtpWebRequest fileMoveRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                            fileMoveRequest.UseBinary = true;
                            fileMoveRequest.Method = WebRequestMethods.Ftp.Rename;
                            fileMoveRequest.Credentials = credentials;
                            //fileMoveRequest.RenameTo = $"{ConfigurationManager.AppSettings["FTPArchiveFolder"]}{name}";
                            var response = (FtpWebResponse)fileMoveRequest.GetResponse();
                            bool Success = response.StatusCode == FtpStatusCode.CommandOK || response.StatusCode == FtpStatusCode.FileActionOK;
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"DOWNLOAD FTP FILE: {line} with ERROR:{ex.Message}, DETAILS: {ex.StackTrace}");
                }
            }
        }

        private static void GetPOResponseFromFTP()
        {
            // string url, NetworkCredential credentials, string localPath, string jobType
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();

                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPPORESPONSEPattern))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv");

                        string executedFiles = _ExecutedFilesFolder + "\\" + Path.GetFileName(filepath);
                        executedFiles = executedFiles.Replace(".xls", ".csv");

                        try
                        {

                            //List<string> columns = "MATKL,WGBEZ,MATNR,MTART,STEUC,MAKTX,MEINS,CASNR,MEINH,TAXIM,LVORM,MMSTA,MSTAE,ERSDA,LAEDA,WERKS,ASNUM,ASKTX,MEINS1,ASTYP,MATKL1,LVORM1,ERDAT1,AEDAT1,BEST_PRICE,VERTICAL_TYPE".Split(',').ToList(); ;
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            string query = "";
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"PO Response records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(5000);
                                foreach (var chunk in prChunks)
                                {
                                    foreach (var row in chunk)
                                    {
                                        string sapPONum = GetDataRowVaue(row, "SAP_PO_NUMBER");
                                        string prmPONum = GetDataRowVaue(row, "PRM_PO_NUMBER");
                                        query += string.Format("UPDATE POScheduleDetails SET PO_NUMBER = '{0}' WHERE PRM_PO_NUMBER = '{1}';", sapPONum, prmPONum);
                                    }
                                    bizClass.ExecuteNonQuery_IUD(query);
                                }
                            }
                           
                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                            File.Move(archiveFolder, executedFiles);
                            File.Delete(ConfigurationManager.AppSettings["PRRUNDATA"] + "\\" + fileName);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }

        private static string GetVerticalTypeDataRowVaue(DataRow row, string columnName)
        {
            if (row.Table.Columns.Contains(columnName))
            {
                var text = row[columnName] != DBNull.Value ? row[columnName].ToString() : string.Empty;
                if (string.IsNullOrEmpty(text))
                    return text;

                string result = Regex.Replace(text, "[:!@#$%^&*()}{|\":?><\\[\\]\\;'/.,~\n]", "");
                return result;

            }
            else
            {
                return string.Empty;
            }

        }

        private static string GetDataRowVaue(DataRow row, string columnName)
        {
            if (row.Table.Columns.Contains(columnName))
            {
                return row[columnName] != DBNull.Value ? row[columnName].ToString() : string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }

        private static string RemoveFirstandLastChar(DataRow row, string columnName)
        {
            if (row.Table.Columns.Contains(columnName))
            {
                string resultString = row[columnName] != DBNull.Value ? row[columnName].ToString() : string.Empty;
                string unQuotedString = "";
                char[] charArr = { '\'', '\"', ',', '"', '-' };
                foreach (char C in charArr)
                {
                    string C1 = Convert.ToString(C);
                    if (resultString.StartsWith(C1))
                    {
                        char C2 = Convert.ToChar(C1);
                        unQuotedString = resultString.TrimStart(C2);

                    }
                    else if (resultString.EndsWith(C1))
                    {
                        char C2 = Convert.ToChar(C1);
                        unQuotedString = resultString.TrimEnd(C2);
                    }
                    else
                    {
                        unQuotedString = resultString;
                    }
                }
                resultString = unQuotedString;

                return resultString;
            }
            else
            {
                return string.Empty;
            }
        }

        public string RemoveAllSpecialCharacters(string text)
        {
            if (string.IsNullOrEmpty(text))
                return text;

            string result = Regex.Replace(text, "[:!@#$%^&*()}{|\":?><\\[\\]\\;'/.,~\n]", " ");
            return result;
        }
    }
}
