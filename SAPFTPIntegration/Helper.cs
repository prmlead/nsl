﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SAPFTPIntegration
{
    public static class Helper
    {

        public static DataTable ReadCsvFile1(string path, string[] csvDelimiter, string fileName)
        {
            DataTable dtCsv = new DataTable();
            string Fulltext;
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                    string[] rows = Fulltext.Split('\n'); //split full file text into rows  

                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        string[] rowValues = rows[i].Split(csvDelimiter, StringSplitOptions.None); //split each row with comma to get individual values  
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    string columnName = string.Empty;
                                    if (rowValues[j] != null)
                                    {
                                        columnName = rowValues[j].Trim();
                                        if (columnName.Contains("("))
                                        {
                                            columnName = columnName.Split('(')[0];
                                        }

                                        columnName = columnName.Replace(" ", "_");
                                        if (columnName.Equals("REQUESITION_DATE", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            columnName = "REQUESITION_DATE";
                                        }
                                        if (columnName.Equals("ITEM_OF_REQUESITION", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            columnName = "REQUISITION_ITEM";
                                        }
                                    }

                                    columnName = columnName.ToUpper();
                                    if (columnName.ToLower().Contains("date"))
                                    {
                                        dtCsv.Columns.Add(columnName, typeof(DateTime));
                                    }
                                    else
                                    {
                                        dtCsv.Columns.Add(columnName, typeof(String));
                                    }
                                }

                                dtCsv.Columns.Add("FILE_NAME", typeof(String));
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                string dateValue = ConfigurationManager.AppSettings["DATE_FORMAT"].ToString();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    string columnValue = rowValues[k]?.ToString().Trim();
                                    try
                                    {
                                        DateTime dt;
                                        DateTime.TryParseExact(columnValue,
                                                               dateValue,
                                                               CultureInfo.InvariantCulture,
                                                               DateTimeStyles.None,
                                                               out dt);
                                        if (dt.Year != 0001)
                                        {
                                            dr[k] = dt;
                                        }
                                        else
                                        {
                                            dr[k] = columnValue;
                                        }
                                    }
                                    catch
                                    {
                                        try
                                        {
                                            dr[k] = columnValue;
                                        }
                                        catch
                                        {
                                            //dr[k] = columnValue;
                                        }
                                    }

                                }

                                dr["FILE_NAME"] = fileName;
                                dtCsv.Rows.Add(dr); //add other rows  
                            }
                        }
                    }
                }
            }
            return dtCsv;
        }

        public static DataTable ReadCsvFile(string path, char csvDelimiter, string fileName)
        {
            DataTable dtCsv = new DataTable();
            string Fulltext;
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                    string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        string[] rowValues = rows[i].Split(csvDelimiter); //split each row with comma to get individual values  
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    string columnName = string.Empty;
                                    if (rowValues[j] != null)
                                    {
                                        columnName = rowValues[j].Trim();
                                        if (columnName.Contains("("))
                                        {
                                            columnName = columnName.Split('(')[0];
                                        }

                                            columnName = columnName.Replace(" ", "_");
                                            if (columnName.Equals("REQUESITION_DATE", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                columnName = "REQUESITION_DATE";
                                            }
                                            if (columnName.Equals("ITEM_OF_REQUESITION", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                columnName = "REQUISITION_ITEM";
                                            }
                                    }

                                    columnName = columnName.ToUpper();
                                    columnName = Regex.Replace(columnName, "[:!@#$%^&*()}{|\":?><\\[\\]\\;'/.,~\n]", "");
                                    if (columnName.ToLower().Contains("date"))
                                    {
                                        dtCsv.Columns.Add(columnName, typeof(DateTime));
                                    }
                                    else
                                    {
                                        dtCsv.Columns.Add(columnName, typeof(String));
                                    }
                                }

                                dtCsv.Columns.Add("FILE_NAME", typeof(String));
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                string dateValue = ConfigurationManager.AppSettings["DATE_FORMAT"].ToString();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    string columnValue = rowValues[k]?.ToString().Trim();
                                    try
                                    {
                                        DateTime dt;
                                        DateTime.TryParseExact(columnValue,
                                                               dateValue,
                                                               CultureInfo.InvariantCulture,
                                                               DateTimeStyles.None,
                                                               out dt);
                                        if(dt.Year != 0001)
                                        {
                                            dr[k] = dt;
                                        }
                                        else
                                        {
                                            dr[k] = columnValue;
                                        }
                                    }
                                    catch {
                                        try
                                        {
                                            dr[k] = columnValue;
                                        }
                                        catch
                                        {
                                            //dr[k] = columnValue;
                                        }
                                    }
                                    
                                }

                                dr["FILE_NAME"] = fileName;
                                dtCsv.Rows.Add(dr); //add other rows  
                            }
                        }
                    }
                }
            }
            return dtCsv;
        }
       
        public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> source, int chunksize)
        {
            while (source.Any())
            {
                yield return source.Take(chunksize);
                source = source.Skip(chunksize);
            }
        }

        //List<string> PRColumns = new List<string>();
        //if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PR_JOB")
        //{
        //    PRColumns = "PLANT,PLANT_CODE,PURCHASE_GROUP_CODE,PURCHASE_GROUP_NAME,REQUESITION_DATE,PR_RELEASE_DATE,PR_CHANGE_DATE,PR_NUMBER,REQUISITIONER_NAME,REQUISITIONER_EMAIL,ITEM_OF_REQUESITION,MATERIAL_GROUP_CODE,MATERIAL_GROUP_DESC,MATERIAL_CODE,MATERIAL_TYPE,MATERIAL_HSN_CODE,MATERIAL_DESCRIPTION,SHORT_TEXT,UOM,QTY_REQUIRED,MATERIAL_DELIVERY_DATE,PR_TYPE,PR_TYPE_DESC,PR_NOTE,PR_CREATOR_NAME,PROJECT_DESCRIPTION,PROFIT_CENTER,ALTERNATE_UOM,ALTERNATE_QTY,SECTION_HEAD,SERVICE_CODE,SERVICE_CODE_DESCRIPTION,SERVICE_QTY,SERVICE_UNIT_OF_MEASURE,SERVICE_LINE_ITEM,ITEM_CATEGORY,ACCOUNT_ASSIGNMENT_CATEGORY,EBAKZ,LOEKZ,ERDAT,OTHER_INFO1,VERTICAL_TYPE".Split(',').ToList();
        //    for (int t = 0; t < PRColumns.Count(); t++)
        //    {
        //        string columnName = string.Empty;
        //        if (PRColumns[t] != null)
        //        {
        //            columnName = PRColumns[t].Trim();
        //            if (columnName.Contains("("))
        //            {
        //                columnName = columnName.Split('(')[0];
        //            }

        //            columnName = columnName.Replace(" ", "_");
        //            if (columnName.Equals("REQUESITION_DATE", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                columnName = "REQUESITION_DATE";
        //            }
        //            if (columnName.Equals("ITEM_OF_REQUESITION", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                columnName = "REQUISITION_ITEM";
        //            }
        //        }

        //        columnName = columnName.ToUpper();
        //        if (columnName.ToLower().Contains("date"))
        //        {
        //            dtCsv.Columns.Add(columnName, typeof(DateTime));
        //        }
        //        else
        //        {
        //            dtCsv.Columns.Add(columnName, typeof(String));
        //        }
        //    }
        //    dtCsv.Columns.Add("FILE_NAME", typeof(String));
        //};
    }
}
