﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;

//using PR = NeulandSAPIntegration.Neuland.PR1;
using PR = NeulandSAPIntegration.Neuland.PROD.PR;
using MAT = NeulandSAPIntegration.Neuland.Material1;
//using MAT = NeulandSAPIntegration.Neuland.PROD.Material;
using VD = NeulandSAPIntegration.Neuland.Vendor1;
using GRN = NeulandSAPIntegration.NeulandSAPIntegration.Neuland.GRN;
using PO = NeulandSAPIntegration.Neuland.PO;
//using GRN = TestSol.GRNService;
//using POSCH = TestSol.POScheduleService;
using CURR = NeulandSAPIntegration.Neuland.Currency;

using PRMServices.SQLHelper;
using System.Text.RegularExpressions;

namespace NeulandSAPIntegration
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static bool writeToDB = true;
        private static string hostURL = ConfigurationManager.AppSettings["Host"];
        static void Main(string[] args)
        {
            string jobType = ConfigurationManager.AppSettings["JOB_TYPE"].ToString();
            logger.Info("JOB_TYPE:" + jobType);

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
               jobType.Contains("MAT_JOB"))
            {
                GetMaterailDetails();
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                jobType.Contains("VENDOR_JOB"))
            {
                GetVendorDetails();
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                jobType.Contains("PR_JOB"))
            {
                GetPRDetails();
            }

            if (jobType.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                jobType.Contains("GRN_JOB"))
            {
                GetGRNDetails();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PO_JOB")
            {
                GetPOScheduleDetails();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "CURRENCY_JOB")
            {
                GetCurrencyDetails();
            }
        }

        private static void GetVendorDetails()
        {
            try
            {
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string VendorParams = ConfigurationManager.AppSettings["VendorParams"];
                logger.Info("VendorParams:" + VendorParams);
                string dates = VendorParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = VendorParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";

                logger.Info("VendorParams:" + VendorParams);
                logger.Debug("START GetVendorDetails: " + DateTime.Now.ToString());
                string newURL = hostURL + "zvendor_srv";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                VD.zvendor_srv test = new VD.zvendor_srv();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                VD.ZFM_VENDOR_DATA vENDOR_DATA = new VD.ZFM_VENDOR_DATA();
                List<VD.ZDATE_STR> S_ZDATE_STR = new List<VD.ZDATE_STR>();
                if (string.IsNullOrEmpty(currentIndFlag))
                {
                    S_ZDATE_STR.Add(new VD.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    }); ;
                }
                else
                {
                    vENDOR_DATA.CURENT_IND = currentIndFlag;
                }

                vENDOR_DATA.R_DATE = S_ZDATE_STR.ToArray();
                vENDOR_DATA.T_VENDOR_DATA = new List<VD.ZVENDOR_DATA_STR>().ToArray();
                
                //test.SoapVersion = System.Web.Services.Protocols.SoapProtocolVersion.Default;
                var vendorsResponse = test.ZFM_VENDOR_DATA(vENDOR_DATA);
                var vendors = vendorsResponse.T_VENDOR_DATA;
                logger.Debug("TOTAL COUNT GetVendorDetails: " + (vendors != null ? vendors.Count().ToString() : "0"));
                logger.Debug("END GetVendorDetails: " + DateTime.Now.ToString());
                List<string> columns = @"NAMEV;NAME1;SMTP_ADDR;TELF1;NAME2;WAERS;SMTP_ADDR1;SMTP_ADDR2;SMTP_ADDR3;SMTP_ADDR4;SMTP_ADDR5;SMTP_ADDR6;SMTP_ADDR7;SMTP_ADDR8;SMTP_ADDR9;SMTP_ADDR10;TEL_NUMBER;VEN_ALT_FIR;VEN_ALT_LAS;LIFNR;MATNR;WERKS;STCD3;CITY1;LAND1;ZTERM;TEXT1;ERDAT".Split(';').ToList();

                if (vendors != null && vendors.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = vendors.AsEnumerable().ToList().Chunk(500);
                    logger.Debug("GetVendorDetails GUID: " + guid);
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        foreach (var vendor in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;
                            row["NAMEV"] = $"{vendor.VENDOR_FIRSTNAME} {vendor.VENDOR_LASTNAME}";
                            row["NAME1"] = $"{vendor.VENDOR_LASTNAME}";
                            row["SMTP_ADDR"] = $"{vendor.VENDOR_EMAIL}";
                            row["TELF1"] = $"{vendor.VENDOR_PHONENUMBER}";
                            row["NAME2"] = $"{vendor.VENDOR_FIRSTNAME} {vendor.VENDOR_LASTNAME}"; // $"{vendor.VENDOR_COMPANY_NAME}";
                            row["WAERS"] = $"{vendor.VENDOR_CURRENCY}";
                            row["SMTP_ADDR1"] = $"{vendor.VENDOR_ALT_EMAIL1}";
                            row["SMTP_ADDR2"] = $"{vendor.VENDOR_ALT_EMAIL2}";
                            row["SMTP_ADDR3"] = $"{vendor.VENDOR_ALT_EMAIL3}";
                            row["SMTP_ADDR4"] = $"{vendor.VENDOR_ALT_EMAIL4}";
                            row["SMTP_ADDR5"] = $"{vendor.VENDOR_ALT_EMAIL5}";
                            row["SMTP_ADDR6"] = $"{vendor.VENDOR_ALT_EMAIL6}";
                            row["SMTP_ADDR7"] = $"{vendor.VENDOR_ALT_EMAIL7}";
                            row["SMTP_ADDR8"] = $"{vendor.VENDOR_ALT_EMAIL8}";
                            row["SMTP_ADDR9"] = $"{vendor.VENDOR_ALT_EMAIL9}";
                            row["SMTP_ADDR10"] = $"{vendor.VENDOR_ALT_EMAIL10}";
                            row["TEL_NUMBER"] = $"{vendor.VENDOR_ALT_PHONENO}";
                            row["VEN_ALT_FIR"] = $"{vendor.VENDOR_ALT_POC_FIRSTNAME}";
                            row["VEN_ALT_LAS"] = $"{vendor.VENDOR_ALT_CONTACT_LASTNAME}";
                            row["LIFNR"] = $"{vendor.VENDOR_VENDOR_ERPCODE}";
                            row["MATNR"] = $"{vendor.VENDOR_MATERIAL_CODE}";
                            row["WERKS"] = $"{vendor.PLANT}";
                            row["STCD3"] = $"{vendor.VENDOR_GST_NUMBER}";
                            row["CITY1"] = $"{vendor.VENDOR_LOCATION}";
                            row["LAND1"] = $"{vendor.VENDOR_COUNTRY_DETAILS}";
                            row["ZTERM"] = $"{vendor.PAYMENT_TERMS_CODE}";
                            row["TEXT1"] = $"{vendor.PAYMENT_TERMS_DESC}";
                            row["ERDAT"] = $"{vendor.VENDOR_CREATION_DATE}";
                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_VENDOR_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (vendors != null && vendors.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_vendor_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETVENDORDETAILS ERROR");
            }
        }

        private static void GetMaterailDetails()
        {
            try
            {
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string getMATParams = ConfigurationManager.AppSettings["MATParams"];

                string dates = getMATParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);


                logger.Info("getMATParams:" + getMATParams);
                string materials = getMATParams.Split(';')[1];
                List<string> materialLis = materials.Split(',').ToList();
                
                logger.Debug("START GetMaterailDetails: " + DateTime.Now.ToString());
                string newURL = hostURL + "zmaterial_srv";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                MAT.zmaterial_srv test = new MAT.zmaterial_srv();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);

                List<MAT.ZDATE_STR> S_ZDATE_STR = new List<MAT.ZDATE_STR>();
                MAT.ZMATERIAL_DATA zMATERIAL_DATA = new MAT.ZMATERIAL_DATA();

                if (string.IsNullOrEmpty(currentIndFlag))
                {
                    S_ZDATE_STR.Add(new MAT.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }
                else
                {
                    zMATERIAL_DATA.CURENT_IND = currentIndFlag;
                }

                List<MAT.ZMTART_STR> S_MTART = new List<MAT.ZMTART_STR>();
                foreach (var mat in materialLis)
                {
                    S_MTART.Add(new MAT.ZMTART_STR()
                    {
                        SIGN = "I",
                        OPTION = "EQ",
                        LOW = mat,
                        HIGH = ""
                    });
                }
                
                zMATERIAL_DATA.R_DATE_RANGE = S_ZDATE_STR.ToArray();
                zMATERIAL_DATA.R_MATERIAL_TYPE = S_MTART.ToArray();
                zMATERIAL_DATA.T_MATERIAL_DATA = new List<MAT.ZMATERIAL_DATA_STR>().ToArray();
                var materailsresponse = test.ZMATERIAL_DATA(zMATERIAL_DATA);
                var materails = materailsresponse.T_MATERIAL_DATA;
                logger.Debug($"TOTAL COUNT GetMaterailDetails: {(materails != null ? materails.Count().ToString() : "0")}");
                logger.Debug("END GetMaterailDetails: " + DateTime.Now.ToString());
                List<string> columns = @"MATKL;WGBEZ;MATNR;MTART;STEUC;MAKTX;PO_TEXT;MEINS;CASNR;GPNUM;ASNUM;ASKTX;MEINS1;MATKL1;MEINH;MEINH1;MEINH2;MEINH3;MEINH4;MEINH5;MEINH6;MEINH7;UMREN;UMREN1;UMREN2;UMREN3;UMREN4;UMREN5;UMREN6;UMREN7;UMREZ;UMREZ1;UMREZ2;UMREZ3;UMREZ4;UMREZ5;UMREZ6;UMREZ7;TAXIM;LVORM;MMSTA;MSTAE;ERSDA;LVORM1;ERDAT1;AEDAT1;LAEDA;WERKS".Split(';').ToList();

                if (materails != null && materails.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = materails.AsEnumerable().ToList().Chunk(500);
                    logger.Debug("GetMaterailDetails GUID: " + guid);
                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        foreach (var material in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;
                            row["MATKL"] = $"{material.MATERIAL_GROUP_CODE}";
                            row["WGBEZ"] = $"{material.MATERIAL_GROUP_DESC}";
                            row["MATNR"] = $"{material.MATERIAL_CODE}";
                            row["MTART"] = $"{material.MATERIAL_DESCRIPTION}";
                            row["STEUC"] = $"{material.MATERIAL_HSN_CODE}";
                            row["MAKTX"] = $"{material.MATERIAL_CODE}";
                            row["PO_TEXT"] = $"{material.PO_TEXT}";
                            //row["MEINS"] = $"{material.ALTERNATE_QTY2}";
                            row["CASNR"] = $"{material.CAS_NUMBER}";
                            row["GPNUM"] = $"{material.MFCD_CODE}";
                            row["ASNUM"] = $"{""}";
                            row["ASKTX"] = $"{""}";
                            //row["MEINS1"] = $"{material.ALTERNATE_QTY2}";
                            row["MATKL1"] = $"";
                            row["MEINS"] = $"{material.UOM}";
                            row["MEINH1"] = $"{material.ALTERNATE_UOM1}";
                            row["MEINH2"] = $"{material.ALTERNATE_UOM2}";
                            row["MEINH3"] = $"{material.ALTERNATE_UOM3}";
                            row["MEINH4"] = $"{material.ALTERNATE_UOM4}";
                            row["MEINH5"] = $"{material.ALTERNATE_UOM5}";
                            row["MEINH6"] = $"{material.ALTERNATE_UOM6}";
                            row["MEINH7"] = $"{material.ALTERNATE_UOM7}";
                            row["UMREN"] = $"{0}";
                            row["UMREN1"] = $"{0}";
                            row["UMREN2"] = $"{0}";
                            row["UMREN3"] = $"{0}";
                            row["UMREN4"] = $"{0}";
                            row["UMREN5"] = $"{0}";
                            row["UMREN6"] = $"{0}";
                            row["UMREN7"] = $"{0}";
                            row["UMREZ"] = $"{0}";
                            row["UMREZ1"] = $"{0}";
                            row["UMREZ2"] = $"{0}";
                            row["UMREZ3"] = $"{0}";
                            row["UMREZ4"] = $"{0}";
                            row["UMREZ5"] = $"{0}";
                            row["UMREZ6"] = $"{0}";
                            row["UMREZ7"] = $"{0}";
                            row["TAXIM"] = $"0";
                            row["LVORM"] = $"";
                            row["MMSTA"] = $"";
                            row["MSTAE"] = $"";
                            row["ERSDA"] = $"{material.MATERIAL_CREATED_DATE}";
                            row["LVORM1"] = $"{""}";
                            row["ERDAT1"] = $"{material.MATERIAL_CREATED_DATE}";
                            row["AEDAT1"] = "1970-01-01";
                            row["LAEDA"] = "1970-01-01";
                            row["WERKS"] = $"{material.PLANT}";
                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_MATERIAL_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (materails != null && materails.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_material_details", sd);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETMATERIALDETAILS ERROR");
            }
        }

        private static void GetPRDetails()
        {
            try
            {
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string getPRParams = ConfigurationManager.AppSettings["PRParams"];
                logger.Info("getPRParams:" + getPRParams);
                string dates = getPRParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = getPRParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    plants = "ALL";
                }
                else if (plants.Contains(":"))
                {
                    fromPlant = plants.Split(':')[0];
                    toPlant = plants.Split(':')[1];
                }

                logger.Debug("START GetPRDetails: " + DateTime.Now.ToString());
                string newURL = hostURL + "zpreq_srv";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                PR.ZPREQ_SRV test = new PR.ZPREQ_SRV();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                PR.ZFM_PR_DATA zFM_PR_DATA = new PR.ZFM_PR_DATA();
                List<PR.ZDATE_STR> S_ZDATE_STR = new List<PR.ZDATE_STR>();

                if (string.IsNullOrEmpty(currentIndFlag))
                {
                    S_ZDATE_STR.Add(new PR.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }
                else
                {
                    zFM_PR_DATA.CURENT_IND = currentIndFlag;
                }
                

                zFM_PR_DATA.R_DATE = S_ZDATE_STR.ToArray();
                zFM_PR_DATA.T_PR_DATA = new List<PR.ZPR_DATA_STR>().ToArray();
                var prresponse = test.ZFM_PR_DATA(zFM_PR_DATA);
                var prs = prresponse.T_PR_DATA;
                logger.Debug("TOTAL COUNT GetPRDetails: " + (prs != null ? prs.Count().ToString() : "0"));
                logger.Debug("END GetPRDetails: " + DateTime.Now.ToString());
                List<string> columns = @"PLANT;PLANT_CODE;GMP;PURCHASE_GROUP_CODE;PURCHASE_GROUP_NAME;REQUESITION_DATE;PR_RELEASE_DATE;PR_CHANGE_DATE;PR_NUMBER;PR_CREATOR_NAME;REQUISITIONER_NAME;REQUISITIONER_EMAIL;ITEM_OF_REQUESITION;MATERIAL_GROUP_CODE;MATERIAL_GROUP_DESC;MATERIAL_CODE;MATERIAL_TYPE;MATERIAL_HSN_CODE;MATERIAL_DESCRIPTION;SHORT_TEXT;ITEM_TEXT;UOM;QTY_REQUIRED;MATERIAL_DELIVERY_DATE;LEAD_TIME;PR_TYPE;PR_TYPE_DESC;PR_NOTE;CASNR;WBS_CODE;MFCD_NUMBER;PROJECT_DESCRIPTION;PROFIT_CENTER;ALTERNATE_UOM;ALTERNATE_QTY;SECTION_HEAD;SERVICE_CODE;SERVICE_CODE_DESCRIPTION;SERVICE_QTY;SERVICE_UNIT_OF_MEASURE;ITEM_CATEGORY;ACCOUNT_ASSIGNMENT_CATEGORY;PROJECT_TYPE;SUB_VERTICAL;LOEKZ;ERDAT;EBAKZ".Split(';').ToList();

                if (prs != null && prs.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = prs.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        int processedRows = 0;
                        foreach (var pr in chunk)
                        {
                            var row = dt.NewRow();
                            row["JOB_ID"] = guid1;
                            row["PLANT"] = $"{pr.PLANT}";
                            row["PLANT_CODE"] = $"{pr.PLANT}";
                            row["GMP"] = $"";
                            row["PURCHASE_GROUP_CODE"] = $"{pr.PUR_GROUP}";
                            row["PURCHASE_GROUP_NAME"] = $"{pr.PUR_GROUP}";
                            row["REQUESITION_DATE"] = $"{pr.PREQ_DATE}";
                            row["PR_RELEASE_DATE"] = $"{pr.REL_DATE}";
                            row["PR_CHANGE_DATE"] = $"{pr.CHANGED_ON_DATE}";
                            row["PR_NUMBER"] = $"{pr.PR_NO}";
                            row["PR_CREATOR_NAME"] = $"{pr.PR_CRE_NAME}";
                            row["REQUISITIONER_NAME"] = $"{pr.PREQ_NAME}";
                            row["REQUISITIONER_EMAIL"] = $"{pr.E_MAIL}";
                            row["ITEM_OF_REQUESITION"] = $"{pr.ITEM_OF_PR}";
                            row["MATERIAL_GROUP_CODE"] = $"{pr.MAT_GRP_CODE}";
                            row["MATERIAL_GROUP_DESC"] = $"{pr.MAT_GRP_DESC}";
                            row["MATERIAL_CODE"] = $"{pr.MATERIAL}";
                            row["MATERIAL_TYPE"] = $"{pr.MAT_TYPE}";
                            row["MATERIAL_HSN_CODE"] = $"{pr.HSN_CODE}";
                            row["MATERIAL_DESCRIPTION"] = $"{pr.MAT_DESC}";
                            row["SHORT_TEXT"] = $"{pr.SHORT_TEXT}";
                            row["ITEM_TEXT"] = $"{pr.ITEM_TEXT}";
                            row["UOM"] = $"{pr.UOM}";
                            row["QTY_REQUIRED"] = $"{pr.QTY_REQD}";
                            row["MATERIAL_DELIVERY_DATE"] = $"{pr.MAT_DEL_DATE}";
                            row["LEAD_TIME"] = $"{""}";
                            row["PR_TYPE"] = $"{pr.PR_TYPE}";
                            row["PR_TYPE_DESC"] = $"{pr.PR_TYPE_DESC}";
                            row["PR_NOTE"] = $"{pr.PR_NOTE}";
                            row["CASNR"] = $"{""}";
                            row["WBS_CODE"] = $"{""}";
                            row["MFCD_NUMBER"] = $"{""}";
                            row["PROJECT_DESCRIPTION"] = $"{pr.PROJECT_DESCRIPTION}";
                            row["PROFIT_CENTER"] = $"{pr.PROFIT_CENTER}";
                            row["ALTERNATE_UOM"] = $"{pr.ALTERNATE_UOM}";
                            row["ALTERNATE_QTY"] = $"{pr.ALTERNATE_QTY}";
                            row["SECTION_HEAD"] = $"{pr.SERVICE_CODE}";
                            row["SERVICE_CODE"] = $"{pr.SERVICE_CODE}";
                            row["SERVICE_CODE_DESCRIPTION"] = $"{pr.SERVICE_CODE_DESC}";
                            row["SERVICE_QTY"] = $"{pr.SERVICE_QTY}";
                            row["SERVICE_UNIT_OF_MEASURE"] = $"{pr.SERVICE_UOM}";
                            row["ITEM_CATEGORY"] = $"{pr.ITEM_CATEGORY}";
                            row["ACCOUNT_ASSIGNMENT_CATEGORY"] = $"{pr.ACC_ASSIGNMENT_CAT}";
                            row["PROJECT_TYPE"] = $"{pr.PROJECT_TYPE}";
                            row["SUB_VERTICAL"] = $"{""}";
                            row["LOEKZ"] = $"{""}";
                            row["ERDAT"] = $"{pr.CHANGED_ON_DATE}";
                            row["EBAKZ"] = $"{pr.PR_STATUS}";
                            row["DATE_CREATED"] = DateTime.UtcNow;
                            dt.Rows.Add(row);
                            processedRows++;
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_PR_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (prs != null && prs.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_pr_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETPRDETAILS ERROR");
            }
        }

        private static void GetGRNDetails()
        {
            try
            {
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string getGRNParams = ConfigurationManager.AppSettings["GRNParams"];
                logger.Info("getGRNParams:" + getGRNParams);
                string dates = getGRNParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = getGRNParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    plants = "ALL";
                }
                else if (plants.Contains(":"))
                {
                    fromPlant = plants.Split(':')[0];
                    toPlant = plants.Split(':')[1];
                }

                logger.Debug("START GetGRNDetails: " + DateTime.Now.ToString());
                string newURL = hostURL + "zgrn_srv";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                GRN.zgrn_srv test = new GRN.zgrn_srv();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                GRN.ZFM_GRN_DATA zFM_GRN_DATA = new GRN.ZFM_GRN_DATA();
                List<GRN.ZBSART_STR> ZBSART_STR = new List<GRN.ZBSART_STR>();

                if (string.IsNullOrEmpty(currentIndFlag))
                {
                    ZBSART_STR.Add(new GRN.ZBSART_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }
                else
                {
                    zFM_GRN_DATA.CURENT_IND = currentIndFlag;
                }

                //zFM_GRN_DATA.R_BLDAT = ZBSART_STR.ToArray();
                zFM_GRN_DATA.R_BUDAT = ZBSART_STR.ToArray();
                zFM_GRN_DATA.T_GRN_DATA = new List<GRN.ZGRN_DATA_STR>().ToArray();
                var grnresponse = test.ZFM_GRN_DATA(zFM_GRN_DATA);
                var grns = grnresponse.T_GRN_DATA;
                logger.Debug("TOTAL COUNT GetGRNDetails: " + (grns != null ? grns.Count().ToString() : "0"));
                logger.Debug("END GetGRNDetails: " + DateTime.Now.ToString());
                List<string> columns = @"BUDAT;ZZSBILL_ENTRY;ZZBILL_ENTRY;ZZDATE_SBILL_ENTRY;SGTXT;WAERS;BLDAT;VFDAT;WRBTR;WRBTRSpecified;ZZGATE_ENT_DATE;ZZGATE_ENTRY;CHARG;LFBNR;BUDAT1;ZEILE;MBLNR;MJAHR;MENGE;MENGESpecified;WRBTR1;ZZBILL_DOC;ZZLR_DATE;ZZLR_NUMBER;ZZLUT_NUM;HSDAT;MAKTX;MATNR;BWART;WERKS;MENGE1;RDATE;SRVPOS;LFBNR1;LFPOS;STATUS;LGORT;MEINS;CHARG1;LIFNR;NAME1;LAND1;PS_PSP_PNR;EBELN;EBELP;CPUDT_MKPF;CPUTM_MKPF".Split(';').ToList();

                if (grns != null && grns.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = grns.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        int processedRows = 0;
                        foreach (var pr in chunk)
                        {
                            var newRow = dt.NewRow();
                            newRow["JOB_ID"] = guid1;
                            newRow["BUDAT"] = pr.ACUTAL_DELIVERY_DATE == "0000-00-00" ? null : pr.ACUTAL_DELIVERY_DATE;
                            newRow["WAERS"] = pr.CURRENCY;
                            newRow["BLDAT"] = pr.DOCUMENT_DT;
                            newRow["VFDAT"] = pr.EXPIRY_DATE == "0000-00-00" ? null : pr.EXPIRY_DATE; ;
                            newRow["WRBTR"] = pr.FRIEGHT_VALUE;
                            newRow["CHARG"] = pr.GR_BATCH_NO;
                            newRow["LFBNR"] = pr.GR_REFERENCE;
                            newRow["BUDAT1"] = pr.GRN_DT;
                            newRow["ZEILE"] = pr.GRN_LINE_ITEM;
                            newRow["MBLNR"] = pr.GRN_NO;
                            newRow["MJAHR"] = pr.GR_YEAR;
                            newRow["MENGE"] = pr.GRN_QTY;
                            newRow["WRBTR1"] = pr.GRN_VALUE;
                            newRow["HSDAT"] = pr.MANUFACTURING_DATE == "0000-00-00" ? null : pr.MANUFACTURING_DATE;
                            newRow["MAKTX"] = pr.MAT_DESCRIPTION;
                            newRow["MATNR"] = pr.MATERIAL;
                            newRow["BWART"] = pr.MV_TYPE;
                            newRow["WERKS"] = pr.PLANT;
                            newRow["MENGE1"] = pr.PO_QUANTITY;
                            newRow["RDATE"] = pr.RETEST_DATE == "0000-00-00" ? null : pr.RETEST_DATE;
                            newRow["SRVPOS"] = pr.SERVICE_CODE;
                            newRow["LFBNR1"] = pr.SERVICE_ENTRY_SHEET;
                            newRow["LFPOS"] = pr.SERVICE_LN_ITM;
                            newRow["STATUS"] = pr.STATUS;
                            newRow["LGORT"] = pr.STORAGE_LOCATION;
                            newRow["MEINS"] = pr.UOM;
                            newRow["CHARG1"] = pr.VENDOR_BATCH_NO;
                            newRow["LIFNR"] = pr.VENDOR_CODE;
                            newRow["NAME1"] = pr.VENDOR_NAME;
                            newRow["LAND1"] = pr.VENDORS_COUNTRY;
                            newRow["PS_PSP_PNR"] = "";
                            newRow["EBELN"] = pr.PO_NUMBER;
                            newRow["EBELP"] = pr.PO_LINE_ITEM;
                            newRow["CPUDT_MKPF"] = pr.GR_CREATION_DATE == "0000-00-00" ? null : pr.GR_CREATION_DATE;
                            newRow["CPUTM_MKPF"] = "";
                            newRow["DATE_CREATED"] = DateTime.UtcNow;

                            dt.Rows.Add(newRow);
                            processedRows++;
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_GRN_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (grns != null && grns.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_grn_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETGRNDETAILS ERROR");
            }
        }

        private static void GetPOScheduleDetails()
        {
            try
            {
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string getPOSCHParams = ConfigurationManager.AppSettings["POParams"];
                logger.Info("getPOSCHParams:" + getPOSCHParams);
                string dates = getPOSCHParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                string plants = getPOSCHParams.Split(';')[1];
                string fromPlant = "";
                string toPlant = "";
                if (plants.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    plants = "ALL";
                }
                else if (plants.Contains(":"))
                {
                    fromPlant = plants.Split(':')[0];
                    toPlant = plants.Split(':')[1];
                }

                logger.Debug("START GetPOScheduleDetails: " + DateTime.Now.ToString());
                string newURL = hostURL + "zpo_srv";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                PO.zpo_srv test = new PO.zpo_srv();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                PO.ZFM_PO_DATA zFM_PO_DATA = new PO.ZFM_PO_DATA();
                List<PO.ZDATE_STR> ZDATE_STR = new List<PO.ZDATE_STR>();
                if (string.IsNullOrEmpty(currentIndFlag))
                {
                    ZDATE_STR.Add(new PO.ZDATE_STR()
                    {
                        SIGN = "I",
                        OPTION = "BT",
                        LOW = fromDate,
                        HIGH = toDate
                    });
                }
                else
                {
                    zFM_PO_DATA.CURENT_IND = currentIndFlag;
                }

                zFM_PO_DATA.R_BUKRS = new List<PO.ZBUKRS_STR>().ToArray();
                zFM_PO_DATA.R_EBELN = new List<PO.ZEBELN_STR>().ToArray();
                zFM_PO_DATA.R_ERDAT = ZDATE_STR.ToArray();
                zFM_PO_DATA.R_MATNR = new List<PO.ZMATERIAL_STR>().ToArray();
                zFM_PO_DATA.R_MTART = new List<PO.ZMTART_STR>().ToArray();
                zFM_PO_DATA.R_WERKS = new List<PO.ZWERKS_STR>().ToArray();
                zFM_PO_DATA.T_PO_DATA = new List<PO.ZPO_DATA_STR>().ToArray();

                var poresponse = test.ZFM_PO_DATA(zFM_PO_DATA);
                var POs = poresponse.T_PO_DATA;
                logger.Debug("TOTAL COUNT GetPOScheduleDetails: " + (POs != null ? POs.Count().ToString() : "0"));
                logger.Debug("END GetGRNDetails: " + DateTime.Now.ToString());
                List<string> columns = @"WERKS;MATNR;TXZ01;MATKL;LIFNR;NAME1;TELF1;SMTP_ADDR;EBELN;EBELP;AEDAT;ERNAM;ZTERM;MWSKZ;TEXT1;UDATE;EINDT;MTART;ORT01;REGIO;J_1BNBM;ORD_QTY;MEINS;MEINS1;MENGE1;MENGE1Specified;EFFWR;EFFWRSpecified;NETWR1;NETWR1Specified;WAERS;KNUMV;KNUMVSpecified;MENGE2;MENGE2Specified;BANFN;BNFPO;MENGE3;MENGE3Specified;UDATE1;LTEXT1;LFDAT;LTEXT2;FRGKZ;AFNAM;BSART;SRVPOS;ASKTX;KNUMV1;KNUMV1Specified;KNUMV2;KNUMV2Specified;BSTYP;KDATB;KDATE;AEDAT1;ELIKZ;LOEKZ;CGST;SGST;IGST;CESS;TCS".Split(';').ToList();

                if (POs != null && POs.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = POs.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        int processedRows = 0;
                        foreach (var pr in chunk)
                        {
                            var newRow = dt.NewRow();
                            newRow["JOB_ID"] = guid1;

                            newRow["WERKS"] = pr.PLANT;
                            newRow["MATNR"] = pr.MATERIAL;
                            newRow["TXZ01"] = pr.DESCRIPTION;
                            newRow["MATKL"] = pr.MAT_GROUP;
                            newRow["LIFNR"] = pr.VENDOR_CODE; //GetDataRowVaue(row, "VENDOR");
                            newRow["NAME1"] = pr.VENDOR_NAME;
                            newRow["TELF1"] = pr.VENDOR_PRIMARY_PHNO;
                            newRow["SMTP_ADDR"] = pr.VENDOR_PRIMARY_EMAIL;
                            newRow["EBELN"] = pr.PO_NUMBER;
                            newRow["EBELP"] = pr.PO_LINE_ITEM;
                            newRow["AEDAT"] = pr.PO_DATE == "0000-00-00" ? null : pr.PO_DATE;
                            newRow["ERNAM"] = pr.PO_CREATOR;
                            newRow["ZTERM"] = pr.PAYMENT_TERMS;
                            newRow["MWSKZ"] = pr.TAX_CODE;
                            newRow["TEXT1"] = pr.TAX_CODE_DESC;
                            newRow["UDATE"] = pr.PO_RELEASE_DATE == "0000-00-00" ? null : pr.PO_RELEASE_DATE;
                            newRow["EINDT"] = pr.DELV_DATE == "0000-00-00" ? null : pr.DELV_DATE;
                            newRow["MTART"] = pr.MAT_TYPE;
                            newRow["ORT01"] = pr.CITY;
                            newRow["REGIO"] = pr.REGION_DESC;
                            newRow["J_1BNBM"] = pr.HSN_CODE;
                            newRow["ORD_QTY"] = pr.ORD_QTY;
                            newRow["MEINS"] = pr.UOM;
                            newRow["MEINS1"] = pr.ALTERNATIVE_UOM;
                            newRow["MENGE1"] = pr.QTY_ALTERNATE_UOM;
                            newRow["EFFWR"] = pr.NET_PRICE;
                            newRow["NETWR1"] = pr.VALUE_INR;
                            newRow["WAERS"] = pr.CURRENCY;
                            newRow["KNUMV"] = pr.FREIGHT == "" ? null : pr.FREIGHT;
                            newRow["MENGE2"] = pr.PEND_QTY;
                            newRow["BANFN"] = pr.PR_LINE;
                            newRow["BNFPO"] = pr.PR_NUM;
                            newRow["MENGE3"] = pr.PR_QTY;
                            newRow["UDATE1"] = pr.PR_RELEASE_DATE == "0000-00-00" ? null : pr.PR_RELEASE_DATE;
                            newRow["LTEXT1"] = pr.PR_LINE_TEXT;
                            newRow["LFDAT"] = pr.PR_DELV_DATE == "0000-00-00" ? null : pr.PR_DELV_DATE;
                            newRow["LTEXT2"] = pr.ITEM_TEXT_PO;
                            newRow["FRGKZ"] = pr.REL_IND;
                            newRow["AFNAM"] = pr.PR_REQUISITIONER;
                            newRow["BSART"] = pr.DOC_TYPE;
                            newRow["SRVPOS"] = pr.SERVICE_CODE;
                            newRow["ASKTX"] = pr.SERVICE_DESCRIPTION;
                            newRow["KNUMV1"] = pr.MISC_CHARGES == "" ? null : pr.MISC_CHARGES;
                            newRow["KNUMV2"] = pr.PACKING_CHARGES == "" ? null : pr.PACKING_CHARGES;
                            newRow["BSTYP"] = pr.PO_CONTRACT;
                            newRow["KDATB"] = pr.VALID_FROM == "0000-00-00" ? null : pr.VALID_FROM;
                            newRow["KDATE"] = pr.VALID_TO == "0000-00-00" ? null : pr.VALID_TO;
                            newRow["ELIKZ"] = pr.DELIVERY_COMPLETION_IND;
                            newRow["LOEKZ"] = pr.DELETED_IND;
                            newRow["CGST"] = pr.CGST;
                            newRow["SGST"] = pr.SGST;
                            newRow["IGST"] = pr.IGST;
                            newRow["CESS"] = pr.CESS;
                            newRow["TCS"] = pr.TCS;
                            newRow["DATE_CREATED"] = DateTime.UtcNow;

                            dt.Rows.Add(newRow);
                            processedRows++;
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_PO_SCHEDULE_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (POs != null && POs.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_po_schedule_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETPOSCHDETAILS ERROR");
            }
        }

        private static void GetCurrencyDetails()
        {
            try
            {
                List<CURR.ZEXCH_RATE> rates = new List<CURR.ZEXCH_RATE>();
                string currentIndFlag = ConfigurationManager.AppSettings["IND_FLAG"].ToString();
                string getCurrencyParams = ConfigurationManager.AppSettings["CurrencyParams"];
                logger.Info("getCurrencyParams:" + getCurrencyParams);
                string dates = getCurrencyParams.Split(';')[0];
                string fromDate = dates.Split(':')[0];
                string toDate = dates.Split(':')[1];
                logger.Info("fromDate:" + fromDate);
                logger.Info("toDate:" + toDate);
                logger.Debug("START GetCurrencyDetails: " + DateTime.Now.ToString());
                string newURL = "http://NLLCOSAPDEV01.NEULANDLABS.COM/sap/bc/srt/wsdl/flv_10002p111ad1/sdef_url/zsrv_erate";// hostURL + "zexchange_rate_350";
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(newURL);
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                CURR.ZSRV_ERATE test = new CURR.ZSRV_ERATE();
                test.Url = newURL;
                test.Credentials = credentials;
                test.PreAuthenticate = true;
                test.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOut"]);
                CURR.ZEXCHANGE_RATE ZEXCHANGE_RATE_DATA = new CURR.ZEXCHANGE_RATE();
                ZEXCHANGE_RATE_DATA.IT_TCURR = rates.ToArray();
                var poresponse = test.ZEXCHANGE_RATE(ZEXCHANGE_RATE_DATA);
                var history = poresponse.IT_HISTORY;
                var current = poresponse.IT_TCURR;
                logger.Debug("TOTAL COUNT GetCurrencyDetails: " + (current != null ? current.Count().ToString() : "0"));
                logger.Debug("END GetCurrencyDetails: " + DateTime.Now.ToString());
                List<string> columns = @"KURST;FCURR;TCURR;GDATU;UKURS".Split(';').ToList();

                if (current != null && current.Length > 0 && writeToDB)
                {
                    DataTable dt = new DataTable();
                    dt.Clear();
                    dt.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                    foreach (var column in columns)
                    {
                        dt.Columns.Add(column);
                    }

                    dt.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }

                    MSSQLBizClass bizClass = new MSSQLBizClass();
                    var guid1 = Guid.NewGuid();
                    string guid = guid1.ToString();
                    int count = 1;
                    var totalChunks = current.AsEnumerable().ToList().Chunk(500);
                    foreach (var chunk in totalChunks)
                    {
                        dt.Rows.Clear();
                        logger.Debug($"Reading :  Chunk: {count}");
                        int processedRows = 0;
                        foreach (var pr in chunk)
                        {
                            var newRow = dt.NewRow();
                            newRow["JOB_ID"] = guid1;
                            newRow["KURST"] = pr.KURST;
                            newRow["FCURR"] = pr.FCURR;
                            newRow["TCURR"] = pr.TCURR;
                            newRow["GDATU"] = pr.GDATU;
                            newRow["UKURS"] = pr.UKURS;
                            newRow["DATE_CREATED"] = DateTime.UtcNow;

                            dt.Rows.Add(newRow);
                            processedRows++;
                        }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bizClass.BulkInsert(dt, "[dbo].[SAP_CURRENCY_DETAILS]", columnMappings);
                        }

                        dt.Rows.Clear();
                        count++;
                    }

                    if (current != null && current.Length > 0)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_JOB_ID", guid);
                        bizClass.SelectList("erp_process_sap_currency_details", sd);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace, "GETPOSCHDETAILS ERROR");
            }
        }
    }

    public static class ObjExtensions
    {
        public static Object GetPropValue(this Object obj, String name)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        public static T GetPropValue<T>(this Object obj, String name)
        {
            Object retval = GetPropValue(obj, name);
            if (retval == null) { return default(T); }

            // throws InvalidCastException if types are incompatible
            return (T)retval;
        }

    }
}
