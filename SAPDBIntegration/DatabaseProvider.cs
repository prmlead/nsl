﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PRMServices.SQLHelper
{
    public static class DatabaseProvider
    {
        public static IDatabaseHelper GetDatabaseProvider()
        {
            string database = ConfigurationManager.AppSettings["INTEGRATION_CLIENT_DB"].ToString();
            if(database.Equals("MSSQL" , StringComparison.InvariantCultureIgnoreCase))
            {
                return new MSSQLBizClass();
            }
            else if (database.Equals("ORACLE", StringComparison.InvariantCultureIgnoreCase))
            {
                return new OracleBizClass();
            }
            else
            {
                return new MySQLBizClass();
            }
        }
    }
}