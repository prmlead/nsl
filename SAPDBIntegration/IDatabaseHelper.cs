﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace PRMServices.SQLHelper
{
    public interface IDatabaseHelper
    {
        DataSet SelectList(String SP_NAME, SortedDictionary<object, object> sd, int timeout = 0);
        DataTable SelectQuery(String query);
        DataSet ExecuteQuery(String query);
        int ExecuteNonQuery_IUD(String Query);
    }
}