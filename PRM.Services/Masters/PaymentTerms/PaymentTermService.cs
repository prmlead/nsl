﻿using Dapper;
using PRM.Core.Domain.Acl;
using PRM.Core.Domain.Masters.GeneralTerms;
using PRM.Core.Pagers;
using PRM.Data;
using PRM.Services.Acl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Services.Masters.PaymentTerms
{
  public  class PaymentTermService : IPaymentTermService
    {

        public PaymentTermService(IRepository<PaymentTerm> paymentTermRep,
          IDbProvider dbProvider, IAclMappingService aclMappingService)
        {
            _paymentTermRep = paymentTermRep;
            _dbProvider = dbProvider;
            _aclMappingService = aclMappingService;

        }

        private readonly IRepository<PaymentTerm> _paymentTermRep;
        private readonly IAclMappingService _aclMappingService;
        private readonly IDbProvider _dbProvider;
        public void DeletePaymentTerm(PaymentTerm entity)
        {
            _paymentTermRep.Delete(entity);
        }

        public PaymentTerm GetPaymentTermById(int id)
        {

            var entity = _paymentTermRep.GetById(id);

            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "GeneralTerm", EntityId = entity.Id });
            if (entity.LimitedToDepartment)
                entity.AllowedDepartments = mapper.Where(e => e.Type == (int)AclMappingType.Department).Select(e => e.Value).ToArray();

            if (entity.LimitedToProject)
                entity.AllowedProjects = mapper.Where(e => e.Type == (int)AclMappingType.Project).Select(e => e.Value).ToArray();

            if (entity.LimitedToUser)
                entity.AllowedUsers = mapper.Where(e => e.Type == (int)AclMappingType.User).Select(e => e.Value).ToArray();

            return entity;
        }

        public IPagedList<PaymentTerm> GetPaymentTermList(Pager pager,int companyId)
        {
            using (var con = _dbProvider.Connection())
            {
                var query = "SELECT SQL_CALC_FOUND_ROWS  * FROM master_paymentterm";

                List<String> WhereColumns = new List<string>();

                WhereColumns.Add(string.Format(" Company_Id = {0} ", companyId));
                WhereColumns.Add(string.Format(" Deleted = {0}", 0));

                if (!string.IsNullOrEmpty(pager.Criteria.SearchTerm))
                {
                    WhereColumns.Add(string.Format(" Tttle Like %{0}%", pager.Criteria.SearchTerm));
                    WhereColumns.Add(string.Format(" Terms Like %{0}%", pager.Criteria.SearchTerm));


                }

                if (pager.Criteria.Filter.Any())
                {

                }

                if (WhereColumns.Any())
                    query = query + string.Format(" WHERE {0}", string.Join(" AND ", WhereColumns));

                query = query + " ORDER BY CreatedOnUtc DESC ";

                if (pager.PageIndex >= 0 && pager.PageSize > 0)
                {
                    query = query + string.Format(" LIMIT {0},{1} ", pager.PageIndex * pager.PageSize, pager.PageSize);
                }

                query += " ; SELECT FOUND_ROWS()";

                var multi = con.QueryMultiple(query);

                var lst = multi.Read<PaymentTerm>().ToList();
                int total = multi.Read<int>().SingleOrDefault();

                return new PagedList<PaymentTerm>(lst, pager.PageIndex, pager.PageSize, total);
            }
        }

        public IList<PaymentTerm> GetPaymentTermList(object filter)
        {
            return _paymentTermRep.GetList(filter).ToList();
        }

        public void InsertPaymentTerm(PaymentTerm entity)
        {
            var id = _paymentTermRep.Insert(entity);


            entity.Id = id;
            InsertMapping(entity);
        }

        public void UpdatePaymentTerm(PaymentTerm entity)
        {
            _paymentTermRep.Update(entity);

            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "GeneralTerm", EntityId = entity.Id });

            foreach (var map in mapper)
                _aclMappingService.DeleteAclMapping(map);

            InsertMapping(entity);
        }

        private void InsertMapping(PaymentTerm entity)
        {
            if (entity.LimitedToDepartment)
            {
                _aclMappingService.InsertDepartmentMapping(entity.Company_Id, "PaymentTerm", entity.Id, entity.AllowedDepartments);
            }

            if (entity.LimitedToProject)
            {
                _aclMappingService.InsertProjectMapping(entity.Company_Id, "PaymentTerm", entity.Id, entity.AllowedProjects);

            }

            if (entity.LimitedToUser)
            {
                _aclMappingService.InsertUserMapping(entity.Company_Id, "PaymentTerm", entity.Id, entity.AllowedUsers);

            }
        }

    }
}
