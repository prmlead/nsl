﻿using PRM.Services.Vendors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRM.Core.Domain.Vendors;
using PRM.Data;
using PRM.Services.Configurations;
using Newtonsoft.Json;
using Dapper;

namespace PRM.Services.Vendors
{
    public class VendorService : IVendorService
    {
        public VendorService(IRepository<Vendor> vendorRep,
            IRegistrationFormService registrationFormService, IDbProvider dbProvider)
        {
            _vendorRep = vendorRep;
            _registrationFormService = registrationFormService;
            _dbProvider = dbProvider;
        }

        private readonly IRepository<Vendor> _vendorRep;
        private readonly IRegistrationFormService _registrationFormService;
        private readonly IDbProvider _dbProvider;

        public void DeleteVendor(Vendor entity)
        {
            _vendorRep.Delete(entity);
        }

        public Vendor GetVendorById(int Id)
        {
            return _vendorRep.GetById(Id);
        }

        public IList<Vendor> GetVendorList(object filter)
        {
            return _vendorRep.GetList(filter).ToList();
        }

        public void InsertVendor(Vendor entity)
        {
           entity.Id = _vendorRep.Insert(entity);
        }

        public void UpdateVendor(Vendor entity)
        {
            _vendorRep.Update(entity);
        }

        public int CalculateScore(int companyId, int vendorId)
        {
            var vendor = this.GetVendorList(new { U_ID = vendorId }).FirstOrDefault();
            var score = 0;
            if (vendor == null)
                return score;

            if (string.IsNullOrEmpty(vendor.AdditionalInfo))
                return score;


            vendor.AdditionalData = JsonConvert.DeserializeObject<Dictionary<string, object>>(vendor.AdditionalInfo);

            var registraion = _registrationFormService.GetRegistrationForm(companyId);

            if (registraion.Any())
            {
                var hasFilled = registraion.Where(e => vendor.AdditionalData.Keys.Contains(e.Name));

                if (hasFilled.Any())
                {
                    foreach (var item in hasFilled)
                    {
                        score = score + item.Score;
                    }
                }
            }


            return score;


        }

        public IList<Dictionary<string, object>> GetVendorScore(int companyId, int vendorId)
        {
            var vendor = this.GetVendorList(new { U_ID = vendorId }).FirstOrDefault();
            List<Dictionary<string, object>> dir = new List<Dictionary<string, object>>();
            if (vendor != null)
            {
                vendor.AdditionalData = JsonConvert.DeserializeObject<Dictionary<string, object>>(vendor.AdditionalInfo);
                var registraion = _registrationFormService.GetRegistrationForm(companyId);
                object ScoreObject;

                vendor.AdditionalData.TryGetValue("Score",out ScoreObject);

                var scores = JsonConvert.DeserializeObject<IList<VendorScore>>(JsonConvert.SerializeObject(ScoreObject));


                if (registraion.Any())
                {
                    var hasFilled = registraion.Where(e => vendor.AdditionalData.Keys.Contains(e.Name));


                    if (hasFilled.Any())
                    {
                        foreach (var item in hasFilled)
                        {
                            var dir2 = new Dictionary<string, object>();

                            if (ScoreObject != null)
                            {

                                dir2.Add("Value", scores.Where(e => e.Field.Equals(item.Name, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault().Value);
                            }
                            else
                            {
                                dir2.Add("Value", item.Score);
                            }
                            dir2.Add("DefaultValue", item.Score);
                            dir2.Add("Field", item.Name);
                            dir.Add(dir2);


                        }
                    }
                }


            }

            return dir;
        }


        public void SaveVendorScore(int vendorId, List<VendorScore> scores)
        {
            var vendor = this.GetVendorList(new { U_ID = vendorId }).FirstOrDefault();

            if(vendor != null)
            {
                vendor.AdditionalData = JsonConvert.DeserializeObject<Dictionary<string, object>>(vendor.AdditionalInfo);
                vendor.AdditionalData["Score"] = scores;
                vendor.AdditionalInfo = JsonConvert.SerializeObject(vendor.AdditionalData);

                _vendorRep.Update(vendor);

            }
        }


        public void UpdateVedorCategory(int vendorId, string categoryName)
        {

            using (var con = _dbProvider.Connection())
            using (var trans = con.BeginTransaction())
            {
                try
                {
                    con.Query("UPDATE userdata SET V_CATEGORY = @categoryName WHERE U_ID = @vendorId", new { vendorId, categoryName }, trans);
                    trans.Commit();

                }
                catch (Exception ex)
                {
                    trans.Rollback();

                }

            }


        }

        public int[] GetVendorSubCategort(int vendorId)
        {
            using (var con = _dbProvider.Connection())
            {
                try
                {
                    var list = con.Query<int>("SELECT CAT_ID FROM usercategories WHERE U_ID = @vendorId", new { vendorId });

                    return list.ToArray();
                }
                catch(Exception ex)
                {
                    return null;
                }
            }
        }

        public void UpdateVedorSubCategory(int vendorId,int companyId, int[] subCategories)
        {



            using (var con = _dbProvider.Connection())
            using (var trans = con.BeginTransaction())
            {
              try {
                    if (subCategories.Any())
                    {
                        con.Query("DELETE FROM  usercategories WHERE U_ID = @vendorId", new { vendorId }, trans);

                        foreach (var cat in subCategories)
                        {
                            con.Query("INSERT INTO usercategories (U_ID,CAT_ID) VALUES (@vendorId,@categoryId)", new { vendorId, categoryId = cat, companyId }, trans);

                        }
                        trans.Commit();

                    }

                }

                catch (Exception ex)
                {
                    trans.Rollback();

                }
            }



        }
    }
}
