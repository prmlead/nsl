﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRM.Core.Domain.RealTimePrices;
using PRM.Data;
using PRM.Services.Configurations;
using Dapper;
using PRM.Core.Pagers;
using PRM.Core.Domain.Companies;

namespace PRM.Services.RealTimePrices
{
    public class RealTimePriceService : IRealTimePriceService
    {
        public RealTimePriceService(IRepository<RealTimePrice> realtimePriceService,
            IRepository<RealTimePriceSetting> realSettingRep,
           IDbProvider dbProvider)
        {
            _realtimePriceRep = realtimePriceService;
            _dbProvider = dbProvider;
            _realSettingRep = realSettingRep;
        }

        private readonly IRepository<RealTimePrice> _realtimePriceRep;
        private readonly IRepository<RealTimePriceSetting> _realSettingRep;
        private readonly IDbProvider _dbProvider;

        public RealTimePrice GetPriceById(int id)
        {
            return _realtimePriceRep.GetById(id);
        }

        public IList<RealTimePrice> GetPriceList(string categoryName)
        {
            return _realtimePriceRep.GetList(new { Category = categoryName }).ToList();
        }

        public IList<RealTimePrice> GetPriceList(object filters)
        {
            return _realtimePriceRep.GetList(filters).ToList();
        }

        public void InsertPrice(RealTimePrice entity)
        {
            entity.Id = _realtimePriceRep.Insert(entity);
        }

        public void UpdatePrice(RealTimePrice entity)
        {
            _realtimePriceRep.Update(entity);
        }

        public string[] GetCategories(int typeId)
        {
            using (var con = _dbProvider.Connection())
            {
               return con.Query<string>("SELECT Category From realtimeprice  WHERE TypeId = @TypeId GROUP BY Category ORDER BY Category",new { TypeId = typeId }).ToArray();
            }
        }

        public string[] GetMarkets(int typeId)
        {
            using (var con = _dbProvider.Connection())
            {
                return con.Query<string>("SELECT Market From realtimeprice WHERE TypeId = @TypeId GROUP BY Market ORDER BY Market", new { TypeId = typeId }).ToArray();
            }
        }

        public string[] GetProducts(int typeId)
        {
            using (var con = _dbProvider.Connection())
            {
                return con.Query<string>("SELECT Product From realtimeprice WHERE TypeId = @TypeId GROUP BY Product ORDER BY Product", new { TypeId = typeId }).ToArray();
            }
        }

        public string[] GetProducts()
        {
            using (var con = _dbProvider.Connection())
            {
                return con.Query<string>("SELECT Product From realtimeprice GROUP BY Product ORDER BY Product").ToArray();
            }
        }

        public IPagedList<RealTimePrice> GetPriceList(Pager pager)
        {

            using (var con = _dbProvider.Connection())
            {
                var query = "SELECT * FROM realtimeprice";

                if (pager.Criteria.Filter.Any())
                {
                    List<String> WhereColumns = new List<string>();

                    var category = pager.Criteria.Filter.Where(e => e.Column == "Category").FirstOrDefault();
                    if (category != null)
                        WhereColumns.Add(string.Format(" Category = '{0}' ", category.Value));

                    var market = pager.Criteria.Filter.Where(e => e.Column == "Market").FirstOrDefault();
                    if (market != null)
                        WhereColumns.Add(string.Format(" Market = '{0}' ", market.Value));

                    var product = pager.Criteria.Filter.Where(e => e.Column == "Product").FirstOrDefault();
                    if (product != null)
                        WhereColumns.Add(string.Format(" Product = '{0}' ", product.Value));

                    var TypeId = pager.Criteria.Filter.Where(e => e.Column == "TypeId").FirstOrDefault();
                    if (TypeId != null)
                        WhereColumns.Add(string.Format(" TypeId = '{0}' ", TypeId.Value));


                    var fromDate = pager.Criteria.Filter.Where(e => e.Column == "FromDate").FirstOrDefault();
                    var toDate = pager.Criteria.Filter.Where(e => e.Column == "ToDate").FirstOrDefault();

                    if(fromDate != null && toDate != null)
                    {
                        WhereColumns.Add(string.Format(" ( PriceDate BETWEEN '{0}' AND '{1}' ) ", DateTime.Parse(fromDate.Value.ToString()).ToString("yyyy-MM-dd 00:00:00"), DateTime.Parse(toDate.Value.ToString()).ToString("yyyy-MM-dd 23:59:59")));
                    }

                    if (WhereColumns.Any())
                        query = query + string.Format(" WHERE {0}", string.Join(" AND ", WhereColumns));
                }


                query = query + " ORDER BY PriceDate DESC ";

                if (pager.PageIndex >= 0 && pager.PageSize > 0)
                {
                    query = query + string.Format(" OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY", pager.PageIndex * pager.PageSize, pager.PageSize);
                }

                query += ";";
                var multi = con.QueryMultiple(query);
                
                var lst = multi.Read<RealTimePrice>().ToList();
                var multi1 = con.QueryMultiple("SELECT COUNT(1) AS TOTAL_COUNT FROM realtimeprice;");
                int total = multi1.Read<int>().SingleOrDefault();

                return new PagedList<RealTimePrice>(lst, pager.PageIndex, pager.PageSize, total);
            }
        }

        public void InsertPrice(IList<RealTimePrice> entity)
        {
            _realtimePriceRep.InsertMany(entity);
        }

        public IList<RealTimePriceReport> GetPriceReport(int typeId, string product, string market = "", object formDate = null, object toDate = null)
        {
            using (var con = _dbProvider.Connection())
            {
                var query = "SELECT PriceDate,Price  FROM realtimeprice";
                List<String> WhereColumns = new List<string>();

                WhereColumns.Add(string.Format(" Product = '{0}' ", product));
                WhereColumns.Add(string.Format(" TypeId = '{0}' ", typeId));

                if (!string.IsNullOrEmpty(market))
                    WhereColumns.Add(string.Format(" Market = '{0}' ", market));

                if (formDate != null && toDate != null)
                {
                    WhereColumns.Add(string.Format(" ( PriceDate BETWEEN '{0}' AND '{1}' ) ", DateTime.Parse(formDate.ToString()).ToString("yyyy-MM-dd 00:00:00"), DateTime.Parse(toDate.ToString()).ToString("yyyy-MM-dd 23:59:59")));
                }

                if (WhereColumns.Any())
                    query = query + string.Format(" WHERE {0} ", string.Join(" AND ", WhereColumns));


                query += " ORDER BY PriceDate ";

                return con.Query<RealTimePriceReport>(query).ToList();



            }

        }


        public RealTimePriceSetting GetRealTimeSettingByCompany(int companyId)
        {

            var settings = _realSettingRep.GetList(new { Company_Id = companyId }).FirstOrDefault();

            return settings;

        }


        public void UpdateRealTimeSetting(RealTimePriceSetting entity)
        {

            _realSettingRep.Update(entity);

        }

        public void InsertRealTimeSetting(RealTimePriceSetting entity)
        {

            _realSettingRep.Insert(entity);

        }

        public RealTimePriceSetting GetRealTimeSettingById(int id)
        {

            return _realSettingRep.GetById(id);

        }


        public IList<RealtimePriceDrop> GetPriceDrop(string[] products, DateTime? compareDate, DateTime? compareWith)
        {
            using(var db = _dbProvider.Connection())
            {
                var lst = db.Query<RealTimePrice, RealTimePrice, RealtimePriceDrop>($"Select TODAY.* , UESTER.* from  (Select * from realtimeprice Where PriceDate = date(\"{ compareDate.Value.ToString("yyyy-MM-dd") } \") ) As TODAY LEFT JOIN (Select * from realtimeprice Where PriceDate = date(\"{ compareWith.Value.ToString("yyyy-MM-dd") }\")) As UESTER ON UESTER.Product = TODAY.Product  Where TODAY.Price < UESTER.Price AND TODAY.Product IN @products ;" , (t,y)=> {

                    var drop = new RealtimePriceDrop();
                    drop.Today = t;
                    drop.Yesterday = y;
                    return drop;
                },param: new { products = products });

                return lst.ToList();
            }
        }

        public IList<RealTimePrice> GetPriceThershold(string[] products, DateTime? date)
        {
            using (var db = _dbProvider.Connection())
            {
                var lst = db.Query<RealTimePrice>($"Select * from realtimeprice Where PriceDate = date(\"{ date.Value.ToString("yyyy-MM-dd") } \") AND Product IN @products ;", new
                {

                    products = products

                });

                return lst.ToList();
            }
        }

        public IList<CompanyUser> GetCompanyUser(int companyId)
        {
            using (var db = _dbProvider.Connection())
            {
                var lst = db.Query<CompanyUser>($"select U_ID as User_Id, concat( U_FNAME,' ', U_LNAME) as Name , U_EMAIL as Email  from user  where u_id = (select  CREATED_BY from company Where COMP_ID = @CompanyId );", new
                {

                    CompanyId = companyId

                });

                return lst.ToList();
            }
        }
    }
}
