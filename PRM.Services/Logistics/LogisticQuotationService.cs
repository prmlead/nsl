﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRM.Core.Domain.Logistics;
using PRM.Data;
using Dapper;
using PRM.Core.Domain.Vendors;
using PRM.Services.Vendors;

namespace PRM.Services.Logistics
{
    public class LogisticQuotationService : ILogisticQuotationService
    {
        public LogisticQuotationService(IRepository<LogisticQuotation> logisticQuotationRep,
           IDbProvider dbProvider, IVendorService vendorService)
        {
            _logisticQuotationRep = logisticQuotationRep;
            _dbProvider = dbProvider;
            _vendorService = vendorService;
        }

        private readonly IRepository<LogisticQuotation> _logisticQuotationRep;
        private readonly IVendorService _vendorService;
        private readonly IDbProvider _dbProvider;
        public IList<LogisticQuotationReport> GetLogisticQuotationByRequrimentId(int requirment)
        {
            using (var db = _dbProvider.Connection())
            {
                //

                var query = @" SELECT Distinct  LQ.QUOT_ID as QuotationId, LRD.REQ_ID AS  RequirementId, LRI.ITEM_ID AS ItemId, LAD.U_ID AS VendorId,  C.COMPANY_NAME AS VendorCompanyName,
                        LRI.PROD_ID AS ProductId, LRI.PROD_NO AS ProductNo, LRI.QUANTITY as ProductQuantity,
                        LQ.AIRLINE AS Airlines, LQ.ROUTING AS Routing , LQ.TRANSIT AS Transit , LQ.UNIT_TARIFF AS Tariff , LQ.UNIT_PRICE AS OfferedRate,
                        LQ.UNIT_PRICE_TYPE AS OfferedRateType,
                        LQ.REV_UNIT_PRICE AS RevisedOfferedRate, LQ.REV_UNIT_PRICE_TYPE AS RevisedOfferedRateType, LQ.FSC AS Fsc, LQ.FSC_TYPE AS FscType, 
                        LQ.SSC AS Ssc, LQ.SSC_TYPE AS SscType,LQ.CUSTOM_CLEARANCE as CustomClearance , LQ.CUSTOM_CLEARANCE_TYPE as  CustomClearanceType, LQ.SERVICE_CHARGES as ServiceCharges ,LQ.SERVICE_CHARGES_TYPE as ServiceChargesType, LQ.TERMINAL_HANDLING as TerminalHandling , LQ.TERMINAL_HANDLING_TYPE as  TerminalHandlingType,
                        LQ.MISC AS Misc, LQ.MISC_TYPE AS MiscType, LQ.OTHER_CHARGES AS OtherCharges, LQ.OTHER_CHARGES_TYPE AS OtherChargesType,
                        LQ.REV_OTHER_CHARGES AS RevisedOtherCharges, LQ.REV_OTHER_CHARGES_TYPE AS RevisedOtherChargesType, LQ.X_RAY AS Xray, LQ.X_RAY_TYPE AS XrayType,
                        LQ.AMS AS Ams, LQ.AMS_TYPE AS AmsType, LQ.AWB_FEES AS AwbFee, LQ.AWB_FEES_TYPE AS AwbFeeType, LAD.OTHER_PROPERTIES as VendorRemark,
                        LQ.CG_FEES AS DgFee,  LQ.CG_FEES_TYPE AS DgFeeType, LQ.FORWORD_DG_FEE AS ForwordDgFee, LQ.FORWORD_DG_FEE_TYPE AS ForwordDgFeeType, LAD.IS_QUOTATION_REJECTED as IsQuotationRejected, LRD.CLOSED as RequirmentStatus, 
                        LRD.REQ_TITLE AS Title, LQ.INVOICE_NUMBER as InvoiceNumber,LQ.INVOICE_DATE as InvoiceDate,LQ.CONSIGNEE_NAME as ConsigneeName
                        FROM logistic_quotations LQ  
                        LEFT JOIN logistic_requirementdetails LRD  ON  LRD.REQ_ID = LQ.REQ_ID
                        LEFT JOIN logistic_requirementitems LRI ON LRI.ITEM_ID  = LQ.ITEM_ID 
						left JOIN logistic_auctiondetails LAD ON LAD.REQ_ID = LQ.REQ_ID AND LAD.U_ID = LQ.U_ID
                        LEFT JOIN company C ON  LAD.U_ID = C.CREATED_BY
                        WHERE LQ.REQ_ID = @requirment AND   LRD.CLOSED != 'DELETED' AND LAD.IS_QUOTATION_REJECTED = 0 AND  LAD.IS_DELETED = 0  AND LQ.IS_SELECTED = 1";
                //, LQ.INVOICE_NUMBER as InvoiceNumber, LQ.INVOICE_DATE as InvoiceDate, LQ.CONSIGNEE_NAME as ConsigneeName
                var lst = db.Query<LogisticQuotationReport>(query, new { requirment });

                lst.ToList().ForEach(e =>
                {
                    e.Total = e.CalulateTotal();
                    e.RevTotal = e.CalulateRevTotal();
                });
                return lst.OrderBy(e=>e.Total).ThenBy(e=>e.RevTotal).ToList();
            }

        }

        public void UpdateQuotationCharges(LogisticQuotationReport entity)
        {

            using (var db = _dbProvider.Connection())
            using (var trans = db.BeginTransaction())
            {
                try
                {
                    
                    var query = "UPDATE logistic_quotations SET TERMINAL_HANDLING =  @TERMINAL_HANDLING , SERVICE_CHARGES = @SERVICE_CHARGES , CUSTOM_CLEARANCE = @CUSTOM_CLEARANCE , INVOICE_NUMBER = @INVOICE_NUMBER , INVOICE_DATE = @INVOICE_DATE , CONSIGNEE_NAME = @CONSIGNEE_NAME WHERE  QUOT_ID = @QUOT_ID ";
                    db.Execute(query, new { TERMINAL_HANDLING = entity.TerminalHandling , SERVICE_CHARGES = entity.ServiceCharges , CUSTOM_CLEARANCE = entity.CustomClearance , QUOT_ID = entity.QuotationId , INVOICE_NUMBER = entity.InvoiceNumber , INVOICE_DATE = entity.InvoiceDate , CONSIGNEE_NAME = entity.ConsigneeName }, trans);
                    trans.Commit();
                    //
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                }
            }
        }
    }
}
